﻿using Newtonsoft.Json.Linq;
using UnityEngine;

namespace Sportfaction.CasualFantasy.Stadium
{
    [System.Serializable]
    public sealed class StadiumsItemData
    {
        #region Properties

        public string Name { get { return _name; } }

        public string Tax { get { return _tax; } }

        public string Cashback { get { return _cashback; } }

        public bool IsAvailable { get { return _isAvailable; } set { _isAvailable = value; } }

        public Sprite SpriteBG { get { return _spriteBG; } }

        public Sprite SpritePreMatchBG { get { return _spritePreMatchBG; } }

        public Sprite SpriteIconStadium { get { return _spriteIconStadium; } }

        #endregion

        #region Fields


        [SerializeField, Tooltip("_name")]
        private string _name;

        [SerializeField, Tooltip("_tax")]
        private string _tax;

        [SerializeField, Tooltip("_cashback")]
        private string _cashback;

        [SerializeField, Tooltip("_isAvailable")]
        private bool _isAvailable;

        [SerializeField, Tooltip("_spriteBG")]
        private Sprite _spriteBG;

        [SerializeField, Tooltip("_spritePreMatchBG")]
        private Sprite _spritePreMatchBG;

        [SerializeField, Tooltip("_spriteIconStadium")]
        private Sprite _spriteIconStadium;

        #endregion

    }
}

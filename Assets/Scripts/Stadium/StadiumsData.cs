﻿using UnityEngine;

namespace Sportfaction.CasualFantasy.Stadium
{
    [System.Serializable]
    public sealed class StadiumsData
    {
        #region Properties

        public StadiumsItemData[] Items { get { return _items; } }

        #endregion

        #region Fields

        [SerializeField, Tooltip("Stadium")]
        private StadiumsItemData[] _items;

        #endregion


    }
}

﻿using Sportfaction.CasualFantasy.Events;
using Sportfaction.CasualFantasy.Manager.Profile;
using Sportfaction.CasualFantasy.Services;
using Sportfaction.CasualFantasy.Services.Userstracking;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.SceneManagement;
using Sportfaction.CasualFantasy.PVP.Gameplay.Matchs;
using Sportfaction.CasualFantasy.Meta;
using Sportfaction.CasualFantasy.Menu;
using UnityEngine.UI;
using System.Collections;
using DG.Tweening;
using UnityEngine.Events;
using System.Collections.Generic;

namespace Sportfaction.CasualFantasy.PostSplashScreen
{
    /// <summary>
    /// Description: UserMain
    /// Author: Antoine de Lachèze-Murel
    /// </summary>
    public class PostSplashScreenMain : MonoBehaviour
    {
        #region Fields


        [Header("UI")]
        [SerializeField, Tooltip("DefaultUI")]
        private DefaultUI _defaultUI;
        [SerializeField, Tooltip("Soccer Arena BG")]
        private GameObject _soccerArenaBG;
        [SerializeField, Tooltip("Black BG")]
        private GameObject _blackBG;
        [SerializeField, Tooltip("Logo SF")]
        private GameObject _logoSF;
        [SerializeField, Tooltip("Anim Loading")]
        private GameObject _animLoading;
        [SerializeField, Tooltip("Loading fill")]
        private Image _loadingFill;
        [SerializeField, Tooltip("Loading text")]
        private TMPro.TMP_Text _loadingTxt;
        [SerializeField, Tooltip("Main")]
        private GameObject _main;



        [Header("Data")]
		[SerializeField, Tooltip("Scriptable object contains infos about match UI")]
		private	PVPData		_pvpData;

		#pragma warning disable 0414

		[Header("Events Status")]
		[SerializeField, ReadOnly]
		private	bool	_onConnectEvent;
		[SerializeField, ReadOnly]
		private	bool	_onGetProfileEvent;
		[SerializeField, ReadOnly]
		private	bool	_getTeamIdEvent;

        #pragma warning restore 0414


        [Header("Game Events")]
        [SerializeField]
        private List<string> _allEvents;

        [SerializeField]
        private GameEvent _onEndAnimationPostSplashScreen;


        private int _maxCalls; 
        private int _nbCallsEnded;
        private bool _screenDisplayed;

        #endregion

        #region Public Methods

        public void OnEndAnimationPostSplashScreenEvent()
        {
            #if !DEVELOPMENT_BUILD
               StartCoroutine(ScenesMain.Instance.LoadAsyncMetaScene());
            #endif
        }

        public void OnEndLoadAsyncMetaScene_GE()
        {
            _defaultUI.AnimateCanevasOut(_main);

            ScenesMain.Instance.UnloadAsyncMetaScene();
        }

        public void NoUpdate_GE()
        {
            UpdateLoader("NoUpdate_GE");

#if UNITY_EDITOR
            object body = new
                {
                    device_id = ApiConstants.editorDeviceId,
                    language = Application.systemLanguage.ToString()
                };
            #else
                object body = new
                {
                    device_id = SystemInfo.deviceUniqueIdentifier,
                    language = Application.systemLanguage.ToString()
                };
            #endif

            MetaData.Instance.UserMain.Connect(body);
        }
        
        //public void OnEmptyTeam()
        //{
        //    _onGetTeamId.Invoke();

        //    _onGetSquadPlayers.Invoke();
        //}

        public void OnConnect_GE()
        {
            UpdateLoader("OnConnect_GE");

            if(false == _onConnectEvent)
            {
                MetaData.Instance.UserMain.GetProfile();
            }

            _onConnectEvent = true;
        }

        public void OnGetProfile_GE()
        {
            if(false == _onGetProfileEvent)
            {
                UpdateLoader("OnGetProfile_GE");

                if (null != UserData.Instance && Application.systemLanguage.ToString() != UserData.Instance.Profile.Language)
                {
                    MetaData.Instance.UserMain.ChangeLanguage();
                }

                MetaData.Instance.SquadMain.GetTeam();

                MetaData.Instance.ActionMain.GetListActions("200");

                MetaData.Instance.ActionMain.GetListPositionsActions();

                MetaData.Instance.ActionMain.GetActionsBalancing();

                MetaData.Instance.PositionsMain.GetPositionsManager();

                MetaData.Instance.ShopMain.GetProductList();

                MetaData.Instance.LeaderboardMain.GetOverallRankingByTrophy("13", "0");

                MetaData.Instance.ArenaSeasonMain.GetRewardsArenaSeason();

                MetaData.Instance.LootMain.PreloadLoot();

                MetaData.Instance.LootMain.GetLootTimers();

                MetaData.Instance.PlayersMain.GetPlayersManager();

                MetaData.Instance.ChampionshipMain.GetChampionshipNextPlayerFormUpdate("France");

                MetaData.Instance.ChampionshipMain.GetChampionshipNextPlayerFormUpdate("Germany");

                MetaData.Instance.ChampionshipMain.GetChampionshipNextPlayerFormUpdate("Italy");

                MetaData.Instance.ChampionshipMain.GetChampionshipNextPlayerFormUpdate("England");

                MetaData.Instance.ChampionshipMain.GetChampionshipNextPlayerFormUpdate("Spain");
            }

            if (Application.platform == RuntimePlatform.IPhonePlayer)
            {
                UserEventsManager.Instance.RecordEvent(new UserActionEvent("ios_onboarding"));
            }
            else
            {
                UserEventsManager.Instance.RecordEvent(new UserActionEvent("android_onboarding"));
            }

            _onGetProfileEvent = true;
        }

        public void OnEmptyTeam_GE()
        {
            UpdateLoader("OnGetTeamId_GE");
            UpdateLoader("OnGetSquadPlayers_GE");
        }

        public void OnGetTeamId_GE()
        {
            UpdateLoader("OnGetTeamId_GE");

            if(false == _getTeamIdEvent)
            {
                Assert.IsNotNull(_pvpData, "[PostSplashScreenMain] OnGetTeamIdEvent(), _pvpData is null.");

                if (_pvpData.TeamId != -1)
                {

                    MetaData.Instance.SquadMain.GetSquadPlayers(MetaData.Instance.SquadMain.PvpData.TeamId);
                    // INIT MATCH ENGINE
                    MatchEngine.Instance.InitEngine();
                }
            }

            _getTeamIdEvent = true;
        }


        public void OnGetPlayersManager_GE() { UpdateLoader("OnGetPlayersManager_GE"); }
        public void OnGetRewardsArenaSeason_GE() { UpdateLoader("OnGetRewardsArenaSeason_GE"); }
        public void OnGetOverallRanking_GE() { UpdateLoader("OnGetOverallRanking_GE"); }
        public void OnGetPositionsActionsList_GE() { UpdateLoader("OnGetPositionsActionsList_GE"); }
        public void OnMatchPositionsGet_GE() { UpdateLoader("OnMatchPositionsGet_GE"); }
        public void OnGetAppParamManager_GE() { UpdateLoader("OnGetAppParamManager_GE"); }
        public void OnGetProductsIAPStore_GE() { UpdateLoader("OnGetProductsIAPStore_GE"); }
        public void OnGetProductList_GE() { UpdateLoader("OnGetProductList_GE"); }
        public void OnGetActions_GE() { UpdateLoader("OnGetActions_GE"); }
        public void OnGetSquadPlayers_GE() { UpdateLoader("OnGetSquadPlayers_GE"); }
        public void OnUpdateStatusReceived_GE() { UpdateLoader("OnUpdateStatusReceived_GE"); }
        public void OnGetLootTimers_GE() { UpdateLoader("OnGetLootTimers_GE"); }


       

        public void UpdateLoader(string pName)
        {
            if(_allEvents.Contains(pName))
            {
                _allEvents.Remove(pName);

                float lLastNbCalls = (float)_nbCallsEnded;
                _nbCallsEnded++;

                _ = DOTween.To(() => lLastNbCalls, x => lLastNbCalls = x, _nbCallsEnded, 0.5f).OnUpdate(() =>
                {
                    float lNbCalls = lLastNbCalls > _maxCalls ? _maxCalls : lLastNbCalls;
                    UtilityMethods.SetText(_loadingTxt, ((lNbCalls / _maxCalls) * 100f).ToString("F0") + "%");
                    _loadingFill.fillAmount = (lLastNbCalls / _maxCalls);
                }).OnComplete(() =>
                {
                    if (lLastNbCalls >= _maxCalls && true == _screenDisplayed)
                    {
                        _onEndAnimationPostSplashScreen.Invoke();
                    }
                });
            }
           
        }

        #endregion

        #region Private Methods

        private void OnEnable()
        {
            _allEvents = new List<string>
            {
                "OnGetProfile_GE",
                "OnGetTeamId_GE",
                "OnGetPlayersManager_GE",
                "OnGetRewardsArenaSeason_GE",
                "OnGetOverallRanking_GE",
                "OnGetPositionsActionsList_GE",
                "OnMatchPositionsGet_GE",
                "OnGetProductsIAPStore_GE",
                "OnGetProductList_GE",
                "OnGetActions_GE",
                "OnGetSquadPlayers_GE",
                "OnGetLootTimers_GE"
            };


            #if !DEVELOPMENT_BUILD
                _allEvents.Add("NoUpdate_GE");
                _allEvents.Add("OnUpdateStatusReceived_GE");
            #endif

            _maxCalls = _allEvents.Count;
        }

        private void    Start()
        {


            MetaData.Instance.AppParamMain.GetAppParamManagerList();

     
            UserEventsManager.Instance.StartTracking();


            if(false == UserData.Instance.Profile.OnBoarding)
            {
                UserData.Instance.Profile.OnBoardingHome = false;

                UserData.Instance.PersistUserData();
            }

             _screenDisplayed = false;

            #if !DEVELOPMENT_BUILD
                StartCoroutine(StartAnimation());
            #else
            _screenDisplayed = true;
            #endif

        }


        private IEnumerator StartAnimation()
        {
            _defaultUI.AnimateCanevasIn(_logoSF, 0.5f);

            yield return new WaitForSeconds(1f);

            _defaultUI.AnimateCanevasOut(_logoSF, 0.5f);

            yield return new WaitForSeconds(0.5f);

            _defaultUI.AnimateBackgroundInOut(_soccerArenaBG, _blackBG, 0.5f);

            _defaultUI.AnimateCanevasIn(_animLoading);

            yield return new WaitForSeconds(0.5f);

            _screenDisplayed = true;
        }


        #endregion
    }
}

﻿using Sportfaction.CasualFantasy.Manager.Profile;
using Sportfaction.CasualFantasy.Menu;
using Sportfaction.CasualFantasy.PVP.Network;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Sportfaction.CasualFantasy.PVP.Gameplay.Matchs;
using System.Collections;
using Sportfaction.CasualFantasy.Meta;
//using BugseePlugin;

namespace Sportfaction.CasualFantasy.PostSplashScreen
{
    /// <summary>
    /// Class handling DEV splash screen
    /// </summary>
    public class PostSplashScreenMainDEV : MonoBehaviour
    {
        #region Fields

        [Header("Links")]
        [SerializeField, Tooltip("DefaultUI")]
        private DefaultUI   _defaultUI;

        [Header("Data")]
        [SerializeField, Tooltip("Scriptable object contains infos about match UI")]
        private PVPData _pvpData;

        [Header("UI")]

        [SerializeField, Tooltip("LoaderGrp")]
        private GameObject _loaderGrp;


        [SerializeField, Tooltip("Username Step")]
        private GameObject  _usernameStep;

        [SerializeField, Tooltip("Menu Step")]
        private GameObject _menuStep;

        [SerializeField, Tooltip("Edit Profile Step")]
        private GameObject  _editProfileStep;

        [SerializeField, Tooltip("PVP Step")]
        private GameObject  _pvpStep;

        [SerializeField, Tooltip("_patchUserSuccess")]
        private GameObject _patchUserSuccess;

        [SerializeField, Tooltip("_badCredentials")]
        private GameObject _badCredentials;

        [SerializeField, Tooltip("Username Text")]
        private TMPro.TMP_InputField _username;

        [SerializeField, Tooltip("Password Text")]
        private TMPro.TMP_InputField _password;

        [SerializeField, Tooltip("_onBoarding")]
        private Toggle _onBoarding;

        [SerializeField, Tooltip("_onEmptyMinions")]
        private Toggle _onEmptyMinions;

        [SerializeField]
        private TMPro.TMP_InputField _hardEdit;

        [SerializeField]
        private TMPro.TMP_InputField _fragmentsEdit;

        [SerializeField]
        private TMPro.TMP_InputField _trophyEdit;

        [SerializeField]
        private TMPro.TMP_InputField _trophyMaxEdit;

        [SerializeField]
        private TMPro.TMP_InputField _arenaSeasonUnlockStepEdit;

        TouchScreenKeyboard keyboard;

        bool _startOnboarding;

        bool _startMenu;

        #endregion

        #region Public Methods

        public void OnEndAnimationPostSplashScreenEvent()
        {
            _defaultUI.AnimateCanevasOut(_loaderGrp);

     

        }

        public void OnConnectEvent()
        {
            Assert.IsNotNull(UserData.Instance, "[EndMatchWinLoseUI] OnConnectEvent(), UserData.Instance is null.");

			Assert.IsNotNull(UserData.Instance.Profile, "[EndMatchWinLoseUI] OnConnectEvent(), UserData.Instance.Profile is null.");

			UserData.Instance.Profile.Username = _username.text;

			UserData.Instance.Profile.Password = _password.text;

            UserData.Instance.PersistUserData();

			MetaData.Instance.UserMain.GetProfile();
			
        }

        public void OnGetProfileEvent()
        {
            //Bugsee.SetAttribute("pseudo", UserData.Instance.Profile.Pseudo);
            if (true == _startOnboarding)
            {
                StartCoroutine(ScenesMain.Instance.LoadAsyncMetaScene());
            }
            else if (true == _startMenu)
            {
                StartCoroutine(ScenesMain.Instance.LoadAsyncMetaScene());
            }
            else
            {

                Assert.IsNotNull(UserData.Instance, "[EndMatchWinLoseUI] OnGetProfileEvent(), UserData.Instance is null.");

                Assert.IsNotNull(UserData.Instance.Profile, "[EndMatchWinLoseUI] OnGetProfileEvent(), UserData.Instance.Profile is null.");

                if (Application.systemLanguage.ToString() != UserData.Instance.Profile.Language)
                {
                    MetaData.Instance.UserMain.ChangeLanguage();
                }

                _fragmentsEdit.text = UserData.Instance.Profile.Fragment.ToString();

                _hardEdit.text = UserData.Instance.Profile.Hard.ToString();

                _trophyEdit.text = UserData.Instance.Profile.Score.ToString();

                _trophyMaxEdit.text = UserData.Instance.Profile.MaxScore.ToString();

                _arenaSeasonUnlockStepEdit.text = UserData.Instance.Profile.LastArenaSeasonReward.ToString();

                _onBoarding.isOn = UserData.Instance.Profile.OnBoarding;

                _onEmptyMinions.isOn = UserData.Instance.Profile.OnEmptyMinions;

                _defaultUI.AnimateCanevasIn(_menuStep, 0.2f);
                _defaultUI.AnimateCanevasInOut(_loaderGrp, _usernameStep, 0.2f);

                _defaultUI.AnimateCanevasOut(_editProfileStep, 0.2f);
            }
           
        }

        public void ConnectUser()
        {
            _defaultUI.AnimateCanevasOut(_badCredentials, 0.2f);

            if (!String.IsNullOrEmpty(_username.text) && !String.IsNullOrEmpty(_password.text))
            {
                Dictionary<string, string> body = new Dictionary<string, string>
                {
                    { "username", _username.text },
                    { "password", _password.text }
                };

                MetaData.Instance.UserMain.LoginDefault(body);

                // Error.SetActive(true);
                // ErrorTxt.text = I2.Loc.LocalizationManager.GetTranslation("UsernameEmpty");
            }
        }

        public void StartPVPMatch()
        {
            _defaultUI.AnimateCanevasOut(_menuStep, 0f);

            UserData.Instance.Redirect = "matchmaking_pvp";

            //SceneManager.LoadScene("Meta");

            StartCoroutine(ScenesMain.Instance.LoadAsyncMetaScene());
        }

        /// <summary>
        /// PunManager Game Event : OnReconnectFailed
        /// </summary>
        public void OnReconnectFailed()
        {
            PhotonMain.Instance.Search();
        }

        public void StartBotMatch()
        {
            _defaultUI.AnimateCanevasOut(_menuStep, 0f);

            Assert.IsNotNull(_pvpData, "[MatchUI] OnPUNRoomReady(), _pvpData is null.");

            UserData.Instance.Redirect = "matchmaking_bot";

            StartCoroutine(ScenesMain.Instance.LoadAsyncMetaScene());
        }

        public void CancelPVPMatch()
        {
            _defaultUI.AnimateCanevasInOut(_menuStep, _pvpStep, 0.2f);
            PhotonMain.Instance.Cancel();
        }


        public void StartOnBoarding()
        {
            _startOnboarding = true;

            Debug.Log("UserData.Instance.Profile.OnBoardingHome = " + UserData.Instance.Profile.OnBoardingHome);

            Dictionary<string, object> body = new Dictionary<string, object>
            {
                { "tutorial_completed", false }
            };

            MetaData.Instance.UserMain.PatchUser(body);
            
        }


        public void GoToMenu()
        {

            _startMenu = true;
            UserData.Instance.Profile.OnBoardingHome = false;
            UserData.Instance.PersistUserData();

            Dictionary<string, object> body = new Dictionary<string, object>
            {
                { "tutorial_completed", true }
            };

            MetaData.Instance.UserMain.PatchUser(body);
        }

        public void EditProfileGoBack()
        {
            _defaultUI.AnimateCanevasInOut(_menuStep, _editProfileStep, 0.2f);
            _defaultUI.AnimateCanevasOut(_patchUserSuccess, 0.2f);
        }

        public void EditProfile()
        {
            _defaultUI.AnimateCanevasInOut(_editProfileStep, _menuStep, 0.2f);
        }

        public void Save()
        {
			bool lTutoCompleted = !_onBoarding.isOn;

            Dictionary<string, object> body = new Dictionary<string, object>() { };

            body.Add("tutorial_completed", lTutoCompleted);

            if (!String.IsNullOrEmpty(_hardEdit.text))
            {
                body.Add("hard", _hardEdit.text);
            }

            if (!String.IsNullOrEmpty(_fragmentsEdit.text))
            {
                body.Add("fragment", _fragmentsEdit.text);
            }

            if (!String.IsNullOrEmpty(_trophyEdit.text))
            {
                body.Add("score", _trophyEdit.text);
            }

            if (!String.IsNullOrEmpty(_trophyMaxEdit.text))
            {
                body.Add("max_score", _trophyMaxEdit.text);
            }

            if (!String.IsNullOrEmpty(_arenaSeasonUnlockStepEdit.text))
            {
                body.Add("last_arenaseason_reward", _arenaSeasonUnlockStepEdit.text);
            }

            MetaData.Instance.UserMain.PatchUser(body);

            if(_onEmptyMinions.isOn != UserData.Instance.Profile.OnEmptyMinions)
            {
                UserData.Instance.Profile.OnEmptyMinions = _onEmptyMinions.isOn;
                UserData.Instance.PersistUserData();
                MatchEngine.Instance.InitEngine();
            }
      
            Assert.IsNotNull(UserData.Instance, "[PostPlashScreenMainDEV] Save(), UserData.Instance is null.");

			Assert.IsNotNull(UserData.Instance.Profile, "[PostPlashScreenMainDEV] Save(), UserData.Instance.Profile is null.");

            StartCoroutine(OnPatchUserEvent());


        }

        public void Reset()
        {
            

            UserData.Instance.Profile.OnBoarding = true;
            UserData.Instance.Profile.OnBoardingHome = true;
            UserData.Instance.Profile.OnBoardingMeta = true;
            UserData.Instance.PersistUserData();

            MetaData.Instance.UserMain.ResetUserLoots();

            MetaData.Instance.UserMain.ResetUserTeam();

            Dictionary<string, object> body = new Dictionary<string, object>
            {
                { "tutorial_completed", false },
                { "score", 0 }
            };

            _startOnboarding = true;

            MetaData.Instance.UserMain.PatchUser(body);

        }

        public IEnumerator OnPatchUserEvent()
        {
            _defaultUI.AnimateCanevasIn(_patchUserSuccess, 0.2f);
            yield return new WaitForSeconds(1.3f);
            _defaultUI.AnimateCanevasOut(_patchUserSuccess, 0.22f);
			//_userMain.GetProfile();
        }

        /// <summary>
        /// Displays bad credential error panel
        /// </summary>
        public void OnBadCredentialsEvent()
        {
            _defaultUI.AnimateCanevasIn(_badCredentials, 0.2f);
        }
        
        #endregion

        #region Private Methods

        /// <summary>
        /// Sets username & password in inputs if data saved
        /// </summary>
        private void    Start()
        {
			Assert.IsNotNull(UserData.Instance, "[PostPlashScreenMainDEV] Start(), UserData.Instance is null.");

			Assert.IsNotNull(UserData.Instance.Profile, "[PostPlashScreenMainDEV] Start(), UserData.Instance.Profile is null.");

            if(null != UserData.Instance.Profile && null != UserData.Instance.Profile.Username && null != UserData.Instance.Profile.Password)
            {
                _username.text	=	UserData.Instance.Profile.Username;
                _password.text	=	UserData.Instance.Profile.Password;
            }
        }

        /// <summary>
        /// Checks if user touches "Enter" key & launches connection
        /// </summary>
        private void    Update()
        {
            if (keyboard != null && keyboard.status == TouchScreenKeyboard.Status.Done)
            {
                ConnectUser();
            }
        }



        #endregion
    }

}

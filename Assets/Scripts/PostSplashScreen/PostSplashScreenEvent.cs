using Sportfaction.CasualFantasy.Menu;

using UnityEngine;
using UnityEngine.EventSystems;

namespace Sportfaction.CasualFantasy.PostSplashScreen
{
    /// <summary>
    /// Description: PostSplashScreenEvent
    /// Author: Antoine de Lachèze-Murel
    /// </summary>
    public class PostSplashScreenEvent : MonoBehaviour, IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler
    {
    
        #region Fields

        [Header("Links")]

        [SerializeField, Tooltip("PostSplashScreenMainDEV")]
        private PostSplashScreenMainDEV _postSplashScreenMainDEV = null;

        [SerializeField, Tooltip("DefaultUI")]
        private DefaultUI _defaultUI = null;

        [SerializeField, ReadOnly, Tooltip("_currentTransform")]
        private Transform _currentTransform = null;

        #endregion

        #region Public Methods

        /// <summary>
        /// Called by the EventSystem when the pointer
        /// enters the object associated with this EventTrigger.
        /// </summary>
        public void OnPointerEnter(PointerEventData eventData)
        {
            if (null != eventData.pointerEnter)
            {
                switch (eventData.pointerEnter.transform.name)
                {
                    case "[OK-Username]":
                    case "[EditProfile]":
                    case "[GoToMenu]":
                    case "[GoToOnBoarding]":
                    case "[StartPVPMatch]":
                    case "[StartBotMatch]":
                    case "[BackBtnPVPGame]":
                    case "EditBack_Grp":
                    case "Save_Grp":
                    case "Reset_Grp":
                        _defaultUI.AnimateButtonEnter(eventData.pointerEnter.transform);
                        _currentTransform = eventData.pointerEnter.transform;
                        break;
                }
            }

        }

        /// <summary>
        /// Called by the EventSystem when the pointer
        /// enters the object associated with this EventTrigger.
        /// </summary>
        public void OnPointerExit(PointerEventData eventData)
        {
            if (null != _currentTransform)
            {
                _defaultUI.AnimateButtonUp(_currentTransform);
            }
        }

        /// <summary>
        /// Register button presses using the IPointerClickHandler
        /// </summary>
        public void OnPointerClick(PointerEventData eventData)
        {
            if (null != _currentTransform)
            {
                _defaultUI.AnimateButtonUp(_currentTransform);
                GoTo();
            }
        }

        #endregion

        #region Private Methods

        private void GoTo()
        {
            if (null != _currentTransform)
            {

                switch (_currentTransform.name)
                {

                    case "[OK-Username]":
                        _postSplashScreenMainDEV.ConnectUser();
                        break;
                    case "[GoToMenu]":
                        _postSplashScreenMainDEV.GoToMenu();
                        break;
                    case "[GoToOnBoarding]":
                        _postSplashScreenMainDEV.StartOnBoarding();
                        break;
                    case "[StartPVPMatch]":
                        _postSplashScreenMainDEV.StartPVPMatch();
                        break;
                    case "[StartBotMatch]":
                        _postSplashScreenMainDEV.StartBotMatch();
                        break;
                    case "[BackBtnPVPGame]":
                        _postSplashScreenMainDEV.CancelPVPMatch();
                        break;
                    case "[EditProfile]":
                        _postSplashScreenMainDEV.EditProfile();
                        break;
                    case "EditBack_Grp":
                        _postSplashScreenMainDEV.EditProfileGoBack();
                        break;
                    case "Save_Grp":
                        _postSplashScreenMainDEV.Save();
                        break;
                    case "Reset_Grp":
                        _postSplashScreenMainDEV.Reset();
                        break;

                }
            }
        }

        #endregion
    }
}
using Sportfaction.CasualFantasy.PVP;

using UnityEngine;

namespace Sportfaction.CasualFantasy.Manager.Profile
{
    /// <summary>
    /// Description: Class contains data about pvp
    /// Author: Antoine
    /// </summary>
    [CreateAssetMenu(fileName = "Store-PVPData", menuName = "CasualFantasy/Data/Store/PVP")]
    public sealed class PVPData : ScriptableObject
    {
        #region Properties

        public int                      TeamId                  { get { return _teamId; } set { _teamId = value; } }

		// Property used to get the id of the opponent team
		public	int						OpponentTeamId			{ get { return _opponentTeamId; } set { _opponentTeamId = value; } }

        public string                   OpponentName            { get { return _opponentName; } set { _opponentName = value; } }

        public string                   OpponentSoft            { get { return _opponentSoft; } set { _opponentSoft = value; } }

        public ePVPGameMode             GameMode                 { get { return _gameMode; } set { _gameMode = value; } }

        #endregion

        #region Fields

        [SerializeField, Tooltip("_teamId")]
        private int              _teamId = -1;

		[SerializeField, Tooltip("Id of the opponent team")]
		private	int					_opponentTeamId	=	-1;

        [SerializeField, Tooltip("_opponentName")]
        private string            _opponentName;

        [SerializeField, Tooltip("_opponentSoft")]
        private string            _opponentSoft;

        [SerializeField, Tooltip("_gameMode")]
        private ePVPGameMode      _gameMode;

        #endregion


    }
}

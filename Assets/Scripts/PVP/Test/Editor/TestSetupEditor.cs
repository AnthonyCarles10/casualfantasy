﻿using Sportfaction.CasualFantasy.PVP.Test;

namespace Sportfaction.CasualFantasy.Events.Editor
{
    /// <summary>
    /// Description: 
    /// Author: Madeleine Tranier
    /// </summary>
    [UnityEditor.CustomEditor(typeof(MatchSimulation))]
    public sealed class TestSetupInspector : UnityEditor.Editor
    {
        #region Public Methods

        public  override    void    OnInspectorGUI()
        {
            base.OnInspectorGUI();
            
            //TestSetup  lEvent  =   target as TestSetup;

            //Assert.IsNotNull(lEvent, "TestSetupInspector: OnInspectorGUI() failed, lEvent is null.");

            //if (GUILayout.Button("Change Setup"))
            //{
            //    lEvent.ChangeSetup();
            //}

            //if (GUILayout.Button("Display Action Choices"))
            //{
            //    lEvent.DisplayActionChoices();
            //}

            ////if (GUILayout.Button("Simulate Pass"))
            ////{
            ////    lEvent.SimulatePass();
            ////}

            ////if (GUILayout.Button("Preview FS Init"))
            ////{
            ////    lEvent.PreviewFSInit();
            ////}

            //if (GUILayout.Button("Cancel Action Preparation"))
            //{
            //    lEvent.CancelAction();
            //}

            //if (GUILayout.Button("Prepare Own Action"))
            //{
            //    lEvent.PrepareOwnAction();
            //}

            //if (GUILayout.Button("Prepare Opponent Action"))
            //{
            //    lEvent.PrepareOppAction();
            //}

            //if (GUILayout.Button("Execute Actions"))
            //{
            //    lEvent.ExecuteActions();
            //}
        }

        #endregion
    }
}
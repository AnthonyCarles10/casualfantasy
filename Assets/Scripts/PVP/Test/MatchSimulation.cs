﻿using Sportfaction.CasualFantasy.Manager.Profile;
using Sportfaction.CasualFantasy.PVP.Gameplay.Actions;
using Sportfaction.CasualFantasy.PVP.Gameplay.Field;
using Sportfaction.CasualFantasy.PVP.Gameplay.Matchs;
using Sportfaction.CasualFantasy.PVP.Gameplay.Players;

using SportFaction.CasualFootballEngine.ActionService.Enum;
using SportFaction.CasualFootballEngine.ActionService.Response;
using SportFaction.CasualFootballEngine.FieldService.Model;
using SportFaction.CasualFootballEngine.PhaseService.Enum;
using SportFaction.CasualFootballEngine.PlayerService.Enum;
using SportFaction.CasualFootballEngine.PlayerService.Model;
using SportFaction.CasualFootballEngine.TeamService.Enum;

using System.Collections;
using UnityEngine;

using SFManager = SportFaction.CasualFootballEngine.ManagerService.Model.Manager;

namespace Sportfaction.CasualFantasy.PVP.Test
{
    /// <summary>
    /// Simulates an entire match scenario
    /// </summary>
    /// @author Madeleine Tranier
    public sealed class MatchSimulation : Singleton<MatchSimulation>
    {
		#region Fields

		[Header("Simulation settings")]
		[SerializeField, Tooltip("Test action phase only ?")]
		public bool    TestActionOnly   =   false;
		[SerializeField, Tooltip("Bypass prematch only ?")]
		public bool    ActivateBOT      =   false;

        [Header("Data")]
        [SerializeField, Tooltip("Script with simulation data")]
        private MatchSimulationData _simulationData =   null; //!< Class storing entire match scenario
        [SerializeField, Tooltip("Scriptable object contains infos about match UI")]
        private PVPData             _pvpData        =   null; //!< Class storing every PVP related data

        private ForceChallengeValue _forceChallenge =   null;

        #endregion

        #region Public Methods

        #region Game Event Listeners

        public  void    OnPerformTactics()
        {
            if (_simulationData.CounterActionPhase > _simulationData.ActionPhases.Length - 1)
                return;

            _forceChallenge =   new ForceChallengeValue();

            ActionPhase lPhaseData  =   _simulationData.ActionPhases[_simulationData.CounterActionPhase];
            _forceChallenge.TeamAValue  =   50;
            _forceChallenge.TeamBValue  =   50;

            if (eTeam.TEAM_A == lPhaseData.DuelWinner && null != MatchEngine.Instance.GetDuelPlayer(eTeam.TEAM_A))
                _forceChallenge.Players.Add(new ForceChallengeValuePlayer(MatchEngine.Instance.GetDuelPlayer(eTeam.TEAM_A).Position.ToString(), eTeam.TEAM_A.ToString(), 70));
            if (eTeam.TEAM_B == lPhaseData.DuelWinner && null != MatchEngine.Instance.GetDuelPlayer(eTeam.TEAM_B))
                _forceChallenge.Players.Add(new ForceChallengeValuePlayer(MatchEngine.Instance.GetDuelPlayer(eTeam.TEAM_B).Position.ToString(), eTeam.TEAM_B.ToString(), 70));

            for (int li = 0; li < lPhaseData.TacticalChoices.TeamAChoices.Length; li++)
                if (lPhaseData.TacticalChoices.TeamAChoices[li].WinsCase)
                    _forceChallenge.Players.Add(new ForceChallengeValuePlayer(lPhaseData.TacticalChoices.TeamAChoices[li].Position.ToString(), eTeam.TEAM_A.ToString(), 60));
                else
                    _forceChallenge.Players.Add(new ForceChallengeValuePlayer(lPhaseData.TacticalChoices.TeamAChoices[li].Position.ToString(), eTeam.TEAM_A.ToString(), 40));

            for (int li = 0; li < lPhaseData.TacticalChoices.TeamBChoices.Length; li++)
                if (lPhaseData.TacticalChoices.TeamBChoices[li].WinsCase)
                    _forceChallenge.Players.Add(new ForceChallengeValuePlayer(lPhaseData.TacticalChoices.TeamBChoices[li].Position.ToString(), eTeam.TEAM_B.ToString(), 60));
                else
                    _forceChallenge.Players.Add(new ForceChallengeValuePlayer(lPhaseData.TacticalChoices.TeamBChoices[li].Position.ToString(), eTeam.TEAM_B.ToString(), 40));

            if (eTeam.NONE != lPhaseData.ResolutionWinningTeam && ePosition.NONE != lPhaseData.ResolutionWinningPosition)
                _forceChallenge.Players.Add(new ForceChallengeValuePlayer(lPhaseData.ResolutionWinningPosition.ToString(), lPhaseData.ResolutionWinningTeam.ToString(), 100));

            if (ePosition.NONE != lPhaseData.ResolutionDuelOpponentOpposition)
                _forceChallenge.Players.Add(new ForceChallengeValuePlayer(lPhaseData.ResolutionDuelOpponentOpposition.ToString(), eTeamMethods.GetOpposite(MatchEngine.Instance.Engine.PossessionMain.CurTeamPossession).ToString(), 60));

            MatchEngine.Instance.Engine.ActionMain.ForceScore = _forceChallenge.ConvertToJson();

            if (eTeam.TEAM_A == UserData.Instance.PVPTeam || (eTeam.TEAM_B == UserData.Instance.PVPTeam && !ActivateBOT))
            {
                for (int li = 0; li < lPhaseData.TacticalChoices.TeamAChoices.Length; li++)
                    StartCoroutine(_prepareTacticalAction(eExecutorMethods.GetEExecutor(eTeam.TEAM_A, false), 1.5f * li + 1f, lPhaseData.TacticalChoices.TeamAChoices[li]));
            }

            if (eTeam.TEAM_B == UserData.Instance.PVPTeam || (eTeam.TEAM_A == UserData.Instance.PVPTeam && !ActivateBOT))
            {
                for (int li = 0; li < lPhaseData.TacticalChoices.TeamBChoices.Length; li++)
                    StartCoroutine(_prepareTacticalAction(eExecutorMethods.GetEExecutor(eTeam.TEAM_B, false), 1.5f * li + 1f, lPhaseData.TacticalChoices.TeamBChoices[li]));
            }
        }

        public  void    OnPrepareAction()
        {
            if (ePhase.KICK_OFF == MatchEngine.Instance.CurPhase)
                return;

            _simulationData.CounterActionPhase++;

            if (_simulationData.CounterActionPhase > _simulationData.ActionPhases.Length - 1)
                return;

            //_simulationData.DebugPhase("Action");

            _forceChallenge =   new ForceChallengeValue();

            ActionPhase lPhaseData  =   _simulationData.ActionPhases[_simulationData.CounterActionPhase];
            _forceChallenge.TeamAValue  =   50;
            _forceChallenge.TeamBValue  =   50;

            if (eTeam.TEAM_A == lPhaseData.DuelWinner && null != MatchEngine.Instance.GetDuelPlayer(eTeam.TEAM_A))
                _forceChallenge.Players.Add(new ForceChallengeValuePlayer(MatchEngine.Instance.GetDuelPlayer(eTeam.TEAM_A).Position.ToString(), eTeam.TEAM_A.ToString(), 70));
            if (eTeam.TEAM_B == lPhaseData.DuelWinner && null != MatchEngine.Instance.GetDuelPlayer(eTeam.TEAM_B))
                _forceChallenge.Players.Add(new ForceChallengeValuePlayer(MatchEngine.Instance.GetDuelPlayer(eTeam.TEAM_B).Position.ToString(), eTeam.TEAM_B.ToString(), 70));

            if (eTeam.NONE != lPhaseData.ResolutionWinningTeam && ePosition.NONE != lPhaseData.ResolutionWinningPosition)
                _forceChallenge.Players.Add(new ForceChallengeValuePlayer(lPhaseData.ResolutionWinningPosition.ToString(), lPhaseData.ResolutionWinningTeam.ToString(), 100));

            if (ePosition.NONE != lPhaseData.ResolutionDuelOpponentOpposition)
                _forceChallenge.Players.Add(new ForceChallengeValuePlayer(lPhaseData.ResolutionDuelOpponentOpposition.ToString(), eTeamMethods.GetOpposite(MatchEngine.Instance.Engine.PossessionMain.CurTeamPossession).ToString(), 60));

            MatchEngine.Instance.Engine.ActionMain.ForceScore = _forceChallenge.ConvertToJson();

            if (eTeam.TEAM_A == UserData.Instance.PVPTeam || (eTeam.TEAM_B == UserData.Instance.PVPTeam && !ActivateBOT))
            {
                if (null != MatchEngine.Instance.GetDuelPlayer(eTeam.TEAM_A))
                    StartCoroutine(_prepareAction(eExecutor.TEAM_A_MANAGER, 1.25f));
            }

            if (eTeam.TEAM_B == UserData.Instance.PVPTeam || (eTeam.TEAM_A == UserData.Instance.PVPTeam && !ActivateBOT))
            {
                if (null != MatchEngine.Instance.GetDuelPlayer(eTeam.TEAM_B))
                    StartCoroutine(_prepareAction(eExecutor.TEAM_B_MANAGER, 1.25f));
            }
        }

        public  void    OnPhaseChanged(ePhase pPhase)
        {
            switch (pPhase)
            {
                case ePhase.TOSS_RESOLUTION:
                    if (TestActionOnly)
                    {
                        MatchEngine.Instance.Engine.TossMain.TeamWinning    =   _simulationData.TossWinner;
                        MatchEngine.Instance.Engine.PossessionMain.ChangeTeamPossession(_simulationData.TossWinner);
                    }
                    return;

                default:
                    return;
            }
        }

        #endregion

        #endregion

        #region Private Methods

        private void    Start()
        {
            if (null != UserData.Instance)
                UserData.Instance.PVPTeam       =   _simulationData.TeamManager;

            _pvpData.TeamId =   1392;

            MatchEngine.Instance.InitEngine();

            // (Force) sets manager & team IDs to start match
            SFManager lManagerTeamA = new SFManager();
            //lManagerTeamA.Id        =   "1579";
            //lManagerTeamA.TeamId    =   "1392";
            lManagerTeamA.Id        =   "1619";
            lManagerTeamA.TeamId    =   "1426";
            lManagerTeamA.Team      =   eTeam.TEAM_A.ToString();

            SFManager   lManagerTeamB   =   new SFManager();
            lManagerTeamB.Id        = "1619";
            lManagerTeamB.TeamId    = "1426";
            lManagerTeamB.Team      =   eTeam.TEAM_B.ToString();

            MatchEngine.Instance.Engine.ManagerMain.SetManagerA(lManagerTeamA);
            MatchEngine.Instance.Engine.ManagerMain.SetManagerB(lManagerTeamB);

            ePhase lNextPhase = MatchEngine.Instance.Engine.PhaseMain.GetNextPhase();
            MatchEngine.Instance.Engine.PhaseMain.ChangePhase(lNextPhase); // FIRST_SYNCHRONIZE

            lNextPhase = MatchEngine.Instance.Engine.PhaseMain.GetNextPhase();
            MatchEngine.Instance.Engine.PhaseMain.ChangePhase(lNextPhase); // LOAD_GAMEPLAY_SCENE

            MatchEngine.Instance.Engine.PhaseMain.OnPhaseChanged += pPhase => OnPhaseChanged(pPhase);
            MatchEngine.Instance.Engine.PhaseMain.OnPhaseChanged += pPhase => PlayersManager.Instance.OnPhaseChanged(pPhase);

            PlayersManager.Instance.InitPlayers();
            SoccerFieldUI.Instance.OnEngineInitialized();
            ActionsManager.Instance.Start();
            ActionsManager.Instance.IsTester    =   !ActivateBOT;

            if (TestActionOnly)
            {
                MatchEngine.Instance.IsTester   =   true;

                if ("FS_A_NEW_INIT" == _simulationData.InitFieldSetup)
                    MatchEngine.Instance.Engine.FieldSetupMain.ChangeFieldSetup(_simulationData.InitFieldSetup, ePosition.MCD, _simulationData.TossWinner);
                else
                    MatchEngine.Instance.Engine.FieldSetupMain.ChangeFieldSetup(_simulationData.InitFieldSetup, ePosition.MCD, eTeam.TEAM_A);

                MatchEngine.Instance.Engine.PossessionMain.ChangeTeamPossession(_simulationData.TossWinner);
                MatchEngine.Instance.Engine.PossessionMain.ChangePlayerPossession(_simulationData.InitPossessionPos);

                MatchEngine.Instance.Engine.TossMain.TeamWinning = _simulationData.TossWinner;
            }

            PlayersManager.Instance.PlacePlayers(true);

            _simulationData.CounterActionPhase      =   -1;
        }

        #region Coroutines

        private IEnumerator _prepareTacticalAction(eExecutor pExecutor, float pWaitingTime, TeamTacticalChoice pChoice)
        {
            yield return new WaitForSeconds(pWaitingTime);

            PlayerPVP   lPlayer =   PlayersManager.Instance.GetPlayer(eTeamMethods.GetTeamFromExecutor(pExecutor), pChoice.Position);

            if (eAction.NONE != pChoice.Action)
            {
                if (UserData.Instance.PVPTeam == eTeamMethods.GetTeamFromExecutor(pExecutor))
                {
                    lPlayer.Player3D.OnMouseDown();

                    yield return new WaitForSeconds(0.2f);

                    lPlayer.Player3D.OnMouseUp();

                    yield return new WaitForSeconds(0.5f);

                    string  lFCGob  =   "move_" + pChoice.Direction.ToString().ToLower() + "_" + pChoice.Distance;
                    if (null != GameObject.Find(lFCGob))
                    {
                        GameObject.Find(lFCGob).GetComponent<FieldCaseButton>().OnMouseDown();

                        yield return new WaitForSeconds(0.2f);

                        GameObject.Find(lFCGob).GetComponent<FieldCaseButton>().OnMouseUp();
                    }
                }
                else
                {
                    MatchEngine.Instance.SelectPlayer(lPlayer.Data);

                    MatchEngine.Instance.PrepareAction(
                        pExecutor,
                        pChoice.Action,
                        lPlayer.Data.Position,
                        _getTargetCase(lPlayer.Data.CurFieldCase, pChoice.Direction, pChoice.Distance)
                    );
                }
            }
        }

        private IEnumerator _prepareAction(eExecutor pExecutor, float pWaitingTime)
        {
            yield return new WaitForSeconds(pWaitingTime);

            ActionPhase         lCurPhaseData       =   _simulationData.ActionPhases[_simulationData.CounterActionPhase];
            TeamActionChoice    lActionTeamChoice   =   (eTeam.TEAM_A == eTeamMethods.GetTeamFromExecutor(pExecutor)) ? lCurPhaseData.TeamAChoice : lCurPhaseData.TeamBChoice;

            if (UserData.Instance.PVPTeam == eTeamMethods.GetTeamFromExecutor(pExecutor) && eAction.NONE != lActionTeamChoice.Action)
            {
				for (int li = 0; li < ActionsManager.Instance.ActionPrefabs.Count; li++)
                {
					if (null != ActionsManager.Instance.ActionPrefabs[li] && ActionsManager.Instance.ActionPrefabs[li].name == "Action_" + lActionTeamChoice.Action + "_Btn")
					{
                        // Trigger click on action button
                        ActionsManager.Instance.ActionPrefabs[li].OnPointerDown(null);

						if (eAction.PASS == lActionTeamChoice.Action)
						{
						    yield return new WaitForSeconds(1f);

                            // Trigger click on chosen receiver
                            ePosition   lReceiverPos    =   lActionTeamChoice.TargetPosition;
                            PlayerPVP   lReceiver       =   PlayersManager.Instance.GetPlayer(eTeamMethods.GetTeamFromExecutor(pExecutor), lReceiverPos);

							if (null != lReceiver)
							{
								lReceiver.Player3D.OnMouseDown();

                                yield return new WaitForSeconds(0.4f);

								lReceiver.Player3D.OnMouseUp();
							}
						}
                        else if (eAction.SPRINT == lActionTeamChoice.Action && null != ActionsManager.Instance.DuelPlayer)
                        {
						    yield return new WaitForSeconds(pWaitingTime);

                            MatchEngine.Instance.PrepareAction(
                                pExecutor,
                                lActionTeamChoice.Action,
                                ActionsManager.Instance.DuelPlayer.Data.Position,
                                _getTargetCase(ActionsManager.Instance.DuelPlayer.Data.CurFieldCase, lActionTeamChoice.Direction, lActionTeamChoice.Distance)
                            );
                        }
                    }
				}
            }
            else if (eTeamMethods.GetOpposite(UserData.Instance.PVPTeam) == eTeamMethods.GetTeamFromExecutor(pExecutor) && eAction.NONE != lActionTeamChoice.Action)
            {
                if (eAction.SPRINT == lActionTeamChoice.Action && null != ActionsManager.Instance.DuelOppPlayer)
                {
                    yield return new WaitForSeconds(pWaitingTime);

                    MatchEngine.Instance.SelectPlayer(ActionsManager.Instance.DuelOppPlayer.Data);

                    MatchEngine.Instance.PrepareAction(
                        pExecutor,
                        lActionTeamChoice.Action,
                        ActionsManager.Instance.DuelOppPlayer.Data.Position,
                        _getTargetCase(ActionsManager.Instance.DuelOppPlayer.Data.CurFieldCase, lActionTeamChoice.Direction, lActionTeamChoice.Distance)
                    );
                }
                else if (eAction.SPRINT != lActionTeamChoice.Action)
                {
                    Player      lActionPlayer   =   MatchEngine.Instance.GetDuelPlayer(eTeamMethods.GetTeamFromExecutor(pExecutor));
                    FieldCase   lFC             =   (eTeam.TEAM_A == eTeamMethods.GetTeamFromExecutor(pExecutor)) ? new FieldCase(53, 16) : new FieldCase(0, 16);
                    Player      lTarget         =   MatchEngine.Instance.Engine.TeamMain.GetPlayer(lActionPlayer.Team, lActionTeamChoice.TargetPosition);

                    if (eAction.PASS == lActionTeamChoice.Action)
                    {
                        MatchEngine.Instance.Engine.ActionMain.SimulateAction(lActionPlayer, lActionTeamChoice.Action);
                    }

                    MatchEngine.Instance.PrepareAction(pExecutor, lActionTeamChoice.Action, lActionPlayer.Position, eAction.LONG_KICK == lActionTeamChoice.Action ? lFC : lTarget?.CurFieldCase);
                }
            }
        }

        private FieldCase   _getTargetCase(FieldCase pCurFC, eMove pDirection, int pDistance)
        {
            switch(pDirection)
            {
                case eMove.RIGHT:
                    return MatchEngine.Instance.Engine.FieldMain.GetRightFieldCase(pCurFC, pDistance);
                case eMove.DOWN_RIGHT:
                    return MatchEngine.Instance.Engine.FieldMain.GetDownRightFieldCase(pCurFC, pDistance);
                case eMove.DOWN:
                    return MatchEngine.Instance.Engine.FieldMain.GetDownFieldCase(pCurFC, pDistance);
                case eMove.DOWN_LEFT:
                    return MatchEngine.Instance.Engine.FieldMain.GetDownLeftFieldCase(pCurFC, pDistance);
                case eMove.LEFT:
                    return MatchEngine.Instance.Engine.FieldMain.GetLeftFieldCase(pCurFC, pDistance);
                case eMove.TOP:
                    return MatchEngine.Instance.Engine.FieldMain.GetTopFieldCase(pCurFC, pDistance);
                case eMove.TOP_LEFT:
                    return MatchEngine.Instance.Engine.FieldMain.GetTopLeftFieldCase(pCurFC, pDistance);
                case eMove.TOP_RIGHT:
                    return MatchEngine.Instance.Engine.FieldMain.GetTopRightFieldCase(pCurFC, pDistance);
                default:
                    return pCurFC;
            }
        }

        #endregion

        #endregion
    }
}
﻿using Sportfaction.CasualFantasy.Action;
using Sportfaction.CasualFantasy.Events;
using Sportfaction.CasualFantasy.Manager.Profile;
using Sportfaction.CasualFantasy.PVP.Gameplay.Actions;
using Sportfaction.CasualFantasy.PVP.Gameplay.Field;
using Sportfaction.CasualFantasy.PVP.Gameplay.Matchs;
using Sportfaction.CasualFantasy.PVP.Gameplay.Players;
using Sportfaction.CasualFantasy.PVP.Gameplay.UI;
using Sportfaction.CasualFantasy.References.CustomClass;
using Sportfaction.CasualFantasy.Utilities;

using SportFaction.CasualFootballEngine.ActionService.Enum;
using SportFaction.CasualFootballEngine.ActionService.Response;
using SportFaction.CasualFootballEngine.FieldService.Model;
using SportFaction.CasualFootballEngine.PhaseService.Enum;
using SportFaction.CasualFootballEngine.PlayerService.Enum;
using SportFaction.CasualFootballEngine.PlayerService.Model;
using SportFaction.CasualFootballEngine.TeamService.Enum;

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using SFManager = SportFaction.CasualFootballEngine.ManagerService.Model.Manager;

namespace Sportfaction.CasualFantasy.PVP.Test
{
    public struct TutorialIndication
    {
        public string Message;
        public string BtnText;

        public TutorialIndication(string pMsg, string pBtn = "")
        {
            Message = pMsg;
            BtnText = pBtn;
        }
    }

    /// <summary>
    /// Simulates an entire match tutorial scenario
    /// </summary>
    /// @author Madeleine Tranier
    public sealed class MatchTutorial : Singleton<MatchTutorial>
    {
        #region Properties

        public MatchTutorialData TutorialData { get { return _tutorialData; } }

        #endregion

        #region Public Fields

        [ReadOnly]
        public bool PauseGame = false;
        [ReadOnly]
        public ePosition StartPosition = ePosition.NONE;
        [ReadOnly]
        public int CurTacticalChoice = 0;
        [ReadOnly]
        public int CurTutorialStep = 0;

        #endregion

        #region Serialized Fields

        [Header("Simulation settings")]
        [SerializeField, Tooltip("Test action phase only ?")]
        public bool TestActionOnly = false;
        [SerializeField, Tooltip("Bypass prematch only ?")]
        public bool ActivateBOT = false;

        [Header("References")]
        [SerializeField, Tooltip("Ref to player selected")]
        private PlayerPVPReference _playerFocusedRef = null; //!< Ref to currently selected player's main script

        [Header("Game Events")]
        [SerializeField, Tooltip("Event to invoke when current tutorial step is done")]
        private GameEvent _tutorialNextStepEvt = null;

        [Header("Data")]
        [SerializeField, Tooltip("Script with simulation data")]
        private MatchTutorialData _tutorialData = null; //!< Class storing entire match scenario
        [SerializeField, Tooltip("Scriptable object contains infos about match UI")]
        private PVPData _pvpData = null; //!< Class storing every PVP related data

        #endregion

        #region Private Fields

        private ForceChallengeValue _forceChallenge = null;
        private List<TutorialIndication> _indications = new List<TutorialIndication>
        {
            new TutorialIndication("L'adversaire a la possession du ballon. Attends qu'il ait fait un choix.", "OK"),
            new TutorialIndication("Tu peux déplacer un de tes joueurs. Pour cela, il te suffit de taper sur le joueur que tu veux déplacer. Tu peux déplacer un joueur pour qu'il tente d'intercepter la passe."),
            new TutorialIndication("Tu peux déplacer un autre joueur pour le snapper avec le receveur de la passe."),
            new TutorialIndication("Si tu as finis tous tes déplacements avant le temps imparti, tu peux finir ton tour en tapant sur le bouton \"Finir le tour\""),
            new TutorialIndication("Tu as récupéré le ballon, tu peux tenter une passe vers un coéquipier. Pour cela, clique sur le bouton \"Passe\"")
        };
        private bool _tossForced = false;

        #endregion

        #region Public Methods

        #region Game Event Listeners

        public void OnPerformTactics()
        {
            Debug.Log("Tutorial step = " + CurTutorialStep);
            if (_tutorialData.CounterActionPhase > _tutorialData.ActionPhases.Length - 1)
                return;

            ActionTutorialPhase lPhaseData = _tutorialData.ActionPhases[_tutorialData.CounterActionPhase];
            _forceChallengeValues(lPhaseData);

            if (5 != CurTutorialStep)
                PauseGame = lPhaseData.PauseGame;
            else
                ActionsUI.Instance.ToggleSkipBtn(true);

            if (1 == CurTutorialStep)
                MatchMessagesUI.Instance.DisplayPopin(_indications[CurTutorialStep]);

            // Launch tactical move preparation
            if (eTeam.TEAM_A == UserData.Instance.PVPTeam || (eTeam.TEAM_B == UserData.Instance.PVPTeam && !ActivateBOT))
            {
                if (lPhaseData.TacticalChoices.TeamAChoices.Length > 0)
                    AllowTacticalChoice(eExecutor.TEAM_A_MANAGER);

                for (int li = 0; li < lPhaseData.TacticalChoices.TeamBChoices.Length; li++)
                    StartCoroutine(_prepareTacticalAction(eExecutor.TEAM_B_AI, 4f * li + 1f, lPhaseData.TacticalChoices.TeamBChoices[li]));
            }
            else if (eTeam.TEAM_B == UserData.Instance.PVPTeam || (eTeam.TEAM_A == UserData.Instance.PVPTeam && !ActivateBOT))
            {
                if (lPhaseData.TacticalChoices.TeamBChoices.Length > 0)
                    AllowTacticalChoice(eExecutor.TEAM_A_MANAGER);

                for (int li = 0; li < lPhaseData.TacticalChoices.TeamBChoices.Length; li++)
                    StartCoroutine(_prepareTacticalAction(eExecutor.TEAM_B_AI, 4f * li + 1f, lPhaseData.TacticalChoices.TeamAChoices[li]));
            }
        }

        public void OnPrepareAction()
        {
            Debug.Log("Tutorial step = " + CurTutorialStep);
            _tutorialData.CounterActionPhase++;
            PauseGame = false;
            CurTacticalChoice = 0;

            if (_tutorialData.CounterActionPhase > _tutorialData.ActionPhases.Length - 1)
            {
                ActionsManager.Instance.IsTester = false;
                return;
            }

            //_tutorialData.DebugPhase("Action");

            ActionTutorialPhase lPhaseData = _tutorialData.ActionPhases[_tutorialData.CounterActionPhase];
            _forceChallengeValues(lPhaseData);

            PauseGame = lPhaseData.PauseGame;

            if (0 == CurTutorialStep)
                MatchMessagesUI.Instance.DisplayPopin(_indications[CurTutorialStep]);
            if (4 == CurTutorialStep)
                MatchMessagesUI.Instance.DisplayPopin(_indications[CurTutorialStep]);

            // Launch tactical move preparation
            if (eTeam.TEAM_A == UserData.Instance.PVPTeam || (eTeam.TEAM_B == UserData.Instance.PVPTeam && !ActivateBOT))
            {
                if (eAction.NONE != lPhaseData.TeamAChoice.Action)
                    StartCoroutine(_allowActionChoice(eExecutor.TEAM_A_MANAGER, 1.5f));

                if (CurTutorialStep >= 1)
                    if (null != MatchEngine.Instance.GetDuelPlayer(eTeam.TEAM_B))
                        StartCoroutine(_prepareAction(eExecutor.TEAM_B_AI, 1.75f));
            }
            else if (eTeam.TEAM_B == UserData.Instance.PVPTeam || (eTeam.TEAM_A == UserData.Instance.PVPTeam && !ActivateBOT))
            {
                if (eAction.NONE != lPhaseData.TeamBChoice.Action)
                    StartCoroutine(_allowActionChoice(eExecutor.TEAM_B_MANAGER, 1.5f));

                if (null != MatchEngine.Instance.GetDuelPlayer(eTeam.TEAM_A))
                    StartCoroutine(_prepareAction(eExecutor.TEAM_A_AI, 1.75f));
            }
        }

        /// <summary>
        /// MatchEngine Event Listener > Phase Changed
        /// _engine.PhaseMain.OnPhaseChanged += pPhase => this.OnPhaseChanged(pPhase);
        /// </summary>
        public void OnPhaseChanged(ePhase pPhase)
        {
            switch (pPhase)
            {
                case ePhase.TOSS_RESOLUTION:
                    StartPosition = _tutorialData.InitPossessionPos;
                    MatchEngine.Instance.Engine.TossMain.TeamWinning = _tutorialData.TossWinner;
                    MatchEngine.Instance.Engine.PossessionMain.ChangeTeamPossession(_tutorialData.TossWinner);
                    return;

                default:
                    return;
            }
        }

        /// <summary>
        /// Event Listener > OnEngineTeamAPreparedAction | OnEngineTeamBPreparedAction
        /// </summary>
        public void OnEngineTeamPreparedAction()
        {
            if (_tutorialData.CounterActionPhase > _tutorialData.ActionPhases.Length - 1)
                return;

            Debug.Log("Tutorial step = " + CurTutorialStep);
            ResponseAction lResponse = MatchEngine.Instance.GetResponse();
            eTeam lPlayerTeam = eTeamMethods.ConvertStringToETeam(lResponse.Team);

            if (lPlayerTeam == UserData.Instance.PVPTeam)
            {
                ActionTutorialPhase lPhaseData = _tutorialData.ActionPhases[_tutorialData.CounterActionPhase];

                if (ePhase.TACTICAL_ATTACK == MatchEngine.Instance.CurPhase)
                {
                    CurTacticalChoice++;

                    if (2 == CurTutorialStep)
                        MatchMessagesUI.Instance.DisplayPopin(_indications[CurTutorialStep]);

                    if (lPhaseData.TacticalChoices.TeamAChoices.Length > 0 && CurTacticalChoice < lPhaseData.TacticalChoices.TeamAChoices.Length)
                        AllowTacticalChoice(eExecutorMethods.GetEExecutor(UserData.Instance.PVPTeam, false));
                    else
                    {
                        PlayerPVP[] lTeamPlayers = PlayersManager.Instance.GetTeamPlayers(UserData.Instance.PVPTeam);
                        for (int li = 0; li < lTeamPlayers.Length; li++)
                        {
                            lTeamPlayers[li].Player3D.TogglePlayerCollider(false);
                            lTeamPlayers[li].Player3D.TogglePlayerActive(false);
                            lTeamPlayers[li].UI.ToggleSelection(false);
                        }
                    }
                }

                if (3 == CurTutorialStep)
                {
                    MatchMessagesUI.Instance.DisplayPopin(_indications[CurTutorialStep]);
                    MatchTimers.Instance.StartCurrentTimer();
                    ActionsUI.Instance.ToggleSkipBtn(true);
                }
            }
        }

        public void OnSkipPhase()
        {
            if (3 == CurTutorialStep)
                _tutorialNextStepEvt.Invoke();

            ActionsUI.Instance.ToggleActionIndications(false);
            MatchTimers.Instance.StopCurrentTimer();

            ActionsManager.Instance.DuelPlayer?.UpdateHasAction(false);
            ActionsManager.Instance.DuelOppPlayer?.UpdateHasAction(false);

            PlayerPVP[] lTeamPlayers = PlayersManager.Instance.GetTeamPlayers(UserData.Instance.PVPTeam);
            for (int li = 0; li < lTeamPlayers.Length; li++)
                lTeamPlayers[li].Player3D.TogglePlayerActive(true);
        }

        public void OnTacticalTimerDone()
        {
            if (3 == CurTutorialStep)
            {
                CurTutorialStep = 4;
                MatchMessagesUI.Instance.OnTutorialNextStep();
            }
        }

        public void OnActionInProgress()
        {
            Debug.Log("OnActionInProgress");
            if (null == ActionsManager.Instance.ActionInProcess || _tutorialData.CounterActionPhase > _tutorialData.ActionPhases.Length - 1)
                return;

            ActionTutorialPhase lCurPhaseData = _tutorialData.ActionPhases[_tutorialData.CounterActionPhase];
            TeamActionChoice lActionTeamChoice = (eTeam.TEAM_A == UserData.Instance.PVPTeam) ? lCurPhaseData.TeamAChoice : lCurPhaseData.TeamBChoice;

            switch (ActionsManager.Instance.ActionInProcess.Type)
            {
                case eAction.PASS:
                    PlayersManager.Instance.TogglePlayerCollider(eTeamMethods.GetOpposite(UserData.Instance.PVPTeam), false);
                    PlayerPVP lReceiver = PlayersManager.Instance.GetPlayer(UserData.Instance.PVPTeam, lActionTeamChoice.TargetPosition);
                    lReceiver.IsClickable = true;
                    lReceiver.Player3D.TogglePlayerCollider(true);
                    break;
            }
        }

        public void OnTutorialNextStep()
        {
            if (0 == CurTutorialStep)
            {
                MatchTimers.Instance.StartCurrentTimer();
                StartCoroutine(_prepareAction(eExecutor.TEAM_B_AI, 1.75f));
            }
            Debug.Log("Last tutorial step = " + CurTutorialStep);
            CurTutorialStep++;
        }

        #endregion

        public void MovePreview()
        {
            _tutorialNextStepEvt.Invoke();
            //ActionsManager.Instance.ClearMovePreview();

            if (null == _playerFocusedRef && null == ActionsManager.Instance.DuelPlayer)
                return;

            PlayerPVP lPlayerFocused = _playerFocusedRef.CurrentValue;
            GameObject.Find("ActionCanceler")?.SetActive(false);
            PlayersManager.Instance.TogglePlayerCollider(false);

            //UtilityMethods.DebugSimulation(eAction.MOVE, lPlayerFocused);
            MoveSimulationResponse lMoveSimulation = ActionsManager.Instance.MoveSimulation;
            int lNbCases = lMoveSimulation.AvailableFieldCases.Count;
            FieldCaseButton lCurBtn = null;

            PlayersManager.Instance.TogglePlayerSelection(UserData.Instance.PVPTeam, false);
            PlayersManager.Instance.TogglePlayerForbiddenSelection(UserData.Instance.PVPTeam, false);

            PlayerPVP[] lTeamPlayers = PlayersManager.Instance.GetTeamPlayers(UserData.Instance.PVPTeam);
            int lNbPlayers = lTeamPlayers.Length;
            TeamTacticalChoice lChoice = _tutorialData.ActionPhases[_tutorialData.CounterActionPhase].TacticalChoices.TeamAChoices[CurTacticalChoice];
            if (eTeam.TEAM_B == UserData.Instance.PVPTeam)
            {
                lChoice = _tutorialData.ActionPhases[_tutorialData.CounterActionPhase].TacticalChoices.TeamBChoices[CurTacticalChoice];
            }
            FieldCase lFCChoice = _getMoveTargetCase(lPlayerFocused.Data.CurFieldCase, lChoice.Direction, lChoice.Distance);

            for (int li = 0; li < lNbPlayers; li++)
            {
                lTeamPlayers[li].Player3D.TogglePlayerActive(lTeamPlayers[li].Data.IsEqualTo(lPlayerFocused.Data));
            }

            // Display available field cases with risks
            for (int li = 0; li < lNbCases; li++)
            {
                lCurBtn = ActionsManager.Instance.FieldCaseOutlines[li];

                lCurBtn.Init(
                    lMoveSimulation.AvailableFieldCases[li],
                    lMoveSimulation.AvailableFieldCases[li].HasPlayer && lMoveSimulation.AvailableFieldCases[li].HasPlayerTeam != lPlayerFocused.Data.Team.ToString(),
                    PlayersManager.Instance.CalculatePositionByCoordinates(lMoveSimulation.AvailableFieldCases[li].FieldCase),
                    lPlayerFocused.Data.CurFieldCase,
                    lFCChoice.IsEqualTo(lMoveSimulation.AvailableFieldCases[li].FieldCase)
                );
            }
        }

        public void PassPreview()
        {
            _tutorialNextStepEvt.Invoke();
            ActionsManager.Instance.ClearPassTrajectories();
            MatchCamera.Instance.DisplayResolutionView();

            GameObject.Find("ActionCanceler")?.SetActive(false);
            PlayersManager.Instance.TogglePlayerCollider(false);

            SimulatePassReceiver[] lReceivers = ActionsManager.Instance.PassSimulation.Receivers.ToArray();
            PlayerPVP lPasser = PlayersManager.Instance.GetPlayer(ActionsManager.Instance.CurTeamPossess, MatchEngine.Instance.Engine.PossessionMain.CurPlayerPossession);
            PlayerPVP lReceiver;
            TeamActionChoice lChoice = _tutorialData.ActionPhases[_tutorialData.CounterActionPhase].TeamAChoice;
            PlayerPVP lSelectedPlayer = PlayersManager.Instance.GetPlayer(UserData.Instance.PVPTeam, lChoice.TargetPosition);

            for (int li = 0; li < lReceivers.Length; li++)
            {
                // Display overall risk on receiver
                lReceiver = PlayersManager.Instance.GetPlayer(lPasser.Data.Team, ePositionMethods.ConvertStringToEPosition(lReceivers[li].Position));
                lReceiver.UI.ToggleTarget(true);
                lReceiver.IsClickable = lReceiver.Data.IsEqualTo(lSelectedPlayer.Data);
                lReceiver.Player3D.TogglePlayerActive(lReceiver.Data.IsEqualTo(lSelectedPlayer.Data));

                // Draw trajectory line
                string lTrajectoryName = "PASS_" + lPasser.name + "_" + lReceiver.Data.Position;
                ActionsManager.Instance.DrawTrajectory(li, lTrajectoryName, lPasser.transform, lReceiver.transform, eColorDifficultyMethods.ConvertStringToEnum(lReceivers[li].ColorArrow), true);

                if (lReceivers[li].IsOffside)
                    ActionsManager.Instance.DrawOffside(lReceiver.Data.CurFieldCase.X - 1);
            }
        }

        #endregion

        #region Private Methods

        private void Start()
        {
            if (null != UserData.Instance)
            {
                UserData.Instance.PVPTeam = _tutorialData.TeamManager;
            }

            _pvpData.TeamId = 1392;

            bool lIsSimulation = null == MatchEngine.Instance.Engine;

            if (lIsSimulation)
            {
                MatchEngine.Instance.InitEngine();

                SFManager lManagerTeamA = new SFManager();
                lManagerTeamA.Id = "1619";
                lManagerTeamA.TeamId = "1426";
                lManagerTeamA.Team = eTeam.TEAM_A.ToString();

                SFManager lManagerTeamB = new SFManager();
                lManagerTeamB.Id = "1579";
                lManagerTeamB.TeamId = "1392";
                lManagerTeamB.Team = eTeam.TEAM_B.ToString();

                MatchEngine.Instance.Engine.ManagerMain.SetManagerA(lManagerTeamA);
                MatchEngine.Instance.Engine.ManagerMain.SetManagerB(lManagerTeamB);

                ePhase lNextPhase = MatchEngine.Instance.Engine.PhaseMain.GetNextPhase();
                MatchEngine.Instance.Engine.PhaseMain.ChangePhase(lNextPhase); // FIRST_SYNCHRONIZE

                lNextPhase = MatchEngine.Instance.Engine.PhaseMain.GetNextPhase();
                MatchEngine.Instance.Engine.PhaseMain.ChangePhase(lNextPhase); // LOAD_GAMEPLAY_SCENE

                //PlayersManager.Instance.InitPlayers();
                //SoccerFieldUI.Instance.OnEngineInitialized();
                //ActionsManager.Instance.Start();
                //ActionsManager.Instance.IsTester = !ActivateBOT;
            }

            PlayersManager.Instance.InitPlayers();
            SoccerFieldUI.Instance.OnEngineInitialized();
            ActionsManager.Instance.Start();
            ActionsManager.Instance.IsTester = !ActivateBOT;

            MatchEngine.Instance.Engine.PhaseMain.OnPhaseChanged += pPhase => OnPhaseChanged(pPhase);
            MatchEngine.Instance.Engine.PhaseMain.OnPhaseChanged += pPhase => PlayersManager.Instance.OnPhaseChanged(pPhase);

            if (TestActionOnly)
            {
                MatchEngine.Instance.IsTester = true;

                MatchEngine.Instance.Engine.FieldSetupMain.ChangeFieldSetup(_tutorialData.InitFieldSetup, ePosition.MCD, eTeam.TEAM_A);
                MatchEngine.Instance.Engine.PossessionMain.ChangeTeamPossession(_tutorialData.TossWinner);
                MatchEngine.Instance.Engine.PossessionMain.ChangePlayerPossession(_tutorialData.InitPossessionPos);

                MatchEngine.Instance.Engine.TossMain.TeamWinning = _tutorialData.TossWinner;
            }

            if (lIsSimulation)
                PlayersManager.Instance.PlacePlayers(true);

            _tutorialData.CounterActionPhase = -1;
        }

        private void AllowTacticalChoice(eExecutor pExecutor)
        {
            ActionTutorialPhase lCurPhaseData = _tutorialData.ActionPhases[_tutorialData.CounterActionPhase];
            TeamTacticalChoice[] lTacticalTeamChoices = (eTeam.TEAM_A == eTeamMethods.GetTeamFromExecutor(pExecutor)) ? lCurPhaseData.TacticalChoices.TeamAChoices : lCurPhaseData.TacticalChoices.TeamBChoices;
            PlayerPVP lPlayer = PlayersManager.Instance.GetPlayer(eTeamMethods.GetTeamFromExecutor(pExecutor), lTacticalTeamChoices[CurTacticalChoice].Position);

            PlayerPVP[] lTeamPlayers = PlayersManager.Instance.GetTeamPlayers(UserData.Instance.PVPTeam);
            for (int li = 0; li < lTeamPlayers.Length; li++)
            {
                lTeamPlayers[li].Player3D.TogglePlayerCollider(false);
                lTeamPlayers[li].Player3D.TogglePlayerActive(false);
                lTeamPlayers[li].UI.ToggleSelection(false);
            }

            lPlayer.IsClickable = true;
            lPlayer.Player3D.TogglePlayerCollider(true);
            lPlayer.Player3D.TogglePlayerActive(true);
            lPlayer.UI.ToggleSelection(true);
        }

        #region Coroutines

        private IEnumerator _allowActionChoice(eExecutor pExecutor, float pWaitingTime)
        {
            yield return new WaitForSeconds(pWaitingTime);

            ActionTutorialPhase lCurPhaseData = _tutorialData.ActionPhases[_tutorialData.CounterActionPhase];
            TeamActionChoice lActionTeamChoice = (eTeam.TEAM_A == eTeamMethods.GetTeamFromExecutor(pExecutor)) ? lCurPhaseData.TeamAChoice : lCurPhaseData.TeamBChoice;

            PlayerPVP[] lTeamPlayers = PlayersManager.Instance.GetTeamPlayers(UserData.Instance.PVPTeam);
            for (int li = 0; li < lTeamPlayers.Length; li++)
            {
                lTeamPlayers[li].Player3D.TogglePlayerCollider(false);
                lTeamPlayers[li].Player3D.TogglePlayerActive(false);
                lTeamPlayers[li].UI.ToggleSelection(false);
            }

            for (int li = 0; li < ActionsManager.Instance.ActionPrefabs.Count; li++)
            {
                if (null != ActionsManager.Instance.ActionPrefabs[li])
                {
                    ActionsManager.Instance.ActionPrefabs[li].ToggleClickable(ActionsManager.Instance.ActionPrefabs[li].name == "Action_" + lActionTeamChoice.Action + "_Btn");
                }
            }
        }

        private IEnumerator _prepareTacticalAction(eExecutor pExecutor, float pWaitingTime, TeamTacticalChoice pChoice)
        {
            yield return new WaitForSeconds(pWaitingTime);

            PlayerPVP lPlayer = PlayersManager.Instance.GetPlayer(eTeamMethods.GetTeamFromExecutor(pExecutor), pChoice.Position);

            if (eAction.NONE != pChoice.Action)
            {
                if (UserData.Instance.PVPTeam == eTeamMethods.GetTeamFromExecutor(pExecutor))
                {
                    lPlayer.Player3D.OnMouseDown();

                    yield return new WaitForSeconds(0.5f);

                    lPlayer.Player3D.OnMouseUp();

                    yield return new WaitForSeconds(1f);

                    string lFCGob = "move_" + pChoice.Direction.ToString().ToLower() + "_" + pChoice.Distance;
                    if (null != GameObject.Find(lFCGob))
                    {
                        GameObject.Find(lFCGob).GetComponent<FieldCaseButton>().OnMouseDown();

                        yield return new WaitForSeconds(0.5f);

                        GameObject.Find(lFCGob).GetComponent<FieldCaseButton>().OnMouseUp();
                    }
                }
                else
                {
                    MatchEngine.Instance.SelectPlayer(lPlayer.Data);

                    MatchEngine.Instance.PrepareAction(
                        pExecutor,
                        pChoice.Action,
                        lPlayer.Data.Position,
                        _getMoveTargetCase(lPlayer.Data.CurFieldCase, pChoice.Direction, pChoice.Distance)
                    );
                }
            }
        }

        private IEnumerator _prepareAction(eExecutor pExecutor, float pWaitingTime)
        {
            yield return new WaitForSeconds(pWaitingTime);

            ActionTutorialPhase lCurPhaseData = _tutorialData.ActionPhases[_tutorialData.CounterActionPhase];
            TeamActionChoice lActionTeamChoice = (eTeam.TEAM_A == eTeamMethods.GetTeamFromExecutor(pExecutor)) ? lCurPhaseData.TeamAChoice : lCurPhaseData.TeamBChoice;

            if (UserData.Instance.PVPTeam == eTeamMethods.GetTeamFromExecutor(pExecutor) && eAction.NONE != lActionTeamChoice.Action)
            {
                for (int li = 0; li < ActionsManager.Instance.ActionPrefabs.Count; li++)
                {
                    if (null != ActionsManager.Instance.ActionPrefabs[li] && ActionsManager.Instance.ActionPrefabs[li].name == "Action_" + lActionTeamChoice.Action + "_Btn")
                    {
                        // Trigger click on action button
                        ActionsManager.Instance.ActionPrefabs[li].OnPointerDown(null);

                        if (eAction.PASS == lActionTeamChoice.Action)
                        {
                            yield return new WaitForSeconds(pWaitingTime);

                            // Trigger click on chosen receiver
                            ePosition lReceiverPos = lActionTeamChoice.TargetPosition;
                            PlayerPVP lReceiver = PlayersManager.Instance.GetPlayer(eTeamMethods.GetTeamFromExecutor(pExecutor), lReceiverPos);

                            if (null != lReceiver)
                            {
                                lReceiver.Player3D.OnMouseDown();

                                yield return new WaitForSeconds(0.5f);

                                lReceiver.Player3D.OnMouseUp();
                            }
                        }
                        else if (eAction.LONG_KICK == lActionTeamChoice.Action)
                        {
                            yield return new WaitForSeconds(pWaitingTime);

                            // Validate goal case for long kick
                            GameObject lGoalTarget = GameObject.Find("Long_Kick_Risk");

                            if (null != lGoalTarget)
                            {
                                lGoalTarget.GetComponent<InteractableGameObject>().OnMouseDown();

                                yield return new WaitForSeconds(0.5f);

                                lGoalTarget.GetComponent<InteractableGameObject>().OnMouseUp();
                            }
                        }
                        else if (eAction.SPRINT == lActionTeamChoice.Action && null != ActionsManager.Instance.DuelPlayer)
                        {
                            yield return new WaitForSeconds(pWaitingTime);

                            MatchEngine.Instance.PrepareAction(
                                pExecutor,
                                lActionTeamChoice.Action,
                                ActionsManager.Instance.DuelPlayer.Data.Position,
                                _getMoveTargetCase(ActionsManager.Instance.DuelPlayer.Data.CurFieldCase, lActionTeamChoice.Direction, lActionTeamChoice.Distance)
                            );
                        }
                    }
                }
            }
            else if (eTeamMethods.GetOpposite(UserData.Instance.PVPTeam) == eTeamMethods.GetTeamFromExecutor(pExecutor) && eAction.NONE != lActionTeamChoice.Action)
            {
                if (eAction.SPRINT == lActionTeamChoice.Action && null != ActionsManager.Instance.DuelOppPlayer)
                {
                    yield return new WaitForSeconds(pWaitingTime);

                    MatchEngine.Instance.SelectPlayer(ActionsManager.Instance.DuelOppPlayer.Data);

                    MatchEngine.Instance.PrepareAction(
                        pExecutor,
                        lActionTeamChoice.Action,
                        ActionsManager.Instance.DuelOppPlayer.Data.Position,
                        _getMoveTargetCase(ActionsManager.Instance.DuelOppPlayer.Data.CurFieldCase, lActionTeamChoice.Direction, lActionTeamChoice.Distance)
                    );
                }
                else if (eAction.SPRINT != lActionTeamChoice.Action)
                {
                    Player lActionPlayer = MatchEngine.Instance.GetDuelPlayer(eTeamMethods.GetTeamFromExecutor(pExecutor));
                    FieldCase lFC = (eTeam.TEAM_A == eTeamMethods.GetTeamFromExecutor(pExecutor)) ? new FieldCase(53, 16) : new FieldCase(0, 16);
                    Player lTarget = MatchEngine.Instance.Engine.TeamMain.GetPlayer(lActionPlayer.Team, lActionTeamChoice.TargetPosition);

                    if (eAction.PASS == lActionTeamChoice.Action)
                    {
                        MatchEngine.Instance.Engine.ActionMain.SimulateAction(lActionPlayer, lActionTeamChoice.Action);
                    }

                    MatchEngine.Instance.PrepareAction(pExecutor, lActionTeamChoice.Action, lActionPlayer.Position, eAction.LONG_KICK == lActionTeamChoice.Action ? lFC : lTarget?.CurFieldCase);
                }
            }
        }

        private FieldCase _getMoveTargetCase(FieldCase pCurFC, eMove pDirection, int pDistance)
        {
            switch (pDirection)
            {
                case eMove.RIGHT:
                    return MatchEngine.Instance.Engine.FieldMain.GetRightFieldCase(pCurFC, pDistance);
                case eMove.DOWN_RIGHT:
                    return MatchEngine.Instance.Engine.FieldMain.GetDownRightFieldCase(pCurFC, pDistance);
                case eMove.DOWN:
                    return MatchEngine.Instance.Engine.FieldMain.GetDownFieldCase(pCurFC, pDistance);
                case eMove.DOWN_LEFT:
                    return MatchEngine.Instance.Engine.FieldMain.GetDownLeftFieldCase(pCurFC, pDistance);
                case eMove.LEFT:
                    return MatchEngine.Instance.Engine.FieldMain.GetLeftFieldCase(pCurFC, pDistance);
                case eMove.TOP:
                    return MatchEngine.Instance.Engine.FieldMain.GetTopFieldCase(pCurFC, pDistance);
                case eMove.TOP_LEFT:
                    return MatchEngine.Instance.Engine.FieldMain.GetTopLeftFieldCase(pCurFC, pDistance);
                case eMove.TOP_RIGHT:
                    return MatchEngine.Instance.Engine.FieldMain.GetTopRightFieldCase(pCurFC, pDistance);
                default:
                    return pCurFC;
            }
        }

        private void _forceChallengeValues(ActionTutorialPhase pCurPhaseData)
        {
            _forceChallenge = new ForceChallengeValue();
            _forceChallenge.TeamAValue = 50;
            _forceChallenge.TeamBValue = 50;

            // Tweak main duel resolution results
            if (eTeam.TEAM_A == pCurPhaseData.DuelWinner && null != MatchEngine.Instance.GetDuelPlayer(eTeam.TEAM_A))
                _forceChallenge.Players.Add(new ForceChallengeValuePlayer(MatchEngine.Instance.GetDuelPlayer(eTeam.TEAM_A).Position.ToString(), eTeam.TEAM_A.ToString(), 70));
            if (eTeam.TEAM_B == pCurPhaseData.DuelWinner && null != MatchEngine.Instance.GetDuelPlayer(eTeam.TEAM_B))
                _forceChallenge.Players.Add(new ForceChallengeValuePlayer(MatchEngine.Instance.GetDuelPlayer(eTeam.TEAM_B).Position.ToString(), eTeam.TEAM_B.ToString(), 70));

            // Tweak tactical resolution results
            for (int li = 0; li < pCurPhaseData.TacticalChoices.TeamAChoices.Length; li++)
                if (pCurPhaseData.TacticalChoices.TeamAChoices[li].WinsCase)
                    _forceChallenge.Players.Add(new ForceChallengeValuePlayer(pCurPhaseData.TacticalChoices.TeamAChoices[li].Position.ToString(), eTeam.TEAM_A.ToString(), 60));
                else
                    _forceChallenge.Players.Add(new ForceChallengeValuePlayer(pCurPhaseData.TacticalChoices.TeamAChoices[li].Position.ToString(), eTeam.TEAM_A.ToString(), 40));

            for (int li = 0; li < pCurPhaseData.TacticalChoices.TeamBChoices.Length; li++)
                if (pCurPhaseData.TacticalChoices.TeamBChoices[li].WinsCase)
                    _forceChallenge.Players.Add(new ForceChallengeValuePlayer(pCurPhaseData.TacticalChoices.TeamBChoices[li].Position.ToString(), eTeam.TEAM_B.ToString(), 60));
                else
                    _forceChallenge.Players.Add(new ForceChallengeValuePlayer(pCurPhaseData.TacticalChoices.TeamBChoices[li].Position.ToString(), eTeam.TEAM_B.ToString(), 40));

            // Tweak action resolution results
            if (eTeam.NONE != pCurPhaseData.ResolutionWinningTeam && ePosition.NONE != pCurPhaseData.ResolutionWinningPosition)
                _forceChallenge.Players.Add(new ForceChallengeValuePlayer(pCurPhaseData.ResolutionWinningPosition.ToString(), pCurPhaseData.ResolutionWinningTeam.ToString(), 100));
            if (ePosition.NONE != pCurPhaseData.ResolutionDuelOpponentOpposition)
                _forceChallenge.Players.Add(new ForceChallengeValuePlayer(pCurPhaseData.ResolutionDuelOpponentOpposition.ToString(), eTeamMethods.GetOpposite(MatchEngine.Instance.Engine.PossessionMain.CurTeamPossession).ToString(), 60));

            MatchEngine.Instance.Engine.ActionMain.ForceScore = _forceChallenge.ConvertToJson();
        }

        #endregion

        #endregion
    }
}
﻿using SportFaction.CasualFootballEngine.PlayerService.Enum;
using SportFaction.CasualFootballEngine.TeamService.Enum;

using UnityEngine;

namespace Sportfaction.CasualFantasy.PVP.Test
{
    [System.Serializable]
    public class ActionTutorialPhase : ActionPhase
    {
        public  bool    PauseGame;
    }

    /// <summary>
    /// Class storing data of an entire match scenario
    /// </summary>
    [CreateAssetMenu(fileName = "Match Tutorial Data", menuName = "CasualFantasy/Data/MatchTutorial")]
    public sealed class MatchTutorialData : ScriptableObject
    {
        #region Fields

        [SerializeField, Tooltip("Manager's team")]
        public  eTeam       TeamManager         =   eTeam.TEAM_A;
        [SerializeField, Tooltip("Toss winner")]
        public  eTeam       TossWinner          =   eTeam.TEAM_A;

        [SerializeField, Tooltip("Field Setup Init")]
        public  string      InitFieldSetup      =   "NONE";
        [SerializeField, Tooltip("Position Possession Init")]
        public  ePosition   InitPossessionPos   =   ePosition.MCD;

        [SerializeField, Tooltip("Action phase choices")]
        public  ActionTutorialPhase[]   ActionPhases;

        [ReadOnly]
        public  int CounterActionPhase  =   -1; //!< Action phase counter

        #endregion

        #region Public Methods

        public  void    DebugPhase(string pPhase)
        {
            Debug.LogFormat("<color=#DDA0DD>ACTION PHASE N°" + CounterActionPhase + "</color>");
            Debug.LogFormat("<color=#DDA0DD>-------------------------</color>");
            ActionPhases[CounterActionPhase].DebugData();
        }

        #endregion
    }
}
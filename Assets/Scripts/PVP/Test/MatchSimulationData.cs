﻿using SportFaction.CasualFootballEngine.ActionService.Enum;
using SportFaction.CasualFootballEngine.PlayerService.Enum;
using SportFaction.CasualFootballEngine.TeamService.Enum;

using Newtonsoft.Json.Linq;
using UnityEngine;
using Sportfaction.CasualFantasy.PVP.Gameplay.Matchs;

namespace Sportfaction.CasualFantasy.PVP.Test
{
    #region Structures

    [System.Serializable]
    public class TacticalPhase
    {
        public TeamTacticalChoice[] TeamAChoices;
        public TeamTacticalChoice[] TeamBChoices;

        public void DebugData()
        {
            Debug.LogFormat("<color=#DDA0DD>/////////// Team A Choices ///////////</color>");
            //if (0 == TeamAChoices.Length)
            //    Debug.LogFormat("<color=#DDA0DD>Skips turn !</color>");
            for (int li = 0; li < TeamAChoices.Length; li++)
                TeamAChoices[li].DebugData();

            Debug.LogFormat("<color=#DDA0DD>/////////// Team B Choices ///////////</color>");
            //if (0 == TeamAChoices.Length)
            //    Debug.LogFormat("<color=#DDA0DD>Skips turn !</color>");
            for (int li = 0; li < TeamBChoices.Length; li++)
                TeamBChoices[li].DebugData();
        }
    }

    [System.Serializable]
    public class TeamTacticalChoice
    {
        [SerializeField]
        public  eAction     Action      =   eAction.MOVE;
        public  ePosition   Position;
        public  eMove       Direction;
        [SerializeField]
        public  int         Distance    =   1;
        public  bool        WinsCase;

        public void DebugData()
        {
            Debug.LogFormat("<color=#DDA0DD>Move " + Position + " to " + Direction + " (" + Distance + ") ====> " + WinsCase + "</color>");
        }
    }

    [System.Serializable]
    public class ActionPhase
    {
        public  TeamActionChoice        TeamAChoice;
        public  TeamActionChoice        TeamBChoice;
        public  eTeam                   DuelWinner;

        [Space(8)]
        public  TacticalPhase           TacticalChoices;

        [Space(8)]
        public  eTeam                   ResolutionWinningTeam;
        public  ePosition               ResolutionWinningPosition;
        public  ePosition               ResolutionDuelOpponentOpposition;

        public void DebugData()
        {
            Debug.LogFormat("<color=#DDA0DD>/////////// Team A Choices ///////////</color>");
            TeamAChoice.DebugData(eTeam.TEAM_A);
            for (int li = 0; li < TacticalChoices.TeamAChoices.Length; li++)
                TacticalChoices.TeamAChoices[li].DebugData();

            Debug.LogFormat("<color=#DDA0DD>/////////// Team B Choices ///////////</color>");
            TeamBChoice.DebugData(eTeam.TEAM_B);
            for (int li = 0; li < TacticalChoices.TeamBChoices.Length; li++)
                TacticalChoices.TeamBChoices[li].DebugData();

            if (eAction.NONE != TeamAChoice.Action && eAction.NONE != TeamBChoice.Action)
                Debug.LogFormat("<color=#DDA0DD>/////////// Duel winner ===> " + DuelWinner + " ///////////</color>");

            if (eTeam.NONE != ResolutionWinningTeam)
                Debug.LogFormat("<color=#DDA0DD>/////////// Resolution winner ===> " + ResolutionWinningTeam + " - " + ResolutionWinningPosition + " ///////////</color>");
        }
    }

    [System.Serializable]
    public class TeamActionChoice
    {
        public  eAction     Action;

        [Header("Pass action specific")]
        public  ePosition   TargetPosition;

        [Header("Sprint action specific")]
        public  eMove       Direction;
        public  int         Distance;
        public  bool        WinsCase;

        public  void    DebugData(eTeam pTeam)
        {
            if (eAction.NONE == Action)
                Debug.LogFormat("<color=#DDA0DD>No action to prepare !</color>");
            else if (eAction.SPRINT == Action)
                Debug.LogFormat("<color=#DDA0DD>Sprint to " + Direction + " ====> " + WinsCase + "</color>");
            /*else if (eAction.PASS == Action)
                Debug.LogFormat("<color=#DDA0DD>" + Action + " " + MatchEngine.Instance.GetDuelPlayer(pTeam).Position + " to " + TargetPosition + "</color>");*/
            /*else
                Debug.LogFormat("<color=#DDA0DD>" + Action + " " + MatchEngine.Instance.GetDuelPlayer(pTeam).Position + "</color>");*/
        }
    }

    [System.Serializable]
    public class CaseTarget
    {
        public int X;
        public int Y;
        
        public CaseTarget(JToken pToken)
        {
            X   =   pToken.Value<int>("X");
            Y   =   pToken.Value<int>("Y");
        }
    }
    
    #endregion
    
    /// <summary>
    /// Class storing data of an entire match scenario
    /// </summary>
    [CreateAssetMenu(fileName = "Match Simulation Data", menuName = "CasualFantasy/Data/MatchSimulation")]
    public sealed class MatchSimulationData : ScriptableObject
    {
        #region Fields

        [SerializeField, Tooltip("Manager's team")]
        public  eTeam       TeamManager         =   eTeam.TEAM_A;
        [SerializeField, Tooltip("Toss winner")]
        public  eTeam       TossWinner          =   eTeam.TEAM_A;

        [SerializeField, Tooltip("Field Setup Init")]
        public  string      InitFieldSetup      =   "NONE";
        [SerializeField, Tooltip("Position Possession Init")]
        public  ePosition   InitPossessionPos   =   ePosition.MCD;

        [SerializeField, Tooltip("Action phase choices")]
        public  ActionPhase[]   ActionPhases;

        [ReadOnly]
        public int CounterActionPhase      =   -1; //!< Action phase counter

        #endregion

        #region Public Methods

        public  void    DebugPhase(string pPhase)
        {
            Debug.LogFormat("<color=#DDA0DD>ACTION PHASE N°" + CounterActionPhase + "</color>");
            Debug.LogFormat("<color=#DDA0DD>-------------------------</color>");
            ActionPhases[CounterActionPhase].DebugData();
        }

        #endregion
    }
}
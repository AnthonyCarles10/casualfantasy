﻿using System;

namespace Sportfaction.CasualFantasy.PVP
{
    public enum ePVPGameMode
    {
        NONE,
        CLASSIC,
        DRAFT
    }
}

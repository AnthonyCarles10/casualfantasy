﻿using Sportfaction.CasualFantasy.Manager.Profile;
using Sportfaction.CasualFantasy.PVP.Gameplay.Field;
using Sportfaction.CasualFantasy.PVP.Gameplay.Matchs;
using Sportfaction.CasualFantasy.PVP.Gameplay.Players;
using Sportfaction.CasualFantasy.Utilities;

using SportFaction.CasualFootballEngine.ActionService.Enum;
using SportFaction.CasualFootballEngine.ActionService.Response;
using SportFaction.CasualFootballEngine.PhaseService.Enum;
using SportFaction.CasualFootballEngine.PlayerService.Enum;
using SportFaction.CasualFootballEngine.PlayerService.Model;
using SportFaction.CasualFootballEngine.TeamService.Enum;

using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SportFaction.CasualFootballEngine.Utilities;

namespace Sportfaction.CasualFantasy.PVP.Gameplay.Actions
{
    /// <summary>
    /// Class handling action management (display + preview + preparation & execution)
    /// </summary>
    /// @author Madeleine Tranier
    public sealed partial class ActionsManager : Singleton<ActionsManager>
    {
        #region Private Methods

        #region Pass Methods

        private void    _passPreview()
        {
            ClearPassTrajectories();
            MatchCamera.Instance.DisplayResolutionView();

            PlayersManager.Instance.TogglePlayerCollider(eTeamMethods.GetOpposite(UserData.Instance.PVPTeam), false);

            SimulatePassReceiver[]  lReceivers  =   _passSimulation.Receivers.ToArray();
            PlayerPVP   lPasser =   PlayersManager.Instance.GetPlayer(_curTeamPossess, MatchEngine.Instance.Engine.PossessionMain.CurPlayerPossession);
            PlayerPVP   lReceiver;

            //UtilityMethods.DebugSimulation(eAction.PASS, lPasser);

            for (int li = 0; li < lReceivers.Length; li++)
            {
                // Display overall risk on receiver
                lReceiver = PlayersManager.Instance.GetPlayer(lPasser.Data.Team, ePositionMethods.ConvertStringToEPosition(lReceivers[li].Position));

                lReceiver.UI.ToggleTarget(true);
                lReceiver.IsClickable = true;

                // Draw trajectory line
                string lTrajectoryName = "PASS_" + lPasser.name + "_" + lReceiver.Data.Position;
                DrawTrajectory(li, lTrajectoryName, lPasser.transform, lReceiver.transform, eColorDifficultyMethods.ConvertStringToEnum(lReceivers[li].ColorArrow), true);

                if (lReceivers[li].IsOffside)
                    DrawOffside(lReceiver.Data.CurFieldCase.X - 1);
            }
        }

        private void    _clearPassPreview()
        {
            ClearPassTrajectories();

            _passSimulation = null;

            _offsideLine.enabled = false;
            _xOffside = 100;

            ActionsUI.Instance.ToggleOffside(false);
        }

        public  void    ClearPassTrajectories()
        {
            for (int li = 0; li < _passTrajectories.Length; li++)
                _passTrajectories[li].gameObject.SetActive(false);

            PlayerPVP[] lAllPlayers =   PlayersManager.Instance.GetTeamPlayers(UserData.Instance.PVPTeam);
            int         lNbPlayers  =   lAllPlayers.Length;

            if (ePhase.ACTION == MatchEngine.Instance.CurPhase)
                for (int li = 0; li < lNbPlayers; li++)
                    lAllPlayers[li].UI.ToggleTarget(false);
        }

        private void    _passPrepared(TeamActionResponse pTeamResponse, int pIndex)
        {
            _clearPassPreview();
            //Debug.Log(GZipCompressor.Uncompress(PassPreparation.ConvertToJson()));
            //UtilityMethods.DebugPreparation(eAction.PASS, "");
            SimulatePassReceiver lResponse   =   PassPreparation.Receiver;

            PlayerPVP       lPasser         =   PlayersManager.Instance.GetPlayer(pTeamResponse.Team, pTeamResponse.Players[pIndex].Position);
            PlayerPVP       lReceiver       =   PlayersManager.Instance.GetPlayer(pTeamResponse.Team, lResponse.Receiver.Position);
            PlayerPVP       lOpponent, lDuelOpponent;
            List<Player>    lActionPlayers  =   new List<Player>();
            bool            lIsFirstPass    =   ePhase.PERIOD_INIT == MatchEngine.Instance.CurPhase;

            // Place target animation on receiver
            if (!lIsFirstPass)
            {
                _targetAnchor       =   lPasser.transform;
                _targetAnchorBis    =   lReceiver.transform;

                _targetLockedSprite.transform.position  =   lReceiver.transform.position;
                _targetLockedSprite.color               =   _colorConfig.GetTeamColor(lPasser.IsBlueTeam);
                _targetLockedSprite.gameObject.name     =   "Pass_Target";
                _targetLockedSprite.size                =   new Vector2(0.4f, 0.4f);
                _targetLockedSprite.gameObject.SetActive(true);

                _targetSprite.transform.parent.position =   lPasser.transform.position;
                _targetSprite.transform.parent.gameObject.SetActive(true);
            }

            if (!lIsFirstPass)
                lReceiver.UI.ToggleTarget(true);

            // Handle passer
            lPasser.Movements.OrientPlayerTowardsTarget(lReceiver.transform.position);
            SoccerBall.Instance.FollowPlayer(true, lPasser);
            lPasser.Animator.ToggleDuelAnim(true);

            lActionPlayers.Add(lPasser.Data);

            // Handle receiver
            lReceiver.Animator.ToggleDuelAnim(true);

            lActionPlayers.Add(lReceiver.Data);

            // Handle interceptors
            foreach (PlayerActionResponse lOpp in lResponse.Interceptors)
            {
                lOpponent   =   PlayersManager.Instance.GetPlayer(lOpp.Team, lOpp.Position);
                lOpponent.Animator.ToggleDuelAnim(true);
                lActionPlayers.Add(lOpponent.Data);
            }

            // Handle receiver pressure opponents
            foreach (PlayerActionResponse lOpp in lResponse.ReceiverOpponentsPressure)
            {
                lOpponent   =   PlayersManager.Instance.GetPlayer(lOpp.Team, lOpp.Position);
                lOpponent.Movements.OrientPlayerTowardsTarget(lReceiver.transform.position);
                lOpponent.Animator.ToggleDuelAnim(true);
            }

            // Handle receiver duel opponent
            if (null != lResponse.ReceiverOpponentDuel)
            {
                lDuelOpponent = PlayersManager.Instance.GetPlayer(lResponse.ReceiverOpponentDuel.Team, lResponse.ReceiverOpponentDuel.Position);
                lDuelOpponent.Animator.ToggleDuelAnim(true);
                lActionPlayers.Add(lDuelOpponent.Data);

                // Handle receiver duel opponent's pressure opponents
                foreach (PlayerActionResponse lOpp in lResponse.ReceiverOpponentDuelOpponentsPressure)
                {
                    lOpponent = PlayersManager.Instance.GetPlayer(lOpp.Team, lOpp.Position);
                    lOpponent.Movements.OrientPlayerTowardsTarget(lDuelOpponent.transform.position);
                    lOpponent.Animator.ToggleDuelAnim(true);
                }
            }

            // Draw trajectory line
            string lTrajectoryName = "PASS_" + lPasser.name + "_" + lReceiver.Data.Position;
            DrawTrajectory(0, lTrajectoryName, lPasser.transform, lReceiver.transform, eColorDifficultyMethods.ConvertStringToEnum(lResponse.ColorArrow));

            // Display icon in field cases on trajectory
            //if (!lPasser.IsBlueTeam)
            //{
            //    for (int li = 0; li < PassPreparation.FieldCasesPath.Length; li++)
            //        if (!PassPreparation.FieldCasesPath[li].IsEqualTo(lPasser.Data.CurFieldCase) && !PassPreparation.FieldCasesPath[li].IsEqualTo(lReceiver.Data.CurFieldCase))
            //            SoccerFieldUI.Instance.HighlightFieldCase(PassPreparation.FieldCasesPath[li], true);
            //}

            // Remove other players stats
            PlayerPVP[] lAllPlayers =   PlayersManager.Instance.GetAllPlayers();
            int         lNbPlayers  =   lAllPlayers.Length;

            for (int li = 0; li < lNbPlayers; li++)
            {
                if (!lActionPlayers.Contains(lAllPlayers[li].Data))
                    lAllPlayers[li].UI.ToggleTarget(false);
            }

            StartCoroutine(_delayBeforeHandleSkip());
        }

        #endregion

        #region Move Methods

        public  void    ClearMovePreview()
        {
            for (int li = 0; li < _fieldCaseOutlines.Length; li++)
                _fieldCaseOutlines[li].Hide();

            PlayersManager.Instance.TogglePlayerActive(UserData.Instance.PVPTeam);
        }

        private void    _movePreview()
        {
            ClearMovePreview();

            if (null == _playerFocusedRef && null == _duelPlayer)
                return;

            PlayerPVP lPlayerFocused = _playerFocusedRef.CurrentValue;

            PlayersManager.Instance.TogglePlayerCollider(false);

            //UtilityMethods.DebugSimulation(eAction.MOVE, lPlayerFocused);
            int lNbCases = _moveSimulation.AvailableFieldCases.Count;
            FieldCaseButton lCurBtn = null;

            PlayersManager.Instance.TogglePlayerSelection(UserData.Instance.PVPTeam, false);
            PlayersManager.Instance.TogglePlayerForbiddenSelection(UserData.Instance.PVPTeam, false);

            PlayerPVP[] lTeamPlayers    =   PlayersManager.Instance.GetTeamPlayers(UserData.Instance.PVPTeam);
            int         lNbPlayers      =   lTeamPlayers.Length;

            for (int li = 0; li < lNbPlayers; li++)
            {
                lTeamPlayers[li].Player3D.TogglePlayerActive(lTeamPlayers[li].Data.IsEqualTo(lPlayerFocused.Data));
            }

            // Display available field cases with risks
            for (int li = 0; li < lNbCases; li++)
            {
                lCurBtn = _fieldCaseOutlines[li];

                lCurBtn.Init(
                    _moveSimulation.AvailableFieldCases[li],
                    _moveSimulation.AvailableFieldCases[li].HasPlayer && _moveSimulation.AvailableFieldCases[li].HasPlayerTeam != lPlayerFocused.Data.Team.ToString(),
                    PlayersManager.Instance.CalculatePositionByCoordinates(_moveSimulation.AvailableFieldCases[li].FieldCase),
                    lPlayerFocused.Data.CurFieldCase,
                    _moveSimulation.AvailableFieldCases[li].CanMove
                );
            }
        }

        private void    _movePrepared(bool pFromCurManager, string pTeam, string pPosPlayer)
        {
            ClearMovePreview();

            // Hide available FC
            if (pFromCurManager)
            {
                for (int li = 0; li < 4; li++)
                    _fieldCaseOutlines[li].Hide();
            }

            PlayerPVP   lMovingPlayer   =   PlayersManager.Instance.GetPlayer(pTeam, pPosPlayer);
            Vector3     lFCPos          =   PlayersManager.Instance.CalculatePositionByCoordinates(MovePreparation.FieldCaseTarget);
            //UtilityMethods.DebugPreparation(eAction.MOVE, "", lMovingPlayer);
            GameObject lIntentionGO = Instantiate(_moveIntentionPrefab);
            lIntentionGO.transform.position = lFCPos;
            lIntentionGO.tag = "Disposable";
            lIntentionGO.name = "MOVE_" + lMovingPlayer.name;

            // Display feedback
            if (pFromCurManager)
            {
                _instructionFeedbackFX.transform.position = lMovingPlayer.transform.position;
                _instructionFeedbackFX.SetActive(true);
                _instructionFeedbackFX.GetComponent<Animator>().SetTrigger(pFromCurManager ? "BlueTeam" : "RedTeam");
            }

            // Orient player towards destination
            lMovingPlayer.Movements.OrientPlayerTowardsTarget(lFCPos);
            if (pFromCurManager)
                lMovingPlayer.UI.ValidatePlayerAction();

            if (pFromCurManager)
                MatchCamera.Instance.DisplayResolutionView();

            //lIntentionGO.transform.GetChild(pFromCurManager ? 2 : 1).GetComponent<SpriteRenderer>().enabled = true;

            // Draw trajectory line
            LineRenderer lTrajectory = lIntentionGO.transform.GetChild(5).GetComponent<LineRenderer>();
            lTrajectory.SetPosition(0, lMovingPlayer.transform.position);
            lTrajectory.SetPosition(1, lFCPos);
            lTrajectory.material.SetColor("_Color", pFromCurManager ? _colorConfig.GoodScore : _colorConfig.GetTeamColor(pFromCurManager));

            // Place trajectory arrow
            Transform lArrowTransform = lIntentionGO.transform.GetChild(4);
            lArrowTransform.position = lFCPos;
            lArrowTransform.eulerAngles = lMovingPlayer.Movements.PuppetTransform.eulerAngles;
            lArrowTransform.GetChild(0).GetComponent<SpriteRenderer>().color = pFromCurManager ? _colorConfig.GoodScore : _colorConfig.GetTeamColor(pFromCurManager);
            _ownMovePreparation.Add(lIntentionGO);

            Transform lSquareTransform = lIntentionGO.transform.GetChild(6);
            lSquareTransform.position = lMovingPlayer.transform.position;
            if (pFromCurManager)
                lSquareTransform.GetComponent<SpriteRenderer>().color = _colorConfig.GoodScore;
            else
                lSquareTransform.GetComponent<SpriteRenderer>().enabled = false;
        }

        #endregion

        #region Sprint Methods

        private void _sprintPrepared(bool pFromCurManager, string pTeam, string pPosPlayer)
        {
            ClearMovePreview();

            // Hide available FC
            if (pFromCurManager)
            {
                for (int li = 0; li < 4; li++)
                    _fieldCaseOutlines[li].Hide();
            }

            PlayerPVP   lMovingPlayer   =   PlayersManager.Instance.GetPlayer(pTeam, pPosPlayer);
            Vector3     lFCPos          =   PlayersManager.Instance.CalculatePositionByCoordinates(SprintPreparation.FieldCaseTarget);
            //UtilityMethods.DebugPreparation(eAction.SPRINT, "", lMovingPlayer);

            GameObject  lIntentionGO    =   GameObject.Find("MOVE_" + lMovingPlayer.name);
            if (null == lIntentionGO)
                lIntentionGO = Instantiate(_moveIntentionPrefab);

            lIntentionGO.transform.position = lFCPos;
            lIntentionGO.tag    =   "Disposable";
            lIntentionGO.name   =   "MOVE_" + lMovingPlayer.name;

            // Display feedback
            _instructionFeedbackFX.transform.position   =   lMovingPlayer.transform.position;
            _instructionFeedbackFX.SetActive(true);
            _instructionFeedbackFX.GetComponent<Animator>().SetTrigger(pFromCurManager ? "BlueTeam" : "RedTeam");

            // Orient player towards destination
            lMovingPlayer.Movements.OrientPlayerTowardsTarget(lFCPos);
            lMovingPlayer.UI.ValidatePlayerAction(false);
            SoccerBall.Instance.FollowPlayer(true, lMovingPlayer);

            if (pFromCurManager && ePhase.ACTION == MatchEngine.Instance.CurPhase)
                MatchCamera.Instance.DisplayResolutionView();

            int lCurManagerIndex = ePhase.ACTION == MatchEngine.Instance.CurPhase ? 2 : 0;
            lIntentionGO.transform.GetChild(2).GetComponent<SpriteRenderer>().enabled = false;
            lIntentionGO.transform.GetChild(pFromCurManager ? lCurManagerIndex : 1).GetComponent<SpriteRenderer>().enabled = true;

            // Draw trajectory line
            LineRenderer lTrajectory = lIntentionGO.transform.GetChild(5).GetComponent<LineRenderer>();
            lTrajectory.SetPosition(0, lMovingPlayer.transform.position);
            lTrajectory.SetPosition(1, lFCPos);
            if (ePhase.ACTION == MatchEngine.Instance.CurPhase)
                lTrajectory.material.SetColor("_Color", pFromCurManager ? _colorConfig.GoodScore : _colorConfig.GetTeamColor(false));
            else
                lTrajectory.material.SetColor("_Color", _colorConfig.GetTeamColor(pFromCurManager));

            // Place trajectory arrow
            Transform lArrowTransform = lIntentionGO.transform.GetChild(4);
            lArrowTransform.position = lFCPos;
            lArrowTransform.eulerAngles = lMovingPlayer.Movements.PuppetTransform.eulerAngles;
            if (ePhase.ACTION == MatchEngine.Instance.CurPhase)
                lArrowTransform.GetChild(0).GetComponent<SpriteRenderer>().color = pFromCurManager ? _colorConfig.GoodScore : _colorConfig.GetTeamColor(false);
            else
                lArrowTransform.GetChild(0).GetComponent<SpriteRenderer>().color = _colorConfig.GetTeamColor(pFromCurManager);

            Transform lSquareTransform = lIntentionGO.transform.GetChild(6);
            lSquareTransform.position = lMovingPlayer.transform.position;
            if (ePhase.ACTION == MatchEngine.Instance.CurPhase)
                lSquareTransform.GetComponent<SpriteRenderer>().color = pFromCurManager ? _colorConfig.GoodScore : _colorConfig.GetTeamColor(false);
            else
                lSquareTransform.GetComponent<SpriteRenderer>().color = _colorConfig.GetTeamColor(pFromCurManager);

            StartCoroutine(_delayBeforeHandleSkip());
        }

        #endregion

        #region Long Kick Methods

        /*private void _longKickPreview()
        {
            MatchCamera.Instance.DisplayLongKickProjection();

            ePosition lPlayerPossess = MatchEngine.Instance.Engine.PossessionMain.CurPlayerPossession;
            PlayerPVP lStriker = PlayersManager.Instance.GetPlayer(_curTeamPossess, lPlayerPossess);

            // Display LK zoning
            SoccerFieldUI.Instance.ToggleZoning(_longKickSimulation.FieldCasesRisk, true, lStriker.Data.CurFieldCase);

            // Activate goal difficulty sprite
            _goalRiskMesh.enabled = false;
            _goalRiskMesh.GetComponent<BoxCollider>().enabled = true; // Make sure box collider always enabled just in case
            _goalRiskMesh.transform.position = new Vector3(4.086f, 0.01f, -1.618f);
            _goalRiskMesh.transform.GetChild(0).localPosition = new Vector3(0.18f, 0.009f, 0f);
            _goalRiskMesh.transform.DOPunchScale(new Vector3(0.7f, 0.7f, 1f), 0.5f, 1);
            _goalRiskMesh.gameObject.SetActive(true);
            _goalRiskMesh.GetComponent<InteractableGameObject>().Active = true;
            _goalRiskMesh.transform.GetChild(0).GetChild(0).GetComponent<SpriteRenderer>().color = _colorConfig.GetTeamColor(true);

            foreach (LongKickTrajectorySimulation pTrajectory in _longKickSimulation.Trajectories)
            {
                if ("center" == pTrajectory.Direction)
                {
                    // Draw trajectory line
                    string lTrajectoryName = "LONG_KICK_" + lStriker.name;
                    DrawTrajectory(0, lTrajectoryName, lStriker.transform, _goalRiskMesh.transform, eColorDifficultyMethods.ConvertStringToEnum(pTrajectory.ColorArrow), true);
                }
            }

            _goalMesh.GetComponent<Animator>().enabled = true;
        }*/

        private void    _clearLongKickPreview()
        {
            _passTrajectories[0].gameObject.SetActive(false);

            _goalRiskMesh.gameObject.SetActive(false);
            _goalMesh.GetComponent<Animator>().enabled = false;
            _goalMesh.GetComponent<Animator>().WriteDefaultValues();
        }

        private void    _longKickPrepared(string pPreparation)
        {
            _clearLongKickPreview();

            LongKickPreparationResponse lPreparation    =   new LongKickPreparationResponse();
            lPreparation.Parse(pPreparation);

            PlayerPVP   lStriker    =   PlayersManager.Instance.GetPlayer(lPreparation.Stricker.Team, lPreparation.Stricker.Position);
            PlayerPVP   lGoalKeeper =   PlayersManager.Instance.GetPlayer(lPreparation.GoalKeeper.Team, lPreparation.GoalKeeper.Position);
            PlayerPVP   lOpponent;
            //UtilityMethods.DebugPreparation(eAction.LONG_KICK, pPreparation);

            // Place target animation on opponent team's goalkeeper
            _goalRiskMesh.transform.position    =   new Vector3(lStriker.IsBlueTeam ? 4.086f : -4.217f, 0.01f, -1.618f);
            _goalRiskMesh.transform.GetChild(0).localPosition   =   new Vector3(lStriker.IsBlueTeam ? 0.18f : -0.175f, 0.009f, 0f);
            _goalRiskMesh.transform.GetChild(0).GetChild(0).GetComponent<SpriteRenderer>().color = _colorConfig.GetTeamColor(lStriker.IsBlueTeam);
            _goalRiskMesh.GetComponent<BoxCollider>().enabled = false;
            _goalRiskMesh.gameObject.SetActive(true);
            _goalRiskMesh.GetComponent<InteractableGameObject>().Active = false;

            _targetAnchor = lStriker.transform;
            _targetSprite.transform.parent.position = lStriker.transform.position;
            _targetSprite.transform.parent.gameObject.SetActive(true);

            _targetAnchorBis = null;
            _targetLockedSprite.color = _colorConfig.GetTeamColor(lStriker.IsBlueTeam);
            _targetLockedSprite.transform.position = _goalRiskMesh.transform.GetChild(0).GetChild(0).position;
            _targetLockedSprite.gameObject.name = "Long_Kick_Target";
            _targetLockedSprite.size = new Vector2(0.55f, 0.55f);
            _targetLockedSprite.gameObject.SetActive(true);

            if (ePhase.ACTION == MatchEngine.Instance.CurPhase)
            {
                lStriker.UI.ValidatePlayerAction(false);
                StartCoroutine(_delayBeforeHandleSkip());
                return;
            }

            // Handle striker
            lStriker.Animator.ToggleDuelAnim(true);
            lStriker.Movements.OrientPlayerTowardsGoal();

            // Handle interceptors
            foreach (PlayerActionResponse pOpp in lPreparation.Interceptors)
            {
                lOpponent = PlayersManager.Instance.GetPlayer(pOpp.Team, pOpp.Position);
                lOpponent.Animator.ToggleDuelAnim(true);
            }

            // Handle goal keeper
            lGoalKeeper.Animator.ToggleDuelAnim(true);

            if (null == GameObject.Find("LONG_KICK_" + lStriker.name))
            {
                // Draw trajectory line
                string lTrajectoryName = "LONG_KICK_" + lStriker.name;
                DrawTrajectory(0, lTrajectoryName, lStriker.transform, _goalRiskMesh.transform.GetChild(0).GetChild(0).transform, eColorDifficultyMethods.ConvertStringToEnum(lPreparation.ColorArrow));
            }

            StartCoroutine(_delayBeforeHandleSkip());
        }

        #endregion

        private void    _actionPrepared(PlayerPVP pPlayer, TeamActionResponse pTeamResponse)
        {
            if (pPlayer.Data.Team == UserData.Instance.PVPTeam)
            {
                PlayersManager.Instance.ResetActionUI(eTeam.TEAM_A);
                PlayersManager.Instance.ResetActionUI(eTeam.TEAM_B);
            }

            pPlayer.UI.ValidatePlayerAction(false);

            StartCoroutine(_delayBeforeHandleSkip());
        }

        public  void    DrawTrajectory(int pLineIndex, string pLineName, Transform pStartPoint, Transform pEndPoint, eColorDifficulty pDifficulty, bool pAnimate = false, bool pFill = true)
        {
            GameObject lTrajectory = _passTrajectories[pLineIndex];
            lTrajectory.transform.localEulerAngles = Vector3.zero;
            lTrajectory.SetActive(true);
            lTrajectory.GetComponent<LineRenderer>().enabled = false;
            lTrajectory.name = pLineName;

            float distance_Flt = Vector3.Distance(pStartPoint.position, pEndPoint.position);
            GameObject lineGrp_Gob = lTrajectory.transform.GetChild(1).gameObject;
            lTrajectory.transform.position = pStartPoint.position;
            lineGrp_Gob.transform.position = pStartPoint.position;
            lineGrp_Gob.transform.LookAt(pEndPoint.transform);
            lineGrp_Gob.transform.localScale = new Vector3(1, 1, distance_Flt);

            GameObject linePlane_Gob = lineGrp_Gob.transform.GetChild(0).gameObject;
            linePlane_Gob.GetComponent<MeshRenderer>().enabled = ePhase.PERIOD_INIT != MatchEngine.Instance.CurPhase;
            linePlane_Gob.GetComponent<MeshRenderer>().material.SetFloat("_scale", 0.5f * (distance_Flt * 2.0f));
            linePlane_Gob.GetComponent<MeshRenderer>().material.SetColor("_color", _colorConfig.GetColorDifficulty(pDifficulty, pFill));

            if (pAnimate)
            {
                float lFadeValue = -1f;
                linePlane_Gob.GetComponent<MeshRenderer>().material.SetFloat("_fade", lFadeValue);
                DOTween.To(() => lFadeValue, x => lFadeValue = x, 1f, 1f).OnUpdate(() =>
                {
                    linePlane_Gob.GetComponent<MeshRenderer>().material.SetFloat("_fade", lFadeValue);
                });
            }
        }

        #region Coroutines

        private IEnumerator _delayBeforeHandleSkip()
        {
            yield return new WaitForSeconds(0f);

            if ((ePhase.ACTION == MatchEngine.Instance.CurPhase || ePhase.TACTICAL_ATTACK == MatchEngine.Instance.CurPhase || ePhase.KICK_OFF == MatchEngine.Instance.CurPhase) && !_duelFinished)
                _handleSkip();
        }

        #endregion

        #endregion
    }
}
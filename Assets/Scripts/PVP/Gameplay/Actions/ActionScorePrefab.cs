﻿using Sportfaction.CasualFantasy.Action;

using SportFaction.CasualFootballEngine.ActionService.Enum;

using I2.Loc;
using UnityEngine;
using UnityEngine.UI;

namespace Sportfaction.CasualFantasy.PVP.Gameplay.Actions
{
	/// <summary>
	/// Class handling action scoring button
	/// </summary>
	/// @author Madeleine Tranier
    #pragma warning disable 414, CS0649
	public class ActionScorePrefab : MonoBehaviour
	{
		#region Properties

        public  bool    OwnAction   { get { return _ownAction; } }

        #endregion

        #region Serialized Fields

        [Header("Own Action UI")]
		[SerializeField, Tooltip("Action name text")]
		private TMPro.TMP_Text  _actionNameTxt  =   null; //!< Text component of action name
		[SerializeField, Tooltip("Action score")]
		private TMPro.TMP_Text  _maxScoreTxt    =   null; //!< Text component of action max score
        [SerializeField, Tooltip("Check icon img")]
        private Image           _checkIconImg   =   null;

		[Header("Configuration")]
		[SerializeField, Tooltip("Sprites config")]
		private Sprite[]    _spriteConfig; //!< Array of every sprite that can be used for setting up an action button
        [SerializeField, Tooltip("Text materials")]
        private Material[]  _txtMaterials;

		#endregion

		#region Private Fields

		private bool    _ownAction  =   true;

		#endregion

		#region Public Methods

        public  void    Setup(bool pOwnAction, eAction pActionType, int pMinScore, int pMaxScore)
        {
            _ownAction      =   pOwnAction;
            this.name       =   "Action_Score_" + pActionType + "_Btn";

            UtilityMethods.SetText(_actionNameTxt, LocalizationManager.GetTranslation(pActionType.ToString().ToLower() + "_title"));
            UtilityMethods.SetText(_maxScoreTxt, "[" + pMinScore + "-" + pMaxScore + "]");

            this.GetComponent<Image>().sprite   =   UtilityMethods.GetSprite(_spriteConfig, "ActionButton" + (_ownAction ? "Blue" : "RedResolution"));
            _checkIconImg.sprite                =   UtilityMethods.GetSprite(_spriteConfig, "Check_" + (_ownAction ? "Player" : "Adv") + "_Icone");

            DisplayScore(false);
            this.transform.GetChild(4).localEulerAngles =   new Vector3(180f, 0f, 0f);
            this.transform.GetChild(4).localPosition    =   new Vector3(0f, 1f, 0f);
            this.transform.GetChild(4).gameObject.SetActive(true);
        }

        public  void    HighlightName()
        {
            _actionNameTxt.fontMaterial         =   _txtMaterials[_ownAction ? 2 : 3];
            _actionNameTxt.characterSpacing     =   7;
            _actionNameTxt.fontSize             =   75;
            _actionNameTxt.enableWordWrapping   =   false;
        }

        public  void    DisplayScore(bool pActive)
        {
            _checkIconImg.gameObject.SetActive(!pActive);
            _actionNameTxt.enabled  =   !pActive;
            _maxScoreTxt.enabled    =   !pActive;
        }

        #endregion
    }
}
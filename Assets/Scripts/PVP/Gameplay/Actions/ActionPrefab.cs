﻿using Sportfaction.CasualFantasy.Action;
using Sportfaction.CasualFantasy.Manager.Profile;
using Sportfaction.CasualFantasy.PVP.Gameplay.Matchs;
using Sportfaction.CasualFantasy.Services.Userstracking;

using SportFaction.CasualFootballEngine.ActionService.Enum;
using SportFaction.CasualFootballEngine.ActionService.Response;

using DG.Tweening;
using I2.Loc;
using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

using   SFPositionsActionsItemBasic =   SportFaction.CasualFootballEngine.PositionService.Model.PositionsActionsItemLevelBasic;
using SportFaction.CasualFootballEngine.PhaseService.Enum;

namespace Sportfaction.CasualFantasy.PVP.Gameplay.Actions
{
    /// <summary>
    /// Class handling action button display & interactions
    /// </summary>
    /// @author Madeleine Tranier
    #pragma warning disable 414, CS0649
    public class ActionPrefab : MonoBehaviour, IPointerDownHandler, IPointerEnterHandler
    {
        #region Properties

        public  ActionsItemData             ActionData  { get { return _actionData; } }
        public  SFPositionsActionsItemBasic Action      { get { return _action; } }

        #endregion

        #region Serialized Fields

        [Header("Own Action UI")]
        [SerializeField, Tooltip("Action name text")]
        private TMPro.TMP_Text  _actionNameTxt      =   null; //!< Text component of action name
        [SerializeField, Tooltip("Action score")]
        private TMPro.TMP_Text  _maxScoreTxt        =   null; //!< Text component of action max score
        [SerializeField, Tooltip("Selection image")]
        private Image           _selectionImg       =   null; //!< Selection image
        [SerializeField, Tooltip("Risk pastil")]
        private Image           _riskPastil         =   null;
        [SerializeField, Tooltip("Move extension group")]
        private GameObject      _moveExtGO          =   null;

        #endregion

        #region Private Fields

        private ActionsItemData             _actionData =   null; //!< Class storing data related to action
        private SFPositionsActionsItemBasic _action     =   null;

        private bool    _isActive   =   false; //!< Is action button active or not ?
        private bool    _mouseDown  =   false; //!< Is mouse down ?

        #endregion

        #region Public Methods

        #region Touch Events

        public  void    OnPointerDown(PointerEventData pEventData)
        {
            if (!_isActive) return;

            UserEventsManager.Instance.RecordEvent(new UserActionEvent("manager_match_action_" + _actionData.KeyName));

            if (_actionData.Type != eAction.PASS && _actionData.Type != eAction.SPRINT)
            {
                MatchAudio.Instance.OnTapActivation();
                ActionsManager.Instance.PrepareAction(_actionData);
                ToggleClickable(false);
            }
            else
            {
                MatchAudio.Instance.OnTapValidation();
                ActionsManager.Instance.PreviewAction(_actionData);
            }
            this.GetComponent<Animation>().Play("FB_Button_Action");
        }

        public  void    OnPointerEnter(PointerEventData pEventData)
        {
            if (_mouseDown && _isActive)
                OnPointerDown(null);
        }

        #endregion

        public  void    Setup(ActionsItemData pData, SFPositionsActionsItemBasic pAction, SFPositionsActionsItemBasic pOppAction, int pIndex = 0)
        {
            _actionData =   pData;
            _action     =   pAction;
            this.name   =   "Action_" + pData.Type + "_Btn";

            bool lDisplayMove = (eAction.SPRINT == pAction.Action || eAction.PASS == pAction.Action) && ePhase.ACTION == MatchEngine.Instance.CurPhase;
            this.GetComponent<CanvasGroup>().DOFade(0f, 0f);
            this.GetComponent<RectTransform>().sizeDelta = new Vector2(lDisplayMove ? 323f : 304f, 186f);

            _actionNameTxt.rectTransform.sizeDelta = new Vector2(267f, eAction.SPRINT == pAction.Action ? 133f : 86f);
            _maxScoreTxt.transform.gameObject.SetActive(eAction.SPRINT != pAction.Action);

            UtilityMethods.SetText(_actionNameTxt, LocalizationManager.GetTranslation(pData.Type.ToString().ToLower() + "_title"));
            UtilityMethods.SetText(_maxScoreTxt, "[" + pAction.MinScore + "-" + pAction.MaxScore + "]");

            _moveExtGO.SetActive(lDisplayMove);

            ToggleClickable(true);

            ToggleSelection(false);
            this.GetComponent<Image>().raycastTarget = false;
            this.gameObject.SetActive(true);
            StartCoroutine(_delayDisplay(pIndex * 0.05f));

            _riskPastil.enabled = eAction.LONG_KICK == pData.Type;
            if (eAction.LONG_KICK == pData.Type)
            {
                LongKickSimulationResponse lLongKickSim = MatchEngine.Instance.SimulationLongKick(MatchEngine.Instance.GetDuelPlayer(UserData.Instance.PVPTeam));
                _riskPastil.color   =  ActionsManager.Instance.ColorConfig.GetColorDifficulty(lLongKickSim.Stricker.Color);
            }
        }

        public  void    ToggleSelection(bool pActive)
        {
            _selectionImg.enabled   =   pActive;
        }

        public  void    ToggleClickable(bool pActive)
        {
            _isActive   =   pActive;

            //_actionNameTxt.transform.parent.GetComponent<Image>().color         =   pActive ? Color.white : Color.gray;
            //_actionNameTxt.transform.parent.parent.GetComponent<Image>().color  =   pActive ? Color.white : Color.gray;

            _actionNameTxt.color    =   pActive ? Color.white : Color.gray;
            _maxScoreTxt.color      =   pActive ? Color.white : Color.gray;
        }

        #endregion

        #region Private Methods

        private void    Update()
        {
            if (Input.GetMouseButtonDown(0))
                _mouseDown = true;

            if (Input.GetMouseButtonUp(0))
                _mouseDown = false;
        }
        
        private IEnumerator _delayDisplay(float pDelay)
        {
            yield return new WaitForSeconds(pDelay);
            this.GetComponent<CanvasGroup>().DOFade(1f, 0f);
            //this.GetComponent<Animation>().Play("Btn_in_solo");
            this.GetComponent<Image>().raycastTarget = true;
        }
        
        #endregion
    }
}
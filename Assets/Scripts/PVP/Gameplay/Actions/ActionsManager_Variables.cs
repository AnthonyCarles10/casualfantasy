﻿using Sportfaction.CasualFantasy.Action;
using Sportfaction.CasualFantasy.Config;
using Sportfaction.CasualFantasy.Events;
using Sportfaction.CasualFantasy.PVP.Gameplay.Field;
using Sportfaction.CasualFantasy.PVP.Gameplay.Players;
using Sportfaction.CasualFantasy.References.CustomClass;
using Sportfaction.CasualFantasy.Utilities;

using SportFaction.CasualFootballEngine.ActionService.Response;
using SportFaction.CasualFootballEngine.FieldService.Model;
using SportFaction.CasualFootballEngine.PhaseService.Enum;
using SportFaction.CasualFootballEngine.PlayerService.Enum;
using SportFaction.CasualFootballEngine.PlayerService.Model;
using SportFaction.CasualFootballEngine.TeamService.Enum;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Sportfaction.CasualFantasy.PVP.Gameplay.Actions
{
    /// <summary>
    /// Class handling action management (display + preview + preparation & execution)
    /// </summary>
    /// @author Madeleine Tranier
    #pragma warning disable 414, CS0649
    public sealed partial class ActionsManager : Singleton<ActionsManager>
    {
        #region Properties
        
        public  ActionsItemData             ActionInProcess     { get { return _actionInProcess; } }

        public  LongKickSimulationResponse  LongKickSimulation  { get { return _longKickSimulation; } }
        public  PassSimulationResponse      PassSimulation      { get { return _passSimulation; } }
        public  MoveSimulationResponse      MoveSimulation      { get { return _moveSimulation; } }

        public  PlayerPVP                   DuelPlayer          { get { return _duelPlayer; } }
        public  PlayerPVP                   DuelOppPlayer       { get { return _duelOppPlayer; } }
        public  bool                        IsSoloAction        { get { return _isSoloAction; } }

        public  List<ActionPrefab>          ActionPrefabs       { get { return _actionPrefabs; } }

        public  Sprite[]                    SpriteConfig        { get { return _spriteConfig; } }
        public  TimingConfig                TimingConfig        { get { return _timingConfig; } }
        public  GameplayConfig              GameplayConfig      { get { return _gameplayConfig; } }
        public  ColorConfig                 ColorConfig         { get { return _colorConfig; } }

        public  bool                        IsTester            { get { return _isTester; } set { _isTester = value; } }

        public  eTeam                       CurTeamPossess      { get { return _curTeamPossess; } }

        public  ActionsData                 ActionsDatabase     { get { return _actionsDatabase; } }

        public  FieldCaseButton[]           FieldCaseOutlines   { get { return _fieldCaseOutlines; } }


        #endregion

        #region Public Fields

        [ReadOnly]
        public  GameObject              CurActionPrefab     =   null;
        [ReadOnly]
        public  PassPreparationResponse PassPreparation     =   new PassPreparationResponse();
        [ReadOnly]
        public  MoveResolution          MovePreparation     =   new MoveResolution();
        [ReadOnly]
        public  MoveResolution          SprintPreparation   =   new MoveResolution();

        #endregion

        #region Serialized Fields

        [Header("UI")]
        [SerializeField, Tooltip("Field case outlines")]
        private FieldCaseButton[]   _fieldCaseOutlines; //!< Array of move direction buttons
        [SerializeField, Tooltip("Action checkmark")]
        private SpriteRenderer      _targetSprite           =   null; //!< Quad for highlighting targeted player
        [SerializeField, Tooltip("Target locked")]
        private SpriteRenderer      _targetLockedSprite     =   null;
        [SerializeField, Tooltip("Goal quad")]
        private MeshRenderer        _goalRiskMesh           =   null; //!< Sprite for long kick risk
        [SerializeField, Tooltip("Goal mesh")]
        private MeshRenderer        _goalMesh               =   null; //!< Opponent goal mesh
        [SerializeField, Tooltip("Duel quad")]
        private SpriteRenderer[]    _duelQuadRenderers      =   null; //!< Current duel quad
        [SerializeField, Tooltip("Current own player indicator")]
        private Animation           _minionSelectedAnim     =   null; //!< Animation for currently selected minion
        [SerializeField, Tooltip("Action canceler")]
        private GameObject          _actionCancelerGO       =   null; //!< Action canceler GO
        [SerializeField, Tooltip("Pass trajectories")]
        private GameObject[]        _passTrajectories; //!< Array of pass trajectories
        [SerializeField, Tooltip("Instruction feedback FX")]
        private GameObject          _instructionFeedbackFX  =   null; //!< Instruction given feedback
        [SerializeField, Tooltip("Goal opportunity GO")]
        private GameObject          _goalOpportunityGO      =   null;
        [SerializeField, Tooltip("Offside Line")]
        private LineRenderer        _offsideLine            =   null;

        [Header("Prefabs")]
        [SerializeField, Tooltip("Action prefab")]
        private GameObject  _actionPrefab               =   null; //!< ActionPrefab GO
        [SerializeField, Tooltip("Pressure GO prefab")]
        private GameObject  _pressurePrefab             =   null; //!< Pressure GO prefab
        [SerializeField, Tooltip("Move intention prefab")]
        private GameObject  _moveIntentionPrefab        =   null; //!< Move intention prefab
		[SerializeField, Tooltip("Risk prefab")]
		private GameObject  _riskPrefab                 =   null; //!< Risk sprite prefab

        [Header("Game events")]
        [SerializeField, Tooltip("Event to invoke when an action projection is displayed")]
        private GameEvent   _projectionInProgressEvt    =   null; //!< Event to invoke when pass projection is in progress
        
        [Header("References")]
        [SerializeField, Tooltip("Ref to player selected")]
        private PlayerPVPReference  _playerFocusedRef   =   null; //!< Ref to currently selected player's main script
        
        [Header("Configuration")]
        [SerializeField, Tooltip("Database of every action data")]
        private ActionsData         _actionsDatabase    =   null; //!< Database of every action data
        [SerializeField, Tooltip("Timing config")]
        private TimingConfig        _timingConfig       =   null; //!< Class storing timing for every game animation
        [SerializeField, Tooltip("Color config")]
        private ColorConfig         _colorConfig        =   null; //!< Class storing every color used in game
        [SerializeField, Tooltip("Gameplay config")]
        private GameplayConfig      _gameplayConfig     =   null; //!< Class storing match related data in game
        [SerializeField, Tooltip("Sprite config")]
        private Sprite[]            _spriteConfig; //!< Array of every sprite that can be used by script
        
        #endregion
        
        #region Private Fields
        
        private bool    _isManagerTeamA =   true; //!< Is current manager Team A's manager ?
        private bool    _isSoloAction   =   true; //!< Is current situation a solo action phase ?
        private eTeam   _curTeamPossess =   eTeam.NONE; //!< Current team possession

        private ActionPool              _pool                   =   null; //!< Pool of ActionPrefab
        private ActionsItemData         _actionInProcess        =   null; //!< Currently previewed action data
        private Player                  _playerInProcess        =   null; //!< Currently previewed action' player data
        private FieldCase               _fieldCaseInProgress    =   null; //!< FieldCase chosen by manager for currently previewed action
        private PlayerPVP               _duelPlayer             =   null; //!< Current duel player main script (owned by current manager)
        private PlayerPVP               _duelOppPlayer          =   null; //!< Current duel player main script (owned by current manager)
        private List<PlayerPVP>         _curPlayers             =   new List<PlayerPVP>(); //!< Current phase's players in action
        private Dictionary<ePosition, IEnumerator>    _cor      =   new Dictionary<ePosition, IEnumerator>(); //!< Blink animation coroutines
        private Dictionary<ePosition, List<int>>    _receiverOpps   =   new Dictionary<ePosition, List<int>>();
        private List<bool>                          _corRunning =   new List<bool>();
        private LongKickSimulationResponse  _longKickSimulation =   null; //!< Current long kick simulation data
        private MoveSimulationResponse      _moveSimulation     =   null; //!< Current move simulation data
        private PassSimulationResponse      _passSimulation     =   null;

        private List<MoveResolution>   _oppMovePreparation =   new List<MoveResolution>();
        private List<GameObject>                _ownMovePreparation =   new List<GameObject>();
        private List<string>                    _oppMovePosition    =   new List<string>();
        private LongKickPreparationResponse _longKickPreparation    =   new LongKickPreparationResponse();

        private List<ActionPrefab>          _actionPrefabs      =   new List<ActionPrefab>(); //!< List of action button scripts
        private List<Player>                _duelPlayerPressureOpps     =   new List<Player>();
        private List<Player>                _duelOppPlayerPressureOpps  =   new List<Player>();

        private bool        _isTester           =   false; //!< Boolean to set to true if testing
        private Transform   _targetAnchor       =   null;
        private Transform   _targetAnchorBis    =   null;
        private ushort      _xOffside           =   100;
        private bool        _skipPhase          =   false;
        private IEnumerator _botCoroutine       =   null;
        private bool        _duelFinished       =   false;

        #endregion
    }
}
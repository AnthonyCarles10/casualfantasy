﻿using Sportfaction.CasualFantasy.Manager.Profile;
using Sportfaction.CasualFantasy.PVP.Gameplay.Matchs;
using Sportfaction.CasualFantasy.PVP.Gameplay.Players;
using Sportfaction.CasualFantasy.PVP.Gameplay.UI;
using Sportfaction.CasualFantasy.PVP.Test;
using Sportfaction.CasualFantasy.References;
using Sportfaction.CasualFantasy.References.CustomClass;

using SportFaction.CasualFootballEngine.ActionService.Enum;
using SportFaction.CasualFootballEngine.ActionService.Response;
using SportFaction.CasualFootballEngine.FieldService.Model;
using SportFaction.CasualFootballEngine.PhaseService.Enum;
using SportFaction.CasualFootballEngine.TeamService.Enum;

using DG.Tweening;
using I2.Loc;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Sportfaction.CasualFantasy.PVP.Gameplay.Actions
{
    /// <summary>
    /// Class handling display of action board
    /// </summary>
    /// @author Madeleine Tranier
    #pragma warning disable 414, CS0649
    public class ActionsUI : MonoBehaviour
    {
        #region Public Fields

        public  static  ActionsUI   Instance; //!< Static instance of class

        ///////////////////////////////////////////////////////////////--------------------------------------------> [MAC] || Vars
        //public  GameObject[]    MinionsStatsGrpArray_Gob;
        public  GameObject  ResolutionUIFXManagerPC_Gob;
        public  GameObject  ResolutionUIFXManagerNPC_Gob;
        public  GameObject  ResolutionFieldFXManager_Gob;
        public  GameObject  ResolutionsManagerGrp_Gob;

        public  GameObject  GoodDefenseFX_Gob;

        #endregion

        #region Properties

        public  bool    BoardDisplayed      { get { return _boardDisplayed; } }

        #endregion

        #region Serialized Fields

        [Header("UI Elements")]
		[SerializeField, Tooltip("Duel Container")]
		private	RectTransform   _actionUIContainer	        =	null; //!< Action entire panel transform
		[SerializeField, Tooltip("Action buttons")]
		private	RectTransform   _actionListContainer        =	null; //!< Action list container transform

        [Space(8)]
        [SerializeField, Tooltip("Action indication text")]
        private TMPro.TMP_Text  _actionIndicationTxt        =   null; //!< Text component for action indication
        [SerializeField, Tooltip("Action tutorial text")]
        private TMPro.TMP_Text  _actionTutorialTxt          =   null; //!< Text component for action indication
        [SerializeField, Tooltip("Action tutorial text")]
        private TMPro.TMP_Text  _actionTutorialBisTxt       =   null; //!< Text component for action indication
        [SerializeField, Tooltip("Skip tactics button")]
        private GameObject      _skipTacticsBtnGO           =   null; //!< GO for skip tactics btn
        [SerializeField, Tooltip("Hourglass GO")]
        private GameObject      _hourGlassGO                =   null;
        [SerializeField, Tooltip("Tutorial img")]
        private Image[]         _tutorialImg;
        [SerializeField, Tooltip("Offside GO")]
        private GameObject      _offsideGO                  =   null;
        [SerializeField, Tooltip("Offside Receiver GO")]
        private GameObject      _offsideReceiverGO          =   null;

        [Space(8)]
        [SerializeField, Tooltip("Own player infos")]
        private PlayerFocusUI   _ownPlayerInfos         =   null; //!< Class handling current manager's active player infos

        [Header("Timer")]
        [SerializeField, Tooltip("Current manager timer fill")]
        private Image           _timerFill              =   null; //!< Image component for current manager timer UI

        [Header("References")]
        [SerializeField, Tooltip("Ref to player selected")]
        private PlayerPVPReference  _playerFocusedRef       =   null; //!< Ref to currently selected player's main script
        [SerializeField, Tooltip("Ref to duel timer")]
        private IntReference        _duelTimerRef           =   null; //!< Ref to action phase timer value
        [SerializeField, Tooltip("Ref to tactical timer")]
        private IntReference        _tacticalTimerRef       =   null;

        [Header("Configuration")]
        [SerializeField, Tooltip("Sprite config")]
        private Sprite[]            _spriteConfig; //!< Array of every sprite that can be used by script
        
        #endregion
        
        #region Private Fields
        
        private bool    _isManagerTeamA     =   true; //!< Is current manager Team A's manager ?
        private bool    _isSoloAction       =   false; //!< Is current situation a solo action phase ?
        private bool    _boardDisplayed     =   false;

        private IntReference            _curTimerRef        =   null; //!< Timer to use for display (changed on phase change)
        private List<ActionScorePrefab> _scorePrefabs       =   new List<ActionScorePrefab>();
        private FieldCase               _offsideFC          =   null;
        private PlayerPVP               _offsideReceiver    =   null;
        private int                     _timerValue         =   3000;

        #endregion

        #region Public Methods

        #region Game Event Listeners

        public  void    OnEngineStopPeriod()
        {
            ClearActions();
            ToggleActionBoard(false);
        }

        public  void    OnPerformTactics()
        {
            ResetTimers();
            ClearActions();

            _curTimerRef    =   _tacticalTimerRef;

            // Handle duel players infos
            _ownPlayerInfos.Setup(null);
            TogglePlayerInfos(false);

            _timerFill.transform.parent.gameObject.SetActive(true);
            UpdateBoardMessage("select_player");
            _actionListContainer.anchoredPosition = Vector2.zero;
            _actionUIContainer.transform.GetChild(0).gameObject.SetActive(true);

            ToggleActionBoard(true);
            ToggleActionIndications(true);
            ToggleActionTutorial(false);

            if (ActionsManager.Instance.IsTester && null != MatchTutorial.Instance && MatchTutorial.Instance.TutorialData.CounterActionPhase <= MatchTutorial.Instance.TutorialData.ActionPhases.Length - 1)
            {
                _skipTacticsBtnGO.SetActive(!MatchTutorial.Instance.PauseGame || 5 == MatchTutorial.Instance.CurTutorialStep);
                if (!MatchTutorial.Instance.PauseGame || 5 == MatchTutorial.Instance.CurTutorialStep)
                {
                    MatchTimers.Instance.StartCurrentTimer();
                }
            }
            else
            {
                _skipTacticsBtnGO.SetActive(true);
                MatchTimers.Instance.StartCurrentTimer();
            }
        }

        public  void    OnPrepareAction()
        {
            ResetTimers();
            ClearActions();
            ToggleActionIndications(false);

            eTeam lTeamPossess  =   MatchEngine.Instance.Engine.PossessionMain.CurTeamPossession;

            _isSoloAction   =   null == MatchEngine.Instance.GetDuelPlayer(eTeam.TEAM_A) || null == MatchEngine.Instance.GetDuelPlayer(eTeam.TEAM_B);
            _curTimerRef    =   _duelTimerRef;

            _timerFill.transform.parent.gameObject.SetActive(true);
            _actionUIContainer.transform.GetChild(0).gameObject.SetActive(false);
            _actionListContainer.anchoredPosition = new Vector2(0f, (lTeamPossess != UserData.Instance.PVPTeam || ePhase.KICK_OFF == MatchEngine.Instance.CurPhase) ? -25f : 0f);

            // Handle duel players infos
            _ownPlayerInfos.Setup(MatchEngine.Instance.GetDuelPlayer(UserData.Instance.PVPTeam));
            TogglePlayerInfos(null != MatchEngine.Instance.GetDuelPlayer(UserData.Instance.PVPTeam));

            // Handle timer UI
            if (_isSoloAction)
            {
                if (lTeamPossess != UserData.Instance.PVPTeam)
                {
                    UpdateBoardMessage("wait_for_opponent");
                    ToggleActionIndications(true);
                    _skipTacticsBtnGO.SetActive(false);
                }
            }

			ToggleActionBoard(true);

            if (ActionsManager.Instance.IsTester && null != MatchTutorial.Instance)
            {
                if (!MatchTutorial.Instance.PauseGame)
                    MatchTimers.Instance.StartCurrentTimer();
            }
            else
                MatchTimers.Instance.StartCurrentTimer();
        }
        
        public  void    ActionPhaseBegins()
        {
            if (ePhase.ACTION == MatchEngine.Instance.CurPhase || ePhase.KICK_OFF == MatchEngine.Instance.CurPhase)
            {
                if (null != ActionsManager.Instance.DuelPlayer)
                    UpdateDuelActions();

                _skipTacticsBtnGO.transform.localScale = new Vector3(1f, 1f, 1f);
            }
            else if (ePhase.ACTION_RESOLUTION == MatchEngine.Instance.CurPhase)
            {
                _skipTacticsBtnGO.SetActive(false);
            }
        }

        public  void    OnPlayerSelected()
        {
            if (null == _playerFocusedRef.CurrentValue || ePhase.ACTION == MatchEngine.Instance.CurPhase || ePhase.KICK_OFF == MatchEngine.Instance.CurPhase)
                return;

            MatchMessagesUI.Instance.TogglePossessionArrow(true);
            _ownPlayerInfos.Setup(_playerFocusedRef.CurrentValue.Data);
            TogglePlayerInfos(true);
            ToggleActionIndications(true);

            if (!_playerFocusedRef.CurrentValue.Data.IsEqualTo(ActionsManager.Instance.DuelPlayer?.Data))
                UpdateBoardMessage("move_player");
        }

        public  void    OnActionPrepared()
        {
            ResponseAction      lResponse       =   MatchEngine.Instance.GetResponse();
            eTeam               lPlayerTeam     =   eTeamMethods.ConvertStringToETeam(lResponse.Team);
            TeamActionResponse  lTeamResponse   =   lResponse.TeamA;

            if (lPlayerTeam == eTeam.TEAM_B)
                lTeamResponse = lResponse.TeamB;

            eAction lActionType = eActionMethods.ConvertStringToEnum(lTeamResponse.Players[lTeamResponse.Players.Count - 1].Action);

            if (UserData.Instance.PVPTeam == lPlayerTeam)
            {
                if (eAction.MOVE == lActionType)
                {
                    _playerFocusedRef.CurrentValue  =   null;

                    UpdateBoardMessage("select_player");
                    TogglePlayerInfos(false);
                    ToggleActionIndications(true);
                }
                else if (ePhase.ACTION == MatchEngine.Instance.CurPhase)
                {
                    TogglePlayerInfos(false);
                    ClearActions();

                    if (!eExecutorMethods.IsAIExecutor(eExecutorMethods.ConvertStringToEnum(lTeamResponse.Executor)))
                        DoneBoard();
                }
            }

            if (ActionsManager.Instance.CanSkipPhase() && ePhase.ACTION != MatchEngine.Instance.CurPhase)
                ToggleActionBoard(false);
        }

        #endregion

        public  void    UpdateDuelActions(bool pClear = true)
        {
            if (pClear)
            {
                ClearActions();
                _actionListContainer.gameObject.SetActive(true);
                ActionsManager.Instance.GetDuelActions(_actionListContainer);
            }
            else
            {
                _actionListContainer.gameObject.SetActive(true);
                ActionPrefab    lPrefabAction;
                int             lNbActions  =   ActionsManager.Instance.ActionPrefabs.Count;

                for (int li = 0; li < lNbActions; li++)
                {
                    lPrefabAction   =   ActionsManager.Instance.ActionPrefabs[li];

                    lPrefabAction.ToggleSelection(lPrefabAction?.ActionData.Type == ActionsManager.Instance.ActionInProcess?.Type);

                    lPrefabAction.ToggleClickable(!ActionsManager.Instance.DuelPlayer.HasAction && lPrefabAction.Action.IsAvailable);
                    if (ActionsManager.Instance.DuelPlayer.HasAction && ActionsManager.Instance.DuelPlayer.Action == lPrefabAction?.ActionData.Type)
                        lPrefabAction.gameObject.SetActive(false);
                    lPrefabAction.gameObject.SetActive(!ActionsManager.Instance.DuelPlayer.HasAction || ActionsManager.Instance.DuelPlayer.Action == lPrefabAction?.ActionData.Type);
                }
            }
        }

        public  void    UpdateBoardMessage(string pCase)
        {
            switch(pCase)
            {
                case "select_player":
                    _hourGlassGO.SetActive(true);
                    _hourGlassGO.GetComponent<Image>().sprite   =   UtilityMethods.GetSprite(_spriteConfig, "GameplaycursorMinion");
                    _hourGlassGO.GetComponent<Image>().color    =   ActionsManager.Instance.ColorConfig.TacticalColor;
                    _hourGlassGO.GetComponent<RectTransform>().sizeDelta = new Vector2(72f, 72f);
                    _hourGlassGO.GetComponent<RectTransform>().anchoredPosition = new Vector2(5f, -70f);
                    UtilityMethods.SetText(_actionIndicationTxt, LocalizationManager.GetTranslation("select_player"));
                    break;

                case "move_player":
                    _hourGlassGO.SetActive(true);
                    _hourGlassGO.GetComponent<Image>().sprite = UtilityMethods.GetSprite(_spriteConfig, "GameplayTacticPlacementBlue");
                    _hourGlassGO.GetComponent<Image>().color = Color.white;
                    _hourGlassGO.GetComponent<RectTransform>().sizeDelta = new Vector2(72f, 72f);
                    _hourGlassGO.GetComponent<RectTransform>().anchoredPosition = new Vector2(5f, -45f);
                    UtilityMethods.SetText(_actionIndicationTxt, LocalizationManager.GetTranslation("move_player_desc"));
                    break;

                case "wait_for_opponent":
                    _hourGlassGO.SetActive(true);
                    _hourGlassGO.GetComponent<Image>().sprite = UtilityMethods.GetSprite(_spriteConfig, "Timer_Icone");
                    _hourGlassGO.GetComponent<Image>().color = Color.white;
                    _hourGlassGO.GetComponent<RectTransform>().sizeDelta = new Vector2(68f, 89f);
                    _hourGlassGO.GetComponent<RectTransform>().anchoredPosition = new Vector2(5f, -60f);
                    UtilityMethods.SetText(_actionIndicationTxt, LocalizationManager.GetTranslation("action_opponent_turn"));
                    break;

                case "pass_in_progress":
                    _hourGlassGO.SetActive(false);
                    _actionListContainer.gameObject.SetActive(false);
                    _tutorialImg[0].sprite  =   UtilityMethods.GetSprite(_spriteConfig, "Shoot_Icone");
                    _tutorialImg[1].sprite  =   UtilityMethods.GetSprite(_spriteConfig, "Field_Icone");
                    _tutorialImg[0].color   =   ActionsManager.Instance.ColorConfig.GetTeamColor(true);
                    UtilityMethods.SetText(_actionTutorialTxt, LocalizationManager.GetTranslation("select_receiver"));
                    UtilityMethods.SetText(_actionTutorialBisTxt, LocalizationManager.GetTranslation("select_cancel"));
                    ToggleActionIndications(false);
                    ToggleActionTutorial(true);
                    break;
            }
        }

        public  void    DoneBoard()
        {
            TogglePlayerInfos(false);

            _skipTacticsBtnGO.SetActive(false);
            _hourGlassGO.SetActive(true);
            ClearActions();
            UpdateBoardMessage("wait_for_opponent");
            ToggleActionIndications(true);
        }

        public  void    ValidateOppAction(ActionPrefab pAction)
        {
            pAction.transform.SetParent(_actionListContainer);
            pAction.gameObject.SetActive(false);
            ToggleActionIndications(false);
        }

        public  void    ResetTimers()
        {
            _hourGlassGO.SetActive(false);
            _timerFill.transform.parent.gameObject.SetActive(false);
        }

        public  void    StartResolution(bool pMainDuel)
        {
            if (!ActionsManager.Instance.IsSoloAction)
                ToggleActionBoard(true, false);

            ResetTimers();
            ToggleActionIndications(false);
        }

        public  void    Resolution()
		{
			foreach (ActionScorePrefab pPrefab in _scorePrefabs)
                pPrefab.DisplayScore(true);
        }

        #region Toggle Methods

        public  void    ToggleSkipBtn(bool pActive)
        {
            _skipTacticsBtnGO.SetActive(pActive);
        }

        public  void    ToggleActionBoard(bool pActive, bool pPrepPhase = true)
        {
            DOTween.Kill(_actionUIContainer);
            //Debug.Log("ToggleActionBoard " + pActive + " | " + pPrepPhase);

            if (pActive)
			{
                int lWait   =   0;

                DOTween.To(() => lWait, x => lWait = x, 5, ActionsManager.Instance.TimingConfig.BoardAppearance / 2f).OnComplete(() =>
                {
                    MatchAudio.Instance.OnBoardAppears(true);
                });

                if (!_boardDisplayed)
                    _actionUIContainer.GetComponent<Animation>().Play("Board_In");
                else
                    ActionPhaseBegins();
            }
            else
            {
                _skipTacticsBtnGO.SetActive(false);
                _timerFill.transform.parent.gameObject.SetActive(false);

                if (_boardDisplayed)
                    _actionUIContainer.GetComponent<Animation>().Play("Board_Out");
            }

            _boardDisplayed =   pActive;
		}

        public  void    ToggleActionIndications(bool pActive)
        {
            if (pActive)
                _actionIndicationTxt.rectTransform.sizeDelta = new Vector2(_ownPlayerInfos.gameObject.activeSelf ? 975f : 1202f, 0f);

            _actionIndicationTxt.gameObject.SetActive(pActive);
        }
        
        public  void    ToggleActionTutorial(bool pActive)
        {
            if (pActive)
            {
                _actionTutorialTxt.rectTransform.sizeDelta = new Vector2(_ownPlayerInfos.gameObject.activeSelf ? 975f : 1202f, 0f);
                _actionTutorialBisTxt.rectTransform.sizeDelta = new Vector2(_ownPlayerInfos.gameObject.activeSelf ? 975f : 1202f, 0f);
            }
            _actionTutorialTxt.gameObject.SetActive(pActive);
        }

        public  void    ToggleActionBtn(bool pActive)
        {
            _actionListContainer.gameObject.SetActive(true);
        }

        public void TogglePlayerInfos(bool pActive)
        {
            _ownPlayerInfos.gameObject.SetActive(pActive);
        }

        public  void    ToggleOffside(bool pDisplay, FieldCase pFC = null)
        {
            _offsideGO.SetActive(pDisplay);
            _offsideFC = pFC;
            _offsideGO.transform.DOScaleX(ActionsManager.Instance.CurTeamPossess == UserData.Instance.PVPTeam ? 1f : -1f, 0f);

            if (pDisplay)
                _offsideGO.transform.position = Camera.main.WorldToScreenPoint(PlayersManager.Instance.CalculatePositionByCoordinates(pFC, false));
        }

        public  void    ToggleReceiverOffside(bool pDisplay, PlayerPVP pPlayer = null)
        {
            _offsideReceiverGO.SetActive(pDisplay);
            _offsideReceiver = pPlayer;
            _offsideReceiverGO.transform.DOScaleX(MatchEngine.Instance.ResponseAction.Team == UserData.Instance.PVPTeam.ToString() ? 1f : -1f, 0f);

            if (pDisplay)
            {
                _offsideReceiverGO.GetComponent<Image>().sprite =   UtilityMethods.GetSprite(_spriteConfig, "Offside_" + (pPlayer.IsBlueTeam ? "Player" : "Adv"));
                _offsideReceiverGO.transform.position   =   Camera.main.WorldToScreenPoint(pPlayer.transform.position);
            }
        }

        #endregion

        #region Clear Methods
        
        public  void    ClearActions()
        {
            for (int li = _actionListContainer.transform.childCount - 1; li >= 0; li--)
                _actionListContainer.GetChild(li).SetParent(ActionsManager.Instance.transform, false);
        }

        public  void    ClearBoard()
        {
            ToggleActionIndications(false);
            ToggleActionBoard(false);

            ClearActions();
        }

        public  void    ClearResolution()
        {
            // TOREMOVE
        }

        #endregion

        #endregion

        #region Private Methods

        private void    Awake()
        {
            Instance    =   this;

            if (null != UserData.Instance)
                _isManagerTeamA =   UserData.Instance.PVPTeam == eTeam.TEAM_A;
        }

        private void    Start()
		{
			_ownPlayerInfos.Init(_spriteConfig);
		}

        private void    Update()
        {
            if (null != _curTimerRef)
            {
                if (_timerFill.gameObject.activeSelf)
                    _timerFill.fillAmount       =   (float)_curTimerRef.CurrentValue / _curTimerRef.DefaultValue;
                //if (_curTimerRef.CurrentValue <= _timerValue && !_timerFill.transform.parent.parent.gameObject.activeSelf)
                //    _timerFill.transform.parent.parent.gameObject.SetActive(true);
            }

            //if (_timerIntTxt.gameObject.activeSelf && null != _curTimerRef)
            //{
            //    //Check "," & "." split on windows and mac
            //    string      catch_Str   =   ((float)_curTimerRef.CurrentValue / 1000f).ToString("F2");
            //    string[]    ltr         =   ((float)_curTimerRef.CurrentValue / 1000f).ToString("F2").Split(catch_Str.Contains(".") ? '.' : ',');

            //    UtilityMethods.SetText(_timerIntTxt, ltr[0] + '"');
            //    UtilityMethods.SetText(_timerDecTxt, ltr[1]);
            //}

            if (_offsideGO.activeSelf && null != _offsideFC)
                _offsideGO.transform.position = Camera.main.WorldToScreenPoint(PlayersManager.Instance.CalculatePositionByCoordinates(_offsideFC, false));

            if (_offsideReceiverGO.activeSelf && null != _offsideReceiver)
                _offsideReceiverGO.transform.position   =   Camera.main.WorldToScreenPoint(_offsideReceiver.transform.position);
        }

        #endregion
    }
}
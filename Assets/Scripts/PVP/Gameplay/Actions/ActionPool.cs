﻿using Sportfaction.CasualFantasy.Utilities;

using UnityEngine;

namespace Sportfaction.CasualFantasy.PVP.Gameplay.Actions
{
	/// <summary>
	/// Class handling pool of action prefabs
	/// </summary>
    /// @author Madeleine Tranier
	public sealed class ActionPool : AComponentsPool<ActionPrefab>
	{
		#region Public Methods

		/// <summary>
		/// Specific constructor
		/// </summary>
		/// <param name="pPrefab">Prefab to create</param>
		/// <param name="pParent">Parent of every action prefab created and inactive</param>
		/// <param name="pMinElements">Minimum of elements to create</param>
		public	ActionPool(GameObject pPrefab, Transform pParent, uint pMinElements)
		: base(pPrefab, pParent, pMinElements)
		{
		}

		/// <summary>
		/// Retrieves an unused ActionPrefab
		/// </summary>
		/// <returns>An inactive ActionPrefab</returns>
		public	ActionPrefab	GetUnusedAction()
		{
			return GetUnusedComponent();
		}

		#endregion
	}
}
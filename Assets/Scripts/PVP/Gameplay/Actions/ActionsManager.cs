using Sportfaction.CasualFantasy.Action;
using Sportfaction.CasualFantasy.Manager.Profile;
using Sportfaction.CasualFantasy.PVP.Gameplay.Field;
using Sportfaction.CasualFantasy.PVP.Gameplay.Matchs;
using Sportfaction.CasualFantasy.PVP.Gameplay.Players;
using Sportfaction.CasualFantasy.PVP.Gameplay.UI;
using Sportfaction.CasualFantasy.PVP.Network;
using Sportfaction.CasualFantasy.PVP.Test;
using Sportfaction.CasualFantasy.Services.Userstracking;

using SportFaction.CasualFootballEngine.ActionService.Enum;
using SportFaction.CasualFootballEngine.ActionService.Response;
using SportFaction.CasualFootballEngine.FieldService.Model;
using SportFaction.CasualFootballEngine.PhaseService.Enum;
using SportFaction.CasualFootballEngine.PlayerService.Model;
using SportFaction.CasualFootballEngine.PlayerService.Enum;
using SportFaction.CasualFootballEngine.TeamService.Enum;
using SportFaction.CasualFootballEngine.Utilities;

using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using SFPositionsActionsItemBasic = SportFaction.CasualFootballEngine.PositionService.Model.PositionsActionsItemLevelBasic;

namespace Sportfaction.CasualFantasy.PVP.Gameplay.Actions
{
    /// <summary>
    /// Class handling action management (display + preview + preparation & execution)
    /// </summary>
    /// @author Madeleine Tranier
    public sealed partial class ActionsManager : Singleton<ActionsManager>
    {
        #region Public Methods

        #region Game Event Listeners

        public  void    OnEngineStartPeriod()
        {
            _duelPlayer = null;
            _duelOppPlayer = null;
            _isSoloAction = true;
            _playerFocusedRef.CurrentValue = null;

            PlayersManager.Instance.UpdatePlayerName();

            _duelQuadRenderers[0].transform.parent.localPosition = Vector3.zero;
        }

        public  void    OnEngineStopPeriod()
        {
            _goalOpportunityGO.SetActive(false);
        }

        public  void    OnPhaseChanged(ePhase pPhase)
        {
            switch (pPhase)
            {
                case ePhase.DUEL_RESOLUTION:
                    _actionCancelerGO.SetActive(false);
                    ClearMovePreview();
                    break;

                case ePhase.ACTION_RESOLUTION:
                    _actionCancelerGO.SetActive(false);
                    ClearMovePreview();

                    _playerFocusedRef.CurrentValue = null;

                    MatchCamera.Instance.DisplayResolutionView();

                    if (null != _botCoroutine)
                        StopCoroutine(_botCoroutine);
                    break;

                default:
                    break;
            }
        }

		public  void    OnSkipTactics()
		{
            UserEventsManager.Instance.RecordEvent(new UserActionEvent("manager_match_end_turn"));
            if (true == PhotonNetwork.IsConnected)
            {
				PhotonMain.Instance.PhotonView.RPC("RPC_SkipTacticalPhase", RpcTarget.All, UserData.Instance.PVPTeam);
			}
			else
			{
                _skipPhase  =   true;
                //MatchEngine.Instance.Engine.ActionMain.SkipTacticalPhase(UserData.Instance.PVPTeam);
                ActionsUI.Instance.DoneBoard();
                _handleSkip();
            }
        }

        public  void    OnTacticalTimerDone()
        {
            _actionCancelerGO.SetActive(false);

            ActionsUI.Instance.ToggleActionBoard(false);
            PlayersManager.Instance.TogglePlayerCollider(false);
            PlayersManager.Instance.TogglePlayerSelection(UserData.Instance.PVPTeam, false);
            PlayersManager.Instance.TogglePlayerForbiddenSelection(UserData.Instance.PVPTeam, false);
            PlayersManager.Instance.TogglePlayerSquadPos(false);
        }

        public  void    OnDuelTimerDone()
        {
            _duelFinished = true;
            _actionCancelerGO.SetActive(false);

            if (null != _duelPlayer && !_duelPlayer.HasAction)
            {
                ClearSimulation();
                PlayersManager.Instance.ResetActionUI(UserData.Instance.PVPTeam);
            }
            else if (null == _duelPlayer)
            {
                _minionSelectedAnim.gameObject.SetActive(false);
            }

            if (ePhase.ACTION_RESOLUTION == MatchEngine.Instance.Engine.PhaseMain.GetNextPhase())
                ActionsUI.Instance.ToggleActionBoard(false);

            _duelPlayer?.UI.TogglePlayerName(true);
            _duelOppPlayer?.UI.TogglePlayerName(true);
            PlayersManager.Instance.TogglePlayerCollider(false);
            PlayersManager.Instance.TogglePlayerSelection(UserData.Instance.PVPTeam, false);
            PlayersManager.Instance.TogglePlayerForbiddenSelection(UserData.Instance.PVPTeam, false);
            PlayersManager.Instance.TogglePlayerSquadPos(false);
            ActionsUI.Instance.ResetTimers();

            if (null != _actionInProcess && eAction.PASS == _actionInProcess.Type)
                _clearPassPreview();
            else if (null != _actionInProcess && eAction.LONG_KICK == _actionInProcess.Type)
                _clearLongKickPreview();
            else if (null != _actionInProcess && (eAction.MOVE == _actionInProcess.Type || eAction.SPRINT == _actionInProcess.Type))
                ClearMovePreview();

            _actionInProcess = null;
            _playerFocusedRef.CurrentValue = null;
        }

        public  void    OnPrepareAction()
        {
            UtilityMethods.DebugData("POSSESSION");
            _actionCancelerGO.SetActive(false);
            UpdateDuelInfos();
            UpdateDuelQuad(false);
            HandleDuelPressureOpponents();
            _oppMovePreparation.Clear();
            _oppMovePosition.Clear();
            _ownMovePreparation.Clear();
            _skipPhase = false;
            _duelFinished = false;
            SoccerBall.Instance.FollowPlayer(true, PlayersManager.Instance.GetBallPossesser());

            if (MatchEngine.Instance.Engine.PhaseMain.GoalOpportunityZone && MatchMessagesUI.Instance.PeriodTimerStopped)
            {
                bool lCurPossess = UserData.Instance.PVPTeam == _curTeamPossess;
                _goalOpportunityGO.transform.localPosition = new Vector3(lCurPossess ? 2.918f : -3.05f, 0.01f, -1.626f);
                _goalOpportunityGO.SetActive(true);
            }
            else
                _goalOpportunityGO.SetActive(false);

            PlayersManager.Instance.ResetFirstActivation();
            PlayersManager.Instance.UpdatePlayerName();

            _minionSelectedAnim.gameObject.SetActive(!_isSoloAction || _curTeamPossess == UserData.Instance.PVPTeam);
            _playerFocusedRef.CurrentValue = _duelPlayer;

            if (_isSoloAction)
            {
                if (_curTeamPossess == UserData.Instance.PVPTeam)
                {
                    _duelPlayer.Animator.ToggleDuelAnim(true);
                    _duelPlayer.UI.TogglePlayerName(false);

                    _minionSelectedAnim.transform.position = _duelPlayer.transform.position;
                }
                else
                {
                    _duelOppPlayer.Animator.ToggleDuelAnim(true);
                    _duelOppPlayer.UI.TogglePlayerName(false);

                    PlayerPVP[] lAllPlayers = PlayersManager.Instance.GetAllPlayers();
                    int lNbPlayers = lAllPlayers.Length;

                    for (int li = 0; li < lNbPlayers; li++)
                        lAllPlayers[li].UI.ToggleTacticalValue(true);
                }
            }
            else
            {
                _duelPlayer.Animator.ToggleDuelAnim(true);
                _duelPlayer.UI.TogglePlayerName(false);

                _duelOppPlayer.Animator.ToggleDuelAnim(true);
                _duelOppPlayer.UI.TogglePlayerName(false);

                _minionSelectedAnim.transform.position = _duelPlayer.transform.position;
            }

            _duelPlayer?.UpdateHasAction(false);
            _duelPlayer?.UI.ToggleBubble(true);
            _duelOppPlayer?.UpdateHasAction(false);
            _duelOppPlayer?.UI.ToggleBubble(true);

            if (!_isTester && false == PhotonNetwork.IsConnected)
                StartCoroutine(_botPrepareAction(eTeamMethods.GetOpposite(UserData.Instance.PVPTeam)));
        }
        
        public  void    OnPerformTactics()
        {
            _duelFinished = false;
            UtilityMethods.DebugData("POSSESSION");
            _oppMovePreparation.Clear();
            _oppMovePosition.Clear();
            _ownMovePreparation.Clear();

            PlayersManager.Instance.ResetFirstActivation();
            PlayersManager.Instance.UpdatePlayerName();
            _handleSelectablePlayers();

            if (!_isTester && false == PhotonNetwork.IsConnected)
            {
                _botCoroutine = _botPrepareAction(eTeamMethods.GetOpposite(UserData.Instance.PVPTeam));
                StartCoroutine(_botCoroutine);
            }
        }

        public  void    OnPlayerSelected()
        {
            if (null == _playerFocusedRef.CurrentValue || ePhase.ACTION == MatchEngine.Instance.CurPhase || ePhase.KICK_OFF == MatchEngine.Instance.CurPhase)
                return;

            _minionSelectedAnim.gameObject.SetActive(true);
            _minionSelectedAnim.Play();

            if (_playerFocusedRef.CurrentValue.Data.IsEqualTo(_duelPlayer?.Data))
            {
                _duelPlayer.UI.ToggleSelection(false);
                _actionInProcess = null;
            }
            else if (ePhase.TACTICAL_ATTACK == MatchEngine.Instance.CurPhase)
            {
                UserEventsManager.Instance.RecordEvent(new UserActionEvent("manager_match_tactical_move"));
                ActionsItemData lAction = _actionsDatabase.GetActionData(eAction.MOVE);
                PreviewAction(lAction);
            }
        }

        public  void    OnActionPrepared()
        {
            ResponseAction      lResponse       =   MatchEngine.Instance.GetResponse();
            eTeam               lPlayerTeam     =   eTeamMethods.ConvertStringToETeam(lResponse.Team);
            TeamActionResponse  lTeamResponse   =   lResponse.TeamA;

            if (lPlayerTeam == eTeam.TEAM_B)
                lTeamResponse = lResponse.TeamB;

            eAction         lActionType     =   eActionMethods.ConvertStringToEnum(lTeamResponse.Players[lTeamResponse.Players.Count - 1].Action);
            PlayerPVP       lCurPlayer      =   PlayersManager.Instance.GetPlayer(lPlayerTeam, ePositionMethods.ConvertStringToEPosition(lTeamResponse.PlayerActionPosition));
            PlayerResponse  lPlayerResponse =   MatchEngine.Instance.Engine.ActionMain.GetPlayerResponse(lCurPlayer.Data.Team, lCurPlayer.Data.Position);

            if (lPlayerTeam == UserData.Instance.PVPTeam)
                _minionSelectedAnim.gameObject.SetActive(false);

            if (ePhase.PERIOD_INIT == MatchEngine.Instance.CurPhase)
                return;

            // Display action chosen UI on player
            lCurPlayer.UpdateHasAction(true, lActionType);
            if (lCurPlayer.Data.IsEqualTo(_duelOppPlayer?.Data) || lCurPlayer.Data.IsEqualTo(_duelPlayer?.Data))
                lCurPlayer.UI.CheckBubble();

            // Handle manager's choice
            if (lPlayerResponse.Team == UserData.Instance.PVPTeam.ToString())
            {
                _actionInProcess = null;
                _actionCancelerGO.SetActive(false);

                if (ePhase.ACTION != MatchEngine.Instance.CurPhase && ePhase.KICK_OFF != MatchEngine.Instance.CurPhase)
                    _handleSelectablePlayers();

                PlayersManager.Instance.TogglePlayerActive(UserData.Instance.PVPTeam);
                PlayersManager.Instance.TogglePlayerName(true);
                _duelPlayer?.UI.TogglePlayerName(false);
                _duelOppPlayer?.UI.TogglePlayerName(false);

                //if (eAction.MOVE != lActionType)
                //    MatchCamera.Instance.DisplayResolutionView();

                if (eAction.PASS == lActionType)
                {
                    ActionsUI.Instance.ToggleActionTutorial(false);
                    //Debug.Log(GZipCompressor.Uncompress(lPlayerResponse.Preparation));
                    PassPreparation.Parse(MatchEngine.Instance.Engine, lPlayerResponse.Preparation);
                    _passPrepared(lTeamResponse, lTeamResponse.Players.Count - 1);
                }
                else if (eAction.LONG_KICK == lActionType)
                {
                    _longKickPrepared(lPlayerResponse.Preparation);
                }
                else if (eAction.MOVE == lActionType)
                {
                    PlayersManager.Instance.TogglePlayerActive(UserData.Instance.PVPTeam);
                    MovePreparation.Parse(lTeamResponse.Players[lTeamResponse.Players.Count - 1].Preparation);
                    _movePrepared(true, lTeamResponse.Team, lTeamResponse.PlayerActionPosition);
                    StartCoroutine(_delayBeforeHandleSkip());
                }
                else if (eAction.SPRINT == lActionType)
                {
                    SprintPreparation.Parse(lPlayerResponse.Preparation);
                    _sprintPrepared(true, lTeamResponse.Team, lTeamResponse.PlayerActionPosition);
                }
                else
                    _actionPrepared(_duelPlayer, lTeamResponse);
            }

            // Handle opponent's choice
            else if (eTeamMethods.GetOpposite(UserData.Instance.PVPTeam).ToString() == lTeamResponse.Team)
            {
                if (eAction.PASS == lActionType)
                {
                    PassPreparation.Parse(MatchEngine.Instance.Engine, lPlayerResponse.Preparation);
                    _passPrepared(lTeamResponse, lTeamResponse.Players.Count - 1);
                }
                else if (eAction.LONG_KICK == lActionType)
                {
                    _longKickPrepared(lPlayerResponse.Preparation);
                }
                else if (eAction.MOVE == lActionType)
                {
                    MoveResolution lOppMove = new MoveResolution();
                    lOppMove.Parse(lTeamResponse.Players[lTeamResponse.Players.Count - 1].Preparation);
                    _oppMovePreparation.Add(lOppMove);
                    _oppMovePosition.Add(lTeamResponse.PlayerActionPosition);
                }
                else if (eAction.SPRINT == lActionType)
                {
                    SprintPreparation.Parse(lPlayerResponse.Preparation);
                    _sprintPrepared(false, lTeamResponse.Team, lTeamResponse.PlayerActionPosition);
                }
                else
                    _actionPrepared(_duelOppPlayer, lTeamResponse);

                StartCoroutine(_delayBeforeHandleSkip());
            }
        }

        public  void    OnActionCanceled()
        {
            MatchAudio.Instance.OnTapCancellation();

            if (null != _playerFocusedRef.CurrentValue)
            {
                _minionSelectedAnim.gameObject.SetActive(false);
                if (null != _actionInProcess && eAction.MOVE == _actionInProcess.Type)
                    _playerFocusedRef.CurrentValue = null;

                ActionsUI.Instance.TogglePlayerInfos(ePhase.ACTION == MatchEngine.Instance.CurPhase);
                if (!MatchEngine.Instance.HasAction(UserData.Instance.PVPTeam))
                {
                    ActionsUI.Instance.ToggleActionTutorial(false);
                    ActionsUI.Instance.ToggleActionBtn(true);
                    ActionsUI.Instance.UpdateDuelActions();
                }
                MatchCamera.Instance.DisplayResolutionView();
            }

            CancelActionInProcess();
        }

        public  void    OnActionFinished()
        {
            ClearSimulation();

            ActionsUI.Instance.ToggleOffside(false);
            ActionsUI.Instance.ToggleReceiverOffside(false);

            PlayersManager.Instance.ResetTacticalUI();
            PlayersManager.Instance.TogglePlayerName(true);
            PlayersManager.Instance.UpdatePlayerName();
            PlayersManager.Instance.UpdatePlayerPossession();

            _duelPlayerPressureOpps.Clear();
            _duelOppPlayerPressureOpps.Clear();

            _offsideLine.enabled = false;
            _xOffside = 100;

            PlayerPVP[] lAllPlayers =   PlayersManager.Instance.GetAllPlayers();
            int         lNbPlayers  =   lAllPlayers.Length;

            for (int li = 0; li < lNbPlayers; li++)
            {
                lAllPlayers[li].UI.UpdatePlayerName();
                if (ePhase.ACTION == MatchEngine.Instance.Engine.PhaseMain.GetNextPhase())
                    lAllPlayers[li].UI.ToggleTacticalValue(false);
            }

            if (ePhase.ACTION_RESOLUTION == MatchEngine.Instance.CurPhase ||
                (ePhase.DUEL_RESOLUTION == MatchEngine.Instance.CurPhase && ePhase.ACTION == MatchEngine.Instance.Engine.PhaseMain.GetNextPhase())
            )
            {
                ClearBeforeResolution();
                _targetLockedSprite.gameObject.SetActive(false);
                _targetSprite.transform.parent.gameObject.SetActive(false);

                GameObject[] lDisposable_GO = GameObject.FindGameObjectsWithTag("Disposable");
                for (int li = 0; li < lDisposable_GO.Length; li++)
                {
                    if (null != lDisposable_GO[li].GetComponent<Animator>())
                        lDisposable_GO[li].GetComponent<Animator>().enabled = true;
                    else
                        Destroy(lDisposable_GO[li]);
                }

                _targetLockedSprite.gameObject.SetActive(false);
                _goalRiskMesh.gameObject.SetActive(false);

                for (int li = 0; li < lNbPlayers; li++)
                    lAllPlayers[li].UI.ToggleTarget(false);
            }

            GameObject[] lRemovable_GO = GameObject.FindGameObjectsWithTag("Removable");
            for (int li = 0; li < lRemovable_GO.Length; li++)
                Destroy(lRemovable_GO[li]);

            GameObject[] lPooling_GO = GameObject.FindGameObjectsWithTag("Pooling");
            for (int li = 0; li < lPooling_GO.Length; li++)
                lPooling_GO[li].SetActive(false);
        }

        #endregion

        public  void    GetDuelActions(Transform pContainer)
        {
            _actionPrefabs.Clear();
            if (null == _duelPlayer) return;

            eTeam lTeamPossession = MatchEngine.Instance.Engine.PossessionMain.CurTeamPossession;
            List<SFPositionsActionsItemBasic> lPlayerActions = _duelPlayer.Data.GetActions(lTeamPossession == UserData.Instance.PVPTeam, false), lOppActions;
            Debug.Assert(lPlayerActions != null, "lPlayerActions can't be null");
            ActionPrefab lPrefabAction;
            ActionsItemData lActionData;
            bool lActionAvailable = false;
            Player lOpp = null;
            int lMaxScore = 0, lIndex = -1, lOppActionScore = -1;
            SFPositionsActionsItemBasic lOppAction = null;
            eAction lOppEAction = eAction.NONE;

            // Update player actions
            for (int li = 0; li < lPlayerActions.Count; li++)
            {
                if (eAction.NONE == lPlayerActions[li].Action || eAction.MOVE == lPlayerActions[li].Action)
                    continue;

                lActionData = _actionsDatabase.GetActionData(lPlayerActions[li].Action);
                lActionAvailable = (lTeamPossession == UserData.Instance.PVPTeam && lActionData.Attack && lActionData.CanUse(_isSoloAction)) || (lTeamPossession != UserData.Instance.PVPTeam && lActionData.Defense);
                lOppEAction = eAction.SPRINT != lPlayerActions[li].Action ? MatchEngine.Instance.Engine.ActionMain.GetShifumi(lPlayerActions[li].Action) : eAction.NONE;

                lMaxScore = 0;
                lIndex = -1;
                lOppAction = null;

                if (ePhase.KICK_OFF == MatchEngine.Instance.CurPhase)
                    lActionAvailable = eAction.PASS == lPlayerActions[li].Action;

                if (lActionAvailable)
                {
                    lPlayerActions[li].UpdateActionConfidence(MatchEngine.Instance.Engine, _duelPlayer.Data);
                    lPrefabAction = _getUnusedAction();

                    if (!_isSoloAction)
                    {
                        lOpp = MatchEngine.Instance.GetDuelPlayer(eTeamMethods.GetOpposite(UserData.Instance.PVPTeam));
                        lOppActions = lOpp.ActionsBasic;
                        Debug.Assert(lOppActions != null, "lOppActions can't be null");

                        lOppAction = lOppActions.Find(x => x.Action == lOppEAction);

                        if (null == lOppAction)
                        {
                            for (int lj = 0; lj < lOppActions.Count; lj++)
                            {
                                if (lOppActions[lj].WithBall == lPlayerActions[li].WithBall || eAction.SPRINT == lOppActions[lj].Action || eAction.MOVE == lOppActions[lj].Action)
                                    continue;

                                lOppActionScore = lOppActions[lj].MaxScore;

                                if (lMaxScore <= lOppActionScore)
                                {
                                    lMaxScore = lOppActionScore;
                                    lIndex = lj;
                                }
                            }
                            if (-1 < lIndex)
                                lOppAction = lOppActions[lIndex];
                        }

                        Debug.Assert(lOppAction != null, "lOppAction can't be null");

                        lOppAction.UpdateActionConfidence(MatchEngine.Instance.Engine, _duelOppPlayer.Data);
                    }

                    lPrefabAction.transform.SetParent(pContainer, false);
                    lPrefabAction.Setup(lActionData, lPlayerActions[li], lOppAction, li);
                    _actionPrefabs.Add(lPrefabAction);
                }
            }
        }

        public  void    RevealMoves()
        {
            UpdateDuelQuad(true);
            foreach (GameObject pIntention in _ownMovePreparation)
            {
                pIntention.transform.GetChild(0).GetComponent<SpriteRenderer>().enabled = false;
                pIntention.transform.GetChild(2).GetComponent<SpriteRenderer>().enabled = false;

                // Draw trajectory line
                LineRenderer lTrajectory = pIntention.transform.GetChild(5).GetComponent<LineRenderer>();
                lTrajectory.material.SetColor("_Color", _colorConfig.GetTeamColor(true));

                // Place trajectory arrow
                Transform lArrowTransform = pIntention.transform.GetChild(4);
                lArrowTransform.GetChild(0).GetComponent<SpriteRenderer>().color = _colorConfig.GetTeamColor(true);

                //pIntention.transform.GetChild(6).GetComponent<SpriteRenderer>().color = _colorConfig.GetTeamColor(true);
                pIntention.transform.GetChild(6).GetComponent<SpriteRenderer>().enabled = false;
            }

            for (int li = 0; li < _oppMovePreparation.Count; li++)
            {
                MovePreparation = _oppMovePreparation[li];
                _movePrepared(false, eTeamMethods.GetOpposite(UserData.Instance.PVPTeam).ToString(), _oppMovePosition[li]);
            }
        }

        public  void    RevealResolution()
        {
            ActionsUI.Instance.ClearResolution();
            UpdateDuelQuad(false);
            _xOffside = 100;

            _duelPlayerPressureOpps.Clear();
            _duelOppPlayerPressureOpps.Clear();

            // Reveal own team's choice
            if (null != _duelPlayer)
            {
                TeamActionResponse lOwnTeamResponse = MatchEngine.Instance.ResponseAction.TeamA;
                if (eTeam.TEAM_B == UserData.Instance.PVPTeam)
                    lOwnTeamResponse = MatchEngine.Instance.ResponseAction.TeamB;

                if (0 < lOwnTeamResponse.Players.Count)
                    _revealTeamChoice(true, lOwnTeamResponse, _duelPlayer);

                if (null != _duelPlayer)
                {
                    int lDuelPlayerIndex = lOwnTeamResponse.Players.FindIndex(x => x.Position == _duelPlayer.Data.Position.ToString());
                    foreach (PlayerActionResponse pOpp in lOwnTeamResponse.Players[lDuelPlayerIndex].PressureOpponents)
                    {
                        _duelPlayerPressureOpps.Add(MatchEngine.Instance.Engine.TeamMain.GetPlayer(pOpp.Team, pOpp.Position));
                    }
                }
            }

            // Reveal opponent team's choice
            if (null != _duelOppPlayer)
            {
                TeamActionResponse lOppTeamResponse = MatchEngine.Instance.ResponseAction.TeamB;
                if (eTeam.TEAM_B == UserData.Instance.PVPTeam)
                    lOppTeamResponse = MatchEngine.Instance.ResponseAction.TeamA;

                if (0 < lOppTeamResponse.Players.Count)
                    _revealTeamChoice(false, lOppTeamResponse, _duelOppPlayer);

                if (null != _duelOppPlayer)
                {
                    int lDuelPlayerIndex = lOppTeamResponse.Players.FindIndex(x => x.Position == _duelOppPlayer.Data.Position.ToString());
                    foreach (PlayerActionResponse pOpp in lOppTeamResponse.Players[lDuelPlayerIndex].PressureOpponents)
                    {
                        _duelOppPlayerPressureOpps.Add(MatchEngine.Instance.Engine.TeamMain.GetPlayer(pOpp.Team, pOpp.Position));
                    }
                }
            }

            _duelPlayer?.UI.ToggleBubble(false);
            _duelOppPlayer?.UI.ToggleBubble(false);

            HandleDuelPressureOpponents();

            //ActionsUI.Instance.StartResolution(!_isSoloAction);
        }

        public  void    RevealResolution(eTeam pTeam)
        {
            if (pTeam == UserData.Instance.PVPTeam)
            {
                _duelPlayerPressureOpps.Clear();
                _duelPlayer?.UI.ToggleBubble(false);

                // Reveal own team's choice
                if (null != _duelPlayer)
                {
                    TeamActionResponse lOwnTeamResponse = MatchEngine.Instance.ResponseAction.TeamA;
                    if (eTeam.TEAM_B == UserData.Instance.PVPTeam)
                        lOwnTeamResponse = MatchEngine.Instance.ResponseAction.TeamB;

                    if (0 < lOwnTeamResponse.Players.Count)
                        _revealTeamChoice(true, lOwnTeamResponse, _duelPlayer);

                    if (null != _duelPlayer)
                    {
                        int lDuelPlayerIndex = lOwnTeamResponse.Players.FindIndex(x => x.Position == _duelPlayer.Data.Position.ToString());
                        foreach (PlayerActionResponse pOpp in lOwnTeamResponse.Players[lDuelPlayerIndex].PressureOpponents)
                        {
                            _duelPlayerPressureOpps.Add(MatchEngine.Instance.Engine.TeamMain.GetPlayer(pOpp.Team, pOpp.Position));
                        }
                    }
                }
            }
            else
            {
                _duelOppPlayerPressureOpps.Clear();
                _duelOppPlayer?.UI.ToggleBubble(false);

                // Reveal opponent team's choice
                if (null != _duelOppPlayer)
                {
                    TeamActionResponse lOppTeamResponse = MatchEngine.Instance.ResponseAction.TeamB;
                    if (eTeam.TEAM_B == UserData.Instance.PVPTeam)
                        lOppTeamResponse = MatchEngine.Instance.ResponseAction.TeamA;

                    if (0 < lOppTeamResponse.Players.Count)
                        _revealTeamChoice(false, lOppTeamResponse, _duelOppPlayer);

                    if (null != _duelOppPlayer)
                    {
                        int lDuelPlayerIndex = lOppTeamResponse.Players.FindIndex(x => x.Position == _duelOppPlayer.Data.Position.ToString());
                        foreach (PlayerActionResponse pOpp in lOppTeamResponse.Players[lDuelPlayerIndex].PressureOpponents)
                            _duelOppPlayerPressureOpps.Add(MatchEngine.Instance.Engine.TeamMain.GetPlayer(pOpp.Team, pOpp.Position));
                    }
                }
            }

            HandleDuelPressureOpponents();
        }

        public  void    PreviewAction(ActionsItemData pActionData)
        {
            if (ePhase.KICK_OFF != MatchEngine.Instance.CurPhase && ePhase.ACTION != MatchEngine.Instance.CurPhase && ePhase.TACTICAL_ATTACK != MatchEngine.Instance.CurPhase)
                return;

            if (eAction.MOVE == pActionData.Type || eAction.SPRINT == pActionData.Type || eAction.PASS == pActionData.Type)
                _actionCancelerGO.SetActive(true);

            if (null != _actionInProcess && eAction.PASS == _actionInProcess.Type)
                _clearPassPreview();
            else if (null != _actionInProcess && (eAction.MOVE == _actionInProcess.Type || eAction.SPRINT == _actionInProcess.Type))
                ClearMovePreview();

            _actionInProcess    =   pActionData;

            PlayersManager.Instance.TogglePlayerCollider(eAction.PASS == pActionData.Type);

            if ((ePhase.KICK_OFF != MatchEngine.Instance.CurPhase || ePhase.ACTION == MatchEngine.Instance.CurPhase) && null != _duelPlayer)
                ActionsUI.Instance.UpdateDuelActions(false);

            if (eAction.MOVE == _actionInProcess.Type)
                PlayersManager.Instance.TogglePlayerSelection(UserData.Instance.PVPTeam, false);

            Player lPlayerHasBall = MatchEngine.Instance.Engine.PossessionMain.GetPlayerWithBall();

            switch (pActionData.Type)
            {
                case eAction.PASS:
                    ActionsUI.Instance.UpdateBoardMessage("pass_in_progress");
                    _duelPlayer?.UI.ToggleBubble(true, false);
                    _passSimulation = MatchEngine.Instance.SimulationPass(lPlayerHasBall);
                    _duelOppPlayer?.UI.ToggleBubble(false);

                    if (_isTester && null != MatchTutorial.Instance && MatchTutorial.Instance.TutorialData.CounterActionPhase <= MatchTutorial.Instance.TutorialData.ActionPhases.Length - 1)
                        MatchTutorial.Instance.PassPreview();
                    else
                        _passPreview();
                    break;

                case eAction.MOVE:
                case eAction.SPRINT:
                    if (eAction.SPRINT == pActionData.Type)
                        _duelPlayer?.UI.ToggleBubble(true, false);

                    if (ePhase.TACTICAL_ATTACK == MatchEngine.Instance.CurPhase)
                        MatchCamera.Instance.FocusOnPlayer(_playerFocusedRef.CurrentValue.gameObject);

                    MatchEngine.Instance.SelectPlayer(_playerFocusedRef.CurrentValue.Data);
                    _moveSimulation = MatchEngine.Instance.SimulationMove(_playerFocusedRef.CurrentValue.Data);

                    if (_isTester && null != MatchTutorial.Instance && MatchTutorial.Instance.TutorialData.CounterActionPhase <= MatchTutorial.Instance.TutorialData.ActionPhases.Length - 1)
                        MatchTutorial.Instance.MovePreview();
                    else
                        _movePreview();
                    break;

                default:
                    //_duelPlayer?.UI.ToggleBubble(true, false);
                    break;
            }

            UtilityMethods.InvokeGameEvent(_projectionInProgressEvt);
        }

        public  void    ValidatePlayer(PlayerPVP pPlayer)
        {
            _playerInProcess = pPlayer.Data;
            _fieldCaseInProgress = pPlayer.Data.CurFieldCase;
        }

        public void ValidateFieldCase(FieldCase pFieldCase)
        {
            _fieldCaseInProgress = pFieldCase;
        }

        public void PrepareAction(ActionsItemData pActionData)
        {
            if (ePhase.ACTION != MatchEngine.Instance.CurPhase && ePhase.KICK_OFF != MatchEngine.Instance.CurPhase && ePhase.TACTICAL_ATTACK != MatchEngine.Instance.CurPhase)
                return;

            if (ePhase.ACTION == MatchEngine.Instance.CurPhase && eAction.LONG_KICK == pActionData.Type)
            {
                MatchAudio.Instance.OnTargetValidated();
                ValidateFieldCase(_isManagerTeamA ? new FieldCase(53, 16) : new FieldCase(0, 16));
            }

            _actionInProcess = pActionData;

            if (pActionData.Type != eAction.MOVE)
            {
                // Handle basic action preparation
                MatchEngine.Instance.PrepareAction(
                    eExecutorMethods.GetEExecutor(UserData.Instance.PVPTeam, false),
                    pActionData.Type,
                    _duelPlayer.Data.Position,
                    (pActionData.Type == eAction.PASS || pActionData.Type == eAction.LONG_KICK || eAction.SPRINT == pActionData.Type) ? _fieldCaseInProgress : null
                );
            }
            else
            {
                // Handle tactical action preparation
                MatchEngine.Instance.PrepareAction(
                    eExecutorMethods.GetEExecutor(UserData.Instance.PVPTeam, false),
                    pActionData.Type,
                    _playerFocusedRef.CurrentValue.Data.Position,
                    _fieldCaseInProgress
                );
            }
        }

        public void CancelActionInProcess()
        {
            if (null == _actionInProcess)
                return;

            if (null != _actionInProcess && eAction.PASS == _actionInProcess.Type)
                _clearPassPreview();
            else if (null != _actionInProcess && eAction.LONG_KICK == _actionInProcess.Type)
                _clearLongKickPreview();
            else if (null != _actionInProcess && (eAction.MOVE == _actionInProcess.Type || eAction.SPRINT == _actionInProcess.Type))
                ClearMovePreview();

            _actionInProcess = null;
            _playerInProcess = null;
            _fieldCaseInProgress = null;

            _duelPlayer?.UI.ToggleBubble(ePhase.ACTION == MatchEngine.Instance.CurPhase);
            PlayersManager.Instance.TogglePlayerCollider(true);

            if (ePhase.TACTICAL_ATTACK == MatchEngine.Instance.CurPhase)
                _handleSelectablePlayers();
        }

        public void UpdateOpportunityZone()
        {
            if (MatchEngine.Instance.Engine.PhaseMain.GoalOpportunityZone)
            {
                bool lCurPossess = UserData.Instance.PVPTeam == _curTeamPossess;
                _goalOpportunityGO.transform.localPosition = new Vector3(lCurPossess ? 2.918f : -3.05f, 0.01f, -1.626f);
                _goalOpportunityGO.SetActive(true);
            }
            else
                _goalOpportunityGO.SetActive(false);
        }

        public void DisplayReceptionQuad()
        {
            ActionsUI.Instance.ClearResolution();
            PassResolutionResponse lResolution = new PassResolutionResponse();
            lResolution.Parse(MatchEngine.Instance.Engine.ActionMain.GetPlayerResponseWithAction(eAction.PASS).Resolution);

            if (_curTeamPossess == UserData.Instance.PVPTeam)
            {
                _duelPlayer = PlayersManager.Instance.GetPlayer(lResolution.Receiver.Team, lResolution.Receiver.Position);
                if (null != lResolution.ReceiverOpponentDuel)
                    _duelOppPlayer = PlayersManager.Instance.GetPlayer(lResolution.ReceiverOpponentDuel.Team, lResolution.ReceiverOpponentDuel.Position);
                else
                    _duelOppPlayer = null;
            }
            else
            {
                _duelOppPlayer = PlayersManager.Instance.GetPlayer(lResolution.Receiver.Team, lResolution.Receiver.Position);
                if (null != lResolution.ReceiverOpponentDuel)
                    _duelPlayer = PlayersManager.Instance.GetPlayer(lResolution.ReceiverOpponentDuel.Team, lResolution.ReceiverOpponentDuel.Position);
                else
                    _duelPlayer = null;
            }

            _isSoloAction = null == _duelPlayer || null == _duelOppPlayer;

            UpdateDuelQuad(false);

            _duelPlayer?.UI.TogglePlayerName(false);
            _duelOppPlayer?.UI.TogglePlayerName(false);
        }

        public void HandleDuelPressureOpponents()
        {
            GameObject lPressure = null;
            PlayerPVP lOpponent = null;

            if (!_isSoloAction && _curTeamPossess == UserData.Instance.PVPTeam)
            {
                _duelOppPlayer.Movements.ShouldFaceBall = false;
                _duelOppPlayer.Movements.OrientPlayerTowardsTarget(_duelPlayer.transform.position);
            }
            else if (!_isSoloAction && _curTeamPossess != UserData.Instance.PVPTeam)
            {
                _duelPlayer.Movements.ShouldFaceBall = false;
                _duelPlayer.Movements.OrientPlayerTowardsTarget(_duelOppPlayer.transform.position);
            }

            // Handle own player pressure
            if (0 < _duelPlayerPressureOpps.Count)
            {
                foreach (Player pPlayer in _duelPlayerPressureOpps)
                {
                    if (pPlayer.IsEqualTo(_duelOppPlayer?.Data))
                        break;

                    lOpponent = PlayersManager.Instance.GetPlayer(pPlayer.Team, pPlayer.Position);

                    lOpponent.Movements.ShouldFaceBall = false;
                    lOpponent.Movements.OrientPlayerTowardsTarget(_duelPlayer.transform.position);
                    lOpponent.Animator.ToggleDuelAnim(true);

                    lPressure = Instantiate(_pressurePrefab);
                    lPressure.name = lOpponent.name + "_Pressure_Direction";
                    lPressure.tag = "Removable";
                    lPressure.transform.position = lOpponent.transform.position;
                    lPressure.transform.LookAt(_duelPlayer.transform);
                    lPressure.SetActive(true);

                    GameObject pressureIdle_Gob = GameObject.Find(lPressure.name + "/Pressure_Grp/Pressure_Idle");
                    pressureIdle_Gob.GetComponent<SpriteRenderer>().enabled = true;
                    pressureIdle_Gob.GetComponent<Animator>().enabled = true;
                    pressureIdle_Gob.GetComponent<SpriteRenderer>().sprite = UtilityMethods.GetSprite(_spriteConfig, _isManagerTeamA ? "PressureArrowRed" : "PressureArrowBlue");
                }
            }

            // Handle player B pressure
            if (0 < _duelOppPlayerPressureOpps.Count)
            {
                foreach (Player pPlayer in _duelOppPlayerPressureOpps)
                {
                    if (pPlayer.IsEqualTo(_duelPlayer?.Data))
                        break;

                    lOpponent = PlayersManager.Instance.GetPlayer(pPlayer.Team, pPlayer.Position);

                    lOpponent.Movements.ShouldFaceBall = false;
                    lOpponent.Movements.OrientPlayerTowardsTarget(_duelOppPlayer.transform.position);
                    lOpponent.Animator.ToggleDuelAnim(true);

                    lPressure = Instantiate(_pressurePrefab);
                    lPressure.name = lOpponent.name + "_Pressure_Direction";
                    lPressure.tag = "Removable";
                    lPressure.transform.position = lOpponent.transform.position;
                    lPressure.transform.LookAt(_duelOppPlayer.transform);
                    lPressure.SetActive(true);

                    GameObject pressureIdle_Gob = GameObject.Find(lPressure.name + "/Pressure_Grp/Pressure_Idle");
                    pressureIdle_Gob.GetComponent<SpriteRenderer>().enabled = true;
                    pressureIdle_Gob.GetComponent<Animator>().enabled = true;
                    pressureIdle_Gob.GetComponent<SpriteRenderer>().sprite = UtilityMethods.GetSprite(_spriteConfig, _isManagerTeamA ? "PressureArrowBlue" : "PressureArrowRed");
                }
            }
        }

        public void UpdateDuelInfos()
        {
            _duelPlayer?.UI.UpdatePlayerPossession(false);
            _duelOppPlayer?.UI.UpdatePlayerPossession(false);

            _duelPlayer = PlayersManager.Instance.GetDuelPlayer(UserData.Instance.PVPTeam);
            _duelOppPlayer = PlayersManager.Instance.GetDuelPlayer(eTeamMethods.GetOpposite(UserData.Instance.PVPTeam));

            _duelPlayerPressureOpps.Clear();
            _duelOppPlayerPressureOpps.Clear();

            if (null != _duelPlayer)
                _duelPlayerPressureOpps = MatchEngine.Instance.Engine.OppositionMain.GetPressureOpponentsPlayer(_duelPlayer.Data);
            if (null != _duelOppPlayer)
                _duelOppPlayerPressureOpps = MatchEngine.Instance.Engine.OppositionMain.GetPressureOpponentsPlayer(_duelOppPlayer.Data);

            _isSoloAction = null == _duelPlayer || null == _duelOppPlayer;
            _curTeamPossess = MatchEngine.Instance.Engine.PossessionMain.CurTeamPossession;

            _placeDuelQuad();
            ToggleDuelQuad(true);

            _playerFocusedRef.CurrentValue = null;
        }

        public void ToggleDuelQuad(bool pActive)
        {
            _duelQuadRenderers[0].transform.parent.gameObject.SetActive(pActive);
        }

        public void UpdateDuelQuad(bool pTacticalView)
        {
            // Set sprites & colors
            int lNbSprites = _duelQuadRenderers.Length;
            for (int li = 0; li < lNbSprites; li++)
            {
                _duelQuadRenderers[li].sprite = UtilityMethods.GetSprite(_spriteConfig, "Square_Thin" + (_isSoloAction ? "" : "_colored"));

                if (_isSoloAction)
                    _duelQuadRenderers[li].color = _colorConfig.GetTeamColor(UserData.Instance.PVPTeam == _curTeamPossess);
                else
                    _duelQuadRenderers[li].color = Color.white;

                if (li > 0)
                    _duelQuadRenderers[li].enabled = !pTacticalView;
            }

            // Launch appropriate animation
            _duelQuadRenderers[0].transform.parent.GetComponent<Animation>().Play("BallPossession_" + (_isSoloAction ? "Solo" : "Duel"));
        }

        public FieldCaseButton FindSelectableFC()
        {
            if (!_fieldCaseOutlines[0].FcHovered)
                return _fieldCaseOutlines[0];

            for (int li = 0; li < _fieldCaseOutlines.Length; li++)
            {
                if (!_fieldCaseOutlines[li].FcHovered)
                    return _fieldCaseOutlines[li];
            }

            return _fieldCaseOutlines[1];
        }

        public bool CanSkipPhase()
        {
            if (ePhase.TACTICAL_ATTACK == MatchEngine.Instance.CurPhase)
            {
                if (_skipPhase)
                    return true;

                return false;
            }


            bool lTeamAIsDone = MatchEngine.Instance.HasAction(eTeam.TEAM_A);
            bool lTeamBIsDone = MatchEngine.Instance.HasAction(eTeam.TEAM_B);

            if (_skipPhase && eTeam.TEAM_A == UserData.Instance.PVPTeam)
                lTeamAIsDone = true;
            if (_skipPhase && eTeam.TEAM_B == UserData.Instance.PVPTeam)
                lTeamBIsDone = true;

            if (_isSoloAction && _curTeamPossess == eTeam.TEAM_A)
                lTeamBIsDone    =   true;
            if (_isSoloAction && _curTeamPossess == eTeam.TEAM_B)
                lTeamAIsDone    =   true;

            return (lTeamAIsDone && lTeamBIsDone);
        }

        public void DrawOffside(int pX)
        {
            _xOffside = _xOffside > pX ? (ushort)pX : _xOffside;
            _offsideLine.SetPosition(0, PlayersManager.Instance.CalculatePositionByCoordinates(new FieldCase(_xOffside, 0), false));
            _offsideLine.SetPosition(1, PlayersManager.Instance.CalculatePositionByCoordinates(new FieldCase(_xOffside, 33), false));
            _offsideLine.enabled = true;
            ActionsUI.Instance.ToggleOffside(true, new FieldCase(_xOffside, 33));
        }

        public void Start()
        {
            _isManagerTeamA = UserData.Instance.PVPTeam == eTeam.TEAM_A || UserData.Instance.PVPTeam == eTeam.NONE;

            if (null != MatchEngine.Instance && null != MatchEngine.Instance.Engine)
                MatchEngine.Instance.Engine.PhaseMain.OnPhaseChanged += pPhase => OnPhaseChanged(pPhase);
        }

        #region Clear Methods

        public void ClearSimulation()
        {
            if (null == _actionInProcess)
                return;
        }

        public void ClearBeforeResolution()
        {
            GameObject[] lDisposable_GO = GameObject.FindGameObjectsWithTag("Disposable");
            for (int li = 0; li < lDisposable_GO.Length; li++)
                if (!lDisposable_GO[li].name.Contains("MOVE_"))
                    Destroy(lDisposable_GO[li]);

            for (int li = 0; li < _fieldCaseOutlines.Length; li++)
                _fieldCaseOutlines[li].Hide();

            _targetSprite.transform.parent.gameObject.SetActive(false);
            _goalMesh.GetComponent<Animator>().enabled = false;
            _goalMesh.GetComponent<Animator>().WriteDefaultValues();

            ClearPassTrajectories();

            if (null != PassPreparation)
                for (int li = 0; li < PassPreparation.FieldCasesPath.Length; li++)
                    SoccerFieldUI.Instance.HighlightFieldCase(PassPreparation.FieldCasesPath[li], false);
        }

        public void ClearField()
        {
            _minionSelectedAnim.gameObject.SetActive(false);

            _actionInProcess = null;
            _playerInProcess = null;

            _targetSprite.transform.parent.gameObject.SetActive(false);
            _duelQuadRenderers[0].transform.parent.gameObject.SetActive(false);

            for (int li = 0; li < 4; li++)
                _fieldCaseOutlines[li].Hide();
        }

        #endregion

        #endregion

        #region Private Methods

        private void    Update()
        {
            // Make minion selected animation follow player selected
            if (_minionSelectedAnim.gameObject.activeSelf)
            {
                if (null != _playerFocusedRef.CurrentValue)
                    _minionSelectedAnim.transform.position = _playerFocusedRef.CurrentValue.transform.position;
                else if (null != _duelPlayer)
                    _minionSelectedAnim.transform.position = _duelPlayer.transform.position;
            }

            // Make duel quad follow duel players / ball player
            if (null != UserData.Instance)
                _placeDuelQuad();

            if (null != _targetAnchor && _targetSprite.isVisible)
                _targetSprite.transform.position = _targetAnchor.position;
            if (null != _targetAnchorBis && _targetLockedSprite.isVisible)
                _targetLockedSprite.transform.position = _targetAnchorBis.position;
        }

        private void    _handleSkip()
        {
            if ((ePhase.ACTION == MatchEngine.Instance.CurPhase || ePhase.TACTICAL_ATTACK == MatchEngine.Instance.CurPhase || ePhase.KICK_OFF == MatchEngine.Instance.CurPhase) && CanSkipPhase())
            {
                _duelFinished = true;
                ActionsUI.Instance.ToggleActionIndications(false);
                MatchTimers.Instance.StopCurrentTimer();

                _duelPlayer?.UpdateHasAction(false);
                _duelOppPlayer?.UpdateHasAction(false);
            }
        }

        private void _handleSelectablePlayers()
        {
            if (_isTester && null != MatchTutorial.Instance && MatchTutorial.Instance.TutorialData.CounterActionPhase <= MatchTutorial.Instance.TutorialData.ActionPhases.Length - 1)
                return;

            PlayerPVP[] lAllPlayers = PlayersManager.Instance.GetAllPlayers();
            List<string> lPlayersMoved = _isManagerTeamA ? MatchEngine.Instance.GetResponse().TacticalMovePositionsTeamA : MatchEngine.Instance.GetResponse().TacticalMovePositionsTeamB;
            int lNbPlayers = lAllPlayers.Length;

            for (int li = 0; li < lNbPlayers; li++)
            {
                lAllPlayers[li].IsClickable = lAllPlayers[li].IsBlueTeam
                                                && !MatchEngine.Instance.TeamIsDone(UserData.Instance.PVPTeam)
                                                && !lPlayersMoved.Contains(lAllPlayers[li].Data.Position.ToString())
                                                && ePosition.G != lAllPlayers[li].Data.Position
                                                && !lAllPlayers[li].Data.IsEqualTo(_duelPlayer?.Data);
                lAllPlayers[li].Player3D.TogglePlayerCollider(
                    (lAllPlayers[li].IsClickable || ePosition.G == lAllPlayers[li].Data.Position)
                    && lAllPlayers[li].IsBlueTeam
                    && (null == _actionInProcess || eAction.MOVE != _actionInProcess.Type)
                    && !lAllPlayers[li].Data.IsEqualTo(_duelPlayer?.Data)
                );
                lAllPlayers[li].UI.ToggleSelection(
                    lAllPlayers[li].IsClickable
                    && lAllPlayers[li].IsBlueTeam
                    && (null == _playerFocusedRef.CurrentValue || !lAllPlayers[li].Data.IsEqualTo(_playerFocusedRef.CurrentValue.Data))
                );
                lAllPlayers[li].UI.ToggleForbiddenSelection(
                    lAllPlayers[li].IsBlueTeam
                    && ePosition.G == lAllPlayers[li].Data.Position
                );
                lAllPlayers[li].Player3D.TogglePlayerActive(true);
            }
        }

        private void _placeDuelQuad()
        {
            _duelQuadRenderers[0].transform.parent.localEulerAngles = new Vector3(90f, 0f, 0f);

            if (null != _duelPlayer && null != _duelOppPlayer)
            {
                _duelQuadRenderers[0].transform.parent.position = (_duelPlayer.transform.position + _duelOppPlayer.transform.position) / 2f;
                float lDiffZ = _duelOppPlayer.transform.position.z - _duelPlayer.transform.position.z;
                float lDiffX = _duelOppPlayer.transform.position.x - _duelPlayer.transform.position.x;
                float lY = 0f;
                float lZ = 0f;
                if (Mathf.Abs(lDiffZ) > 0.05f)
                {
                    lZ = lDiffZ > 0f ? 90f : -90f;
                }
                if (Mathf.Abs(lDiffX) > 0.05f)
                {
                    lY = lDiffX > 0 ? 0f : -180f;
                    //if (_curTeamPossess != UserData.Instance.PVPTeam)
                    //    lY = lDiffX > 0 ? -180f : 0f;
                }

                _duelQuadRenderers[0].transform.parent.localEulerAngles = new Vector3(90f, lY, lZ);
            }
            else if (null != _duelPlayer)
                _duelQuadRenderers[0].transform.parent.position = _duelPlayer.transform.position;
            else if (null != _duelOppPlayer)
                _duelQuadRenderers[0].transform.parent.position = _duelOppPlayer.transform.position;
        }

        private void _revealTeamChoice(bool pCurManager, TeamActionResponse pTeamResponse, PlayerPVP pDuelPlayer)
        {
            int lDuelPlayerIndex = pTeamResponse.Players.FindIndex(x => x.Position == pDuelPlayer.Data.Position.ToString());
            int lDrawLevel = pTeamResponse.Players[lDuelPlayerIndex].ExecuteAction.DrawLevel;

            switch (eActionMethods.ConvertStringToEnum(pTeamResponse.Players[lDuelPlayerIndex].Action))
            {
                case eAction.PASS:
                    PassPreparation.Parse(MatchEngine.Instance.Engine, pTeamResponse.Players[lDuelPlayerIndex].Preparation);
                    _passPrepared(pTeamResponse, lDuelPlayerIndex);
                    break;

                case eAction.LONG_KICK:
                    _longKickPreparation.Parse(pTeamResponse.Players[lDuelPlayerIndex].Preparation);
                    lDrawLevel = _longKickPreparation.Stricker.DrawLevel;

                    _longKickPrepared(_longKickPreparation.ConvertToJson());
                    break;

                case eAction.SPRINT:
                    SprintPreparation.Parse(pTeamResponse.Players[lDuelPlayerIndex].Preparation);
                    GameObject lIntentionGO = GameObject.Find("MOVE_" + pTeamResponse.Players[lDuelPlayerIndex].Team + "-" + pTeamResponse.Players[lDuelPlayerIndex].Position);
                    if (pCurManager && null != lIntentionGO)
                    {
                        lIntentionGO.transform.GetChild(0).GetComponent<SpriteRenderer>().enabled = true;
                        lIntentionGO.transform.GetChild(2).GetComponent<SpriteRenderer>().enabled = false;

                        // Draw trajectory line
                        LineRenderer lTrajectory = lIntentionGO.transform.GetChild(5).GetComponent<LineRenderer>();
                        lTrajectory.material.SetColor("_Color", _colorConfig.GetTeamColor(true));

                        // Place trajectory arrow
                        Transform lArrowTransform = lIntentionGO.transform.GetChild(4);
                        lArrowTransform.GetChild(0).GetComponent<SpriteRenderer>().color = _colorConfig.GetTeamColor(true);

                        lIntentionGO.transform.GetChild(6).GetComponent<SpriteRenderer>().color = _colorConfig.GetTeamColor(true);
                    }
                    else
                        _sprintPrepared(pCurManager, pTeamResponse.Team, pTeamResponse.Players[lDuelPlayerIndex].Position);
                    break;

                default:
                    break;
            }

            //if (ePhase.PERIOD_INIT != MatchEngine.Instance.CurPhase && !_isSoloAction && lDuelPlayerIndex != -1)
            //    ActionsUI.Instance.RevealAction(
            //        pCurManager,
            //        eActionMethods.ConvertStringToEnum(pTeamResponse.Players[lDuelPlayerIndex].Action),
            //        (int)pTeamResponse.Players[lDuelPlayerIndex].ExecuteAction.MinScore,
            //        (int)pTeamResponse.Players[lDuelPlayerIndex].ExecuteAction.MaxScore
            //    );
        }

        private ActionPrefab _getUnusedAction()
        {
            if (null == _pool)
            {
                _pool = new ActionPool(_actionPrefab, ActionsManager.Instance.transform, 5);
            }

            return _pool.GetUnusedAction();
        }

        private IEnumerator _botPrepareAction(eTeam pTeam)
        {
            int     lActionCounter  =   null != _duelOppPlayer ? 1 : 0;
            float   lDecisionTime   =   UnityEngine.Random.Range(1f, 3.5f);
            Dictionary<string, object> lResponse = new Dictionary<string, object>();

            if (ePhase.TACTICAL_ATTACK == MatchEngine.Instance.CurPhase)
                lActionCounter = 3;
            else if (ePhase.KICK_OFF == MatchEngine.Instance.CurPhase)
                lActionCounter = 0;

            Debug.Log("_botPrepareAction " + pTeam + " : " + lActionCounter + " actions");
            for (int li = 0; li < lActionCounter; li++)
            {
                lResponse = MatchEngine.Instance.Engine.AIMain.PrepareAction(pTeam);

                yield return new WaitForSeconds(lDecisionTime);

                if (eAction.MOVE == (eAction)lResponse["Action"] && null != (FieldCase)lResponse["FieldCaseTarget"])
                    MatchEngine.Instance.PrepareAction(eExecutorMethods.GetEExecutor(pTeam, true), (eAction)lResponse["Action"], (ePosition)lResponse["Position"], (FieldCase)lResponse["FieldCaseTarget"]);
                else if (eAction.NONE != (eAction)lResponse["Action"] && eAction.MOVE != (eAction)lResponse["Action"])
                    MatchEngine.Instance.PrepareAction(eExecutorMethods.GetEExecutor(pTeam, true), (eAction)lResponse["Action"], (ePosition)lResponse["Position"], (FieldCase)lResponse["FieldCaseTarget"]);
            }

            yield return null;
        }

        #endregion
    }
}
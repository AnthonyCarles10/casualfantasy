﻿using Sportfaction.CasualFantasy.Manager.Profile;
using Sportfaction.CasualFantasy.PVP.Gameplay.Matchs;

using SportFaction.CasualFootballEngine.TeamService.Enum;

using UnityEngine;

namespace Sportfaction.CasualFantasy.PVP.Gameplay.FX
{
    /// <summary>
    /// Class handling FXs
    /// </summary>
    /// @author Madeleine Tranier
    public sealed class FXManager : Singleton<FXManager>
	{
		#region Fields

		[Header("3D Elements")]
		[SerializeField, Tooltip("Victory GO")]
		private	GameObject	_victoryMsgGO	=	null; //!< Victory FX
        [SerializeField, Tooltip("Fog Outro GO")]
        private GameObject  _fogOutroGO     =   null; //!< Fog Outro FX

		#endregion

		#region Public Methods

		#region Game Event Listeners

        public  void    DisplayFogOutro()
        {
            _fogOutroGO.SetActive(true);
		}

		public  void    DisplayResultFX()
		{
			bool    lIsManagerTeamA =   eTeam.TEAM_A == UserData.Instance.PVPTeam;
			float   lScoreA         =   MatchEngine.Instance.Engine.ScoreMain.TeamAScore;
			float   lScoreB         =   MatchEngine.Instance.Engine.ScoreMain.TeamBScore;

			// Determine match result (win / draw / loss)
			bool lIsWinner = (lIsManagerTeamA && lScoreA > lScoreB) || (!lIsManagerTeamA && lScoreB > lScoreA);

			if (lIsWinner)
				_victoryMsgGO.SetActive(true);
		}

		#endregion

		#endregion
	}
}
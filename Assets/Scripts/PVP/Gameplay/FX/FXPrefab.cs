﻿using System.Collections;
using UnityEngine;

namespace Sportfaction.CasualFantasy.PVP.Gameplay.FX
{
    /// <summary>
    /// Class handling FX behaviour
    /// </summary>
    public class FXPrefab : MonoBehaviour
    {
        #region Fields
        
        [SerializeField, Tooltip("FX")]
        private ParticleSystem  _fx =   null; //!< ParticleSystem to control
        
        [Header("Options")]
        [SerializeField, Tooltip("Auto disable")]
        private	bool    _autoDisable    =   true; //!< Should FX be disabled automatically when animation's over ?
        [SerializeField, Tooltip("Auto pause")]
        private	bool    _autoPause      =   false; //!< Should FX be paused automatically at specified time ?
        [SerializeField, Tooltip("Pause time")]
        private	float	_pauseTime      =   0f; //!< Time of FX animation to pause at
        
        private ParticleSystem[]    _subParticles; //!< Array of sub-particles
        
        #endregion
        
        #region Public Methods
        
        /// <summary>
        /// Resumes FX animation
        /// </summary>
        public	void	Resume()
        {
            _fx.Play();

            for (int i = 0; i < _subParticles.Length; i++)
            {
                if (_subParticles[i] != null)
                    _subParticles[i].Play();
            }

            StartCoroutine(_disable(1f));
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Retrieves every children particles
        /// </summary>
        private void    Awake()
        {
            _subParticles	=	GetComponentsInChildren<ParticleSystem>();
        }
        
        /// <summary>
        /// Plays FX automatically when GO is active
        /// </summary>
        private	void    OnEnable()
        {
            _fx.Clear();
            _fx.Play();

            for (int li = 0; li < _subParticles.Length; li++)
            {
                _subParticles[li].Clear();
                _subParticles[li].Play();
            }
            
            if (_autoDisable)
                StartCoroutine(_disable(_fx.main.duration));
            if (_autoPause)
                StartCoroutine(_pause(_pauseTime));
        }

        #region Coroutines 
        
        /// <summary>
        /// Disables FX after specified time
        /// </summary>
        /// <param name="pWaitingTime">Waiting time before disabling</param>
        private IEnumerator	_disable(float pWaitingTime)
        {
            yield return new WaitForSeconds(pWaitingTime);

            this.gameObject.SetActive(false);
        }
        
        /// <summary>
        /// Pauses FX animation after specified time
        /// </summary>
        /// <param name="pWaitingTime">Waiting time before pausing</param>
        private IEnumerator _pause(float pWaitingTime)
        {
            yield return new WaitForSeconds(pWaitingTime);
            
            _fx.Pause();
            
            for (int i = 0; i < _subParticles.Length; i++)
                _subParticles[i].Pause();
        }
        
        #endregion
        
        #endregion
    }
}
﻿using Sportfaction.CasualFantasy.Manager.Profile;
using Sportfaction.CasualFantasy.PVP.Gameplay.Actions;
using Sportfaction.CasualFantasy.PVP.Gameplay.Matchs;

using SportFaction.CasualFootballEngine.FieldService.Model;
using SportFaction.CasualFootballEngine.PeriodService.Enum;
using SportFaction.CasualFootballEngine.PhaseService.Enum;
using SportFaction.CasualFootballEngine.PlayerService.Enum;
using SportFaction.CasualFootballEngine.PlayerService.Model;
using SportFaction.CasualFootballEngine.TeamService.Enum;
using SportFaction.CasualFootballEngine.TeamService.Model;
using SportFaction.CasualFootballEngine.Utilities;

using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

namespace Sportfaction.CasualFantasy.PVP.Gameplay.Players
{
    /// <summary>
    /// Class handling initialization & access to every PlayerPVP script in scene
    /// </summary>
    /// @author Madeleine Tranier
    public sealed class PlayersManager : Singleton<PlayersManager>
    {
        #region Properties

        public  int     NbPlayersToMove     { set { _nbPlayersToMove = value; } }
        public  bool    WaitForPlacement    { set { _waitForPlacement = value; } }

        #endregion

        #region Serialized Fields

        [SerializeField, Tooltip("Should players be initialized on start ?")]
        private bool    _initializeOnStart  =   true; //!< Should engine be initialized on scene start ?

        [Header("Links")]
        [SerializeField, Tooltip("Soccer ball handler")]
        private SoccerBall  _ballHandler    =   null; //!< Class handling ball movements

        [Header("3D elements")]
        [SerializeField, Tooltip("Team transform")]
        private RectTransform   _teamTransform  =   null; //!< Players parent container
        [SerializeField, Tooltip("Anchor point")]
        private Transform       _anchorPoint    =   null; //!< Transform used to convert calculated local position to world position

        [Header("Team Players GO")]
        [SerializeField, Tooltip("All players of team Manager 1")]
        private PlayerPVP[] _teamBluePlayers    =   new PlayerPVP[0]; //!< Array of every blue player's script
        [SerializeField, Tooltip("All players of team Manager 2")]
        private PlayerPVP[] _teamYellowPlayers  =   new PlayerPVP[0]; //!< Array of every red player's script

        #endregion

        #region Private Fields

        private List<Sprite>    _sprites            =   new List<Sprite>();
        private bool            _isManagerTeamA     =   true;
        private int             _nbPlayersToMove    =   0;
        private bool            _waitForPlacement   =   false;
        private int             _nbPlayers          =   0;
        private PlayerPVP[]     _array;
        private float           _distToMove         =   -1f;

        #endregion

        #region Public Methods

        #region Game Event Listeners

        public  void    OnEngineStartPeriod()
        {
            //PlaceReferee(false, ActionsManager.Instance.TimingConfig.DurationTeamPlacement);
            _array = GetAllPlayers();
            _nbPlayers = _array.Length;

            for (int li = 0; li < _nbPlayers; li++)
            {
                _array[li].Animator.ToggleDuelAnim(false);
                _array[li].UI.TogglePlayerName(true);
            }

            TogglePlayerCollider(true);
        }

        public  void    OnEngineStopPeriod()
        {
            TogglePlayerCollider(false);
            ToggleHoveredPlayerName(false);
        }

        public  void    OnEngineChangeFieldSetup()
        {
            if ("FS_A_KICK_OFF" == MatchEngine.Instance.Engine.FieldSetupMain.CurrentFieldSetup && ePeriod.FULL_TIME != MatchEngine.Instance.Engine.PeriodMain.Period)
            {
                SoccerBall.Instance.FollowPlayer(false);
                SoccerBall.Instance.MoveToCenter();
                PlacePlayers();
            }
        }

        public  void    OnPhaseChanged(ePhase pPhase)
        {
            switch (pPhase)
            {
                case ePhase.PRE_TOSS:
                    PlacePlayers();
                    break;

                case ePhase.TOSS_INIT:
                    StartCoroutine(MatchManager.Instance.NextGamePhase(0f));
                    PlacePlayers();
                    break;

                case ePhase.KICK_OFF:
                    SoccerBall.Instance.FollowPlayer(false);
                    SoccerBall.Instance.MoveToCenter();
                    PlacePlayers(false, true, true);
                    break;

                default:
                    break;
            }
        }

        #endregion

        public  void    InitPlayers()
        {
            _isManagerTeamA = eTeam.TEAM_A == UserData.Instance.PVPTeam;
            _teamTransform.localEulerAngles = new Vector3(90f, _isManagerTeamA ? 0f : 180f, 0f);

            _initializeSprites();

            PlayerPVP[] lAllPlayers =   GetAllPlayers();
            int         lNbPlayers  =   lAllPlayers.Length;

            for (int li = 0; li < lNbPlayers; li++)
                lAllPlayers[li].gameObject.SetActive(false);

            _initTeamA();
            _initTeamB();
        }

        public  void    PlacePlayers(bool pInstantaneous = false, bool pPlaceBall = false, bool pWaitForPlacement = false)
        {
            PlayerPVP[] lAllPlayers =   GetAllPlayers();
            int         lNbPlayers  =   lAllPlayers.Length;

            _nbPlayersToMove    =   lAllPlayers.Length;
            _waitForPlacement   =   pWaitForPlacement;
            _distToMove         =   -1f;

            for (int li = 0; li < lNbPlayers; li++)
                lAllPlayers[li].Movements.MovePlayer(pInstantaneous);

            if (pPlaceBall)
                SoccerBall.Instance.FollowPlayer(true, GetBallPossesser());

            if (_distToMove > 0f)
                MatchAudio.Instance.PlayerRun(true);
        }

        public  Vector3 CalculatePositionByCoordinates(FieldCase pFieldCase, bool pCenter = true, bool pWorldPos = true)
        {
		    Assert.IsNotNull(pFieldCase, "CalculatePositionByCoordinates() failed, pFieldCase is null.");

            float   lFieldWidth     =   Constants.SoccerFieldWidth;
            float   lFieldHeight    =   Constants.SoccerFieldHeight;
            float   lOffsetX        =   _teamTransform.sizeDelta.x / lFieldWidth;
            float   lOffsetY        =   _teamTransform.sizeDelta.y / lFieldHeight;

            float   lX  =   (pFieldCase.X * lOffsetX) + (pCenter ? (lOffsetX / 2f) : 0) - (_teamTransform.sizeDelta.x / 2f);
            float   lY  =   (pFieldCase.Y * lOffsetY) + (pCenter ? (lOffsetY / 2f) : 0) - (_teamTransform.sizeDelta.y / 2f);

            _anchorPoint.localPosition = new Vector3(lX, lY, 0f);

            return pWorldPos ? _anchorPoint.position : _anchorPoint.localPosition;
        }
        
        public  void    UpdatePlayersToMove()
        {
            _nbPlayersToMove--;

            if (0 == _nbPlayersToMove && _waitForPlacement)
            {
                ePhase lNextPhase = MatchEngine.Instance.Engine.PhaseMain.GetNextPhase();

                if (ePhase.KICK_OFF == MatchEngine.Instance.CurPhase)
                    SoccerBall.Instance.FollowPlayer(true, GetBallPossesser());

                if (ePhase.TOSS == lNextPhase)
                    StartCoroutine(MatchManager.Instance.NextGamePhase(0f));
                else if (ePhase.PERIOD_INIT == lNextPhase)
                    MatchEngine.Instance.LaunchPreparationPhase();
                else
                    MatchManager.Instance.NextPhase();

                _waitForPlacement = false;
            }

            if (0 == _nbPlayersToMove && null != MatchAudio.Instance)
            {
                MatchAudio.Instance.PlayerRun(false);
            }
        }

        public  void    UpdateDistToMove(float pDistance)
        {
            if (pDistance > _distToMove)
                _distToMove = pDistance;
        }

        public  void    UpdatePlayerName()
        {
            PlayerPVP[] lAllPlayers =   GetAllPlayers();
            int         lNbPlayers  =   lAllPlayers.Length;

            for (int li = 0; li < lNbPlayers; li++)
                lAllPlayers[li].UI.UpdatePlayerName();
        }

        public  void    UpdatePlayerPossession()
        {
            PlayerPVP[] lAllPlayers =   GetAllPlayers();
            int         lNbPlayers  =   lAllPlayers.Length;

            for (int li = 0; li < lNbPlayers; li++)
                lAllPlayers[li].UI.UpdatePlayerPossession(lAllPlayers[li].Data.IsEqualTo(MatchEngine.Instance.GetCurrentPossesser()));
        }

        #region Getters

        public  PlayerPVP   GetPlayer(eTeam pTeam, ePosition pPosition)
        {
            _array = (eTeam.TEAM_A == pTeam) ? _teamBluePlayers : _teamYellowPlayers;

            if (null == _array || 0 == _array.Length)
                return null;

            foreach (PlayerPVP lPlayer in _array)
            {
                if (pPosition == lPlayer.Data.Position)
                    return lPlayer;
            }

            return null;
        }

        public  PlayerPVP   GetPlayer(string pTeam, string pPosition)
        {
            _array = (eTeam.TEAM_A.ToString() == pTeam) ? _teamBluePlayers : _teamYellowPlayers;

            if (null == _array || 0 == _array.Length)
                return null;

            foreach (PlayerPVP lPlayer in _array)
            {
                if (pPosition == lPlayer.Data.Position.ToString())
                    return lPlayer;
            }

            return null;
        }

        public  PlayerPVP[] GetTeamPlayers(eTeam pTeam)
        {
            if (eTeam.TEAM_A == pTeam)
                return _teamBluePlayers;

            return _teamYellowPlayers;
        }

        public  PlayerPVP[] GetAllPlayers()
        {
            List<PlayerPVP> lAllTeams   =   new List<PlayerPVP>();

            lAllTeams.AddRange(_teamBluePlayers);
            lAllTeams.AddRange(_teamYellowPlayers);

            return lAllTeams.ToArray();
        }

        public  PlayerPVP   GetDuelPlayer(eTeam pTeam)
        {
            Player  lDuelPlayer =   MatchEngine.Instance.GetDuelPlayer(pTeam);

            if (null == lDuelPlayer)
                return null;

            return GetPlayer(pTeam, lDuelPlayer.Position);
        }

        public  PlayerPVP   GetBallPossesser()
        {
            Player  lBallPossesser  =   MatchEngine.Instance.GetCurrentPossesser();

            if (null == lBallPossesser)
                return null;

            return GetPlayer(lBallPossesser.Team, lBallPossesser.Position);
        }

        public  PlayerPVP   FindSelectablePlayer(eTeam pTeam, ePosition pException)
        {
            _array      =   GetTeamPlayers(pTeam);
            _nbPlayers  =   _array.Length;

            for (int li = 0; li < _nbPlayers; li++)
            {
                if (_array[li].Data.Position != pException && _array[li].UI.IsSelectable())
                    return _array[li];
            }

            return null;
        }

        #endregion

        #region Toggle Methods

        public  void    TogglePlayerActive(eTeam pTeam, bool pActive = true)
        {
            PlayerPVP[] lTeamPlayers    =   GetTeamPlayers(pTeam);
            int         lNbPlayers      =   lTeamPlayers.Length;

            for (int li = 0; li < lNbPlayers; li++)
                lTeamPlayers[li].Player3D.TogglePlayerActive(pActive);
        }

        public  void    TogglePlayerSelection(eTeam pTeam, bool pActivate)
        {
            _array      =   GetTeamPlayers(pTeam);
            _nbPlayers  =   _array.Length;

            for (int li = 0; li < _nbPlayers; li++)
                _array[li].UI.ToggleSelection(pActivate);
        }

        public  void    TogglePlayerForbiddenSelection(eTeam pTeam, bool pActivate)
        {
            _array      =   GetTeamPlayers(pTeam);
            _nbPlayers  =   _array.Length;

            for (int li = 0; li < _nbPlayers; li++)
                _array[li].UI.ToggleForbiddenSelection(pActivate);
        }

        public  void    TogglePlayerCollider(eTeam pTeam, bool pActivate)
        {
            _array      =   GetTeamPlayers(pTeam);
            _nbPlayers  =   _array.Length;

            for (int li = 0; li < _nbPlayers; li++)
                _array[li].Player3D.TogglePlayerCollider(pActivate);
        }

        public  void    TogglePlayerCollider(bool pActivate)
        {
            _array      =   GetAllPlayers();
            _nbPlayers  =   _array.Length;

            for (int li = 0; li < _nbPlayers; li++)
                _array[li].Player3D.TogglePlayerCollider(pActivate);
        }

        public  void    ToggleHoveredPlayerName(bool pActivate)
        {
            _array      =   GetAllPlayers();
            _nbPlayers  =   _array.Length;

            for (int li = 0; li < _nbPlayers; li++)
                _array[li].UI.ToggleHoveredPlayerName(pActivate);
        }

        public  void    TogglePlayerName(bool pActivate)
        {
            _array      =   GetAllPlayers();
            _nbPlayers  =   _array.Length;

            for (int li = 0; li < _nbPlayers; li++)
                _array[li].UI.TogglePlayerName(pActivate);
        }

        public  void    TogglePlayerSquadPos(bool pActivate)
        {
            PlayerPVP[] lArray      =   GetAllPlayers();
            int         lNbPlayers  =   lArray.Length;

            for (int li = 0; li < lNbPlayers; li++)
                lArray[li].UI.ToggleTacticalValue(pActivate);
        }

        #endregion

        #region Clear Methods

        public  void    ResetFirstActivation()
        {
            PlayerPVP[] lPlayers    =   GetAllPlayers();
            int         lNbPlayers  =   lPlayers.Length;

            for (int li = 0; li < lNbPlayers; li++)
                lPlayers[li].UI.FirstActivation =   true;
        }

        public  void    ResetTacticalUI()
        {
            PlayerPVP[] lPlayers    =   GetTeamPlayers(UserData.Instance.PVPTeam);
            int         lNbPlayers  =   lPlayers.Length;

            for (int li = 0; li < lNbPlayers; li++)
            {
                lPlayers[li].UI.ToggleSelection(false);
                lPlayers[li].UI.ToggleForbiddenSelection(false);
                lPlayers[li].UI.ToggleTacticalValue(false);
            }
        }

        public  void    ResetActionUI(eTeam pTeam)
        {
            PlayerPVP[] lPlayers    =   GetTeamPlayers(pTeam);
            int         lNbPlayers  =   lPlayers.Length;

            for (int li = 0; li < lNbPlayers; li++)
                lPlayers[li].UI.ToggleTarget(false);
        }

        #endregion

        #endregion

        #region Private Methods

        private void    Start()
        {
            // Initializes engine if needed
            if (_initializeOnStart)
            {
                if (null != MatchEngine.Instance)
                    MatchEngine.Instance.Engine.PhaseMain.OnPhaseChanged += pPhase => OnPhaseChanged(pPhase);

                InitPlayers();
            }
        }

        private void    _initializeSprites()
        {
            Sprite[] lSprites;
            string[] lPaths = new string[] { "Sprites/Gameplay/gameplay" };

            for (int lj = 0; lj < lPaths.Length; lj++)
            {
                lSprites = Resources.LoadAll<Sprite>(lPaths[lj]);

                for (int li = 0; li < lSprites.Length; li++)
                    _sprites.Add(lSprites[li]);
            }
        }

        private void    _initTeamA()
        {
            Team lTeam = MatchEngine.Instance.Engine.TeamMain.TeamA;
            Player[] lTeamPlayers = lTeam.GetPlayers();
            int lNbPlayers = lTeamPlayers.Length;

            for (int li = 0; li < lNbPlayers; li++)
                _teamBluePlayers[li].Setup(lTeamPlayers[li], _ballHandler.transform, _sprites.ToArray());
        }

        private void    _initTeamB()
        {
            Team lTeam = MatchEngine.Instance.Engine.TeamMain.TeamB;
            Player[] lTeamPlayers = lTeam.GetPlayers();
            int lNbPlayers = lTeamPlayers.Length;

            for (int li = 0; li < lNbPlayers; li++)
                _teamYellowPlayers[li].Setup(lTeamPlayers[li], _ballHandler.transform, _sprites.ToArray());
        }

        #endregion
    }
}
﻿using Sportfaction.CasualFantasy.PVP.Gameplay.Matchs;

using SportFaction.CasualFootballEngine.PeriodService.Enum;

using System.Collections;
using UnityEngine;

namespace Sportfaction.CasualFantasy.PVP.Gameplay.Players
{
    /// <summary>
    /// Class handling every animation of a player model
    /// </summary>
    /// @author Madeleine Tranier
    public sealed class PlayerAnimator : MonoBehaviour
    {
        #region Serialized Fields

        [Header("Links")]
        [SerializeField, Tooltip("Player PVP")]
        private PlayerPVP   _playerPVP      =   null; //!< Player main script
        
        [Header("Animators")]
        [SerializeField, Tooltip("Player animator")]
        private Animator    _playerAnimator =	null; //!< Player action animator
        [SerializeField, Tooltip("Move success animation GO")]
        private Animator    _moveAnimator   =   null; //!< Movement success animation

        #endregion

        #region Public Methods

        public  void    Setup()
        {
            StartCoroutine(_idleAnimLoop());
        }

        public  void    ToggleDuelAnim(bool pActive)
        {
            StopAllCoroutines();
            _setTrigger(pActive ? "Duel" : "Idle_In_Game");
        }

        public  void    ToggleRunAnim(bool pActivate)
        {
            if (!pActivate)
                _playerAnimator.ResetTrigger("Run");
            _setTrigger(pActivate ? "Run" : "Idle_In_Game");
        }

        public  void    TriggerActionAnim(string pActionName, bool pSuccess)
        {
            switch (pActionName)
            {
                case "CELEBRATION":
                    _setTrigger("Celebration_" + (pSuccess ? "Scorer" : "Team"));
                    break;

                default:
                    break;
            }
        }

        public  void    ActivateMoveSuccess(bool pShowQuad = false)
        {
            if (null != _moveAnimator)
            {
                _playerPVP.GetComponent<AudioSource>().PlayOneShot(UtilityMethods.FindClip(_playerPVP.SfxConfig, "MinionArrived"));

                // Activate either quad or box FX
                _moveAnimator.transform.GetChild(0).gameObject.SetActive(pShowQuad);
                _moveAnimator.transform.GetChild(1).gameObject.SetActive(!pShowQuad);

                _moveAnimator.gameObject.SetActive(true);
                _moveAnimator.enabled   =   true;

                _moveAnimator.SetTrigger(_playerPVP.IsBlueTeam ? "BlueTeam" : "RedTeam");
            }
        }

        #endregion

        #region Private Methods

        private void    _triggerRandomIdleAnim()
        {
            ePeriod lCurPeriod  =   MatchEngine.Instance.Engine.PeriodMain.Period;

            if (ePeriod.FIRST_PERIOD == lCurPeriod || ePeriod.SECOND_PERIOD == lCurPeriod)
                _setTrigger("Idle_In_Game");

            int lRandValue  =   Random.Range(0, 100);

            if (90 >= lRandValue)
                _setTrigger("Idle_Foot");

            else if (94 >= lRandValue)
                _setTrigger("Idle_Jump");

            else if (99 >= lRandValue)
                _setTrigger("Idle_Stretch");

            else
                _setTrigger("Idle");
        }

        private void    _setTrigger(string pTriggerName)
        {
            if (_playerAnimator.isActiveAndEnabled)
                _playerAnimator.SetTrigger(pTriggerName);
        }

        #region Coroutines
        
        private IEnumerator _idleAnimLoop()
        {
            while(true)
            {
                int lRandValue  =   Random.Range(1, 5);

                yield return new WaitForSeconds(lRandValue);

                _triggerRandomIdleAnim();
			}
        }

        #endregion

        #endregion
    }
}

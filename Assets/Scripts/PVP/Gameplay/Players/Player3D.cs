﻿using Sportfaction.CasualFantasy.Events;
using Sportfaction.CasualFantasy.PVP.Gameplay.Actions;
using Sportfaction.CasualFantasy.PVP.Gameplay.Matchs;
using Sportfaction.CasualFantasy.References.CustomClass;

using SportFaction.CasualFootballEngine.ActionService.Enum;
using SportFaction.CasualFootballEngine.PhaseService.Enum;
using SportFaction.CasualFootballEngine.PlayerService.Enum;

using UnityEngine;

namespace Sportfaction.CasualFantasy.PVP.Gameplay.Players
{
    /// <summary>
    /// Class handling player 3D elements & touch events on GO
    /// </summary>
    /// @author Madeleine Tranier
    public sealed class Player3D : MonoBehaviour
    {
        #region Properties

        public  Transform   HeadTransform   { get { return _headTransform; } }

        #endregion

        #region Public Fields

        public  GameObject  BallRefPos_Gob;
        public  GameObject  Shadow_Gob;
        public  GameObject  Animator_Gob;

        public  string  CurrentAnim_Str =   "Idle";

        #endregion

        #region Serialized Fields

        [Header("Links")]
        [SerializeField, Tooltip("Player PVP")]
        private PlayerPVP   _playerPVP  =   null;

        [Header("3D Elements")]
        [SerializeField, Tooltip("Player's body")]
        private SkinnedMeshRenderer _playerRenderer =   null;
        [SerializeField, Tooltip("Player Collider")]
        private BoxCollider         _collider       =   null;
        [SerializeField, Tooltip("Gloves renderers")]
        private MeshRenderer[]      _glovesRenderers;
        [SerializeField, Tooltip("Head transform")]
        private Transform           _headTransform  =   null;
        [SerializeField, Tooltip("Resolution FX")]
        private GameObject          _resolutionFX   =   null;

        [Header("Game Events")]
        [SerializeField, Tooltip("Event to invoke when player has been selected")]
        private GameEvent   _playerSelectedEvt  =   null;

        [Header("References")]
        [SerializeField, Tooltip("Ref to player selected")]
        private PlayerPVPReference  _playerFocusedRef   =   null;

        [Header("Configuration")]
        [SerializeField, Tooltip("Player materials")]
        private Material[]  _playerMats;

        #endregion

        #region Private Fields

        private bool    _btnDown    =   false;
        private bool    _mouseDown  =   false;

        #endregion

        #region Public Methods

        #region Touch Events

        public  void    OnMouseDown()
        {
            _btnDown    =   true;
            _playerPVP.UI.ActivateSelection(true);
            if (!_playerPVP.IsClickable && _playerPVP.IsBlueTeam)
            {
                _playerPVP.UI.AnimateForbiddenSelection();
                if (null != MatchEngine.Instance && ePhase.TACTICAL_ATTACK == MatchEngine.Instance.CurPhase)
                    MatchAudio.Instance.OnTapForbidden();
                else
                    MatchAudio.Instance.OnMinionRollOver();
            }
            else if (_playerPVP.IsClickable && _playerPVP.IsBlueTeam)
                MatchAudio.Instance.OnMinionRollOver();
        }

        public  void    OnMouseEnter()
        {
            if ((_mouseDown || Input.touchCount > 0))
            {
                _btnDown = true;
                _playerPVP.UI.ActivateSelection(true);
                if (!_playerPVP.IsClickable && _playerPVP.IsBlueTeam)
                {
                    _playerPVP.UI.AnimateForbiddenSelection();
                    MatchAudio.Instance.OnTapForbidden();
                }
                else if (_playerPVP.IsClickable && _playerPVP.IsBlueTeam)
                    MatchAudio.Instance.OnMinionRollOver();
            }
        }

        public  void    OnMouseUp()
        {
            if (!_btnDown || !_playerPVP.IsBlueTeam || !_playerPVP.IsClickable || null == MatchEngine.Instance)
                return;

            _playerPVP.UI.ActivateSelection(false);

            if (
                null != ActionsManager.Instance.ActionInProcess
                && ActionsManager.Instance.ActionInProcess.Type == eAction.PASS
                && (ePhase.ACTION == MatchEngine.Instance.CurPhase || ePhase.KICK_OFF == MatchEngine.Instance.CurPhase)
                && MatchEngine.Instance.Engine.PossessionMain.CurPlayerPossession != _playerPVP.Data.Position
            )
            {
                // Selects player to pass ball to
                MatchAudio.Instance.OnTargetValidated();

                ActionsManager.Instance.ValidatePlayer(_playerPVP);
                ActionsManager.Instance.PrepareAction(ActionsManager.Instance.ActionInProcess);
            }
            else if (
                ePhase.TACTICAL_ATTACK == MatchEngine.Instance.CurPhase &&
                (null == _playerFocusedRef.CurrentValue
                || _playerFocusedRef.CurrentValue.Data.IsEqualTo(ActionsManager.Instance.DuelPlayer?.Data))
            )
            {
                // Selects player to give instruction to
                MatchAudio.Instance.OnTargetValidated();

                _playerFocusedRef.CurrentValue = _playerPVP;
                UtilityMethods.InvokeGameEvent(_playerSelectedEvt);
            }

            _btnDown = false;
        }

        public  void    OnMouseExit()
        {
            if (_btnDown)
                _playerPVP.UI.ActivateSelection(false);

            _btnDown = false;
        }

        #endregion

        public  void    Setup()
        {
            if (ePosition.G == _playerPVP.Data.GlobalPosition)
            {
                if (null != _glovesRenderers)
                {
                    for (int li = 0; li < 2; li++)
                    {
                        if (null != _glovesRenderers[li])
                        {
                            _glovesRenderers[li].gameObject.SetActive(true);
                            _glovesRenderers[li].material.SetColor("_BaseColor", ActionsManager.Instance.ColorConfig.GetTeamColor(_playerPVP.IsBlueTeam));
                        }
                    }
                }
            }

            _playerRenderer.material    =   _playerMats[_playerPVP.IsBlueTeam ? 0 : 1];

            TogglePlayerActive(true);
        }

        public  void    TogglePlayerActive(bool pActive)
        {
            _playerRenderer.material.SetColor("_BaseColor", pActive ? Color.white : Color.gray);
        }

        public  void    TogglePlayerCollider(bool pActivate)
        {
            _collider.enabled   =   pActivate;
        }

        public  void    ToggleResolutionFX(bool pActivate)
        {
            if (pActivate)
            {
                _resolutionFX.transform.localEulerAngles    =   new Vector3(1f, 1f, 1f);
                _resolutionFX.transform.localPosition       =   Vector3.zero;
                var lParticleSystem =   _resolutionFX.transform.GetChild(0).GetComponent<ParticleSystem>().main;
                lParticleSystem.startColor  =   Color.white;
            }

            if (_resolutionFX.activeSelf != pActivate)
                _resolutionFX.SetActive(pActivate);
        }

        public  void    ToggleTWPFX(bool pActivate, bool pReverse = false)
        {
            if (pActivate)
            {
                _resolutionFX.transform.localEulerAngles    =   new Vector3(1f, 1f, pReverse ? 180f : 1f);
                _resolutionFX.transform.localPosition       =   new Vector3(0f, pReverse ? 0.5f : 0f, 0f);
                _resolutionFX.transform.GetChild(0).GetComponent<ParticleSystem>().startColor = ActionsManager.Instance.ColorConfig.TWPSynapse;
            }

            _resolutionFX.SetActive(pActivate);
        }

        #endregion

        #region Private Methods

        private void    Update()
        {
            // Checks whether mouse is down or up
            if (Input.GetMouseButtonDown(0))
            {
                _mouseDown = true;
            }
            if (Input.GetMouseButtonUp(0))
            {
                _mouseDown = false;
                if (_btnDown)
                    OnMouseUp();
            }

            // Make shadow follow player
            Shadow_Gob.transform.position = new Vector3(Animator_Gob.transform.position.x, Shadow_Gob.transform.position.y, Animator_Gob.transform.position.z);
        }

        #endregion
    }
}
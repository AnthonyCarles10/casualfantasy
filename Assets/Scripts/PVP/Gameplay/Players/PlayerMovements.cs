﻿using Sportfaction.CasualFantasy.FieldSetup;
using Sportfaction.CasualFantasy.Manager.Profile;
using Sportfaction.CasualFantasy.PVP.Gameplay.Matchs;

using SportFaction.CasualFootballEngine.ActionService.Enum;
using SportFaction.CasualFootballEngine.FieldService.Model;
using SportFaction.CasualFootballEngine.TeamService.Enum;

using DG.Tweening;
using UnityEngine;

namespace Sportfaction.CasualFantasy.PVP.Gameplay.Players
{
    /// <summary>
    /// Class handling every player GO movements on field
    /// </summary>
    /// @author Madeleine Tranier
    public sealed class PlayerMovements : MonoBehaviour
    {
        #region Properties

        public  RectTransform   PlayerTransform { get { return _playerTransform; } }
        public  Transform       PuppetTransform { get { return _puppetTransform; } }
        public  Transform       FootTransform   { get { return _footTransform; } }

        public  bool    ShouldFaceBall          { set { _shouldFaceBall = value; } }

        public  float   TimeToWaitForMovement   { get { return _timeToWaitForMovement; } }

        #endregion

        #region Serialized Fields

        [Header("Links")]
        [SerializeField, Tooltip("Player PVP")]
        private PlayerPVP   _playerPVP  =   null;

        [Header("3D Elements")]
        [SerializeField, Tooltip("Player transform")]
        private RectTransform   _playerTransform    =   null;
        [SerializeField, Tooltip("Puppet transform")]
        private Transform       _puppetTransform    =   null;
        [SerializeField, Tooltip("Player foot transform")]
        private Transform       _footTransform      =   null;

        #endregion

        #region Private Fields

        private Transform   _ballTransform  =   null;

        private bool        _shouldFaceBall =   false;
        private FieldCase   _oldFieldCase   =   null;

        private Vector3[]   _goalPositions  =   new Vector3[2] { new Vector3(-4.075f, -0.018f, -1.59f), new Vector3(4.075f, -0.018f, -1.59f) }; //!< Field goal world coordinates

        private bool    _shouldMovePlayer       =   false;
        private Vector3 _targetSpot             =   Vector3.zero;
        private float   _playerSpeed            =   0f;
        private bool    _activateFeedback       =   false;
        private float   _timeToWaitForMovement  =   0f;

        #endregion

        #region Public Methods

        public  void    Setup(Transform pBallTransform, bool pMovePlayer = true)
        {
            _ballTransform  =   pBallTransform;

            if (pMovePlayer)
                MovePlayer(true);
        }

        public  Tween   LocalMoveTo(Vector3 pPosition, float pAnimDuration = 0f)
        {
            return _playerTransform.DOLocalMove(pPosition, pAnimDuration);
        }

        public  void    MovePlayer(bool pInstantaneous = false, bool pActivateFeedback = false)
        {
            FieldCase lCoords = _playerPVP.Data.CurFieldCase;

            _shouldFaceBall = false;
            OrientPlayerTowardsTarget(PlayersManager.Instance.CalculatePositionByCoordinates(lCoords));

            if (pInstantaneous)
            {
                this.transform.localPosition = PlayersManager.Instance.CalculatePositionByCoordinates(lCoords, true, false);
                PlayerFinishedMoving();
                _timeToWaitForMovement = 0f;
            }
            else
            {
                _activateFeedback = (pActivateFeedback && !lCoords.IsEqualTo(_oldFieldCase));
                _targetSpot = PlayersManager.Instance.CalculatePositionByCoordinates(lCoords, true, false);
                float lDistToMove = Vector3.Distance(this.transform.localPosition, _targetSpot);
                _playerSpeed = (lDistToMove * 0.09f) + 1f;

                _shouldMovePlayer = true;
                PlayersManager.Instance.UpdateDistToMove(lDistToMove);

                if (0.17f <= lDistToMove)
                {
                    _playerPVP.Animator.ToggleRunAnim(true);
                }

                _timeToWaitForMovement = lDistToMove / _playerSpeed + (pActivateFeedback ? 0.8f : 0f);
            }

            _oldFieldCase = lCoords;
        }

        public  void    MovePlayer(FieldCase pNewFC, bool pInstantaneous = false, bool pActivateFeedback = false)
        {
            FieldCase lCoords = pNewFC;

            _shouldFaceBall = false;
            OrientPlayerTowardsTarget(PlayersManager.Instance.CalculatePositionByCoordinates(lCoords));

            if (pInstantaneous)
            {
                this.transform.localPosition = PlayersManager.Instance.CalculatePositionByCoordinates(lCoords, true, false);
                PlayerFinishedMoving();
                _timeToWaitForMovement = 0f;
            }
            else
            {
                _activateFeedback = (pActivateFeedback && !lCoords.IsEqualTo(_oldFieldCase));
                _targetSpot = PlayersManager.Instance.CalculatePositionByCoordinates(lCoords, true, false);
                float lDistToMove = Vector3.Distance(this.transform.localPosition, _targetSpot);
                _playerSpeed = (lDistToMove * 0.09f) + 1f;

                _shouldMovePlayer = true;
                PlayersManager.Instance.UpdateDistToMove(lDistToMove);

                if (0.17f <= lDistToMove)
                {
                    _playerPVP.Animator.ToggleRunAnim(true);
                }

                _timeToWaitForMovement = lDistToMove / _playerSpeed + (pActivateFeedback ? 0.8f : 0f);
            }

            _oldFieldCase = lCoords;
        }

        public  void    PlayerFinishedMoving()
        {
            _activateFeedback   =   false;

            _playerPVP.UI.UpdatePlayerName();

            if (null != PlayersManager.Instance)
                PlayersManager.Instance.UpdatePlayersToMove();

            if (_playerPVP.Data.IsEqualTo(MatchEngine.Instance.GetCurrentPossesser()) && "FS_A_KICK_OFF" != MatchEngine.Instance.Engine.FieldSetupMain.CurrentFieldSetup)
            {
                OrientPlayerTowardsGoal();
            }
            else
            {
                eMove   pDirectionToLookAt  =   FieldSetupMain.Instance.GetPlayerDirection(_playerPVP.Data.Team, MatchEngine.Instance.Engine.FieldSetupMain.CurrentFieldSetup, _playerPVP.Data.Position);

                if (eMove.NONE != pDirectionToLookAt)
                    OrientPlayerTowardsDirection(pDirectionToLookAt);
                else
                    OrientPlayer(_ballTransform.position);
            }

            _playerPVP.Animator.ToggleRunAnim(false);
        }

        public  void    OrientPlayer(Vector3 pTarget)
        {
            Vector3 lTarget = new Vector3(pTarget.x, _puppetTransform.position.y, _puppetTransform.position.z);
            _puppetTransform.LookAt(lTarget);

            _playerPVP.IsLookingRight = (pTarget.x - this.transform.position.x >= 0f);
        }

        public  void    OrientPlayerTowardsDirection(eMove pDirection)
        {
            Vector3 lEulerAngles = Vector3.zero;
            bool lIsManagerTeamA = eTeam.TEAM_A == UserData.Instance.PVPTeam;

            switch (pDirection)
            {
                case eMove.LEFT:
                    lEulerAngles = new Vector3(0f, lIsManagerTeamA ? 0f : 180f, 0f);
                    break;
                case eMove.TOP_LEFT:
                    lEulerAngles = new Vector3(0f, lIsManagerTeamA ? 45f : -135f, 0f);
                    break;
                case eMove.TOP:
                    lEulerAngles = new Vector3(0f, lIsManagerTeamA ? 90f : -90f, 0f);
                    break;
                case eMove.TOP_RIGHT:
                    lEulerAngles = new Vector3(0f, lIsManagerTeamA ? 135f : -45f, 0f);
                    break;
                case eMove.RIGHT:
                    lEulerAngles = new Vector3(0f, lIsManagerTeamA ? 180f : 0f, 0f);
                    break;
                case eMove.DOWN_RIGHT:
                    lEulerAngles = new Vector3(0f, lIsManagerTeamA ? -135f : 45f, 0f);
                    break;
                case eMove.DOWN:
                    lEulerAngles = new Vector3(0f, lIsManagerTeamA ? -90f : 90f, 0f);
                    break;
                case eMove.DOWN_LEFT:
                    lEulerAngles = new Vector3(0f, lIsManagerTeamA ? -45f : 135f, 0f);
                    break;

            }

            _puppetTransform.localEulerAngles = lEulerAngles;
        }

        public  void    OrientPlayerTowardsTarget(Vector3 pTarget)
        {
            Vector3 lTarget = new Vector3(pTarget.x, _puppetTransform.position.y, pTarget.z);
            if (0.15f <= Vector3.Distance(this.transform.position, lTarget))
            {
                _puppetTransform.LookAt(lTarget);

                _playerPVP.IsLookingRight = (lTarget.x - this.transform.position.x >= 0f);
            }
        }

        public  void    OrientPlayerTowardsGoal()
        {
            OrientPlayer(_goalPositions[_playerPVP.IsBlueTeam ? 1 : 0]);
        }

        public  void    OrientPlayerTowardsBall()
        {
            Vector3 lTarget = new Vector3(_ballTransform.position.x, _puppetTransform.position.y, _ballTransform.position.z);
            if (0.25f <= Vector3.Distance(this.transform.position, lTarget))
            {
                _puppetTransform.LookAt(lTarget);

                _playerPVP.IsLookingRight = (lTarget.x - this.transform.position.x >= 0f);
            }
        }

        #endregion

        #region Private Methods

        private void    Update()
        {
            // Orients player towards ball throughout the game (if needed)
            if (null != _puppetTransform && null != _ballTransform && _shouldFaceBall)
                OrientPlayerTowardsBall();

            // Moves player towards target position
            if (_shouldMovePlayer)
            {
                this.transform.localPosition = Vector3.MoveTowards(this.transform.localPosition, _targetSpot, _playerSpeed * Time.deltaTime);

                if (Vector3.Distance(this.transform.localPosition, _targetSpot) <= 0.1f)
                {
                    if (_activateFeedback)
                    {
                        _playerPVP.Animator.ActivateMoveSuccess();
                        // Make move intention UI fade out
                        if (null != GameObject.Find("MOVE_" + _playerPVP.name))
                            GameObject.Find("MOVE_" + _playerPVP.name).GetComponent<Animator>().enabled = true;
                    }

                    _activateFeedback = false;
                    _shouldMovePlayer = false;
                    this.transform.localPosition = _targetSpot;
                    PlayerFinishedMoving();
                    _playerPVP.Animator.ToggleRunAnim(false);
                }
            }
        }

        private void    LateUpdate()
        {
            // Checks if player arrived to destination to stop run anim
            if (_shouldMovePlayer && Vector3.Distance(this.transform.localPosition, _targetSpot) <= 0.1f)
            {
                _playerPVP.Animator.ToggleRunAnim(false);
            }
        }

        #endregion
    }
}

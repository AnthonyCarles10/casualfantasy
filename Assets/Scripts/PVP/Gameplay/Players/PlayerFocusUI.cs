﻿using Sportfaction.CasualFantasy.PVP.Gameplay.Actions;
using Sportfaction.CasualFantasy.Services;

using SportFaction.CasualFootballEngine.PlayerService.Model;

using UnityEngine;
using UnityEngine.UI;

namespace Sportfaction.CasualFantasy.PVP.Gameplay.Players
{
    /// <summary>
    /// Class handling player focus block display on action panel
    /// </summary>
    /// @author Madeleine Tranier
    public  sealed  class   PlayerFocusUI : MonoBehaviour
    {
        #region Serialized Fields
        
        [SerializeField, Tooltip("Is player from current manager's team ?")]
        private bool    _isCurManagerPlayer =   true;

        [Header("UI Elements")]
        [SerializeField, Tooltip("Player name")]
        private TMPro.TMP_Text  _nameTxt    =   null;
        [SerializeField, Tooltip("Player score")]
        private TMPro.TMP_Text _scoreTxt    =   null;
        [SerializeField, Tooltip("Form img")]
        private Image           _formImg    =   null;
        [SerializeField, Tooltip("Card img")]
        private Image           _cardImg    =   null;

        #endregion

        #region Private Fields

        private string      _playerName =   ""; //!< Player name to display
        private Sprite[]    _sprites;
        private Player      _player     =   null;
        
        #endregion
        
        #region Public Methods

        public  void    Init(Sprite[] pSprites)
        {
            _sprites    =   pSprites;
        }

        public  void    Setup(Player pPlayer)
        {
            _player = pPlayer;

            if (null == _player)
            {
                UtilityMethods.SetText(_nameTxt, "");
                UtilityMethods.SetText(_scoreTxt, "");
                return;
            }

            _playerName =   !string.IsNullOrEmpty(_player.NameField) ? _player.NameField : _player.Position.ToString();
            UtilityMethods.SetText(_nameTxt, StringExtensions.FirstCharToUpper(_playerName));
            UtilityMethods.SetText(_scoreTxt, pPlayer.Score.ToString());

            _cardImg.sprite =   UtilityMethods.GetSprite(_sprites, "CardPlayer_Match_" + ActionsManager.Instance.GameplayConfig.GetRarity(_player.RareLevel));
            _formImg.sprite =   UtilityMethods.GetSprite(_sprites, "Form_Arrow_" + ActionsManager.Instance.GameplayConfig.GetFormColorString(_player.FormLevel));
            _formImg.SetNativeSize();
        }

        #endregion
    }
}

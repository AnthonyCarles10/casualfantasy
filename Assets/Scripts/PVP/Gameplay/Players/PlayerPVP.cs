﻿using Sportfaction.CasualFantasy.Manager.Profile;

using SportFaction.CasualFootballEngine.ActionService.Enum;
using SportFaction.CasualFootballEngine.PlayerService.Model;
using SportFaction.CasualFootballEngine.TeamService.Enum;

using UnityEngine;

namespace Sportfaction.CasualFantasy.PVP.Gameplay.Players
{
    /// <summary>
    /// Class handling access to every player script
    /// </summary>
    /// @author Madeleine Tranier
    public sealed class PlayerPVP : MonoBehaviour
    {
        #region Properties
        
        public	Player			Data		    { get { return _data; } }
        public	PlayerUI	    UI              { get { return _ui; } }
        public	PlayerMovements	Movements	    { get { return _movements; } }
        public	Player3D		Player3D	    { get { return _player3D; } }
        public	PlayerAnimator	Animator	    { get { return _animator; } }
        
        public  AudioClip[]     SfxConfig       { get { return _sfxConfig; } }

        #endregion

        #region Public Fields

        [ReadOnly]
        public  bool    IsClickable     =   false;
        [ReadOnly]
        public  bool    IsBlueTeam      =   true; //!< Is player in current manager's team ?
        [ReadOnly]
        public  bool    HasAction       =   false;
        [ReadOnly]
        public  eAction Action          =   eAction.NONE;
        [ReadOnly]
        public  bool    IsLookingRight  =   true;

        #endregion

        #region Serialized Fields

        [Header("External Classes")]
        [SerializeField, Tooltip("Player UI handler")]
        private	PlayerUI        _ui		    =	null;
        [SerializeField, Tooltip("Player movements & orientation handler")]
        private	PlayerMovements	_movements	=	null;
        [SerializeField, Tooltip("Player 3D model handler")]
        private	Player3D		_player3D   =	null;
        [SerializeField, Tooltip("Player animation handler")]
        private	PlayerAnimator	_animator	=	null;

        #pragma warning disable 414, CS0649
        [Header("Infos")]
        [SerializeField, Tooltip("Player SFX clips")]
        private AudioClip[] _sfxConfig;
        [SerializeField, ReadOnly, Tooltip("Player engine data")]
        private	Player	    _data;
        
        #endregion

        #region Public Methods

        public	void	Setup(Player pPlayer, Transform pBallTransform, Sprite[] pSprites)
        {
             this.transform.localEulerAngles    =   new Vector3((eTeam.TEAM_A != UserData.Instance.PVPTeam) ? 180f : 0f, -90f, 90f);

            _data		=	pPlayer;
            IsBlueTeam	=	UserData.Instance.PVPTeam == _data.Team;
            
            this.gameObject.name	=	_data.Team + "-" + _data.Position.ToString();
            this.gameObject.SetActive(true);
            
            _ui.Setup(pSprites);
            _movements.Setup(pBallTransform);
            _player3D.Setup();
            _animator.Setup();
        }

        public  void    UpdateHasAction(bool pHasAction, eAction pAction = eAction.NONE)
        {
            HasAction   =   pHasAction;

            if (pHasAction)
                Action  =   pAction;
        }

        #endregion
    }
}
﻿using Sportfaction.CasualFantasy.Config;
using Sportfaction.CasualFantasy.PVP.Gameplay.Actions;
using Sportfaction.CasualFantasy.PVP.Gameplay.Matchs;
using Sportfaction.CasualFantasy.Services;

using SportFaction.CasualFootballEngine.ActionService.Enum;
using SportFaction.CasualFootballEngine.PlayerService.Model;

using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;
using SportFaction.CasualFootballEngine.PhaseService.Enum;

namespace Sportfaction.CasualFantasy.PVP.Gameplay.Players
{
    /// <summary>
    /// Class handling every UI elements related to player entity
    /// </summary>
    /// @author Madeleine Tranier
    public sealed class PlayerUI : MonoBehaviour
    {
        #region Properties

        public  bool    FirstActivation { set { _firstActivation = value; } }

        #endregion

        #region Serialized Fields

        [Header("Links")]
        [SerializeField, Tooltip("Player PVP")]
        private PlayerPVP   _playerPVP  =   null;

        [Header("UI Elements")]
        [SerializeField, Tooltip("Selection cursor")]
        private Image           _selectionCursor        =   null;
        [SerializeField, Tooltip("Bubble image")]
        private Image           _bubbleImg              =   null;
        [SerializeField, Tooltip("Target image")]
        private SpriteRenderer  _targetImg              =   null;
        [SerializeField, Tooltip("Player name text")]
        private TMPro.TMP_Text  _hoveredPlayerNameTxt   =   null;
        [SerializeField, Tooltip("Player name text")]
        private TMPro.TMP_Text  _playerNameTxt          =   null;
        [SerializeField, Tooltip("Player's att/def value")]
        private TMPro.TMP_Text  _tacticalValueTxt       =   null;
        [SerializeField, Tooltip("Player's forbidden selection GO")]
        private GameObject      _forbiddenSelectionGO   =   null;
        [SerializeField, Tooltip("Player move check GO")]
        private GameObject      _moveCheckGO            =   null;

        [Header("Configuration")]
        [SerializeField, Tooltip("Color config")]
        private ColorConfig     _colorConfig    =   null;
        [SerializeField, Tooltip("Gameplay Config")]
        public  GameplayConfig  _gameplayConfig =   null;
        [SerializeField, Tooltip("Text materials")]
        private Material[]      _txtMaterials;

        #endregion

        #region Private Fields

        private Sprite[]    _sprites;
        private bool        _firstActivation    =   true;
        private int         _namePositioning    =   0;

        #endregion

        #region Public Methods

        public  void    Setup(Sprite[] pSprites)
        {
            _sprites    =   pSprites;
            ToggleHoveredPlayerName(false);
            TogglePlayerName(false);

            _bubbleImg.sprite   =   UtilityMethods.GetSprite(_sprites, "Bubble_" + (_playerPVP.IsBlueTeam ? "Blue" : "Red"));
            _targetImg.color    =   _colorConfig.GetTeamColor(_playerPVP.IsBlueTeam);

            _hoveredPlayerNameTxt.transform.GetChild(0).GetComponent<Image>().sprite   =   UtilityMethods.GetSprite(_sprites, "Name_Strip" + (_playerPVP.IsBlueTeam ? "" : "_Adv"));
            _hoveredPlayerNameTxt.transform.GetChild(1).GetComponent<Image>().sprite   =   UtilityMethods.GetSprite(_sprites, "Form_Square_" + (_playerPVP.IsBlueTeam ? "Player_" : "Adv_") + _gameplayConfig.GetForm(_playerPVP.Data.FormLevel));

            _playerNameTxt.transform.GetChild(1).GetComponent<Image>().sprite   =   UtilityMethods.GetSprite(_sprites, "Form_Square_" + (_playerPVP.IsBlueTeam ? "Player_" : "Adv_") + _gameplayConfig.GetForm(_playerPVP.Data.FormLevel));
            _playerNameTxt.transform.GetChild(1).gameObject.SetActive(true);

            foreach (Transform pChild in _bubbleImg.transform)
                pChild.GetComponent<Image>().sprite =   UtilityMethods.GetSprite(_sprites, "Bubble_Dot_" + (_playerPVP.IsBlueTeam ? "Blue" : "Red"));
            _bubbleImg.transform.GetChild(3).GetComponent<Image>().sprite = UtilityMethods.GetSprite(_sprites, "Check_" + (_playerPVP.IsBlueTeam ? "Player" : "Adv") + "_Icone");

            //_hoveredPlayerNameTxt.fontMaterial  =   _txtMaterials[_playerPVP.IsBlueTeam ? 0 : 1];
            _tacticalValueTxt.color             =   _colorConfig.GetTeamColor(_playerPVP.IsBlueTeam);

            UtilityMethods.SetText(_hoveredPlayerNameTxt, _playerPVP.Data.NameField);
            UtilityMethods.SetText(_playerNameTxt, StringExtensions.Capitalize(_playerPVP.Data.NameField));
        }

        public  void    ActivateSelection(bool pActive)
        {
            ToggleHoveredPlayerName(pActive && ePhase.TACTICAL_ATTACK != MatchEngine.Instance.CurPhase);

            if (_selectionCursor.isActiveAndEnabled)
            {
                _selectionCursor.transform.DOScale(pActive ? new Vector3(3f, 3f, 1f) : new Vector3(1f, 1f, 1f), 0.1f);
                if (!pActive)
                {
                    _selectionCursor.transform.parent.GetComponent<Animator>().enabled  =   true;
                    PlayerPVP   lRef    =   PlayersManager.Instance.FindSelectablePlayer(_playerPVP.Data.Team, _playerPVP.Data.Position);
                    _selectionCursor.transform.parent.GetComponent<Animator>().Play("AssetsSelection_2", 0, null != lRef ? lRef.UI.GetSelectionFrame() : 0f);
                }
                else
                    _selectionCursor.transform.parent.GetComponent<Animator>().enabled = false;
            }

            if (_targetImg.gameObject.activeSelf && _playerPVP.IsClickable && null != ActionsManager.Instance.ActionInProcess && eAction.PASS == ActionsManager.Instance.ActionInProcess.Type)
            {
                _targetImg.transform.DOScale(pActive ? new Vector3(3f, 3f, 1f) : new Vector3(1f, 1f, 1f), 0.1f);
            }
        }

        public  float   GetSelectionFrame()
        {
            float lCurrentFrame   =   0f;

            if (_selectionCursor.transform.parent.GetComponent<Animator>().enabled)
            {
                AnimatorStateInfo   lState  =   _selectionCursor.transform.parent.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0);
                lCurrentFrame   =   lState.normalizedTime;
            }

            return  lCurrentFrame;
        }

        public  bool    IsSelectable()
        {
            return _selectionCursor.enabled;
        }

        public  void    AnimateForbiddenSelection()
        {
            if (_forbiddenSelectionGO.GetComponent<Animation>().isActiveAndEnabled)
                _forbiddenSelectionGO.GetComponent<Animation>().Play("ForbiddenSelection");
        }

        public  void    CheckBubble()
        {
            ToggleBubble(true);
            _bubbleImg.GetComponent<Animator>().SetTrigger("Check");
        }

        public  void    UpdatePlayerName()
        {
            PlayerPVP lBallPossesser = PlayersManager.Instance.GetBallPossesser();

            if (null != lBallPossesser && _playerPVP.Data.IsEqualTo(lBallPossesser.Data))
            {
                //_playerNameBisTxt.color = ActionsManager.Instance.ColorConfig.GetTeamColor(_playerPVP.IsBlueTeam);
            }
            else
            {
                Player[]    lLeftPlayers    =   MatchEngine.Instance.Engine.FieldMain.GetNextPlayersDirection(_playerPVP.Data, eMove.LEFT);
                Player[]    lRightPlayers   =   MatchEngine.Instance.Engine.FieldMain.GetNextPlayersDirection(_playerPVP.Data, eMove.RIGHT);
                Player[]    lTopPlayers     =   MatchEngine.Instance.Engine.FieldMain.GetNextPlayersDirection(_playerPVP.Data, eMove.TOP);

                //Debug.Log(_playerPVP.name + " | Left = " + lLeftPlayers.Length + " | Right = " + lRightPlayers.Length + " | Top = " + lTopPlayers.Length);

                _playerNameTxt.GetComponent<CanvasGroup>().alpha = 0.9f;
                if (0 < lLeftPlayers.Length)
                {
                    if (lLeftPlayers.Length % 2 == 1 || lTopPlayers.Length < 0)
                        _playerNameTxt.GetComponent<CanvasGroup>().alpha = 1f;
                    _namePositioning = (lLeftPlayers.Length % 2 == 1) ? -1 : 1;
                }
                else if (0 < lRightPlayers.Length)
                {
                    _namePositioning = 1;
                }
                else
                {
                    _playerNameTxt.GetComponent<Canvas>().sortingOrder = -1;
                    _namePositioning = 0;
                }

                _playerNameTxt.rectTransform.sizeDelta = new Vector2(_playerNameTxt.preferredWidth, 50f);
            }
        }

        public  void    UpdatePlayerPossession(bool pHasBall)
        {
            if (pHasBall)
                _playerNameTxt.color =   ActionsManager.Instance.ColorConfig.GetTeamColor(_playerPVP.IsBlueTeam);
            else
                _playerNameTxt.color =   Color.white;
        }

        public  void    ValidatePlayerAction(bool pActivatePastil = true)
        {
            _moveCheckGO.transform.GetChild(1).gameObject.SetActive(pActivatePastil);
            _moveCheckGO.SetActive(true);
        }

        #region Toggle Methods

        public  void    ToggleHoveredPlayerName(bool pActivate)
        {
            _hoveredPlayerNameTxt.enabled  =   pActivate;
            _hoveredPlayerNameTxt.transform.GetChild(0).gameObject.SetActive(pActivate);
            _hoveredPlayerNameTxt.transform.GetChild(1).gameObject.SetActive(pActivate);
        }

        public  void    TogglePlayerName(bool pActivate)
        {
            _playerNameTxt.gameObject.SetActive(pActivate);
        }

        public  void    ToggleSelection(bool pActive)
        {
            DOTween.Kill(_selectionCursor.transform);

            _selectionCursor.transform.parent.GetComponent<Animator>().enabled  =   pActive;

            if (pActive)
            {
                if (!_selectionCursor.isActiveAndEnabled && !_firstActivation)
                {
                    _selectionCursor.DOFade(0f, 0f);
                    _selectionCursor.DOFade(1f, 0.5f);
                }

                _selectionCursor.transform.parent.GetComponent<Animator>().SetTrigger(_firstActivation ? "Activate" : "Idle");
                _firstActivation    =   false;
            }

            _selectionCursor.enabled    =   pActive;
        }

        public  void    ToggleForbiddenSelection(bool pActive)
        {
            _forbiddenSelectionGO.SetActive(pActive);
        }

        public  void    ToggleBubble(bool pActive, bool pFull = true)
        {
            bool    lAreadyVisible  =   _bubbleImg.gameObject.activeSelf;

            if (pActive)
            {
                _bubbleImg.gameObject.SetActive(true);
                _bubbleImg.rectTransform.localScale = pFull ? new Vector3(1f, 1f, 1f) : new Vector3(0.5f, 0.5f, 1f);

                if (!lAreadyVisible && !_playerPVP.HasAction)
                    _bubbleImg.GetComponent<Animator>().SetTrigger("Dot");
            }
            else
            {
                if (_bubbleImg.isActiveAndEnabled)
                    _bubbleImg.GetComponent<Animator>().SetTrigger("CheckFadeOut");
            }
        }

        public  void    ToggleTarget(bool pActivate)
        {
            if (pActivate && !_targetImg.gameObject.activeSelf)
            {
                _targetImg.transform.localScale  =   new Vector3(3f, 3f, 1f);
                _targetImg.transform.DOScale(new Vector3(1f, 1f, 1f), 0.5f);
            }

            _targetImg.gameObject.SetActive(pActivate);
        }

        public  void    ToggleTacticalValue(bool pActivate)
        {
            if (pActivate)
                UtilityMethods.SetText(
                    _tacticalValueTxt,
                    _playerPVP.Data.Team == MatchEngine.Instance.Engine.PossessionMain.CurTeamPossession ? _playerPVP.Data.Attack.ToString() : _playerPVP.Data.Defense.ToString()
                );

            _tacticalValueTxt.enabled   =   pActivate;
        }

        #endregion

        #endregion

        #region Private Methods

        private void	Update()
        {
            _placeUI();
        }
        
        private void    _placeUI()
        {
            _hoveredPlayerNameTxt.transform.position       =   Camera.main.WorldToScreenPoint(_playerPVP.transform.position + new Vector3(0f, 0.9f, 0f));
            _playerNameTxt.transform.position    =   Camera.main.WorldToScreenPoint(_playerPVP.transform.position + new Vector3(0f, _namePositioning == 0 ? 0.56f : (_namePositioning < 0 ? 0.41f : 0.58f), 0f));

            if (1f <= _bubbleImg.rectTransform.localScale.x)
                _bubbleImg.transform.position       =   Camera.main.WorldToScreenPoint(_playerPVP.Player3D.HeadTransform.position + new Vector3(_playerPVP.IsBlueTeam ? -0.3f : 0.3f, 0.2f, 0f));
            else
                _bubbleImg.transform.position       =   Camera.main.WorldToScreenPoint(_playerPVP.Player3D.HeadTransform.position + new Vector3(_playerPVP.IsBlueTeam ? -0.18f : 0.18f, 0.13f, 0f));

            if (null != _moveCheckGO)
                _moveCheckGO.transform.position     =   Camera.main.WorldToScreenPoint(_playerPVP.transform.position);
        }

        #endregion
    }
}
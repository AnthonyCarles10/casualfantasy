﻿using Sportfaction.CasualFantasy.Events;
using Sportfaction.CasualFantasy.Manager.Profile;
using Sportfaction.CasualFantasy.PVP.Gameplay.Actions;
using Sportfaction.CasualFantasy.PVP.Gameplay.FX;
using Sportfaction.CasualFantasy.PVP.Gameplay.Matchs;
using Sportfaction.CasualFantasy.PVP.Gameplay.Players;
using Sportfaction.CasualFantasy.PVP.Test;
using Sportfaction.CasualFantasy.References.CustomClass;
using Sportfaction.CasualFantasy.Services.Userstracking;

using SportFaction.CasualFootballEngine.ActionService.Response;
using SportFaction.CasualFootballEngine.FieldService.Model;
using SportFaction.CasualFootballEngine.PeriodService.Enum;
using SportFaction.CasualFootballEngine.TeamService.Enum;
using SportFaction.CasualFootballEngine.PhaseService.Enum;

using I2.Loc;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using System.Collections;

namespace Sportfaction.CasualFantasy.PVP.Gameplay.UI
{
    /// <summary>
    /// Class handling every match message
    /// </summary>
    /// @author Madeleine Tranier
    public sealed class MatchMessagesUI : Singleton<MatchMessagesUI>
    {
        #region Properties

        public bool PeriodTimerStopped { get { return _periodTimerStopped; } }

        #endregion

        #region Serialized Fields

        [Header("Infos + Goal UI")]
        [SerializeField, Tooltip("Phase announcer text")]
        private TMPro.TMP_Text  _infoMsgTxt             =   null;
        [SerializeField, Tooltip("Phase announcer text")]
        private TMPro.TMP_Text  _infoMsgBisTxt          =   null;
        [SerializeField, Tooltip("Resolution phase announcer text")]
        private TMPro.TMP_Text  _infoMsgResolutionTxt   =   null;
        [SerializeField, Tooltip("Actions left announcer text")]
        private TMPro.TMP_Text  _infoMsgActionsLeftTxt  =   null;
        [SerializeField, Tooltip("Possession indicator image")]
        private Image           _possessionAnimImg      =   null;

        [Header("Banners")]
        [SerializeField, Tooltip("Goal GO")]
        private GameObject  _goalScoredGO   =   null;
        [SerializeField, Tooltip("Toss BG")]
        private Image       _tossBG         =   null;
        [SerializeField, Tooltip("Toss GO")]
        private GameObject  _tossGO         =   null;
        [SerializeField, Tooltip("Victory GO")]
        private GameObject  _victoryMsgGO   =   null;
        [SerializeField, Tooltip("Defeat GO")]
        private GameObject  _defeatMsgGO    =   null;
        [SerializeField, Tooltip("Draw GO")]
        private GameObject  _drawMsgGO      =   null;

        [Header("Last actions")]
        [SerializeField, Tooltip("Header actions left text")]
        private TMPro.TMP_Text  _headerActionsLeftTxt   =   null;
        [SerializeField, Tooltip("Header animation")]
        private Animation       _headerAnim             =   null;
        [SerializeField, Tooltip("Goal opportunity info text")]
        private GameObject      _goalOpportunityInfoGO  =   null;
        [SerializeField, Tooltip("Goal opportunity sprite")]
        private GameObject      _goalOppImg             =   null;

        [SerializeField, Tooltip("Tutorial text")]
        private TMPro.TMP_Text  _tutorialTxt    =   null;

        [Header("Game Events")]
        [SerializeField, Tooltip("Event invoked when toss UI hidden")]
        private GameEvent   _tossUIHiddenEvt    =   null;

        [Header("References")]
        [SerializeField, Tooltip("Ref to currently focused player")]
        private PlayerPVPReference  _playerFocusedRef   =   null;

        #pragma warning disable 414, CS0649
        [Header("Configuration")]
        [SerializeField, Tooltip("Sprite config")]
        private Sprite[] _spriteConfig; //!< Array of every sprite that can be used by script

        #endregion

        #region Private Fields

        private ePeriod     _curPeriod          =   ePeriod.NONE;
        private int         _tossStep           =   0;
        private bool        _periodTimerStopped =   false;
        private FieldCase   _fc                 = null;
        private Transform   _msgAnchor          =   null;

        #endregion

        #region Public Methods

        #region Game Event Listeners

        public  void    OnEngineStartPeriod()
        {
            _periodTimerStopped = false;
            _curPeriod = MatchEngine.Instance.Engine.PeriodMain.Period;

            if (ePeriod.FIRST_PERIOD == _curPeriod && ePhase.TOSS_RESOLUTION == MatchEngine.Instance.CurPhase)
                MatchEngine.Instance.NextPhase();
            else if (ePeriod.SECOND_PERIOD == _curPeriod)
                NewPhaseAnnouncement(MatchEngine.Instance.Engine.PhaseMain.GetNextPhase());
        }

        public  void    OnPeriodTimerStopped()
        {
            _periodTimerStopped =   true;

            if (ePhase.ACTION == MatchEngine.Instance.CurPhase)
                OnActionFinished(true);
        }

        public  void    OnEngineStopPeriod()
        {
            _headerActionsLeftTxt.transform.parent.gameObject.SetActive(false);
            //_headerAnim.Play("Header_Shrink");

            _goalOpportunityInfoGO.GetComponent<Animation>().Play("Info_Msg_" + (UserData.Instance.PVPTeam == ActionsManager.Instance.CurTeamPossess ? "Right" : "Left") + "_Out");
            _goalOppImg.SetActive(false);

            _curPeriod = MatchEngine.Instance.Engine.PeriodMain.Period;
            _periodTimerStopped = false;

            if (ePeriod.HALF_TIME == _curPeriod)
            {
                UtilityMethods.SetText(_infoMsgTxt, LocalizationManager.GetTranslation("Half-Time Text"));
                _infoMsgTxt.transform.parent.GetChild(0).GetComponent<Image>().color = Color.black;
                _infoMsgTxt.color = Color.white;
                _infoMsgTxt.transform.parent.gameObject.SetActive(true);
            }
            else if (ePeriod.FULL_TIME == _curPeriod)
            {
                UtilityMethods.SetText(_infoMsgResolutionTxt, LocalizationManager.GetTranslation("Match-End Text"));
                _infoMsgResolutionTxt.color = Color.white;
                _infoMsgResolutionTxt.transform.parent.gameObject.SetActive(true);
            }
        }

        /// <summary>
        /// MatchEngine Event Listener > Phase Changed
        /// _engine.PhaseMain.OnPhaseChanged += pPhase => this.OnPhaseChanged(pPhase);
        /// </summary>
        public  void    OnPhaseChanged(ePhase pPhase)
        {
            switch (pPhase)
            {
                case ePhase.TOSS_RESOLUTION:
                    BeginToss();
                    break;

                case ePhase.KICK_OFF:
                case ePhase.ACTION:
                    OnActionFinished(true);
                    break;
            }
        }

        public  void    OnActionFinished(bool pPhaseStart = false)
        {
            //Debug.Log("OnActionFinished() " + MatchEngine.Instance.Engine.PhaseMain.GoalOpportunityZone + " | " + MatchEngine.Instance.Engine.PeriodMain.PeriodEndTurns);
            bool    lGOppActive =   _goalOpportunityInfoGO.activeSelf;
            eTeam   lCurPossess =   pPhaseStart ? ActionsManager.Instance.CurTeamPossess : MatchEngine.Instance.Engine.PossessionMain.CurTeamPossession;

            ActionsManager.Instance.UpdateOpportunityZone();

            if (_periodTimerStopped && MatchEngine.Instance.Engine.PhaseMain.GoalOpportunityZone)
            {
                if (!_headerActionsLeftTxt.transform.parent.gameObject.activeSelf && !_goalOpportunityInfoGO.activeSelf)
                    _headerActionsLeftTxt.transform.parent.gameObject.SetActive(true);
                //    _headerAnim.Play("Header_Grow");

                _goalOpportunityInfoGO.SetActive(true);

                string lTransKey = "header_goal_opportunity_info_" + (ePeriod.FIRST_PERIOD == _curPeriod ? "half_time" : "full_time");
                UtilityMethods.SetText(_headerActionsLeftTxt, LocalizationManager.GetTranslation(lTransKey));
                if (!lGOppActive)
                {
                    _fc = eTeam.TEAM_A == lCurPossess ? new FieldCase(37, 15) : new FieldCase(16, 15);
                    _goalOppImg.transform.position = Camera.main.WorldToScreenPoint(PlayersManager.Instance.CalculatePositionByCoordinates(_fc));
                    _goalOppImg.transform.DOScaleX(UserData.Instance.PVPTeam == lCurPossess ? -1f : 1f, 0f);
                    _goalOppImg.SetActive(true);
                }
            }
            else if (!MatchEngine.Instance.Engine.PhaseMain.GoalOpportunityZone)
                _goalOppImg.SetActive(false);

            if (_periodTimerStopped && !_goalOpportunityInfoGO.activeSelf)
            {
                if (!_headerActionsLeftTxt.transform.parent.gameObject.activeSelf)
                    _headerActionsLeftTxt.transform.parent.gameObject.SetActive(true);
                //    _headerAnim.Play("Header_Grow");

                string lHeaderTransKey = "header_actions_left_" + (ePeriod.FIRST_PERIOD == _curPeriod ? "half_time" : "full_time");
                UtilityMethods.SetText(_headerActionsLeftTxt, string.Format(LocalizationManager.GetTranslation(lHeaderTransKey), MatchEngine.Instance.Engine.PeriodMain.PeriodEndTurns));
            }
        }

        public  void    OnForfeit()
        {
            //string[] m_buttonsArray = new string[] { I2.Loc.LocalizationManager.GetTranslation("OkButton") };
            //Services.AlertDialogs.Instance.ShowAlert(
            //    I2.Loc.LocalizationManager.GetTranslation("ForfeitTitle"),
            //    I2.Loc.LocalizationManager.GetTranslation("ForfeitMessage"),
            //    m_buttonsArray, (_buttonPressed) =>
            //    { }
            //);
        }

        public  void    OnTutorialNextStep()
        {
            _tutorialTxt.transform.parent.gameObject.SetActive(false);
        }

        #endregion

        #region Toss Methods

        public  void    BeginToss()
        {
            UtilityMethods.SetText(_infoMsgTxt, "Toss");
            MatchAudio.Instance.OnMessageAppears();
            _tossGO.SetActive(true);
            _tossBG.gameObject.SetActive(true);
            _tossBG.DOFade(1f, 0f).SetDelay(1f);
        }

        public  void    DisplayResultToss()
        {
            _tossGO.SetActive(false);
            _tossBG.DOFade(0f, 0f).SetDelay(1f).OnComplete(() =>
            {
                _tossBG.gameObject.SetActive(false);
            });

            _infoMsgTxt.color = ActionsManager.Instance.ColorConfig.GetTeamColor(MatchEngine.Instance.Engine.TossMain.TeamWinning == UserData.Instance.PVPTeam);

            MatchAudio.Instance.OnMessageAppears();
            if (MatchEngine.Instance.Engine.TossMain.TeamWinning == UserData.Instance.PVPTeam)
            {
                UserEventsManager.Instance.RecordEvent(new UserActionEvent("manager_match_toss_won"));
                UtilityMethods.SetText(_infoMsgTxt, LocalizationManager.GetTranslation("TossWin"));
                _infoMsgTxt.transform.parent.gameObject.SetActive(true);
            }
            else
            {
                UserEventsManager.Instance.RecordEvent(new UserActionEvent("manager_match_toss_lost"));
                UtilityMethods.SetText(_infoMsgTxt, LocalizationManager.GetTranslation("TossLoss"));
                _infoMsgTxt.transform.parent.gameObject.SetActive(true);
            }
        }

        #endregion
        
        public  void    OnGoalScored()
        {
            _goalScoredGO.SetActive(true);
        }

        public  void    NewPhaseAnnouncement(ePhase pNextPhase, bool pForced = false)
        {
            if (_periodTimerStopped && !pForced && ePhase.ACTION == pNextPhase)
            {
                ActionsLeft();
                StartCoroutine(DelayBeforeNewPhaseAnnouncement(pNextPhase, 1.5f, true));
                return;
            }

            //Debug.Log("[MatchMessagesUI] NewPhaseAnnouncement() Current Phase = " + MatchEngine.Instance.Engine.PhaseMain.Phase + " | Next phase = " + pNextPhase);
            eTeam lTeamPossess = MatchEngine.Instance.Engine.PossessionMain.CurTeamPossession;
            bool lIsResolution = false;

            //_possessionAnimImg.sprite = UtilityMethods.GetSprite(_spriteConfig, "Arrow_BallPosition_" + (lTeamPossess == UserData.Instance.PVPTeam ? "Blue" : "Red"));
            _possessionAnimImg.sprite = UtilityMethods.GetSprite(_spriteConfig, "Arrow_BallPosition_Blue");
            _infoMsgTxt.color = Color.white;
            _infoMsgResolutionTxt.color = Color.white;
            _infoMsgTxt.transform.parent.GetComponent<RectTransform>().anchoredPosition = Vector2.zero;
            _infoMsgTxt.transform.parent.GetChild(0).GetComponent<Image>().color = Color.black;

            if (_infoMsgTxt.transform.parent.gameObject.activeSelf)
                _infoMsgTxt.transform.parent.GetComponent<Animation>().Stop();

            switch (pNextPhase)
            {
                case ePhase.KICK_OFF:
                    lIsResolution = true;
                    UtilityMethods.SetText(_infoMsgResolutionTxt, LocalizationManager.GetTranslation("KickOff"));
                    _infoMsgResolutionTxt.color = ActionsManager.Instance.ColorConfig.GetTeamColor(lTeamPossess == UserData.Instance.PVPTeam);
                    break;

                case ePhase.TACTICAL_ATTACK:
                    _infoMsgTxt.transform.parent.GetComponent<RectTransform>().anchoredPosition = new Vector2(0f, 100f);
                    _infoMsgTxt.transform.parent.GetChild(0).GetComponent<Image>().color = ActionsManager.Instance.ColorConfig.TacticalColor;
                    UtilityMethods.SetText(_infoMsgTxt, LocalizationManager.GetTranslation("tactical_phase_title"));
                    break;

                case ePhase.TACTICAL_ATTACK_RESOLUTION:
                case ePhase.TACTICAL_DEFENSE_RESOLUTION:
                    lIsResolution = true;
                    UtilityMethods.SetText(_infoMsgResolutionTxt, LocalizationManager.GetTranslation("tactical_resolution_phase_title"));
                    break;

                case ePhase.ACTION_RESOLUTION:
                    lIsResolution = true;
                    bool lIsDuelRes = (null != MatchEngine.Instance.GetDuelPlayer(eTeam.TEAM_A) && null != MatchEngine.Instance.GetDuelPlayer(eTeam.TEAM_B));
                    if (!lIsDuelRes)
                        _infoMsgResolutionTxt.color = ActionsManager.Instance.ColorConfig.GetTeamColor(ActionsManager.Instance.CurTeamPossess == UserData.Instance.PVPTeam);

                    UtilityMethods.SetText(_infoMsgResolutionTxt, LocalizationManager.GetTranslation("action_resolution_phase_title"));
                    break;
            }

            MatchAudio.Instance.OnMessageAppears();
            if (_infoMsgTxt.transform.parent.gameObject.activeSelf && !lIsResolution)
                _infoMsgTxt.transform.parent.GetComponent<Animation>().Play("InfoStripe");
            else
                _infoMsgTxt.transform.parent.gameObject.SetActive(!lIsResolution);

            _infoMsgResolutionTxt.transform.parent.gameObject.SetActive(lIsResolution);
        }

        public  void    Message(string pMsg)
        {
            _msgAnchor = null;
            _infoMsgBisTxt.transform.parent.localPosition = Vector3.zero;
            UtilityMethods.SetText(_infoMsgBisTxt, pMsg);
            _infoMsgBisTxt.transform.parent.gameObject.SetActive(true);
        }

        public  void    Message(string pMsg, Transform pTarget)
        {
            _msgAnchor = pTarget;
            UtilityMethods.SetText(_infoMsgBisTxt, pMsg);
            _infoMsgBisTxt.transform.parent.gameObject.SetActive(true);
        }

        public  void    ActionsLeft()
        {
            //Debug.Log("ActionsLeft ==> " + MatchEngine.Instance.Engine.PhaseMain.GoalOpportunityZone + " | PeriodEndTurns = " + MatchEngine.Instance.Engine.PeriodMain.PeriodEndTurns);
            if (MatchEngine.Instance.Engine.PhaseMain.GoalOpportunityZone)
            {
                string lTransKey = "header_goal_opportunity_info_" + (ePeriod.FIRST_PERIOD == _curPeriod ? "half_time" : "full_time");
                UtilityMethods.SetText(_infoMsgActionsLeftTxt, LocalizationManager.GetTranslation(lTransKey));
            }
            else
            {
                string lTranslationKey = "actions_left_" + (ePeriod.FIRST_PERIOD == _curPeriod ? "half_time" : "full_time") + (UserData.Instance.PVPTeam == MatchEngine.Instance.Engine.PossessionMain.CurTeamPossession ? "" : "_adv");
                UtilityMethods.SetText(_infoMsgActionsLeftTxt, string.Format(LocalizationManager.GetTranslation(lTranslationKey), MatchEngine.Instance.Engine.PeriodMain.PeriodEndTurns));
            }

            _infoMsgActionsLeftTxt.transform.parent.gameObject.SetActive(true);
        }

        public  void    EndInfoAnim()
        {
            ePhase  lNextPhase      =   MatchEngine.Instance.Engine.PhaseMain.GetNextPhase();
            bool    lIsSoloAction   =   null == MatchEngine.Instance.GetDuelPlayer(eTeam.TEAM_A) || null == MatchEngine.Instance.GetDuelPlayer(eTeam.TEAM_B);

            if (ePeriod.FIRST_PERIOD == _curPeriod || ePeriod.SECOND_PERIOD == _curPeriod)
            {
                TogglePossessionArrow(ePhase.ACTION == lNextPhase && (MatchEngine.Instance.Engine.PossessionMain.CurTeamPossession == UserData.Instance.PVPTeam || !lIsSoloAction));
                if (ePhase.KICK_OFF == lNextPhase)
                {
                    if (null != MatchManager.Instance)
                        StartCoroutine(MatchManager.Instance.NextGamePhase());
                }
                else if (ePhase.ACTION == lNextPhase)
                {
                    if (null != MatchEngine.Instance)
                        MatchEngine.Instance.NextPhase();
                }
                else if (ePhase.TACTICAL_DEFENSE_RESOLUTION == lNextPhase || ePhase.TACTICAL_ATTACK_RESOLUTION == lNextPhase)
                {
                    if (null != MatchManager.Instance)
                        StartCoroutine(MatchManager.Instance.NextResolutionPhase());
                }
                else if (ePhase.ACTION_RESOLUTION == lNextPhase || ePhase.TACTICAL_ATTACK == lNextPhase)
                {
                    if (null != MatchManager.Instance)
                        StartCoroutine(MatchManager.Instance.NextGamePhase());
                }
            }
            else if (ePhase.TOSS_RESOLUTION == MatchEngine.Instance.CurPhase && ePeriod.NONE == _curPeriod)
            {
                // Prepare dice animations for toss
                if (0 == _tossStep)
                {
                    PlayerResponse  lManagerA           =   new PlayerResponse();
                    lManagerA.ExecuteAction             =   new PlayerActionResponse();
                    lManagerA.Team = "Toss_Resolution_Anchor";
                    lManagerA.Position = eTeam.TEAM_A == UserData.Instance.PVPTeam ? "Blue" : "Red";
                    lManagerA.ExecuteAction.Score               =   MatchEngine.Instance.Engine.TossMain.ScoreTeamA;
                    lManagerA.ExecuteAction.ScoreWithPressure   =   MatchEngine.Instance.Engine.TossMain.ScoreTeamA;

                    PlayerResponse  lManagerB           =   new PlayerResponse();
                    lManagerB.ExecuteAction             =   new PlayerActionResponse();
                    lManagerB.Team                      =   "Toss_Resolution_Anchor";
                    lManagerB.Position                  =   eTeam.TEAM_A == UserData.Instance.PVPTeam ? "Red" : "Blue";
                    lManagerB.ExecuteAction.Score               =   MatchEngine.Instance.Engine.TossMain.ScoreTeamB;
                    lManagerB.ExecuteAction.ScoreWithPressure   =   MatchEngine.Instance.Engine.TossMain.ScoreTeamB;

                    if (ActionsManager.Instance.IsTester && null != MatchTutorial.Instance)
                    {
                        if (eTeam.TEAM_B == MatchTutorial.Instance.TutorialData.TossWinner)
                        {
                            lManagerA.ExecuteAction.Score = MatchEngine.Instance.Engine.TossMain.ScoreTeamB;
                            lManagerA.ExecuteAction.ScoreWithPressure = MatchEngine.Instance.Engine.TossMain.ScoreTeamB;

                            lManagerB.ExecuteAction.Score = MatchEngine.Instance.Engine.TossMain.ScoreTeamA;
                            lManagerB.ExecuteAction.ScoreWithPressure = MatchEngine.Instance.Engine.TossMain.ScoreTeamA;
                        }
                    }

                    GameObject          lNewFX_Gob          =   Instantiate(ActionsUI.Instance.ResolutionsManagerGrp_Gob, Vector3.zero, Quaternion.identity) as GameObject;
                    Resolutions_Manager lResolutionManager  =   lNewFX_Gob.GetComponent<Resolutions_Manager>();
                    lNewFX_Gob.transform.SetParent(GameObject.Find("UI_FX_Anchor_Point").transform, false);
                    lNewFX_Gob.name                 =   "Resolutions_Manager_Grp";
                    lNewFX_Gob.transform.localScale =   new Vector3(1.2f, 1.2f, 1.2f);

                    lResolutionManager.CurrentResolutionCase_Str    =   "Toss_Resolution";
                    lResolutionManager.CurrentMode_Str              =   eTeam.TEAM_A == UserData.Instance.PVPTeam ? "PC" : "PVP";

                    if (ActionsManager.Instance.IsTester && null != MatchTutorial.Instance)
                    {
                        lResolutionManager.ExecuteActionTeamBlue_Pr     =   lManagerA;
                        lResolutionManager.ExecuteActionTeamRed_Pr      =   lManagerB;
                    }
                    else
                    {
                        lResolutionManager.ExecuteActionTeamBlue_Pr     =   eTeam.TEAM_A == UserData.Instance.PVPTeam ? lManagerA : lManagerB;
                        lResolutionManager.ExecuteActionTeamRed_Pr      =   eTeam.TEAM_A == UserData.Instance.PVPTeam ? lManagerB : lManagerA;
                    }

                    _tossStep++;
                }
                else
                {
                    UtilityMethods.InvokeGameEvent(_tossUIHiddenEvt);
                }
            }
            else if (ePeriod.FULL_TIME == _curPeriod)
            {
                FXManager.Instance.DisplayResultFX();

                bool lIsManagerTeamA = eTeam.TEAM_A == UserData.Instance.PVPTeam;
                float lScoreA = MatchEngine.Instance.Engine.ScoreMain.TeamAScore;
                float lScoreB = MatchEngine.Instance.Engine.ScoreMain.TeamBScore;

                // Determine match result (win / draw / loss)
                bool lIsWinner = (lIsManagerTeamA && lScoreA > lScoreB) || (!lIsManagerTeamA && lScoreB > lScoreA);
                bool lIsLoser = (lIsManagerTeamA && lScoreA < lScoreB) || (!lIsManagerTeamA && lScoreB < lScoreA);

                if (lIsWinner)
                    _victoryMsgGO.SetActive(true);
                else if (lIsLoser)
                    _defeatMsgGO.SetActive(true);
                else
                    _drawMsgGO.SetActive(true);
            }
        }

        public  void    TogglePossessionArrow(bool pActive)
        {
            _possessionAnimImg.transform.parent.gameObject.SetActive(pActive);
        }

        public  void    MakeMsgBlink()
        {
            if (_goalOpportunityInfoGO.activeSelf)
                _goalOpportunityInfoGO.GetComponent<Animation>().Play("Info_Msg_Blink");
        }

        public  void    DisplayPopin(string pMsg, bool pDisplayNextBtn = false)
        {
            UtilityMethods.SetText(_tutorialTxt, pMsg);
            _tutorialTxt.transform.parent.gameObject.SetActive(true);
            _tutorialTxt.transform.parent.GetChild(1).gameObject.SetActive(pDisplayNextBtn);
        }

        public  void    DisplayPopin(TutorialIndication pIndication)
        {
            UtilityMethods.SetText(_tutorialTxt, pIndication.Message);
            _tutorialTxt.transform.parent.gameObject.SetActive(true);
            UtilityMethods.SetText(_tutorialTxt.transform.parent.GetChild(1).GetChild(0).gameObject.GetComponent<TMPro.TMP_Text>(), pIndication.BtnText);
            _tutorialTxt.transform.parent.GetChild(1).gameObject.SetActive(!string.IsNullOrEmpty(pIndication.BtnText));
        }

        public  IEnumerator DelayBeforeNewPhaseAnnouncement(ePhase pNextPhase, float pWaitingTime = 0.5f, bool pForced = false)
        {
            yield return new WaitForSeconds(pWaitingTime);
            NewPhaseAnnouncement(pNextPhase, pForced);
        }

        #endregion

        #region Private Methods

        private void    Start()
        {
            MatchEngine.Instance.Engine.PhaseMain.OnPhaseChanged += pPhase => OnPhaseChanged(pPhase);
            _possessionAnimImg.sprite = UtilityMethods.GetSprite(_spriteConfig, "Arrow_BallPosition_Blue");
        }

        private void    Update()
        {
            if (null != _possessionAnimImg && null != _playerFocusedRef.CurrentValue)
            {
                Vector3 lPlayerPos = _playerFocusedRef.CurrentValue.transform.position;
                //Vector3 lIndicatorPos = Camera.main.WorldToScreenPoint(lPlayerPos + new Vector3(lTeamPossess == UserData.Instance.PVPTeam ? -0.25f : 0.25f, 0.1f, 0f));
                Vector3 lIndicatorPos = Camera.main.WorldToScreenPoint(lPlayerPos + new Vector3(-0.25f, 0.1f, 0f));
                _possessionAnimImg.transform.parent.position = lIndicatorPos;
            }
            else if (null == _playerFocusedRef.CurrentValue)
                TogglePossessionArrow(false);

            if (_goalOppImg.activeSelf && null != _fc)
            {
                _goalOppImg.transform.position = Camera.main.WorldToScreenPoint(PlayersManager.Instance.CalculatePositionByCoordinates(_fc));
                _goalOppImg.SetActive(true);
            }

            if (_infoMsgBisTxt.isActiveAndEnabled && null != _msgAnchor)
                _infoMsgBisTxt.transform.parent.DOMoveY(Camera.main.WorldToScreenPoint(_msgAnchor.position).y + 20f, 0f);
        }

        #endregion
    }
}
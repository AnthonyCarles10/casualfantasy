﻿using Sportfaction.CasualFantasy.Manager.Profile;
using Sportfaction.CasualFantasy.PVP.Gameplay.Matchs;
using Sportfaction.CasualFantasy.Services.Userstracking;

using SportFaction.CasualFootballEngine.TeamService.Enum;

using DG.Tweening;
using UnityEngine;

namespace Sportfaction.CasualFantasy.PVP.UI
{
    /// <summary>
    /// Class handling first period recap
    /// </summary>
    /// @author Madeleine Tranier
    public class HalfTimeUI : MonoBehaviour
	{
		#region Fields

		[Header("UI Elements")]
		[SerializeField, Tooltip("Canvas Group")]
		private CanvasGroup _canvas =	null;

        #endregion

        #region Public Methods

		public	void	OnHalfTimeStarted()
		{
            bool    lIsManagerTeamA =   eTeam.TEAM_A == UserData.Instance.PVPTeam;
            float   lBlueTeamScore  =   MatchEngine.Instance.GetTeamScore(lIsManagerTeamA ? eTeam.TEAM_A : eTeam.TEAM_B);
            float   lRedTeamScore   =   MatchEngine.Instance.GetTeamScore(lIsManagerTeamA ? eTeam.TEAM_A : eTeam.TEAM_B);

            if(lBlueTeamScore > lRedTeamScore)
                UserEventsManager.Instance.RecordEvent(new UserActionEvent("manager_match_half_time_win"));
            else if(lBlueTeamScore < lRedTeamScore)
                UserEventsManager.Instance.RecordEvent(new UserActionEvent("manager_match_half_time_loose"));
            else
                UserEventsManager.Instance.RecordEvent(new UserActionEvent("manager_match_half_time_draw"));


            if((int)lBlueTeamScore == 0 && (int)lRedTeamScore == 0)
            {
                UserEventsManager.Instance.RecordEvent(new UserActionEvent("manager_match_half_time_draw_nogoal"));
            }

            _canvas.gameObject.SetActive(true);
			_canvas.DOFade(1f, 1f);
			_canvas.DOFade(0f, 1f).SetDelay(1f);
        }
        
        public	void	OnHalfTimeFinished()
		{
			_canvas.DOFade(0f, 1f).OnComplete(() => _canvas.gameObject.SetActive(false));
		}
		
		#endregion

		#region Private Methods

		/*private	void	_updateText()
		{
			_secondsRemaining	=	(_halfTimeTimerRef.CurrentValue / 1000) + 1;
			_uiText.text		=	_secondsRemaining.ToString();
		}*/

		#endregion
	}
}
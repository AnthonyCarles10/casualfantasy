﻿using Sportfaction.CasualFantasy.ArenaCup;
using Sportfaction.CasualFantasy.Config;
using Sportfaction.CasualFantasy.Events;
using Sportfaction.CasualFantasy.Manager.Profile;
using Sportfaction.CasualFantasy.PVP.Gameplay.Matchs;
using Sportfaction.CasualFantasy.Services.Userstracking;

using SportFaction.CasualFootballEngine.MatchService.Model;
using SportFaction.CasualFootballEngine.TeamService.Enum;

using DG.Tweening;
using I2.Loc;
using Photon.Pun;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Collections;
using Sportfaction.CasualFantasy.PVP.Gameplay.Players;

namespace Sportfaction.CasualFantasy.PVP.UI
{
    /// <summary>
    /// Class handling end of match recap
    /// </summary>
    #pragma warning disable 414, CS0649
    public	class	EndMatchUI : Singleton<EndMatchUI>
    {
		#region Properties
		
		public	bool	TeamForfeit             { set { _teamForfeit = value; } }
		
		#endregion
		
		#region Serialized Fields

		[Header("Links")]
        [SerializeField, Tooltip("UserMain")]
        private	UserMain	    _userMain	    =	null;

        [Header("UI Elements")]
        [SerializeField, Tooltip("End match CG")]
        private CanvasGroup     _endMatchCG         =   null;
        [SerializeField, Tooltip("Trophies CG")]
        private CanvasGroup     _leaderboardCG      =   null;
        [SerializeField, Tooltip("Soccer Cubes CG")]
        private CanvasGroup     _soccerCubesCG      =   null;
        [SerializeField, Tooltip("Go to menu btn")]
        private GameObject      _menuBtnGO          =   null;

        [Header("Leaderboard Block")]
        [SerializeField, Tooltip("Trophies value")]
        private TMPro.TMP_Text  _leaderboardTrophiesTxt     =   null;
        [SerializeField, Tooltip("Manager's rank")]
        private TMPro.TMP_Text  _leaderboardRankTxt         =   null;
        [SerializeField, Tooltip("Trophies won/lost")]
        private TMPro.TMP_Text  _leaderboardTrophiesWonTxt  =   null;

        [Header("Soccer Cubes Box")]
        [SerializeField, Tooltip("Nb lootboxes")]
        private TMPro.TMP_Text  _nbLootboxTxt               =   null;
        [SerializeField, Tooltip("Fragments won by playing")]
        private TMPro.TMP_Text  _matchResultTxt             =   null;
        [SerializeField, Tooltip("Fragments won for match played")]
        private TMPro.TMP_Text  _fragmentsMatchPlayedTxt    =   null;
        [SerializeField, Tooltip("Fragments won from match result")]
        private TMPro.TMP_Text  _fragmentsMatchResultTxt    =   null;
        [SerializeField, Tooltip("Fragments earned")]
        private TMPro.TMP_Text  _fragmentsEarned            =   null;
        [SerializeField, Tooltip("Fragments earned fill")]
        private Image           _fragmentsEarnedFill        =   null;

        [Header("Prefabs")]
        [SerializeField, Tooltip("Trophy animation")]
        private Animation   _trophyAnimPrefab   =   null;
        [SerializeField, Tooltip("Rank animation")]
        private Animation   _rankAnimPrefab     =   null;
        [SerializeField, Tooltip("Soccer Cubes animation")]
        private Animation   _soccerCubesPrefab  =   null;

        [Header("Game Events")]
		[SerializeField, Tooltip("Event to invoke to display tutorial")]
		private	GameEvent	_launchTutorialEvt	=	null;
		[SerializeField, Tooltip("Event to invoke to display loader")]
		private	GameEvent	_displayLoaderEvt	=	null;

		[Header("Configuration")]
        [SerializeField, Tooltip("Color config")]
		private	ColorConfig	_colorConfig	=	null;
		[SerializeField, Tooltip("Sprite config")]
		private	Sprite[]	_spriteConfig;

        #endregion

        #region Private Fields

        private bool    _isManagerTeamA =   true;
        private bool    _teamForfeit    =   false;

        private EndMatchInfos   _matchResultData    =   null;
        private bool    _endGameDataReceived    =   false;
        private bool    _arenaCupDataReceived   =   false;
        private bool    _userDataReceived       =   false;
        private bool    _matchResultDisplayed   =   false;

        private int     _previousFragments      =   0;
        private float   _previousRatio          =   0f;
        private float   _nbLootboxes            =   0;

        #endregion

        #region Public Methods

        /*public	void	RestartAGame()
        {
            StartCoroutine(LoadScene("PVPLoading", 1f));

            Sequence lSequence = DOTween.Sequence();
            lSequence.Append(_btnQuitTransform.DOLocalRotate(new Vector3(90f, -540f, 0), 1f, RotateMode.FastBeyond360));
            lSequence.Insert(0, _btnQuitTransform.DOLocalMove(_btnQuitInitPosition, 1f));
            lSequence.Insert(0, _btnQuitTransform.DOLocalRotate(new Vector3(90f, -540f, 0), 1f, RotateMode.FastBeyond360));
            lSequence.SetEase(Ease.InOutSine);
            lSequence.Play();
        }*/

        /// <summary>
        /// Handles display of tutorial | Redirection to menu
        /// </summary>
        public  void	GoToMenu()
        {
            _menuBtnGO.SetActive(false);
            // Reset Engine Data
            MatchEngine.Instance.InitEngine();

            UserEventsManager.Instance.RecordEvent(new UserActionEvent("end_pvp_game_menu"));

            if (true == PhotonNetwork.IsConnected && true == PhotonNetwork.InRoom)
            {
                PhotonNetwork.DestroyPlayerObjects(PhotonNetwork.LocalPlayer);
                PhotonNetwork.LeaveRoom();
            }

            MatchTimers.Instance.ResetPeriodTimers();

            if (null == UserData.Instance || UserData.Instance.Profile.OnBoarding)
            {
                UtilityMethods.InvokeGameEvent(_launchTutorialEvt);

                UserData.Instance.Profile.OnBoarding	=	false;

                UserData.Instance.PersistUserData();

                _userMain.SetOnBoardingCompleted();
            }
            UtilityMethods.InvokeGameEvent(_displayLoaderEvt);

        	//SceneManager.LoadScene("Meta");
            StartCoroutine(ScenesMain.Instance.UnLoadAsyncGameplayScene());
        }
		
		/// <summary>
		/// Handles end of match
		/// </summary>
        public	void	OnFullTimeStarted()
        {
            PlayersManager.Instance.TogglePlayerCollider(false);
			UserData.Instance.Profile.LastSceneGameplay	=	true;
			
        	if (_teamForfeit)
        	{
				OnTeamProclaimForfeit();
				return;
        	}

            if (UserData.Instance.Profile.NumberMatchPlayed <= 10)
            {
                UserEventsManager.Instance.RecordEvent(new UserActionEvent("end_match_" + UserData.Instance.Profile.NumberMatchPlayed));
            }

            float   lBlueTeamScore  =   MatchEngine.Instance.GetTeamScore(_isManagerTeamA ? eTeam.TEAM_A : eTeam.TEAM_B);
            float   lRedTeamScore   =   MatchEngine.Instance.GetTeamScore(_isManagerTeamA ? eTeam.TEAM_B : eTeam.TEAM_A);

            if(lBlueTeamScore > lRedTeamScore)
                UserEventsManager.Instance.RecordEvent(new UserActionEvent("manager_match_win"));
            else if(lBlueTeamScore < lRedTeamScore)
                UserEventsManager.Instance.RecordEvent(new UserActionEvent("manager_match_loose"));
            else
                UserEventsManager.Instance.RecordEvent(new UserActionEvent("manager_match_draw"));

            if ((int)lBlueTeamScore == 0 && (int)lRedTeamScore == 0)
            {
                UserEventsManager.Instance.RecordEvent(new UserActionEvent("manager_match_draw_nogoal"));
            }
        }
        
		/// <summary>
		/// Method called when team A proclaim forfeit
		/// </summary>
		public	void	OnTeamProclaimForfeit()
		{
            // Tracking OB end match
            UserEventsManager.Instance.RecordEvent(new UserActionEvent("end_pvp_game"));

            UserEventsManager.Instance.RecordEvent(new UserActionEvent("end_match_" + UserData.Instance.Profile.NumberMatchPlayed));

            float   lBlueTeamScore  =   MatchEngine.Instance.GetTeamScore(_isManagerTeamA ? eTeam.TEAM_A : eTeam.TEAM_B);
            float   lRedTeamScore   =   MatchEngine.Instance.GetTeamScore(_isManagerTeamA ? eTeam.TEAM_B : eTeam.TEAM_A);

            if(lBlueTeamScore > lRedTeamScore)
                UserEventsManager.Instance.RecordEvent(new UserActionEvent("manager_match_win"));
            else if(lBlueTeamScore < lRedTeamScore)
                UserEventsManager.Instance.RecordEvent(new UserActionEvent("manager_match_loose"));
            else
                UserEventsManager.Instance.RecordEvent(new UserActionEvent("manager_match_draw"));
		}

        public  void    DisplayResults()
        {
            int     lBlueTeamScore  =   (int)MatchEngine.Instance.GetTeamScore(_isManagerTeamA ? eTeam.TEAM_A : eTeam.TEAM_B);
            int     lRedTeamScore   =   (int)MatchEngine.Instance.GetTeamScore(_isManagerTeamA ? eTeam.TEAM_B : eTeam.TEAM_A);
            bool    lBlueTeamWon    =   lBlueTeamScore > lRedTeamScore;
            bool    lDraw           =   lBlueTeamScore == lRedTeamScore;
            string  lResult         =   LocalizationManager.GetTranslation(lBlueTeamScore > lRedTeamScore ? "outro_match_won" : "outro_match_lost");
            if (lDraw)
                lResult =   LocalizationManager.GetTranslation("outro_match_draw");

            _matchResultData    =   MatchEngine.Instance.Engine.MatchMain.EndMatchInfos;

            #if DEVELOPMENT_BUILD
            Debug.Log("## MATCH RESULT DATA ##");
            Debug.Log(MatchEngine.Instance.Engine.MatchMain.EndMatchInfos.ConvertToJson());
            #endif

            if (null == _matchResultData)
                return;

            int     lTrophiesWon            =   _isManagerTeamA ? _matchResultData.MemberAScoreDiff : _matchResultData.MemberBScoreDiff;
            int     lLeaderboardTrophies    =   _isManagerTeamA ? _matchResultData.MemberANewScore : _matchResultData.MemberBNewScore;
            int     lACTrophies             =   _isManagerTeamA ? _matchResultData.MemberAArenaCupNewScore : _matchResultData.MemberBArenaCupNewScore;
            int     lRanksWon               =   _isManagerTeamA ? _matchResultData.MemberARankDiff : _matchResultData.MemberBRankDiff;
            int     lNewRank                =   _isManagerTeamA ? _matchResultData.MemberANewRank : _matchResultData.MemberBNewRank;
            int     lNewRankAC              =   _isManagerTeamA ? _matchResultData.MemberAArenaCupNewRank : _matchResultData.MemberBArenaCupNewRank;
            int     lRanksACWon             =   _isManagerTeamA ? _matchResultData.MemberAArenaCupRankDiff : _matchResultData.MemberBArenaCupRankDiff;

            // Leaderboard Block
            UtilityMethods.SetText(_leaderboardTrophiesTxt, (lLeaderboardTrophies - lTrophiesWon).ToString());
            UtilityMethods.SetText(_leaderboardRankTxt, "#" + (lNewRank + lRanksWon));
            UtilityMethods.SetText(_leaderboardTrophiesWonTxt, lTrophiesWon > 0 ? ("+" + lTrophiesWon) : lTrophiesWon.ToString());
            _leaderboardTrophiesWonTxt.color = lTrophiesWon >= 0 ? _colorConfig.TrophiesValue : _colorConfig.BadScore;

            // SoccerCubes Block
            int lFragmentsPlayed    =   _isManagerTeamA ? _matchResultData.MemberAFragmentsPlayed : _matchResultData.MemberBFragmentsPlayed;
            int lFragmentsWon       =   _isManagerTeamA ? _matchResultData.MemberAFragmentsWon : _matchResultData.MemberBFragmentsWon;
            int lFragmentsDraw      =   _isManagerTeamA ? _matchResultData.MemberAFragmentsDraw : _matchResultData.MemberBFragmentsDraw;
            int lFragmentsToDisplay =   lBlueTeamWon ? lFragmentsWon : 0;
            if (lDraw)
                lFragmentsToDisplay =   lFragmentsDraw;

            UtilityMethods.SetText(_fragmentsMatchPlayedTxt, lFragmentsPlayed.ToString());
            UtilityMethods.SetText(_fragmentsMatchResultTxt, lFragmentsToDisplay.ToString());
            UtilityMethods.SetText(_matchResultTxt, lResult);
            UtilityMethods.SetText(_nbLootboxTxt, ((int)(UserData.Instance.Profile.Fragment / 100)).ToString());

            _previousFragments  =   UserData.Instance.Profile.Fragment;
            _previousRatio      =   UserData.Instance.Profile.Fragment % 100f;

            UtilityMethods.SetText(_fragmentsEarned, _previousRatio + "/100");
            _fragmentsEarnedFill.fillAmount =   _previousRatio / 100f;

            _endGameDataReceived    =   true;
            _userMain.GetProfile();
        }

        public  void    DisplayFragments()
        {
            _userDataReceived   =   true;
            DisplayMatchOutro();
        }

        public  void    MatchResultDisplayed()
        {
            _matchResultDisplayed   =   true;
            DisplayMatchOutro();
        }

        public  void    DisplayMatchOutro()
        {
            if (!_endGameDataReceived || !_userDataReceived || !_matchResultDisplayed)
                return;

			_endMatchCG.gameObject.SetActive(true);
            _endMatchCG.DOFade(1f, 1f).OnComplete(_displayLeaderboardBlock);
        }

        public  void    AnimateLeaderboardBlock()
        {
            _leaderboardTrophiesWonTxt.transform.parent.gameObject.SetActive(true);

            Sequence lLeaderboardSequence = DOTween.Sequence();
            int lTrophiesWon = _isManagerTeamA ? _matchResultData.MemberAScoreDiff : _matchResultData.MemberBScoreDiff;
            int lLeaderboardTrophies = _isManagerTeamA ? _matchResultData.MemberANewScore : _matchResultData.MemberBNewScore;
            int lRanksWon = _isManagerTeamA ? _matchResultData.MemberARankDiff : _matchResultData.MemberBRankDiff;
            int lNewRank = _isManagerTeamA ? _matchResultData.MemberANewRank : _matchResultData.MemberBNewRank;

            lLeaderboardSequence.Append(
                _leaderboardTrophiesWonTxt.transform.parent.DOPunchScale(new Vector3(1.2f, 1.2f, 1f), 0.5f, 2)
                    .OnStart(() => {
                        if (lTrophiesWon > 0)
                            MatchAudio.Instance.RewardWinAppears();
                        else
                            MatchAudio.Instance.RewardLoseAppears();
                        _leaderboardTrophiesWonTxt.transform.parent.GetComponent<CanvasGroup>().DOFade(1f, 0.5f);
                    })
            );

            int lNbTrophies = (lLeaderboardTrophies - lTrophiesWon);
            int lFormerRank = lNewRank + lRanksWon;

            lLeaderboardSequence.Append(
                DOTween.To(() => lNbTrophies, x => lNbTrophies = x, lLeaderboardTrophies, 1f)
                    .SetEase(Ease.Linear)
                    .OnStart(() => {
                        _leaderboardTrophiesTxt.transform.DOScale(new Vector3(2f, 2f, 1f), 0.5f);
                        StartCoroutine(_animCup(_leaderboardCG.transform, 0f));
                    })
                    .OnUpdate(() => {
                        UtilityMethods.SetText(_leaderboardTrophiesTxt, lNbTrophies.ToString());
                        UtilityMethods.SetText(_leaderboardTrophiesWonTxt, (lLeaderboardTrophies - lNbTrophies).ToString());
                    })
            );

            lLeaderboardSequence.Append(
                _leaderboardTrophiesWonTxt.transform.parent.GetComponent<CanvasGroup>().DOFade(0f, 0.5f)
                    .OnStart(() => {
                        _leaderboardTrophiesTxt.transform.DOScale(new Vector3(1f, 1f, 1f), 0.5f);
                    })
            );

            if (lRanksWon != 0)
            {
                lLeaderboardSequence.Append(
                    DOTween.To(() => lFormerRank, x => lFormerRank = x, lNewRank, 1f)
                        .SetEase(Ease.Linear)
                        .OnStart(() =>
                        {
                            _leaderboardRankTxt.transform.DOScale(new Vector3(2f, 2f, 1f), 0.5f);
                            MatchAudio.Instance.OutroArrows(lRanksWon > 0);
                            StartCoroutine(_animArrow(_leaderboardCG.transform, 9f));
                        })
                        .OnUpdate(() => {
                            UtilityMethods.SetText(_leaderboardRankTxt, "#" + lFormerRank.ToString());
                        })
                );

                lLeaderboardSequence.Append(_leaderboardRankTxt.transform.DOScale(new Vector3(1f, 1f, 1f), 0.5f));
            }

            lLeaderboardSequence.Append(
                _leaderboardCG.GetComponent<RectTransform>().DOAnchorPos(new Vector2(-470f, 0f), 1f)
                    .OnStart(MatchAudio.Instance.BoardMoves)
            );

            lLeaderboardSequence.Play().OnComplete(_displaySoccerCubesBlock);
        }

        public  void    AnimateSoccerCubesBlock()
        {
            _fragmentsMatchPlayedTxt.transform.parent.gameObject.SetActive(true);

            Sequence    lSoccerCubesSequence   =   DOTween.Sequence();
            int     lBlueTeamScore  =   (int)MatchEngine.Instance.GetTeamScore(_isManagerTeamA ? eTeam.TEAM_A : eTeam.TEAM_B);
            int     lRedTeamScore   =   (int)MatchEngine.Instance.GetTeamScore(_isManagerTeamA ? eTeam.TEAM_B : eTeam.TEAM_A);
            bool    lBlueTeamWon    =   lBlueTeamScore > lRedTeamScore;
            bool    lDraw           =   lBlueTeamScore == lRedTeamScore;
            int lFragmentsPlayed    =   _isManagerTeamA ? _matchResultData.MemberAFragmentsPlayed : _matchResultData.MemberBFragmentsPlayed;
            int lFragmentsWon       =   _isManagerTeamA ? _matchResultData.MemberAFragmentsWon : _matchResultData.MemberBFragmentsWon;
            int lFragmentsDraw      =   _isManagerTeamA ? _matchResultData.MemberAFragmentsDraw : _matchResultData.MemberBFragmentsDraw;
            int lFragmentsToDisplay =   lBlueTeamWon ? lFragmentsWon : 0;
            if (lDraw)
                lFragmentsToDisplay =   lFragmentsDraw;

            _fragmentsMatchPlayedTxt.transform.parent.localScale = new Vector3(1.2f, 1.2f, 1f);
            lSoccerCubesSequence.Append(
                _fragmentsMatchPlayedTxt.transform.parent.DOScale(new Vector3(1f, 1f, 1f), 0.5f)
                    .OnStart(() => _fragmentsMatchPlayedTxt.transform.parent.GetComponent<CanvasGroup>().DOFade(1f, 0.5f))
            );
            
            int lNbFragments    =   _previousFragments;
            int lNbFragmentsBis =   _previousFragments + lFragmentsPlayed;

            lSoccerCubesSequence.Append(
                DOTween.To(() => lNbFragments, x => lNbFragments = x, _previousFragments + lFragmentsPlayed, 1f)
                    .SetEase(Ease.Linear)
                    .OnStart(() =>
                    {
                        MatchAudio.Instance.OutroSoccerCubes();
                        StartCoroutine(_animCubes(_soccerCubesCG.transform));
                    })
                    .OnUpdate(() => {
                        UtilityMethods.SetText(_fragmentsEarned, (lNbFragments % 100) + "/100");
                        UtilityMethods.SetText(_fragmentsMatchPlayedTxt, (_previousFragments + lFragmentsPlayed - lNbFragments).ToString());
                        _fragmentsEarnedFill.fillAmount =   (lNbFragments % 100) / 100f;

                        if (0 == lNbFragments % 100 && 0 != _previousFragments % 100)
                        {
                            MatchAudio.Instance.OutroBoxWon();
                            _nbLootboxTxt.transform.parent.parent.GetComponent<Animation>().Play("Lootbox_Update");
                            _nbLootboxes    =   lNbFragments / 100;
                        }
                    })
                    .OnComplete(() => {
                        _fragmentsMatchPlayedTxt.color = Color.white;
                        _fragmentsMatchPlayedTxt.transform.parent.GetChild(0).GetComponent<TMPro.TMP_Text>().color = Color.white;
                    })
            );

            if (lFragmentsToDisplay > 0)
            {
                lSoccerCubesSequence.Append(
                    _fragmentsMatchPlayedTxt.transform.parent.GetComponent<RectTransform>().DOAnchorPosY(10f, 0.5f)
                );

                _fragmentsMatchResultTxt.transform.parent.localScale = new Vector3(1.2f, 1.2f, 1f);
                lSoccerCubesSequence.Append(
                    _fragmentsMatchResultTxt.transform.parent.DOScale(new Vector3(1f, 1f, 1f), 0.5f)
                        .OnStart(() =>
                        {
                            _fragmentsMatchResultTxt.transform.parent.gameObject.SetActive(true);
                            _fragmentsMatchResultTxt.transform.parent.GetComponent<CanvasGroup>().DOFade(1f, 0.5f);
                        })
                );

                lSoccerCubesSequence.Append(
                    DOTween.To(() => lNbFragmentsBis, x => lNbFragmentsBis = x, _previousFragments + lFragmentsPlayed + lFragmentsToDisplay, 1f)
                        .SetEase(Ease.Linear)
                        .OnStart(() =>
                        {
                            MatchAudio.Instance.OutroSoccerCubes();
                            StartCoroutine(_animCubes(_soccerCubesCG.transform));
                        })
                        .OnUpdate(() => {
                            UtilityMethods.SetText(_fragmentsEarned, (lNbFragmentsBis % 100) + "/100");
                            UtilityMethods.SetText(_fragmentsMatchResultTxt, (_previousFragments + lFragmentsPlayed + lFragmentsToDisplay - lNbFragmentsBis).ToString());
                            _fragmentsEarnedFill.fillAmount = (lNbFragmentsBis % 100) / 100f;

                            if (0 == lNbFragmentsBis % 100 && 0 != (_previousFragments + lFragmentsPlayed) % 100)
                            {
                                MatchAudio.Instance.OutroBoxWon();
                                _nbLootboxTxt.transform.parent.parent.GetComponent<Animation>().Play("Lootbox_Update");
                                _nbLootboxes = lNbFragmentsBis / 100;
                            }
                        })
                        .OnComplete(() => {
                            _fragmentsMatchResultTxt.color = Color.white;
                            _matchResultTxt.color = Color.white;
                        })
                );

                //lSoccerCubesSequence.Append(_fragmentsMatchResultTxt.transform.parent.GetComponent<CanvasGroup>().DOFade(0f, 0.5f));
            }

            lSoccerCubesSequence.Append(
                _soccerCubesCG.GetComponent<RectTransform>().DOAnchorPos(new Vector2(470f, 0f), 1f)
                    .OnStart(MatchAudio.Instance.BoardMoves)
            );

            lSoccerCubesSequence.Play().OnComplete(() => _menuBtnGO.SetActive(true));
        }

        public  void    UpdateFragments()
        {
            MatchAudio.Instance.OutroBoxPlusOne();
            UtilityMethods.SetText(_nbLootboxTxt, _nbLootboxes.ToString());
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Initializes UI for team name
        /// </summary>
        private void	Start()
        {
            if (null != UserData.Instance)
                _isManagerTeamA   =   UserData.Instance.PVPTeam == eTeam.TEAM_A;
        }

        private void    _displayLeaderboardBlock()
        {
            _leaderboardCG.GetComponent<RectTransform>().anchoredPosition = Vector2.zero;
            _leaderboardCG.gameObject.SetActive(true);
            MatchAudio.Instance.BoardAppears();
            _leaderboardCG.transform.localScale =   new Vector3(1.5f, 1.5f, 1f);
            _leaderboardCG.transform.DOScale(new Vector3(1f, 1f, 1f), 0.5f);
            _leaderboardCG.DOFade(1f, 0.5f).OnComplete(AnimateLeaderboardBlock);
        }

        private void    _displaySoccerCubesBlock()
        {
            _soccerCubesCG.GetComponent<RectTransform>().anchoredPosition   =   Vector2.zero;
            _soccerCubesCG.gameObject.SetActive(true);
            MatchAudio.Instance.BoardAppears();
            _soccerCubesCG.transform.localScale = new Vector3(1.5f, 1.5f, 1f);
            _soccerCubesCG.transform.DOScale(new Vector3(1f, 1f, 1f), 0.5f);
            _soccerCubesCG.DOFade(1f, 0.5f).OnComplete(AnimateSoccerCubesBlock);
        }

        private IEnumerator _animCup(Transform pParent, float pPosY)
        {
            int     lBlueTeamScore  =   (int)MatchEngine.Instance.GetTeamScore(_isManagerTeamA ? eTeam.TEAM_A : eTeam.TEAM_B);
            int     lRedTeamScore   =   (int)MatchEngine.Instance.GetTeamScore(_isManagerTeamA ? eTeam.TEAM_B : eTeam.TEAM_A);
            bool    lBlueTeamWon    =   lBlueTeamScore > lRedTeamScore;
            bool    lDraw           =   lBlueTeamScore == lRedTeamScore;

            for (int li = 0; li < 5; li++)
            {
                GameObject lTrophy = Instantiate(_trophyAnimPrefab.gameObject);
                lTrophy.transform.SetParent(pParent, false);
                lTrophy.GetComponent<RectTransform>().anchoredPosition = new Vector2(0f, pPosY);
                lTrophy.GetComponent<Animation>().Play((lBlueTeamWon || lDraw) ? "Outro_Win_Cup" : "Outro_Lose_Cup");

                yield return new WaitForSeconds(0.25f);
            }
        }
        
        private IEnumerator _animArrow(Transform pParent, float pPosY)
        {
            int     lBlueTeamScore  =   (int)MatchEngine.Instance.GetTeamScore(_isManagerTeamA ? eTeam.TEAM_A : eTeam.TEAM_B);
            int     lRedTeamScore   =   (int)MatchEngine.Instance.GetTeamScore(_isManagerTeamA ? eTeam.TEAM_B : eTeam.TEAM_A);
            bool    lBlueTeamWon    =   lBlueTeamScore > lRedTeamScore;
            bool    lDraw           =   lBlueTeamScore == lRedTeamScore;

            for (int li = 0; li < 1; li++)
            {
                GameObject lRank = Instantiate(_rankAnimPrefab.gameObject);
                lRank.transform.SetParent(pParent, false);
                lRank.GetComponent<RectTransform>().anchoredPosition = new Vector2(-13f, pPosY);
                lRank.GetComponent<Animation>().Play((lBlueTeamWon || lDraw) ? "Outro_Win_Arrow" : "Outro_Lose_Arrow");

                yield return new WaitForSeconds(0.5f);
            }
        }
        
        private IEnumerator _animCubes(Transform pParent)
        {
            for (int li = 0; li < 5; li++)
            {
                GameObject lCube = Instantiate(_soccerCubesPrefab.gameObject);
                lCube.transform.SetParent(pParent, false);

                yield return new WaitForSeconds(0.2f);
            }
        }

        #endregion
    }
}
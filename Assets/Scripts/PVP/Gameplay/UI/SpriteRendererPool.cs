﻿using Sportfaction.CasualFantasy.Utilities;

using UnityEngine;

namespace Sportfaction.CasualFantasy.PVP.Gameplay
{
    /// <summary>
    /// Class handling pool of sprite renderer components
    /// </summary>
    public sealed class SpriteRendererPool : AComponentsPool<SpriteRenderer>
    {
        #region Public Methods

        /// <summary>
        /// Specific constructor
        /// </summary>
        /// <param name="pPrefab">Prefab to create</param>
        /// <param name="pParent">Parent for all cards created</param>
        /// <param name="pMin">Minimum of cards to create</param>
        public  SpriteRendererPool(GameObject pPrefab, Transform pParent, uint pMin)
        : base(pPrefab, pParent, pMin)
        {
        }

        /// <summary>
        /// Get an unused card
        /// </summary>
        /// <returns>A disable card</returns>
        public  SpriteRenderer  GetUnusedRenderer()
        {
            return GetUnusedComponent();
        }

        #endregion
    }
}
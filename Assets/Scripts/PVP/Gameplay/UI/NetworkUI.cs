﻿

using System.Collections;
using DG.Tweening;
using UnityEngine;

namespace Sportfaction.CasualFantasy.PVP.UI
{
    /// <summary>
    /// Description: 
    /// Author: Antoine de Lachèze-Murel
    /// </summary>
    public sealed class NetworkUI : MonoBehaviour
    {

        public static NetworkUI Instance;

        #region Fields

        [SerializeField]
		private	GameObject _reconnectGrp;
        [SerializeField]
        private GameObject _isWaitingGrp;
        [SerializeField]
        private GameObject _isDisconnectedGrp;
        [SerializeField]
        private GameObject _isReconnectedGrp;

        #endregion

        public void ShowNeedToReconnectGrp()
        {
            _reconnectGrp.SetActive(true);
        }

        public void HideNeedToReconnectGrp()
        {
            _reconnectGrp.SetActive(false);
        }
        
        public void ShowIsWaitingGrp()
        {
            _isWaitingGrp.SetActive(true);

            RectTransform lRect = _isWaitingGrp.GetComponent<RectTransform>();
            float lWidth = lRect.sizeDelta.x;
            lRect.position = new Vector3(lWidth, lRect.position.y, lRect.position.z);

            Sequence lSequence = DOTween.Sequence();
            lSequence.Insert(0, lRect.DOAnchorPosX(0, 1f));

		}

        public IEnumerator HideIsWaitingGrp()
        {
            RectTransform lRect = _isWaitingGrp.GetComponent<RectTransform>();
            float lWidth = lRect.sizeDelta.x;
            lRect.position = new Vector3(lWidth, lRect.position.y, lRect.position.z);

            Sequence lSequence = DOTween.Sequence();
            lSequence.Insert(0, lRect.DOAnchorPosX(lWidth, 1f));

			yield return new WaitForSeconds(1f);

			_isWaitingGrp.SetActive(false);
		}

        public IEnumerator ShowIsDisconnectedGrp()
        {

            _isDisconnectedGrp.SetActive(true);

            RectTransform lRect = _isDisconnectedGrp.GetComponent<RectTransform>();
            float lWidth = lRect.sizeDelta.x;

            lRect.position = new Vector3(lWidth, lRect.position.y, lRect.position.z);

            Sequence lSequence = DOTween.Sequence();
            lSequence.Insert(0, lRect.DOAnchorPosX(0, 1f));
            lSequence.Insert(2f, lRect.DOAnchorPosX(lWidth, 1f));

			yield return new WaitForSeconds(2f);

			_isDisconnectedGrp.SetActive(false);
        }


        public IEnumerator ShowIsReconnectedGrp()
        {
            _isReconnectedGrp.SetActive(true);

            RectTransform lRect = _isReconnectedGrp.GetComponent<RectTransform>();
            float lWidth = lRect.sizeDelta.x;

            lRect.position = new Vector3(lWidth, lRect.position.y, lRect.position.z);

            Sequence lSequence = DOTween.Sequence();
            lSequence.Insert(0, lRect.DOAnchorPosX(0, 1f));
            lSequence.Insert(2f, lRect.DOAnchorPosX(lWidth, 1f));

            yield return new WaitForSeconds(2f);

            _isReconnectedGrp.SetActive(false);
        }


        void Awake()
        {
            Instance = this;
        }

        
    }
}
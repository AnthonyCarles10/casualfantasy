﻿using Sportfaction.CasualFantasy.Events;
using Sportfaction.CasualFantasy.Config;
using Sportfaction.CasualFantasy.Manager.Profile;
using Sportfaction.CasualFantasy.PVP.Gameplay.Actions;
using Sportfaction.CasualFantasy.PVP.Gameplay.Matchs;
using Sportfaction.CasualFantasy.PVP.Gameplay.Players;
using Sportfaction.CasualFantasy.PVP.Network;
using Sportfaction.CasualFantasy.Services.Userstracking;

using SportFaction.CasualFootballEngine.ActionService.Enum;
using SportFaction.CasualFootballEngine.ActionService.Response;
using SportFaction.CasualFootballEngine.PeriodService.Enum;
using SportFaction.CasualFootballEngine.PhaseService.Enum;
using SportFaction.CasualFootballEngine.PlayerService.Enum;
using SportFaction.CasualFootballEngine.PlayerService.Model;
using SportFaction.CasualFootballEngine.TeamService.Enum;

using Photon.Pun;
using System.Collections;
using UnityEngine;
using I2.Loc;

namespace Sportfaction.CasualFantasy.PVP.Gameplay.UI
{
    /// <summary>
    /// Class handling every phase resolution during a match
    /// </summary>
    /// @author Madeleine Tranier
    public sealed class OppositionUI : Singleton<OppositionUI>
    {
        #region Serialized Fields

        [Header("Game events")]
        [SerializeField, Tooltip("Event to invoke when Team A scores goal")]
        private GameEvent   _teamAScoredGoalEvt =   null;
        [SerializeField, Tooltip("Event to invoke when Team B scores goal")]
        private GameEvent   _teamBScoredGoalEvt =   null;
        [SerializeField, Tooltip("Event to invoke when action is finished")]
        private GameEvent   _actionFinishedEvt  =   null;
        [SerializeField, Tooltip("Event to invoke to stop period")]
        private GameEvent   _stopPeriodEvt      =   null;

        [Header("Configuration")]
        [SerializeField, Tooltip("Timing config")]
        private TimingConfig    _timingConfig   =   null;

        #endregion

        #region Public Methods

        #region Game Event Listeners

        public  void    OnEngineExecuteActions()
        {
            //UtilityMethods.DebugData("RESPONSE_EXECUTION");
            ActionsManager.Instance.DuelPlayer?.UI.ToggleBubble(false);
            ActionsManager.Instance.DuelOppPlayer?.UI.ToggleBubble(false);
            ActionsManager.Instance.DuelOppPlayer?.UI.ToggleTarget(false);

            SoccerBall.Instance.FollowPlayer(false);

            switch (MatchEngine.Instance.CurPhase)
            {
                case ePhase.PERIOD_INIT:
                    StartCoroutine(_launchResolution());
                    break;

                case ePhase.ACTION_RESOLUTION:
                    if ("NONE" == MatchEngine.Instance.Engine.ActionMain.Response.ResolutionAction)
                        OnDuelFinished();
                    else
                        StartCoroutine(_launchResolution());

                    break;
            }
        }

        #endregion

        public  void    ExecuteActionResolution()
        {
            //UtilityMethods.DebugData("EXECUTION");

            // Set Response Team Blue & Team Red
            int lDuelPlayerAIndex = -1;
            int lDuelPlayerBIndex = -1;

            for (int li = 0; li < MatchEngine.Instance.ResponseAction.TeamA.Players.Count; li++)
            {
                if (
                    eAction.MOVE.ToString() != MatchEngine.Instance.ResponseAction.TeamA.Players[li].Action
                    && eAction.DUEL_ATTACK.ToString() != MatchEngine.Instance.ResponseAction.TeamA.Players[li].Action
                    && eAction.DUEL_DEFENSE.ToString() != MatchEngine.Instance.ResponseAction.TeamA.Players[li].Action
                )
                    lDuelPlayerAIndex = li;
            }
            for (int li = 0; li < MatchEngine.Instance.ResponseAction.TeamB.Players.Count; li++)
            {
                if (
                    eAction.MOVE.ToString() != MatchEngine.Instance.ResponseAction.TeamB.Players[li].Action
                    && eAction.DUEL_ATTACK.ToString() != MatchEngine.Instance.ResponseAction.TeamB.Players[li].Action
                    && eAction.DUEL_DEFENSE.ToString() != MatchEngine.Instance.ResponseAction.TeamB.Players[li].Action
                )
                    lDuelPlayerBIndex = li;
            }

            PlayerResponse lResponseExecuteActionTeamBlue = (lDuelPlayerAIndex > -1) ? MatchEngine.Instance.ResponseAction.TeamA.Players[lDuelPlayerAIndex] : null;
            PlayerResponse lResponseExecuteActionTeamRed = (lDuelPlayerBIndex > -1) ? MatchEngine.Instance.ResponseAction.TeamB.Players[lDuelPlayerBIndex] : null;

            if (MatchEngine.Instance.ResponseAction.Team == eTeam.TEAM_B.ToString())
            {
                lResponseExecuteActionTeamBlue = (lDuelPlayerBIndex > -1) ? MatchEngine.Instance.ResponseAction.TeamB.Players[lDuelPlayerBIndex] : null;
                lResponseExecuteActionTeamRed = (lDuelPlayerAIndex > -1) ? MatchEngine.Instance.ResponseAction.TeamA.Players[lDuelPlayerAIndex] : null;
            }
            // End Set Response Team Blue & Team Red

            if (null == lResponseExecuteActionTeamBlue && null == lResponseExecuteActionTeamRed)
            {
                OnDuelFinished();
                return;
            }

            //Create Main Sequence Manager 
            GameObject parentPivot_Gob = GameObject.Find("UI_FX_Anchor_Point");
            GameObject newFX_Gob = Instantiate(ActionsUI.Instance.ResolutionsManagerGrp_Gob, Vector3.zero, Quaternion.identity) as GameObject;
            Resolutions_Manager lResolutionManager = newFX_Gob.GetComponent<Resolutions_Manager>();
            //Debug.Log("<color=cyan> --------------------------------------------------------------------------> Resolution Case </color>");
            newFX_Gob.name = "Resolutions_Manager_Grp";
            newFX_Gob.transform.SetParent(parentPivot_Gob.transform, false);

            lResolutionManager.ExecuteActionTeamBlue_Pr = lResponseExecuteActionTeamBlue;
            lResolutionManager.ExecuteActionTeamRed_Pr = lResponseExecuteActionTeamRed;

            // Set manager mode
            if (MatchEngine.Instance.ResponseAction.Team == eTeam.TEAM_A.ToString())
            {
                lResolutionManager.CurrentMode_Str = (eTeam.TEAM_A == UserData.Instance.PVPTeam) ? "PC" : "NPC";
            }
            else
            {
                lResolutionManager.CurrentMode_Str = (eTeam.TEAM_A == UserData.Instance.PVPTeam) ? "NPC" : "PVP";
            }
            // End Set manager mode

            // Orient players towards ball
            ActionsManager.Instance.HandleDuelPressureOpponents();

            // Set manager resolution case
            bool lIsSoloAction = MatchEngine.Instance.ResponseAction.IsSoloAction || ePhase.PERIOD_INIT == MatchEngine.Instance.CurPhase;


            switch (MatchEngine.Instance.Engine.ActionMain.Response.ResolutionAction)
            {
                case "PASS":
                    UtilityMethods.DebugResolution(eAction.PASS, MatchEngine.Instance.Engine.ActionMain.GetPlayerResponseWithAction(eAction.PASS).Resolution);
                    PassResolutionResponse lResolution = new PassResolutionResponse();
                    lResolution = lResolution.Parse(MatchEngine.Instance.Engine.ActionMain.GetPlayerResponseWithAction(eAction.PASS).Resolution);

                    lResolutionManager.PassResolution_Prr = lResolution;
                    HandlePressureOpponents(lResolution);
                    _handleActionPlayersOrientation(lResolution);

                    //HandlePressureOpponents(lResolution);

                    if (true == lIsSoloAction)
                    {
                        if (true == lResolution.IsBallIntercepted)
                        {
                            // BAll INTERCEPTED

                            //#region PASS : No Duel Only Display Selected Minion [Part_1] -> Display Interceptors & Interceptor Catch The Ball [Part_2] -> Move The Ball To Intercpetor Position [Part_3]
                            //Resolution_Pass_Case_9
                            lResolutionManager.CurrentResolutionCase_Str = "Resolution_Pass_Case_9";
                        }
                        else
                        {
                            // BAll NOT INTERCEPTED
                            if (null != lResolution.ReceiverOpponentDuel)
                            {
                                // DUEL RECEIVER

                                //Compare Score

                                if (lResolution.Receiver.Score > lResolution.ReceiverOpponentDuel.Score)
                                {
                                    //#region PASS : No Duel Only Display Selected Minion [Part_1] -> Display Interceptors [Part_2] -> Start & Win Duel Receiver Vs Opponent [Part_3] -> Move The Ball To Receiver Position [Part_4]
                                    //Resolution_Pass_Case_6
                                    lResolutionManager.CurrentResolutionCase_Str = "Resolution_Pass_Case_6";
                                }
                                else
                                {
                                    //#region PASS : No Duel Only Display Selected Minion [Part_1] -> Display Interceptors [Part_2] -> Start & Lose Duel Receiver Vs Opponent [Part_3] -> Move The Ball To Receiver Opponent Position [Part_4]
                                    //Resolution_Pass_Case_7
                                    lResolutionManager.CurrentResolutionCase_Str = "Resolution_Pass_Case_7";
                                }
                            }
                            else
                            {
                                // NO DUEL RECEIVER

                                //#region PASS : No Duel Only Display Selected Minion [Part_1] -> Display Interceptors [Part_2] ->  No Duel Only Display Selected Receiver [Part_3] -> Move The Ball To Receiver Position [Part_4]
                                //Resolution_Pass_Case_8
                                lResolutionManager.CurrentResolutionCase_Str = "Resolution_Pass_Case_8";
                            }
                        }
                    }
                    else
                    {
                        //Compare Score

                        if (lResponseExecuteActionTeamBlue.ExecuteAction.Score < lResponseExecuteActionTeamRed.ExecuteAction.Score)
                        {
                            //#region PASS : Start & Win Lose Against Opponent [Part_1] -> Move The Ball To Opponent Position [Part_2]
                            //Resolution_Pass_Case_5
                            lResolutionManager.CurrentResolutionCase_Str = "Resolution_Pass_Case_5";
                        }
                        else
                        {
                            // DUEL
                            if (true == lResolution.IsBallIntercepted)
                            {
                                // BAll INTERCEPTED

                                //#region PASS : Start & Win Duel Against Opponent [Part_1] -> Display Interceptors & Interceptor Catch The Ball [Part_2] -> Move The Ball To Intercpetor Position [Part_3]
                                //Resolution_Pass_Case_4
                                lResolutionManager.CurrentResolutionCase_Str = "Resolution_Pass_Case_4";
                            }
                            else
                            {
                                // BAll NOT INTERCEPTED
                                if (null != lResolution.ReceiverOpponentDuel)
                                {
                                    // DUEL RECEIVER

                                    if (lResolution.Receiver.Score > lResolution.ReceiverOpponentDuel.Score)
                                    {
                                        //#region PASS : Start & Win Duel Against Opponent [Part_1] -> Display Interceptors [Part_2] -> Start & Win Duel Receiver Vs Opponent [Part_3] -> Move The Ball To Receiver Position [Part_4]
                                        //Resolution_Pass_Case_1
                                        lResolutionManager.CurrentResolutionCase_Str = "Resolution_Pass_Case_1";
                                    }
                                    else
                                    {
                                        //#region PASS : Start & Win Duel Against Opponent [Part_1] -> Display Interceptors [Part_2] -> Start & Lose Duel Receiver Vs Opponent [Part_3] -> Move The Ball To Receiver Opponent Position [Part_4]
                                        //Resolution_Pass_Case_2
                                        lResolutionManager.CurrentResolutionCase_Str = "Resolution_Pass_Case_2";
                                    }
                                }
                                else
                                {
                                    // NO DUEL RECEIVER

                                    //#region PASS : Start & Win Duel Against Opponent [Part_1] -> Display Interceptors [Part_2] ->  No Duel Only Display Selected Receiver [Part_3] -> Move The Ball To Receiver Position [Part_4]
                                    //Resolution_Pass_Case_3
                                    lResolutionManager.CurrentResolutionCase_Str = "Resolution_Pass_Case_3";
                                }
                            }
                        }
                    }

                    break;
                case "LONG_KICK":
                    LongKickResolutionResponse lResolutionLongKick = new LongKickResolutionResponse();
                    lResolutionLongKick = lResolutionLongKick.Parse(MatchEngine.Instance.Engine.ActionMain.GetPlayerResponseWithAction(eAction.LONG_KICK).Resolution);

                    lResolutionManager.LongKickResolution_Lkrr = lResolutionLongKick;

                    if (true == lResolutionLongKick.IsBallIntercepted)
                    { lResolutionManager.CurrentResolutionCase_Str = "Resolution_Long_Kick_Case_7"; }
                    else
                    { lResolutionManager.CurrentResolutionCase_Str = "Resolution_Long_Kick_Case_1"; }

                    break;
                case "SPRINT":
                    lResolutionManager.CurrentResolutionCase_Str = "Resolution_Sprint";
                    break;
                case "MOVE":
                case "NONE":
                    OnDuelFinished();
                    break;
                default:
                    if (!lIsSoloAction)
                    {
                        // ANIM DUEL SIMPLE
                        if (lResponseExecuteActionTeamBlue.ExecuteAction.Score > lResponseExecuteActionTeamRed.ExecuteAction.Score)
                        {
                            //#region Duel : Start & Win Duel Against Opponent [Part_1]
                            //Resolution_Duel_Case_1
                            lResolutionManager.CurrentResolutionCase_Str = "Resolution_Duel_Case_1";
                        }
                        else
                        {
                            //#region Duel : Start & Lose Duel Against Opponent [Part_1] -> Move The Ball To Opponent Position [Part_2]
                            //Resolution_Duel_Case_2
                            lResolutionManager.CurrentResolutionCase_Str = "Resolution_Duel_Case_2";
                        }
                    }

                    break;
            }
            // End Set manager resolution case`

            SoccerBall.Instance.FollowPlayer(false);
            ActionsUI.Instance.Resolution();
        }

        public  void    OnDuelFinished()
        {

            ePhase lNextPhase = MatchEngine.Instance.Engine.PhaseMain.GetNextPhase();
            ePeriod lCurPeriod = MatchEngine.Instance.Engine.PeriodMain.Period;

            #if DEVELOPMENT_BUILD
            Debug.Log("[OppositionUI] OnDuelFinished()");
            #endif

            if (ePhase.DUEL_RESOLUTION == MatchEngine.Instance.CurPhase)
            {
                MatchCamera.Instance.NextCameraPhase();
                SoccerBall.Instance.FollowPlayer(true, PlayersManager.Instance.GetBallPossesser());
                UtilityMethods.InvokeGameEvent(_actionFinishedEvt);
                if (ePhase.CHANGE_PERIOD != lNextPhase || ePeriod.SECOND_PERIOD != lCurPeriod)
                    PlayersManager.Instance.PlacePlayers(false, false, true);
                else
                    MatchManager.Instance.NextPhase();
            }
            else
            {
                //if (ePhase.ACTION_RESOLUTION == MatchEngine.Instance.CurPhase)
                    ActionsUI.Instance.ClearBoard();

                PlayersManager.Instance.TogglePlayerCollider(true);
                PlayerPVP[] lAllPlayers =   PlayersManager.Instance.GetAllPlayers();
                int         lNbPlayers  =   lAllPlayers.Length;

                if (eAction.LONG_KICK.ToString() == MatchEngine.Instance.Engine.ActionMain.Response.ResolutionAction)
                {
                    LongKickResolutionResponse lResolution = new LongKickResolutionResponse();
                    lResolution.Parse(MatchEngine.Instance.Engine.ActionMain.GetPlayerResponseWithAction(eAction.LONG_KICK).Resolution);

                    PlayerPVP   lStriker    =   PlayersManager.Instance.GetPlayer(lResolution.Stricker.Team, lResolution.Stricker.Position);
                    lStriker.UI.UpdatePlayerPossession(false);

                    if (!lResolution.IsBallIntercepted && !lResolution.IsGoalkeeperStopBall)
                    {
                        UtilityMethods.InvokeGameEvent(eTeam.TEAM_A.ToString() == lResolution.Stricker.Team ? _teamAScoredGoalEvt : _teamBScoredGoalEvt);
                        ActionsManager.Instance.ClearField();
                        ActionsManager.Instance.OnEngineStartPeriod();
                        MatchMessagesUI.Instance.OnGoalScored();
                        UserEventsManager.Instance.RecordEvent(new UserActionEvent(lResolution.Stricker.Team == UserData.Instance.PVPTeam.ToString() ? "goal_for" : "goal_against"));
                        UtilityMethods.InvokeGameEvent(_actionFinishedEvt);
                        StartCoroutine(_goal(eTeamMethods.ConvertStringToETeam(lResolution.Stricker.Team)));
                    }
                    else
                    {
                        SoccerBall.Instance.FollowPlayer(true, PlayersManager.Instance.GetBallPossesser());
                        if (ePhase.CHANGE_PERIOD != lNextPhase || ePeriod.SECOND_PERIOD != lCurPeriod)
                            PlayersManager.Instance.PlacePlayers(false, true, true);
                        else
                            MatchManager.Instance.NextPhase();
                        UtilityMethods.InvokeGameEvent(_actionFinishedEvt);

                        for (int li = 0; li < lNbPlayers; li++)
                            lAllPlayers[li].Animator.ToggleDuelAnim(false);
                    }
                }
                else if (eAction.PASS.ToString() == MatchEngine.Instance.Engine.ActionMain.Response.ResolutionAction)
                {
                    PassResolutionResponse lPassResolution = new PassResolutionResponse();
                    lPassResolution.Parse(MatchEngine.Instance.Engine.ActionMain.GetPlayerResponseWithAction(eAction.PASS).Resolution);

                    for (int li = 0; li < lNbPlayers; li++)
                        lAllPlayers[li].Animator.ToggleDuelAnim(false);

                    if (MatchEngine.Instance.ResponseAction.Team == lPassResolution.TeamResolution)
                    {
                        SoccerBall.Instance.FollowPlayer(true, PlayersManager.Instance.GetBallPossesser());
                        _nextStep();
                    }
                    else
                    {
                        if (lPassResolution.IsOffside && string.IsNullOrEmpty(lPassResolution.TeamResolution))
                        {
                            StartCoroutine(_offsidePass(lPassResolution));
                        }
                        else
                        {
                            SoccerBall.Instance.FollowPlayer(true, PlayersManager.Instance.GetBallPossesser());
                            _nextStep();
                        }
                    }
                }
                else if (eAction.DRIBBLE.ToString() == MatchEngine.Instance.Engine.ActionMain.Response.ResolutionAction)
                {
                    SoccerBall.Instance.FollowPlayer(true, PlayersManager.Instance.GetBallPossesser());
                    UtilityMethods.InvokeGameEvent(_actionFinishedEvt);
                    if (ePhase.CHANGE_PERIOD != lNextPhase || ePeriod.SECOND_PERIOD != lCurPeriod)
                        PlayersManager.Instance.PlacePlayers(false, false, true);
                    else
                        MatchManager.Instance.NextPhase();
                }
                else if (ePhase.PERIOD_INIT != MatchEngine.Instance.CurPhase)
                {
                    SoccerBall.Instance.FollowPlayer(true, PlayersManager.Instance.GetBallPossesser());
                    UtilityMethods.InvokeGameEvent(_actionFinishedEvt);

                    for (int li = 0; li < lNbPlayers; li++)
                        lAllPlayers[li].Animator.ToggleDuelAnim(false);

                    //if (ePhase.ACTION_RESOLUTION == MatchEngine.Instance.CurPhase)
                    //{
                        if (ePhase.CHANGE_PERIOD != lNextPhase || ePeriod.SECOND_PERIOD != lCurPeriod)
                            PlayersManager.Instance.PlacePlayers(false, true, true);
                        else
                            MatchManager.Instance.NextPhase();
                    //}
                    //else
                    //{
                    //    if (ePhase.CHANGE_PERIOD != lNextPhase || ePeriod.SECOND_PERIOD != lCurPeriod)
                    //        PlayersManager.Instance.PlacePlayers(false, true, false);
                    //    MatchManager.Instance.NextPhase();
                    //}
                }

                for (int li = 0; li < lNbPlayers; li++)
                    lAllPlayers[li].UI.ToggleTarget(false);
            }
        }

        public  void    HandlePressureOpponents(PassResolutionResponse pResolution)
        {
            PlayerPVP lReceiver = PlayersManager.Instance.GetPlayer(pResolution.Receiver.Team, pResolution.Receiver.Position);
            PlayerPVP lOpponent, lDuelOpponent;

            lReceiver.Animator.ToggleDuelAnim(true);

            GameObject lPassTrajectory = GameObject.Find("Pass_" + pResolution.PlayerPasser + "_" + lReceiver.Data.Position);
            if (null != lPassTrajectory)
                lPassTrajectory.SetActive(false);

            foreach (PlayerActionResponse pOpponent in pResolution.ReceiverOpponentsPressure)
            {
                lOpponent = PlayersManager.Instance.GetPlayer(pOpponent.Team, pOpponent.Position);

                lOpponent.Animator.ToggleDuelAnim(true);
                lOpponent.Movements.ShouldFaceBall = false;
                lOpponent.Movements.OrientPlayerTowardsTarget(lReceiver.transform.position);
            }

            if (null != pResolution.ReceiverOpponentDuel)
            {
                lDuelOpponent = PlayersManager.Instance.GetPlayer(pResolution.ReceiverOpponentDuel.Team, pResolution.ReceiverOpponentDuel.Position);

                lDuelOpponent.Animator.ToggleDuelAnim(true);

                foreach (PlayerActionResponse pOpponent in pResolution.ReceiverOpponentDuelOpponentsPressure)
                {
                    lOpponent = PlayersManager.Instance.GetPlayer(pOpponent.Team, pOpponent.Position);

                    lOpponent.Animator.ToggleDuelAnim(true);
                    lOpponent.Movements.ShouldFaceBall = false;
                    lOpponent.Movements.OrientPlayerTowardsTarget(lDuelOpponent.transform.position);
                }
            }
        }

        #endregion

        #region Private Methods

        private void    _handleActionPlayersOrientation(PassResolutionResponse pResolution)
        {
            PlayerPVP lOpponent;

            lOpponent = PlayersManager.Instance.GetPlayer(pResolution.Receiver.Team, pResolution.Receiver.Position);
            lOpponent.Animator.ToggleDuelAnim(true);
            lOpponent.Movements.ShouldFaceBall = true;

            foreach (PlayerActionResponse pOpponent in pResolution.Interceptors)
            {
                lOpponent = PlayersManager.Instance.GetPlayer(pOpponent.Team, pOpponent.Position);

                lOpponent.Animator.ToggleDuelAnim(true);
                lOpponent.Movements.ShouldFaceBall = true;
            }

            if (null != pResolution.ReceiverOpponentDuel)
            {
                lOpponent = PlayersManager.Instance.GetPlayer(pResolution.ReceiverOpponentDuel.Team, pResolution.ReceiverOpponentDuel.Position);
                lOpponent.Animator.ToggleDuelAnim(true);
                lOpponent.Movements.ShouldFaceBall = true;
            }
        }

        private void    _next(eTeam lScoringTeam)
        {
            if (ePhase.CHANGE_PERIOD == MatchEngine.Instance.Engine.PhaseMain.GetNextPhase())
                UtilityMethods.InvokeGameEvent(_stopPeriodEvt);
            else
            {
                if (false == PhotonNetwork.IsConnected)
                {
                    MatchEngine.Instance.Engine.PhaseMain.Phase = ePhase.TOSS_RESOLUTION;
                    MatchEngine.Instance.Engine.PossessionMain.ChangeTeamPossession(eTeamMethods.GetOpposite(lScoringTeam));
                    MatchEngine.Instance.Engine.PossessionMain.ChangePlayerPossession(ePosition.ACD);

                    if (ePhase.CHANGE_PERIOD == MatchEngine.Instance.Engine.PhaseMain.GetNextPhase())
                        UtilityMethods.InvokeGameEvent(_stopPeriodEvt);
                    else
                    {
                        MatchEngine.Instance.Engine.FieldSetupMain.ChangeCurrentFieldSetup("FS_A_KICK_OFF", ePosition.ACD);
                        PlayersManager.Instance.PlacePlayers();
                        MatchMessagesUI.Instance.NewPhaseAnnouncement(MatchEngine.Instance.Engine.PhaseMain.GetNextPhase());
                    }
                }
                else
                {
                    PhotonMain.Instance.PhotonView.RPC("RPC_EndedPhase", RpcTarget.Others, UserData.Instance.PVPTeam, eTeamMethods.GetOpposite(lScoringTeam));
                }
            }
        }

        private void    _nextStep()
        {
            ePhase  lNextPhase  =   MatchEngine.Instance.Engine.PhaseMain.GetNextPhase();
            ePeriod lCurPeriod  =   MatchEngine.Instance.Engine.PeriodMain.Period;

            UtilityMethods.InvokeGameEvent(_actionFinishedEvt);
            if (ePhase.CHANGE_PERIOD != lNextPhase || ePeriod.SECOND_PERIOD != lCurPeriod)
                PlayersManager.Instance.PlacePlayers(false, false, true);
            else
                MatchManager.Instance.NextPhase();
        }

        #region Coroutines

        private IEnumerator _goal(eTeam lScoringTeam)
        {
            if (null != MatchTimers.Instance)
                MatchTimers.Instance.PauseCurrentPeriodTimer();

            LongKickResolutionResponse lResolution = new LongKickResolutionResponse();
            lResolution.Parse(MatchEngine.Instance.Engine.ActionMain.GetPlayerResponseWithAction(eAction.LONG_KICK).Resolution);

            PlayerPVP[] lTeamPlayers = PlayersManager.Instance.GetTeamPlayers(lScoringTeam);

            for (int li = 0; li < lTeamPlayers.Length; li++)
                lTeamPlayers[li].Animator.TriggerActionAnim("CELEBRATION", lTeamPlayers[li].Data.Position.ToString() == lResolution.Stricker.Position);

            yield return new WaitForSeconds(_timingConfig.WaitBeforeKickOff);

            _next(lScoringTeam);
        }

        private IEnumerator _offsidePass(PassResolutionResponse pResolution)
        {
            PlayerPVP lReceiver = PlayersManager.Instance.GetPlayer(pResolution.TeamPasser, pResolution.Receiver.Position);

            MatchAudio.Instance.KickOff();

            ActionsManager.Instance.DrawOffside(pResolution.OffsideX + (pResolution.TeamPasser == UserData.Instance.PVPTeam.ToString() ? 1 : 0));
            ActionsUI.Instance.ToggleReceiverOffside(true, lReceiver);

            yield return new WaitForSeconds(0.5f);

            MatchCamera.Instance.FocusOnPlayer(lReceiver.gameObject);

            yield return new WaitForSeconds(MatchCamera.Instance.TimeToWait);

            MatchMessagesUI.Instance.Message(LocalizationManager.GetTranslation("offside_title"), lReceiver.transform);

            yield return new WaitForSeconds(2f);

            MatchCamera.Instance.DisplayResolutionView();

            yield return new WaitForSeconds(MatchCamera.Instance.TimeToWait);
            //SoccerBall.Instance.FollowPlayer(true, PlayersManager.Instance.GetBallPossesser());

            _nextStep();
        }

        private IEnumerator _launchResolution()
        {
            yield return new WaitForSeconds(MatchCamera.Instance.TimeToWait);

            //if (ePhase.ACTION_RESOLUTION == MatchEngine.Instance.CurPhase || ePhase.)
            ActionsManager.Instance.ClearBeforeResolution();

            ExecuteActionResolution();
        }

        #endregion

        #endregion
    }
}
using Sportfaction.CasualFantasy.PVP.Gameplay.Matchs;
using Sportfaction.CasualFantasy.References;

using SportFaction.CasualFootballEngine.PeriodService.Enum;

using UnityEngine;

namespace Sportfaction.CasualFantasy.PVP.UI
{
    /// <summary>
    /// Class handling display of match fictional timer
    /// </summary>
    /// @author Rémi Carreira
    [RequireComponent(typeof(TMPro.TMP_Text))]
	public sealed class FictionalTimerUI : MonoBehaviour
	{
		#region Fields

		[SerializeField, Tooltip("UI Text displaying fictional timer")]
		private	TMPro.TMP_Text  _uiText					=   null;
		[SerializeField, Tooltip("Reference to the timer used in the first period")]
		private	IntReference	_firstPeriodTimerRef	=   null;
		[SerializeField, Tooltip("Reference to the timer used in the second period")]
		private	IntReference	_secondPeriodTimerRef	=   null;
		[SerializeField, Tooltip("Format string to use for displaying timer")]
		private	string			_formatString			=   "{0:00.}:{1:00.}";
		[SerializeField, ReadOnly, Tooltip("Current time (real)")]
		private	float			_timeRemaining			=   0;
		[SerializeField, ReadOnly, Tooltip("Total time (real)")]
		private float			_totalTime				=   0;
		[SerializeField, ReadOnly, Tooltip("Fictional time")]
		private	float			_fictionalTime			=   0f;

		private int	_minutes	=	0;
		private	int	_seconds	=	0;

		#endregion

		#region Public Methods

        /// <summary>
        /// Event Listener > OnEngineStartPeriod
        /// </summary>
		public	void	OnEngineStartPeriod()
		{
			ePeriod	lCurPeriod	= MatchEngine.Instance.Engine.PeriodMain.Period;

            if (ePeriod.SECOND_PERIOD == lCurPeriod)
			{
				_firstPeriodTimerRef.CurrentValue	=	0;
			}
		}
		
		#endregion

		#region Private Methods

		/// <summary>
		/// Initializes fictional timer ui
		/// </summary>
		private	void	Awake()
		{
			_totalTime		= _firstPeriodTimerRef.DefaultValue + _secondPeriodTimerRef.DefaultValue;
			_timeRemaining	= _firstPeriodTimerRef.CurrentValue + _secondPeriodTimerRef.CurrentValue;
		}

		/// <summary>
		/// Monobehaviour method used to update fictional timer UI
		/// </summary>
		private	void	OnEnable()
		{
			Update();
		}

		/// <summary>
		/// Updates fictional timer UI
		/// </summary>
		private void	Update()
		{
			_timeRemaining	= _firstPeriodTimerRef.CurrentValue + _secondPeriodTimerRef.CurrentValue;

			_fictionalTime	=	90f - ((_timeRemaining / _totalTime) * 90f);

			_minutes	=	Mathf.FloorToInt(_fictionalTime);
			_seconds	=	(int)((_fictionalTime - _minutes) * 60f);

			_uiText.text	=	string.Format(_formatString, _minutes, _seconds);
		}

		#endregion
	}
}
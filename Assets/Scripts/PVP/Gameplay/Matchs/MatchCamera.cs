using Sportfaction.CasualFantasy.PVP.Gameplay.Actions;

using SportFaction.CasualFootballEngine.PhaseService.Enum;

using DG.Tweening;
using UnityEngine;

namespace Sportfaction.CasualFantasy.PVP.Gameplay.Matchs
{
    /// <summary>
    /// Class handling camera position & orientation during a match
    /// </summary>
    /// @author Madeleine Tranier
    public sealed class MatchCamera : Singleton<MatchCamera>
	{
        #region Public Fields

        [ReadOnly]
        public  float   TimeToWait  =   0f; //!< Duration of current camera movement

        #endregion

        #region Serialized Fields

        [Header("Additional Cameras")]
        [SerializeField, Tooltip("Interaction phase view")]
        private Camera  _interactionView    =   null; //!< Tactical view camera settings
        [SerializeField, Tooltip("Resolution phase view")]
        private Camera  _resolutionView     =   null; //!< Resolution view camera settings

        #endregion

        #region Public Methods

        public  void    NextCameraPhase()
        {
            ePhase  lNextPhase  =   MatchEngine.Instance.Engine.PhaseMain.GetNextPhase();

            if (ePhase.DUEL_RESOLUTION == lNextPhase && !ActionsManager.Instance.IsSoloAction)
                FocusBetweenPlayers(new GameObject[2] { ActionsManager.Instance.DuelPlayer.gameObject, ActionsManager.Instance.DuelOppPlayer.gameObject });
            else
                DisplayResolutionView();
        }

        //public  void    DisplayInteractionView()
        //{
        //    UpdateCamera(_interactionView.transform.position, _interactionView.fieldOfView, _interactionView.transform.localEulerAngles);
        //}

        public  void    DisplayLongKickProjection()
        {
            UpdateCamera(_interactionView.transform.position + new Vector3(1f, 0f, 0f), _interactionView.fieldOfView, _interactionView.transform.localEulerAngles);
        }

        public  void    DisplayResolutionView()
        {
            UpdateCamera(_resolutionView.transform.position, _resolutionView.fieldOfView, _resolutionView.transform.localEulerAngles);
        }

        public  void    DisplayOutroView()
        {
            this.GetComponent<Animator>().enabled = true;
            this.GetComponent<Animator>().SetTrigger("Outro");
        }

        public  void    FocusOnPlayer(GameObject pPlayerGO)
        {
            Vector3 lCheckedPos =   _adjustPos(pPlayerGO.transform.position + new Vector3(0f, 0f, -11f));
            Vector3 lCamPos     =   new Vector3(lCheckedPos.x, _interactionView.transform.position.y, lCheckedPos.z);

            UpdateCamera(lCamPos, ActionsManager.Instance.GameplayConfig.PlayerFocusFOV, _interactionView.transform.localEulerAngles);
        }

        public  void    FocusBetweenPlayers(GameObject[] pPlayersGO)
        {
            Vector3 lMidPos =   Vector3.zero;
            int     lNbGO   =   pPlayersGO.Length;

            if (0 == lNbGO)
                return;

            for (int li = 0; li < lNbGO; li++)
            {
                lMidPos += pPlayersGO[li].transform.position;
            }

            lMidPos =   lMidPos / lNbGO;

            Vector3 lCamPos =   new Vector3(lMidPos.x, _resolutionView.transform.position.y, lMidPos.z - 11f);

            UpdateCamera(lCamPos, 18f, _resolutionView.transform.localEulerAngles);
        }

        public  void    UpdateCamera(Vector3 pCamTargetPos, float pCamTargetFOV, Vector3 pEulerAngles)
        {
            float   lDist   =   (pCamTargetPos - this.transform.position).magnitude;
            float   lSpeed  =   (lDist > 1f) ? 2f : 1.7f;

            TimeToWait  =   lDist / lSpeed;

            DOTween.Kill(transform);
            DOTween.Kill(Camera.main);

            transform.DOMove(pCamTargetPos, TimeToWait).SetEase(Ease.InOutQuint);

            Camera.main.DOFieldOfView(pCamTargetFOV, TimeToWait).SetEase(Ease.InOutQuint);

            transform.DORotate(pEulerAngles, TimeToWait);
        }

        /*public  void    UpdateCameraFast(Vector3 pCamTargetPos, float pCamTargetFOV, Vector3 pEulerAngles)
        {
            float   lDist   =   (pCamTargetPos - this.transform.position).magnitude;
            float   lSpeed  =   (lDist > 1f) ? 1.6f : 1.2f;

            TimeToWait  =   lDist / lSpeed;

            DOTween.Kill(transform);
            DOTween.Kill(Camera.main);

            transform.DOMove(pCamTargetPos, TimeToWait).SetEase(Ease.Linear);

            Camera.main.DOFieldOfView(pCamTargetFOV, TimeToWait).SetEase(Ease.Linear);

            transform.DORotate(pEulerAngles, TimeToWait);
        }*/

        #endregion

        #region Private Methods

        private Vector3 _adjustPos(Vector3 pPosToCheck)
        {
            Vector3 lNewPos =   pPosToCheck;

            // X Limit camera
            if (pPosToCheck.x < ActionsManager.Instance.GameplayConfig.CamLeftXLimit)
                lNewPos.x =   ActionsManager.Instance.GameplayConfig.CamLeftXLimit;
            else if (pPosToCheck.x > ActionsManager.Instance.GameplayConfig.CamRightXLimit)
                lNewPos.x =   ActionsManager.Instance.GameplayConfig.CamRightXLimit;

            // Z Limit camera
            if (pPosToCheck.z < ActionsManager.Instance.GameplayConfig.CamBottomZLimit)
                lNewPos.z =   ActionsManager.Instance.GameplayConfig.CamBottomZLimit;
            else if (pPosToCheck.z > ActionsManager.Instance.GameplayConfig.CamTopZLimit)
                lNewPos.z =   ActionsManager.Instance.GameplayConfig.CamTopZLimit;

            return  lNewPos;
        }

        #endregion
    }
}
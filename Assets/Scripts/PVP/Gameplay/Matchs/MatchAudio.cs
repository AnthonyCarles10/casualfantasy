﻿using SportFaction.CasualFootballEngine.PeriodService.Enum;

using UnityEngine;

namespace Sportfaction.CasualFantasy.PVP.Gameplay.Matchs
{
    /// <summary>
    /// Class handling every sound FX during a match
    /// </summary>
	public sealed class MatchAudio : Singleton<MatchAudio>
	{
		#region Fields

		[SerializeField, Tooltip("Match events source")]
		private	AudioSource	_matchEventsSfx     =	null; //!< Match events audio source settings
		[SerializeField, Tooltip("Actions source")]
		private	AudioSource	_actionsSfx         =	null; //!< Action audio source settings
		[SerializeField, Tooltip("Points source")]
		private	AudioSource	_pointsSfx          =	null; //!< Points feedback audio source settings
		[SerializeField, Tooltip("User interactions source")]
		private	AudioSource	_interactionsSfx    =	null; //!< User interactions feedback audio source settings
		[SerializeField, Tooltip("Player audio source")]
		private	AudioSource	_playerSfx          =	null; //!< Player actions audio source settings
        [SerializeField, Tooltip("Loop source")]
        private AudioSource _loopSfx            =   null;
		
		[Header("Audio Ambience")]
		[SerializeField, Tooltip("1st period clip")]
		private	AudioSource	_firstPeriodSfx     =	null; //!< First period ambience audio source settings
		[SerializeField, Tooltip("1st period clip")]
		private	AudioSource	_secondPeriodSfx    =	null; //!< Second period ambience audio source settings

        #pragma warning disable 414, CS0649
		[SerializeField, Tooltip("Clips")]
        private	AudioClip[]	_clips;
		
		#endregion

		#region Public Methods

		#region Game Event Listeners
		
		public	void	OnEngineStartPeriod()
		{
			ePeriod lPeriod	= MatchEngine.Instance.Engine.PeriodMain.Period;

            if (ePeriod.FIRST_PERIOD == lPeriod)
				_firstPeriodSfx.Play();
			else
				_secondPeriodSfx.Play();
		}
		
		public	void	OnEngineStopPeriod()
		{
			ePeriod lPeriod	= MatchEngine.Instance.Engine.PeriodMain.Period;

            if (ePeriod.HALF_TIME == lPeriod)
			{
				_matchEventsSfx.PlayOneShot(_findClip("WhistleHalfTime"));
				_firstPeriodSfx.Stop();
			}
			else if (ePeriod.FULL_TIME == lPeriod)
			{
				_matchEventsSfx.PlayOneShot(_findClip("WhistleEndMatch"));
				_secondPeriodSfx.Stop();
			}
		}

		#endregion

		#region User Interaction Sounds
		
		public	void	OnTapActivation()
		{
			_interactionsSfx.PlayOneShot(_findClip("Tap1"));
        }

        public  void    OnTapValidation()
        {
            _interactionsSfx.PlayOneShot(_findClip("Tap2"));
        }

        public	void	OnTargetValidated()
		{
			_interactionsSfx.PlayOneShot(_findClip("TargetValidated"));
		}
		
		public	void	OnTapCancellation()
		{
			_interactionsSfx.PlayOneShot(_findClip("SelectCancel"));
        }

        public  void    OnTapForbidden()
        {
            _interactionsSfx.PlayOneShot(_findClip("Cantselect"));
        }

        public  void    OnMinionRollOver()
        {
            _interactionsSfx.PlayOneShot(_findClip("MinionRollHover"));
        }

        #endregion

        #region Player Sounds

        public  void    PlayerRun(bool pPlay)
        {
            _loopSfx.enabled = pPlay;

            if (pPlay)
                _loopSfx.Play();
        }

        public	void	Kick()
		{
			_playerSfx.PlayOneShot(_findClip("Kick"));
		}
		
		public	void	Control()
		{
			_playerSfx.PlayOneShot(_findClip("Control"));
		}
		
		public	void	Tackle()
		{
			_playerSfx.PlayOneShot(_findClip("Tackle"));
		}
		
        #endregion

        #region UI Sounds

        public  void    OnBoardAppears(bool pCurManagerTurn)
        {
            _actionsSfx.PlayOneShot(_findClip(pCurManagerTurn ? "BoardOnAppears" : "BoardOffAppears"));
        }
        
		public	void	OnGoodDefense()
		{
			_actionsSfx.PlayOneShot(_findClip("GoodDefense"));
		}

        public  void    OnDefenseBonusMoves()
        {
            _actionsSfx.PlayOneShot(_findClip("BonusDefenseMoves"));
        }

        public  void    OnDefenseBonusApplied()
        {
			_actionsSfx.PlayOneShot(_findClip("BonusDefenseApplied"));
        }

        public  void    OnMessageAppears()
        {
            _actionsSfx.PlayOneShot(_findClip("MessageAppears"));
        }

        public  void    OnMessageDisappears()
        {
            _actionsSfx.PlayOneShot(_findClip("MessageDisappears"));
        }

        #endregion

        #region Resolution Sounds

        public  void    Draw()
        {
            if (!_pointsSfx.isPlaying)
                _pointsSfx.PlayOneShot(_findClip("Draw"));
        }

        public  void    DrawEnd()
        {
            _actionsSfx.PlayOneShot(_findClip("DrawEnd"));
        }

        public  void    ScoreWin()
        {
            _actionsSfx.PlayOneShot(_findClip("ScoreWins"));
        }

        public  void    ScoreLose()
        {
            _actionsSfx.PlayOneShot(_findClip("ScoreLose"));
        }

        public  void    ArrowAppears()
        {
            _actionsSfx.PlayOneShot(_findClip("ArrowAppears"));
        }

        public  void    PressionAppears()
        {
            _actionsSfx.PlayOneShot(_findClip("PressionAppears"));
        }

        public  void    PressionImpact()
        {
            _actionsSfx.PlayOneShot(_findClip("PressionImpact"));
        }

        public  void    Goal()
        {
            _matchEventsSfx.PlayOneShot(_findClip("Goal"));
        }

        public  void    GoalMissed()
        {
            _matchEventsSfx.PlayOneShot(_findClip("GoalMissed"));
        }

        #endregion

        #region Outro

        public  void    OutroTrophy(bool pWon)
        {
            _matchEventsSfx.PlayOneShot(_findClip(pWon ? "TrophyWin" : "TrophyLose"));
        }

        public  void    OutroArrows(bool pWon)
        {
            _matchEventsSfx.PlayOneShot(_findClip(pWon ? "GreenArrows" : "RedArrows"));
        }

        public  void    OutroRank(bool pWon)
        {
            _matchEventsSfx.PlayOneShot(_findClip(pWon ? "RankUp" : "RankDown"));
        }

        public  void    OutroSoccerCubes()
        {
            _matchEventsSfx.PlayOneShot(_findClip("ScubeWin"));
        }

        public  void    OutroBoxWon()
        {
            _matchEventsSfx.PlayOneShot(_findClip("BoxGained"));
        }

        public  void    OutroBoxPlusOne()
        {
            _actionsSfx.PlayOneShot(_findClip("BoxNumberPlusOne"));
        }

        public  void    BoardAppears()
        {
            _actionsSfx.PlayOneShot(_findClip("RewardBoardAppearance"));
        }

        public  void    BoardMoves()
        {
            _actionsSfx.PlayOneShot(_findClip("RewardBoardMove"));
        }

        public  void    RewardWinAppears()
        {
            _actionsSfx.PlayOneShot(_findClip("RewardWinAppearance"));
        }

        public  void    RewardLoseAppears()
        {
            _actionsSfx.PlayOneShot(_findClip("RewardLoseAppearance"));
        }

        #endregion

        public  void    KickOff()
        {
            _matchEventsSfx.PlayOneShot(_findClip("WhistleKickOff"));
        }

        public  void    ClapStart()
        {
            _matchEventsSfx.PlayOneShot(_findClip("ClapStart"));
        }

        public  void    OnOpponentInstructionValidated()
        {
            _actionsSfx.PlayOneShot(_findClip("OpponentInstructionValidated"));
        }

        public  void    OnOpponentActionRevealed()
        {
            _actionsSfx.PlayOneShot(_findClip("OpponentActionNameAppears"));
        }
		
		public	void	OnVictory()
		{
			_actionsSfx.PlayOneShot(_findClip("Victory"));
		}

        public  void    TWPAppearance()
        {
			_actionsSfx.PlayOneShot(_findClip("TWPAppearance"));
        }

        public  void    TWPMove()
        {
            _actionsSfx.PlayOneShot(_findClip("TWPMove"));
        }

        public  void    TWPAbsorbed()
        {
            _actionsSfx.PlayOneShot(_findClip("TWPAbsorbed"));
        }

        public  void    TWPGeneratingFeedback()
        {
            _interactionsSfx.PlayOneShot(_findClip("TWPGeneratingFeedback"));
        }

        public  void    TWPChargingInMinion()
        {
            _interactionsSfx.PlayOneShot(_findClip("TWPChargingInMinion"));
        }

        #endregion

        #region Private Methods

        private void    Start()
        {
            ClapStart();
        }

        private	AudioClip	_findClip(string pName)
		{
			for (int li = 0; li < _clips.Length; li++)
			{
				if (pName == _clips[li].name)
					return _clips[li];
			}

			return null;
		}
		
		#endregion
	}
}
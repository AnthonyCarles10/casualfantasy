﻿using Sportfaction.CasualFantasy.Events;
using Sportfaction.CasualFantasy.Manager.Profile;
using Sportfaction.CasualFantasy.PVP.Gameplay.Actions;
using Sportfaction.CasualFantasy.PVP.Gameplay.FX;
using Sportfaction.CasualFantasy.PVP.Gameplay.Players;
using Sportfaction.CasualFantasy.PVP.Gameplay.UI;
using Sportfaction.CasualFantasy.PVP.Network;
using Sportfaction.CasualFantasy.PVP.Test;
using Sportfaction.CasualFantasy.Services.Userstracking;

using SportFaction.CasualFootballEngine.PeriodService.Enum;
using SportFaction.CasualFootballEngine.PhaseService.Enum;
using SportFaction.CasualFootballEngine.TeamService.Enum;

using DG.Tweening;
using Photon.Pun;
using System.Collections;
using UnityEngine;
using I2.Loc;

namespace Sportfaction.CasualFantasy.PVP.Gameplay.Matchs
{
    /// <summary>
    /// Class handling match phase changes
    /// </summary>
    /// @author Madeleine Tranier
    public sealed class MatchManager : Singleton<MatchManager>
    {
        #region Serialized Fields

        [Header("Game Events")]
        [SerializeField, Tooltip("Event invoked when half time msg displayed")]
        private GameEvent _halfTimeStartedEvt = null; //!< Event to invoke to start half-time
        [SerializeField, Tooltip("Event to invoke when match rewards should be displayed")]
        private GameEvent _displayMatchResultsEvt = null; //!< Event to invoke to start rewards display
        [SerializeField, Tooltip("Event to invoke to stop period")]
        private GameEvent _stopPeriodEvt = null; //!< Event to invoke to stop current period

        [Header("Configuration")]
        [SerializeField, Tooltip("Is tester ?")]
        private bool _isTester = false;

        #endregion

        #region Public Methods

        #region Game Event Listeners

        public void OnEngineChangeFieldSetup()
        {
            ePeriod lCurPeriod = MatchEngine.Instance.Engine.PeriodMain.Period;

            if (ePeriod.HALF_TIME != lCurPeriod && ePeriod.FULL_TIME != lCurPeriod && null != MatchMessagesUI.Instance)
            {
                if (
                    "FS_A_KICK_OFF" != MatchEngine.Instance.Engine.FieldSetupMain.CurrentFieldSetup &&
                    "FS_A_PERIOD_INIT" != MatchEngine.Instance.Engine.FieldSetupMain.CurrentFieldSetup &&
                    "FS_A_NEW_INIT" != MatchEngine.Instance.Engine.FieldSetupMain.CurrentFieldSetup &&
                    "FS_A_TOSS_INIT" != MatchEngine.Instance.Engine.FieldSetupMain.CurrentFieldSetup
                )
                    MatchMessagesUI.Instance.NewPhaseAnnouncement(MatchEngine.Instance.Engine.PhaseMain.GetNextPhase());
            }
        }

        /// <summary>
        /// Event Listener > OnDuelTimerFinished
		/// </summary>
        public void OnTimerDone(bool pForced = false)
        {
            StartCoroutine(NextResolutionPhase());
        }

        public void OnEngineStopPeriod()
        {
            ActionsManager.Instance.ClearField();
            SoccerBall.Instance.FollowPlayer(false);
            SoccerBall.Instance.MoveToCenter();

            if (ePeriod.HALF_TIME == MatchEngine.Instance.Engine.PeriodMain.Period)
                StartCoroutine(_halfTime());
            else
            {
                bool lIsManagerTeamA = eTeam.TEAM_A == UserData.Instance.PVPTeam;
                float lScoreA = MatchEngine.Instance.Engine.ScoreMain.TeamAScore;
                float lScoreB = MatchEngine.Instance.Engine.ScoreMain.TeamBScore;

                // Determine match result (win / draw / loss)
                bool lIsLoser = (lIsManagerTeamA && lScoreA < lScoreB) || (!lIsManagerTeamA && lScoreB < lScoreA);

                StartCoroutine(_displayMatchResult(lIsLoser ? 5f : 3.5f));
            }
        }

        #endregion

        public void NextPhase(bool pBypassActionRes = false)
        {
            ePhase lNextPhase = MatchEngine.Instance.Engine.PhaseMain.GetNextPhase();

            //#if DEVELOPMENT_BUILD
            //Debug.Log("[MatchManager] NextPhase() Current phase = " + MatchEngine.Instance.CurPhase + " | Next phase = " + lNextPhase);
            //#endif

            if (ePhase.CHANGE_PERIOD == lNextPhase)
                EndPeriod();
            else
            {
                if (ePhase.ACTION == lNextPhase)
                    MatchCamera.Instance.DisplayResolutionView();

                if (ePhase.TACTICAL_ATTACK == lNextPhase)
                    MatchMessagesUI.Instance.NewPhaseAnnouncement(lNextPhase);
                else
                    MatchEngine.Instance.NextPhase();
            }
        }

        public void StartMatch()
        {
            if (_isTester)
            {
                if (null != MatchSimulation.Instance)
                {
                    if (MatchSimulation.Instance.TestActionOnly)
                    {
                        SoccerBall.Instance.PlaceBall(false);
                        MatchCamera.Instance.DisplayResolutionView();
                        MatchEngine.Instance.Engine.PeriodMain.StartPeriod();
                        MatchEngine.Instance.Engine.PhaseMain.ChangePhase(ePhase.ACTION);
                    }
                    else
                        MatchEngine.Instance.NextPhase(); // PRE-TOSS
                }
                else if (null != MatchTutorial.Instance)
                {
                    if (MatchTutorial.Instance.TestActionOnly)
                    {
                        SoccerBall.Instance.PlaceBall(false);
                        MatchCamera.Instance.DisplayResolutionView();
                        MatchEngine.Instance.Engine.PeriodMain.StartPeriod();
                        MatchEngine.Instance.Engine.PhaseMain.ChangePhase(ePhase.ACTION);
                    }
                    else
                        MatchEngine.Instance.NextPhase(); // PRE-TOSS
                }
            }
            else
                MatchEngine.Instance.NextPhase(); // PRE-TOSS

            if (null != GameObject.Find("FX-SynapseGeneric"))
                GameObject.Find("FX-SynapseGeneric").SetActive(false);
        }

        public void EndPeriod()
        {
            MatchCamera.Instance.DisplayResolutionView();

            UtilityMethods.InvokeGameEvent(_stopPeriodEvt);
        }

        #region Public Coroutines

        public IEnumerator NextResolutionPhase(float pWait = 0f)
        {
            yield return new WaitForSeconds(pWait);

            if ((ePhase.ACTION == MatchEngine.Instance.CurPhase || ePhase.KICK_OFF == MatchEngine.Instance.CurPhase) && null != MatchEngine.Instance.GetDuelPlayer(UserData.Instance.PVPTeam))
            {
                MatchEngine.Instance.ExecuteActionPVP(
                    UserData.Instance.PVPTeam,
                    !MatchEngine.Instance.HasAction(UserData.Instance.PVPTeam)
                );
            }

            if (!PhotonNetwork.IsConnected && (ePhase.ACTION == MatchEngine.Instance.CurPhase || ePhase.KICK_OFF == MatchEngine.Instance.CurPhase) && null != MatchEngine.Instance.GetDuelPlayer(eTeamMethods.GetOpposite(UserData.Instance.PVPTeam)))
            {
                MatchEngine.Instance.ExecuteActionPVP(
                    eTeamMethods.GetOpposite(UserData.Instance.PVPTeam),
                    !MatchEngine.Instance.HasAction(eTeamMethods.GetOpposite(UserData.Instance.PVPTeam))
                );
            }

            ePhase lNextPhase = MatchEngine.Instance.Engine.PhaseMain.GetNextPhase();

            //#if DEVELOPMENT_BUILD
            //Debug.Log("[MatchManager] NextResolutionPhase() Current phase = " + MatchEngine.Instance.CurPhase + " | Next phase = " + lNextPhase);
            //#endif

            if (ePhase.CHANGE_PERIOD == lNextPhase)
                EndPeriod();
            else if  (ePhase.TACTICAL_ATTACK == lNextPhase)
                MatchMessagesUI.Instance.NewPhaseAnnouncement(lNextPhase);
            else
                MatchEngine.Instance.NextPhase();
        }

        public IEnumerator NextGamePhase(float pWaitingtime = 0f)
        {
            yield return new WaitForSeconds(pWaitingtime);

            ePhase lNextPhase = MatchEngine.Instance.Engine.PhaseMain.GetNextPhase();

            //#if DEVELOPMENT_BUILD
            //Debug.Log("[MatchManager] NextGamePhase() Current phase = " + MatchEngine.Instance.CurPhase + " | Next phase = " + lNextPhase);
            //#endif

            if (ePhase.CHANGE_PERIOD == lNextPhase)
                EndPeriod();
            else
                MatchEngine.Instance.NextPhase();
            //MatchEngine.Instance.NextPhase();

            //MatchEngine.Instance.Engine.ScoreMain.IncrementTeamAScore();
            //MatchEngine.Instance.Engine.PeriodMain.ChangePeriod(ePeriod.FULL_TIME);
        }

        #endregion

        #endregion

        #region Private Methods

        #region Coroutines

        private IEnumerator Start()
        {
            UserData.Instance.Profile.NumberMatchPlayed++;
            UserData.Instance.PersistUserData();
            if (UserData.Instance.Profile.NumberMatchPlayed <= 10)
            {
                UserEventsManager.Instance.RecordEvent(new UserActionEvent("manager_match_start"));
                UserEventsManager.Instance.RecordEvent(new UserActionEvent("start_match_" + UserData.Instance.Profile.NumberMatchPlayed));
            }

            yield return new WaitForSeconds(1f);

            // Display UI
            GameObject lHeader = GameObject.Find("[OverlaySpaceUI]/Header_UI");
            if (null != lHeader)
                lHeader.GetComponent<RectTransform>().DOAnchorPosY(-30f, 1f);
        }

        private IEnumerator _halfTime()
        {
            yield return new WaitForSeconds(1f);

            UserEventsManager.Instance.RecordEvent(new UserActionEvent("manager_match_half_time"));
            UtilityMethods.InvokeGameEvent(_halfTimeStartedEvt);

            yield return new WaitForSeconds(1f);

            PlayersManager.Instance.PlacePlayers(true);
        }

        private IEnumerator _displayMatchResult(float pWaitingTime = 1f)
        {
            UserData.Instance.PvpRoomName = null;

            // Tracking end match
            UserEventsManager.Instance.RecordEvent(new UserActionEvent("end_pvp_game"));

            yield return new WaitForSeconds(pWaitingTime);

            GameObject lHeader = GameObject.Find("[OverlaySpaceUI]/Header_UI");
            if (null != lHeader)
                lHeader.GetComponent<RectTransform>().DOAnchorPosY(120f, 1f);

            PlayersManager.Instance.TogglePlayerName(false);
            MatchCamera.Instance.DisplayOutroView();
            FXManager.Instance.DisplayFogOutro();
            UtilityMethods.InvokeGameEvent(_displayMatchResultsEvt);
        }

        #endregion

        #endregion
    }
}
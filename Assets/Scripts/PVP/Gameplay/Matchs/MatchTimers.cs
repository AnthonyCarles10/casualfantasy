﻿using Sportfaction.CasualFantasy.Events;
using Sportfaction.CasualFantasy.PVP.Network;
using Sportfaction.CasualFantasy.References;

using SportFaction.CasualFootballEngine.PeriodService.Enum;
using SportFaction.CasualFootballEngine.PhaseService.Enum;

using Photon.Pun;
using UnityEngine;

namespace Sportfaction.CasualFantasy.PVP.Gameplay.Matchs
{
    /// <summary>
    /// Class handling every timer during a match
    /// </summary>
    public sealed class MatchTimers : MonoBehaviourPun
	{
        static  public  MatchTimers Instance    =   null;

        #region Properties

        public IntReference FirstPeriodTimerRef { get { return _firstPeriodTimerRef; } set { _firstPeriodTimerRef = value; } }

        public IntReference SecondPeriodTimerRef { get { return _secondPeriodTimerRef; } set { _secondPeriodTimerRef = value; } }

        #endregion

        #region Serialized Fields

        [Header("Timer handlers")]
		[SerializeField, Tooltip("Timer for first period")]
		private	NetworkedTimer	_firstPeriodTimer	=	null; //!< First period timer script
		[SerializeField, Tooltip("Timer for second period")]
		private	NetworkedTimer	_secondPeriodTimer	=	null; //!< Second period timer script
		[SerializeField, Tooltip("Timer for half-time")]
		private	NetworkedTimer	_halfTimeTimer		=	null; //!< Half-time timer script
		[SerializeField, Tooltip("Action phase timer handler")]
        private NetworkedTimer  _duelTimer          =   null; //!< Duel timer script
        [SerializeField, Tooltip("Tactical phase timer handler")]
        private NetworkedTimer  _tacticalTimer      =   null; //!< Duel timer script

        [Header("References")]
		[SerializeField, Tooltip("Ref to first period timer")]
		private	IntReference	_firstPeriodTimerRef	=	null; //!< Ref to first period timer
		[SerializeField, Tooltip("Ref to half-time timer")]
		private	IntReference	_halfTimeTimerRef		=	null; //!< Ref to half-time timer
		[SerializeField, Tooltip("Ref to second period timer")]
		private	IntReference	_secondPeriodTimerRef	=	null; //!< Ref to second period timer
        [SerializeField, Tooltip("Ref to action timer")]
        private IntReference    _actionTimerRef         =   null;

		[Header("Game Events")]
		[SerializeField, Tooltip("Event to invoke to end match")]
		private	GameEvent	_fullTimeStartedEvt	        =	null; //!< Event to invoke when match ended
        [SerializeField, Tooltip("Event to invoke when action timer is finished")]
        private GameEvent   _actionTimerFinishedEvt     =   null;
        [SerializeField, Tooltip("Event to invoke when tactical timer is finished")]
        private GameEvent   _tacticalTimerFinishedEvt   =   null;

        #endregion

        #region Private Fields

        private ePeriod _curPeriod  =	ePeriod.NONE; //!< Current match period
        private int     _choiceDuration =   0;

        #endregion

        #region Public Methods

        #region Game Event Listeners

        public  void    OnEngineStartPeriod()
		{
			_updatePeriod();
			
			switch(_curPeriod)
			{
				case ePeriod.FIRST_PERIOD:
					_firstPeriodTimer.StartTimer();
					break;
				case ePeriod.SECOND_PERIOD:
					_secondPeriodTimer.StartTimer();
					break;
				default:
					break;
			}
		}

        public  void    OnEngineStopPeriod()
		{
			_updatePeriod();
			
			if(ePeriod.HALF_TIME == _curPeriod)
			{
				_firstPeriodTimer.StopTimer();
			}
			else if (ePeriod.FULL_TIME == _curPeriod)
			{
				_secondPeriodTimer.StopTimer();
			}
		}
		
        /// <summary>
        /// Forces every timer to stop to end match
        /// </summary>
		public	void	OnForceEndMatch()
		{
			_firstPeriodTimer.StopTimer();
			_secondPeriodTimer.StopTimer();
			_halfTimeTimer.StopTimer();
		}

        public  void    OnPeriodChanged(ePeriod pPeriod)
        {
            _updatePeriod();

            switch (pPeriod)
            {
                case ePeriod.FIRST_PERIOD:
                    _firstPeriodTimer.StartTimer();
                    break;
                case ePeriod.SECOND_PERIOD:
                    _secondPeriodTimer.StartTimer();
                    break;
                case ePeriod.HALF_TIME:
                    _firstPeriodTimer.StopTimer();
                    break;
                case ePeriod.FULL_TIME:
                    _secondPeriodTimer.StopTimer();
                    _fullTimeStartedEvt.Invoke();
                    break;
            }
        }

        #endregion

        public  void    PauseCurrentPeriodTimer()
        {
            switch(MatchEngine.Instance.Engine.PeriodMain.Period)
            {
                case ePeriod.FIRST_PERIOD:
                    _firstPeriodTimer.StopTimer();
                    break;
                case ePeriod.SECOND_PERIOD:
                    _secondPeriodTimer.StopTimer();
                    break;
            }
        }

        public  void    ResumeCurrentPeriodTimer()
        {
            switch (MatchEngine.Instance.Engine.PeriodMain.Period)
            {
                case ePeriod.FIRST_PERIOD:
                    _firstPeriodTimer.StartTimer();
                    break;
                case ePeriod.SECOND_PERIOD:
                    _secondPeriodTimer.StartTimer();
                    break;
            }
        }

        public  void    StartCurrentTimer()
        {
            switch (MatchEngine.Instance.CurPhase)
            {
                case ePhase.ACTION:
                case ePhase.KICK_OFF:
                    _duelTimer.StartTimer();
                    break;
                case ePhase.TACTICAL_ATTACK:
                    _tacticalTimer.StartTimer();
                    break;
            }
        }

        public  void    StopCurrentTimer()
        {
            switch(MatchEngine.Instance.CurPhase)
            {
                case ePhase.ACTION:
                case ePhase.KICK_OFF:
                    _duelTimer.StopTimer();
                    UtilityMethods.InvokeGameEvent(_actionTimerFinishedEvt);
                    break;
                case ePhase.TACTICAL_ATTACK:
                    _tacticalTimer.StopTimer();
                    UtilityMethods.InvokeGameEvent(_tacticalTimerFinishedEvt);
                    break;
            }
        }

        public  void    PauseCurrentTimer()
        {
            switch (MatchEngine.Instance.CurPhase)
            {
                case ePhase.ACTION:
                case ePhase.KICK_OFF:
                    _duelTimer.StopTimer();
                    break;
                case ePhase.TACTICAL_ATTACK:
                    _tacticalTimer.StopTimer();
                    break;
            }
        }

        public  int GetTimerDiff()
        {
            switch (MatchEngine.Instance.CurPhase)
            {
                case ePhase.ACTION:
                case ePhase.KICK_OFF:
                    _duelTimer.StopTimer();
                    _choiceDuration = _actionTimerRef.DefaultValue - _actionTimerRef.CurrentValue;
                    return _choiceDuration;
                case ePhase.TACTICAL_ATTACK:
                    _tacticalTimer.StopTimer();
                    UtilityMethods.InvokeGameEvent(_tacticalTimerFinishedEvt);
                    return _choiceDuration;
            }

            return -1;
        }

        public  void    ResetPeriodTimers()
        {
            _firstPeriodTimerRef.CurrentValue   =   0;
            _secondPeriodTimerRef.CurrentValue  =   0;
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Initializes timers' value
        /// </summary>
        private	void    Awake()
		{
            Instance    =   this;

			_firstPeriodTimerRef.CurrentValue	=	_firstPeriodTimerRef.DefaultValue;

			_secondPeriodTimerRef.CurrentValue	=	_secondPeriodTimerRef.DefaultValue;

			_halfTimeTimerRef.CurrentValue		=	_halfTimeTimerRef.DefaultValue;

            if (null != MatchEngine.Instance)
                MatchEngine.Instance.Engine.PeriodMain.OnPeriodChanged += pPeriod => OnPeriodChanged(pPeriod);
        }

        /// <summary>
        /// Updates current period value
        /// </summary>
		private	void    _updatePeriod()
		{
			if (null != MatchEngine.Instance)
                _curPeriod  =	MatchEngine.Instance.Engine.PeriodMain.Period;
		}

		#endregion
	}
}
﻿using Sportfaction.CasualFantasy.Events;
using Sportfaction.CasualFantasy.Config;
using Sportfaction.CasualFantasy.Manager.Profile;
using Sportfaction.CasualFantasy.PVP.Gameplay.Actions;
using Sportfaction.CasualFantasy.PVP.Gameplay.Players;
using Sportfaction.CasualFantasy.PVP.Network;
using Sportfaction.CasualFantasy.PVP.Test;
using Sportfaction.CasualFantasy.Services;
using Sportfaction.CasualFantasy.Utilities;

using SportFaction.CasualFootballEngine;
using SportFaction.CasualFootballEngine.ActionService.Enum;
using SportFaction.CasualFootballEngine.ActionService.Response;
using SportFaction.CasualFootballEngine.FieldService.Model;
using SportFaction.CasualFootballEngine.PeriodService.Enum;
using SportFaction.CasualFootballEngine.PhaseService.Enum;
using SportFaction.CasualFootballEngine.PlayerService.Enum;
using SportFaction.CasualFootballEngine.PlayerService.Model;
using SportFaction.CasualFootballEngine.TeamService.Enum;
using SportFaction.CasualFootballEngine.TossService.Enum;

using Newtonsoft.Json;
using Photon.Pun;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

using   SFEngine    =   SportFaction.CasualFootballEngine.MatchEngine;
using   SFManager   =   SportFaction.CasualFootballEngine.ManagerService.Model.Manager;

namespace Sportfaction.CasualFantasy.PVP.Gameplay.Matchs
{
    /// <summary>
    /// Class handling every engine feature
    /// </summary>
    public sealed class MatchEngine : Singleton<MatchEngine>
    {
        #region Properties
        
        public  SFEngine        Engine          { get { return _engine; } set { _engine = value; } }

        public  bool            IsTester        { set { _isTester = value; } }

        public  ResponseAction  ResponseAction  { get { return _responseAction; } }

        public  ePhase          CurPhase        { get { return _curPhase; } }
        public  ePhase          PrevPhase       { get { return _prevPhase; } }


        #endregion
        
        #region Serialized Fields
        
        [Header("Essentials")]
        [SerializeField, ReadOnly, Tooltip("Is user doing a full engine synchronization?")]
        private bool        _fullSynchronization    =   false; //!< Is engine synchronizing ?

        [Header("Game Events")]
        [SerializeField, Tooltip("Event to invoke when engine is starting full synchronization")]
        private GameEvent   _onEngineFullySynchronizing     =   null; //!< Event to invoke when engine is fully synchronizing
        [SerializeField, Tooltip("Event to invoke when engine started period")]
        private GameEvent   _onEngineStartPeriod            =   null; //!< Event to invoke when engine started period
        [SerializeField, Tooltip("Event to invoke when engine stopped period")]
        private GameEvent   _onEngineStopPeriod             =   null; //!< Event to invoke when engine stopped period
        [SerializeField, Tooltip("Event to invoke when engine has changed field setup")]
        private GameEvent   _onEngineChangeFieldSetup       =   null; //!< Event to invoke when engine changed field setup
        [SerializeField, Tooltip("Event to invoke when engine executed actions")]
        private GameEvent   _onEngineExecuteActions         =   null; //!< Event to invoke when engine executed actions
        [SerializeField, Tooltip("Event to invoke when team A forfeited")]
        private GameEvent   _onEngineProclaimTeamAForfeit   =   null; //!< Event to invoke when engine proclaimed team A forfeit
        [SerializeField, Tooltip("Event to invoke when team B forfeited")]
        private GameEvent   _onEngineProclaimTeamBForfeit   =   null; //!< Event to invoke when engine proclaimed team B forfeit
        [SerializeField, Tooltip("Event to invoke when results should be displayed")]
        private GameEvent   _endMatchFXEvt                  =   null;
        [SerializeField, Tooltip("Event to invoke to start tactic phase")]
        private GameEvent   _performTactics                 =   null; //!< Event to invoke to start tactic phase
        [SerializeField, Tooltip("Event to invoke to start prepare action")]
        private GameEvent   _prepareActions                 =   null; //!< Event to invoke to start prepare action

        [Header("Configuration")]
        [SerializeField, Tooltip("Scriptable object contains infos about match UI")]
        private PVPData     _pvpData            =   null; //!< SO storing PVP match data

        #endregion

        #region Private Fields

        private SFEngine        _engine         =   null; //!< Engine used for gameplay logic
        private ResponseAction  _responseAction =   new ResponseAction(); //!< Action execution response

        private bool    _isTester       =   false; //!< Boolean to set to true if testing
        private ePhase  _curPhase       =   ePhase.NONE; //!< Current phase (without forced modifications)
        private ePhase  _prevPhase      =   ePhase.NONE; //!< Current phase (without forced modifications)

        #endregion

        #region Public Methods

        #region Event Listeners

        /// <summary>
        /// MatchEngine Event Listener > Phase Changed
        /// _engine.PhaseMain.OnPhaseChanged += pPhase => this.OnPhaseChanged(pPhase);
        /// </summary>
        public  void    OnPhaseChanged(ePhase pPhase)
        {
            #if UNITY_EDITOR && DEVELOPMENT_BUILD
                Debug.LogFormat("<color=lime> [MatchEngine] OnPhaseChanged => "+ pPhase +" </color>");
            #endif

            _prevPhase  =   _curPhase;
            _curPhase   =   pPhase;

            switch (pPhase)
            {
                case ePhase.LOAD_GAMEPLAY_SCENE:
                    if (null != Meta.Matchmaking.Instance)
                    {
                        Meta.Matchmaking.Instance.CountdownCoroutine = Meta.Matchmaking.Instance.OnOpponentFinded();
                        StartCoroutine(Meta.Matchmaking.Instance.CountdownCoroutine);
                    }
                    else if (_isTester)
                        PhotonMain.Instance.LoadGamePlayTest();

                    break;
                case ePhase.PRE_TOSS:
                    StartCoroutine(MatchManager.Instance.NextGamePhase(0f));

                    break;

                case ePhase.TOSS:
                    if (true == PhotonNetwork.IsConnected)
                    {
                        if (PhotonNetwork.IsMasterClient)
                            PhotonMain.Instance.PhotonView.RPC("RPC_ReadyForToss", RpcTarget.All, eCoinSide.HEADS);
                    }
                    else
                    {
                        _engine.TossMain.CoinDraw();

                        ePhase lNextPhase = Engine.PhaseMain.GetNextPhase();
                        Engine.PhaseMain.ChangePhase(lNextPhase); // TOSS_RESOLUTION
                    }

                    break;

                case ePhase.KICK_OFF:
                    MatchAudio.Instance.KickOff();
                    PlayersManager.Instance.UpdatePlayerPossession();
                    _engine.ActionMain.UpdateActionsPlayers();
                    if (null != MatchTimers.Instance)
                        MatchTimers.Instance.PauseCurrentPeriodTimer();

                    //string[]    lPositions  =   new string[] { "G", "DCD", "DCG", "DD", "DG" };
                    //int         rand        =   UnityEngine.Random.Range(0, lPositions.Length - 1);
                    //Player      lReceiver   =   _engine.TeamMain.GetPlayer(_engine.PossessionMain.CurTeamPossession, ePositionMethods.ConvertStringToEPosition(lPositions[rand]));

                    //if (ActionsManager.Instance.IsTester && null != MatchTutorial.Instance && 0 >= MatchTutorial.Instance.TutorialData.CounterActionPhase)
                    //    lReceiver   =   _engine.TeamMain.GetPlayer(_engine.PossessionMain.CurTeamPossession, MatchTutorial.Instance.StartPosition);

                    //// Handle automatic first pass
                    //if (false == PhotonNetwork.IsConnected && _engine.PossessionMain.CurTeamPossession != UserData.Instance.PVPTeam)
                    //{
                    //    PrepareAction(eExecutorMethods.GetEExecutor(_engine.PossessionMain.CurTeamPossession, true), eAction.PASS, _engine.PossessionMain.CurPlayerPossession, lReceiver.CurFieldCase);
                    //    //_engine.ActionMain.ExecuteAction();
                    //}
                    //else
                    //{
                    //    if (UserData.Instance.PVPTeam == _engine.PossessionMain.CurTeamPossession)
                    //    {
                    //        PrepareAction(eExecutorMethods.GetEExecutor(_engine.PossessionMain.CurTeamPossession, true), eAction.PASS, _engine.PossessionMain.CurPlayerPossession, lReceiver.CurFieldCase);
                    //        //PhotonMain.Instance.PhotonView.RPC("RPC_PrepareAction", RpcTarget.Others, eExecutorMethods.GetEExecutor(UserData.Instance.PVPTeam, true), eAction.PASS, _engine.PossessionMain.CurPlayerPossession, lReceiver.CurFieldCase.ConvertToJson());
                    //    }
                    //}
                    break;

                case ePhase.TACTICAL_ATTACK:
                    UtilityMethods.InvokeGameEvent(_performTactics);
                    if(null != MatchTimers.Instance)
                        MatchTimers.Instance.PauseCurrentPeriodTimer();
                    return;

                case ePhase.PERIOD_INIT:
                    if (null != MatchTimers.Instance)
                        MatchTimers.Instance.ResumeCurrentPeriodTimer();

                    ActionsManager.Instance.RevealMoves();

                    _responseAction = _engine.ActionMain.Response;
                    _onEngineExecuteActions.Invoke();

                    return;

                case ePhase.ACTION_RESOLUTION:
                    if (null != MatchTimers.Instance)
                        MatchTimers.Instance.ResumeCurrentPeriodTimer();

                    ActionsManager.Instance.RevealMoves();
                    if (false == PhotonNetwork.IsConnected)
                        _engine.ActionMain.ExecuteAction();

                    _responseAction = _engine.ActionMain.Response;
                    _onEngineExecuteActions.Invoke();

                    return;

                case ePhase.ACTION:
                    _engine.ActionMain.UpdateActionsPlayers();
                    if (null != MatchTimers.Instance)
                        MatchTimers.Instance.PauseCurrentPeriodTimer();
                    _prepareActions.Invoke();

                    return;

                case ePhase.CHANGE_PERIOD:
                    MatchManager.Instance.EndPeriod();
                    return;

                default:
                    break;
            }
        }

        /// <summary>
        /// MatchEngine Event Listener > Period Changed
        /// _engine.PeriodMain.OnPeriodChanged += pPeriod => this.OnPeriodChanged(pPhase);
        /// </summary>
        public  void    OnPeriodChanged(ePeriod pPeriod)
        {
            #if UNITY_EDITOR && DEVELOPMENT_BUILD
                Debug.LogFormat("<color=#00CED1> [MatchEngine] OnPeriodChanged => " + pPeriod + " </color>");
            #endif

            if (ePeriod.FIRST_PERIOD == pPeriod || ePeriod.SECOND_PERIOD == pPeriod)
            {
                _onEngineStartPeriod.Invoke();
            }
            if (ePeriod.HALF_TIME == pPeriod || ePeriod.FULL_TIME == pPeriod)
            {
                _engine.PhaseMain.Phase =   ePhase.TOSS_RESOLUTION;

                _engine.FieldSetupMain.ChangeFieldSetup("FS_A_KICK_OFF", ePosition.ACD, eTeamMethods.GetOpposite(_engine.TossMain.TeamWinning));

                _onEngineStopPeriod.Invoke();
            }
        }

        /// <summary>
        /// MatchEngine Event Listener > Field Setup Changed
        /// _engine.FieldSetupMain.OnCurrentFieldSetupChanged += pFieldSetup => this.OnChangeFieldSetup(pPhase);
        /// </summary>
        public  void    OnChangeFieldSetup(string pFieldSetup)
        {
            #if UNITY_EDITOR && DEVELOPMENT_BUILD
                Debug.LogFormat("<color=#00CED1> [MatchEngine] OnChangeFieldSetup => " + pFieldSetup + " </color>");
            #endif

            _onEngineChangeFieldSetup.Invoke();
        }

        /// <summary>
        /// Event Listener > TacticsSkipped
        /// </summary>
        public  void    OnTacticsSkipped()
        {
            _engine.ActionMain.CheatClearResponseAction();
        }

        /// <summary>
        /// Init Events from MatchEngine Engine
        /// </summary>
        public  void    InitEventsListener()
        {
            _engine.PhaseMain.OnPhaseChanged += pPhase => OnPhaseChanged(pPhase);
            _engine.PhaseMain.OnPhaseChanged += pPhase => PhotonMain.Instance.OnPhaseChanged(pPhase);

            _engine.PeriodMain.OnPeriodChanged += (pPeriod) => OnPeriodChanged(pPeriod);

            _engine.FieldSetupMain.OnCurrentFieldSetupChanged += (pFieldSetup) => OnChangeFieldSetup(pFieldSetup);

            _engine.MatchMain.OnEndMatchInfoReceived += (pData) => {
                Debug.Log(pData.ConvertToJson());
                _endMatchFXEvt.Invoke();
            };
        }

        public  void    RemoveEventsListener()
        {
            _engine.PhaseMain.OnPhaseChanged = null;

            _engine.PeriodMain.OnPeriodChanged = null;

            _engine.FieldSetupMain.OnCurrentFieldSetupChanged = null;

            _engine.MatchMain.OnEndMatchInfoReceived = null;
        }

        public  void    OnPeriodTimerFinished()
        {
            if (true == PhotonNetwork.IsConnected)
            {
                PhotonMain.Instance.PhotonView.RPC("RPC_NeedToChangePeriod", RpcTarget.Others, UserData.Instance.PVPTeam);
                _engine.PeriodMain.NeedToChangePeriod = true;
            }
            else
            {
                PhotonMain.Instance.RPC_NeedToChangePeriod(UserData.Instance.PVPTeam);
            }
        }

        #endregion

        public  void    InitEngine(bool pReconnect = false)
        {
            #if UNITY_EDITOR && DEVELOPMENT_BUILD
                Debug.LogFormat("<color=#00CED1> [MatchEngine] InitEngine </color>");
            #endif

            _engine =   new SFEngine();

            

            if (eConfig.DEV == ConfigData.Instance.ENV)
            {
                var maxRetryAttempts = 3;
                var pauseBetweenFailures = System.TimeSpan.FromSeconds(2);
                RetryHelper.RetryOnException(maxRetryAttempts, pauseBetweenFailures, () => {
                    _engine.LoadInitData(ConfigData.Instance.ENV, UserData.Instance.Profile.OnBoarding, UserData.Instance.Profile.OnEmptyMinions);
                });
         
            }
            else
            {
                var maxRetryAttempts = 3;
                var pauseBetweenFailures = System.TimeSpan.FromSeconds(2);
                RetryHelper.RetryOnException(maxRetryAttempts, pauseBetweenFailures, () => {
                    _engine.LoadInitData(ConfigData.Instance.ENV, UserData.Instance.Profile.OnBoarding);
                });
            }



            if (false == pReconnect)
            {
                SFManager lManagerTeamA = new SFManager();
                lManagerTeamA.Id = UserData.Instance.Profile.Id.ToString();
                lManagerTeamA.Name = UserData.Instance.Profile.Pseudo;
                lManagerTeamA.TeamId = _pvpData.TeamId.ToString();
                lManagerTeamA.Team = eTeam.TEAM_A.ToString();

                var maxRetryAttempts = 3;
                var pauseBetweenFailures = System.TimeSpan.FromSeconds(2);
                RetryHelper.RetryOnException(maxRetryAttempts, pauseBetweenFailures, () => {
                    _engine.ManagerMain.SetManagerA(lManagerTeamA);
                });

            }

            InitEventsListener();
        }

        public  void    StartPeriod()
        {
            if (true == PhotonNetwork.IsConnected)
            {
                PhotonMain.Instance.PhotonView.RPC("RPC_OnManagerReadyToChangePeriod", RpcTarget.Others, UserData.Instance.PVPTeam);
            }
            else
            {
                Engine.PeriodMain.NextPeriod();
                //Debug.Log("[MatchEngine] StartPeriod() Current Phase = " + _engine.PhaseMain.Phase + " | Next phase = " +  _engine.PhaseMain.GetNextPhase());
            }
        }

        public	void	StopPeriod()
        {
            if (true == PhotonNetwork.IsConnected)
            {
                PhotonMain.Instance.PhotonView.RPC("RPC_OnManagerReadyToChangePeriod", RpcTarget.Others, UserData.Instance.PVPTeam);
            }
            else
            {
                if (!_isTester)
                {
                    _engine.PossessionMain.ChangeTeamPossession(eTeamMethods.GetOpposite(_engine.TossMain.TeamWinning));
                    _engine.PossessionMain.ChangePlayerPossession(ePosition.ACD);
                }

                Engine.PeriodMain.NextPeriod();
            }
        }

        public  void    NextPhase()
        {
            #if DEVELOPMENT_BUILD
            Debug.Log("[MatchEngine] NextPhase() : Current phase = " + _engine.PhaseMain.Phase + " ===> Next = " + _engine.PhaseMain.GetNextPhase());
            #endif

            if (true == PhotonNetwork.IsConnected)
            {
                int lTimerValue =   _engine.PeriodMain.Period == ePeriod.FIRST_PERIOD ? MatchTimers.Instance.FirstPeriodTimerRef.CurrentValue : MatchTimers.Instance.SecondPeriodTimerRef.CurrentValue;

                PhotonMain.Instance.PhotonView.RPC("RPC_ManagerIsReadyToChangePhase", RpcTarget.Others, UserData.Instance.PVPTeam, lTimerValue);
            }
            else
            {
                ePhase lNextPhase = Engine.PhaseMain.GetNextPhase();

                if (ePhase.PERIOD_INIT == lNextPhase)
                    _engine.ActionMain.ExecuteAction();

                Engine.PhaseMain.ChangePhase(lNextPhase); 
            }
        }
        
        public  void    ChangePhase(ePhase pPhase)
        {
            if (false == PhotonNetwork.IsConnected)
            {
                _engine.PhaseMain.ChangePhase(pPhase);
            }
            else
            {
                int lTimerValue = _engine.PeriodMain.Period == ePeriod.FIRST_PERIOD ? MatchTimers.Instance.FirstPeriodTimerRef.CurrentValue : MatchTimers.Instance.SecondPeriodTimerRef.CurrentValue;

                PhotonMain.Instance.PhotonView.RPC("RPC_ManagerIsReadyToChangePhase", RpcTarget.Others, UserData.Instance.PVPTeam, lTimerValue);
            }
        }

        public  void    LaunchPreparationPhase()
        {
            _prepareActions.Invoke();
        }

        #region Simulation Methods

        public  PassSimulationResponse      SimulationPass(Player pPlayer)
        {
            _engine.ActionMain.SimulateAction(pPlayer, eAction.PASS);
            
            PassSimulationResponse  lSimulation =   new PassSimulationResponse();

            if(pPlayer.Team == eTeam.TEAM_A)
            {
                lSimulation.Parse(_engine.ActionMain.Response.TeamA.Simulation);
            }
            else
            {
                lSimulation.Parse(_engine.ActionMain.Response.TeamB.Simulation);
            }
          
            
            return lSimulation;
        }
        
        public  LongKickSimulationResponse  SimulationLongKick(Player pPlayer)
        {
            _engine.ActionMain.SimulateAction(pPlayer, eAction.LONG_KICK);
            
            LongKickSimulationResponse  lSimulation =   new LongKickSimulationResponse();

            if (pPlayer.Team == eTeam.TEAM_A)
            {
                lSimulation.Parse(_engine.ActionMain.Response.TeamA.Simulation);
            }
            else
            {
                lSimulation.Parse(_engine.ActionMain.Response.TeamB.Simulation);
            }

            return lSimulation;
        }
        
        public  MoveSimulationResponse      SimulationMove(Player pPlayer)
        {
            _engine.ActionMain.SimulateAction(pPlayer, eAction.MOVE);
            
            MoveSimulationResponse  lSimulation =   new MoveSimulationResponse();

            if (eTeam.TEAM_A == pPlayer.Team)
            {
                lSimulation.Parse(_engine.ActionMain.Response.TeamA.Simulation);
            }
            else
            {
                lSimulation.Parse(_engine.ActionMain.Response.TeamB.Simulation);
            }

            return lSimulation;
        }
        
        public  DribbleSimulationResponse   SimulationDribble(Player pPlayer)
        {
            _engine.ActionMain.SimulateAction(pPlayer, eAction.DRIBBLE);

            DribbleSimulationResponse lSimulation = null;

            if (pPlayer.Team == eTeam.TEAM_A)
            {
                lSimulation = new DribbleSimulationResponse(_engine.ActionMain.Response.TeamA.Simulation);
            }
            else
            {
                lSimulation = new DribbleSimulationResponse(_engine.ActionMain.Response.TeamB.Simulation);
            }

            return lSimulation;
        }

        #endregion

        #region Getters

        public  Player  GetCurrentPossesser()
        {
            return _engine.TeamMain.GetPlayer(_engine.PossessionMain.CurTeamPossession, _engine.PossessionMain.CurPlayerPossession);
        }

        public  Player  GetDuelPlayer(eTeam pTeam)
        {
            Player lPlayerA = null, lPlayerB = null;

            if (eTeam.TEAM_A == _engine.PossessionMain.CurTeamPossession)
            {
                lPlayerA = _engine.TeamMain.GetPlayer(eTeam.TEAM_A, _engine.PossessionMain.CurPlayerPossession);
                if (null != lPlayerA)
                {
                    lPlayerB = _engine.OppositionMain.GetFrontOpponentPlayer(lPlayerA);
                    if (null != lPlayerB && lPlayerA.Team == lPlayerB.Team)
                        lPlayerB = null;
                }
            }
            else
            {
                lPlayerB = _engine.TeamMain.GetPlayer(eTeam.TEAM_B, _engine.PossessionMain.CurPlayerPossession);
                if (null != lPlayerB)
                {
                    lPlayerA = _engine.OppositionMain.GetFrontOpponentPlayer(lPlayerB);
                    if (null != lPlayerA && lPlayerA.Team == lPlayerB.Team)
                        lPlayerA = null;
                }
            }

            if (eTeam.TEAM_A == pTeam)
                return lPlayerA;
            
            return lPlayerB;
        }

        public  Player  GetPlayer(eTeam pTeam, ePosition pPosition)
        {
            return _engine.TeamMain.GetPlayer(pTeam, pPosition);
        }
        
        public  float   GetTeamScore(eTeam pTeam)
        {
            if (eTeam.TEAM_A == pTeam)
                return _engine.ScoreMain.TeamAScore;

            return _engine.ScoreMain.TeamBScore;
        }

        public  ResponseAction  GetResponse()
        {
            return _engine.ActionMain.Response;
        }

        public  int     GetTeamTacticalPoints(eTeam pTeam)
        {
            if (eTeam.TEAM_A == pTeam)
                return _engine.ManagerMain.ManagerTeamA.TacticalPoints;

            return _engine.ManagerMain.ManagerTeamB.TacticalPoints;
        }

        #endregion

        public  void    SelectPlayer(Player pPlayer)
        {
            _engine.ActionMain.SelectPlayer(pPlayer.Team, pPlayer.Position);
        }
        
        public  void    PrepareAction(eExecutor pExecutor, eAction pAction, ePosition pPosition, FieldCase pTarget)
        {
            if (ePhase.ACTION != _curPhase && ePhase.KICK_OFF != _curPhase && ePhase.TACTICAL_DEFENSE != _curPhase && ePhase.TACTICAL_ATTACK != _curPhase)
                return;

            #if UNITY_EDITOR && DEVELOPMENT_BUILD
                Debug.LogFormat("<color=yellow>[MatchEngine] PrepareAction() " + pExecutor + " " + pAction + " " + pPosition + " " + pTarget?.ToString() + " </color>");
            #endif

            if (true == PhotonNetwork.IsConnected)
            {
                PhotonMain.Instance.PhotonView.RPC("RPC_PrepareAction", RpcTarget.Others, pExecutor, pAction, pPosition, pTarget?.ConvertToJson());
                PhotonMain.Instance.RPC_PrepareAction(pExecutor, pAction, pPosition, pTarget?.ConvertToJson());
            }
            else
            {
                PhotonMain.Instance.RPC_PrepareAction(pExecutor, pAction, pPosition, pTarget?.ConvertToJson());
            }

        }

        public  void    ExecuteActionPVP(eTeam pTeam, bool pAutoAction)
        {
            if (!pAutoAction)
                return;

            if (ePhase.KICK_OFF == _curPhase)
            {
                string[]    lPositions  =   new string[] { "ACG", "MCD", "MCG", "MD", "MG" };
                int         rand        =   UnityEngine.Random.Range(0, lPositions.Length - 1);
                Player      lReceiver   =   _engine.TeamMain.GetPlayer(_engine.PossessionMain.CurTeamPossession, ePositionMethods.ConvertStringToEPosition(lPositions[rand]));

                if (ActionsManager.Instance.IsTester && null != MatchTutorial.Instance && 0 >= MatchTutorial.Instance.TutorialData.CounterActionPhase)
                    lReceiver   =   _engine.TeamMain.GetPlayer(_engine.PossessionMain.CurTeamPossession, MatchTutorial.Instance.StartPosition);

                // Handle automatic first pass
                if (false == PhotonNetwork.IsConnected)
                {
                    PrepareAction(eExecutorMethods.GetEExecutor(_engine.PossessionMain.CurTeamPossession, true), eAction.PASS, _engine.PossessionMain.CurPlayerPossession, lReceiver.CurFieldCase);
                }
                else
                {
                    if (UserData.Instance.PVPTeam == _engine.PossessionMain.CurTeamPossession)
                    {
                        PrepareAction(eExecutorMethods.GetEExecutor(_engine.PossessionMain.CurTeamPossession, true), eAction.PASS, _engine.PossessionMain.CurPlayerPossession, lReceiver.CurFieldCase);
                        //PhotonMain.Instance.PhotonView.RPC("RPC_PrepareAction", RpcTarget.Others, eExecutorMethods.GetEExecutor(UserData.Instance.PVPTeam, true), eAction.PASS, _engine.PossessionMain.CurPlayerPossession, lReceiver.CurFieldCase.ConvertToJson());
                    }
                }
            }
            else if (ePhase.ACTION == _curPhase)
            {
                Dictionary<string, object> lResponse = _engine.AIMain.PrepareAction(pTeam);
                PrepareAction(eExecutorMethods.GetEExecutor(pTeam, true), (eAction)lResponse["Action"], (ePosition)lResponse["Position"], (FieldCase)lResponse["FieldCaseTarget"]);
            }
        }

        public	void    ProclaimForfeitForTeamA()
        {
            _engine.ScoreMain.ProclaimForfeit(eTeam.TEAM_A);
            
            UtilityMethods.InvokeGameEvent(_onEngineProclaimTeamAForfeit);
        }
        
        public	void    ProclaimForfeitForTeamB()
        {
            _engine.ScoreMain.ProclaimForfeit(eTeam.TEAM_B);
            
            UtilityMethods.InvokeGameEvent(_onEngineProclaimTeamBForfeit);
        }
        
        [ContextMenu("Do a full engine synchronization")]
        public	void    DoFullSynchronization()
        {
            _fullSynchronization	=	true;
            
            Assert.IsNotNull(PhotonNetwork.PlayerListOthers, "[MatchEngine] DoFullSynchronization(), PhotonNerwork.PlayerListOthers is null.");
            
            Assert.IsTrue(0 < PhotonNetwork.PlayerListOthers.Length, "[MatchEngine] DoFullSynchronization(), PhotonNetwork.PlayerListOthers is empty.");

            PhotonMain.Instance.PhotonView.RPC("RPC_NeedFullSynchronization", PhotonNetwork.PlayerListOthers[0]);

            UtilityMethods.InvokeGameEvent(_onEngineFullySynchronizing);
        }
        
        public  bool    IsDoingAFullSynchronization()
        {
            return  _fullSynchronization;
        }

        #region Utilities Methods

        public  bool    HasAction(eTeam pTeam)
        {
            ResponseAction lResponse = GetResponse();
            if (eTeam.TEAM_A == pTeam)
            {
                if (null == GetDuelPlayer(eTeam.TEAM_A))
                    return true;

                foreach (PlayerResponse pRes in lResponse.TeamA.Players)
                    if (eAction.MOVE.ToString() != pRes.Action)
                        return true;

                return false;
            }

            if (null == GetDuelPlayer(eTeam.TEAM_B))
                return true;

            foreach (PlayerResponse pRes in lResponse.TeamB.Players)
                if (eAction.MOVE.ToString() != pRes.Action)
                    return true;

            return false;
        }

        public  bool    TeamIsDone(eTeam pTeam)
        {
            if (ePhase.ACTION == _curPhase)
                return HasAction(pTeam);

            return false;
        }

        #endregion

        #endregion

        private void    Start()
        {
            #if DEVELOPMENT_BUILD
                UnitySystemConsoleRedirector.Redirect();
            #endif
        }
    }
}

﻿using Sportfaction.CasualFantasy.Manager.Profile;
using Sportfaction.CasualFantasy.PVP.Gameplay.Actions;

using SportFaction.CasualFootballEngine.ActionService.Enum;
using SportFaction.CasualFootballEngine.FieldService.Model;
using SportFaction.CasualFootballEngine.TeamService.Enum;
using SportFaction.CasualFootballEngine.Utilities;

using DG.Tweening;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Sportfaction.CasualFantasy.PVP.Gameplay.Field
{
	/// <summary>
	/// Handles soccer field grid
	/// </summary>
    /// @author Madeleine Tranier
	public sealed class SoccerFieldUI : Singleton<SoccerFieldUI>
    {
        #region Serialized Fields

        [SerializeField, Tooltip("Field Grid Container")]
		private	Transform   _fieldGrid	        =	null; //!< Field grid container
        [SerializeField, Tooltip("Field case prefab")]
        private Image       _fieldCasePrefab    =   null; //!< Grid case prefab
        [SerializeField, Tooltip("Field board animation")]
        private Animation   _fieldBoardAnim     =   null;

        [Header("Config")]
        [SerializeField, Tooltip("Sprites")]
        private Sprite[]    _spriteConfig;

        #endregion
        
        #region Private Fields
        
        private bool        _isManagerTeamA =   true; //!< Is current manager Team A's manager ?
        private List<Image> _fieldCases     =   new List<Image>(); //!< List of grid image cases

        #endregion

        #region Public Methods

        public  void    OnEngineInitialized()
        {
            // Flips field cases container if current manager is PVP Client
            _isManagerTeamA         =   (UserData.Instance.PVPTeam == eTeam.TEAM_A);
            _fieldGrid.eulerAngles  =   new Vector3(90f, 0f, _isManagerTeamA ? 0f : 180f);
        }
        
        public  void    DisableGridHighlight()
        {
            for (int li = 0; li < _fieldCases.Count; li++)
                _fieldCases[li].enabled =   false;
        }
        
        public  void    HighlightFieldCase(FieldCase pFC, bool pHighlight, Color32 pColor)
        {
            Image   lCase       =   _fieldCases[pFC.X * Constants.SoccerFieldHeight + pFC.Y];
            //Debug.Log("HighlightFieldCase() " + lCase.name  + " (" + pX + "-" + pY + ")");
            
            if (pHighlight)
            {
                lCase.color     =   pColor;
                lCase.DOFade(1f, 0f);
            }
            
            lCase.enabled   =   pHighlight;
        }

        public  void    HighlightFieldCase(ushort pX, ushort pY, bool pShouldHighlight, eColorDifficulty pHighlightColor, FieldCase pOrigin = null)
        {
            _fieldGrid.localEulerAngles = new Vector3(90f, _isManagerTeamA ? 0f : 180f, 0f);
            Image   lCase       =   _fieldCases[pX * Constants.SoccerFieldHeight + pY];
            //Debug.Log("HighlightFieldCase() " + lCase.name + " (" + pX + "-" + pY + ") " + pOrigin?.ConvertToJson());

            if (pShouldHighlight && null != pOrigin)
            {
                bool    lDisplay    =   ((pX - pOrigin.X) % 3 == 0) && ((pY - pOrigin.Y) % 3 == 0);
                lCase.color         =   ActionsManager.Instance.ColorConfig.GetColorDifficulty(pHighlightColor);
                lCase.DOFade(lDisplay ? 1f : 0f, 0f);
                lCase.transform.localScale  =   lDisplay ? new Vector3(3f, 3f, 3f) : new Vector3(1f, 1f, 1f);
            }

            lCase.enabled   =   pShouldHighlight && pHighlightColor != eColorDifficulty.NONE;
        }

        public  void    HighlightFieldCase(FieldCase pFC, bool pHighlight)
        {
            Image   lCase   =   _fieldCases[pFC.X * Constants.SoccerFieldHeight + pFC.Y];
            //Debug.Log("HighlightFieldCase() " + lCase.name + " (" + pFC.X + "-" + pFC.Y + ")");

            if (pHighlight)
            {
                lCase.sprite    =   UtilityMethods.GetSprite(_spriteConfig, "Bubble_Dot_Blue");
                lCase.DOFade(1f, 0f);
            }

            lCase.enabled   =   pHighlight;
        }

        public  void    HideFieldBoard()
        {
            _fieldBoardAnim.Play("BoardAdd_Intro");
        }

        #endregion

        #region Private Methods

        private void    Start()
		{
            Image   lFieldCase  =   null;
            OnEngineInitialized();

            // Instantiates every field grid case
            for (int li = 0; li < Constants.SoccerFieldWidth; li++)
            {
                for (int lj = 0; lj < Constants.SoccerFieldHeight; lj++)
                {
                    lFieldCase      =   Instantiate(_fieldCasePrefab, _fieldGrid);
                    lFieldCase.name =   "FieldCase-" + li + "-" + lj;
                    _fieldCases.Add(lFieldCase);
                }
            }
		}

        #endregion
    }
}
﻿using Sportfaction.CasualFantasy.PVP.Gameplay.Actions;
using Sportfaction.CasualFantasy.PVP.Gameplay.Matchs;
using Sportfaction.CasualFantasy.Services.Userstracking;

using SportFaction.CasualFootballEngine.ActionService.Response;
using SportFaction.CasualFootballEngine.FieldService.Model;

using DG.Tweening;
using UnityEngine;

namespace Sportfaction.CasualFantasy.PVP.Gameplay.Field
{
    /// <summary>
    /// Class handling display & interactions with clickable FieldCase
    /// </summary>
    /// @author Madeleine Tranier
    public class FieldCaseButton : MonoBehaviour
    {
        #region Properties

        public  bool    FcHovered   { get { return _fcHovered; } }

        #endregion

        #region Serialized Fields

        [SerializeField, Tooltip("Collider")]
        private BoxCollider     _collider   =   null;
        [SerializeField, Tooltip("FC Sprite renderer")]
        private SpriteRenderer  _fcRenderer =   null;

        [ReadOnly, Tooltip("Field case")]
        private FieldCase   _fieldCase  =   null;

        #endregion

        #region Private Fields

        private bool    _btnDown    =   false; //!< Is button touched ?
        private bool    _mouseDown  =   false;
        private bool    _fcHovered  =   false;

        #endregion

        #region Public Methods

        #region Touch Events

        public  void    OnMouseDown()
        {
            _btnDown = true;
            _activateButton(true);
        }

        public  void    OnMouseEnter()
        {
            if (_mouseDown || Input.touchCount > 0)
            {
                _btnDown = true;
                _activateButton(true);
            }
        }

        public  void    OnMouseUp()
        {
            if (_btnDown)
            {
                UserEventsManager.Instance.RecordEvent(new UserActionEvent("manager_match_validate_tactical_move"));
                MatchAudio.Instance.OnTapValidation();
                ActionsManager.Instance.ValidateFieldCase(_fieldCase);
                ActionsManager.Instance.PrepareAction(ActionsManager.Instance.ActionInProcess);
                Hide();
                _activateButton(false);
            }

            _btnDown = false;
        }

        public  void    OnMouseExit()
        {
            if (_btnDown)
                _activateButton(false);

            _btnDown = false;
        }

        #endregion

        public  void    Init(AvailableFieldCase pValue, bool pHasOpponent, Vector3 pPosition, FieldCase pPlayerFC, bool pCanMove)
        {
            _mouseDown  =   false;
            _btnDown    =   false;
            _activateButton(false);
            _fieldCase = pValue.FieldCase;

            this.transform.position =   pPosition;
            this.gameObject.SetActive(true);

            int lDistance       =   _getDistance(pPlayerFC, pValue);
            this.name           =   "move_" + pValue.Direction.ToLower() + "_" + lDistance;
            _fcRenderer.sprite  =   UtilityMethods.GetSprite(ActionsManager.Instance.SpriteConfig, "GameplayTacticPlacement" + (pValue.IsPassTrajectory ? "" : "Blue"));

            //if (pHasOpponent)
            //    _fcRenderer.color = ActionsManager.Instance.ColorConfig.GetColorDifficulty(pValue.HasPlayerColor, pCanMove);
            if (pValue.IsPassTrajectory)
                _fcRenderer.color = ActionsManager.Instance.ColorConfig.GetColorDifficulty(SportFaction.CasualFootballEngine.ActionService.Enum.eColorDifficulty.YELLOW, pCanMove);
            else
                _fcRenderer.color = pCanMove ? Color.white : Color.gray;

            _collider.enabled   =   pCanMove;
        }

        public  void    Hide()
        {
            this.gameObject.SetActive(false);
            _collider.enabled   =   false;
        }

        /// <summary>
        /// Retrieve selection animation normalized time
        /// </summary>
        public  float   GetSelectionFrame()
        {
            float lCurrentFrame = 0f;

            if (this.GetComponent<Animator>().enabled)
            {
                AnimatorStateInfo lState = this.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0);
                lCurrentFrame = lState.normalizedTime;
            }

            return lCurrentFrame;
        }

        #endregion

        #region Private Methods

        private void    Update()
        {
            if (Input.GetMouseButtonDown(0))
            {
                _mouseDown = true;
            }
            if (Input.GetMouseButtonUp(0))
            {
                _mouseDown = false;
                if (_btnDown)
                    OnMouseUp();
            }
        }

        private void    _activateButton(bool pActive)
        {
            this.GetComponent<Animator>().enabled = !pActive;

            if (!pActive)
            {
                FieldCaseButton lRef    =   ActionsManager.Instance.FindSelectableFC();
                if (this.isActiveAndEnabled)
                    this.GetComponent<Animator>().Play("MoveCase_Idle", 0, null != lRef ? lRef.GetSelectionFrame() : 0f);
            }

            this.transform.GetChild(0).DOScale(pActive ? new Vector3(3f, 3f, 1f) : new Vector3(1f, 1f, 1f), 0.1f);
            _fcHovered  =   pActive;
        }

        private int     _getDistance(FieldCase pOrigin, AvailableFieldCase pDestination)
        {
            switch (pDestination.Direction)
            {
                case "TOP":
                case "DOWN":
                    return Mathf.Abs(pDestination.FieldCase.Y - pOrigin.Y) / 3;

                default:
                    return Mathf.Abs(pDestination.FieldCase.X - pOrigin.X) / 3;
            }
        }

        #endregion
    }
}
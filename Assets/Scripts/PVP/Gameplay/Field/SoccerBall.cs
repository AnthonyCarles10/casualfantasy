﻿using Sportfaction.CasualFantasy.Config;
using Sportfaction.CasualFantasy.PVP.Gameplay.Matchs;
using Sportfaction.CasualFantasy.PVP.Gameplay.Players;

using DG.Tweening;
using UnityEngine;

namespace Sportfaction.CasualFantasy.PVP.Gameplay
{
    /// <summary>
    /// Class handling ball movements during match
    /// </summary>
    /// @author Madeleine Tranier
    public sealed class SoccerBall : Singleton<SoccerBall>
	{
        #region Fields

		[Header("Configuration")]
		[SerializeField, Tooltip("Timing config")]
		private TimingConfig	_timingConfig	=	null;
		
		private	PlayerPVP	_anchor				=	null;	// Anchor the ball has to follow
		private	bool		_shouldFollowAnchor	=	false;	// Should the ball follow specific anchor ?
		
		#endregion

		#region Public Methods

		#region Game Event Listeners

        /// <summary>
        /// Event Listener > OnHalfTimeStarted | OnFullTimeStarted
        /// </summary>
		public	void	OnPeriodStopped()
		{
			FollowPlayer(false);
		}

		#endregion
        
        public  void    RevealBall(PlayerPVP lPossesser)
        {
            this.transform.position     =   lPossesser.Movements.FootTransform.position + new Vector3(0f, 3.5f, 0f);

            this.transform.localScale   =   new Vector3(1.2f, 1.2f, 1.2f);
            this.transform.DOMove(lPossesser.Movements.FootTransform.position + new Vector3(0f, 0.04f, 0f), 0.5f).OnComplete(() => FollowPlayer(true, lPossesser));
        }

        public  void    PlaceBall(bool pAnimate = true)
        {
            PlayerPVP   lPossesser  =   PlayersManager.Instance.GetPlayer(MatchEngine.Instance.Engine.PossessionMain.CurTeamPossession, MatchEngine.Instance.Engine.PossessionMain.CurPlayerPossession);
            //FollowPlayer(true, lPossesser);
            if (pAnimate)
                RevealBall(lPossesser);
            else
            {
                this.transform.localScale   =   new Vector3(1.2f, 1.2f, 1.2f);
                this.transform.position     =   lPossesser.Movements.FootTransform.position;
            }
        }

        public  void    MoveToCenter()
        {
            this.transform.localPosition = new Vector3(0f, 0f, -0.06f);
        }

        public	void	FollowPlayer(bool pShouldFollow, PlayerPVP pAnchor = null)
		{
            _shouldFollowAnchor	=	pShouldFollow;
			
			if (pShouldFollow) _anchor = pAnchor;
		}

		public	Tween	MoveTo(Vector3 pPosition, float pAnimDuration = 0f)
		{	
			return this.transform.DOLocalMove(new Vector3(pPosition.x, pPosition.y, -0.052f), pAnimDuration);
		}
		
		public	Tween	WorldMoveTo(Vector3 pPosition, float pAnimDuration = 0f)
		{
			return this.transform.DOMove(pPosition, pAnimDuration);
		}

		#endregion

		#region Private Methods
		
        private void    Update()
		{
            gameObject.GetComponent<MeshRenderer>().sharedMaterial.SetFloat("Add", gameObject.transform.position.y);

            GameObject  shadow_Gob          =   GameObject.Find("Ball_Shadow");
            shadow_Gob.transform.position   =   new Vector3(gameObject.transform.position.x, shadow_Gob.transform.position.y, gameObject.transform.position.z);

            float   localStamp_Flt          =   0.05f - (Mathf.Clamp01(gameObject.transform.position.y) / 25.0f);
            shadow_Gob.transform.localScale =   new Vector3(localStamp_Flt, localStamp_Flt, localStamp_Flt);

            localStamp_Flt  =   0.5f - (Mathf.Clamp01(gameObject.transform.position.y) * 0.5f);//print(shadow_Gob.GetComponent<SpriteRenderer>().color.a);
            shadow_Gob.GetComponent<SpriteRenderer>().color =   new Color(0, 0, 0, localStamp_Flt);

            // Updates ball position on the field (if needed)
            if (_shouldFollowAnchor && null != _anchor)
			{
				this.transform.position	=	Vector3.MoveTowards(
					this.transform.position,
					_anchor.Movements.FootTransform.position + new Vector3(0f, 0.075f, 0f),
					_timingConfig.BallSpeed * Time.deltaTime
				);
			}
		}
		
		#endregion
	}
}
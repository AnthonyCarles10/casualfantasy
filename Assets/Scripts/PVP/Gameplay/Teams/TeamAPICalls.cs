﻿using Sportfaction.CasualFantasy.Events;
using Sportfaction.CasualFantasy.References;
using Sportfaction.CasualFantasy.Services;

using BestHTTP;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using System;

namespace Sportfaction.CasualFantasy.PVP.Gameplay.Teams
{
	/// <summary>
	/// Class handling every team related API calls
	/// </summary>
    /// @author Madeleine Tranier
	public sealed class TeamAPICalls : MonoBehaviour
	{
        #region Serialized Fields

        [SerializeField, ReadOnly, Tooltip("Id of the manager")]
        private string          _managerId      =   null;
		[SerializeField, ReadOnly, Tooltip("Reference to the team JSON")]
        private StringReference _teamJSONRef    =	null;

		[SerializeField, Tooltip("Event to invoke when team infos have been retrieved")]
		private GameEvent   _onGetTeam  =   null;

        #endregion

        #region Private Fields

        private string  _curTeamCompo   =   null;	// Current team composition
        private int     _teamId         =   -1;

        private string  _getTeamIdByManagerIdURL    =   "";
        private string  _getSquadPlayersByTeamId    =   "";

        #endregion

        public void Synchronize(
            StringReference pTeamJSONRef,
            GameEvent _onGetPVPTeam,
            string pManagerId,
            string pComposition)
        {
            _teamJSONRef    =   pTeamJSONRef;
            _managerId      =   pManagerId;
            _curTeamCompo   =   pComposition;

            _onGetTeam      =   _onGetPVPTeam;

            GetTeamIdByManagerId();
        }

        #region Public Methods

        /// <summary>
        /// Synchronize composition of the team by calling API
        /// </summary>
        public	void	GetTeamIdByManagerId()
		{
			if (null != Api.Instance)
			{
                _getTeamIdByManagerIdURL =	Api.Instance.GenerateUrl(ApiConstants.GetTeam + "/" + _managerId);

				Assert.IsFalse(string.IsNullOrEmpty(_getTeamIdByManagerIdURL), "[TeamAPICalls] SynchronizeTeamComposition(), lUrl is null or empty.");

				SendRequest(_getTeamIdByManagerIdURL, Api.Methods.GET, true);
			}
		}

		/// <summary>
		/// Synchronize team players by calling API
		/// </summary>
		public	void    GetSquadPlayersByTeamId()
		{
			if (null != Api.Instance)
			{
                Dictionary<string, string> lQueryParams = new Dictionary<string, string>()
                {
                    { "composition", _curTeamCompo }
                };

                _getSquadPlayersByTeamId =	Api.Instance.GenerateUrl(ApiConstants.GetSquadPlayersURL + "/" + _teamId.ToString(), lQueryParams);

				Assert.IsFalse(string.IsNullOrEmpty(_getSquadPlayersByTeamId), "[TeamAPICalls] SynchronizeTeamPlayers(), lUrl is null or empty.");

				SendRequest(_getSquadPlayersByTeamId, Api.Methods.GET, true);
			}
		}

		#endregion

		#region Private Methods

		/// <summary>
		/// Monobehaviour method used to link OnRequestSuccess
		/// </summary>
		private	void	OnEnable()
		{
			if (null != Api.Instance)
			{
				Api.Instance.OnSuccess	+=	OnRequestSuccess;
			}
		}

		/// <summary>
		/// Monobehaviour method used to unlink OnRequestSuccess
		/// </summary>
		private	void	OnDisable()
		{
			if (null != Api.Instance)
			{
				Api.Instance.OnSuccess	-=	OnRequestSuccess;
			}
		}

		/// <summary>
		/// Send a request to API
		/// </summary>
		/// <param name="pUrl">Url to call</param>
		/// <param name="pMethod">Method to use for API call</param>
		/// <param name="pAuthenticationRequired">Does API require authentication token ?</param>
		private	void	SendRequest(string pUrl, Api.Methods pMethod, bool pAuthenticationRequired)
		{
			HTTPRequest	lRequest	=	Api.Instance.SetUpRequest(pUrl, pMethod, pAuthenticationRequired);

			Assert.IsNotNull(lRequest, "[TeamAPICalls] SendRequest(), lRequest is null.");

            lRequest.Timeout = TimeSpan.FromSeconds(5);

            StartCoroutine(Api.Instance.SendRequest(lRequest));
		}

        /// <summary>
        /// Callback invoke when request from API was done successfully
        /// </summary>
        /// <param name="pCurrentUrl">string</param>
        /// <param name="pMethod">HTTPMethods</param>
        /// <param name="pData">string</param>
        private void    OnRequestSuccess(string pCurrentUrl, HTTPMethods pMethod, string pData)
        {
            Assert.IsFalse(string.IsNullOrEmpty(pCurrentUrl), "[TeamAPICalls] OnRequestSuccess(), pCurrentUrl is null.");
			Assert.IsFalse(string.IsNullOrEmpty(pData), "[TeamAPICalls] OnRequestSuccess(), pData is null.");

			
            if (pCurrentUrl.Equals(_getTeamIdByManagerIdURL))
			{
                TransformJsonToTeamId(pData);
			}
            else if (pCurrentUrl.Equals(_getSquadPlayersByTeamId))
            {
                SetJsonToTeamRef(pData);
            }
            //else if (pCurrentUrl.Contains(ApiConstants.GetCompositionURL + "/" + _curTeamCompo))
            //{
            //             Debug.Log("step3");
            //             HandleCompositionRequest(pData);
            //}
        }

		/// <summary>
		/// Handle squad players requested
		/// </summary>
		/// <param name="pData">Data received</param>
		private	void	SetJsonToTeamRef(string pData)
		{
			Assert.IsNotNull(_teamJSONRef, "[TeamAPICalls] HandleSquadPlayersRequest(), _teamAJSONRef is null.");


            _teamJSONRef.CurrentValue   =   pData;
			//_teamJSONRef.CurrentValue = "{\"players\":[{\"player\":{\"id\":172,\"team_id\":33,\"player_id\":172,\"value\":\"4.2\",\"is_onsale\":0,\"position\":\"D\",\"squad_position\":\"DG\",\"team_name\":\"Man City\",\"official_team_opta_id\":null,\"official_team_id\":33,\"official_team_country_id\":548,\"opta_id\":\"162509\",\"old_opta_id\":null,\"sdapi_id\":\"9q1pgu6p4wcn9qg4idxsipql1\",\"name\":\"Benjamin MENDY\",\"name_short\":\"MENDY\",\"old_value\":\"-1.0\",\"status\":0,\"injured\":null,\"date_injury\":null,\"date_start_suspended\":null,\"date_end_suspended\":null,\"pseudo_twitter\":\"\",\"birth_date\":\"1994-07-17 00:00:00\",\"weight\":85,\"height\":185,\"number\":22,\"join_date\":null,\"country\":\"France\",\"control_skill\":\"54.0\",\"pass_skill\":\"47.0\",\"long_pass_skill\":\"82.0\",\"shot_skill\":\"38.0\",\"dribble_skill\":\"40.0\",\"vision_skill\":\"66.0\",\"tackle_skill\":\"59.0\",\"finish_skill\":\"32.0\",\"interception_skill\":\"51.0\",\"defense_headed_skill\":\"50.0\",\"attack_headed_skill\":\"40.0\",\"close_save_skill\":\"30.0\",\"far_save_skill\":\"30.0\",\"coming_off_skill\":\"30.0\",\"strength_skill\":\"32.0\",\"percussion_skill\":\"33.0\",\"rigor_skill\":\"76.0\",\"corner_skill\":\"30.0\",\"param_skill\":\"32.0\",\"technical\":\"43.0\",\"attack\":\"33.0\",\"fitness_mental\":\"47.0\",\"goalkeeper\":\"30.0\",\"defense\":\"68.0\",\"param\":\"1.3\",\"concentration_skill\":\"30.0\",\"score\":\"42.0\",\"level\":0,\"full_position\":\"DG\",\"finish_skill_tmp\":\"0.0\",\"shot_skill_tmp\":\"0.0\",\"percussion_skill_tmp\":\"0.0\",\"interception_skill_tmp\":\"0.0\",\"defense_headed_skill_tmp\":\"0.0\",\"attack_headed_skill_tmp\":\"0.0\",\"rigor_skill_tmp\":\"0.0\",\"tackle_skill_tmp\":\"0.0\",\"close_save_skill_tmp\":\"0.0\",\"far_save_skill_tmp\":\"0.0\",\"coming_off_skill_tmp\":\"0.0\",\"strength_skill_tmp\":\"0.0\",\"vision_skill_tmp\":\"0.0\",\"pass_skill_tmp\":\"0.0\",\"long_pass_skill_tmp\":\"0.0\",\"dribble_skill_tmp\":\"0.0\",\"control_skill_tmp\":\"0.0\",\"corner_skill_tmp\":\"0.0\",\"param_skill_tmp\":\"0.0\",\"concentration_skill_tmp\":\"0.0\",\"pvp_attack\":\"71.0\",\"pvp_defense\":\"85.0\",\"next_game\":null},\"buffs\":[]},{\"player\":{\"id\":176,\"team_id\":9,\"player_id\":176,\"value\":\"4.2\",\"is_onsale\":0,\"position\":\"D\",\"squad_position\":\"DD\",\"team_name\":\"Monaco\",\"official_team_opta_id\":null,\"official_team_id\":9,\"official_team_country_id\":548,\"opta_id\":\"97835\",\"old_opta_id\":null,\"sdapi_id\":\"2liyw2r4k4gwdkdz51ohws9ud\",\"name\":\"Djibril SIDIBÉ\",\"name_short\":\"SIDIBÉ\",\"old_value\":\"-1.0\",\"status\":0,\"injured\":null,\"date_injury\":null,\"date_start_suspended\":null,\"date_end_suspended\":null,\"pseudo_twitter\":\"\",\"birth_date\":\"1992-07-29 00:00:00\",\"weight\":71,\"height\":182,\"number\":19,\"join_date\":null,\"country\":\"France\",\"control_skill\":\"57.0\",\"pass_skill\":\"45.0\",\"long_pass_skill\":\"60.0\",\"shot_skill\":\"46.0\",\"dribble_skill\":\"45.0\",\"vision_skill\":\"65.0\",\"tackle_skill\":\"63.0\",\"finish_skill\":\"39.0\",\"interception_skill\":\"57.0\",\"defense_headed_skill\":\"60.0\",\"attack_headed_skill\":\"60.0\",\"close_save_skill\":\"30.0\",\"far_save_skill\":\"30.0\",\"coming_off_skill\":\"30.0\",\"strength_skill\":\"59.0\",\"percussion_skill\":\"52.0\",\"rigor_skill\":\"56.0\",\"corner_skill\":\"30.0\",\"param_skill\":\"59.0\",\"technical\":\"40.0\",\"attack\":\"45.0\",\"fitness_mental\":\"64.0\",\"goalkeeper\":\"30.0\",\"defense\":\"58.0\",\"param\":\"4.3\",\"concentration_skill\":\"30.0\",\"score\":\"47.0\",\"level\":0,\"full_position\":\"DD\",\"finish_skill_tmp\":\"0.0\",\"shot_skill_tmp\":\"0.0\",\"percussion_skill_tmp\":\"0.0\",\"interception_skill_tmp\":\"0.0\",\"defense_headed_skill_tmp\":\"0.0\",\"attack_headed_skill_tmp\":\"0.0\",\"rigor_skill_tmp\":\"0.0\",\"tackle_skill_tmp\":\"0.0\",\"close_save_skill_tmp\":\"0.0\",\"far_save_skill_tmp\":\"0.0\",\"coming_off_skill_tmp\":\"0.0\",\"strength_skill_tmp\":\"0.0\",\"vision_skill_tmp\":\"0.0\",\"pass_skill_tmp\":\"0.0\",\"long_pass_skill_tmp\":\"0.0\",\"dribble_skill_tmp\":\"0.0\",\"control_skill_tmp\":\"0.0\",\"corner_skill_tmp\":\"0.0\",\"param_skill_tmp\":\"0.0\",\"concentration_skill_tmp\":\"0.0\",\"pvp_attack\":\"69.0\",\"pvp_defense\":\"87.0\",\"next_game\":null},\"buffs\":[]},{\"player\":{\"id\":755,\"team_id\":28,\"player_id\":755,\"value\":\"4.2\",\"is_onsale\":0,\"position\":\"M\",\"squad_position\":\"MD\",\"team_name\":\"Chelsea\",\"official_team_opta_id\":null,\"official_team_id\":28,\"official_team_country_id\":541,\"opta_id\":\"9051\",\"old_opta_id\":null,\"sdapi_id\":\"afcwu800ox2u5zx6xaiygg1qt\",\"name\":\"Willian BORGES DA SILVA\",\"name_short\":\"WILLIAN\",\"old_value\":\"-1.0\",\"status\":0,\"injured\":null,\"date_injury\":null,\"date_start_suspended\":null,\"date_end_suspended\":null,\"pseudo_twitter\":\"\",\"birth_date\":\"1988-08-09 00:00:00\",\"weight\":77,\"height\":175,\"number\":22,\"join_date\":null,\"country\":\"Brazil\",\"control_skill\":\"53.0\",\"pass_skill\":\"63.0\",\"long_pass_skill\":\"60.0\",\"shot_skill\":\"67.0\",\"dribble_skill\":\"61.0\",\"vision_skill\":\"61.0\",\"tackle_skill\":\"58.0\",\"finish_skill\":\"54.0\",\"interception_skill\":\"30.0\",\"defense_headed_skill\":\"45.0\",\"attack_headed_skill\":\"58.0\",\"close_save_skill\":\"30.0\",\"far_save_skill\":\"30.0\",\"coming_off_skill\":\"30.0\",\"strength_skill\":\"60.0\",\"percussion_skill\":\"67.0\",\"rigor_skill\":\"36.0\",\"corner_skill\":\"60.0\",\"param_skill\":\"38.0\",\"technical\":\"62.0\",\"attack\":\"61.0\",\"fitness_mental\":\"55.0\",\"goalkeeper\":\"30.0\",\"defense\":\"32.0\",\"param\":\"3.6\",\"concentration_skill\":\"30.0\",\"score\":\"49.0\",\"level\":0,\"full_position\":\"MCD\",\"finish_skill_tmp\":\"0.0\",\"shot_skill_tmp\":\"0.0\",\"percussion_skill_tmp\":\"0.0\",\"interception_skill_tmp\":\"0.0\",\"defense_headed_skill_tmp\":\"0.0\",\"attack_headed_skill_tmp\":\"0.0\",\"rigor_skill_tmp\":\"0.0\",\"tackle_skill_tmp\":\"0.0\",\"close_save_skill_tmp\":\"0.0\",\"far_save_skill_tmp\":\"0.0\",\"coming_off_skill_tmp\":\"0.0\",\"strength_skill_tmp\":\"0.0\",\"vision_skill_tmp\":\"0.0\",\"pass_skill_tmp\":\"0.0\",\"long_pass_skill_tmp\":\"0.0\",\"dribble_skill_tmp\":\"0.0\",\"control_skill_tmp\":\"0.0\",\"corner_skill_tmp\":\"0.0\",\"param_skill_tmp\":\"0.0\",\"concentration_skill_tmp\":\"0.0\",\"pvp_attack\":\"84.0\",\"pvp_defense\":\"83.0\",\"next_game\":null},\"buffs\":[]},{\"player\":{\"id\":782,\"team_id\":30,\"player_id\":782,\"value\":\"4.2\",\"is_onsale\":0,\"position\":\"A\",\"squad_position\":\"ACD\",\"team_name\":\"Man Utd\",\"official_team_opta_id\":null,\"official_team_id\":30,\"official_team_country_id\":540,\"opta_id\":\"79495\",\"old_opta_id\":null,\"sdapi_id\":\"8qgbxff7xlg0zjtyees7ljljp\",\"name\":\"Romelu LUKAKU MENAMA\",\"name_short\":\"LUKAKU\",\"old_value\":\"-1.0\",\"status\":0,\"injured\":null,\"date_injury\":null,\"date_start_suspended\":null,\"date_end_suspended\":null,\"pseudo_twitter\":\"\",\"birth_date\":\"1993-05-13 00:00:00\",\"weight\":94,\"height\":190,\"number\":9,\"join_date\":null,\"country\":\"Belgium\",\"control_skill\":\"65.0\",\"pass_skill\":\"30.0\",\"long_pass_skill\":\"53.0\",\"shot_skill\":\"63.0\",\"dribble_skill\":\"57.0\",\"vision_skill\":\"44.0\",\"tackle_skill\":\"31.0\",\"finish_skill\":\"65.0\",\"interception_skill\":\"30.0\",\"defense_headed_skill\":\"50.0\",\"attack_headed_skill\":\"68.0\",\"close_save_skill\":\"30.0\",\"far_save_skill\":\"30.0\",\"coming_off_skill\":\"30.0\",\"strength_skill\":\"46.0\",\"percussion_skill\":\"38.0\",\"rigor_skill\":\"48.0\",\"corner_skill\":\"30.0\",\"param_skill\":\"41.0\",\"technical\":\"43.0\",\"attack\":\"63.0\",\"fitness_mental\":\"40.0\",\"goalkeeper\":\"30.0\",\"defense\":\"41.0\",\"param\":\"5.6\",\"concentration_skill\":\"30.0\",\"score\":\"39.0\",\"level\":0,\"full_position\":\"AC\",\"finish_skill_tmp\":\"0.0\",\"shot_skill_tmp\":\"0.0\",\"percussion_skill_tmp\":\"0.0\",\"interception_skill_tmp\":\"0.0\",\"defense_headed_skill_tmp\":\"0.0\",\"attack_headed_skill_tmp\":\"0.0\",\"rigor_skill_tmp\":\"0.0\",\"tackle_skill_tmp\":\"0.0\",\"close_save_skill_tmp\":\"0.0\",\"far_save_skill_tmp\":\"0.0\",\"coming_off_skill_tmp\":\"0.0\",\"strength_skill_tmp\":\"0.0\",\"vision_skill_tmp\":\"0.0\",\"pass_skill_tmp\":\"0.0\",\"long_pass_skill_tmp\":\"0.0\",\"dribble_skill_tmp\":\"0.0\",\"control_skill_tmp\":\"0.0\",\"corner_skill_tmp\":\"0.0\",\"param_skill_tmp\":\"0.0\",\"concentration_skill_tmp\":\"0.0\",\"pvp_attack\":\"83.0\",\"pvp_defense\":\"64.0\",\"next_game\":null},\"buffs\":[]},{\"player\":{\"id\":810,\"team_id\":30,\"player_id\":810,\"value\":\"4.2\",\"is_onsale\":0,\"position\":\"M\",\"squad_position\":\"MCD\",\"team_name\":\"Man Utd\",\"official_team_opta_id\":null,\"official_team_id\":30,\"official_team_country_id\":540,\"opta_id\":\"13519\",\"old_opta_id\":null,\"sdapi_id\":\"351v0ovetkdkympri37ldwez9\",\"name\":\"Marouane FELLAINI-BAKKIOUI\",\"name_short\":\"FELLAINI\",\"old_value\":\"-1.0\",\"status\":0,\"injured\":null,\"date_injury\":null,\"date_start_suspended\":null,\"date_end_suspended\":null,\"pseudo_twitter\":\"\",\"birth_date\":\"1987-11-22 00:00:00\",\"weight\":85,\"height\":194,\"number\":27,\"join_date\":null,\"country\":\"Belgium\",\"control_skill\":\"65.0\",\"pass_skill\":\"63.0\",\"long_pass_skill\":\"30.0\",\"shot_skill\":\"54.0\",\"dribble_skill\":\"37.0\",\"vision_skill\":\"33.0\",\"tackle_skill\":\"34.0\",\"finish_skill\":\"60.0\",\"interception_skill\":\"30.0\",\"defense_headed_skill\":\"62.0\",\"attack_headed_skill\":\"78.0\",\"close_save_skill\":\"30.0\",\"far_save_skill\":\"30.0\",\"coming_off_skill\":\"30.0\",\"strength_skill\":\"53.0\",\"percussion_skill\":\"45.0\",\"rigor_skill\":\"80.0\",\"corner_skill\":\"30.0\",\"param_skill\":\"33.0\",\"technical\":\"47.0\",\"attack\":\"61.0\",\"fitness_mental\":\"33.0\",\"goalkeeper\":\"30.0\",\"defense\":\"69.0\",\"param\":\"2.0\",\"concentration_skill\":\"30.0\",\"score\":\"53.0\",\"level\":0,\"full_position\":\"MCD\",\"finish_skill_tmp\":\"0.0\",\"shot_skill_tmp\":\"0.0\",\"percussion_skill_tmp\":\"0.0\",\"interception_skill_tmp\":\"0.0\",\"defense_headed_skill_tmp\":\"0.0\",\"attack_headed_skill_tmp\":\"0.0\",\"rigor_skill_tmp\":\"0.0\",\"tackle_skill_tmp\":\"0.0\",\"close_save_skill_tmp\":\"0.0\",\"far_save_skill_tmp\":\"0.0\",\"coming_off_skill_tmp\":\"0.0\",\"strength_skill_tmp\":\"0.0\",\"vision_skill_tmp\":\"0.0\",\"pass_skill_tmp\":\"0.0\",\"long_pass_skill_tmp\":\"0.0\",\"dribble_skill_tmp\":\"0.0\",\"control_skill_tmp\":\"0.0\",\"corner_skill_tmp\":\"0.0\",\"param_skill_tmp\":\"0.0\",\"concentration_skill_tmp\":\"0.0\",\"pvp_attack\":\"72.0\",\"pvp_defense\":\"69.0\",\"next_game\":null},\"buffs\":[]},{\"player\":{\"id\":1107,\"team_id\":40,\"player_id\":1107,\"value\":\"4.2\",\"is_onsale\":0,\"position\":\"G\",\"squad_position\":\"G\",\"team_name\":\"Tottenham\",\"official_team_opta_id\":null,\"official_team_id\":40,\"official_team_country_id\":548,\"opta_id\":\"1255\",\"old_opta_id\":null,\"sdapi_id\":\"ombvyo6p3p5khkn91vubvpat\",\"name\":\"Hugo LLORIS\",\"name_short\":\"LLORIS\",\"old_value\":\"-1.0\",\"status\":0,\"injured\":null,\"date_injury\":null,\"date_start_suspended\":null,\"date_end_suspended\":null,\"pseudo_twitter\":\"\",\"birth_date\":\"1986-12-26 00:00:00\",\"weight\":82,\"height\":188,\"number\":1,\"join_date\":null,\"country\":\"France\",\"control_skill\":\"80.0\",\"pass_skill\":\"47.0\",\"long_pass_skill\":\"30.0\",\"shot_skill\":\"38.0\",\"dribble_skill\":\"31.0\",\"vision_skill\":\"31.0\",\"tackle_skill\":\"33.0\",\"finish_skill\":\"32.0\",\"interception_skill\":\"30.0\",\"defense_headed_skill\":\"53.0\",\"attack_headed_skill\":\"40.0\",\"close_save_skill\":\"81.0\",\"far_save_skill\":\"81.0\",\"coming_off_skill\":\"85.0\",\"strength_skill\":\"58.0\",\"percussion_skill\":\"33.0\",\"rigor_skill\":\"35.0\",\"corner_skill\":\"30.0\",\"param_skill\":\"78.0\",\"technical\":\"43.0\",\"attack\":\"33.0\",\"fitness_mental\":\"54.0\",\"goalkeeper\":\"84.0\",\"defense\":\"30.0\",\"param\":\"5.9\",\"concentration_skill\":\"79.0\",\"score\":\"48.0\",\"level\":0,\"full_position\":\"G\",\"finish_skill_tmp\":\"0.0\",\"shot_skill_tmp\":\"0.0\",\"percussion_skill_tmp\":\"0.0\",\"interception_skill_tmp\":\"0.0\",\"defense_headed_skill_tmp\":\"0.0\",\"attack_headed_skill_tmp\":\"0.0\",\"rigor_skill_tmp\":\"0.0\",\"tackle_skill_tmp\":\"0.0\",\"close_save_skill_tmp\":\"0.0\",\"far_save_skill_tmp\":\"0.0\",\"coming_off_skill_tmp\":\"0.0\",\"strength_skill_tmp\":\"0.0\",\"vision_skill_tmp\":\"0.0\",\"pass_skill_tmp\":\"0.0\",\"long_pass_skill_tmp\":\"0.0\",\"dribble_skill_tmp\":\"0.0\",\"control_skill_tmp\":\"0.0\",\"corner_skill_tmp\":\"0.0\",\"param_skill_tmp\":\"0.0\",\"concentration_skill_tmp\":\"0.0\",\"pvp_attack\":\"0.0\",\"pvp_defense\":\"92.0\",\"next_game\":null},\"buffs\":[]},{\"player\":{\"id\":1202,\"team_id\":24,\"player_id\":1202,\"value\":\"4.2\",\"is_onsale\":0,\"position\":\"D\",\"squad_position\":\"DCG\",\"team_name\":\"Leicester\",\"official_team_opta_id\":null,\"official_team_id\":24,\"official_team_country_id\":547,\"opta_id\":\"182047\",\"old_opta_id\":null,\"sdapi_id\":\"1vz038uyzmq8saskeeo0qhm8l\",\"name\":\"Harry MAGUIRE\",\"name_short\":\"MAGUIRE\",\"old_value\":\"-1.0\",\"status\":0,\"injured\":null,\"date_injury\":null,\"date_start_suspended\":null,\"date_end_suspended\":null,\"pseudo_twitter\":\"\",\"birth_date\":\"1993-03-05 00:00:00\",\"weight\":100,\"height\":194,\"number\":15,\"join_date\":null,\"country\":\"England\",\"control_skill\":\"60.0\",\"pass_skill\":\"39.0\",\"long_pass_skill\":\"35.0\",\"shot_skill\":\"40.0\",\"dribble_skill\":\"58.0\",\"vision_skill\":\"49.0\",\"tackle_skill\":\"67.0\",\"finish_skill\":\"36.0\",\"interception_skill\":\"79.0\",\"defense_headed_skill\":\"73.0\",\"attack_headed_skill\":\"54.0\",\"close_save_skill\":\"30.0\",\"far_save_skill\":\"30.0\",\"coming_off_skill\":\"30.0\",\"strength_skill\":\"75.0\",\"percussion_skill\":\"42.0\",\"rigor_skill\":\"52.0\",\"corner_skill\":\"30.0\",\"param_skill\":\"67.0\",\"technical\":\"46.0\",\"attack\":\"39.0\",\"fitness_mental\":\"64.0\",\"goalkeeper\":\"30.0\",\"defense\":\"65.0\",\"param\":\"4.6\",\"concentration_skill\":\"30.0\",\"score\":\"52.0\",\"level\":0,\"full_position\":\"DCG\",\"finish_skill_tmp\":\"0.0\",\"shot_skill_tmp\":\"0.0\",\"percussion_skill_tmp\":\"0.0\",\"interception_skill_tmp\":\"0.0\",\"defense_headed_skill_tmp\":\"0.0\",\"attack_headed_skill_tmp\":\"0.0\",\"rigor_skill_tmp\":\"0.0\",\"tackle_skill_tmp\":\"0.0\",\"close_save_skill_tmp\":\"0.0\",\"far_save_skill_tmp\":\"0.0\",\"coming_off_skill_tmp\":\"0.0\",\"strength_skill_tmp\":\"0.0\",\"vision_skill_tmp\":\"0.0\",\"pass_skill_tmp\":\"0.0\",\"long_pass_skill_tmp\":\"0.0\",\"dribble_skill_tmp\":\"0.0\",\"control_skill_tmp\":\"0.0\",\"corner_skill_tmp\":\"0.0\",\"param_skill_tmp\":\"0.0\",\"concentration_skill_tmp\":\"0.0\",\"pvp_attack\":\"67.0\",\"pvp_defense\":\"88.0\",\"next_game\":null},\"buffs\":[]},{\"player\":{\"id\":7431,\"team_id\":261,\"player_id\":7431,\"value\":\"4.2\",\"is_onsale\":0,\"position\":\"A\",\"squad_position\":\"ACG\",\"team_name\":\"Juventus\",\"official_team_opta_id\":null,\"official_team_id\":261,\"official_team_country_id\":538,\"opta_id\":\"5274\",\"old_opta_id\":null,\"sdapi_id\":\"d3w4np40y5dshxm58rr67ld5h\",\"name\":\"Gonzalo Gerardo HIGUAÍN\",\"name_short\":\"HIGUAIN\",\"old_value\":\"-1.0\",\"status\":0,\"injured\":null,\"date_injury\":null,\"date_start_suspended\":null,\"date_end_suspended\":null,\"pseudo_twitter\":\"\",\"birth_date\":\"1987-12-10 00:00:00\",\"weight\":89,\"height\":186,\"number\":9,\"join_date\":null,\"country\":\"Argentina\",\"control_skill\":\"64.0\",\"pass_skill\":\"65.0\",\"long_pass_skill\":\"34.0\",\"shot_skill\":\"61.0\",\"dribble_skill\":\"60.0\",\"vision_skill\":\"40.0\",\"tackle_skill\":\"38.0\",\"finish_skill\":\"64.0\",\"interception_skill\":\"30.0\",\"defense_headed_skill\":\"46.0\",\"attack_headed_skill\":\"61.0\",\"close_save_skill\":\"30.0\",\"far_save_skill\":\"30.0\",\"coming_off_skill\":\"30.0\",\"strength_skill\":\"45.0\",\"percussion_skill\":\"58.0\",\"rigor_skill\":\"48.0\",\"corner_skill\":\"30.0\",\"param_skill\":\"42.0\",\"technical\":\"61.0\",\"attack\":\"63.0\",\"fitness_mental\":\"37.0\",\"goalkeeper\":\"30.0\",\"defense\":\"41.0\",\"param\":\"6.0\",\"concentration_skill\":\"30.0\",\"score\":\"48.0\",\"level\":0,\"full_position\":\"AC\",\"finish_skill_tmp\":\"0.0\",\"shot_skill_tmp\":\"0.0\",\"percussion_skill_tmp\":\"0.0\",\"interception_skill_tmp\":\"0.0\",\"defense_headed_skill_tmp\":\"0.0\",\"attack_headed_skill_tmp\":\"0.0\",\"rigor_skill_tmp\":\"0.0\",\"tackle_skill_tmp\":\"0.0\",\"close_save_skill_tmp\":\"0.0\",\"far_save_skill_tmp\":\"0.0\",\"coming_off_skill_tmp\":\"0.0\",\"strength_skill_tmp\":\"0.0\",\"vision_skill_tmp\":\"0.0\",\"pass_skill_tmp\":\"0.0\",\"long_pass_skill_tmp\":\"0.0\",\"dribble_skill_tmp\":\"0.0\",\"control_skill_tmp\":\"0.0\",\"corner_skill_tmp\":\"0.0\",\"param_skill_tmp\":\"0.0\",\"concentration_skill_tmp\":\"0.0\",\"pvp_attack\":\"80.0\",\"pvp_defense\":\"65.0\",\"next_game\":null},\"buffs\":[]},{\"player\":{\"id\":9614,\"team_id\":338,\"player_id\":9614,\"value\":\"4.2\",\"is_onsale\":0,\"position\":\"M\",\"squad_position\":\"MG\",\"team_name\":\"Leipzig\",\"official_team_opta_id\":null,\"official_team_id\":338,\"official_team_country_id\":566,\"opta_id\":\"78541\",\"old_opta_id\":null,\"sdapi_id\":\"ckox9w95duwhmt72xxk7lt2vp\",\"name\":\"Emil FORSBERG\",\"name_short\":\"FORSBERG\",\"old_value\":\"-1.0\",\"status\":0,\"injured\":null,\"date_injury\":null,\"date_start_suspended\":null,\"date_end_suspended\":null,\"pseudo_twitter\":\"\",\"birth_date\":\"1991-10-23 00:00:00\",\"weight\":78,\"height\":179,\"number\":10,\"join_date\":null,\"country\":\"Sweden\",\"control_skill\":\"-7.8\",\"pass_skill\":\"2.0\",\"long_pass_skill\":\"0.7\",\"shot_skill\":\"1.2\",\"dribble_skill\":\"5.7\",\"vision_skill\":\"5.4\",\"tackle_skill\":\"1.0\",\"finish_skill\":\"2.2\",\"interception_skill\":\"1.5\",\"defense_headed_skill\":\"-0.4\",\"attack_headed_skill\":\"0.0\",\"close_save_skill\":\"0.0\",\"far_save_skill\":\"0.0\",\"coming_off_skill\":\"0.0\",\"strength_skill\":\"0.4\",\"percussion_skill\":\"1.7\",\"rigor_skill\":\"-0.6\",\"corner_skill\":\"3.7\",\"param_skill\":\"4.8\",\"technical\":\"0.0\",\"attack\":\"1.0\",\"fitness_mental\":\"2.0\",\"goalkeeper\":\"0.0\",\"defense\":\"0.0\",\"param\":\"4.8\",\"concentration_skill\":\"0.0\",\"score\":\"1.0\",\"level\":0,\"full_position\":\"MCG\",\"finish_skill_tmp\":\"0.0\",\"shot_skill_tmp\":\"0.0\",\"percussion_skill_tmp\":\"0.0\",\"interception_skill_tmp\":\"0.0\",\"defense_headed_skill_tmp\":\"0.0\",\"attack_headed_skill_tmp\":\"0.0\",\"rigor_skill_tmp\":\"0.0\",\"tackle_skill_tmp\":\"0.0\",\"close_save_skill_tmp\":\"0.0\",\"far_save_skill_tmp\":\"0.0\",\"coming_off_skill_tmp\":\"0.0\",\"strength_skill_tmp\":\"0.0\",\"vision_skill_tmp\":\"0.0\",\"pass_skill_tmp\":\"0.0\",\"long_pass_skill_tmp\":\"0.0\",\"dribble_skill_tmp\":\"0.0\",\"control_skill_tmp\":\"0.0\",\"corner_skill_tmp\":\"0.0\",\"param_skill_tmp\":\"0.0\",\"concentration_skill_tmp\":\"0.0\",\"pvp_attack\":\"74.0\",\"pvp_defense\":\"83.0\",\"next_game\":null},\"buffs\":[]},{\"player\":{\"id\":10511,\"team_id\":364,\"player_id\":10511,\"value\":\"4.2\",\"is_onsale\":0,\"position\":\"D\",\"squad_position\":\"DCD\",\"team_name\":\"Espanyol\",\"official_team_opta_id\":null,\"official_team_id\":364,\"official_team_country_id\":543,\"opta_id\":\"42671\",\"old_opta_id\":null,\"sdapi_id\":\"a9yimyxegu9sx46hqvwswjs2d\",\"name\":\"Óscar Esau DUARTE GAITÁN\",\"name_short\":\"DUARTE GAIT√ÅN\",\"old_value\":\"-1.0\",\"status\":0,\"injured\":null,\"date_injury\":null,\"date_start_suspended\":null,\"date_end_suspended\":null,\"pseudo_twitter\":\"\",\"birth_date\":\"1989-06-03 00:00:00\",\"weight\":81,\"height\":186,\"number\":6,\"join_date\":null,\"country\":\"Costa Rica\",\"control_skill\":\"-2.3\",\"pass_skill\":\"1.0\",\"long_pass_skill\":\"0.2\",\"shot_skill\":\"-0.2\",\"dribble_skill\":\"0.3\",\"vision_skill\":\"0.6\",\"tackle_skill\":\"1.1\",\"finish_skill\":\"0.0\",\"interception_skill\":\"2.5\",\"defense_headed_skill\":\"2.7\",\"attack_headed_skill\":\"-0.1\",\"close_save_skill\":\"0.0\",\"far_save_skill\":\"0.0\",\"coming_off_skill\":\"0.0\",\"strength_skill\":\"2.0\",\"percussion_skill\":\"0.0\",\"rigor_skill\":\"5.0\",\"corner_skill\":\"0.0\",\"param_skill\":\"5.0\",\"technical\":\"-1.0\",\"attack\":\"-1.0\",\"fitness_mental\":\"1.0\",\"goalkeeper\":\"0.0\",\"defense\":\"2.0\",\"param\":\"5.0\",\"concentration_skill\":\"0.0\",\"score\":\"1.0\",\"level\":0,\"full_position\":\"DCG\",\"finish_skill_tmp\":\"0.0\",\"shot_skill_tmp\":\"0.0\",\"percussion_skill_tmp\":\"0.0\",\"interception_skill_tmp\":\"0.0\",\"defense_headed_skill_tmp\":\"0.0\",\"attack_headed_skill_tmp\":\"0.0\",\"rigor_skill_tmp\":\"0.0\",\"tackle_skill_tmp\":\"0.0\",\"close_save_skill_tmp\":\"0.0\",\"far_save_skill_tmp\":\"0.0\",\"coming_off_skill_tmp\":\"0.0\",\"strength_skill_tmp\":\"0.0\",\"vision_skill_tmp\":\"0.0\",\"pass_skill_tmp\":\"0.0\",\"long_pass_skill_tmp\":\"0.0\",\"dribble_skill_tmp\":\"0.0\",\"control_skill_tmp\":\"0.0\",\"corner_skill_tmp\":\"0.0\",\"param_skill_tmp\":\"0.0\",\"concentration_skill_tmp\":\"0.0\",\"pvp_attack\":\"58.0\",\"pvp_defense\":\"80.0\",\"next_game\":null},\"buffs\":[]},{\"player\":{\"id\":11255,\"team_id\":379,\"player_id\":11255,\"value\":\"-1.0\",\"is_onsale\":0,\"position\":\"M\",\"squad_position\":\"MCG\",\"team_name\":\"Huddersfield\",\"official_team_opta_id\":null,\"official_team_id\":379,\"official_team_country_id\":539,\"opta_id\":\"97581\",\"old_opta_id\":null,\"sdapi_id\":\"axicg3mdtrur97rjz423pl8t1\",\"name\":\"Aaron MOOY\",\"name_short\":\"MOOY\",\"old_value\":\"-1.0\",\"status\":0,\"injured\":null,\"date_injury\":null,\"date_start_suspended\":null,\"date_end_suspended\":null,\"pseudo_twitter\":\"\",\"birth_date\":\"1990-09-15 00:00:00\",\"weight\":68,\"height\":174,\"number\":10,\"join_date\":null,\"country\":\"Australia\",\"control_skill\":\"-8.7\",\"pass_skill\":\"2.0\",\"long_pass_skill\":\"1.2\",\"shot_skill\":\"0.3\",\"dribble_skill\":\"1.3\",\"vision_skill\":\"5.5\",\"tackle_skill\":\"1.0\",\"finish_skill\":\"2.4\",\"interception_skill\":\"2.0\",\"defense_headed_skill\":\"-0.2\",\"attack_headed_skill\":\"0.0\",\"close_save_skill\":\"0.0\",\"far_save_skill\":\"0.0\",\"coming_off_skill\":\"0.0\",\"strength_skill\":\"2.3\",\"percussion_skill\":\"1.0\",\"rigor_skill\":\"-0.9\",\"corner_skill\":\"2.2\",\"param_skill\":\"4.9\",\"technical\":\"-1.0\",\"attack\":\"0.0\",\"fitness_mental\":\"3.0\",\"goalkeeper\":\"0.0\",\"defense\":\"0.0\",\"param\":\"4.9\",\"concentration_skill\":\"0.0\",\"score\":\"1.0\",\"level\":0,\"full_position\":\"MC\",\"finish_skill_tmp\":\"0.0\",\"shot_skill_tmp\":\"0.0\",\"percussion_skill_tmp\":\"0.0\",\"interception_skill_tmp\":\"0.0\",\"defense_headed_skill_tmp\":\"0.0\",\"attack_headed_skill_tmp\":\"0.0\",\"rigor_skill_tmp\":\"0.0\",\"tackle_skill_tmp\":\"0.0\",\"close_save_skill_tmp\":\"0.0\",\"far_save_skill_tmp\":\"0.0\",\"coming_off_skill_tmp\":\"0.0\",\"strength_skill_tmp\":\"0.0\",\"vision_skill_tmp\":\"0.0\",\"pass_skill_tmp\":\"0.0\",\"long_pass_skill_tmp\":\"0.0\",\"dribble_skill_tmp\":\"0.0\",\"control_skill_tmp\":\"0.0\",\"corner_skill_tmp\":\"0.0\",\"param_skill_tmp\":\"0.0\",\"concentration_skill_tmp\":\"0.0\",\"pvp_attack\":\"76.0\",\"pvp_defense\":\"86.0\",\"next_game\":null},\"buffs\":[]}],\"challenges\":[{\"name\":\"corner_ball_striking\",\"skills\":[{\"name\":\"finish_skill\"},{\"name\":\"shot_skill\"}]},{\"name\":\"interception\",\"skills\":[{\"name\":\"interception_skill\"}]},{\"name\":\"chance_gk_save\",\"skills\":[{\"name\":\"close_save_skill\"},{\"name\":\"rigor_skill\"}]},{\"name\":\"far_save\",\"skills\":[{\"name\":\"far_save_skill\"},{\"name\":\"rigor_skill\"}]},{\"name\":\"corner_defense\",\"skills\":[{\"name\":\"defense_headed_skill\"},{\"name\":\"rigor_skill\"},{\"name\":\"interception_skill\"},{\"name\":\"strength_skill\"}]},{\"name\":\"midrange_pass\",\"skills\":[{\"name\":\"pass_skill\"},{\"name\":\"long_pass_skill\"}]},{\"name\":\"covering\",\"skills\":[{\"name\":\"strength_skill\"},{\"name\":\"interception_skill\"}]},{\"name\":\"provoking\",\"skills\":[{\"name\":\"control_skill\"},{\"name\":\"dribble_skill\"}]},{\"name\":\"corner_gk_comingoff\",\"skills\":[{\"name\":\"coming_off_skill\"}]},{\"name\":\"circuling\",\"skills\":[{\"name\":\"pass_skill\"},{\"name\":\"long_pass_skill\"}]},{\"name\":\"chance_ball_striking\",\"skills\":[{\"name\":\"finish_skill\"},{\"name\":\"shot_skill\"}]},{\"name\":\"long_pass\",\"skills\":[{\"name\":\"long_pass_skill\"}]},{\"name\":\"corner_kick\",\"skills\":[{\"name\":\"corner_skill\"},{\"name\":\"long_pass_skill\"}]},{\"name\":\"target_man_play\",\"skills\":[{\"name\":\"strength_skill\"},{\"name\":\"control_skill\"},{\"name\":\"pass_skill\"}]},{\"name\":\"chance_last_pass_quality\",\"skills\":[{\"name\":\"vision_skill\"},{\"name\":\"pass_skill\"}]},{\"name\":\"flank_play\",\"skills\":[{\"name\":\"percussion_skill\"},{\"name\":\"dribble_skill\"},{\"name\":\"control_skill\"}]},{\"name\":\"call_for_ball\",\"skills\":[{\"name\":\"control_skill\"},{\"name\":\"percussion_skill\"}]},{\"name\":\"long_shot\",\"skills\":[{\"name\":\"shot_skill\"},{\"name\":\"finish_skill\"}]},{\"name\":\"tackle\",\"skills\":[{\"name\":\"tackle_skill\"}]},{\"name\":\"distribution\",\"skills\":[{\"name\":\"vision_skill\"},{\"name\":\"pass_skill\"},{\"name\":\"long_pass_skill\"}]},{\"name\":\"cut_trajectories\",\"skills\":[{\"name\":\"rigor_skill\"},{\"name\":\"interception_skill\"},{\"name\":\"defense_headed_skill\"}]},{\"name\":\"short_pass\",\"skills\":[{\"name\":\"pass_skill\"}]},{\"name\":\"defensive_challenge\",\"skills\":[{\"name\":\"strength_skill\"},{\"name\":\"tackle_skill\"},{\"name\":\"interception_skill\"},{\"name\":\"defense_headed_skill\"}]},{\"name\":\"corner_attack\",\"skills\":[{\"name\":\"attack_headed_skill\"},{\"name\":\"control_skill\"},{\"name\":\"strength_skill\"}]},{\"name\":\"technical_challenge\",\"skills\":[{\"name\":\"dribble_skill\"},{\"name\":\"control_skill\"}]},{\"name\":\"combination\",\"skills\":[{\"name\":\"vision_skill\"},{\"name\":\"pass_skill\"}]},{\"name\":\"dribble\",\"skills\":[{\"name\":\"dribble_skill\"}]},{\"name\":\"marking\",\"skills\":[{\"name\":\"rigor_skill\"},{\"name\":\"interception_skill\"}]},{\"name\":\"corner_save\",\"skills\":[{\"name\":\"close_save_skill\"},{\"name\":\"rigor_skill\"}]}]}";
            _onGetTeam.Invoke();
		}

		/// <summary>
		/// Handle team informations requested
		/// </summary>
		/// <param name="pData">Data received</param>
		private void    TransformJsonToTeamId(string pData)
		{
			JToken	lToken = JToken.Parse(pData);

			Assert.IsNotNull(lToken, "[TeamAPICalls] HandleTeamRequest(), lToken is null.");


            _teamId = lToken.Value<int>("id");
            // SynchronizeSquadPlayers(); // GET squads by teamId
            // _curTeamCompo	=	lToken.Value<string>("composition_name");

            Assert.IsFalse(string.IsNullOrEmpty(_curTeamCompo), "[TeamAPICalls] HandleTeamRequest(), _curTeamCompo is null.");

            GetSquadPlayersByTeamId();

   //         if (null != Api.Instance)
			//{
			//	string	lUrl	=	Api.Instance.GenerateUrl(ApiConstants.GetCompositionURL + "/" + _curTeamCompo);

			//	Assert.IsFalse(string.IsNullOrEmpty(lUrl), "[TeamAPICalls] HandleTeamRequest(), lUrl is null.");

			//	SendRequest(lUrl, Api.Methods.GET, true);
			//}
		}

		#endregion
	}
}
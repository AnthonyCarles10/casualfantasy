﻿using Sportfaction.CasualFantasy.Manager.Profile;
using Sportfaction.CasualFantasy.PVP.Gameplay.Matchs;

using SportFaction.CasualFootballEngine.TeamService.Enum;

using Photon.Pun;
using UnityEngine;

namespace Sportfaction.CasualFantasy.PVP.Gameplay.Teams
{
    /// <summary>
    /// Class handling team infos display
    /// </summary>
    /// @author Madeleine Tranier
    public class TeamUI : MonoBehaviour
    {
		#region Fields

		[SerializeField, Tooltip("Is Manager One ?")]
		private	bool		_isManagerOne	=	true;
        [SerializeField, ReadOnly, Tooltip("Is Team A ?")]
        private	eTeam    	_team    		=   eTeam.TEAM_A;

		[Header("UI Elements")]
        [SerializeField, Tooltip("Text for team name")]
        private TMPro.TMP_Text	_teamNameTxt    =   null;
		[SerializeField, Tooltip("Team score text")]
		private	TMPro.TMP_Text  _teamScoreTxt   =	null;

        [Header("Configuration")]
        [SerializeField, Tooltip("Scriptable object contains infos about match UI")]
        private PVPData _pvpData    =   null;

        #endregion

		#region Public Methods

		#region Game Event Listeners

        /// <summary>
        /// Event Listener > OnTeamAScoredGoal | OnTeamBScoredGoal
        /// </summary>
        public  void	OnGoalScored()
		{
			float	lScoreA =   MatchEngine.Instance.Engine.ScoreMain.TeamAScore;
			float	lScoreB =   MatchEngine.Instance.Engine.ScoreMain.TeamBScore;

			_teamScoreTxt.text	=	(eTeam.TEAM_A == _team) ? lScoreA.ToString() : lScoreB.ToString();
		}
		
		public	void	OnFullySynchronized()
		{
			if (null != MatchEngine.Instance)
			{
				float	lScoreA =   MatchEngine.Instance.Engine.ScoreMain.TeamAScore;
				float	lScoreB =   MatchEngine.Instance.Engine.ScoreMain.TeamBScore;
			
				_teamScoreTxt.text	=	(eTeam.TEAM_A == _team) ? lScoreA.ToString() : lScoreB.ToString();
			}
		}

        #endregion

        #endregion

        #region Private Methods

        private void	Start()
		{
            // Initializes team name
            if (null != UserData.Instance)
            {
                if (_isManagerOne)
                {
                    _team               =   UserData.Instance.PVPTeam;
                    _teamNameTxt.text   =   UserData.Instance.Profile.Pseudo;
                }
                else
                {
                    _team   =   (eTeam.TEAM_B == UserData.Instance.PVPTeam) ? eTeam.TEAM_A : eTeam.TEAM_B;
                    _teamNameTxt.text = (PhotonNetwork.IsConnected && PhotonNetwork.InRoom)
                                            ? PhotonNetwork.PlayerListOthers[0].NickName
                                            : _pvpData.OpponentName;
                }
            }
            else
            {
                _team   =   _isManagerOne ? eTeam.TEAM_A : eTeam.TEAM_B;
            }

            this.name   =   _team + "_Grp";
		}

        #endregion
    }
}

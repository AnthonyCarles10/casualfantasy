﻿namespace Sportfaction.CasualFantasy.PVP.Gameplay.History
{
	/// <summary>
    /// Enum containing every type of history event
	/// </summary>
	public enum eHistoryEventType
	{
		NONE,

		GOAL,
		RED_CARD,
		YELLOW_CARD,

		COUNT,
	}
}
﻿using Sportfaction.CasualFantasy.References;
using Sportfaction.CasualFantasy.References.CustomClass;

using Photon.Pun;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

namespace Sportfaction.CasualFantasy.PVP.Gameplay.History
{
	/// <summary>
	/// Class handling every history event in a match
	/// </summary>
	[RequireComponent(typeof(PhotonView))]
	public sealed class HistoryManager : Singleton<HistoryManager>
	{
		#region Fields

		[SerializeField, Tooltip("Ref to every event in history")]
		private PVPHistoryEventsReference	_historyEventsRef		=	null; //!< Ref to every event in history
		[SerializeField, Tooltip("Ref to the timer used in the first period")]
		private	IntReference				_firstPeriodTimerRef	=	null; //!< Ref to first period timer
		[SerializeField, Tooltip("Ref to the timer used in the second period")]
		private IntReference				_secondPeriodTimerRef	=	null; //!< Ref to second period timer
		[SerializeField, Tooltip("Photon view used to send RPC")]
		private	PhotonView					_photonView				=	null; //!< Photon view used to send RPC

		#endregion

		#region Public Methods

        /// <summary>
        /// Callback invoked when engine is initialized
        /// </summary>
        /*  Initializes history ref */
        public  void    OnEngineInitialized()
        {
            Assert.IsNotNull(_historyEventsRef, "[HistoryManager] Awake(), _historyEventsRef is null.");

            _historyEventsRef.CurrentValue  =   new List<HistoryEvent>();

            Assert.IsNotNull(_historyEventsRef.CurrentValue, "[HistoryManager] Awake(), _historyEventsRef.CurrentValue is null.");
        }

		/// <summary>
		/// Adds specified event
		/// </summary>
		/// <param name="pType">Type of event</param>
		/// <param name="pFromTeamA">Is event from team A ?</param>
		/// <param name="pPlayerName">Name of player causing event</param>
		public	void	AddEvent(eHistoryEventType pType, bool pFromTeamA, string pPlayerName)
		{
            int lMinute =   0;
            int lSecond =   0;
            
			if (true == PhotonNetwork.IsConnected)
			{
				if (true == PhotonNetwork.IsMasterClient && true == PhotonNetwork.InRoom)
				{
					GetFictionalTime(out lMinute, out lSecond);

					Assert.IsNotNull(_photonView, "[HistoryManager] AddEvent(), _photonView is null.");

					_photonView.RPC("RPC_AddEvent", RpcTarget.AllBufferedViaServer, (int)pType, pFromTeamA, pPlayerName, lMinute, lSecond);
				}
			}
			else
			{
				GetFictionalTime(out lMinute, out lSecond);

				RPC_AddEvent(pType, pFromTeamA, pPlayerName, lMinute, lSecond);
			}
		}

		/// <summary>
		/// Removes every event saved
		/// </summary>
		public	void	ClearHistory()
		{
			if (true == PhotonNetwork.IsConnected)
			{
				if (true == PhotonNetwork.IsMasterClient && true == PhotonNetwork.InRoom)
				{
					Assert.IsNotNull(_photonView, "[HistoryManager] ClearHistory(), this.photonView is null.");

					_photonView.RPC("RPC_ClearHistory", RpcTarget.AllBufferedViaServer);
				}
			}
			else
			{
				RPC_ClearHistory();
			}
		}

		#endregion

		#region Private Methods

		/// <summary>
		/// Gets fictional time
		/// </summary>
		/// <param name="pMinute">Fictional minute</param>
		/// <param name="pSecond">Fictional second</param>
		private	void	GetFictionalTime(out int pMinute, out int pSecond)
		{
			Assert.IsNotNull(_firstPeriodTimerRef, "[HistoryManager] GetFictionalTime(), _firstPeriodTimerRef is null.");

			Assert.IsNotNull(_secondPeriodTimerRef, "[HistoryManager] GetFictionalTime(), _secondPeriodTimerRef is null.");

			float lTimeRemaining	= _firstPeriodTimerRef.CurrentValue + _secondPeriodTimerRef.CurrentValue;
			float lTotalTime		= _firstPeriodTimerRef.DefaultValue + _secondPeriodTimerRef.DefaultValue;
			float lFictionalTime	= 90f - ((lTimeRemaining / lTotalTime) * 90f);

			pMinute = Mathf.FloorToInt(lFictionalTime);
			pSecond = (int)((lFictionalTime - pMinute) * 60f);
		}

		#region Network Methods

		/// <summary>
		/// RPC called to add specified event
		/// </summary>
        /// <param name="pType">Type of event</param>
        /// <param name="pFromTeamA">Is event from team A ?</param>
        /// <param name="pPlayerName">Name of player causing event</param>
		/// <param name="pMinute">Minute the event occured</param>
		/// <param name="pSecond">Second the event occured</param>
		[PunRPC]
		private	void	RPC_AddEvent(object pType, object pFromTeamA, object pPlayerName, object pMinute, object pSecond)
		{
			HistoryEvent	lNewEvent	=	new HistoryEvent((eHistoryEventType)pType, (bool)pFromTeamA, (string)pPlayerName, (int)pMinute, (int)pSecond);

			Assert.IsNotNull(lNewEvent, "[HistoryManager] RPC_AddEvent(), lNewEvent is null.");

			Assert.IsNotNull(_historyEventsRef, "[HistoryManager] RPC_AddEvent(), _historyEventsRef is null.");

			Assert.IsNotNull(_historyEventsRef.CurrentValue, "[HistoryManager] RPC_AddEvent(), _historyEventsRef.CurrentValue is null.");

			_historyEventsRef.CurrentValue.Add(lNewEvent);
		}

		/// <summary>
		/// RPC called to clear history
		/// </summary>
		[PunRPC]
		private	void	RPC_ClearHistory()
		{
			Assert.IsNotNull(_historyEventsRef, "[HistoryManager] RPC_ClearHistory(), _historyEventsRef is null.");

			Assert.IsNotNull(_historyEventsRef.CurrentValue, "[HistoryManager] RPC_ClearHistory(), _historyEventsRef.CurrentValue is null.");

			_historyEventsRef.CurrentValue.Clear();
		}

		#endregion

		#endregion
	}
}
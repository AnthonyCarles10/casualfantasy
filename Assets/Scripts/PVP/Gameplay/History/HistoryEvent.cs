﻿using UnityEngine;

namespace Sportfaction.CasualFantasy.PVP.Gameplay.History
{
	/// <summary>
	/// Class handling match events informations
	/// </summary>
	[System.Serializable]
	public sealed class HistoryEvent
	{
		#region Properties

		//! Type of history event
		public	eHistoryEventType	Type		{ get { return _type; } }
		//! Is event from team A ?
		public	bool				FromTeamA	{ get { return _fromTeamA; } }
		//! Minute the event occured
		public	int					Minute		{ get { return _minute;} }
		//! Second the event occured
		public	int					Second		{ get { return _second; } }
		//! Name of player causing event
		public	string				PlayerName	{ get { return _playerName; } }

		#endregion

		#region Fields

		[SerializeField, ReadOnly, Tooltip("Type of history event")]
		private	eHistoryEventType	_type       =	eHistoryEventType.NONE; //!< Type of history event
		[SerializeField, ReadOnly, Tooltip("Is event from team A")]
		private	bool				_fromTeamA  =	false; //!< Is event from team A ?
		[SerializeField, ReadOnly, Tooltip("Minute when history event occured")]
		private int					_minute     =	0; //!< Minute the event occured
		[SerializeField, ReadOnly, Tooltip("Seconds when history event occured")]
		private int					_second     =	0; //!< Second the event occured
		[SerializeField, ReadOnly, Tooltip("Name of the player")]
		private string				_playerName =	""; //!< Name of player causing event

		#endregion

		#region Public Methods

		/// <summary>
		/// Default constructor
		/// </summary>
		/// <param name="pType">Type of event</param>
		/// <param name="pFromTeamA">Is event from team A ?</param>
		/// <param name="pPlayerName">Name of the player causing event</param>
		/// <param name="pMinute">Minute the event occured</param>
		/// <param name="pSecond">Second the event occured</param>
		public HistoryEvent(eHistoryEventType pType, bool pFromTeamA, string pPlayerName, int pMinute, int pSecond)
		{
			_type		=	pType;
			_fromTeamA	=	pFromTeamA;
			_playerName =	pPlayerName;
			_minute		=	pMinute;
			_second		=	pSecond;
		}

		#endregion
	}
}
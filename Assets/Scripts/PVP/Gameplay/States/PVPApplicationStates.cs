﻿using Photon.Pun;
using Sportfaction.CasualFantasy.Manager.Profile;
using Sportfaction.CasualFantasy.PVP.Network;
using Sportfaction.CasualFantasy.PVP.UI;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Sportfaction.CasualFantasy.PVP.Gameplay.States
{
	/// <summary>
	/// Class handling every application state (focus/pause) during PVP
	/// </summary>
	public class PVPApplicationStates : MonoBehaviour
	{
		#region Variables

		[Header("Variables")]
		[SerializeField, Tooltip("If true, pause and focus are taken in consideration")]
		private	bool		_workInEditor	=	false;
		[SerializeField, Tooltip("Scene to load if loses focus or is pausing")]
		private	string		_sceneToLoad	=	"";

		#endregion

		#region Private Methods

		/// <summary>
		/// MonoBehaviour message received when the player gets or loses focus 
		/// </summary>
		/// <param name="pFocus">True if the GameObjects have focus, else False.</param>
		private void OnApplicationFocus(bool pFocus)
		{
			if (false == Application.isEditor || true == _workInEditor)
			{
				if (false == pFocus)
				{
					if (true == PhotonNetwork.IsConnected)
					{
                        //PhotonNetwork.Disconnect();

                        //SceneManager.LoadScene(_sceneToLoad);
                        Debug.Log("OnApplicationFocus true");
                        PhotonMain.Instance.PhotonView.RPC("RPC_ManagerIsDisconnected", RpcTarget.All, UserData.Instance.PVPTeam);

                        if(null != NetworkUI.Instance)
                        {
                            NetworkUI.Instance.ShowNeedToReconnectGrp();
                        }
                    }
                }
                else
                {
                    if (true == PhotonNetwork.IsConnected)
                    {
                        Debug.Log("OnApplicationFocus false");
                        PhotonMain.Instance.PhotonView.RPC("RPC_ManagerIsReconnected", RpcTarget.All, UserData.Instance.PVPTeam);
                    }
                }
			}
		}

		/// <summary>
		/// MonoBehaviour message received when the application pauses
		/// </summary>
		/// <param name="pPause">True if the application is paused, else False.</param>
		private void OnApplicationPause(bool pPause)
		{
			if (false == Application.isEditor || true == _workInEditor)
			{
				if (true == pPause)
				{
					if (true == PhotonNetwork.IsConnected)
					{
                        //PhotonNetwork.Disconnect();

                        //SceneManager.LoadScene(_sceneToLoad);
                        Debug.Log("OnApplicationPause true");
                        PhotonMain.Instance.PhotonView.RPC("RPC_ManagerIsDisconnected", RpcTarget.All, UserData.Instance.PVPTeam);
                        if (null != NetworkUI.Instance)
                        {
                            NetworkUI.Instance.ShowNeedToReconnectGrp();
                        }
                    }
				}
                else
                {

                    if (true == PhotonNetwork.IsConnected)
                    {
                        Debug.Log("OnApplicationPause false");
                        PhotonMain.Instance.PhotonView.RPC("RPC_ManagerIsReconnected", RpcTarget.All, UserData.Instance.PVPTeam);
                    }

                }
			}
		}

		#endregion
	}
}

﻿using Sportfaction.CasualFantasy.Manager.Profile;

using Photon.Realtime;
using System;
using UnityEngine;
using UnityEngine.Assertions;

namespace Sportfaction.CasualFantasy.PVP.Network
{
	/// <summary>
	/// Description: Handle pvp matchmaking
	/// Author: Antoine de Lachèze-Murel
	/// </summary>
    public class Matchmaking
	{
		#region Fields

		private	RoomInfo[]			_rooms		=	new RoomInfo[0];	// List of all rooms
		private	UserProfileData		_profile	=	null;				// User profile data
		private	string				_version	=	"";					// Room's version to find
		private	string				_roomName	=	"";					// Name of the room found
		private	int					_roomScore	=	Int32.MaxValue;		// Score of this room
		private	int					_userRank	=	0;					// Rank of the user
		private	bool				_firstMatch	=	false;				// Is the first match of this player

		#endregion

		#region Public Methods

		/// <summary>
		/// Specific constructor
		/// </summary>
		/// <param name="pVersion">Current network version</param>
		public	Matchmaking(string pVersion)
		{
			_roomName	=	"";
			_roomScore	=	Int32.MaxValue;
			_version	=	pVersion;

			if (null != UserData.Instance)
			{
				_profile	=	UserData.Instance.Profile;

				Assert.IsNotNull(_profile, "<color=#48D1CC>[Matchmaking]</color> FindRoom(), _profile is null.");

				_firstMatch	=	(0 == _profile.NbGamesPlayed);
				_userRank	=	_profile.Rank;
			}
		}

		/// <summary>
		/// Update all photons rooms
		/// </summary>
		/// <param name="pRooms"></param>
		public	void	UpdatePhotonRooms(RoomInfo[] pRooms)
		{
			_rooms	=	pRooms;

			Assert.IsNotNull(_rooms, "<color=#48D1CC>[Matchmaking]</color> FindRoom(), _rooms is null.");
		}

		/// <summary>
		/// Find a room to join
		/// </summary>
		/// <param name="pRoomName">Name of the room to join</param>
		/// <returns>True if a room was found</returns>
		public	bool	FindRoom(ref string pRoomName)
		{
			#if UNITY_EDITOR && DEVELOPMENT_BUILD
				Debug.Log("<color=#48D1CC>[Matchmaking]</color> FindRoom() invoke");
			#endif

			FindBestRoom();

			pRoomName	=	_roomName;

			return (false == string.IsNullOrEmpty(_roomName));
		}

		#endregion

		#region Private Methods

		/// <summary>
		/// Find the best room to join
		/// </summary>
		private	void	FindBestRoom()
		{
			int		lCurScore	=	0;

			foreach (RoomInfo lInfo in _rooms)
			{
				Assert.IsNotNull(lInfo, "<color=#48D1CC>[Matchmaking]</color> FindFirstMatch(), lInfo is null.");

				Assert.IsNotNull(lInfo.CustomProperties, "<color=#48D1CC>[Matchmaking]</color> FindFirstMatch(), lInfo.CustomProperties is null.");

				if (0 < lInfo.PlayerCount && lInfo.PlayerCount < lInfo.MaxPlayers)
				{
					if (_version.Equals(lInfo.CustomProperties[RoomSettings.KeyVersion] as string))
					{
						lCurScore	=	ComputeRoomScore(lInfo);

						if (lCurScore < _roomScore)
						{
							_roomScore	=	lCurScore;
							_roomName	=	lInfo.Name;
						}
						if (lCurScore == 1)
						{
							break;
						}
					}
				}
			}
		}

		/// <summary>
		/// Compute score for a room
		/// </summary>
		/// <param name="lInfo">Infos of a room</param>
		/// <returns>Score of this room</returns>
		private	int		ComputeRoomScore(RoomInfo lInfo)
		{
			if (false == _firstMatch)
			{
				return Mathf.Abs((int)lInfo.CustomProperties[RoomSettings.KeyMasterRank] - _userRank);
			}
			else
			{
				if (true == (bool)lInfo.CustomProperties[RoomSettings.KeyMasterFirstGame])
				{
					if (true == (bool)lInfo.CustomProperties[RoomSettings.KeyMasterPrenium])
					{
						return 1;
					}
					else
					{
						return 2;
					}
				}
				else
				{
					return (int)lInfo.CustomProperties[RoomSettings.KeyMasterRank] + 2;
				}
			}
		}

		#endregion
	}
}
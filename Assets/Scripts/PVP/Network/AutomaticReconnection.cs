﻿using Photon.Realtime;
using Photon.Pun;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Sportfaction.CasualFantasy.PVP.Network
{
	/// <summary>
	/// Description: Reconnect to current photon room
	/// Author: Rémi Carreira
	/// </summary>
	public class AutomaticReconnection : MonoBehaviourPunCallbacks
	{
		#region Properties

		[SerializeField]
		private	byte	_maxAttempts	=	3;	// Max reconnection attempts before return to menu
		[SerializeField]
		private	float	_delay			=	3f;	// Delay before retry connection
		[SerializeField]
		private	string	_levelName		=	""; // Name of the level to load if reconnection failed

		private Coroutine	_coroutine	=	null;   // Coroutine used to reconnect to the room
		private	byte		_nbAttemps	=	0;		// Cur number of attempts
  		
  		#pragma warning disable 0414
		private	bool		_reconnect	=	false;	// Use to know if reconnect
  		#pragma warning restore 0414
		
		#endregion

		#region Public Methods

		/// <summary>
		/// Pun Behaviour Method | Set reconnection to false
		/// </summary>
		public	override	void	OnJoinedRoom()
		{
			#if UNITY_EDITOR
				Debug.Log("AutomaticReconnection: OnJoinedRoom() was called.");
			#endif

			_reconnect	=	false;
		}

		/// <summary>
		/// Pun Behaviour Method | Reconnect if connection failed
		/// </summary>
		/// <param name="pCause">Cause of the disconnection</param>
		public	override	void	OnDisconnected(DisconnectCause pCause)
		{
			#if UNITY_EDITOR
				Debug.Log("AutomaticReconnection: OnConnectionFail() was called.");
			#endif

			_nbAttemps	=	0;

			switch (pCause)
			{
				case DisconnectCause.ClientTimeout:
					_reconnect	=	true;
					break;
				case DisconnectCause.ServerTimeout:
					_reconnect	=	true;
					break;
				case DisconnectCause.DisconnectByServerLogic:
					_reconnect	=	true;
					break;
				default:
					break;
			}
		}

		#endregion

		#region Private Methods

		/// <summary>
		/// Reconnect and rejoin the current room
		/// </summary>
		public void	Reconnect()
		{
			if (null == _coroutine)
			{
				_coroutine	=	StartCoroutine(ReconnectCoroutine());
			}
			#if UNITY_EDITOR
			else
			{
				Debug.LogWarning("AutomaticReconnection: Reconnect() already called.");
			}
			#endif
		}

		/// <summary>
		/// Coroutine of reconnection
		/// </summary>
		/// <returns>Return wait time</returns>
		private	IEnumerator	ReconnectCoroutine()
		{
			while (_nbAttemps < _maxAttempts)
			{
				++_nbAttemps;

				if (NetworkReachability.NotReachable != Application.internetReachability)
				{
					if (true == PhotonNetwork.ReconnectAndRejoin())
					{
						#if UNITY_EDITOR
							Debug.Log("ReconnectionCoroutine: ReconnectAndRejoinRoom() was called.");
						#endif

						break;
					}
				}

				yield return new WaitForSeconds(_delay);
			}
			if (ClientState.ConnectingToGameserver != PhotonNetwork.NetworkClientState &&
				false == PhotonNetwork.InRoom)
			{
				SceneManager.LoadScene(_levelName, LoadSceneMode.Single);
			}

			_coroutine = null;
		}

		#endregion
	}
}
﻿using Sportfaction.CasualFantasy.References;
using Sportfaction.CasualFantasy.Events;

using Photon.Pun;
using System;
using UnityEngine;
using UnityEngine.Assertions;
using Sportfaction.CasualFantasy.Manager.Profile;

namespace Sportfaction.CasualFantasy.PVP.Network
{
	/// <summary>
	/// Description: 
	/// Author: Rémi Carreira
	/// </summary>
	public sealed class NetworkedTimer : MonoBehaviourPun
	{
		#region Fields

		[Header("Variables")]
		[SerializeField, Tooltip("Reference to the duration of the timer")]
		private	IntReference	_durationRef		=	null;
		[SerializeField, Tooltip("Game event invoke when timer finished")]
		private	GameEvent		_onTimerFinished	=	null;

		[Header("Informations")]
		[SerializeField, ReadOnly, Tooltip("Previous timestamp")]
		private	int		_prevTimestamp	=	0;
		[SerializeField, ReadOnly, Tooltip("Current timestamp")]
		private	int		_curTimerstamp	=	0;
		[SerializeField, ReadOnly, Tooltip("Delta timestamp")]
		private	int		_deltaTimestamp	=	0;

		#endregion

		#region Public Methods

		/// <summary>OnTimer
		/// Start timer (only master)
		/// </summary>
		public	void	StartTimer()
		{
			if (false == this.enabled)
			{
                OnTimerStarted(PhotonNetwork.ServerTimestamp);
            }
		}

		/// <summary>
		/// Stop timer (only master)
		/// </summary>
		public	void	StopTimer()
		{
			if (true == this.enabled)
			{
               OnTimerStopped(PhotonNetwork.ServerTimestamp);
            }
		}

		/// <summary>
		/// Reset timer (only master)
		/// </summary>
		public	void	ResetTimer()
		{
            OnTimerReset(PhotonNetwork.ServerTimestamp);
        }

		#endregion

		#region Private Methods

		/// <summary>
		/// Monobehaviour used to disable this script
		/// </summary>
		private	void	Awake()
		{
			this.enabled = false;
		}

		/// <summary>
		/// Monobehaviour used to update timer
		/// </summary>
		private	void	Update()
		{
			ComputeDeltaTimestamp();

			UpdateDurationRef();
		}

		/// <summary>
		/// Compute delta timestamp
		/// </summary>
		private	void	ComputeDeltaTimestamp()
		{
			_curTimerstamp	=	PhotonNetwork.ServerTimestamp;
			_deltaTimestamp	=	0;

			if (_curTimerstamp < _prevTimestamp)
			{
				_deltaTimestamp	+=	Int32.MaxValue - _prevTimestamp;
				_prevTimestamp	=	Int32.MinValue;
			}

			_deltaTimestamp	+=	_curTimerstamp - _prevTimestamp;

			_prevTimestamp	=	_curTimerstamp;
		}

		/// <summary>
		/// Update duration reference
		/// </summary>
		private	void	UpdateDurationRef()
		{
			Assert.IsNotNull(_durationRef, "[NetworkedTimer] UpdateDurationRef(), _durationRef is null.");

			_durationRef.CurrentValue	-=	_deltaTimestamp;
			
			if (_durationRef.CurrentValue <= 0)
            {
                Assert.IsNotNull(_durationRef, "[NetworkedTimer] RPC_OnTimerFinished(), _durationRef is null.");

                _durationRef.CurrentValue = 0;

                Assert.IsNotNull(_onTimerFinished, "[NetworkedTimer] RPC_OnTimerFinished(), _onTimerFinished is null.");

                if (this.enabled)
                    _onTimerFinished.Invoke();

                this.enabled = false;
            }
		}

		#region Networked Methods

		/// <summary>
		/// RPC called when master start timer
		/// </summary>
		/// <param name="pTimestamp">Timestamp when timer was started</param>
		private	void	OnTimerStarted(int pTimestamp)
		{
			this.enabled	=	true;

			_prevTimestamp	=	pTimestamp;
		}

		/// <summary>
		/// RPC called when master stop timer
		/// </summary>
		/// <param name="pTimestamp">Timestamp when timer was stopped</param>
		private	void	OnTimerStopped(int pTimestamp)
		{
			this.enabled	=	false;

			if (_curTimerstamp > pTimestamp)
			{
				Assert.IsNotNull(_durationRef, "[NetworkedTimer] RPC_OnTimerStopped(), _durationRef is null.");

				_durationRef.CurrentValue	+=	_curTimerstamp - pTimestamp;
			}

			_prevTimestamp	=	pTimestamp;
			_curTimerstamp	=	pTimestamp;
		}

		/// <summary>
		/// RPC called when timer is reset
		/// </summary>
		/// <param name="pTimestamp">Timestamp when timer was reset</param>
		private	void	OnTimerReset(int pTimestamp)
		{
			Assert.IsNotNull(_durationRef, "[NetworkedTimer] RPC_OnTimerReset(), _durationRef is null.");

			_durationRef.CurrentValue	=	_durationRef.DefaultValue;

			_prevTimestamp	=	pTimestamp;
		}

		#endregion

		#endregion
	}
}
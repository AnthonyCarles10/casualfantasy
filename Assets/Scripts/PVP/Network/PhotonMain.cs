﻿
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using Newtonsoft.Json;
using Photon.Pun;
using Photon.Realtime;
using Sportfaction.CasualFantasy.Config;
using Sportfaction.CasualFantasy.Events;
using Sportfaction.CasualFantasy.Manager.Profile;
using Sportfaction.CasualFantasy.PVP.Gameplay.Matchs;
using Sportfaction.CasualFantasy.PVP.PreMatch;
using Sportfaction.CasualFantasy.PVP.UI;
using Sportfaction.CasualFantasy.Services;
using SportFaction.CasualFootballEngine.ActionService.Enum;
using SportFaction.CasualFootballEngine.FieldService.Model;
using SportFaction.CasualFootballEngine.PeriodService.Enum;
using SportFaction.CasualFootballEngine.PhaseService.Enum;
using SportFaction.CasualFootballEngine.PlayerService.Enum;
using SportFaction.CasualFootballEngine.SynchronizationService.Model;
using SportFaction.CasualFootballEngine.TeamService.Enum;
using SportFaction.CasualFootballEngine.TossService.Enum;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.SceneManagement;

namespace Sportfaction.CasualFantasy.PVP.Network
{
    /// <summary>
    /// Description: Photon RPC
    /// Author: Antoine de Lachèze-Murel
    /// </summary>
    #pragma warning disable 414, CS0649
	[RequireComponent(typeof(PhotonView))]
    public class PhotonMain : SingletonPUN<PhotonMain>
    {

        #region Properties

        private static TypedLobby _lobby = new TypedLobby("SFLobby", LobbyType.SqlLobby);
       
        public PhotonView PhotonView { get { return _photonView; } }

        #endregion

        #region Fields

        [Header("Settings")]
        [SerializeField, Tooltip("If true, define to Photon to always load the same scene")]
        private bool _autoSyncScene = false;
        [SerializeField, Tooltip("Max number of attempt to join a room")]
        private byte _maxJoinAttempt = 10;
        [SerializeField, Tooltip("Delay before PUN send timeout when application pause")]
        private float _backgroundTimeout = 1f;
        [SerializeField, Tooltip("Delay (in seconds) before playing with a bot")]
        private float _delayBeforePlayingBot = 30f;
        [SerializeField, Tooltip("Text used to display some infos")]
        private TMPro.TMP_Text _matchmakingInfos = null;
        [SerializeField, Tooltip("Settings when create a room")]
        private RoomSettings _roomSettings = null;
        [SerializeField, Tooltip("Matchmaking Version")]
        private string _version = null;

        [Space]
        [SerializeField, ReadOnly, Tooltip("Infos used to know if is currenlty searching a room")]
        private bool _searching = false;
        [SerializeField, ReadOnly, Tooltip("Infos used to know if user is currently joining a room")]
        private bool _joining = false;
        [SerializeField, ReadOnly, Tooltip("Infos used to know if the user is reconnecting to the previous room")]
        private bool _reconnecting = false;
        [SerializeField, ReadOnly, Tooltip("Define if user launch or not a bot")]
        private bool _launchBot = false;
        [SerializeField, ReadOnly, Tooltip("Cur number of attempt to join a room")]
        private byte _curJoinAttempt = 0;
        [SerializeField, ReadOnly, Tooltip("Name of the room to join")]
        private string _roomToJoin = "";

        [Header("Data")]
        [SerializeField, Tooltip("Scriptable object contains infos about match UI")]
        private PVPData _pvpData = null;

        // Matchmaking used to find the room to connect
        private Matchmaking _matchMaking        =   null;
        // Coroutine used for the countdown before playing against a bot
        private IEnumerator _countdownCoroutine =   null;
        // Coroutine used for reconnect max delay
        private IEnumerator _reconnectDelayCoroutine =   null;

        private JsonSerializerSettings _autoJsonSettings = new JsonSerializerSettings { TypeNameHandling = TypeNameHandling.All }; //!< Custom json serializer settings


        //private JsonSerializerSettings _autoJsonSettings = new JsonSerializerSettings { TypeNameHandling = TypeNameHandling.All }; //!< Custom json serializer settings

        [SerializeField, Tooltip("Photon view used to send RPC")]
        private PhotonView _photonView = null; //!< Photon view used to send RPC


        [Header("Game events")]
        [SerializeField, Tooltip("Event invoke when pun room is ready")]
        private GameEvent _onPUNRoomReady = null;
        [SerializeField, Tooltip("Event invoke when reconnect is failed")]
        private GameEvent _onFailedReconnect = null;
        [SerializeField]
        private GameEvent _onEngineChangePhase = null;
        [SerializeField]
        private GameEvent RPC_LoadGameplay_GE = null;
        [SerializeField]
        private GameEvent _onEngineStartPeriod = null;
        [SerializeField]
        private GameEvent _onEngineStopPeriod = null;
        [SerializeField]
        private GameEvent _onEngineChangeFieldSetup = null;
        [SerializeField]
        private GameEvent _onEngineExecuteActions = null;
        [SerializeField]
        private GameEvent _onManagerAPrepareAction = null;
        [SerializeField]
        private GameEvent _onManagerBPrepareAction = null;
        [SerializeField]
        private GameEvent _onTossSideSelected = null;
        [SerializeField]
        private GameEvent _RPC_TimerHasEllapsed_GE = null;
        [SerializeField]
        private GameEvent _skipTacticalPhaseEvt = null;

        private bool _isTeamADisconnected = false;

        private bool _isTeamBDisconnected = false;

        private bool _isTeamAReconnected = false;

        private bool _isTeamBReconnected = false;

        private bool _needToSyncAllData = false;


        #endregion

        #region Public methods

        /// <summary>
        /// Search match
        /// </summary>
        public void Search()
        {
            if(true == _searching)
            {
                #if (UNITY_EDITOR || UNITY_STANDALONE) && DEVELOPMENT_BUILD
                    Debug.Log("<color=#DC143C>[PUNManager] Already searching a room.</color> ");
                #endif
                return;
            }
            else
            {
                _searching = true;
            }

            #if (UNITY_EDITOR || UNITY_STANDALONE) && DEVELOPMENT_BUILD
                Debug.Log("<color=#48D1CC>[PUNManager] Search() called.</color> ");
            #endif

         
            _curJoinAttempt = 0;

            SetInfosText(I2.Loc.LocalizationManager.GetTranslation("PUNManagerSearchingGame"));

            if (true == PhotonNetwork.IsConnected && PhotonNetwork.NetworkingClient.State != ClientState.Joined)
            {
             
                if (!Reconnect())
                {
                    JoinRoom();
                }
            }
            else if(false == PhotonNetwork.IsConnected)
            {
                // Check if photon network class is created and available to connect
                if (PhotonNetwork.NetworkClientState == ClientState.PeerCreated || PhotonNetwork.NetworkClientState == ClientState.Disconnected)
                {
                    // Connect to the photon master-server. We use the settings saved in PhotonServerSettings (a .asset file in this project)
                    PhotonNetwork.ConnectUsingSettings();
                }
            }
        }

        /// <summary>
        /// Cancel Bot and photon connection
        /// </summary>
        public void Cancel()
        {
            _searching = false;

            _cancelCountdown();

            DisconnectFromPhoton();

            SetInfosText(I2.Loc.LocalizationManager.GetTranslation("PUNManagerCancel"));
        }

        /// <summary>
        /// Called when the initial connection got established but before you can use the server. 
        /// OnJoinedLobby() or OnConnectedToMaster() are called when PUN is ready.
        /// </summary>
        public override void OnConnected()
        {
            #if (UNITY_EDITOR || UNITY_STANDALONE) && DEVELOPMENT_BUILD
                Debug.LogFormat("<color=#48D1CC>[PUNManager]</color> OnConnectedToPhoton() called.");
            #endif

            Assert.IsNotNull(UserData.Instance, "[PUNManager] OnConnectedToPhoton(), UserData.Instance is null.");

            PhotonNetwork.LocalPlayer.NickName = UserData.Instance.Profile.Pseudo;
            // PhotonNetwork.player.NickName = Store.Instance.User.Profile.Id.ToString();

            ExitGames.Client.Photon.Hashtable lPlayerProperties = new ExitGames.Client.Photon.Hashtable()
            {
                { "ApiId", UserData.Instance.Profile.Id },
                { "IsVIP", UserData.Instance.Profile.IsVip },
                { "Level", UserData.Instance.Profile.Level },
                { "StarLevel", UserData.Instance.Profile.StarLevel },
                { "StarLevelUp", UserData.Instance.Profile.StarLevelUp },
                { "Soft", UserData.Instance.Profile.Soft },
                { "TeamId", _pvpData.TeamId },
            };

            PhotonNetwork.LocalPlayer.SetCustomProperties(lPlayerProperties);
            PhotonNetwork.AuthValues = new AuthenticationValues(UserData.Instance.Profile.Id.ToString());
        }

        /// <summary>
        /// Called after the connection to the master is established and authenticated but only when PhotonNetwork.autoJoinLobby is false.
        /// If you set PhotonNetwork.autoJoinLobby to true, OnJoinedLobby() will be called instead of this.
        /// You can join rooms and create them even without being in a lobby. 
        /// The default lobby is used in that case. The list of available rooms won't become available unless you join a lobby via PhotonNetwork.joinLobby.
        /// </summary>
        public override void OnConnectedToMaster()
        {
            #if (UNITY_EDITOR || UNITY_STANDALONE) && DEVELOPMENT_BUILD
                Debug.LogFormat("<color=#48D1CC>[PUNManager]</color> OnConnectedToMaster() called.");
            #endif

            PhotonNetwork.JoinLobby(_lobby);
        }

        /// <summary>
        ///  Called for any update of the room-listing while in a lobby (PhotonNetwork.insideLobby) on the Master Server.
        /// </summary>
        /// <param name="pRoomList">List of all rooms' info</param>
        public override void OnRoomListUpdate(List<RoomInfo> pRoomList)
        {
            #if (UNITY_EDITOR || UNITY_STANDALONE) && DEVELOPMENT_BUILD
                Debug.LogFormat("<color=#48D1CC>[PUNManager]</color> OnReceivedRoomListUpdate() called.");
            #endif

            if (!Reconnect())
            {
                if (null != _matchMaking)
                {
                    _matchMaking.UpdatePhotonRooms(pRoomList.ToArray());
                }
#if (UNITY_EDITOR || UNITY_STANDALONE) && DEVELOPMENT_BUILD
                Debug.LogFormat("<color=#48D1CC>[PUNManager]</color> _searching = . " +_searching);
#endif

                if (true == _searching && false == _joining)
                {
                    JoinRoom();
                }
            }
        }

        /// <summary>
        /// Called when entering a room(by creating or joining it). Called on all clients(including the Master Client).
        /// This method is commonly used to instantiate player characters.If a match has to be started "actively", you can call an PunRPC triggered by a user's button-press or a timer.
        /// When this is called, you can usually already access the existing players in the room via PhotonNetwork.playerList.Also, 
        /// all custom properties should be already available as Room.customProperties.Check Room.playerCount to find out if enough players are in the room to start playing.
        /// </summary>
        public override void OnJoinedRoom()
        {
            #if (UNITY_EDITOR || UNITY_STANDALONE) && DEVELOPMENT_BUILD
                Debug.LogFormat("<color=#48D1CC>[PUNManager]</color> OnJoinedRoom() ( Name: " + PhotonNetwork.CurrentRoom.Name + " / isReconnecting: " + _reconnecting + " / HaveMinPlayers: " + HaveMinPlayers() + " )");
            #endif
          
            if (null != UserData.Instance)
            {

                UserData.Instance.PvpRoomName = PhotonNetwork.CurrentRoom.Name;
                UserData.Instance.PVPTeam = PhotonNetwork.IsMasterClient ? eTeam.TEAM_A : eTeam.TEAM_B;

                if (false == _reconnecting)
                {

                    SportFaction.CasualFootballEngine.ManagerService.Model.Manager lManager = new SportFaction.CasualFootballEngine.ManagerService.Model.Manager();
                    lManager.Id = UserData.Instance.Profile.Id.ToString();
                    lManager.TeamId = _pvpData.TeamId.ToString();
                    lManager.Name = UserData.Instance.Profile.Pseudo;

                    Debug.Log("OnJoinedRoom => " + lManager.ConvertToJson());

                    _photonView.RPC("RPC_ManagerJoinRoomWithTeam", RpcTarget.Others, lManager.ConvertToJson());
                }
                else if (true == HaveMinPlayers())
                {
                    StopCoroutine(_countdownCoroutine);
                    if (null != _reconnectDelayCoroutine)
                    {
                        StopCoroutine(_reconnectDelayCoroutine);
                        _reconnectDelayCoroutine = null;
                    }
                    _needToSyncAllData = true;

                    _photonView.RPC("RPC_NeedToSyncAllData", RpcTarget.Others, UserData.Instance.Profile.Id);
                    _searching = false;
                }
            }
        }

        /// <summary>
        /// Called when a remote player entered the room. This PhotonPlayer is already added to the playerlist at this time.
        /// </summary>
        /// <remarks>
        /// If your game starts with a certain number of players, this callback can be useful to check the
        /// Room.playerCount and find out if you can start.
        /// </remarks>
        public override void OnPlayerEnteredRoom(Player pPlayer)
        {
            #if (UNITY_EDITOR || UNITY_STANDALONE) && DEVELOPMENT_BUILD
                Debug.LogFormat("<color=#48D1CC>[PUNManager]</color> OnPlayerEnteredRoom() called, player with name '" + pPlayer.NickName + "'");
            #endif

            if (true == HaveMinPlayers())
            {
                if (true == PhotonNetwork.IsMasterClient)
                {
                    StopCoroutine(_countdownCoroutine);
                    Assert.IsNotNull(PhotonNetwork.CurrentRoom, "[PunManager] OnPhotonPlayerConnected(), PhotonNetwork.room is null.");

                    // Remove if we have functional reconnect method 
                    PhotonNetwork.CurrentRoom.IsVisible = false;

                    RegisterPlayersInRoom();

                    // Assert.IsNotNull(photonView, "[PUNManager] OnPhotonPlayerConnected(), this.photonView is null.");

                    //this.photonView.RPC("RPC_ManagerIsReady", RpcTarget.Others, UserData.Instance.Profile.Id);

                    //this.photonView.RPC("RPC_LoadGameplay", RpcTarget.All);
                }

            }

        }

        /// <summary>
        /// Called when a previous OpJoinRoom call failed on the server.
        /// </summary>
        /// <remarks>
        /// The most common causes are that a room is full or does not exist (due to someone else being faster or closing the room).
        /// </remarks>
        /// <param name="pReturnCode">Operation ReturnCode from the server.</param>
        /// <param name="pMessage">Debug message for the error.</param>
        public override void OnJoinRoomFailed(short pReturnCode, string pMessage)
        {
           
            #if (UNITY_EDITOR || UNITY_STANDALONE) && DEVELOPMENT_BUILD
                Debug.LogFormat("<color=#DC143C>[PUNManager] OnJoinRoomFailed() called with message '" + pMessage + "'.</color>");
            #endif

            if(null != _reconnectDelayCoroutine)
            {
                StopCoroutine(_reconnectDelayCoroutine);
                _reconnectDelayCoroutine = null;
                _searching = false;
                SceneManager.LoadScene("PostSplashScreen");
            }

            UserData.Instance.PvpRoomName = "";
            UserData.Instance.SavePVPRoomName();

            if (true == _searching && false == _reconnecting)
            {
                if (_curJoinAttempt < _maxJoinAttempt)
                {
                    JoinRoom();
                }
                else
                {
                    CreateRoom();
                }
            }
            else if(true == _searching && true == _reconnecting)
            {
                CreateRoom();
            }
        }

        /// <summary>
        /// Most likely all rooms are full or no rooms are available.
        /// When using multiple lobbies(via JoinLobby or TypedLobby), another lobby might have more/fitting rooms.
        /// PUN logs some info if the PhotonNetwork.logLevel is >= PhotonLogLevel.Informational.
        /// </summary>
        /// <param name="pReturnCode">Return code</param>
        /// <param name="pMessage">Error message</param>
        public override void OnJoinRandomFailed(short pReturnCode, string pMessage)
        {
            #if (UNITY_EDITOR || UNITY_STANDALONE) && DEVELOPMENT_BUILD
                Debug.LogFormat("<color=#DC143C>[PUNManager] OnPhotonRandomJoinFailed() called with message '" + pMessage + "'.</color>");
            #endif

            if (true == _searching)
            {
                if (_curJoinAttempt < _maxJoinAttempt)
                {
                    JoinRoom();
                }
                else
                {
                    CreateRoom();
                }
            }
        }

        /// <summary>
        /// PUNBehaviour method called when disconnected from photon
        /// </summary>
        /// <param name="pCause">Cause of disconnection</param>
        public override void OnDisconnected(DisconnectCause pCause)
        {
            #if (UNITY_EDITOR || UNITY_STANDALONE) && DEVELOPMENT_BUILD
                Debug.LogFormat("<color=#DC143C>[PUNManager] OnDisconnected() called with message '" + pCause + "'.</color>");
            #endif

            if (true == _launchBot || (DisconnectCause.DisconnectByClientLogic != pCause && DisconnectCause.DisconnectByServerLogic != pCause))
            {
                LaunchInstantMatchAgainstBot();
            }
        }

        /// <summary>
		/// Called when a remote player left the room. This PhotonPlayer is already removed from the playerlist at this time.
		/// </summary>
		/// <param name="pOtherPlayer">Player disconnected</param>
		public override void OnPlayerLeftRoom(Player pOtherPlayer)
        {



            if (null != UserData.Instance)
            {
                if (UserData.Instance.Profile.Id.ToString() != pOtherPlayer.UserId && eTeam.TEAM_A == UserData.Instance.PVPTeam)
                {
                    PhotonView.RPC("RPC_ManagerIsDisconnected", RpcTarget.All, eTeam.TEAM_B);

                    #if DEVELOPMENT_BUILD
                        Debug.LogFormat("<color=#DC143C>[PVP] OnPlayerLeftRoom TeamB </color>");
                    #endif
                }
                else
                {
                    PhotonView.RPC("RPC_ManagerIsDisconnected", RpcTarget.All, eTeam.TEAM_A);

                    #if DEVELOPMENT_BUILD
                        Debug.LogFormat("<color=#DC143C>[PVP] OnPlayerLeftRoom TeamA </color>");
                    #endif
                }
            }
        }

        /// <summary>
        /// Launch instant match against a bot
        /// </summary>
        public void LaunchInstantMatchAgainstBot()
        {
            if (null != UserData.Instance)
            {
                UserData.Instance.PVPTeam       =   eTeam.TEAM_A;
                UserData.Instance.PvpRoomName   =   "";

                UserData.Instance.SavePVPRoomName();

                InitEngineWithBot();

				//if (null != PreMatchLoader.Instance)
				//    PreMatchLoader.Instance.LoadScene();
				if (null != _onPUNRoomReady)
					_onPUNRoomReady.Invoke();
            }
            else
            {
                Debug.LogError("[PUNManager] LaunchInstantMatchAgainstBot(), Store.Instance is null.");
            }

        }

        /// <summary>
        /// MonoBehaviour method used to unlink functions to matchmaking
        /// </summary>
        public override void OnDisable()
        {
            base.OnDisable();
        }


		public void LoadGamePlay()
        {
            _cancelCountdown();

            if (null != UserData.Instance)
            {
                UserData.Instance.SavePVPRoomName();
            }

            StartCoroutine(ScenesMain.Instance.LoadAsyncGameplayScene());
            //SceneManager.LoadScene("Gameplay3D", LoadSceneMode.Single);
            //PreMatchLoader.Instance.LoadScene();

            _searching = false;

        }


        public void LoadGamePlayTest()
        {
            _cancelCountdown();

            if (null != UserData.Instance)
            {
                UserData.Instance.SavePVPRoomName();
            }

            SceneManager.LoadScene("Gameplay3DSimulation", LoadSceneMode.Single);
            //PreMatchLoader.Instance.LoadScene();

            _searching = false;
        }

        public void OnPhaseChanged(ePhase pPhase)
        {
            if (true == PhotonNetwork.IsConnected && null != NetworkUI.Instance)
            {
                if (
                    true == _isTeamAReconnected && eTeam.TEAM_A == UserData.Instance.PVPTeam ||
                    true == _isTeamBReconnected && eTeam.TEAM_B == UserData.Instance.PVPTeam
                    )
                {
                    NetworkUI.Instance.HideNeedToReconnectGrp();
                    if(eTeam.TEAM_A == UserData.Instance.PVPTeam)
                    {
                        _isTeamAReconnected = false;
                    }
                    else
                    {
                        _isTeamBReconnected = false;
                    }
                }
                StartCoroutine(NetworkUI.Instance.HideIsWaitingGrp());
            }
          
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// MonoBehaviour method used to initialized Photon
        /// </summary>
        private void Awake()
        {
            base.Awake();

            PhotonNetwork.BackgroundTimeout = _backgroundTimeout;

            PhotonNetwork.AutomaticallySyncScene = _autoSyncScene;

            #if DEVELOPMENT_BUILD
                _version    =   "DEV:" + _version;
            #else
            _version = "PROD:" + _version;
            #endif

            if (null == _matchMaking)
            {
                _matchMaking = new Matchmaking(_version);

                Assert.IsNotNull(_matchMaking, "[PUNManager] Awake(), _matchMaking is null.");
            }
            SetInfosText("");
        }

        /// <summary>
        /// Reconnect to the previous match
        /// </summary>
        /// <returns>True if is reconnecting</returns>
        public bool Reconnect()
        {
            if (false == string.IsNullOrEmpty(UserData.Instance.PvpRoomName))
            {

                #if (UNITY_EDITOR || UNITY_STANDALONE) && DEVELOPMENT_BUILD
                Debug.Log("<color=#48D1CC>[PUNManager]</color> Reconnect() called.");
                #endif

                _reconnecting = true;

                MatchEngine.Instance.InitEngine(true);

                PhotonNetwork.JoinRoom(UserData.Instance.PvpRoomName);

                _reconnectDelayCoroutine = ReconnectDelay(3);

                StartCoroutine(_reconnectDelayCoroutine);

                return true;
            }

            return false;
        }

        private IEnumerator ReconnectDelay(float delay = 3)
        {
            while (delay >= 0)
            {
                yield return new WaitForSeconds(1.0f);
                #if (UNITY_EDITOR || UNITY_STANDALONE) && DEVELOPMENT_BUILD
                    Debug.Log("<color=#48D1CC>[PUNManager]</color> Reconnect() Delay ( " + delay + "s )");
                #endif
                delay--;
            }



            #if (UNITY_EDITOR || UNITY_STANDALONE) && DEVELOPMENT_BUILD
                Debug.Log("<color=#DC143C>[PUNManager] Reconnect() Timeout.</color>");
            #endif
            _reconnectDelayCoroutine = null;

            UserData.Instance.PvpRoomName = "";
            UserData.Instance.SavePVPRoomName();

            _reconnecting = false;
            _searching = false;
            SceneManager.LoadScene("PostSplashScreen");
        }


        /// <summary>
        /// Try to join a room
        /// </summary>
        private void JoinRoom()
        {
            #if (UNITY_EDITOR || UNITY_STANDALONE) && DEVELOPMENT_BUILD
                Debug.Log("<color=#48D1CC>[PUNManager]</color> JoinRoom() called.");
            #endif

            if (true == _matchMaking.FindRoom(ref _roomToJoin))
            {
                Assert.IsFalse(string.IsNullOrEmpty(_roomToJoin), "[PUNManager] JoinRoom(), _roomToJoin is null.");

                _reconnecting = false;

                PhotonNetwork.JoinRoom(_roomToJoin);

                ++_curJoinAttempt;

                _joining = true;
            }
            else
            {
                CreateRoom();
            }

        }

        /// <summary>
        /// Create room with room' settings
        /// </summary>
        /// <returns>Return true if a room was successfully created</returns>
        private void CreateRoom()
        {
            #if (UNITY_EDITOR || UNITY_STANDALONE) && DEVELOPMENT_BUILD
                Debug.LogFormat("<color=#48D1CC>[PUNManager]</color> CreateRoom() called.");
            #endif

            _reconnecting = false;
            _joining = false;

            RoomOptions lOptions = new RoomOptions();

            Assert.IsNotNull(lOptions, "<color=#48D1CC>Matchmaking</color> CreateRoom() failed, create RoomOptions failed.");

            lOptions.IsVisible = _roomSettings.IsVisible;
            lOptions.PublishUserId = _roomSettings.PublishUserId;
            lOptions.MaxPlayers = _roomSettings.MaxPlayers;
            lOptions.PlayerTtl = _roomSettings.PlayerTTL;
            lOptions.EmptyRoomTtl = _roomSettings.RoomTTL;
            lOptions.PublishUserId = true;

            // Room properties are synced to all players in the room
            lOptions.CustomRoomProperties = new ExitGames.Client.Photon.Hashtable();
            lOptions.CustomRoomProperties.Add(RoomSettings.KeyVersion, _version);
            lOptions.CustomRoomProperties.Add(RoomSettings.KeyLevelIndex, 0);
            // lOptions.CustomRoomProperties.Add(RoomSettings.KeyLevel, _roomSettings.LevelName[1]);
            lOptions.CustomRoomProperties.Add(RoomSettings.KeyLevel, "PreMatchScreen");
            lOptions.CustomRoomProperties.Add(RoomSettings.KeySkillLevel, 0);

            if (null != UserData.Instance)
            {
                UserProfileData lProfileData = UserData.Instance.Profile;

                Assert.IsNotNull(lProfileData, "<color=#48D1CC>[PUNManager]</color> CreateRoom(), lProfileData is null.");

                lOptions.CustomRoomProperties.Add(RoomSettings.KeyMasterFirstGame, (0 == lProfileData.NbGamesPlayed));
                lOptions.CustomRoomProperties.Add(RoomSettings.KeyMasterPrenium, (0 != lProfileData.NbRealPurchase));
                lOptions.CustomRoomProperties.Add(RoomSettings.KeyMasterRank, lProfileData.Rank);
            }

            lOptions.CustomRoomPropertiesForLobby = new string[6] {
                RoomSettings.KeyVersion,
                RoomSettings.KeyLevel,
                RoomSettings.KeySkillLevel,

                RoomSettings.KeyMasterFirstGame,
                RoomSettings.KeyMasterPrenium,
                RoomSettings.KeyMasterRank,
            };

            // create a room and join it
            PhotonNetwork.JoinOrCreateRoom(GetUniqueRoomName(), lOptions, _lobby);

            // SetInfosText("Created a room ! Waiting for another player to join...");
            _countdownCoroutine = _startCountdown(_delayBeforePlayingBot);
            StartCoroutine(_countdownCoroutine);
        }

        /// <summary>
        /// Start a game against a bot
        /// </summary>
        private void PlayAgainstBot()
        {
            #if (UNITY_EDITOR || UNITY_STANDALONE) && DEVELOPMENT_BUILD
                Debug.LogFormat("<color=#48D1CC>[PUNManager]</color> StartGameWithABot() called");
            #endif

            DisconnectFromPhoton();

            if (null != UserData.Instance)
            {
                UserData.Instance.PvpRoomName = "";
                UserData.Instance.SavePVPRoomName();

                InitEngineWithBot();
            }

            // TODO Persist API

            _launchBot = true;


        }

        /// <summary>
        /// Disconnect user from photon servers
        /// </summary>
        private void DisconnectFromPhoton()
        {
            if (true == PhotonNetwork.IsConnected)
            {
                PhotonNetwork.Disconnect();
            }
        }

        /// <summary>
        /// Cancel the countdown when search a room
        /// </summary>
        private void    _cancelCountdown()
        {
            if (null != _countdownCoroutine)
            {
                StopCoroutine(_countdownCoroutine);

                _countdownCoroutine = null;
            }
        }

        /// <summary>
        /// Set the text with infos
        /// </summary>
        /// <param name="pInfos">Infos to display</param>
        private void SetInfosText(string pInfos)
        {
            GameObject lMatchmakingInfosGO = GameObject.Find("Photon_Infos_TXT");
            _matchmakingInfos = null != lMatchmakingInfosGO ? lMatchmakingInfosGO.GetComponent<TMPro.TMP_Text>() : null;
            if (null != _matchmakingInfos)
            {
                _matchmakingInfos.text = pInfos;
            }
        }

        /// <summary>
        /// Check if the current room have the min of players required
        /// </summary>
        /// <returns>Return true if the min of players was reached</returns>
        private bool HaveMinPlayers()
        {
            return (PhotonNetwork.CurrentRoom.PlayerCount >= _roomSettings.MinPlayers);
        }

        /// <summary>
        /// Get a unique room name string
        /// </summary>
        /// <returns>Return a GUID string</returns>
        private string GetUniqueRoomName()
        {
            return System.Guid.NewGuid().ToString();
        }

        /// <summary>
        /// Register players in the custom properties of the room
        /// </summary>
        private void RegisterPlayersInRoom()
        {
            #if (UNITY_EDITOR || UNITY_STANDALONE) && DEVELOPMENT_BUILD
                Debug.LogFormat("<color=#48D1CC>[PUNManager]</color> RegisterPlayersInRoom() called.");
            #endif

            if (true == PhotonNetwork.IsConnected && true == PhotonNetwork.IsMasterClient &&
                null != PhotonNetwork.CurrentRoom && null != PhotonNetwork.PlayerListOthers &&
                0 < PhotonNetwork.PlayerListOthers.Length)
            {
                ExitGames.Client.Photon.Hashtable lNewHashtable = new ExitGames.Client.Photon.Hashtable();

                Assert.IsNotNull(lNewHashtable, "[PUNManager] RegisterPlayersInRoom(), lNewHashtable is null.");

                lNewHashtable.Add(eTeam.TEAM_A.ToString(), GetPlayerID(PhotonNetwork.LocalPlayer));

                lNewHashtable.Add(eTeam.TEAM_B.ToString(), GetPlayerID(PhotonNetwork.PlayerListOthers[0]));

                PhotonNetwork.CurrentRoom.SetCustomProperties(lNewHashtable);
            }
        }

        /// <summary>
        /// Retrieve an ID of a specific player in the current photon room
        /// </summary>
        /// <param name="pTeam">Team of the player to retrieve</param>
        /// <returns>An integer</returns>
        private int RetrievePlayerIDInRoom(eTeam pTeam)
        {
            if (true == PhotonNetwork.IsConnected && null != PhotonNetwork.CurrentRoom)
            {
                Assert.IsNotNull(PhotonNetwork.CurrentRoom.CustomProperties, "[PUNManager] RetrievePlayerIDInRoom(), PhotonNetwork.CurrentRoom.CustomProperties is null.");

                string lTeamStr = pTeam.ToString();

                if (true == PhotonNetwork.CurrentRoom.CustomProperties.ContainsKey(lTeamStr))
                {
                    return (int)PhotonNetwork.CurrentRoom.CustomProperties[lTeamStr];
                }
            }
            return -1;
        }

        /// <summary>
        /// Get the player ID
        /// </summary>
        /// <param name="pPlayer">Player where the ID is store</param>
        /// <returns>An integer</returns>
        private int GetPlayerID(Player pPlayer)
        {
            if (null != pPlayer && null != pPlayer.CustomProperties && true == pPlayer.CustomProperties.ContainsKey("ApiId"))
            {
                return (int)pPlayer.CustomProperties["ApiId"];
            }
            return -1;
        }

        private IEnumerator _startCountdown(float countdownValue = 10)
        {
            while (countdownValue >= 0)
            {
                //Debug.Log("[PhotonMain] _startCountdown() " + countdownValue);
                SetInfosText(I2.Loc.LocalizationManager.GetTranslation("PUNManagerStartCountdown") + " : " + countdownValue + "s");
                yield return new WaitForSeconds(1.0f);
                countdownValue--;
            }

            _countdownCoroutine = null;

            PlayAgainstBot();
        }

        private void InitEngineWithBot()
        {
            SportFaction.CasualFootballEngine.ManagerService.Model.Manager lManagerTeamB = new SportFaction.CasualFootballEngine.ManagerService.Model.Manager();
            lManagerTeamB.Id = "0";
            //lManagerTeamB.TeamId = "1";
            lManagerTeamB.Team = eTeam.TEAM_B.ToString();


            var maxRetryAttempts = 3;
            var pauseBetweenFailures = TimeSpan.FromSeconds(2);
            RetryHelper.RetryOnException(maxRetryAttempts, pauseBetweenFailures, () => {
                MatchEngine.Instance.Engine.ManagerMain.SetManagerB(lManagerTeamB, true);
            });



            MatchEngine.Instance.Engine.PhaseMain.ChangePhase(ePhase.LOAD_GAMEPLAY_SCENE);
        }

        private void OnSceneLoaded(Scene scene, LoadSceneMode mode)
        {

            if (scene.name == "GameplayLogic" && true == _needToSyncAllData)
            {
                if(null != NetworkUI.Instance)
                {
                    Debug.Log(" NetworkUI.Instance.ShowNeedToReconnectGrp()");
                    NetworkUI.Instance.ShowNeedToReconnectGrp();
                }

                Debug.Log("OnSceneLoaded GameplayLogic = " + MatchEngine.Instance.Engine.PeriodMain.Period);
                Debug.Log("TimerValue = " + MatchEngine.Instance.Engine.PeriodMain.TimerValue);
                if (MatchEngine.Instance.Engine.PeriodMain.Period == ePeriod.FIRST_PERIOD)
                {
                    MatchTimers.Instance.FirstPeriodTimerRef.CurrentValue = MatchEngine.Instance.Engine.PeriodMain.TimerValue;
                }
                else
                {
                    MatchTimers.Instance.SecondPeriodTimerRef.CurrentValue = MatchEngine.Instance.Engine.PeriodMain.TimerValue;
                }
                _needToSyncAllData = false;

                PhotonView.RPC("RPC_ManagerIsReconnected", RpcTarget.All, UserData.Instance.PVPTeam);
            }
        }

        // called first
        void OnEnable()
        {
            base.OnEnable();
            SceneManager.sceneLoaded += OnSceneLoaded;
        }

        #endregion

        #region RPC methods

        /// <summary>
        /// RPC_OnEngineChangePhase
        /// </summary>
        [PunRPC]
		public	void RPC_OnEngineChangePhase()
		{
        }

        /// <summary>
        /// RPC_LoadGameplay
        /// </summary>
        [PunRPC]
        public void RPC_LoadGameplay()
        {
            RPC_LoadGameplay_GE.Invoke();
        }

        /// <summary>
        /// RPC_ManagerSyncIsDone
        /// </summary>
        [PunRPC]
        public void RPC_ManagerSyncIsDone(int pData) {}

        /// <summary>
        /// RPC called to synchronize both managers' engine
        /// </summary>
        /// <param name="pData">Data to synchronize</param>
        [PunRPC]
        public void RPC_Synchronize(object pData)
        {
            #if UNITY_EDITOR && DEVELOPMENT_BUILD
                Debug.Log("<color=#48D1CC>Client receive " +  GetObjectSize(pData) + " byte(s) for synchronization.</color>");
            #endif

            string lJson = pData as string;

            Debug.Log(lJson);

            Assert.IsFalse(string.IsNullOrEmpty(lJson), "[MatchEngine] RPC_Synchronize(), lJson is null or empty.");

            SynchronizationInfo[] lInfos = JsonConvert.DeserializeObject<SynchronizationInfo[]>(lJson, _autoJsonSettings);

            Assert.IsNotNull(lInfos, "[MatchEngine] RPC_Synchronize(), lInfos is null.");

            Assert.IsNotNull(MatchEngine.Instance.Engine, "[MatchEngine] RPC_Synchronize(), _engine is null.");

            MatchEngine.Instance.Engine.SynchronizationMain.Synchronize(lInfos);

            MatchEngine.Instance.Engine.SynchronizationMain.CleanNewInfosList();

            if(null != MatchEngine.Instance.Engine.ManagerMain.ManagerTeamA && null != MatchEngine.Instance.Engine.ManagerMain.ManagerTeamB)
            {
                UserData.Instance.PVPTeam = MatchEngine.Instance.Engine.ManagerMain.ManagerTeamA.Id == UserData.Instance.Profile.Id.ToString() ? eTeam.TEAM_A : eTeam.TEAM_B;
            }

            if(UserData.Instance.PVPTeam == eTeam.TEAM_B)
            {
                MatchEngine.Instance.Engine.FieldMain.IsTeamB = true;
            }
        

            //if(false == MatchEngine.Instance.Engine.IsLoadDataFromAPI)
            //{
            //    MatchEngine.Instance.Engine.LoadDataFromAPI(ConfigData.Instance.ENV, false);
            //}

            _photonView.RPC("RPC_ManagerSyncIsDone", RpcTarget.Others, UserData.Instance.PVPTeam);


        }

        /// <summary>
        /// RPC called to synchronize both managers' engine
        /// </summary>
        /// <param name="pData">Data to synchronize</param>
        [PunRPC]
        public void RPC_SynchronizeAllData(object pData)
        {
            if(true == _needToSyncAllData)
            {
#if UNITY_EDITOR && DEVELOPMENT_BUILD
                Debug.Log("<color=#48D1CC>Client receive " + GetObjectSize(pData) + " byte(s) for synchronization.</color>");
#endif

                string lJson = pData as string;

                Debug.Log(lJson);

                Assert.IsFalse(string.IsNullOrEmpty(lJson), "[MatchEngine] RPC_Synchronize(), lJson is null or empty.");

                SynchronizationInfo[] lInfos = JsonConvert.DeserializeObject<SynchronizationInfo[]>(lJson, _autoJsonSettings);

                Assert.IsNotNull(lInfos, "[MatchEngine] RPC_Synchronize(), lInfos is null.");

                Assert.IsNotNull(MatchEngine.Instance.Engine, "[MatchEngine] RPC_Synchronize(), _engine is null.");

                MatchEngine.Instance.RemoveEventsListener();

                MatchEngine.Instance.Engine.SynchronizationMain.Synchronize(lInfos);

                MatchEngine.Instance.Engine.SynchronizationMain.CleanNewInfosList();

                MatchEngine.Instance.InitEventsListener();

                UserData.Instance.PVPTeam = MatchEngine.Instance.Engine.ManagerMain.ManagerTeamA.Id == UserData.Instance.Profile.Id.ToString() ? eTeam.TEAM_A : eTeam.TEAM_B;

                if (UserData.Instance.PVPTeam == eTeam.TEAM_B)
                {
                    MatchEngine.Instance.Engine.FieldMain.IsTeamB = true;
                }

                //SceneManager.LoadScene("Gameplay3D", LoadSceneMode.Single);
                StartCoroutine(ScenesMain.Instance.LoadAsyncGameplayScene());

            }

        }

     
        /// <summary>
        /// Calculates length in bytes of an object an returns size
        /// </summary>
        /// <param name="pTarget">Target object to test</param>
        public int GetObjectSize(object pTarget)
        {
            BinaryFormatter lBinFormatter = new BinaryFormatter();

            Debug.Assert(null != lBinFormatter, "[MatchEngine] GetObjectSize(), lBinFormatter is null.");

            MemoryStream lMemoryStream = new MemoryStream();

            Debug.Assert(null != lMemoryStream, "[MatchEngine] GetObjectSize(), lMemoryStream is null.");

            lBinFormatter.Serialize(lMemoryStream, pTarget);

            byte[] lArray = lMemoryStream.ToArray();

            return lArray.Length;
        }

        /// <summary>
        /// RPC_ManagerJoinRoomWithTeam
        /// </summary>
        [PunRPC]
        public void RPC_ManagerJoinRoomWithTeam(string pJson) {}

        /// <summary>
        /// RPC_ManagerIsReadyToChangePhase
        /// </summary>
        [PunRPC]
        public void RPC_ManagerIsReadyToChangePhase(eTeam pTeam, int lTimerValue) {}

        /// <summary>
        /// RPC called to require opponent to send every engine information
        /// </summary>
        [PunRPC]
        public void RPC_NeedFullSynchronization()
        {
           
        }

        /// <summary>
        /// RPC called when a manager is ready to change phase
        /// </summary>
        [PunRPC]
        public void RPC_ManagerIsReady() { }


        /// <summary>
        /// RPC called when a manager is ready to change phase
        /// </summary>
        [PunRPC]
        public void RPC_EndedPhase(eTeam pTeam, int pManagerId)
        {
            //if (pTeam != UserData.Instance.PVPTeam && null != GameObject.Find("#IsWaiting_Grp"))
            //{
            //    GameObject.Find("#IsWaiting_Grp").SetActive(false);
            //}
        }

        /// <summary>
        /// RPC called when engine started period
        /// </summary>
        [PunRPC]
        public void RPC_OnEngineStartPeriod()
        {
            //MatchEngine.Instance.Engine.PeriodMain.StartPeriod();
            //_onEngineStartPeriod.Invoke();
        }

        /// <summary>
        /// RPC called when manager is ready to change period
        /// </summary>
        [PunRPC]
        public void RPC_OnManagerReadyToChangePeriod(eTeam pTeam)
        {
        }

        /// <summary>
        /// RPC called when engine stopped period
        /// </summary>
        [PunRPC]
        public void RPC_OnEngineStopPeriod()
        {
            //MatchEngine.Instance.Engine.PeriodMain.StopPeriod();
            //_onEngineStopPeriod.Invoke();
        }


        /// <summary>
        /// RPC called when engine changed field setup
        /// </summary>
        [PunRPC]
        public void RPC_OnEngineChangeFieldSetup(string pFieldSetup)
        {
            //MatchEngine.Instance.Engine.FieldSetupMain.ChangeFieldSetup(pFieldSetup, MatchEngine.Instance.Engine.PossessionMain.CurPlayerPossession, MatchEngine.Instance.Engine.PossessionMain.CurTeamPossession);
            //_onEngineChangeFieldSetup.Invoke();
        }

        /// <summary>
        /// RPC called when engine executed actions
        /// </summary>
        [PunRPC]
        public void RPC_SkipTacticalPhase(eTeam pTeam)
        {
            //MatchEngine.Instance.Engine.ActionMain.SkipTacticalPhase(pTeam);
            _skipTacticalPhaseEvt.Invoke();
        }

        /// <summary>
        /// RPC called when engine executed actions
        /// </summary>
        [PunRPC]
        public void RPC_NeedToChangePeriod(eTeam pTeam)
        {
            MatchEngine.Instance.Engine.PeriodMain.NeedToChangePeriod = true;
        }

        /// <summary>
        /// RPC called when engine prepared action
        /// </summary>
        /// <param name="pExecutor">Executor doing this action</param>
        /// <param name="pAction">Type of action</param>
        /// <param name="pPosition">Position of player to execute action</param>
        /// <param name="pFieldCase">Target of action</param>
        [PunRPC]
        public void RPC_PrepareAction(object pExecutor, object pAction, object pPosition, object pFieldCase)
        {
            FieldCase lFieldCase = null;

            if (null != pFieldCase)
            {
                FieldCase lTmpFC = new FieldCase((string)pFieldCase);
                lFieldCase = MatchEngine.Instance.Engine.FieldMain.GetCase(lTmpFC.X, lTmpFC.Y);


            }
            //#if DEVELOPMENT_BUILD
            //if(null != lFieldCase)
            //{
            //    Debug.Log("<color=#48D1CC>[PVP] RPC_PrepareAction </color> pExecutor = " + (eExecutor)pExecutor + " / " + "pAction = " + (eAction)pAction + " pPosition = " + (ePosition)pPosition + " lFieldCase = " + lFieldCase.X + " / " + lFieldCase.Y);
            //}
            //else
            //{
            //    Debug.Log(" <color=#48D1CC>[PVP] RPC_PrepareAction </color> pExecutor = " + (eExecutor)pExecutor + " / " + "pAction = " + (eAction)pAction + " pPosition = " + (ePosition)pPosition);
            //}
            //#endif

            MatchEngine.Instance.Engine.ActionMain.PrepareAction((eExecutor)pExecutor, (eAction)pAction, (ePosition)pPosition, (FieldCase)lFieldCase);

            if (eExecutor.TEAM_A_AI == (eExecutor)pExecutor || eExecutor.TEAM_A_MANAGER == (eExecutor)pExecutor)
            {
                UtilityMethods.InvokeGameEvent(_onManagerAPrepareAction);
            }
            else
            {
                UtilityMethods.InvokeGameEvent(_onManagerBPrepareAction);
            }
        }

        /// <summary>
        /// RPC called when engine changed field setup
        /// </summary>
        [PunRPC]
        public void RPC_ReadyForToss(eCoinSide pCoinSide)
        {
            //MatchEngine.Instance.Engine.TossMain.CoinSideSelected = pCoinSide;
            //_onTossSideSelected.Invoke();
        }

        /// <summary>
        /// RPC_TimerHasEllapsed
        /// </summary>
        [PunRPC]
        public void RPC_TimerHasEllapsed()
        {
            _RPC_TimerHasEllapsed_GE.Invoke();
        }

        /// <summary>
        /// RPC called when a manager is disconnected
        /// </summary>
        [PunRPC]
        public void RPC_ManagerIsDisconnected(eTeam pTeam)
        {

            #if DEVELOPMENT_BUILD
                Debug.LogFormat("<color=#DC143C>[PVP] " + pTeam.ToString()+ " Disconnected </color>");
            #endif


            if (
                   true == _isTeamAReconnected && eTeam.TEAM_A == UserData.Instance.PVPTeam ||
                   true == _isTeamBReconnected && eTeam.TEAM_B == UserData.Instance.PVPTeam
               )
            {
                if (eTeam.TEAM_A == UserData.Instance.PVPTeam)
                {
                    _isTeamAReconnected = false;
                    _isTeamADisconnected = true;
                }
                else
                {
                    _isTeamBReconnected = false;
                    _isTeamBDisconnected = true;
                }
            }

            if (pTeam == UserData.Instance.PVPTeam && null != NetworkUI.Instance)
            {
                NetworkUI.Instance.ShowNeedToReconnectGrp();

            }
            else if(null != NetworkUI.Instance)
            {
                StartCoroutine(NetworkUI.Instance.ShowIsDisconnectedGrp());
            }

        }


        /// <summary>
        /// RPC called when a manager is disconnected
        /// </summary>
        [PunRPC]
        public void RPC_ManagerIsReconnected(eTeam pTeam)
        {

#if DEVELOPMENT_BUILD
            Debug.LogFormat("<color=#DC143C>[PVP] " + pTeam.ToString()+ " Reconnected </color>");
#endif

            if (pTeam == UserData.Instance.PVPTeam && null != NetworkUI.Instance)
            {
                if (eTeam.TEAM_A == UserData.Instance.PVPTeam)
                {
                    _isTeamAReconnected = true;
                    _isTeamADisconnected = false;
                }
                else
                {
                    _isTeamBReconnected = true;
                    _isTeamBDisconnected = false;
                }
            }
            if (pTeam != UserData.Instance.PVPTeam && null != NetworkUI.Instance)
            {
                StartCoroutine(NetworkUI.Instance.ShowIsReconnectedGrp());
            }
        }

        /// <summary>
        /// RPC called when a manager is waiting opponent manager
        /// </summary>
        [PunRPC]
        public void RPC_IsWaitingManager(eTeam pTeam)
        {
             #if DEVELOPMENT_BUILD
                Debug.LogFormat("<color=#DC143C>[PVP] Waiting " + pTeam.ToString()+ "</color>");
            #endif

            if (pTeam != UserData.Instance.PVPTeam && null != NetworkUI.Instance)
            {
                NetworkUI.Instance.ShowIsWaitingGrp();
            }
        }

        /// <summary>
        /// RPC called when a manager is waiting opponent manager
        /// </summary>
        [PunRPC]
        public void RPC_NeedToSyncAllData(eTeam pTeam)
        {
        }

        #endregion
    }
}
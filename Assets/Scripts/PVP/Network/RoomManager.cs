﻿using Photon.Pun;
using UnityEngine;

namespace Sportfaction.CasualFantasy.PVP.Network
{
	/// <summary>
	/// Description: Class manage a photon room
	/// Author: Rémi Carreira
	/// </summary>
	public class RoomManager : MonoBehaviour
	{
		#region Public Methods

		/// <summary>
		/// Show the current photon room in the lobby
		/// </summary>
		public	void	ShowRoom()
		{
			if (true == PhotonNetwork.IsConnected && null != PhotonNetwork.CurrentRoom)
			{
				PhotonNetwork.CurrentRoom.IsVisible	=	true;
			}
		}

		/// <summary>
		/// Hide the current photon room from the lobby
		/// </summary>
		public	void	HideRoom()
		{
			if (true == PhotonNetwork.IsConnected && null != PhotonNetwork.CurrentRoom)
			{
				PhotonNetwork.CurrentRoom.IsVisible	=	false;
			}
		}

		/// <summary>
		/// Open the current photon room
		/// </summary>
		public	void	OpenRoom()
		{
			if (true == PhotonNetwork.IsConnected && null != PhotonNetwork.CurrentRoom)
			{
				PhotonNetwork.CurrentRoom.IsOpen	=	true;
			}
		}

		/// <summary>
		/// Close the current photon room
		/// </summary>
		public	void	CloseRoom()
		{
			if (true == PhotonNetwork.IsConnected && null != PhotonNetwork.CurrentRoom)
			{
				PhotonNetwork.CurrentRoom.IsOpen	=	false;
			}
		}

		#endregion
	}
}

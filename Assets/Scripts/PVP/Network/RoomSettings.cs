﻿using UnityEngine;

namespace Sportfaction.CasualFantasy.PVP.Network
{
	/// <summary>
	/// Description: Settings used when create a new room
	/// Author: Rémi Carreira
	/// </summary>
	[CreateAssetMenu(fileName = "New Room Settings", menuName = "CasualFantasy/Room Settings")]
	public class RoomSettings : ScriptableObject
	{
		#region Properties

		// Property used to know if room is visible by default
		public	bool	IsVisible		{ get { return _isVisible; } }
		// Property used to know if publish user Id
		public	bool	PublishUserId	{ get { return _publishUserId; } }
		// Property used to get the min players for a room
		public byte		MinPlayers		{ get { return _minPlayers; } }
		// Property used to get the max players for a room
		public byte		MaxPlayers		{ get { return _maxPlayers; } }
		// Property used to get the player time to live in milliseconds
		public int		PlayerTTL		{ get { return _playerTTL; } }
		// Property used to get the room time to live in milliseconds
		public int		RoomTTL			{ get { return _roomTTL; } }
		// Property used to get the name of the level to load
		public string[]	LevelName		{ get { return _levelName; } }

		// Property used to get the key for the game version
		public static string	KeyVersion			{ get { return _keyVersion; } }
		// Property used to get the key for the user level
		public static string	KeyLevel			{ get { return _keyLevel; } }
		// Property used to get the key for skill user level
		public static string	KeySkillLevel		{ get { return _keySkillLevel; } }
		// Property used to get the key for the level queue 
		public static string	KeyLevelQueue		{ get { return _keyLevelQueue; } }
		// Property used to get the key for the level index
		public static string	KeyLevelIndex		{ get { return _keyLevelIndex; } }
		// Property used to get the key used to know if is the first game of the master
		public static string	KeyMasterFirstGame	{ get { return	_keyMasterFirstGame; } }
		// Property used to get the key used to know if master already do a purchase
		public static string	KeyMasterPrenium	{ get { return _keyMasterPrenium; } }
		// Property used to get the key used to retrieve the master rank
		public static string	KeyMasterRank		{ get { return _keyMasterRank; } }

		#endregion

		#region Fields

		[SerializeField, Tooltip("Define if room is visible by default")]
		private	bool		_isVisible		=	true;
		[SerializeField, Tooltip("Define if user id are visible")]
		private	bool		_publishUserId	=	true;
		[SerializeField, Tooltip("Number of player required to start a room.")]
		private byte		_minPlayers		=	2;
		[SerializeField, Tooltip("Maximum number of player in one room.")]
		private byte		_maxPlayers		=	2;
		[SerializeField, Tooltip("Player time to live in milliseconds")]
		private int			_playerTTL		=	0;
		[SerializeField, Tooltip("Room time to live in milliseconds")]
		private int			_roomTTL		=	60000;
		[SerializeField, Tooltip("Name of the level to load")]
		private string[]	_levelName		=	new string[] { "Menu", "PVPGameplay" };

		private static  string  _keyVersion     = "C0"; // Game Version
		private static	string	_keyLevel		= "C1";	// User level key param
		private static	string	_keySkillLevel	= "C2";	// Skill User level key param
		private static	string	_keyLevelQueue	= "MQ";	// Level Queue key param
		private static	string	_keyLevelIndex	= "MI";	// Level Index key param

		private	static	string	_keyMasterFirstGame	= "Master-FirstGame";   // Key used to know if is the first game of the master
		private	static	string	_keyMasterPrenium	= "Master-Prenium";		// Key used to know if master already do a purchase
		private	static	string	_keyMasterRank		= "Master-Rank";		// Key used to retrieve the master rank

		#endregion
	}
}
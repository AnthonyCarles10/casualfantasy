﻿using Sportfaction.CasualFantasy.Manager.Profile;
using Sportfaction.CasualFantasy.Events;

using SportFaction.CasualFootballEngine.TeamService.Enum;

using UnityEngine.Assertions;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;

namespace Sportfaction.CasualFantasy.PVP.Network
{
	/// <summary>
	/// Class handling players disconnection
	/// </summary>
	public class PlayersDisconnection : MonoBehaviourPunCallbacks
	{
		#region Events

		[Header("Events")]
		[SerializeField, Tooltip("Game event to invoke when master disconnect from the current room")]
		private	GameEvent	_onMasterDisconnected	=	null;
		[SerializeField, Tooltip("Game event to invoke when client disconnect from the current room")]
		private	GameEvent	_onClientDisconnected	=	null;

		#endregion

		#region Public Methods

		/// <summary>
		/// Called when a remote player left the room. This PhotonPlayer is already removed from the playerlist at this time.
		/// </summary>
		/// <param name="pOtherPlayer">Player disconnected</param>
		public override void	OnPlayerLeftRoom(Player pOtherPlayer)
		{
			if (null != UserData.Instance)
			{
				if (eTeam.TEAM_A == UserData.Instance.PVPTeam)
				{
					InvokeGameEvent(_onClientDisconnected);
				}
				else
				{
					InvokeGameEvent(_onMasterDisconnected);
				}
			}
		}

		#endregion

		#region Private Methods

		/// <summary>
		/// Invoke a game event
		/// </summary>
		/// <param name="pEvent">Specific game event to invoke</param>
		private	void	InvokeGameEvent(GameEvent pEvent)
		{
			Assert.IsNotNull(pEvent, "[PlayersDisconnection] OnPhotonPlayerDisconnected(), pEvent is null.");

			pEvent.Invoke();
		}

		#endregion
	}
}

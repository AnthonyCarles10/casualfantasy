﻿namespace Sportfaction.CasualFantasy.PVP.Network
{
    /// <summary>
    /// The map queue is defined as a list of this struct
    /// It simply holds the name of a level and a gamemode
    /// </summary>
    [System.Serializable]
    public struct LevelQueueEntry
    {
        public string Name;

        public static LevelQueueEntry None
        {
            get
            {
                return new LevelQueueEntry { Name = "" };
            }
        }

        public override bool Equals( object obj )
        {
            if( obj is LevelQueueEntry )
            {
                LevelQueueEntry otherEntry = (LevelQueueEntry)obj;

                return Name == otherEntry.Name;
            }

            return false;
        }

        public override int GetHashCode()
        {
            return Name.GetHashCode();
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
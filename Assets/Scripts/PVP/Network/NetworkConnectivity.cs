﻿using UnityEngine;
using System.Collections;

#if USES_NETWORK_CONNECTIVITY
namespace Sportfaction.CasualFantasy.Services
{
    /// <summary>
    /// Provides cross-platform interface to check network connectivity status.
    /// </summary>
    public class NetworkConnectivity : Singleton<NetworkConnectivity>
    {
        #region Properties

        /// <summary>
        /// A bool value that indicates connectivity status.
        /// </summary>
        /// <value><c>true</c> if connected to network; otherwise, <c>false</c>.</value>
        public bool IsConnected
        {
            get;
            protected set;
        }

        #endregion

        #region API Methods

        /// <summary>
        /// Initialises the component with the configuration values set in <b>Network Connectivity Settings</b>.
        /// </summary>
        ///	<remarks> 
        /// \note You need to call this method, to start checking if IP Address specified in the <b>Network Connectivity Settings</b> is reachable or not. 
        /// </remarks>
        public virtual void Initialise()
        {
            StartCoroutine(ManuallyTriggerInitialState());
        }

        #endregion

        #region Misc. Methods

        private IEnumerator ManuallyTriggerInitialState()
        {
            yield return new WaitForSeconds(1f);

            if (IsConnected == false)
                ConnectivityChanged(false);
        }

        #endregion

        #region Delegates

        ///	<summary>
        ///	Delegate that will be called whenever connectivity state changes.
        ///	</summary>
        ///	<param name="_isConnected"><c>true</c> if connected to network; otherwise, <c>false</c>.</param>
        public delegate void NetworkConnectivityChanged(bool _isConnected);

        #endregion

        #region Events

        /// <summary>
        /// Event that will be called whenever connectivity state changes.
        /// </summary>
        /// <example>
        /// The following code example demonstrates how to use this event.
        /// <code>
        /// using UnityEngine;
        /// using System.Collections;
        /// using VoxelBusters.NativePlugins;
        /// 
        /// public class ExampleClass : MonoBehaviour 
        /// {
        /// 	private void OnEnable ()
        /// 	{
        /// 		// Registering for event
        /// 	    NetworkConnectivity.NetworkConnectivityChangedEvent	+= OnNetworkConnectivityChanged;
        ///     }
        /// 
        /// 	private void OnDisable ()
        /// 	{
        /// 		// Unregistering event
        /// 	    NetworkConnectivity.NetworkConnectivityChangedEvent	-= OnNetworkConnectivityChanged;
        /// 	}
        /// 
        /// 	private void OnNetworkConnectivityChanged (bool _isConnected)
        /// 	{
        /// 		if (_isConnected)
        /// 		{
        /// 			// Handle when app goes online
        /// 		}
        /// 		else
        /// 		{
        /// 			// Handle when app goes offline
        /// 		}
        /// 	}
        /// }
        /// </code>
        /// </example>
        public static event NetworkConnectivityChanged NetworkConnectivityChangedEvent;

        #endregion

        #region Native Callback Methods

        protected void ConnectivityChanged(string _newstate)
        {
            bool _isConnected = bool.Parse(_newstate);
            ConnectivityChanged(_isConnected);
        }

        protected void ConnectivityChanged(bool _connected)
        {
            IsConnected = _connected;

            // Trigger event in handler
            if (NetworkConnectivityChangedEvent != null)
                NetworkConnectivityChangedEvent(IsConnected);
        }

        #endregion
    }
}
#endif
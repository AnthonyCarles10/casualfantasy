﻿using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

namespace Sportfaction.CasualFantasy.PVP.Network
{
    public class LevelQueue
    {
        /// <summary>
        /// Converts a level queue into a string which can be shared by Photon
        /// </summary>
        /// <param name="levelQueue">The level queue.</param>
        /// <returns>
        /// A string representation of the level queue that can be shared and turned back into a list by other clients
        /// </returns>
        public static string ListToString(List<LevelQueueEntry> levelQueue)
        {
            //Create an array of strings that each represent one entry of the level queue
            string[] levelSegments = new string[levelQueue.Count];

            //Convert each entry into its string representation
            for (int i = 0; i < levelQueue.Count; ++i)
            {
                levelSegments[i] = EntryToString(levelQueue[i]);
            }

            //Glue everything together into one big string
            return string.Join("~", levelSegments);
        }

        /// <summary>
        /// Convert a level queue string back into a list
        /// </summary>
        /// <param name="levelQueueString">The level queue string.</param>
        /// <returns></returns>
        public static List<LevelQueueEntry> StringToList(string levelQueueString)
        {
            List<LevelQueueEntry> levelQueue = new List<LevelQueueEntry>();

            //First, split the big string into it's entry segments
            string[] levelSegments = levelQueueString.Split('~');

            for (int i = 0; i < levelSegments.Length; ++i)
            {
                //And then convert each segment into a level queue entry
                LevelQueueEntry newQueueEntry = StringToEntry(levelSegments[i]);

                levelQueue.Add(newQueueEntry);
            }

            return levelQueue;
        }

        /// <summary>
        /// Convert a single level queue entry string to a LevelQueueEntry object
        /// </summary>
        /// <param name="levelQueueEntryString">The level queue entry string.</param>
        /// <returns></returns>
        public static LevelQueueEntry StringToEntry(string levelQueueEntryString)
        {
            string[] levelData = levelQueueEntryString.Split('#');

            LevelQueueEntry queueEntry = new LevelQueueEntry
            {
                Name = levelData[0],
            };

            return queueEntry;
        }

        /// <summary>
        /// Convert a single LevelQueueEntry to a string representation
        /// </summary>
        /// <param name="entry">The entry.</param>
        /// <returns></returns>
        public static string EntryToString(LevelQueueEntry entry)
        {
            return entry.Name;
        }

        /// <summary>
        /// Gets the length of the current level queue.
        /// </summary>
        /// <returns>Number of entries in the current level queue</returns>
        public static int GetCurrentLevelQueueLength()
        {
            if (PhotonNetwork.CurrentRoom == null)
            {
                Debug.LogError("Can't get current level if player hasn't joined a room yet.");
                return 0;
            }

            if (PhotonNetwork.CurrentRoom.CustomProperties == null)
            {
                Debug.LogError("Current room doesn't have custom properties. Can't find current level.");
                return 0;
            }

            if (PhotonNetwork.CurrentRoom.CustomProperties.ContainsKey(RoomSettings.KeyLevelQueue) == false)
            {
                Debug.LogError("Couldn't find level queue in room properties.");
                return 0;
            }

            string levelQueueString = (string)PhotonNetwork.CurrentRoom.CustomProperties[RoomSettings.KeyLevelQueue];
            string[] levelSegments = levelQueueString.Split('~');

            return levelSegments.Length;
        }

        /// <summary>
        /// Gets a single entry in a level queue string.
        /// </summary>
        /// <param name="levelQueueString">The level queue string.</param>
        /// <param name="levelIndex">Index of the entry we want to retrieve.</param>
        /// <returns></returns>
        public static LevelQueueEntry GetSingleEntryInLevelQueue(string levelQueueString, int levelIndex)
        {
            string[] levelSegments = levelQueueString.Split('~');

            return StringToEntry(levelSegments[levelIndex % levelSegments.Length]);
        }

        /// <summary>
        /// Gets the level queue entry currently running in the joined room
        /// </summary>
        /// <returns></returns>
        public static LevelQueueEntry GetCurrentLevel()
        {
            return GetRoomLevel(0);
        }

        /// <summary>
        /// Gets a level queue entry of the currently joined room
        /// </summary>
        /// <param name="levelIndexOffset">The offset of the level index we want to get, relative to the currently played one</param>
        /// <returns></returns>
        public static LevelQueueEntry GetRoomLevel(int levelIndexOffset)
        {
            //return ("Level" + levelIndexOffset);
            if (PhotonNetwork.CurrentRoom == null)
            {
                Debug.LogError("Can't get current level if player hasn't joined a room yet.");
                return LevelQueueEntry.None;
            }

            if (PhotonNetwork.CurrentRoom.CustomProperties == null)
            {
                Debug.LogError("Current room doesn't have custom properties. Can't find current level.");
                return LevelQueueEntry.None;
            }

            if (PhotonNetwork.CurrentRoom.CustomProperties.ContainsKey(RoomSettings.KeyLevelQueue) == false)
            {
                Debug.LogError("Couldn't find level queue in room properties.");
                return LevelQueueEntry.None;
            }

            if (PhotonNetwork.CurrentRoom.CustomProperties.ContainsKey(RoomSettings.KeyLevelIndex) == false)
            {
                Debug.LogError("Couldn't find level index in room properties.");
                return LevelQueueEntry.None;
            }

            string levelQueueString = (string)PhotonNetwork.CurrentRoom.CustomProperties[RoomSettings.KeyLevelQueue];
            int levelIndex = (int)PhotonNetwork.CurrentRoom.CustomProperties[RoomSettings.KeyLevelIndex] + levelIndexOffset - 1;

            return LevelQueue.GetSingleEntryInLevelQueue(levelQueueString, levelIndex);
        }
    }
}
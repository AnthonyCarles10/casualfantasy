﻿using UnityEngine.Assertions;
using UnityEngine;
using System;

using Photon.Pun;

namespace Sportfaction.CasualFantasy.PVP.Network
{
	/// <summary>
	/// Description: Class handles master client timeout
	/// Author: Rémi Carreira
	/// </summary>
	[RequireComponent(typeof(PhotonView))]
	public class MasterClientTimeoutDetection : MonoBehaviour
	{
		#region Variables

		[Header("Parameters")]
		[SerializeField, MinValue(0), Tooltip("Interval (in seconds) between two checks")]
		private	int			_checkInterval		=	500;
		[SerializeField, MinValue(0), Tooltip("Maximum delay before detect a timeout")]
		private	int			_maxTimeoutDelay	=	500;
		[SerializeField, MinValue(0), Tooltip("Maximum ping from my device to photon server")]
		private	int			_maxSelfPing		=	500;
		[SerializeField, Tooltip("Photon component used to send rpc")]
		private	PhotonView	_photonView			=	null;

		#pragma warning disable 0414
		[Header("Informations")]
		[SerializeField, ReadOnly, Tooltip("Is master ping?")]
		private	bool		_pingMaster				=	false;
		[SerializeField, ReadOnly, Tooltip("My current roundtrip time to the photon server")]
		private	int			_selfPing				=	0;
		[SerializeField, ReadOnly, Tooltip("Current interval according to the last ping")]
		private	int			_curInterval			=	0;
		[SerializeField, ReadOnly, Tooltip("Delay of the current ping")]
		private	int			_pingDelay				=	0;
		[SerializeField, ReadOnly, Tooltip("Timestamp of the last ping")]
		private	int			_pingTimestamp			=	0;
		[SerializeField, ReadOnly, Tooltip("Current photon server timestamp")]
		private	int			_curServerTimestamp		=	0;
		[SerializeField, ReadOnly, Tooltip("Last photon server timestamp")]
		private	int			_lastServerTimestamp	=	0;
		[SerializeField, ReadOnly, Tooltip("Delta photon server timestamp")]
		private	int			_deltaServerTimestamp	=	0;
		#pragma warning restore 0414

		#endregion

		#region Public Methods

		/// <summary>
		/// Ping master
		/// </summary>
		public	void	PingMaster()
		{
			if (true == PhotonNetwork.IsConnected && false == PhotonNetwork.IsMasterClient)
			{
				_curInterval	=	0;
				_pingMaster		=	true;
				_pingTimestamp	=	PhotonNetwork.ServerTimestamp;

				Assert.IsNotNull(_photonView, "[MasterClientTimeoutDetection] PingMaster(), _photonView is null.");

				_photonView.RPC("RPC_PingMaster", RpcTarget.MasterClient, PhotonNetwork.ServerTimestamp);
			}
		}

		/// <summary>
		/// Ping client
		/// </summary>
		public	void	PingClient()
		{
			if (true == PhotonNetwork.IsConnected && true == PhotonNetwork.IsMasterClient)
			{
				Assert.IsNotNull(_photonView, "[MasterClientTimeoutDetection] PingClient(), _photonView is null.");

				_photonView.RPC("RPC_PingClient", RpcTarget.Others, PhotonNetwork.ServerTimestamp);
			}
		}

		#endregion

		#region Private Methods

		/// <summary>
		/// MonoBehaviour used to disable this script if not connected to photon
		/// </summary>
		private	void	OnEnable()
		{
			_pingMaster		=	false;
			_curInterval	=	0;
			_pingTimestamp	=	0;
			_pingDelay		=	0;

			if (false == PhotonNetwork.IsConnected)
			{
				this.enabled = false;
			}
			else
			{
				_curServerTimestamp		=	PhotonNetwork.ServerTimestamp;
				_lastServerTimestamp	=	_curServerTimestamp;
				_deltaServerTimestamp	=	0;
			}
		}

		/// <summary>
		/// MonoBehaviour used to detect timeout
		/// </summary>
		private	void	Update()
		{
			Assert.IsTrue(PhotonNetwork.IsConnected, "[MasterClientTimeoutDetection] Update(), user not connected to photon.");

			if (false == PhotonNetwork.IsMasterClient)
			{
				UpdateSelfPing();

				UpdateTimestampValues();

				CheckSelfPing();

				CheckMasterPing();
			}
		}

		/// <summary>
		/// Update self ping value
		/// </summary>
		private	void	UpdateSelfPing()
		{
			_selfPing	=	PhotonNetwork.GetPing();
		}

		/// <summary>
		/// Update timestamp values
		/// </summary>
		private	void	UpdateTimestampValues()
		{
			_curServerTimestamp		=	PhotonNetwork.ServerTimestamp;
			_deltaServerTimestamp	=	0;

			if (_curServerTimestamp < _lastServerTimestamp)
			{
				_deltaServerTimestamp	+=	Int32.MaxValue - _lastServerTimestamp;
				_lastServerTimestamp	=	Int32.MinValue;
			}

			_deltaServerTimestamp	+=	_curServerTimestamp - _lastServerTimestamp;
			_lastServerTimestamp	=	_curServerTimestamp;
		}

		/// <summary>
		/// Check self ping
		/// </summary>
		private	void	CheckSelfPing()
		{
			if (_maxSelfPing <= _selfPing)
			{
				_pingMaster		=	false;
				_curInterval	=	0;
				_pingDelay		=	0;
				_pingTimestamp	=	0;
			}
		}

		/// <summary>
		/// Check master ping
		/// </summary>
		private	void	CheckMasterPing()
		{
			if (false == _pingMaster)
			{
				_curInterval	+=	_deltaServerTimestamp;

				if (_curInterval >= _checkInterval)
				{
					PingMaster();
				}
			}
			else
			{
				_pingDelay	+=	_deltaServerTimestamp;

				if (_pingDelay >= _maxTimeoutDelay)
				{
					ChangeMaster();
				}
			}
		}

		/// <summary>
		/// Change current master by a client
		/// </summary>
		private	void	ChangeMaster()
		{
			if (true == PhotonNetwork.IsConnected && false == PhotonNetwork.IsMasterClient)
			{
				_pingMaster	=	false;
				_pingDelay	=	0;

				Assert.IsNotNull(PhotonNetwork.LocalPlayer, "[MasterClientTimeoutDetection] ChangeMaster(), PhotonNetwork.LocalPlayer is null.");

				PhotonNetwork.SetMasterClient(PhotonNetwork.LocalPlayer);
			}
		}

		#region Network Methods

		/// <summary>
		/// RPC used to ping master
		/// </summary>
		/// <param name="pTimestamp">Timestamp of this RPC</param>
		[PunRPC]
		private	void	RPC_PingMaster(object pTimestamp)
		{
			_pingTimestamp	=	(int)pTimestamp;

			PingClient();
		}

		/// <summary>
		/// RPC used to ping client
		/// </summary>
		/// <param name="pTimestamp">Timestamp of this RPC</param>
		[PunRPC]
		private	void	RPC_PingClient(object pTimestamp)
		{
			_pingMaster		=	false;
			_pingDelay		=	0;
			_pingTimestamp	=	0;
		}

		#endregion

		#endregion
	}
}

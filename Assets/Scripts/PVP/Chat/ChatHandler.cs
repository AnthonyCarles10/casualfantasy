﻿using Photon.Pun;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.EventSystems;

namespace Sportfaction.CasualFantasy.PVP.Chat
{
	/// <summary>
	/// Description: Class handling chat functionalities
	/// Author: Rémi Carreira
	/// </summary>
	[RequireComponent(typeof(PhotonView))]
	public class ChatHandler : MonoBehaviourPun
	{
		#region Fields

		[SerializeField, Tooltip("Frame displaying choice bubbles")]
		private	ChatFrame			_frame			=	null;
		[SerializeField, Tooltip("Bubble used to display my sentence")]
		private	ChatBubble			_myBubble		=	null;
		[SerializeField, Tooltip("Bubble used to display opponent sentence")]
		private	ChatBubble			_opponentBubble	=	null;

		#endregion

		#region Public Methods

		/// <summary>
		/// Callback invoke when chat button was clicked
		/// </summary>
		public	void	OnChatButtonClicked()
		{
			Assert.IsNotNull(_frame, "[ChatHandler] OnChatButtonClicked(), _frame is null.");

			if (false == _frame.IsVisible() || true == _frame.IsHidding)
			{
				if (false == _frame.IsShowing)
				{
					_frame.ShowFrame();
				}
			}
			else if (true == _frame.IsVisible() || true == _frame.IsShowing)
			{
				if (false == _frame.IsHidding)
				{
					_frame.HideFrame();
				}
			}
		}

		/// <summary>
		/// Callback invoke when a chat bubble choice was clicked
		/// </summary>
		/// <param name="pChatBubbleChoice">Chat bubble choice</param>
		public	void	OnChatBubbleChoiceWasClicked(ChatBubbleChoice pChatBubbleChoice)
		{
			Assert.IsNotNull(pChatBubbleChoice, "[ChatHandler] OnChatBubbleChosen(), pChatBubbleChoice is null.");

			DisplaySentence(pChatBubbleChoice.SentenceKey);

			Assert.IsNotNull(_frame, "[ChatHandler] OnChatBubbleChosen(), _frame is null.");

			if (false == _frame.IsHidding)
			{
				_frame.HideFrame();
			}
		}

		/// <summary>
		/// Display sentence
		/// </summary>
		/// <param name="pSentenceKey">Key of the sentence to display</param>
		public	void	DisplaySentence(string pSentenceKey)
		{
			if (true == PhotonNetwork.IsConnected)
			{
				Assert.IsNotNull(photonView, "[ChatHandler] DisplaySentence(), photonView is null.");

				photonView.RPC("RPC_DisplaySentence", RpcTarget.All, pSentenceKey, PhotonNetwork.IsMasterClient);
			}
			else
			{
				ShowMySentence(pSentenceKey);
			}
		}

		#endregion

		#region Private Methods

		/// <summary>
		/// Monobehaviour method used to handle inputs
		/// </summary>
		private	void	Update()
		{

			#if UNITY_EDITOR || UNITY_STANDALONE

			if (true == Input.GetMouseButtonDown(0) && false == IsPointerOverChat())
			{
				Assert.IsNotNull(_frame, "[ChatHandler] Update(), _frame is null.");

				if (true == _frame.IsVisible() && false == _frame.IsShowing && false == _frame.IsHidding)
				{
					_frame.HideFrame();
				}
			}

			#else

			if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began && false == IsPointerOverChat())
			{
				Assert.IsNotNull(_frame, "[ChatHandler] Update(), _frame is null.");

				if (true == _frame.IsVisible() && false == _frame.IsShowing && false == _frame.IsHidding)
				{
					_frame.HideFrame();
				}
			}

			#endif

		}

		/// <summary>
		/// Display my sentence
		/// </summary>
		/// <param name="pSentenceKey">Key of the sentence to display</param>
		private	void	ShowMySentence(string pSentenceKey)
		{
			Assert.IsNotNull(_myBubble, "[ChatHandler] DisplayMySentence(), _myBubble is null.");

			_myBubble.SetSentence(pSentenceKey);

			_myBubble.ShowBubble();
		}

		/// <summary>
		/// Display opponent sentence
		/// </summary>
		/// <param name="pSentenceKey">Key of the sentence to display</param>
		private	void	ShowOpponentSentence(string pSentenceKey)
		{
			Assert.IsNotNull(_opponentBubble, "[ChatHandler] DisplayOpponentSentence(), _opponentBubble is null.");

			_opponentBubble.SetSentence(pSentenceKey);

			_opponentBubble.ShowBubble();
		}

		/// <summary>
		/// Check if pointer is over chat
		/// </summary>
		/// <returns>True if pointer is over chat</returns>
		private	bool	IsPointerOverChat()
		{
			if (null != EventSystem.current)
			{
				if (null != EventSystem.current.currentSelectedGameObject && true == EventSystem.current.currentSelectedGameObject.tag.Equals("Chat"))
				{
					return true;
				}
			}
			return false;
		}

		#region Network Methods

		/// <summary>
		/// RPC called to display the sentence
		/// </summary>
		/// <param name="pKey"></param>
		/// <param name="pFromMaster"></param>
		[PunRPC]
		private	void	RPC_DisplaySentence(object pKey, object pFromMaster)
		{
			if (true == PhotonNetwork.IsConnected)
			{
				if (true == (bool)pFromMaster)
				{
					if (true == PhotonNetwork.IsMasterClient)
					{
						ShowMySentence(pKey as string);
					}
					else
					{
						ShowOpponentSentence(pKey as string);
					}
				}
				else
				{
					if (true == PhotonNetwork.IsMasterClient)
					{
						ShowOpponentSentence(pKey as string);
					}
					else
					{
						ShowMySentence(pKey as string);
					}
				}
			}
			else
			{
				ShowMySentence(pKey as string);
			}
		}

		#endregion

		#endregion
	}
}

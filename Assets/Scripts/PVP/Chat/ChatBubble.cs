﻿using DG.Tweening;
using I2.Loc;
using TMPro;
using UnityEngine;
using UnityEngine.Assertions;

namespace Sportfaction.CasualFantasy.PVP.Chat
{
	/// <summary>
	/// Description: Class showing the sentence
	/// Author: Rémi Carreira
	/// </summary>
	public class ChatBubble : MonoBehaviour
	{
		#region Public Methods

		// Property used to know if is showing chat bubble
		public	bool	IsShowing	{ get { return _isShowing; } }
		// Property used to know if is hidding chat bubble
		public	bool	IsHidding	{ get { return _isHidding; } }

		#endregion

		#region Fields

		[SerializeField, Tooltip("Canvas group use to show / hide the bubble")]
		private	CanvasGroup			_bubbleGroup		=	null;
		[SerializeField, Tooltip("Text used to display the sentence")]
		private	TextMeshProUGUI		_textMesh			=	null;
		[SerializeField, MinValue(0f), Tooltip("Duration before hide the bubble")]
		private	float				_duration			=	0f;
		[SerializeField, MinValue(0f), Tooltip("Duration to fade in the chat bubble")]
		private	float				_fadeInDuration		=	0f;
		[SerializeField, MinValue(0f), Tooltip("Duration to fade out the chat bubble")]
		private	float				_fadeOutDuration	=	0f;

		[SerializeField, ReadOnly, Tooltip("Is showing chat bubble?")]
		private	bool	_isShowing	=	false;
		[SerializeField, ReadOnly, Tooltip("Is hidding chat bubble ?")]
		private	bool	_isHidding	=	false;

		#endregion

		#region Public Methods

		/// <summary>
		/// Show the bubble
		/// </summary>
		/// <param name="pSmooth">Is showing smmothly</param>
		public	void	ShowBubble(bool pSmooth = true)
		{
			CancelInvoke();

			_isShowing	=	true;
			_isHidding	=	false;

			Assert.IsNotNull(_bubbleGroup, "[ChatBubble] ShowBubble(), _bubbleGroup is null.");

			_bubbleGroup.gameObject.SetActive(true);

			_bubbleGroup.DOKill();

			if (true == pSmooth)
			{
				_bubbleGroup.DOFade(1f, _fadeInDuration).OnComplete(OnBubbleShown);
			}
			else
			{
				_bubbleGroup.alpha	=	1f;

				OnBubbleShown();
			}
		}

		/// <summary>
		/// Hide the bubble
		/// </summary>
		/// <param name="pSmooth">Is hidding smoothly?</param>
		public	void	HideBubble(bool pSmooth = true)
		{
			CancelInvoke();

			_isShowing	=	false;
			_isHidding	=	true;

			Assert.IsNotNull(_bubbleGroup, "[ChatBubble] HideBubble(), _bubbleGroup is null.");

			_bubbleGroup.interactable	=	false;

			if (true == pSmooth)
			{
				_bubbleGroup.DOFade(0f, _fadeOutDuration).OnComplete(OnBubbleHidden);
			}
			else
			{
				_bubbleGroup.alpha	=	0f;

				OnBubbleHidden();
			}
		}

		/// <summary>
		/// Set the sentence of the bubble
		/// </summary>
		/// <param name="lKey">Key of the sentence to display</param>
		public	void	SetSentence(string lKey)
		{
			Assert.IsFalse(string.IsNullOrEmpty(lKey), "[ChatBubble] ShowBubble(), lSentenceKey is null.");

			Assert.IsNotNull(_textMesh, "[ChatBubble] ShowBubble(), _textMesh is null.");

			_textMesh.text = LocalizationManager.GetTranslation(lKey);
		}

		#endregion

		#region Private Methods
		
		/// <summary>
		/// Callback invoke when bubble was shown
		/// </summary>
		private	void	OnBubbleShown()
		{
			_isShowing	=	false;

			Assert.IsNotNull(_bubbleGroup, "[ChatBubble] OnBubbleShown(), _bubbleGroup is null.");

			_bubbleGroup.interactable	=	true;

			Invoke("OnDurationFinished", _duration);
		}

		/// <summary>
		/// Callback invoke when bubble was hidden
		/// </summary>
		private	void	OnBubbleHidden()
		{
			_isHidding	=	false;

			Assert.IsNotNull(_bubbleGroup, "[ChatBubble] OnBubbleHidden(), _bubbleGroup is null.");

			_bubbleGroup.gameObject.SetActive(false);
		}

		/// <summary>
		/// Callback invoke when duration finished
		/// </summary>
		private	void	OnDurationFinished()
		{
			HideBubble();
		}

		#endregion
	}
}

﻿using I2.Loc;
using TMPro;
using UnityEngine;
using UnityEngine.Assertions;

namespace Sportfaction.CasualFantasy.PVP.Chat
{
	/// <summary>
	/// Description: Bubble used to choice chat sentence
	/// Author: Rémi Carreira
	/// </summary>
	public class ChatBubbleChoice : MonoBehaviour
	{
		#region Properties

		// Property used to get the sentence key
		public	string		SentenceKey { get { return _sentenceKey; } }
		// Property used to get the sentence
		public	string		Sentence	{ get { return _sentence; } }

		#endregion

		#region Fields

		[SerializeField, Tooltip("Mesh used to display the text")]
		private	TextMeshProUGUI	_textMesh		=	null;
		[SerializeField, ReadOnly, Tooltip("Key of the sentence to display")]
		private	string			_sentenceKey	=	"";
		[SerializeField, ReadOnly, Tooltip("Sentence to display")]
		private	string			_sentence		=	"";

		#endregion

		#region Public Methods

		/// <summary>
		/// Set text displayed in the chat bubble choice
		/// </summary>
		/// <param name="pSentenceKey">Key of the sentence</param>
		public	void	SetText(string pSentenceKey)
		{
			Assert.IsFalse(string.IsNullOrEmpty(pSentenceKey), "[ChatBubbleChoice] SetText(), pSentenceKey is null.");

			_sentenceKey	=	pSentenceKey;

			_sentence		=	LocalizationManager.GetTranslation(_sentenceKey);

			Assert.IsFalse(string.IsNullOrEmpty(_sentence), "[ChatBubbleChoice] SetText(), _sentence is null.");

			Assert.IsNotNull(_textMesh, "[ChatBubbleChoice] SetText(), _textMesh is null.");

			_textMesh.text	=	_sentence;
		}

		#endregion
	}
}

﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;

namespace Sportfaction.CasualFantasy.PVP.Chat
{
    /// <summary>
    /// Description: Class handling chat frame
    /// Author: Rémi Carreira
    /// </summary>
    public class ChatFrame : MonoBehaviour
	{
		#region Properties

		// Property used to know if is showing the chat frame
		public bool		IsShowing	{ get { return _isShowing; } }
		// Property used to know if is hidding the chat frame
		public	bool	IsHidding	{ get { return _isHidding; } }

		#endregion

		#region Fields

		[SerializeField, MinValue(0f), Tooltip("Duration to fade in the canvas group")]
		private	float				_fadeInDuration		=	1f;
		[SerializeField, MinValue(0f), Tooltip("Duration to fade out the canvas group")]
		private	float				_fadeOutDuration	=	1f;
		[SerializeField, Tooltip("Canvas group used to display/hide frame")]
		private	CanvasGroup			_frameGroup		=	null;
		[SerializeField, Tooltip("Component used to scroll the content")]
		private	ScrollRect			_scrollRect		=	null;
		[SerializeField, Tooltip("Array of key displayed in bubble choice")]
		private	string[]			_bubblesKey		=	new string[0];

		[Header("Informations")]
		[SerializeField, ReadOnly, Tooltip("Is showing frame?")]
		private	bool					_isShowing		=	false;
		[SerializeField, ReadOnly, Tooltip("Is hidding frame?")]
		private	bool					_isHidding		=	false;
		[SerializeField, ReadOnly, Tooltip("Previous scroll position")]
		private	Vector3					_prevScrollPos	=	Vector3.zero;
		[SerializeField, ReadOnly, Tooltip("Current scroll position")]
		private	Vector3					_curScrollPos	=	Vector3.zero;
		[SerializeField, ReadOnly, Tooltip("Transform of the content")]
		private	RectTransform			_content		=	null;
		[SerializeField, ReadOnly, Tooltip("Array contains all bubbles")]
		private	ChatBubbleChoice[]		_bubbles		=	new ChatBubbleChoice[0];

		[Header("First Child")]
		[SerializeField, ReadOnly, Tooltip("First child transform")]
		private	RectTransform		_firstChildTransform	=	null;
		[SerializeField, ReadOnly, Tooltip("First child bubble")]
		private	ChatBubbleChoice	_firstChildBubble		=	null;

		[Header("Second Child")]
		[SerializeField, ReadOnly, Tooltip("Last child transform")]
		private	RectTransform		_lastChildTransform		=	null;
		[SerializeField, ReadOnly, Tooltip("Last child bubble")]
		private	ChatBubbleChoice	_lastChildBubble		=	null;

		private	float		_scrollRectTop			=	0f;				// Top of the scroll rect
		private	float		_scrollRectBottom		=	0f;				// Bottom of the scroll rect
		private	float		_firstChildBottom		=	0f;				// Bottom of the first child
		private	float		_lastChildTop			=	0f;             // Top of the last child
		private	int			_topSentenceIndex		=	0;				// Index of the top bubble sentence
		private	int			_bottomSentenceIndex	=	0;				// Index of the bottom bubble sentence
		//private	Vector3[]	_worldVectors			=	new Vector3[4];	// Array of vectors

		#endregion

		#region Public Methods

		/// <summary>
		/// Show frame
		/// </summary>
		/// <param name="pSmooth">Is showing smoothly?</param>
		public	void	ShowFrame(bool pSmooth = true)
		{
			_isShowing	=	true;
			_isHidding	=	false;

			Assert.IsNotNull(_frameGroup, "[ChatFrame] ShowFrame(), _frameGroup is null.");

			_frameGroup.gameObject.SetActive(true);

			_frameGroup.DOKill();

			if (true == pSmooth)
			{
				_frameGroup.DOFade(1f, _fadeInDuration).OnComplete(OnFrameShown);
			}
			else
			{
				_frameGroup.alpha	=	1f;

				OnFrameShown();
			}
		}

		/// <summary>
		/// Hide frame
		/// </summary>
		/// <param name="pSmooth">Is hidding smoothly?</param>
		public	void	HideFrame(bool pSmooth = true)
		{
			_isShowing	=	false;
			_isHidding	=	true;

			Assert.IsNotNull(_frameGroup, "[ChatFrame] ShowFrame(), _frameGroup is null.");

			_frameGroup.interactable	=	false;

			_frameGroup.DOKill();

			if (true == pSmooth)
			{
				_frameGroup.DOFade(0f, _fadeOutDuration).OnComplete(OnFrameHidden);
			}
			else
			{
				_frameGroup.alpha	=	0f;

				OnFrameHidden();
			}
		}

		/// <summary>
		/// Is chat frame visible?
		/// </summary>
		/// <returns>A boolean</returns>
		public	bool	IsVisible()
		{
			Assert.IsNotNull(_frameGroup, "[ChatFrame] ShowFrame(), _frameGroup is null.");

			return _frameGroup.gameObject.activeSelf;
		}

		#endregion

		#region Private Methods

		/// <summary>
		/// Monobehaviour method used for initialization
		/// </summary>
		private	void	Awake()
		{
			InitScrollRect();

			InitBubblesChoice();

			InitializeFirstAndLastChild();
		}

		/// <summary>
		/// Initialize scroll rect
		/// </summary>
		private	void	InitScrollRect()
		{
			Assert.IsNotNull(_scrollRect, "[ChatFrame] InitScrollRect(), _scrollRect is null.");

			_content			=	_scrollRect.content;

			_scrollRect.movementType	=	ScrollRect.MovementType.Unrestricted;
			_scrollRect.horizontal		=	false;
			_scrollRect.vertical		=	true;

			_scrollRect.onValueChanged.AddListener(OnScroll);

			RectTransform	_scrollRectTransform	=	_scrollRect.GetComponent<RectTransform>();

			Assert.IsNotNull(_scrollRectTransform, "[ChatFrame] InitScrollRect(), _scrollRectTransform is null.");

			_scrollRectTop		=	_scrollRectTransform.TransformPoint(0f, _scrollRectTransform.rect.yMax, 0f).y;
			_scrollRectBottom	=	_scrollRectTransform.TransformPoint(0f, _scrollRectTransform.rect.yMin, 0f).y;
		}

		/// <summary>
		/// Initialize bubbles choice
		/// </summary>
		private	void	InitBubblesChoice()
		{
			Assert.IsNotNull(_bubblesKey, "[ChatFrame] InitBubblesChoice(), _bubblesKey is null.");

			Assert.IsNotNull(_scrollRect, "[ChatFrame] InitBubblesChoice(), _scrollRect is null.");

			_bubbles	=	_scrollRect.GetComponentsInChildren<ChatBubbleChoice>();

			Assert.IsNotNull(_bubbles, "[ChatFrame] InitBubblesChoice(), _bubbles is null.");

			int	lLength	=	(_bubblesKey.Length <= _bubbles.Length) ? _bubblesKey.Length : _bubbles.Length;

			for (int lIndex = 0; lIndex < lLength; ++lIndex)
			{
				Assert.IsNotNull(_bubbles[lIndex], "[ChatFrame] InitBubblesChoice(), bubble at index " + lIndex + " is null.");

				_bubbles[lIndex].SetText(_bubblesKey[lIndex]);
			}

			_bottomSentenceIndex	=	lLength - 1;
		}

		/// <summary>
		/// Initialize first and last child
		/// </summary>
		private	void	InitializeFirstAndLastChild()
		{
			UpdateFirstChild();

			UpdateLastChild();
		}

		/// <summary>
		/// Update first child
		/// </summary>
		private	void	UpdateFirstChild()
		{
			Assert.IsNotNull(_content, "[ChatFrame] UpdateFirstChild(), _content is null.");

			Assert.IsFalse(0 == _content.childCount, "[ChatFrame] UpdateFirstChild(), _content don't have children");

			_firstChildTransform	=	_content.GetChild(0) as RectTransform;

			Assert.IsNotNull(_firstChildTransform, "[ChatFrame] UpdateFirstChild(), _firstChildTransform is null.");

			_firstChildBubble		=	_firstChildTransform.GetComponent<ChatBubbleChoice>();

			Assert.IsNotNull(_firstChildBubble, "[ChatFrame] UpdateFirstChild(), _firstChildBubble is null.");
		}

		/// <summary>
		/// Update last child
		/// </summary>
		private	void	UpdateLastChild()
		{
			Assert.IsNotNull(_content, "[ChatFrame] UpdateLastChild(), _content is null.");

			Assert.IsFalse(0 == _content.childCount, "[ChatFrame] UpdateLastChild(), _content don't have children");

			_lastChildTransform	=	_content.GetChild(_content.childCount - 1) as RectTransform;

			Assert.IsNotNull(_lastChildTransform, "[ChatFrame] UpdateFirstChild(), _lastChildTransform is null.");

			_lastChildBubble		=	_lastChildTransform.GetComponent<ChatBubbleChoice>();

			Assert.IsNotNull(_lastChildBubble, "[ChatFrame] UpdateFirstChild(), _lastChildBubble is null.");
		}

		/// <summary>
		/// Callback invoke when frame was shown
		/// </summary>
		private	void	OnFrameShown()
		{
			_isShowing	=	false;

			Assert.IsNotNull(_frameGroup, "[ChatFrame] OnFrameShown(), _frameGroup is null.");

			_frameGroup.interactable	=	true;
		}

		/// <summary>
		/// Callback invoke when frame was hidden
		/// </summary>
		private	void	OnFrameHidden()
		{
			_isHidding	=	false;

			Assert.IsNotNull(_frameGroup, "[ChatFrame] OnFrameHidden(), _frameGroup is null.");

			_frameGroup.gameObject.SetActive(false);
		}

		/// <summary>
		/// Callback invoke when user is scrolling
		/// </summary>
		/// <param name="pPos"></param>
		private	void	OnScroll(Vector2 pPos)
		{
			_curScrollPos	=	pPos;

			if (_curScrollPos.y > _prevScrollPos.y)
			{
				OnScrollDown();
			}
			else if (_curScrollPos.y < _prevScrollPos.y)
			{
				OnScrollUp();
			}

			_prevScrollPos	=	_curScrollPos;
		}

		/// <summary>
		/// Callback invoke when user scroll up
		/// </summary>
		private	void	OnScrollUp()
		{
			Assert.IsNotNull(_firstChildTransform, "[ChatFrame] OnScrollUp(), _firstChildTransform is null.");

			_firstChildBottom	=	_firstChildTransform.TransformPoint(0f, _firstChildTransform.rect.yMin, 0f).y;

			if (_firstChildBottom > _scrollRectTop)
			{
				Assert.IsNotNull(_lastChildTransform,  "[ChatFrame] OnScrollUp(), _lastChildTransform is null.");

				ChangePivot(_firstChildTransform, new Vector2(0.5f, 1f));
				ChangePivot(_lastChildTransform, new Vector2(0.5f, 0f));

				_firstChildTransform.position	=	_lastChildTransform.position;

				_firstChildTransform.ForceUpdateRectTransforms();

				_firstChildTransform.SetAsLastSibling();

				_lastChildTransform	=	_firstChildTransform;
				_lastChildBubble	=	_firstChildBubble;

				IncreaseSentenceIndex();

				Assert.IsNotNull(_firstChildBubble, "[ChatFrame] OnScrollUp(), _firstChildBubble is null.");

				_firstChildBubble.SetText(_bubblesKey[_bottomSentenceIndex]);

				UpdateFirstChild();

				OnScrollUp();
			}
		}

		/// <summary>
		/// Callback invoke when user scroll down
		/// </summary>
		private	void	OnScrollDown()
		{
			Assert.IsNotNull(_lastChildTransform, "[ChatFrame] OnScrollDown(), _lastChildTransform is null.");

			_lastChildTop	=	_lastChildTransform.TransformPoint(0f, _lastChildTransform.rect.yMax, 0f).y;

			if (_lastChildTop < _scrollRectBottom)
			{
				Assert.IsNotNull(_firstChildTransform, "[ChatFrame] OnScroll(), _firstChildTransform is null.");

				ChangePivot(_firstChildTransform, new Vector2(0.5f, 1f));
				ChangePivot(_lastChildTransform, new Vector2(0.5f, 0f));

				_lastChildTransform.position	=	_firstChildTransform.position;

				_lastChildTransform.ForceUpdateRectTransforms();

				_lastChildTransform.SetAsFirstSibling();

				_firstChildTransform	=	_lastChildTransform;
				_firstChildBubble		=	_lastChildBubble;

				DecreaseSentenceIndex();

				Assert.IsNotNull(_lastChildBubble, "[ChatFrame] OnScrollDown(), _lastChildBubble is null.");

				_lastChildBubble.SetText(_bubblesKey[_topSentenceIndex]);

				UpdateLastChild();

				OnScrollDown();
			}
		}

		/// <summary>
		/// Change rect transform pivot
		/// </summary>
		/// <param name="pRectTransform">Rect transform to modify</param>
		/// <param name="pPivot">New pivot to apply</param>
		private	void	ChangePivot(RectTransform pRectTransform, Vector2 pPivot)
		{
			if (null != pRectTransform)
			{
				Vector2	lSize			=	pRectTransform.rect.size;
				Vector2	lDeltaPivot		=	pRectTransform.pivot - pPivot;
				Vector3	lDeltaPosition	=	pRectTransform.rotation * new Vector3(lDeltaPivot.x * lSize.x * pRectTransform.localScale.x, lDeltaPivot.y * lSize.y * pRectTransform.localScale.y);

				pRectTransform.pivot			=	pPivot;

				pRectTransform.ForceUpdateRectTransforms();

				pRectTransform.localPosition	-=	lDeltaPosition;

				pRectTransform.ForceUpdateRectTransforms();
			}
		}

		/// <summary>
		/// Increase sentence index
		/// </summary>
		private	void	IncreaseSentenceIndex()
		{
			++_topSentenceIndex;
			++_bottomSentenceIndex;

			if (_topSentenceIndex >= _bubblesKey.Length)
			{
				_topSentenceIndex	=	0;
			}
			if (_bottomSentenceIndex >= _bubblesKey.Length)
			{
				_bottomSentenceIndex	=	0;
			}
		}

		/// <summary>
		/// Decrease sentence index
		/// </summary>
		private	void	DecreaseSentenceIndex()
		{
			--_topSentenceIndex;
			--_bottomSentenceIndex;

			if (_topSentenceIndex <	0)
			{
				_topSentenceIndex	=	_bubblesKey.Length - 1;
			}
			if (_bottomSentenceIndex < 0)
			{
				_bottomSentenceIndex	=	_bubblesKey.Length - 1;
			}
		}

		#endregion
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Events_Utility : MonoBehaviour
{
    public static Events_Utility Instance;

    public GameObject Target_Gob;
    public string Param_Str;



    void Awake()
    {
        Instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Param_Str == "Track_Minion_Name_TMPText" && Target_Gob != null)
        {
            //gameObject.GetComponent<RectTransform>().position = new Vector3(Target_Gob.GetComponent<RectTransform>().position.x, Target_Gob.GetComponent<RectTransform>().position.y, Target_Gob.GetComponent<RectTransform>().position.z);
            gameObject.GetComponent<RectTransform>().position = Camera.main.WorldToScreenPoint(GameObject.Find(Target_Gob.name + "/[Animator] Character/Character_Mesh_01/Tip_Point_Ref_Pos").transform.position);
        }
    }
}

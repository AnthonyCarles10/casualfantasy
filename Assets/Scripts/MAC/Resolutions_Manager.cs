﻿using Sportfaction.CasualFantasy.Manager.Profile;
using Sportfaction.CasualFantasy.PVP.Gameplay.Actions;
using Sportfaction.CasualFantasy.PVP.Gameplay.Matchs;
using Sportfaction.CasualFantasy.PVP.Gameplay.Players;
using Sportfaction.CasualFantasy.PVP.Gameplay.UI;

using SportFaction.CasualFootballEngine.ActionService.Enum;
using SportFaction.CasualFootballEngine.ActionService.Response;
using SportFaction.CasualFootballEngine.PhaseService.Enum;
using SportFaction.CasualFootballEngine.PlayerService.Enum;
using SportFaction.CasualFootballEngine.PlayerService.Model;
using SportFaction.CasualFootballEngine.TeamService.Enum;

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Resolutions_Manager : MonoBehaviour
{
    #region Variables

    public static Resolutions_Manager Instance;

    public float DelayBeforeScaleScore_Flt = 1.0f;
    public float DelayBeforeHideScore_Flt = 1.0f;

    public float BallSpeedFactor_Flt = 1;

    [Header("Resolution settings")]
    [ReadOnly]
    public string CurrentResolutionCase_Str = "Null";
    [ReadOnly]
    public string CurrentPhase_Str = "Start_Phase";
    [ReadOnly]
    public string CurrentMode_Str = "PC";

    [Space(8)]
    public GameObject[] PrefabsIdxArray_Gob;
    public string[] PrefabsIdxArray_Str;

    public PlayerResponse ExecuteActionTeamBlue_Pr;
    public PlayerResponse ExecuteActionTeamRed_Pr;

    public PassResolutionResponse PassResolution_Prr;
    public LongKickResolutionResponse LongKickResolution_Lkrr;
    public MoveResolution SprintResolution_Srr;

    private GameObject TeamBlueScoreDisplay;
    private GameObject TeamRedScoreDisplay;

    private float YoffsetHUD_Flt = 8.0f;
    private float XoffsetHUD_Flt = 0.0f;

    public Material RobotoBlueTeam_Mat;
    public Material RobotoRedTeam_Mat;

    // Player & Opponent Data
    [Space(8)]
    [ReadOnly]
    public GameObject PlayerScore_Gob;
    [ReadOnly]
    public int PlayerScoreAfterPressure_Int = 0;
    [ReadOnly]
    public int PlayerScoreBeforePressure_Int = 0;

    [ReadOnly]
    public GameObject OpponentScore_Gob;
    [ReadOnly]
    public int OpponentScoreAfterPressure_Int = 0;
    [ReadOnly]
    public int OpponentScoreBeforePressure_Int = 0;

    private List<GameObject> PressureDirectionLst_Gob = new List<GameObject>();
    private List<GameObject> PressureValueLst_Gob = new List<GameObject>();
    private List<GameObject> InterceptorsLst_Gob = new List<GameObject>();
    private List<GameObject> OnFieldLst_Gob = new List<GameObject>();

    [ReadOnly]
    public Dictionary<string, object> CurrentResolutionData_Dic = new Dictionary<string, object>();

    private float DecrementScoreIdleTimer_Flt = 0;
    public float WaitDelayDisplayChosenNumber_Flt = 1.15f;

    [Header("Delay variables")]
    [Tooltip("Score decrementation speed")]
    public float WaitDelayDecrementScore_Flt = 1.5f;

    [Tooltip("Delay before score decrementation | score scaling | score color change")]
    public float WaitDelayDisplayScaledScore_Flt = 0.4f;
    [Tooltip("Delay before removing final winning score")]
    public float WaitDelayHideScaledScore_Flt = 0.3f;

    [Tooltip("Delay between each pressure animation")]
    public float WaitDelayDisplayPressureElements_Flt = 0.35f;
    [Tooltip("Delay between every pressure element removal")]
    public float WaitDelayHidePressureElements_Flt = 0.35f;

    [Tooltip("Delay between each interceptor dice throw")]
    public float WaitDelayDisplayInterceptorsScore_Flt = 0.5f;

    // Toss/Move Resolutions Vars
    [Space(8)]
    [ReadOnly]
    public int PlayerMinRange_Int = 0;
    [ReadOnly]
    public int PlayerMaxRange_Int = 0;
    [ReadOnly]
    public int PlayerDiceScoreLevel_Int = 0;

    [ReadOnly]
    public int OpponentMinRange_Int = 0;
    [ReadOnly]
    public int OpponentMaxRange_Int = 0;
    [ReadOnly]
    public int OpponentDiceScoreLevel_Int = 0;

    private float PlayerDecrementTimer_Flt = -1f;
    private float OpponentDecrementTimer_Flt = -1f;

    private float PressureDirectionTimer_Flt = 0.5f;
    private int PressureDirectionIdx_Int = 0;

    private float PressureValueTimer_Flt = 0f;
    private int PressureValueIdx_Int = 0;

    private float   _moveDelay          =   0f;
    private bool    _moveWinnerBol      =   false;
    private bool    CameraInPlace       =   true;

    //
    //
    // Resolution Display Blocks
    //
    //

    // Update is called once per frame

    int LocalIdx_Int = 0;
    List<Vector3> pathPointLst_Vct = new List<Vector3>();
    GameObject lineRenderer_Gob;

    public Color32 BlueInnerColor_Col32 = new Color32(66, 248, 255, 255);
    public Color32 BlueOutlineColor_Col32 = new Color32(0, 156, 255, 255);

    public Color32 RedInnerColor_Col32 = new Color32(255, 0, 90, 255);
    public Color32 RedOutlineColor_Col32 = new Color32(170, 0, 60, 255);

    private bool _removeBoard = false;

    #endregion

    void Awake()
    {
        Instance = this;

        //--------------------------------------------------------------------> Store GameObject Names For Array Index Of (Search By Name On A Specific Array)
        PrefabsIdxArray_Str = new string[PrefabsIdxArray_Gob.Length];

        for (int i = 0; i < PrefabsIdxArray_Str.Length; i++)
        { PrefabsIdxArray_Str[i] = PrefabsIdxArray_Gob[i].name; }
    }

    // Start is called before the first frame update
    void Start()
    {
        #region Start Display Player/Opponent Dice Throw

        GameObject selectedMinion_Gob = GameObject.Find("Prefab_Minion Selected");

        if (selectedMinion_Gob != null)
        { selectedMinion_Gob.SetActive(false); }

        //
        //
        // 
        //
        //

        if (CurrentResolutionCase_Str == "Toss_Resolution" || "Move_Resolution" == CurrentResolutionCase_Str)
        {
            #region Toss_Resolution || Move_Resolution

            //
            //
            // Player Score Dice Throw 
            //
            //

            PlayerScoreAfterPressure_Int = (CurrentResolutionCase_Str == "Move_Resolution") ? ExecuteActionTeamBlue_Pr.ExecuteAction.Score : ExecuteActionTeamBlue_Pr.ExecuteAction.ScoreWithPressure;
            PlayerScoreBeforePressure_Int = ExecuteActionTeamBlue_Pr.ExecuteAction.Score;
            PlayerDiceScoreLevel_Int = 4;

            if (CurrentResolutionCase_Str == "Toss_Resolution")
                PlayerDiceScoreLevel_Int = 0;
            else
                PlayerDiceScoreLevel_Int = PlayersManager.Instance.GetPlayer(eTeamMethods.ConvertStringToETeam(ExecuteActionTeamBlue_Pr.Team), ePositionMethods.ConvertStringToEPosition(ExecuteActionTeamBlue_Pr.Position)).Data.FormLevel;

            string[] rangeArray_Str = ExecuteActionTeamBlue_Pr.ExecuteAction.LossRange.Split('-');

            if (ExecuteActionTeamBlue_Pr.ExecuteAction.Draw == "won")
            { rangeArray_Str = ExecuteActionTeamBlue_Pr.ExecuteAction.WinRange.Split('-'); }

            PlayerMinRange_Int = int.Parse(rangeArray_Str[0]);
            PlayerMaxRange_Int = int.Parse(rangeArray_Str[1]);

            GameObject diceThrow_Gob = Instantiate(PrefabsIdxArray_Gob[Array.IndexOf(PrefabsIdxArray_Str, "Dice_Throw_Grp")], Vector3.zero, Quaternion.identity);

            diceThrow_Gob.name = "Dice_Throw_Player_1";
            diceThrow_Gob.transform.SetParent(gameObject.transform, false);

            GameObject catch_Gob = GameObject.Find(diceThrow_Gob.name + "/" + PlayerDiceScoreLevel_Int + "_Dice_Img");
            //catch_Gob.GetComponent<Image>().enabled = true;
            if ("Toss_Resolution" == CurrentResolutionCase_Str)
                catch_Gob.GetComponent<Image>().color = ActionsManager.Instance.ColorConfig.GetTeamColor(true);

            PlaceHUD(diceThrow_Gob, GameObject.Find(ExecuteActionTeamBlue_Pr.Team + '-' + ExecuteActionTeamBlue_Pr.Position));
            diceThrow_Gob.GetComponent<RectTransform>().position = new Vector3(diceThrow_Gob.GetComponent<RectTransform>().position.x, diceThrow_Gob.GetComponent<RectTransform>().position.y + 8, diceThrow_Gob.GetComponent<RectTransform>().position.z);

            //
            //
            // Opponent Score Dice Throw 
            //
            //

            OpponentScoreAfterPressure_Int = (CurrentResolutionCase_Str == "Move_Resolution") ? ExecuteActionTeamRed_Pr.ExecuteAction.Score : ExecuteActionTeamRed_Pr.ExecuteAction.ScoreWithPressure;
            OpponentScoreBeforePressure_Int = ExecuteActionTeamRed_Pr.ExecuteAction.Score;
            OpponentDiceScoreLevel_Int = 4;

            if (CurrentResolutionCase_Str == "Toss_Resolution")
                OpponentDiceScoreLevel_Int = 0;
            else
                OpponentDiceScoreLevel_Int = PlayersManager.Instance.GetPlayer(eTeamMethods.ConvertStringToETeam(ExecuteActionTeamRed_Pr.Team), ePositionMethods.ConvertStringToEPosition(ExecuteActionTeamRed_Pr.Position)).Data.FormLevel;

            rangeArray_Str = ExecuteActionTeamRed_Pr.ExecuteAction.LossRange.Split('-');

            if (ExecuteActionTeamRed_Pr.ExecuteAction.Draw == "won")
            { rangeArray_Str = ExecuteActionTeamRed_Pr.ExecuteAction.WinRange.Split('-'); }

            OpponentMinRange_Int = int.Parse(rangeArray_Str[0]);
            OpponentMaxRange_Int = int.Parse(rangeArray_Str[1]);

            diceThrow_Gob = Instantiate(PrefabsIdxArray_Gob[Array.IndexOf(PrefabsIdxArray_Str, "Dice_Throw_Grp")], Vector3.zero, Quaternion.identity);

            diceThrow_Gob.name = "Dice_Throw_Opponent_1";
            diceThrow_Gob.transform.SetParent(gameObject.transform, false);

            catch_Gob = GameObject.Find(diceThrow_Gob.name + "/" + OpponentDiceScoreLevel_Int + "_Dice_Img");
            //catch_Gob.GetComponent<Image>().enabled = true;
            if ("Toss_Resolution" == CurrentResolutionCase_Str)
                catch_Gob.GetComponent<Image>().color = ActionsManager.Instance.ColorConfig.GetTeamColor(false);

            PlaceHUD(diceThrow_Gob, GameObject.Find(ExecuteActionTeamRed_Pr.Team + '-' + ExecuteActionTeamRed_Pr.Position));
            diceThrow_Gob.GetComponent<RectTransform>().position = new Vector3(diceThrow_Gob.GetComponent<RectTransform>().position.x, diceThrow_Gob.GetComponent<RectTransform>().position.y + 8, diceThrow_Gob.GetComponent<RectTransform>().position.z);

            #endregion
        }
        else
        {
            #region Duel || Pass || Long Kick Resolution

            //
            //
            //-------------------------------------------------------------------------------------------------------------------------------------> Debug Specific Case // No Pressure // No Duel // No Interceptors

            //List<PlayerActionResponse> empty_Par = new List<PlayerActionResponse>();

            //ExecuteActionTeamBlue_Pr.PressureOpponents = empty_Par;
            //ExecuteActionTeamRed_Pr.PressureOpponents = empty_Par;
            //ExecuteActionTeamRed_Pr = null;

            //PassResolution_Prr.Interceptors = empty_Par;
            //PassResolution_Prr.ReceiverOpponentsPressure = empty_Par;
            //PassResolution_Prr.ReceiverOpponentDuelOpponentsPressure = empty_Par;
            //PassResolution_Prr.ReceiverOpponentDuel = null;

            /*List<PlayerActionResponse> onlyGoalKeeperLst_Par = new List<PlayerActionResponse>();

            foreach (var catch_Var in LongKickResolution_Lkrr.Interceptors)
            {
                if (catch_Var.Position == "G")
                { onlyGoalKeeperLst_Par.Add(catch_Var); }
            }

            LongKickResolution_Lkrr.Interceptors = onlyGoalKeeperLst_Par;*/

            //
            //
            //-------------------------------------------------------------------------------------------------------------------------------------> Start Resolution

            CurrentPhase_Str = "Wait";
            pathPointLst_Vct.Add(GameObject.Find(ExecuteActionTeamBlue_Pr.Team + '-' + ExecuteActionTeamBlue_Pr.Position).transform.position);
            StartCoroutine(ExecuteResolution());

            //
            //
            //

            #endregion
        }

        print("//--------------------------------------------------------------------> [CurrentResolutionCase_Str] : " + CurrentResolutionCase_Str + " || [CurrentMode_Str] : " + CurrentMode_Str);

        #endregion
    }

    void Update()
    {
        //
        //
        // Start Phase // Toss Resolution
        //
        //

        if (CurrentPhase_Str == "Start_Phase")
        {
            #region Start Phase : Toss/Move Resolution

            bool startDecrementingScore_Bol = DisplayScore();

            //
            //
            // Exit Loop If PressureOpponents Is Empty Or Score Without Pressure Reach The Same Value Of Score With Pressure
            //
            //

            int onHoldPressureObjectCounter_Int = 0;

            foreach (var catch_Var in PressureDirectionLst_Gob)
            {
                if (catch_Var == null)
                { onHoldPressureObjectCounter_Int++; }
            }

            if (PlayerScoreBeforePressure_Int == PlayerScoreAfterPressure_Int && OpponentScoreBeforePressure_Int == OpponentScoreAfterPressure_Int
                && startDecrementingScore_Bol && PressureDirectionLst_Gob.Count == onHoldPressureObjectCounter_Int && PlayerScore_Gob != null)
            {
                if (CurrentResolutionCase_Str == "Move_Resolution" || CurrentResolutionCase_Str == "Toss_Resolution")
                    CurrentPhase_Str = "End_Phase";
            }

            #endregion
        }

        //
        //
        // End Phase // Toss Resolution
        //
        //

        if (CurrentPhase_Str == "End_Phase")
        {
            #region End Phase : Toss/Move Resolution

            if (CurrentResolutionCase_Str == "Move_Resolution")
            {
                _moveDelay += Time.deltaTime;
                if (_moveDelay > 1f && !_removeBoard)
                {
                    ActionsUI.Instance.ClearResolution();
                    ActionsUI.Instance.ToggleActionBoard(false);
                    _removeBoard = true;
                }

                if (_moveDelay > 1.5f && !_moveWinnerBol)
                {
                    _moveWinnerBol = true;

                    if (ExecuteActionTeamBlue_Pr.ExecuteAction.ScoreWithPressure > ExecuteActionTeamRed_Pr.ExecuteAction.ScoreWithPressure)
                    {
                        print("---------------------------------------------> Player Win Duel");

                        GameObject minion_Gob = GameObject.Find(ExecuteActionTeamBlue_Pr.Team + '-' + ExecuteActionTeamBlue_Pr.Position);
                        GameObject minionAnimator_Gob = GameObject.Find(minion_Gob.name + "/[Animator] Character/Character_Mesh_01");
                        minionAnimator_Gob.GetComponent<Animator>().SetTrigger("Pressure");

                        minion_Gob = GameObject.Find(ExecuteActionTeamRed_Pr.Team + '-' + ExecuteActionTeamRed_Pr.Position);
                        minionAnimator_Gob = GameObject.Find(minion_Gob.name + "/[Animator] Character/Character_Mesh_01");
                        minionAnimator_Gob.GetComponent<Animator>().SetTrigger("Fail");

                        MatchAudio.Instance.Tackle();
                    }
                    else
                    {
                        print("---------------------------------------------> Opponent Win Duel");

                        GameObject minion_Gob = GameObject.Find(ExecuteActionTeamBlue_Pr.Team + '-' + ExecuteActionTeamBlue_Pr.Position);
                        GameObject minionAnimator_Gob = GameObject.Find(minion_Gob.name + "/[Animator] Character/Character_Mesh_01");
                        minionAnimator_Gob.GetComponent<Animator>().SetTrigger("Fail");

                        minion_Gob = GameObject.Find(ExecuteActionTeamRed_Pr.Team + '-' + ExecuteActionTeamRed_Pr.Position);
                        minionAnimator_Gob = GameObject.Find(minion_Gob.name + "/[Animator] Character/Character_Mesh_01");
                        minionAnimator_Gob.GetComponent<Animator>().SetTrigger("Pressure");

                        MatchAudio.Instance.Tackle();
                    }

                }

                if (_moveDelay > 2f)
                {
                    PlayerPVP lPlayerBlue = PlayersManager.Instance.GetPlayer(eTeamMethods.ConvertStringToETeam(ExecuteActionTeamBlue_Pr.Team), ePositionMethods.ConvertStringToEPosition(ExecuteActionTeamBlue_Pr.Position));
                    PlayerPVP lPlayerRed = PlayersManager.Instance.GetPlayer(eTeamMethods.ConvertStringToETeam(ExecuteActionTeamRed_Pr.Team), ePositionMethods.ConvertStringToEPosition(ExecuteActionTeamRed_Pr.Position));
                    PlayersManager.Instance.NbPlayersToMove = 2;
                    PlayersManager.Instance.WaitForPlacement = true;
                    //lPlayerBlue.Movements.MovePlayer(false, true);
                    //lPlayerRed.Movements.MovePlayer(false, true);

                    OppositionUI.Instance.OnDuelFinished();
                    Destroy(gameObject);
                }
            }

            if (CurrentResolutionCase_Str == "Toss_Resolution")
            {
                _moveDelay += Time.deltaTime;

                if (_moveDelay > 1)
                {
                    GameObject.Find(ExecuteActionTeamBlue_Pr.Team + "-" + ExecuteActionTeamBlue_Pr.Position)?.SetActive(false);
                    GameObject.Find(ExecuteActionTeamRed_Pr.Team + "-" + ExecuteActionTeamRed_Pr.Position)?.SetActive(false);
                    MatchMessagesUI.Instance.DisplayResultToss();
                    Destroy(gameObject);
                }
            }


            #endregion
        }

        //
        //
        //
    }

    public void InstantiateNumber(GameObject parent_Gob, string prefabName_Str, int animRandomIdx_Int, int instantiatedNumbersCounter_Int, int minRange_Int, int maxRange_Int, Color32 color_Col32, float destroyDelay_Flt)
    {
        #region Instantiate Random & Chosen Text (Score Between Range During Resolution)

        GameObject newFX_Gob = Instantiate(PrefabsIdxArray_Gob[Array.IndexOf(PrefabsIdxArray_Str, prefabName_Str)], Vector3.zero, Quaternion.identity) as GameObject;

        newFX_Gob.transform.SetParent(parent_Gob.transform, false);
        newFX_Gob.name = prefabName_Str + "_" + instantiatedNumbersCounter_Int;
        newFX_Gob.GetComponent<RectTransform>().position = parent_Gob.GetComponent<RectTransform>().position;
        newFX_Gob.GetComponent<Animator>().SetTrigger("Pos_" + 1);

        GameObject catch_Gob = GameObject.Find(parent_Gob.name + "/" + newFX_Gob.name + "/Number_Txt");
        catch_Gob.GetComponent<TMPro.TextMeshProUGUI>().text = UnityEngine.Random.Range(minRange_Int, maxRange_Int).ToString();
        //catch_Gob.GetComponent<TMPro.TextMeshProUGUI>().color = color_Col32;

        if (CurrentPhase_Str == "Wait")
        {
            GameObject dice_Gob = GameObject.Find(parent_Gob.name + "/" + (int)CurrentResolutionData_Dic[parent_Gob.name + "_Score_Level"] + "_Dice_Img");
            dice_Gob.GetComponent<Image>().enabled = false;
            newFX_Gob.GetComponent<RectTransform>().position = new Vector3(newFX_Gob.GetComponent<RectTransform>().position.x, newFX_Gob.GetComponent<RectTransform>().position.y + 10, newFX_Gob.GetComponent<RectTransform>().position.z);
        }

        //
        //
        //
        //
        //

        if (parent_Gob.name.Contains("Player_1") && prefabName_Str.Contains("Random_Number_Grp"))
        {
            MatchAudio.Instance.Draw();
            PlayersManager.Instance.GetPlayer(ExecuteActionTeamBlue_Pr.Team, ExecuteActionTeamBlue_Pr.Position)?.Player3D.ToggleResolutionFX(true);
            //-------------------------------------------------------------------------------> GET DATA VALUE 
            GameObject textValue_Gob   =   GameObject.Find(parent_Gob.name + "/" + newFX_Gob.name + "/Number_Txt");
            //print("Player_1 || Random_Number_Grp || Value : " + textValue_Gob.GetComponent<TMPro.TextMeshProUGUI>().text);

            //-------------------------------------------------------------------------------> INSTANTIATE NEW TEXT PREFAB 
            if (CurrentResolutionCase_Str != "Toss_Resolution" && null != ExecuteActionTeamRed_Pr)
            {
                GameObject lScoreGO = Instantiate(PrefabsIdxArray_Gob[Array.IndexOf(PrefabsIdxArray_Str, "Score_Display_Grp")], Vector3.zero, Quaternion.identity);
                lScoreGO.name = "Random_Score_" + LocalIdx_Int;
                OnFieldLst_Gob.Add(lScoreGO);
                LocalIdx_Int++;

                parent_Gob = GameObject.Find("Action_Score_" + ExecuteActionTeamBlue_Pr.Action + "_Btn");
                lScoreGO.transform.SetParent(parent_Gob.transform, false);
                lScoreGO.GetComponent<RectTransform>().localPosition = new Vector3(0, 0, 0);
                lScoreGO.transform.GetChild(0).GetComponent<TMPro.TextMeshProUGUI>().text = textValue_Gob.GetComponent<TMPro.TextMeshProUGUI>().text;
            }

            //if (ActionsUI.Instance.BoardDisplayed)
            //{ textValue_Gob.GetComponent<TMPro.TextMeshProUGUI>().enabled = false; }
        }

        if (parent_Gob.name.Contains("Opponent_1") && prefabName_Str.Contains("Random_Number_Grp"))
        {
            if (!CurrentResolutionCase_Str.Contains("Sprint"))
            { PlayersManager.Instance.GetPlayer(ExecuteActionTeamRed_Pr.Team, ExecuteActionTeamRed_Pr.Position)?.Player3D.ToggleResolutionFX(true); }
            //else
            //{ PlayersManager.Instance.GetPlayer(SelectedOpponent_Par.Team, SelectedOpponent_Par.Position)?.Player3D.ToggleResolutionFX(true); }

            MatchAudio.Instance.Draw();
            //-------------------------------------------------------------------------------> GET DATA VALUE 
            GameObject textValue_Gob   =   GameObject.Find(parent_Gob.name + "/" + newFX_Gob.name + "/Number_Txt");
            //print("Opponent_1 || Random_Number_Grp || Value : " + textValue_Gob.GetComponent<TMPro.TextMeshProUGUI>().text);

            //-------------------------------------------------------------------------------> INSTANTIATE NEW TEXT PREFAB
            if (CurrentResolutionCase_Str != "Toss_Resolution" && null != ExecuteActionTeamRed_Pr)
            {
                GameObject lScoreGO = Instantiate(PrefabsIdxArray_Gob[Array.IndexOf(PrefabsIdxArray_Str, "Score_Display_Grp")], Vector3.zero, Quaternion.identity);
                lScoreGO.name = "Random_Score_" + LocalIdx_Int;
                OnFieldLst_Gob.Add(lScoreGO);
                LocalIdx_Int++;

                //print(SelectedOpponent_Par.Action);

                if (!CurrentResolutionCase_Str.Contains("Sprint"))
                { parent_Gob = GameObject.Find("Action_Score_" + ExecuteActionTeamRed_Pr.Action + "_Btn"); }
                //else
                //{ parent_Gob = GameObject.Find("Action_Score_" + SelectedOpponent_Par.Action + "_Btn"); }

                //print(parent_Gob.name);

                lScoreGO.transform.SetParent(parent_Gob.transform, false);
                lScoreGO.GetComponent<RectTransform>().localPosition = new Vector3(0, 0, 0);
                lScoreGO.transform.GetChild(0).GetComponent<TMPro.TextMeshProUGUI>().text = textValue_Gob.GetComponent<TMPro.TextMeshProUGUI>().text;
            }

            if (ActionsUI.Instance.BoardDisplayed)
            { textValue_Gob.GetComponent<TMPro.TextMeshProUGUI>().enabled = false; }
        }

        if (parent_Gob.name.Contains("Player_1") && prefabName_Str.Contains("Chosen_Number_Grp"))
        {
            MatchAudio.Instance.DrawEnd();
            PlayerScore_Gob =   newFX_Gob;
            newFX_Gob.name  =   "Chosen_Number_Grp_Player_1";

            //-------------------------------------------------------------------------------> GET DATA VALUE 
            GameObject textValue_Gob   =   GameObject.Find(parent_Gob.name + "/" + newFX_Gob.name + "/Number_Txt");
            //print("Player_1 || Chosen_Number_Grp || Value : " + textValue_Gob.GetComponent<TMPro.TextMeshProUGUI>().text);

            //-------------------------------------------------------------------------------> UPDATE NEW TEXT PREFAB
            if (CurrentResolutionCase_Str != "Toss_Resolution" && null != ExecuteActionTeamRed_Pr)
            {
                TeamBlueScoreDisplay         =   Instantiate(PrefabsIdxArray_Gob[Array.IndexOf(PrefabsIdxArray_Str, "Score_Display_Grp")], Vector3.zero, Quaternion.identity);
                TeamBlueScoreDisplay.name    =   "Chosen_Score_" + LocalIdx_Int;
                OnFieldLst_Gob.Add(TeamBlueScoreDisplay);
                LocalIdx_Int++;

                parent_Gob = GameObject.Find("Action_Score_" + ExecuteActionTeamBlue_Pr.Action + "_Btn");
                TeamBlueScoreDisplay.transform.SetParent(parent_Gob.transform, false);
                TeamBlueScoreDisplay.GetComponent<RectTransform>().localPosition = new Vector3(0, 0, 0);
                TeamBlueScoreDisplay.transform.GetChild(0).GetComponent<TMPro.TextMeshProUGUI>().text = textValue_Gob.GetComponent<TMPro.TextMeshProUGUI>().text;
                TeamBlueScoreDisplay.GetComponent<Animator>().SetTrigger("Chosen_Value");
            }

            //if (!WaitDisplayScore_Bol)
            if(ActionsUI.Instance.BoardDisplayed)
            { textValue_Gob.GetComponent<TMPro.TextMeshProUGUI>().enabled = false; }
        }

        if (parent_Gob.name.Contains("Opponent_1") && prefabName_Str.Contains("Chosen_Number_Grp"))
        {
            MatchAudio.Instance.DrawEnd();
            OpponentScore_Gob = newFX_Gob;
            newFX_Gob.name = "Chosen_Number_Grp_Opponent_1";

            //-------------------------------------------------------------------------------> GET DATA VALUE 
            GameObject textValue_Gob = GameObject.Find(parent_Gob.name + "/" + newFX_Gob.name + "/Number_Txt");
            //print("Opponent_1 || Chosen_Number_Grp || Value : " + textValue_Gob.GetComponent<TMPro.TextMeshProUGUI>().text);

            //-------------------------------------------------------------------------------> UPDATE NEW TEXT PREFAB
            if (CurrentResolutionCase_Str != "Toss_Resolution" && null != ExecuteActionTeamRed_Pr)
            {
                TeamRedScoreDisplay         =   Instantiate(PrefabsIdxArray_Gob[Array.IndexOf(PrefabsIdxArray_Str, "Score_Display_Grp")], Vector3.zero, Quaternion.identity);
                TeamRedScoreDisplay.name    =   "Chosen_Score_" + LocalIdx_Int;
                OnFieldLst_Gob.Add(TeamRedScoreDisplay);
                LocalIdx_Int++;

                parent_Gob = GameObject.Find("Action_Score_" + ExecuteActionTeamRed_Pr.Action + "_Btn");
                TeamRedScoreDisplay.transform.SetParent(parent_Gob.transform, false);
                TeamRedScoreDisplay.GetComponent<RectTransform>().localPosition = new Vector3(0, 0, 0);
                TeamRedScoreDisplay.transform.GetChild(0).GetComponent<TMPro.TextMeshProUGUI>().text = textValue_Gob.GetComponent<TMPro.TextMeshProUGUI>().text;
                TeamRedScoreDisplay.GetComponent<Animator>().SetTrigger("Chosen_Value");
            }

            if (ActionsUI.Instance.BoardDisplayed)
            { textValue_Gob.GetComponent<TMPro.TextMeshProUGUI>().enabled = false; }
        }

        //
        //
        //
        //
        //

        if (parent_Gob.name.Contains("Player_2") && prefabName_Str.Contains("Random_Number_Grp"))
        {
            MatchAudio.Instance.Draw();
            PlayersManager.Instance.GetPlayer(PassResolution_Prr.Receiver.Team, PassResolution_Prr.Receiver.Position)?.Player3D.ToggleResolutionFX(true);
            //-------------------------------------------------------------------------------> GET DATA VALUE 
            GameObject textValue_Gob = GameObject.Find(parent_Gob.name + "/" + newFX_Gob.name + "/Number_Txt");
            //print("Player_2 || Random_Number_Grp || Value : " + textValue_Gob.GetComponent<TMPro.TextMeshProUGUI>().text);

            //-------------------------------------------------------------------------------> INSTANTIATE NEW TEXT PREFAB 
            GameObject lScoreGO = Instantiate(PrefabsIdxArray_Gob[Array.IndexOf(PrefabsIdxArray_Str, "Score_Display_Grp")], Vector3.zero, Quaternion.identity);
            lScoreGO.name = "Random_Score_" + LocalIdx_Int;
            OnFieldLst_Gob.Add(lScoreGO);
            LocalIdx_Int++;

            parent_Gob = GameObject.Find("Action_Score_" + eAction.RECEPTION + "_Btn");
            lScoreGO.transform.SetParent(parent_Gob.transform, false);
            lScoreGO.GetComponent<RectTransform>().localPosition = new Vector3(0, 0, 0);
            lScoreGO.transform.GetChild(0).GetComponent<TMPro.TextMeshProUGUI>().text = textValue_Gob.GetComponent<TMPro.TextMeshProUGUI>().text;

            if (ActionsUI.Instance.BoardDisplayed)
            { textValue_Gob.GetComponent<TMPro.TextMeshProUGUI>().enabled = false; }
        }

        if (parent_Gob.name.Contains("Opponent_2") && prefabName_Str.Contains("Random_Number_Grp"))
        {
            MatchAudio.Instance.Draw();
            PlayersManager.Instance.GetPlayer(PassResolution_Prr.ReceiverOpponentDuel.Team, PassResolution_Prr.ReceiverOpponentDuel.Position)?.Player3D.ToggleResolutionFX(true);
            //-------------------------------------------------------------------------------> GET DATA VALUE 
            GameObject textValue_Gob = GameObject.Find(parent_Gob.name + "/" + newFX_Gob.name + "/Number_Txt");
            //print("Opponent_2 || Random_Number_Grp || Value : " + textValue_Gob.GetComponent<TMPro.TextMeshProUGUI>().text);

            //-------------------------------------------------------------------------------> INSTANTIATE NEW TEXT PREFAB 
            GameObject lScoreGO = Instantiate(PrefabsIdxArray_Gob[Array.IndexOf(PrefabsIdxArray_Str, "Score_Display_Grp")], Vector3.zero, Quaternion.identity);
            lScoreGO.name = "Random_Score_" + LocalIdx_Int;
            OnFieldLst_Gob.Add(lScoreGO);
            LocalIdx_Int++;

            parent_Gob = GameObject.Find("Action_Score_" + eAction.DUEL_DEFENSE + "_Btn");
            lScoreGO.transform.SetParent(parent_Gob.transform, false);
            lScoreGO.GetComponent<RectTransform>().localPosition = new Vector3(0, 0, 0);
            lScoreGO.transform.GetChild(0).GetComponent<TMPro.TextMeshProUGUI>().text = textValue_Gob.GetComponent<TMPro.TextMeshProUGUI>().text;

            if (ActionsUI.Instance.BoardDisplayed)
            { textValue_Gob.GetComponent<TMPro.TextMeshProUGUI>().enabled = false; }
        }

        if (parent_Gob.name.Contains("Player_2") && prefabName_Str.Contains("Chosen_Number_Grp"))
        {
            MatchAudio.Instance.DrawEnd();
            PlayerScore_Gob =   newFX_Gob;
            newFX_Gob.name  =   "Chosen_Number_Grp_Player_2";

            //-------------------------------------------------------------------------------> GET DATA VALUE 
            GameObject  textValue_Gob   =   GameObject.Find(parent_Gob.name + "/" + newFX_Gob.name + "/Number_Txt");
            //print("Player_2 || Chosen_Number_Grp || Value : " + textValue_Gob.GetComponent<TMPro.TextMeshProUGUI>().text);

            //-------------------------------------------------------------------------------> UPDATE NEW TEXT PREFAB
            TeamBlueScoreDisplay = Instantiate(PrefabsIdxArray_Gob[Array.IndexOf(PrefabsIdxArray_Str, "Score_Display_Grp")], Vector3.zero, Quaternion.identity);
            TeamBlueScoreDisplay.name = "Chosen_Score_" + LocalIdx_Int;
            OnFieldLst_Gob.Add(TeamBlueScoreDisplay);
            LocalIdx_Int++;

            parent_Gob = GameObject.Find("Action_Score_" + eAction.RECEPTION + "_Btn");
            TeamBlueScoreDisplay.transform.SetParent(parent_Gob.transform, false);
            TeamBlueScoreDisplay.GetComponent<RectTransform>().localPosition = new Vector3(0, 0, 0);
            TeamBlueScoreDisplay.transform.GetChild(0).GetComponent<TMPro.TextMeshProUGUI>().text = textValue_Gob.GetComponent<TMPro.TextMeshProUGUI>().text;
            TeamBlueScoreDisplay.GetComponent<Animator>().SetTrigger("Chosen_Value");

            if (ActionsUI.Instance.BoardDisplayed)
            { textValue_Gob.GetComponent<TMPro.TextMeshProUGUI>().enabled = false; }
        }

        if (parent_Gob.name.Contains("Opponent_2") && prefabName_Str.Contains("Chosen_Number_Grp"))
        {
            MatchAudio.Instance.DrawEnd();
            OpponentScore_Gob   =   newFX_Gob;
            newFX_Gob.name      =   "Chosen_Number_Grp_Opponent_2";

            //-------------------------------------------------------------------------------> GET DATA VALUE 
            GameObject  textValue_Gob   =   GameObject.Find(parent_Gob.name + "/" + newFX_Gob.name + "/Number_Txt");
            //print("Opponent_2 || Chosen_Number_Grp || Value : " + textValue_Gob.GetComponent<TMPro.TextMeshProUGUI>().text);

            //-------------------------------------------------------------------------------> UPDATE NEW TEXT PREFAB
            TeamRedScoreDisplay = Instantiate(PrefabsIdxArray_Gob[Array.IndexOf(PrefabsIdxArray_Str, "Score_Display_Grp")], Vector3.zero, Quaternion.identity);
            TeamRedScoreDisplay.name = "Chosen_Score_" + LocalIdx_Int;
            OnFieldLst_Gob.Add(TeamRedScoreDisplay);
            LocalIdx_Int++;

            parent_Gob = GameObject.Find("Action_Score_" + eAction.DUEL_DEFENSE + "_Btn");
            TeamRedScoreDisplay.transform.SetParent(parent_Gob.transform, false);
            TeamRedScoreDisplay.GetComponent<RectTransform>().localPosition = new Vector3(0, 0, 0);
            TeamRedScoreDisplay.transform.GetChild(0).GetComponent<TMPro.TextMeshProUGUI>().text = textValue_Gob.GetComponent<TMPro.TextMeshProUGUI>().text;
            TeamRedScoreDisplay.GetComponent<Animator>().SetTrigger("Chosen_Value");

            if (ActionsUI.Instance.BoardDisplayed)
            { textValue_Gob.GetComponent<TMPro.TextMeshProUGUI>().enabled = false; }
        }

        //
        //
        //
        //
        //

        if (parent_Gob.name.Contains("Interceptor") && prefabName_Str.Contains("Random_Number_Grp"))
        {
            MatchAudio.Instance.Draw();
            int lIndex  =   int.Parse(parent_Gob.name.Split('_')[1]);
            if (CurrentResolutionCase_Str.Contains("Pass"))
                PlayersManager.Instance.GetPlayer(PassResolution_Prr.Interceptors[lIndex].Team, PassResolution_Prr.Interceptors[lIndex].Position)?.Player3D.ToggleResolutionFX(true);
            else if (CurrentResolutionCase_Str.Contains("Long_Kick"))
                PlayersManager.Instance.GetPlayer(LongKickResolution_Lkrr.Interceptors[lIndex].Team, LongKickResolution_Lkrr.Interceptors[lIndex].Position)?.Player3D.ToggleResolutionFX(true);
            
            //-------------------------------------------------------------------------------> GET DATA VALUE 
            //GameObject textValue_Gob = GameObject.Find(parent_Gob.name + "/" + newFX_Gob.name + "/Number_Txt");
            //print(parent_Gob.name + " || Random_Number_Grp || Value : " + textValue_Gob.GetComponent<TMPro.TextMeshProUGUI>().text);
        }

        if (parent_Gob.name.Contains("Interceptor") && prefabName_Str.Contains("Chosen_Number_Grp"))
        {
            MatchAudio.Instance.DrawEnd();
            int lIndexBis  =   int.Parse(parent_Gob.name.Split('_')[1]);
            newFX_Gob.name = "Chosen_Number_Grp_Interceptor";
            InterceptorsLst_Gob.Add(parent_Gob);

            //OpponentScore_Gob = GameObject.Find(SelectedInterceptorScore_Gob.name + "/Chosen_Number_Grp_Interceptor");
            //-------------------------------------------------------------------------------> GET DATA VALUE 
            //GameObject textValue_Gob = GameObject.Find(parent_Gob.name + "/" + newFX_Gob.name + "/Number_Txt");
            //print(parent_Gob.name + " || Chosen_Number_Grp || Value : " + textValue_Gob.GetComponent<TMPro.TextMeshProUGUI>().text);
        }

        if (parent_Gob.name.Contains("Interceptor"))
        { catch_Gob.GetComponent<TMPro.TextMeshProUGUI>().enabled = true; }
        else
        {
            if (CurrentResolutionCase_Str == "Toss_Resolution")
            { catch_Gob.GetComponent<TMPro.TextMeshProUGUI>().enabled = true; }
            else
            {
                //Solo Pass Then Display Score On The Field
                if (CurrentResolutionCase_Str.Contains("Pass") && parent_Gob.name.Contains("Player_1") && ExecuteActionTeamRed_Pr == null)
                { catch_Gob.GetComponent<TMPro.TextMeshProUGUI>().enabled = true; }

                if (CurrentResolutionCase_Str.Contains("Pass") && parent_Gob.name.Contains("Opponent_1"))
                { }

                //Solo Pass Then Display Score On The Field
                if (CurrentResolutionCase_Str.Contains("Pass") && parent_Gob.name.Contains("Player_2") && PassResolution_Prr.ReceiverOpponentDuel == null)
                { catch_Gob.GetComponent<TMPro.TextMeshProUGUI>().enabled = true; }

                if (CurrentResolutionCase_Str.Contains("Pass") && parent_Gob.name.Contains("Opponent_2"))
                { }

                //
                //
                //

                //Solo Pass Then Display Score On The Field
                if (CurrentResolutionCase_Str.Contains("Long_Kick") && parent_Gob.name.Contains("Player_2") && ExecuteActionTeamRed_Pr == null)
                { catch_Gob.GetComponent<TMPro.TextMeshProUGUI>().enabled = true; }

                if (CurrentResolutionCase_Str.Contains("Long_Kick") && parent_Gob.name.Contains("Opponent_2"))
                { }

                //
                //
                //

                //Solo Pass Then Display Score On The Field
                if (CurrentResolutionCase_Str.Contains("Duel") && parent_Gob.name.Contains("Player_1") && ExecuteActionTeamRed_Pr == null)
                { catch_Gob.GetComponent<TMPro.TextMeshProUGUI>().enabled = true; }

                if (CurrentResolutionCase_Str.Contains("Duel") && parent_Gob.name.Contains("Opponent_1"))
                { }

                if (ActionsUI.Instance.BoardDisplayed)
                { catch_Gob.GetComponent<TMPro.TextMeshProUGUI>().enabled = false; }
            }
        }

        if (destroyDelay_Flt > 0.0f)
        { Destroy(newFX_Gob, destroyDelay_Flt); }

        #endregion
    }

    public bool DisplayScore()
    {
        #region Display Score Sequence

        //
        //
        // Pressure Display UI & Field
        //
        //

        int pressureCounter_Int = ExecuteActionTeamBlue_Pr.PressureOpponents.Count;

        if (ExecuteActionTeamRed_Pr != null)
        { pressureCounter_Int += ExecuteActionTeamRed_Pr.PressureOpponents.Count; }

        if (CurrentResolutionCase_Str.Contains("Pass") && CurrentPhase_Str == "Reception_Phase")
        { pressureCounter_Int = PassResolution_Prr.ReceiverOpponentsPressure.Count + PassResolution_Prr.ReceiverOpponentDuelOpponentsPressure.Count; }

        if (CurrentResolutionCase_Str.Contains("Long_Kick") && CurrentPhase_Str == "Reception_Phase")
        { pressureCounter_Int = 0; }

        bool startDecrementingScore_Bol = false;

        if (pressureCounter_Int > 0)
        {
            //
            //
            // Display Pressure Direction First
            //
            //

            if (PressureDirectionLst_Gob.Count > 0 && PressureDirectionIdx_Int < PressureDirectionLst_Gob.Count)
            {
                PressureDirectionTimer_Flt += Time.deltaTime;

                if (PressureDirectionTimer_Flt > 0.2f)
                {
                    PressureDirectionLst_Gob.ToArray()[PressureDirectionIdx_Int].SetActive(true);

                    PressureDirectionTimer_Flt = 0;
                    PressureDirectionIdx_Int++;
                }
            }

            //
            //
            // Display Pressure Value Second
            //
            //

            if (/*PressureDirectionIdx_Int == PressureDirectionLst_Gob.Count &&*/ PressureValueLst_Gob.Count > 0 && PressureValueIdx_Int < PressureValueLst_Gob.Count)
            {
                PressureValueTimer_Flt += Time.deltaTime;

                if (PressureValueTimer_Flt > 0.2f)
                {
                    PressureValueLst_Gob.ToArray()[PressureValueIdx_Int].SetActive(true);

                    PressureValueTimer_Flt = 0;
                    PressureValueIdx_Int++;
                }
            }

            //
            //
            // Start Decrementing Score 
            //
            //

            if (PressureDirectionIdx_Int == PressureDirectionLst_Gob.Count && PressureDirectionLst_Gob.Count > 0
                && PressureValueIdx_Int == PressureValueLst_Gob.Count && PressureValueLst_Gob.Count > 0)
            { startDecrementingScore_Bol = true; }
        }
        else
        {
            //
            //
            // No Pressure Opponent So Directly Display Decrementing Score
            //
            //

            startDecrementingScore_Bol = true;
        }

        //
        //
        // Player Score Decrement
        //
        //

        if (startDecrementingScore_Bol && PlayerScore_Gob != null && OpponentScore_Gob != null)
        {
            DecrementScoreIdleTimer_Flt += Time.deltaTime;

            GameObject dice1_Gob = GameObject.Find("Dice_Throw_Player_1");

            if (PlayerScore_Gob != null && CurrentResolutionCase_Str == "Toss_Resolution" && dice1_Gob != null && dice1_Gob.GetComponent<Image>().enabled == false)
            {
                PlayerScore_Gob.GetComponent<RectTransform>().localScale = new Vector3(1.5f, 1.5f, 1.5f);
                GameObject lParentGO = GameObject.Find(ExecuteActionTeamBlue_Pr.Team + "-" + ExecuteActionTeamBlue_Pr.Position);
                if (null != lParentGO)
                    dice1_Gob.transform.SetParent(lParentGO.transform, false);
                dice1_Gob.GetComponent<Image>().enabled = true;
                dice1_Gob.GetComponent<RectTransform>().localPosition = new Vector3(0f, 20f, 0f);
            }

            GameObject dice2_Gob = GameObject.Find("Dice_Throw_Opponent_1");

            if (OpponentScore_Gob != null && CurrentResolutionCase_Str == "Toss_Resolution" && dice2_Gob != null && dice2_Gob.GetComponent<Image>().enabled == false)
            {
                OpponentScore_Gob.GetComponent<RectTransform>().localScale = new Vector3(1.5f, 1.5f, 1.5f);
                GameObject lParentGO = GameObject.Find(ExecuteActionTeamRed_Pr.Team + "-" + ExecuteActionTeamRed_Pr.Position);
                if (null != lParentGO)
                    dice2_Gob.transform.SetParent(lParentGO.transform, false);

                dice2_Gob.GetComponent<Image>().enabled = true;
                dice2_Gob.GetComponent<RectTransform>().localPosition = new Vector3(0f, 20f, 0f);
            }

            if (DecrementScoreIdleTimer_Flt > WaitDelayDisplayScaledScore_Flt * 3)
            {
                if (ExecuteActionTeamBlue_Pr != null && PlayerScore_Gob != null)
                {
                    if (PlayerScoreBeforePressure_Int > PlayerScoreAfterPressure_Int)
                    {
                        PlayerDecrementTimer_Flt += Time.deltaTime;

                        if (PlayerDecrementTimer_Flt > 0.05f)
                        {
                            GameObject catch_Gob = GameObject.Find(PlayerScore_Gob.name + "/Number_Txt");
                            catch_Gob.GetComponent<TMPro.TextMeshProUGUI>().text = PlayerScoreBeforePressure_Int.ToString();

                            PlayerScoreBeforePressure_Int--;
                            PlayerDecrementTimer_Flt = 0;
                        }
                    }

                    // Select Winner According To Score
                    if (PlayerScoreBeforePressure_Int <= PlayerScoreAfterPressure_Int && PlayerScoreAfterPressure_Int > OpponentScoreAfterPressure_Int)
                    {
                        PlayerScore_Gob.GetComponent<RectTransform>().localScale = new Vector3(2f, 2f, 2f);
                        GameObject catch_Gob = GameObject.Find(PlayerScore_Gob.name + "/Number_Txt");

                        if (OpponentScore_Gob != null && CurrentResolutionCase_Str == "Toss_Resolution")
                        {
                            GameObject dice_Gob = GameObject.Find("Dice_Throw_Player_1");
                            dice_Gob.GetComponent<RectTransform>().localPosition = new Vector3(0f, 20f, 0f);
                        }

                        if (OpponentScore_Gob != null)
                        {
                            if (CurrentPhase_Str != "Middle_Phase")
                            {
                                if (CurrentMode_Str.Contains("NPC"))
                                {
                                    GameObject.Find(PlayerScore_Gob.name + "/Number_Txt").GetComponent<TMPro.TextMeshProUGUI>().faceColor = RedInnerColor_Col32;
                                    GameObject.Find(PlayerScore_Gob.name + "/Number_Txt").GetComponent<TMPro.TextMeshProUGUI>().outlineColor = RedOutlineColor_Col32;
                                    GameObject.Find(PlayerScore_Gob.name + "/Number_Txt").GetComponent<TMPro.TextMeshProUGUI>().fontMaterial.SetColor("_UnderlayColor", RedOutlineColor_Col32);
                                    GameObject.Find(PlayerScore_Gob.name + "/Number_Txt").GetComponent<TMPro.TextMeshProUGUI>().fontMaterial.SetFloat("_UnderlayDilate", 0.25f);
                                    MatchAudio.Instance.ScoreLose();
                                }
                                else
                                {
                                    GameObject.Find(PlayerScore_Gob.name + "/Number_Txt").GetComponent<TMPro.TextMeshProUGUI>().faceColor = BlueInnerColor_Col32;
                                    GameObject.Find(PlayerScore_Gob.name + "/Number_Txt").GetComponent<TMPro.TextMeshProUGUI>().outlineColor = BlueOutlineColor_Col32;
                                    GameObject.Find(PlayerScore_Gob.name + "/Number_Txt").GetComponent<TMPro.TextMeshProUGUI>().fontMaterial.SetColor("_UnderlayColor", BlueOutlineColor_Col32);
                                    GameObject.Find(PlayerScore_Gob.name + "/Number_Txt").GetComponent<TMPro.TextMeshProUGUI>().fontMaterial.SetFloat("_UnderlayDilate", 0.25f);
                                    MatchAudio.Instance.ScoreWin();
                                }

                                if (CurrentMode_Str == "PC")
                                { DisplayResultUI(true, ExecuteActionTeamBlue_Pr.Team == UserData.Instance.PVPTeam.ToString()); }

                                if (CurrentMode_Str.Contains("NPC"))
                                { DisplayResultUI(false, ExecuteActionTeamBlue_Pr.Team == UserData.Instance.PVPTeam.ToString()); }

                                if (CurrentMode_Str.Contains("PVP"))
                                { DisplayResultUI(true, ExecuteActionTeamBlue_Pr.Team == UserData.Instance.PVPTeam.ToString()); }
                            }
                        }
                        else
                        {
                        }
                    }
                }

                //
                //
                // Opponent Score Decrement
                //
                //

                bool opponentSideAvailabe_Bol = true;

                if (CurrentPhase_Str == "Start_Phase" && ExecuteActionTeamRed_Pr == null)
                { opponentSideAvailabe_Bol = false; }

                if (CurrentResolutionCase_Str.Contains("Pass") && CurrentPhase_Str == "Reception_Phase" && PassResolution_Prr.ReceiverOpponentDuel == null)
                { opponentSideAvailabe_Bol = false; }

                if (opponentSideAvailabe_Bol && OpponentScore_Gob != null)
                {
                    if (OpponentScoreBeforePressure_Int > OpponentScoreAfterPressure_Int)
                    {
                        OpponentDecrementTimer_Flt += Time.deltaTime;

                        if (OpponentDecrementTimer_Flt > 0.05f)
                        {
                            GameObject catch_Gob = GameObject.Find(OpponentScore_Gob.name + "/Number_Txt");
                            catch_Gob.GetComponent<TMPro.TextMeshProUGUI>().text = OpponentScoreBeforePressure_Int.ToString();

                            OpponentScoreBeforePressure_Int--;
                            OpponentDecrementTimer_Flt = 0;
                        }
                    }

                    // Select Winner According To Score
                    if (OpponentScoreBeforePressure_Int <= OpponentScoreAfterPressure_Int && OpponentScoreAfterPressure_Int > PlayerScoreAfterPressure_Int)
                    {
                        OpponentScore_Gob.GetComponent<RectTransform>().localScale = new Vector3(2f, 2f, 2f);
                        GameObject catch_Gob = GameObject.Find(OpponentScore_Gob.name + "/Number_Txt");

                        if (CurrentResolutionCase_Str == "Toss_Resolution")
                        {
                            GameObject dice_Gob = GameObject.Find("Dice_Throw_Opponent_1");
                            dice_Gob.GetComponent<RectTransform>().localPosition = new Vector3(0f, 20f, 0f);
                        }

                        if (CurrentPhase_Str != "Middle_Phase")
                        {
                            if (CurrentMode_Str.Contains("NPC"))
                            {
                                GameObject.Find(OpponentScore_Gob.name + "/Number_Txt").GetComponent<TMPro.TextMeshProUGUI>().faceColor = RedInnerColor_Col32;
                                GameObject.Find(OpponentScore_Gob.name + "/Number_Txt").GetComponent<TMPro.TextMeshProUGUI>().outlineColor = RedOutlineColor_Col32;
                                GameObject.Find(OpponentScore_Gob.name + "/Number_Txt").GetComponent<TMPro.TextMeshProUGUI>().fontMaterial.SetColor("_UnderlayColor", RedOutlineColor_Col32);
                                GameObject.Find(OpponentScore_Gob.name + "/Number_Txt").GetComponent<TMPro.TextMeshProUGUI>().fontMaterial.SetFloat("_UnderlayDilate", 0.25f);
                                MatchAudio.Instance.ScoreLose();
                            }
                            else
                            {
                                GameObject.Find(OpponentScore_Gob.name + "/Number_Txt").GetComponent<TMPro.TextMeshProUGUI>().faceColor = BlueInnerColor_Col32;
                                GameObject.Find(OpponentScore_Gob.name + "/Number_Txt").GetComponent<TMPro.TextMeshProUGUI>().outlineColor = BlueOutlineColor_Col32;
                                GameObject.Find(OpponentScore_Gob.name + "/Number_Txt").GetComponent<TMPro.TextMeshProUGUI>().fontMaterial.SetColor("_UnderlayColor", BlueOutlineColor_Col32);
                                GameObject.Find(OpponentScore_Gob.name + "/Number_Txt").GetComponent<TMPro.TextMeshProUGUI>().fontMaterial.SetFloat("_UnderlayDilate", 0.25f);
                                MatchAudio.Instance.ScoreWin();
                            }

                            if (CurrentMode_Str == "PC")
                            { DisplayResultUI(false, ExecuteActionTeamBlue_Pr.Team == UserData.Instance.PVPTeam.ToString()); }

                            if (CurrentMode_Str.Contains("NPC"))
                            { DisplayResultUI(true, ExecuteActionTeamBlue_Pr.Team == UserData.Instance.PVPTeam.ToString()); }

                            if (CurrentMode_Str.Contains("PVP"))
                            { DisplayResultUI(false, ExecuteActionTeamBlue_Pr.Team == UserData.Instance.PVPTeam.ToString()); }
                        }
                    }
                }
            }
            else
            { startDecrementingScore_Bol = false; }
        }

        return startDecrementingScore_Bol;
        #endregion
    }

    public void PlaceHUD(GameObject HUD_Gob, GameObject Target_Gob)
    {
        #region Place HUD According To Camera Position & Target Position

        GameObject headTopLocalPivot_Gob = GameObject.Find(Target_Gob.name + "/HeadTop_Local_Pivot");
        //headTopLocalPivot_Gob.transform.rotation = Camera.main.transform.rotation;

        GameObject tipPoint_Gob = GameObject.Find(Target_Gob.name + "/HeadTop_Local_Pivot/Tip_Point");
        Vector3 seletedPos_Vct;

        if (CurrentResolutionCase_Str == "Toss_Resolution")
            seletedPos_Vct = tipPoint_Gob.transform.position;
        else
            seletedPos_Vct = Camera.main.WorldToScreenPoint(tipPoint_Gob.transform.position);

        if (CurrentResolutionCase_Str == "Toss_Resolution")
        { HUD_Gob.GetComponent<RectTransform>().position = new Vector3(seletedPos_Vct.x + XoffsetHUD_Flt, seletedPos_Vct.y + YoffsetHUD_Flt, seletedPos_Vct.z); }
        else
        {
            //print(HUD_Gob.name);
            //print(Target_Gob.name);
            //TEAM_A-G/[OverlayCanvas]/Player_Name_TMPText
            //headTopLocalPivot_Gob.transform.rotation = Camera.main.transform.rotation;
            //HUD_Gob.GetComponent<RectTransform>().position = new Vector3(seletedPos_Vct.x + XoffsetHUD_Flt, seletedPos_Vct.y + YoffsetHUD_Flt + 10, seletedPos_Vct.z);

            headTopLocalPivot_Gob = GameObject.Find(Target_Gob.name + "/[OverlayCanvas]/Player_Name_TMPText");
            HUD_Gob.GetComponent<RectTransform>().position = new Vector3(headTopLocalPivot_Gob.GetComponent<RectTransform>().position.x, headTopLocalPivot_Gob.GetComponent<RectTransform>().position.y - 10, headTopLocalPivot_Gob.GetComponent<RectTransform>().position.z);

            if (HUD_Gob.GetComponent<Events_Utility>())
            {
                HUD_Gob.GetComponent<Events_Utility>().Target_Gob = headTopLocalPivot_Gob;
                HUD_Gob.GetComponent<Events_Utility>().Param_Str = "Track_Minion_Name_TMPText";
            }
        }

        //
        //
        //

        GameObject diceOpponent_Gob = GameObject.Find("Opponent_1");

        if (diceOpponent_Gob != null)
        {
            GameObject dicePlayer_Gob = GameObject.Find("Player_1");

            if (dicePlayer_Gob.GetComponent<RectTransform>().position.x > diceOpponent_Gob.GetComponent<RectTransform>().position.x)
            {
                for (int i = 0; i < 5; i++)
                {
                    GameObject diceImg_Gob = GameObject.Find(diceOpponent_Gob.name + "/" + i + "_Dice_Img");
                    diceImg_Gob.GetComponent<RectTransform>().localScale = new Vector3(-0.6f, 0.6f, 0.6f);

                    diceImg_Gob = GameObject.Find(dicePlayer_Gob.name + "/" + i + "_Dice_Img");
                    diceImg_Gob.GetComponent<RectTransform>().localScale = new Vector3(0.6f, 0.6f, 0.6f);
                }
            }
            else
            {
                for (int i = 0; i < 5; i++)
                {
                    GameObject diceImg_Gob = GameObject.Find(diceOpponent_Gob.name + "/" + i + "_Dice_Img");
                    diceImg_Gob.GetComponent<RectTransform>().localScale = new Vector3(0.6f, 0.6f, 0.6f);

                    diceImg_Gob = GameObject.Find(dicePlayer_Gob.name + "/" + i + "_Dice_Img");
                    diceImg_Gob.GetComponent<RectTransform>().localScale = new Vector3(-0.6f, 0.6f, 0.6f);
                }
            }
        }

        //
        //
        //
        //

        diceOpponent_Gob = GameObject.Find("Opponent_2");

        if (diceOpponent_Gob != null)
        {
            GameObject dicePlayer_Gob = GameObject.Find("Player_2");

            if (dicePlayer_Gob.GetComponent<RectTransform>().position.x > diceOpponent_Gob.GetComponent<RectTransform>().position.x)
            {
                for (int i = 0; i < 5; i++)
                {
                    GameObject diceImg_Gob = GameObject.Find(diceOpponent_Gob.name + "/" + i + "_Dice_Img");
                    diceImg_Gob.GetComponent<RectTransform>().localScale = new Vector3(-0.6f, 0.6f, 0.6f);

                    diceImg_Gob = GameObject.Find(dicePlayer_Gob.name + "/" + i + "_Dice_Img");
                    diceImg_Gob.GetComponent<RectTransform>().localScale = new Vector3(0.6f, 0.6f, 0.6f);
                }
            }
            else
            {
                for (int i = 0; i < 5; i++)
                {
                    GameObject diceImg_Gob = GameObject.Find(diceOpponent_Gob.name + "/" + i + "_Dice_Img");
                    diceImg_Gob.GetComponent<RectTransform>().localScale = new Vector3(0.6f, 0.6f, 0.6f);

                    diceImg_Gob = GameObject.Find(dicePlayer_Gob.name + "/" + i + "_Dice_Img");
                    diceImg_Gob.GetComponent<RectTransform>().localScale = new Vector3(-0.6f, 0.6f, 0.6f);
                }
            }
        }

        #endregion
    }
       
    /// <summary>
    /// Instantiate score prefab for pressure value
    /// </summary>
    /// <param name="pPressureToBlueMinion">Is pressure applied to blue minion ?</param>
    /// <param name="pActionType">Minion pressured action</param>
    /// <param name="pPressureValue">Pressure effect value</param>
    public void DisplayPressureUI(bool pPressureToBlueMinion, string pActionType, int pPressureValue)
    {
        //-------------------------------------------------------------------------------> INSTANTIATE NEW TEXT PREFAB 
        GameObject lPressureGO  =   Instantiate(PrefabsIdxArray_Gob[Array.IndexOf(PrefabsIdxArray_Str, "Score_Display_Grp")], Vector3.zero, Quaternion.identity);
        //lPressureGO.GetComponent<Animator>().SetTrigger(pPressureToBlueMinion ? "Red_Pressure" : "Blue_Pressure");
        lPressureGO.GetComponent<Animator>().SetTrigger("Red_Pressure");
        lPressureGO.name        =   "Random_Score_" + LocalIdx_Int;
        LocalIdx_Int++;

        GameObject  parent_Gob  =   GameObject.Find("Action_Score_" + pActionType + "_Btn");
        lPressureGO.transform.SetParent(parent_Gob.transform, false);
        lPressureGO.GetComponent<RectTransform>().anchoredPosition = new Vector2(0f, 140f);

        GameObject  lPressureTxt    =   GameObject.Find(lPressureGO.name + "/Score_TMPText");
        lPressureTxt.GetComponent<TMPro.TextMeshProUGUI>().text =   "-" + pPressureValue;
    }

    public void DisplayResultUI(bool pTeamBlueWon, bool pTeamBlueIsCurManager = true)
    {
        if (null == TeamBlueScoreDisplay || null == TeamRedScoreDisplay)
            return;

        if (pTeamBlueWon)
        {
            TeamBlueScoreDisplay.GetComponent<Animator>().enabled = false;
            //TeamBlueScoreDisplay.transform.GetChild(0).GetComponent<TMPro.TMP_Text>().color =   ActionsManager.Instance.ColorConfig.GetTeamColor(pTeamBlueIsCurManager);

            if (pTeamBlueIsCurManager)
            {
                TeamBlueScoreDisplay.transform.GetChild(0).GetComponent<TMPro.TextMeshProUGUI>().faceColor = new Color32(66, 248, 255, 255);
                TeamBlueScoreDisplay.transform.GetChild(0).GetComponent<TMPro.TextMeshProUGUI>().outlineColor = new Color32(0, 156, 255, 255);
                TeamBlueScoreDisplay.transform.GetChild(0).GetComponent<TMPro.TextMeshProUGUI>().fontMaterial.SetColor("_UnderlayColor", new Color32(0, 156, 255, 255));
                TeamBlueScoreDisplay.transform.GetChild(0).GetComponent<TMPro.TextMeshProUGUI>().fontMaterial.SetFloat("_UnderlayDilate", 0.25f);
            }
            else
            {
                TeamBlueScoreDisplay.transform.GetChild(0).GetComponent<TMPro.TextMeshProUGUI>().faceColor = new Color32(255, 0, 90, 255);
                TeamBlueScoreDisplay.transform.GetChild(0).GetComponent<TMPro.TextMeshProUGUI>().outlineColor = new Color32(170, 0, 60, 255);
                TeamBlueScoreDisplay.transform.GetChild(0).GetComponent<TMPro.TextMeshProUGUI>().fontMaterial.SetColor("_UnderlayColor", new Color32(170, 0, 60, 255));
                TeamBlueScoreDisplay.transform.GetChild(0).GetComponent<TMPro.TextMeshProUGUI>().fontMaterial.SetFloat("_UnderlayDilate", 0.25f);
            }

            TeamBlueScoreDisplay.transform.localScale = new Vector3(2f, 2f, 1f);
            TeamBlueScoreDisplay.transform.parent.GetChild(4).localEulerAngles    =   new Vector3(180f, 0f, 0f);
            TeamBlueScoreDisplay.transform.parent.GetChild(4).localPosition       =   new Vector3(0f, 1f, 0f);
            TeamBlueScoreDisplay.transform.parent.GetChild(4).gameObject.SetActive(true);
            //MatchAudio.Instance.ScoreWin();
            TeamRedScoreDisplay.GetComponent<Animator>().enabled = false;
            TeamRedScoreDisplay.transform.GetChild(0).GetComponent<TMPro.TextMeshProUGUI>().color = Color.gray;
        }
        else if (!pTeamBlueWon)
        {
            TeamRedScoreDisplay.GetComponent<Animator>().enabled = false;
            //TeamRedScoreDisplay.transform.GetChild(0).GetComponent<TMPro.TMP_Text>().color  =   ActionsManager.Instance.ColorConfig.GetTeamColor(!pTeamBlueIsCurManager);

            if (pTeamBlueIsCurManager)
            {
                TeamRedScoreDisplay.transform.GetChild(0).GetComponent<TMPro.TextMeshProUGUI>().faceColor = new Color32(255, 0, 90, 255);
                TeamRedScoreDisplay.transform.GetChild(0).GetComponent<TMPro.TextMeshProUGUI>().outlineColor = new Color32(170, 0, 60, 255);
                TeamRedScoreDisplay.transform.GetChild(0).GetComponent<TMPro.TextMeshProUGUI>().fontMaterial.SetColor("_UnderlayColor", new Color32(170, 0, 60, 255));
                TeamRedScoreDisplay.transform.GetChild(0).GetComponent<TMPro.TextMeshProUGUI>().fontMaterial.SetFloat("_UnderlayDilate", 0.25f);
            }
            else
            {
                TeamRedScoreDisplay.transform.GetChild(0).GetComponent<TMPro.TextMeshProUGUI>().faceColor = new Color32(66, 248, 255, 255);
                TeamRedScoreDisplay.transform.GetChild(0).GetComponent<TMPro.TextMeshProUGUI>().outlineColor = new Color32(0, 156, 255, 255);
                TeamRedScoreDisplay.transform.GetChild(0).GetComponent<TMPro.TextMeshProUGUI>().fontMaterial.SetColor("_UnderlayColor", new Color32(0, 156, 255, 255));
                TeamRedScoreDisplay.transform.GetChild(0).GetComponent<TMPro.TextMeshProUGUI>().fontMaterial.SetFloat("_UnderlayDilate", 0.25f);
            }

            TeamRedScoreDisplay.transform.localScale = new Vector3(2f, 2f, 1f);
            TeamRedScoreDisplay.transform.parent.GetChild(4).localEulerAngles     =   new Vector3(180f, 0f, 0f);
            TeamRedScoreDisplay.transform.parent.GetChild(4).localPosition        =   new Vector3(0f, 1f, 0f);
            TeamRedScoreDisplay.transform.parent.GetChild(4).gameObject.SetActive(true);
            //MatchAudio.Instance.ScoreLose();
            TeamBlueScoreDisplay.GetComponent<Animator>().enabled = false;
            TeamBlueScoreDisplay.transform.GetChild(0).GetComponent<TMPro.TextMeshProUGUI>().color = Color.gray;
        }
    }

    private IEnumerator _focusOnReception()
    {
        #region Focus Camera On Receiving Pass

        if (CurrentResolutionCase_Str.Contains("Pass"))
        {
            //bool lPasserIsFromBlueTeam = PassResolution_Prr.Passer.Team == UserData.Instance.PVPTeam.ToString();
            //PlayerPVP lReceiver = PlayersManager.Instance.GetPlayer(PassResolution_Prr.Receiver.Team, PassResolution_Prr.Receiver.Position);

            //if (null != PassResolution_Prr.ReceiverOpponentDuel)
            //{
            //    PlayerPVP lReceiverOpp = PlayersManager.Instance.GetPlayer(PassResolution_Prr.ReceiverOpponentDuel.Team, PassResolution_Prr.ReceiverOpponentDuel.Position);

            //    MatchCamera.Instance.FocusBetweenPlayers(new GameObject[2] { lReceiver.gameObject, lReceiverOpp.gameObject });
            //    ActionsUI.Instance.ToggleActionBoard(true);

            //    yield return new WaitForSeconds(0.5f);

            //    ActionsManager.Instance.DisplayReceptionQuad();
            //    OppositionUI.Instance.HandlePressureOpponents(PassResolution_Prr);

            //    ActionsUI.Instance.RevealAction(lPasserIsFromBlueTeam, eAction.RECEPTION, PassResolution_Prr.Receiver.MinScore, PassResolution_Prr.Receiver.MaxScore);
            //    ActionsUI.Instance.RevealAction(!lPasserIsFromBlueTeam, eAction.DUEL_DEFENSE, PassResolution_Prr.ReceiverOpponentDuel.MinScore, PassResolution_Prr.ReceiverOpponentDuel.MaxScore);

            //    ActionsUI.Instance.SetPlayerInfos(true, lPasserIsFromBlueTeam ? lReceiver.Data : lReceiverOpp.Data);
            //    ActionsUI.Instance.SetPlayerInfos(false, lPasserIsFromBlueTeam ? lReceiverOpp.Data : lReceiver.Data);

            //    ActionsUI.Instance.StartResolution(false);

            //    yield return new WaitForSeconds(MatchCamera.Instance.TimeToWait + 0.5f);
            //}
            //else
            //{
                //ActionsUI.Instance.ToggleActionBoard(false);
                //ActionsUI.Instance.ClearResolution();

                MatchCamera.Instance.DisplayResolutionView();
                yield return new WaitForSeconds(MatchCamera.Instance.TimeToWait);
            //}
        }

        //CameraInPlace   =   false;

        #endregion
    }

    public IEnumerator DelaySoundEffect(string sound_Str, float delay_Flt)
    {
        #region Delay Sound Effect
        yield return new WaitForSeconds(delay_Flt);

        if (sound_Str == "Kick")
        { MatchAudio.Instance.Kick(); }

        if (sound_Str == "PressionImpact")
        { MatchAudio.Instance.PressionImpact(); }
        #endregion
    }

    public IEnumerator InstantiateCustomResolutionFX(GameObject selectedMinion_Gob, PlayerResponse selectedMinion_Pr, PlayerActionResponse selectedMinion_Par, string selectedAnim_Str, bool interceptor_Bol, bool receiver_Bol,
        string sprintScore_Str)
    {
        #region Create Custom Draw FX 

        GameObject minionDrawGrp_Gob = Instantiate(PrefabsIdxArray_Gob[Array.IndexOf(PrefabsIdxArray_Str, "Minion_Draw_Grp")], Vector3.zero, Quaternion.identity);
        minionDrawGrp_Gob.name = selectedMinion_Gob.name + "_Minion_Draw_Grp";//chosenPlayerIdx_Str;
        minionDrawGrp_Gob.transform.SetParent(gameObject.transform, false);
        minionDrawGrp_Gob.GetComponent<RectTransform>().position = Camera.main.WorldToScreenPoint(GameObject.Find(selectedMinion_Gob.name + "/[Animator] Character/Character_Mesh_01/Tip_Point_Ref_Pos").transform.position);
        minionDrawGrp_Gob.GetComponent<Animator>().SetTrigger(selectedAnim_Str);

        minionDrawGrp_Gob.GetComponent<Events_Utility>().Target_Gob = selectedMinion_Gob;
        minionDrawGrp_Gob.GetComponent<Events_Utility>().Param_Str = "Track_Minion_Name_TMPText";

        if (sprintScore_Str == "Null")
        {
            if (selectedMinion_Pr != null)
            {
                if (!CurrentResolutionData_Dic.ContainsKey(selectedMinion_Gob.name + "_Score_After_Pressure"))
                { CurrentResolutionData_Dic.Add(selectedMinion_Gob.name + "_Score_After_Pressure", selectedMinion_Pr.ExecuteAction.ScoreWithPressure); }

                if (!CurrentResolutionData_Dic.ContainsKey(selectedMinion_Gob.name + "_Score_Before_Pressure"))
                { CurrentResolutionData_Dic.Add(selectedMinion_Gob.name + "_Score_Before_Pressure", selectedMinion_Pr.ExecuteAction.Score); }

                if (!CurrentResolutionData_Dic.ContainsKey(selectedMinion_Gob.name + "_Score_Level"))
                { CurrentResolutionData_Dic.Add(selectedMinion_Gob.name + "_Score_Level", PlayersManager.Instance.GetPlayer(eTeamMethods.ConvertStringToETeam(selectedMinion_Pr.Team), ePositionMethods.ConvertStringToEPosition(selectedMinion_Pr.Position)).Data.FormLevel); }

                if (!CurrentResolutionData_Dic.ContainsKey(selectedMinion_Gob.name + "_Score_Min_Range"))
                { CurrentResolutionData_Dic.Add(selectedMinion_Gob.name + "_Score_Min_Range", selectedMinion_Pr.ExecuteAction.MinScore); }

                if (!CurrentResolutionData_Dic.ContainsKey(selectedMinion_Gob.name + "_Score_Max_Range"))
                { CurrentResolutionData_Dic.Add(selectedMinion_Gob.name + "_Score_Max_Range", selectedMinion_Pr.ExecuteAction.MaxScore); }

                GameObject.Find(minionDrawGrp_Gob.name + "/Score_Txt").GetComponent<TMPro.TextMeshProUGUI>().text = selectedMinion_Pr.ExecuteAction.ScoreWithPressure.ToString();
                GameObject.Find(minionDrawGrp_Gob.name + "/Range_Txt").GetComponent<TMPro.TextMeshProUGUI>().text = "[" + selectedMinion_Pr.ExecuteAction.MinScore.ToString() + "-" + selectedMinion_Pr.ExecuteAction.MaxScore.ToString() + "]";
            }

            if (selectedMinion_Par != null)
            {
                if (!CurrentResolutionData_Dic.ContainsKey(selectedMinion_Gob.name + "_Score_After_Pressure"))
                { CurrentResolutionData_Dic.Add(selectedMinion_Gob.name + "_Score_After_Pressure", selectedMinion_Par.ScoreWithPressure); }

                if (!CurrentResolutionData_Dic.ContainsKey(selectedMinion_Gob.name + "_Score_Before_Pressure"))
                { CurrentResolutionData_Dic.Add(selectedMinion_Gob.name + "_Score_Before_Pressure", selectedMinion_Par.Score); }

                if (!CurrentResolutionData_Dic.ContainsKey(selectedMinion_Gob.name + "_Score_Level"))
                { CurrentResolutionData_Dic.Add(selectedMinion_Gob.name + "_Score_Level", PlayersManager.Instance.GetPlayer(eTeamMethods.ConvertStringToETeam(selectedMinion_Par.Team), ePositionMethods.ConvertStringToEPosition(selectedMinion_Par.Position)).Data.FormLevel); }

                if (!CurrentResolutionData_Dic.ContainsKey(selectedMinion_Gob.name + "_Score_Min_Range"))
                { CurrentResolutionData_Dic.Add(selectedMinion_Gob.name + "_Score_Min_Range", selectedMinion_Par.MinScore); }

                if (!CurrentResolutionData_Dic.ContainsKey(selectedMinion_Gob.name + "_Score_Max_Range"))
                { CurrentResolutionData_Dic.Add(selectedMinion_Gob.name + "_Score_Max_Range", selectedMinion_Par.MaxScore); }

                GameObject.Find(minionDrawGrp_Gob.name + "/Score_Txt").GetComponent<TMPro.TextMeshProUGUI>().text = selectedMinion_Par.ScoreWithPressure.ToString();
                GameObject.Find(minionDrawGrp_Gob.name + "/Range_Txt").GetComponent<TMPro.TextMeshProUGUI>().text = "[" + selectedMinion_Par.MinScore.ToString() + "-" + selectedMinion_Par.MaxScore.ToString() + "]";
            }

            MinionDrawGrpLst_Gob.Add(minionDrawGrp_Gob);

            yield return new WaitForSeconds(0.3f);

            if (selectedAnim_Str.Contains("Win"))
            {
                if (!interceptor_Bol)
                {
                    if (!receiver_Bol)
                    {
                        if (!minionDrawGrp_Gob.name.Contains(UserData.Instance.PVPTeam.ToString()))
                        {
                            GameObject.Find(minionDrawGrp_Gob.name + "/Score_Txt").GetComponent<TMPro.TextMeshProUGUI>().faceColor = RedInnerColor_Col32;
                            GameObject.Find(minionDrawGrp_Gob.name + "/Score_Txt").GetComponent<TMPro.TextMeshProUGUI>().outlineColor = RedOutlineColor_Col32;
                            GameObject.Find(minionDrawGrp_Gob.name + "/Score_Txt").GetComponent<TMPro.TextMeshProUGUI>().fontMaterial.SetColor("_UnderlayColor", RedOutlineColor_Col32);
                            GameObject.Find(minionDrawGrp_Gob.name + "/Score_Txt").GetComponent<TMPro.TextMeshProUGUI>().fontMaterial.SetFloat("_UnderlayDilate", 0.25f);
                        }
                        else
                        {
                            GameObject.Find(minionDrawGrp_Gob.name + "/Score_Txt").GetComponent<TMPro.TextMeshProUGUI>().faceColor = BlueInnerColor_Col32;
                            GameObject.Find(minionDrawGrp_Gob.name + "/Score_Txt").GetComponent<TMPro.TextMeshProUGUI>().outlineColor = BlueOutlineColor_Col32;
                            GameObject.Find(minionDrawGrp_Gob.name + "/Score_Txt").GetComponent<TMPro.TextMeshProUGUI>().fontMaterial.SetColor("_UnderlayColor", BlueOutlineColor_Col32);
                            GameObject.Find(minionDrawGrp_Gob.name + "/Score_Txt").GetComponent<TMPro.TextMeshProUGUI>().fontMaterial.SetFloat("_UnderlayDilate", 0.25f);
                        }
                    }
                    else
                    {
                        if (!minionDrawGrp_Gob.name.Contains(UserData.Instance.PVPTeam.ToString()))
                        {
                            GameObject.Find(minionDrawGrp_Gob.name + "/Score_Txt").GetComponent<TMPro.TextMeshProUGUI>().faceColor = RedInnerColor_Col32;
                            GameObject.Find(minionDrawGrp_Gob.name + "/Score_Txt").GetComponent<TMPro.TextMeshProUGUI>().outlineColor = RedOutlineColor_Col32;
                            GameObject.Find(minionDrawGrp_Gob.name + "/Score_Txt").GetComponent<TMPro.TextMeshProUGUI>().fontMaterial.SetColor("_UnderlayColor", RedOutlineColor_Col32);
                            GameObject.Find(minionDrawGrp_Gob.name + "/Score_Txt").GetComponent<TMPro.TextMeshProUGUI>().fontMaterial.SetFloat("_UnderlayDilate", 0.25f);
                        }
                        else
                        {
                            GameObject.Find(minionDrawGrp_Gob.name + "/Score_Txt").GetComponent<TMPro.TextMeshProUGUI>().faceColor = BlueInnerColor_Col32;
                            GameObject.Find(minionDrawGrp_Gob.name + "/Score_Txt").GetComponent<TMPro.TextMeshProUGUI>().outlineColor = BlueOutlineColor_Col32;
                            GameObject.Find(minionDrawGrp_Gob.name + "/Score_Txt").GetComponent<TMPro.TextMeshProUGUI>().fontMaterial.SetColor("_UnderlayColor", BlueOutlineColor_Col32);
                            GameObject.Find(minionDrawGrp_Gob.name + "/Score_Txt").GetComponent<TMPro.TextMeshProUGUI>().fontMaterial.SetFloat("_UnderlayDilate", 0.25f);
                        }
                    }
                }
                else
                {
                    if (minionDrawGrp_Gob.name.Contains(UserData.Instance.PVPTeam.ToString()))
                    {
                        GameObject.Find(minionDrawGrp_Gob.name + "/Score_Txt").GetComponent<TMPro.TextMeshProUGUI>().faceColor = BlueInnerColor_Col32;
                        GameObject.Find(minionDrawGrp_Gob.name + "/Score_Txt").GetComponent<TMPro.TextMeshProUGUI>().outlineColor = BlueOutlineColor_Col32;
                        GameObject.Find(minionDrawGrp_Gob.name + "/Score_Txt").GetComponent<TMPro.TextMeshProUGUI>().fontMaterial.SetColor("_UnderlayColor", BlueOutlineColor_Col32);
                        GameObject.Find(minionDrawGrp_Gob.name + "/Score_Txt").GetComponent<TMPro.TextMeshProUGUI>().fontMaterial.SetFloat("_UnderlayDilate", 0.25f);
                    }
                    else
                    {
                        GameObject.Find(minionDrawGrp_Gob.name + "/Score_Txt").GetComponent<TMPro.TextMeshProUGUI>().faceColor = RedInnerColor_Col32;
                        GameObject.Find(minionDrawGrp_Gob.name + "/Score_Txt").GetComponent<TMPro.TextMeshProUGUI>().outlineColor = RedOutlineColor_Col32;
                        GameObject.Find(minionDrawGrp_Gob.name + "/Score_Txt").GetComponent<TMPro.TextMeshProUGUI>().fontMaterial.SetColor("_UnderlayColor", RedOutlineColor_Col32);
                        GameObject.Find(minionDrawGrp_Gob.name + "/Score_Txt").GetComponent<TMPro.TextMeshProUGUI>().fontMaterial.SetFloat("_UnderlayDilate", 0.25f);
                    }
                }

                minionDrawGrp_Gob.transform.SetAsLastSibling();

                MatchAudio.Instance.ScoreWin();
            }

            if (selectedAnim_Str.Contains("Lose"))
            {
                GameObject.Find(minionDrawGrp_Gob.name + "/Score_Txt").GetComponent<TMPro.TextMeshProUGUI>().faceColor = Color.gray;
                GameObject.Find(minionDrawGrp_Gob.name + "/Score_Txt").GetComponent<TMPro.TextMeshProUGUI>().outlineColor = Color.white;
                GameObject.Find(minionDrawGrp_Gob.name + "/Score_Txt").GetComponent<TMPro.TextMeshProUGUI>().fontMaterial.SetColor("_UnderlayColor", Color.black);
                GameObject.Find(minionDrawGrp_Gob.name + "/Score_Txt").GetComponent<TMPro.TextMeshProUGUI>().fontMaterial.SetFloat("_UnderlayDilate", 0.25f);

                MatchAudio.Instance.ScoreLose();
            }
        }
        else
        {
            GameObject.Find(minionDrawGrp_Gob.name + "/Score_Txt").GetComponent<TMPro.TextMeshProUGUI>().text = selectedMinion_Pr.ExecuteAction.ScoreWithPressure.ToString();
            GameObject.Find(minionDrawGrp_Gob.name + "/Range_Txt").GetComponent<TMPro.TextMeshProUGUI>().text = "[" + selectedMinion_Pr.ExecuteAction.MinScore.ToString() + "-" + selectedMinion_Pr.ExecuteAction.MaxScore.ToString() + "]";

            if (selectedAnim_Str.Contains("Win"))
            {
                if (minionDrawGrp_Gob.name.Contains(UserData.Instance.PVPTeam.ToString()))
                {
                    if (CurrentMode_Str.Contains("NPC"))
                    {
                        GameObject.Find(minionDrawGrp_Gob.name + "/Score_Txt").GetComponent<TMPro.TextMeshProUGUI>().faceColor = RedInnerColor_Col32;
                        GameObject.Find(minionDrawGrp_Gob.name + "/Score_Txt").GetComponent<TMPro.TextMeshProUGUI>().outlineColor = RedOutlineColor_Col32;
                        GameObject.Find(minionDrawGrp_Gob.name + "/Score_Txt").GetComponent<TMPro.TextMeshProUGUI>().fontMaterial.SetColor("_UnderlayColor", RedOutlineColor_Col32);
                        GameObject.Find(minionDrawGrp_Gob.name + "/Score_Txt").GetComponent<TMPro.TextMeshProUGUI>().fontMaterial.SetFloat("_UnderlayDilate", 0.25f);
                    }
                    else
                    {
                        GameObject.Find(minionDrawGrp_Gob.name + "/Score_Txt").GetComponent<TMPro.TextMeshProUGUI>().faceColor = BlueInnerColor_Col32;
                        GameObject.Find(minionDrawGrp_Gob.name + "/Score_Txt").GetComponent<TMPro.TextMeshProUGUI>().outlineColor = BlueOutlineColor_Col32;
                        GameObject.Find(minionDrawGrp_Gob.name + "/Score_Txt").GetComponent<TMPro.TextMeshProUGUI>().fontMaterial.SetColor("_UnderlayColor", BlueOutlineColor_Col32);
                        GameObject.Find(minionDrawGrp_Gob.name + "/Score_Txt").GetComponent<TMPro.TextMeshProUGUI>().fontMaterial.SetFloat("_UnderlayDilate", 0.25f);
                    }
                }
                else
                {
                    if (CurrentMode_Str.Contains("NPC"))
                    {
                        GameObject.Find(minionDrawGrp_Gob.name + "/Score_Txt").GetComponent<TMPro.TextMeshProUGUI>().faceColor = BlueInnerColor_Col32;
                        GameObject.Find(minionDrawGrp_Gob.name + "/Score_Txt").GetComponent<TMPro.TextMeshProUGUI>().outlineColor = BlueOutlineColor_Col32;
                        GameObject.Find(minionDrawGrp_Gob.name + "/Score_Txt").GetComponent<TMPro.TextMeshProUGUI>().fontMaterial.SetColor("_UnderlayColor", BlueOutlineColor_Col32);
                        GameObject.Find(minionDrawGrp_Gob.name + "/Score_Txt").GetComponent<TMPro.TextMeshProUGUI>().fontMaterial.SetFloat("_UnderlayDilate", 0.25f);
                    }
                    else
                    {
                        GameObject.Find(minionDrawGrp_Gob.name + "/Score_Txt").GetComponent<TMPro.TextMeshProUGUI>().faceColor = RedInnerColor_Col32;
                        GameObject.Find(minionDrawGrp_Gob.name + "/Score_Txt").GetComponent<TMPro.TextMeshProUGUI>().outlineColor = RedOutlineColor_Col32;
                        GameObject.Find(minionDrawGrp_Gob.name + "/Score_Txt").GetComponent<TMPro.TextMeshProUGUI>().fontMaterial.SetColor("_UnderlayColor", RedOutlineColor_Col32);
                        GameObject.Find(minionDrawGrp_Gob.name + "/Score_Txt").GetComponent<TMPro.TextMeshProUGUI>().fontMaterial.SetFloat("_UnderlayDilate", 0.25f);
                    }
                }

                minionDrawGrp_Gob.transform.SetAsLastSibling();

                MatchAudio.Instance.ScoreWin();
            }

            if (selectedAnim_Str.Contains("Lose"))
            {
                GameObject.Find(minionDrawGrp_Gob.name + "/Score_Txt").GetComponent<TMPro.TextMeshProUGUI>().faceColor = Color.gray;
                GameObject.Find(minionDrawGrp_Gob.name + "/Score_Txt").GetComponent<TMPro.TextMeshProUGUI>().outlineColor = Color.white;
                GameObject.Find(minionDrawGrp_Gob.name + "/Score_Txt").GetComponent<TMPro.TextMeshProUGUI>().fontMaterial.SetColor("_UnderlayColor", Color.black);
                GameObject.Find(minionDrawGrp_Gob.name + "/Score_Txt").GetComponent<TMPro.TextMeshProUGUI>().fontMaterial.SetFloat("_UnderlayDilate", 0.25f);

                MatchAudio.Instance.ScoreLose();
            }
        }

        #endregion
    }

    public bool CheckMinionDrawGrp()
    {
        #region Verify If All Minion Draw Grp Lst Finish Theirs Animations
        bool allMinionDrawAreDone_Bol = true;

        foreach (var catch_Var in MinionDrawGrpLst_Gob)
        {
            if (GameObject.Find(catch_Var.name + "/Score_Txt").GetComponent<TMPro.TextMeshProUGUI>().enabled)
            { allMinionDrawAreDone_Bol = false; }
        }

        return allMinionDrawAreDone_Bol;
        #endregion
    }

    bool CheckMinionTarget()
    {
        #region Verify If All Minion Reach Theirs Targets
        bool allMinionReachTarget_Bol = true;

        foreach (var movingMinion_Var in _allMovingMinionsTargets_Dic)
        {
            if (movingMinion_Var.Key.transform.position != movingMinion_Var.Value)
            { allMinionReachTarget_Bol = false; }
        }

        if (allMinionReachTarget_Bol)
        {
            foreach (var movingMinion_Var in _allMovingMinionsTargets_Dic)
            {
                //Current Moving Minion Params
                GameObject _movingMinion_Gob = movingMinion_Var.Key;
                GameObject _movingAnimator_Gob = GameObject.Find(_movingMinion_Gob.name + "/[Animator] Character/Character_Mesh_01");
                //GameObject _movingAim_Gob = GameObject.Find(_movingMinion_Gob.name + "/[Animator] Character");

                Animator m_Animator = _movingAnimator_Gob.GetComponent<Animator>();
                AnimatorClipInfo[] m_CurrentClipInfo = m_Animator.GetCurrentAnimatorClipInfo(0);
                //print(_movingMinion_Gob.name + " -----> Current Animation : " + m_CurrentClipInfo[0].clip.name);

                //Activate Idle
                if (movingMinion_Var.Key.transform.position == movingMinion_Var.Value && m_CurrentClipInfo[0].clip.name == "Sprint")
                {
                    /*_movingMinion_Gob.GetComponent<Player3D>().CurrentAnim_Str = "Idle";
                    _movingAnimator_Gob.GetComponent<Animator>().SetTrigger("Idle_In_Game");*/
                }
            }
        }

        return allMinionReachTarget_Bol;
        #endregion
    }

    #region Resolution Vars

    private float ArrowMvtSpeed_Flt = 4f;//6
    private float BallMvtSpeed_Flt = 2.5f;//1.5
    private float LineFadeOutSpeed_Flt = 1;

    private float ArrowTargetStopDistance_Flt = 0.4f;
    private float BallTargetStopDistance_Flt = 0.1f; // > Always Superior To 0.1f

    private List<GameObject> MinionDrawGrpLst_Gob = new List<GameObject>();

    Dictionary<GameObject, float> _allMovingMinionsSpeed_Dic = new Dictionary<GameObject, float>();
    Dictionary<GameObject, PlayerResponse> _allMovingMinionsPR_Dic = new Dictionary<GameObject, PlayerResponse>();
    Dictionary<GameObject, PlayerMove> _allMovingMinionsPM_Dic = new Dictionary<GameObject, PlayerMove>();
    Dictionary<GameObject, Vector3> _allMovingMinionsTargets_Dic = new Dictionary<GameObject, Vector3>();

    Dictionary<string, string> _allMovingMinionsContacts_Dic = new Dictionary<string, String>();

    PlayersMove lPlayersMove;// = MatchEngine.Instance.Engine.ActionMain.PlayersMove;

    #endregion

    public IEnumerator SprintMovePass(string param_Str)
    {
        #region Start Moving Minions 

        //List Containing All Moving Players Data
        GameObject ball_Gob = GameObject.Find("[Ball]");
        bool ActivateMove_Bol = true;
        lPlayersMove = MatchEngine.Instance.Engine.ActionMain.PlayersMove;

        print("Minions Moving List Count = " + lPlayersMove.Players.Count);

        if (ActivateMove_Bol)
        {
            Vector3 _currentPos_Vct;
            Vector3 _targetPos_Vct;
            string firstMinionControllingBall_Str = "Null";

            //Store All Minions Default Speed [1]
            foreach (var movingMinion_Var in lPlayersMove.Players)
            {
                GameObject _movingMinion_Gob = GameObject.Find(movingMinion_Var.Player.Team.ToString() + '-' + movingMinion_Var.Player.Position.ToString());

                if (!_allMovingMinionsSpeed_Dic.ContainsKey(_movingMinion_Gob))
                {
                    PlayerResponse lPlayerResponse = MatchEngine.Instance.Engine.ActionMain.GetPlayerResponse(movingMinion_Var.Team, movingMinion_Var.Position);

                    _allMovingMinionsPM_Dic.Add(_movingMinion_Gob, movingMinion_Var);
                    _allMovingMinionsSpeed_Dic.Add(_movingMinion_Gob, 0.75f);

                    if (movingMinion_Var.IsStaticPlayer)
                    { _allMovingMinionsTargets_Dic.Add(_movingMinion_Gob, _movingMinion_Gob.transform.position); }
                    else
                    { _allMovingMinionsTargets_Dic.Add(_movingMinion_Gob, PlayersManager.Instance.CalculatePositionByCoordinates(movingMinion_Var.FieldCaseTarget)); }

                    if (null != lPlayerResponse)
                    { 
                        _allMovingMinionsPR_Dic.Add(_movingMinion_Gob, lPlayerResponse);
                        //print("//////////////// " + lPlayerResponse.ExecuteAction.Score);
                    }
                    else
                    {
                        PlayerResponse lPlayerResponseTmp = new PlayerResponse();
                        _allMovingMinionsPR_Dic.Add(_movingMinion_Gob, lPlayerResponseTmp);
                        //print("--------------------" + lPlayerResponseTmp.ExecuteAction.Score + " // " + lPlayerResponseTmp.ExecuteAction.Position + " // " + _movingMinion_Gob.name);
                    }
                }

                foreach (var crossMinion_Var in movingMinion_Var.PlayersOnPath)
                {
                    GameObject _crossMinion_Gob = GameObject.Find(crossMinion_Var.Player.Team.ToString() + '-' + crossMinion_Var.Player.Position.ToString());

                    if (!_allMovingMinionsPR_Dic.ContainsKey(_crossMinion_Gob))
                    {
                        //_allMovingMinionsPR_Dic.Add(_crossMinion_Gob, crossMinion_Var.Player);
                    }
                }

                if (param_Str.Contains("Sprint") && movingMinion_Var.HasBall && firstMinionControllingBall_Str == "Null")
                {
                    firstMinionControllingBall_Str = "Block";

                    GameObject _ball_Gob = GameObject.Find("[Ball]");
                    Vector3 _localPosition_Vct = new Vector3(_movingMinion_Gob.GetComponent<Player3D>().BallRefPos_Gob.transform.position.x, 0.025f, _movingMinion_Gob.GetComponent<Player3D>().BallRefPos_Gob.transform.position.z);

                    _ball_Gob.transform.SetParent(_movingMinion_Gob.GetComponent<Player3D>().BallRefPos_Gob.transform, false);
                    _ball_Gob.transform.position = _localPosition_Vct;
                    _ball_Gob.transform.localScale = Vector3.one;
                }
            }

            while (!CheckMinionTarget())
            {
                //print("------------------> Move Minions");

                foreach (var movingMinion_Var in _allMovingMinionsPM_Dic)
                {
                    //Current Moving Minion Params
                    GameObject _movingMinion_Gob = movingMinion_Var.Key;
                    GameObject _movingAnimator_Gob = GameObject.Find(_movingMinion_Gob.name + "/[Animator] Character/Character_Mesh_01");
                    GameObject _movingAim_Gob = GameObject.Find(_movingMinion_Gob.name + "/[Animator] Character");

                    if (_movingMinion_Gob.transform.position != _allMovingMinionsTargets_Dic[_movingMinion_Gob])
                    {
                        //Trigger Animation Once            
                        if (_movingMinion_Gob.GetComponent<Player3D>().CurrentAnim_Str == "Idle")
                        {
                            _movingMinion_Gob.GetComponent<Player3D>().CurrentAnim_Str = "Sprint";
                            _movingAnimator_Gob.GetComponent<Animator>().SetTrigger("Sprint");
                        }

                        //Set Current Position & Target Position
                        _currentPos_Vct = _movingMinion_Gob.transform.position;
                        _targetPos_Vct = _allMovingMinionsTargets_Dic[movingMinion_Var.Key];

                        //Move Selected Minion Towards Selected Destination
                        _currentPos_Vct = Vector3.MoveTowards(_currentPos_Vct, _targetPos_Vct, _allMovingMinionsSpeed_Dic[_movingMinion_Gob] * Time.deltaTime);
                        _movingMinion_Gob.transform.position = _currentPos_Vct;
                        _movingAim_Gob.transform.LookAt(_targetPos_Vct);

                        //print(movingMinion_Var.Value.PlayersOnPath.Count);

                        foreach (var crossMinion_Var in movingMinion_Var.Value.PlayersOnPath)
                        {
                            GameObject _crossMinion_Gob = GameObject.Find(crossMinion_Var.Player.Team.ToString() + '-' + crossMinion_Var.Player.Position.ToString());
                            GameObject _crossAnimator_Gob = GameObject.Find(_crossMinion_Gob.name + "/[Animator] Character/Character_Mesh_01");
                            GameObject _crossAim_Gob = GameObject.Find(_movingMinion_Gob.name + "/[Animator] Character");

                            bool checkPreviousContact_Bol = false;

                            string codeNameVerificationA_Str = _movingMinion_Gob.name + "_" + _crossMinion_Gob.name;
                            string codeNameVerificationB_Str = _crossMinion_Gob.name + "_" + _movingMinion_Gob.name;

                            if (_allMovingMinionsContacts_Dic.ContainsKey(codeNameVerificationA_Str))
                            { checkPreviousContact_Bol = true; }

                            if (_allMovingMinionsContacts_Dic.ContainsKey(codeNameVerificationB_Str))
                            { checkPreviousContact_Bol = true; }

                            if (Vector3.Distance(_movingMinion_Gob.transform.position, _crossMinion_Gob.transform.position) < 0.5f && !checkPreviousContact_Bol)
                            {
                                print("Cross Between : " + codeNameVerificationA_Str + " // " + codeNameVerificationB_Str);

                                _allMovingMinionsContacts_Dic.Add(codeNameVerificationA_Str, "Moving Minion Contact A");
                                _allMovingMinionsContacts_Dic.Add(codeNameVerificationB_Str, "Cross Minion Contact B");

                                Debug.Log(movingMinion_Var.Value.Player.Team + " / " + movingMinion_Var.Value.Player.Position + " - [IsStaticPlayer = " + movingMinion_Var.Value.IsStaticPlayer + "]");
                                Debug.Log(crossMinion_Var.Player.Team + " / " + crossMinion_Var.Player.Position + " - [IsFieldCaseTarget = " + crossMinion_Var.IsFieldCaseTarget + "] | " + "[IsStaticPlayer = " + crossMinion_Var.IsStaticPlayer + "] | " + "[IsWonFieldCaseTarget = " + crossMinion_Var.IsWonFieldCaseTarget + "]");

                                //if (crossMinion_Var.IsFieldCaseTarget && !crossMinion_Var.IsStaticPlayer)
                                if (crossMinion_Var.IsFieldCaseTarget)
                                {
                                    Debug.Log("crossMinion_Var [FieldCaseEnd = " + crossMinion_Var.FieldCaseEnd.X + " / " + crossMinion_Var.FieldCaseEnd.Y + "]");
                                    Debug.Log("movingMinion_Var [FieldCaseEnd = " + movingMinion_Var.Value.FieldCaseEnd.X + " / " + movingMinion_Var.Value.FieldCaseEnd.Y + "]");
                                    Debug.Log("movingMinion_Var [FieldCaseTarget = " + movingMinion_Var.Value.FieldCaseTarget.X + " / " + movingMinion_Var.Value.FieldCaseTarget.Y + "]");
                                    Debug.Log("movingMinion_Var [HasBall = " + movingMinion_Var.Value.HasBall);

                                    if (crossMinion_Var.IsWonFieldCaseTarget)
                                    {
                                        //Moving Minion Lose Duel With Cross Minion
                                        _allMovingMinionsTargets_Dic[movingMinion_Var.Key] = PlayersManager.Instance.CalculatePositionByCoordinates(movingMinion_Var.Value.FieldCaseEnd); //print("1");

                                        if (param_Str.Contains("Sprint") && movingMinion_Var.Value.HasBall)
                                        {
                                            GameObject _ball_Gob = GameObject.Find("[Ball]");
                                            Vector3 _localPosition_Vct = new Vector3(_crossMinion_Gob.GetComponent<Player3D>().BallRefPos_Gob.transform.position.x, 0.025f, _crossMinion_Gob.GetComponent<Player3D>().BallRefPos_Gob.transform.position.z);

                                            _ball_Gob.transform.SetParent(_crossMinion_Gob.GetComponent<Player3D>().BallRefPos_Gob.transform, true);
                                            _ball_Gob.transform.position = _localPosition_Vct;
                                            _ball_Gob.transform.localScale = Vector3.one;

                                            _crossMinion_Gob.GetComponent<Player3D>().CurrentAnim_Str = "Tackle";
                                            _crossAnimator_Gob.GetComponent<Animator>().SetTrigger("Tackle");

                                            _movingMinion_Gob.GetComponent<Player3D>().CurrentAnim_Str = "Fail";
                                            _movingAnimator_Gob.GetComponent<Animator>().SetTrigger("Fail");
                                        }

                                        if (!param_Str.Contains("Sprint"))
                                        {
                                            _crossMinion_Gob.GetComponent<Player3D>().CurrentAnim_Str = "Shoulder_To_Idle";
                                            //_crossAnimator_Gob.GetComponent<Animator>().SetTrigger("Pressure");

                                            _movingMinion_Gob.GetComponent<Player3D>().CurrentAnim_Str = "Shoulder_To_Sprint";
                                            //_movingAnimator_Gob.GetComponent<Animator>().SetTrigger("Shoulder_To_Sprint");   
                                        }

                                        if (param_Str.Contains("Sprint") && movingMinion_Var.Value.HasBall)
                                        {
                                            if (_allMovingMinionsPR_Dic.ContainsKey(_crossMinion_Gob) && _allMovingMinionsPR_Dic[_crossMinion_Gob].ExecuteAction.Score > 0 &&
                                                _allMovingMinionsPR_Dic.ContainsKey(_movingMinion_Gob) && _allMovingMinionsPR_Dic[_movingMinion_Gob].ExecuteAction.Score > 0)
                                            {
                                                StartCoroutine(InstantiateCustomResolutionFX(_crossMinion_Gob, _allMovingMinionsPR_Dic[_crossMinion_Gob], null, "Win", false, false, "Sprint"));
                                                StartCoroutine(InstantiateCustomResolutionFX(_movingMinion_Gob, _allMovingMinionsPR_Dic[_movingMinion_Gob], null, "Lose", false, false, "Sprint"));
                                            }

                                            if (_allMovingMinionsPR_Dic.ContainsKey(_movingMinion_Gob) && _allMovingMinionsPR_Dic[_movingMinion_Gob].ExecuteAction.Score > 0)
                                            {
                                            }
                                        }

                                        print("Cross Result : Cross Minion Minion Win Duel");
                                    }
                                    else
                                    {
                                        //Moving Minion Win Duel With Cross Minion
                                        _allMovingMinionsTargets_Dic[_crossMinion_Gob] = PlayersManager.Instance.CalculatePositionByCoordinates(crossMinion_Var.FieldCaseEnd); //print("2");

                                        if (!param_Str.Contains("Sprint") && crossMinion_Var.HasBall)
                                        {
                                            GameObject _ball_Gob = GameObject.Find("[Ball]");
                                            Vector3 _localPosition_Vct = new Vector3(_movingMinion_Gob.GetComponent<Player3D>().BallRefPos_Gob.transform.position.x, 0.025f, _movingMinion_Gob.GetComponent<Player3D>().BallRefPos_Gob.transform.position.z);

                                            _ball_Gob.transform.SetParent(_movingMinion_Gob.GetComponent<Player3D>().BallRefPos_Gob.transform, true);
                                            _ball_Gob.transform.position = _localPosition_Vct;
                                            _ball_Gob.transform.localScale = Vector3.one;

                                            /*_crossMinion_Gob.GetComponent<Player3D>().CurrentAnim_Str = "Tackle";
                                            _crossAnimator_Gob.GetComponent<Animator>().SetTrigger("Tackle");

                                            _movingMinion_Gob.GetComponent<Player3D>().CurrentAnim_Str = "Fail";
                                            _movingAnimator_Gob.GetComponent<Animator>().SetTrigger("Fail");*/
                                        }

                                        _crossMinion_Gob.GetComponent<Player3D>().CurrentAnim_Str = "Shoulder_To_Sprint";
                                        _crossAnimator_Gob.GetComponent<Animator>().SetTrigger("Shoulder_To_Sprint");

                                        _movingMinion_Gob.GetComponent<Player3D>().CurrentAnim_Str = "Shoulder_To_Idle";
                                        _movingAnimator_Gob.GetComponent<Animator>().SetTrigger("Shoulder_To_Idle");

                                        if (param_Str.Contains("Sprint") && movingMinion_Var.Value.HasBall)
                                        {
                                            if (_allMovingMinionsPR_Dic.ContainsKey(_crossMinion_Gob) && _allMovingMinionsPR_Dic[_crossMinion_Gob].ExecuteAction.Score > 0 &&
                                                _allMovingMinionsPR_Dic.ContainsKey(_movingMinion_Gob) && _allMovingMinionsPR_Dic[_movingMinion_Gob].ExecuteAction.Score > 0)
                                            {
                                                StartCoroutine(InstantiateCustomResolutionFX(_crossMinion_Gob, _allMovingMinionsPR_Dic[_crossMinion_Gob], null, "Lose", false, false, "Sprint"));
                                                StartCoroutine(InstantiateCustomResolutionFX(_movingMinion_Gob, _allMovingMinionsPR_Dic[_movingMinion_Gob], null, "Win", false, false, "Sprint"));
                                            }

                                            if (_allMovingMinionsPR_Dic.ContainsKey(_movingMinion_Gob) && _allMovingMinionsPR_Dic[_movingMinion_Gob].ExecuteAction.Score > 0)
                                            {
                                            }
                                        }

                                        print("Cross Result : Moving Minion Minion Win Duel");
                                    }
                                }

                                if (!crossMinion_Var.IsFieldCaseTarget)
                                {
                                    if (crossMinion_Var.IsStaticPlayer)
                                    {
                                        _crossMinion_Gob.GetComponent<Player3D>().CurrentAnim_Str = "Shoulder_To_Idle"; //print("3");
                                        //_crossAnimator_Gob.GetComponent<Animator>().SetTrigger("Shoulder_To_Idle");

                                        _movingMinion_Gob.GetComponent<Player3D>().CurrentAnim_Str = "Shoulder_To_Sprint";
                                        _movingAnimator_Gob.GetComponent<Animator>().SetTrigger("Shoulder_To_Sprint");

                                        print("Cross Result : Lower Speed Between Moving Minion & Static Minion");
                                    }
                                    else
                                    {
                                        _crossMinion_Gob.GetComponent<Player3D>().CurrentAnim_Str = "Shoulder_To_Sprint"; //print("4");
                                        _crossAnimator_Gob.GetComponent<Animator>().SetTrigger("Shoulder_To_Sprint");

                                        _movingMinion_Gob.GetComponent<Player3D>().CurrentAnim_Str = "Shoulder_To_Sprint";
                                        _movingAnimator_Gob.GetComponent<Animator>().SetTrigger("Shoulder_To_Sprint");

                                        print("Cross Result : Lower Speed Between Moving Minion & Cross Minion");
                                    }
                                }

                                if (!crossMinion_Var.IsStaticPlayer)
                                {
                                    //Push Moving Minion And Continue Moving                                
                                    if (_allMovingMinionsSpeed_Dic.ContainsKey(_crossMinion_Gob) && _allMovingMinionsSpeed_Dic[_crossMinion_Gob] > 0.1f)
                                    { _allMovingMinionsSpeed_Dic[_crossMinion_Gob] = _allMovingMinionsSpeed_Dic[_crossMinion_Gob] - 0.1f; }
                                }

                                //Push Cross Minion And Continue Moving
                                if (_allMovingMinionsSpeed_Dic.ContainsKey(_movingMinion_Gob) && _allMovingMinionsSpeed_Dic[_movingMinion_Gob] > 0.1f)
                                { _allMovingMinionsSpeed_Dic[_movingMinion_Gob] = _allMovingMinionsSpeed_Dic[_movingMinion_Gob] - 0.1f; }
                            }
                        }

                        //Reach Target
                        if (Vector3.Distance(_currentPos_Vct, _targetPos_Vct) < 0.05f && _movingMinion_Gob.GetComponent<Player3D>().CurrentAnim_Str != "Idle")
                        {
                            _currentPos_Vct = _targetPos_Vct;
                            _movingMinion_Gob.transform.position = _currentPos_Vct;
                            _allMovingMinionsTargets_Dic[_movingMinion_Gob] = _currentPos_Vct;

                            _movingMinion_Gob.GetComponent<Player3D>().CurrentAnim_Str = "Idle";
                            _movingAnimator_Gob.GetComponent<Animator>().SetTrigger("Idle_In_Game");
                        }
                    }

                    if (Vector3.Distance(_movingMinion_Gob.transform.position, _allMovingMinionsTargets_Dic[_movingMinion_Gob]) < 0.01f)
                    {
                        Animator m_Animator = _movingAnimator_Gob.GetComponent<Animator>();
                        AnimatorClipInfo[] m_CurrentClipInfo = m_Animator.GetCurrentAnimatorClipInfo(0);
                        //print(_movingMinion_Gob.name + " -----> Current Animation : " + m_CurrentClipInfo[0].clip.name);

                        //Activate Idle
                        if (m_CurrentClipInfo[0].clip.name.Contains("Sprint"))
                        {
                            _movingMinion_Gob.GetComponent<Player3D>().CurrentAnim_Str = "Idle";
                            _movingAnimator_Gob.GetComponent<Animator>().SetTrigger("Idle_In_Game");
                        }
                    }
                }

                yield return 0;
            }
        }

        //gameObject.GetComponent<SpriteRenderer>().enabled = false;

        #endregion
    }

    public IEnumerator ExecuteResolution()
    {
        yield return new WaitForSeconds(0);

        string param_Str = CurrentResolutionCase_Str;
         
        print("[ -------------- Start Execute Resolution -------------- ]");

        //
        //
        // Duel 1v1
        //
        //

        // Duel : Start & Win Duel Against Opponent [Part_1] -> Move The Ball To Opponent Position [Part_2] // Resolution_Duel_Case_1
        // Duel : Start & Lose Duel Against Opponent [Part_1] -> Move The Ball To Opponent Position [Part_2] // Resolution_Duel_Case_2
        if (param_Str.Contains("Resolution_Duel_Case"))
        {
            #region Duel 1V1

            int playerMinionScore_Int = 0;
            int opponentMinionScore_Int = 0;

            string playerAnimTrigger_Str = "";
            string opponentAnimTrigger_Str = "";

            //Trigger Animations
            if (ExecuteActionTeamRed_Pr != null)
            {
                playerMinionScore_Int = ExecuteActionTeamBlue_Pr.ExecuteAction.ScoreWithPressure;
                opponentMinionScore_Int = ExecuteActionTeamRed_Pr.ExecuteAction.ScoreWithPressure;

                //Get The Duel Winner
                if (playerMinionScore_Int > opponentMinionScore_Int)
                {
                    playerAnimTrigger_Str = "Win";
                    opponentAnimTrigger_Str = "Lose";

                    GameObject minion_Gob = GameObject.Find(ExecuteActionTeamBlue_Pr.Team + '-' + ExecuteActionTeamBlue_Pr.Position);
                    GameObject minionAnimator_Gob = GameObject.Find(minion_Gob.name + "/[Animator] Character/Character_Mesh_01");
                    minionAnimator_Gob.GetComponent<Animator>().SetTrigger("Pressure");

                    if (ExecuteActionTeamBlue_Pr.Action.ToString().Contains("TACKLE"))
                    { minionAnimator_Gob.GetComponent<Animator>().SetTrigger("Tackle"); print("---------------------------------------------> Player Win // Dispute The Ball Duel 1V1"); }
                    else
                    { minionAnimator_Gob.GetComponent<Animator>().SetTrigger("Pressure"); print("---------------------------------------------> Player Win // Dispute The Ball Duel 1V1"); }

                    minion_Gob = GameObject.Find(ExecuteActionTeamRed_Pr.Team + '-' + ExecuteActionTeamRed_Pr.Position);
                    minionAnimator_Gob = GameObject.Find(minion_Gob.name + "/[Animator] Character/Character_Mesh_01");
                    minionAnimator_Gob.GetComponent<Animator>().SetTrigger("Fail");

                    MatchAudio.Instance.Tackle();
                }
                else
                {
                    playerAnimTrigger_Str = "Lose";
                    opponentAnimTrigger_Str = "Win";

                    GameObject minion_Gob = GameObject.Find(ExecuteActionTeamBlue_Pr.Team + '-' + ExecuteActionTeamBlue_Pr.Position);
                    GameObject minionAnimator_Gob = GameObject.Find(minion_Gob.name + "/[Animator] Character/Character_Mesh_01");
                    minionAnimator_Gob.GetComponent<Animator>().SetTrigger("Fail");

                    minion_Gob = GameObject.Find(ExecuteActionTeamRed_Pr.Team + '-' + ExecuteActionTeamRed_Pr.Position);
                    minionAnimator_Gob = GameObject.Find(minion_Gob.name + "/[Animator] Character/Character_Mesh_01");

                    if (ExecuteActionTeamRed_Pr.Action.ToString().Contains("TACKLE"))
                    { minionAnimator_Gob.GetComponent<Animator>().SetTrigger("Tackle"); print("---------------------------------------------> Opponent Win // Dispute The Ball Duel 1V1"); }
                    else
                    { minionAnimator_Gob.GetComponent<Animator>().SetTrigger("Pressure"); print("---------------------------------------------> Opponent Win // Dispute The Ball Duel 1V1"); }

                    MatchAudio.Instance.Tackle();
                }

                //Display Duel On Field Sprite
                GameObject duelField_Gob = Instantiate(PrefabsIdxArray_Gob[Array.IndexOf(PrefabsIdxArray_Str, "Duel_Field_Grp")], GameObject.Find(ExecuteActionTeamBlue_Pr.ExecuteAction.Team + '-' + ExecuteActionTeamBlue_Pr.ExecuteAction.Position).transform.position, Quaternion.identity);
                duelField_Gob.name = "Duel_Field_Case_Player_1";
                OnFieldLst_Gob.Add(duelField_Gob);

                // Orient duel field GO
                if (ExecuteActionTeamBlue_Pr.ExecuteAction.Direction == "TOP")
                { duelField_Gob.transform.rotation = Quaternion.Euler(0, -90, 0); }

                if (ExecuteActionTeamBlue_Pr.ExecuteAction.Direction == "DOWN")
                { duelField_Gob.transform.rotation = Quaternion.Euler(0, 90, 0); }

                if (ExecuteActionTeamBlue_Pr.ExecuteAction.Direction == "LEFT" && !CurrentMode_Str.Contains("PVP"))
                { duelField_Gob.transform.rotation = Quaternion.Euler(0, 180, 0); }

                if (ExecuteActionTeamBlue_Pr.ExecuteAction.Direction == "RIGHT" && !CurrentMode_Str.Contains("PVP"))
                { duelField_Gob.transform.rotation = Quaternion.Euler(0, 0, 0); }

                if (ExecuteActionTeamBlue_Pr.ExecuteAction.Direction == "LEFT" && CurrentMode_Str.Contains("PVP"))
                { duelField_Gob.transform.rotation = Quaternion.Euler(0, 0, 0); }

                if (ExecuteActionTeamBlue_Pr.ExecuteAction.Direction == "RIGHT" && CurrentMode_Str.Contains("PVP"))
                { duelField_Gob.transform.rotation = Quaternion.Euler(0, 180, 0); }
            }

            GameObject selectedMinion_Gob = GameObject.Find(ExecuteActionTeamBlue_Pr.Team + '-' + ExecuteActionTeamBlue_Pr.Position);
            PlayerResponse selectedMinion_Pr = ExecuteActionTeamBlue_Pr;

            if (selectedMinion_Gob != null && ExecuteActionTeamRed_Pr != null)
            { StartCoroutine(InstantiateCustomResolutionFX(selectedMinion_Gob, selectedMinion_Pr, null, playerAnimTrigger_Str, false, false, "Null")); }

            selectedMinion_Gob = GameObject.Find(ExecuteActionTeamRed_Pr.Team + '-' + ExecuteActionTeamRed_Pr.Position);
            selectedMinion_Pr = ExecuteActionTeamRed_Pr;

            if (selectedMinion_Gob != null)
            { StartCoroutine(InstantiateCustomResolutionFX(selectedMinion_Gob, selectedMinion_Pr, null, opponentAnimTrigger_Str, false, false, "Null")); }

            yield return new WaitWhile(() => !CheckMinionDrawGrp());

            GameObject.Find("[Ball]").transform.SetParent(GameObject.Find("[Team Players Container]")?.transform, true);
            GameObject.Find("[Ball]").GetComponent<TrailRenderer>().enabled = false;

            yield return new WaitForSeconds(0.5f);

            foreach (var catch_Var in OnFieldLst_Gob)
            { Destroy(catch_Var); }

            OppositionUI.Instance.OnDuelFinished();
            Destroy(gameObject);
            
            #endregion
        }

        //
        //
        // Pass & Move
        //
        //

        // PASS : Start & Win Duel Against Opponent[Part_1]->Display Interceptors[Part_2]->Start & Win Duel Receiver Vs Opponent[Part_3] -> Move The Ball To Receiver Position[Part_4] // Resolution_Pass_Case_1
        // PASS : Start & Win Duel Against Opponent [Part_1] -> Display Interceptors [Part_2] -> Start & Lose Duel Receiver Vs Opponent [Part_3] -> Move The Ball To Receiver Opponent Position [Part_4] // Resolution_Pass_Case_2
        // PASS : Start & Win Duel Against Opponent [Part_1] -> Display Interceptors [Part_2] ->  No Duel Only Display Selected Receiver [Part_3] -> Move The Ball To Receiver Position [Part_4] // Resolution_Pass_Case_3
        // PASS : Start & Win Duel Against Opponent [Part_1] -> Display Interceptors & Interceptor Catch The Ball [Part_2] -> Move The Ball To Intercpetor Position [Part_3] // Resolution_Pass_Case_4
        // PASS : Start & Win Lose Against Opponent [Part_1] -> Move The Ball To Opponent Position [Part_2] ----> Go To Duel 1V1  // Resolution_Pass_Case_5
        // PASS : No Duel Only Display Selected Minion [Part_1] -> Display Interceptors [Part_2] -> Start & Win Duel Receiver Vs Opponent [Part_3] -> Move The Ball To Receiver Position [Part_4] // Resolution_Pass_Case_6
        // PASS : No Duel Only Display Selected Minion [Part_1] -> Display Interceptors [Part_2] -> Start & Lose Duel Receiver Vs Opponent [Part_3] -> Move The Ball To Receiver Opponent Position [Part_4] // Resolution_Pass_Case_7
        // PASS : No Duel Only Display Selected Minion [Part_1] -> Display Interceptors [Part_2] ->  No Duel Only Display Selected Receiver [Part_3] -> Move The Ball To Receiver Position [Part_4] // Resolution_Pass_Case_8
        // PASS : No Duel Only Display Selected Minion [Part_1] -> Display Interceptors & Interceptor Catch The Ball [Part_2] -> Move The Ball To Intercpetor Position [Part_3] // Resolution_Pass_Case_9
        if (param_Str.Contains("Resolution_Pass_Case"))
        {
            #region Pass

            #region First Duel

            int playerMinionScore_Int = 0;
            int opponentMinionScore_Int = 0;

            string playerAnimTrigger_Str = "";
            string opponentAnimTrigger_Str = "";

            //Trigger Animations
            if (ExecuteActionTeamRed_Pr != null)
            {
                playerMinionScore_Int = ExecuteActionTeamBlue_Pr.ExecuteAction.ScoreWithPressure;
                opponentMinionScore_Int = ExecuteActionTeamRed_Pr.ExecuteAction.ScoreWithPressure;

                //Get The Duel Winner
                if (playerMinionScore_Int > opponentMinionScore_Int)
                {
                    playerAnimTrigger_Str = "Win";
                    opponentAnimTrigger_Str = "Lose";

                    GameObject minion_Gob = GameObject.Find(ExecuteActionTeamBlue_Pr.Team + '-' + ExecuteActionTeamBlue_Pr.Position);
                    GameObject minionAnimator_Gob = GameObject.Find(minion_Gob.name + "/[Animator] Character/Character_Mesh_01");
                    minionAnimator_Gob.GetComponent<Animator>().SetTrigger("Pressure");

                    minion_Gob = GameObject.Find(ExecuteActionTeamRed_Pr.Team + '-' + ExecuteActionTeamRed_Pr.Position);
                    minionAnimator_Gob = GameObject.Find(minion_Gob.name + "/[Animator] Character/Character_Mesh_01");
                    minionAnimator_Gob.GetComponent<Animator>().SetTrigger("Fail"); print("---------------------------------------------> Player Win // Dispute The Ball Duel 1V1");

                    MatchAudio.Instance.Tackle();
                }
                else
                {
                    playerAnimTrigger_Str = "Lose";
                    opponentAnimTrigger_Str = "Win";

                    GameObject minion_Gob = GameObject.Find(ExecuteActionTeamBlue_Pr.Team + '-' + ExecuteActionTeamBlue_Pr.Position);
                    GameObject minionAnimator_Gob = GameObject.Find(minion_Gob.name + "/[Animator] Character/Character_Mesh_01");
                    minionAnimator_Gob.GetComponent<Animator>().SetTrigger("Fail");

                    minion_Gob = GameObject.Find(ExecuteActionTeamRed_Pr.Team + '-' + ExecuteActionTeamRed_Pr.Position);
                    minionAnimator_Gob = GameObject.Find(minion_Gob.name + "/[Animator] Character/Character_Mesh_01");
                    minionAnimator_Gob.GetComponent<Animator>().SetTrigger("Pressure"); print("---------------------------------------------> Opponent Win // Dispute The Ball Duel 1V1");

                    MatchAudio.Instance.Tackle();
                }

                //Display Duel On Field Sprite
                GameObject duelField_Gob = Instantiate(PrefabsIdxArray_Gob[Array.IndexOf(PrefabsIdxArray_Str, "Duel_Field_Grp")], GameObject.Find(ExecuteActionTeamBlue_Pr.ExecuteAction.Team + '-' + ExecuteActionTeamBlue_Pr.ExecuteAction.Position).transform.position, Quaternion.identity);
                duelField_Gob.name = "Duel_Field_Case_Player_1";
                OnFieldLst_Gob.Add(duelField_Gob);

                // Orient duel field GO
                if (ExecuteActionTeamBlue_Pr.ExecuteAction.Direction == "TOP")
                { duelField_Gob.transform.rotation = Quaternion.Euler(0, -90, 0); }

                if (ExecuteActionTeamBlue_Pr.ExecuteAction.Direction == "DOWN")
                { duelField_Gob.transform.rotation = Quaternion.Euler(0, 90, 0); }

                if (ExecuteActionTeamBlue_Pr.ExecuteAction.Direction == "LEFT" && !CurrentMode_Str.Contains("PVP"))
                { duelField_Gob.transform.rotation = Quaternion.Euler(0, 180, 0); }

                if (ExecuteActionTeamBlue_Pr.ExecuteAction.Direction == "RIGHT" && !CurrentMode_Str.Contains("PVP"))
                { duelField_Gob.transform.rotation = Quaternion.Euler(0, 0, 0); }

                if (ExecuteActionTeamBlue_Pr.ExecuteAction.Direction == "LEFT" && CurrentMode_Str.Contains("PVP"))
                { duelField_Gob.transform.rotation = Quaternion.Euler(0, 0, 0); }

                if (ExecuteActionTeamBlue_Pr.ExecuteAction.Direction == "RIGHT" && CurrentMode_Str.Contains("PVP"))
                { duelField_Gob.transform.rotation = Quaternion.Euler(0, 180, 0); }
            }

            GameObject selectedMinion_Gob = GameObject.Find(ExecuteActionTeamBlue_Pr.Team + '-' + ExecuteActionTeamBlue_Pr.Position);
            PlayerResponse selectedMinion_Pr = ExecuteActionTeamBlue_Pr;

            if (selectedMinion_Gob != null && ExecuteActionTeamRed_Pr != null)
            { StartCoroutine(InstantiateCustomResolutionFX(selectedMinion_Gob, selectedMinion_Pr, null, playerAnimTrigger_Str, false, false, "Null")); }

            if (ExecuteActionTeamRed_Pr != null)
            {
                selectedMinion_Gob = GameObject.Find(ExecuteActionTeamRed_Pr.Team + '-' + ExecuteActionTeamRed_Pr.Position);
                selectedMinion_Pr = ExecuteActionTeamRed_Pr;
            }

            if (selectedMinion_Gob != null && ExecuteActionTeamRed_Pr != null)
            { StartCoroutine(InstantiateCustomResolutionFX(selectedMinion_Gob, selectedMinion_Pr, null, opponentAnimTrigger_Str, false, false, "Null")); }

            if (ExecuteActionTeamRed_Pr == null)
            {
                StartCoroutine(InstantiateCustomResolutionFX(selectedMinion_Gob, selectedMinion_Pr, null, "Win", false, false, "Null"));
                playerMinionScore_Int = 1;
                opponentMinionScore_Int = 0;
            }

            #endregion

            #region Move Line Renderer

            GameObject startPos_Gob = GameObject.Find(ExecuteActionTeamBlue_Pr.Team + '-' + ExecuteActionTeamBlue_Pr.Position);

            lineRenderer_Gob = Instantiate(PrefabsIdxArray_Gob[Array.IndexOf(PrefabsIdxArray_Str, "Line_Renderer_Grp")], GameObject.Find(ExecuteActionTeamBlue_Pr.Team + '-' + ExecuteActionTeamBlue_Pr.Position).transform.position, Quaternion.identity);
            lineRenderer_Gob.name = "Current_Resolution_Path";

            lineRenderer_Gob.GetComponent<LineRenderer>().positionCount = 2;
            lineRenderer_Gob.GetComponent<LineRenderer>().SetPosition(0, startPos_Gob.transform.position);//Start 
            lineRenderer_Gob.GetComponent<LineRenderer>().SetPosition(1, startPos_Gob.transform.position);//End

            OnFieldLst_Gob.Add(lineRenderer_Gob);

            GameObject arrowSprite_Gob = GameObject.Find(lineRenderer_Gob.name + "/Arrow_Pivot_Grp/Arrow");
            GameObject arrowPivot_Gob = GameObject.Find(lineRenderer_Gob.name + "/Arrow_Pivot_Grp");

            MatchAudio.Instance.ArrowAppears();

            if (CurrentMode_Str == "NPC")
            {
                float alpha = 1.0f;
                Gradient gradient = new Gradient();
                gradient.SetKeys(
                    new GradientColorKey[] { new GradientColorKey(new Color(0.3f, 0, 0.1f, 1), 0.0f), new GradientColorKey(new Color(0.8f, 0, 0.2f, 1), 1.0f) },
                    new GradientAlphaKey[] { new GradientAlphaKey(alpha, 0.0f), new GradientAlphaKey(alpha, 1.0f) }
                );

                lineRenderer_Gob.GetComponent<LineRenderer>().colorGradient = gradient;
                arrowSprite_Gob.GetComponent<SpriteRenderer>().color = new Color(1, 0, 0.3098039f, 1);
            }
            else
            {
                float alpha = 1.0f;
                Gradient gradient = new Gradient();
                gradient.SetKeys(
                    new GradientColorKey[] { new GradientColorKey(new Color(0, 0.25f, 1, 1), 0.0f), new GradientColorKey(new Color(0, 0.7f, 1, 1), 1.0f) },
                    new GradientAlphaKey[] { new GradientAlphaKey(alpha, 0.0f), new GradientAlphaKey(alpha, 1.0f) }
                );

                lineRenderer_Gob.GetComponent<LineRenderer>().colorGradient = gradient;
                arrowSprite_Gob.GetComponent<SpriteRenderer>().color = new Color(0, 1f, 1, 1);
            }

            Vector3 currentPos_Vct = lineRenderer_Gob.GetComponent<LineRenderer>().GetPosition(1);
            Vector3 targetPos_Vct = GameObject.Find(PassResolution_Prr.Receiver.Team + '-' + PassResolution_Prr.Receiver.Position).transform.position;

            ArrowMvtSpeed_Flt = 4;

            // Adjuste Speed Following Framerate Drop
            //if (Application.targetFrameRate < 30)
            //{ frameRateDropSpeed_Flt = ArrowMvtSpeed_Flt * 2; }

            float frameRateDropSpeed_Flt = ArrowMvtSpeed_Flt;

            while (Vector3.Distance(currentPos_Vct, targetPos_Vct) > ArrowTargetStopDistance_Flt)
            {
                currentPos_Vct = Vector3.MoveTowards(currentPos_Vct, targetPos_Vct, frameRateDropSpeed_Flt * Time.deltaTime);
                lineRenderer_Gob.GetComponent<LineRenderer>().SetPosition(1, currentPos_Vct);

                arrowSprite_Gob.GetComponent<SpriteRenderer>().enabled = true;
                arrowPivot_Gob.transform.position = currentPos_Vct;
                arrowPivot_Gob.transform.LookAt(targetPos_Vct);

                yield return 0;
            }

            #endregion

            #region Adapt Camera Angle

            if (PassResolution_Prr.Interceptors.Count == 0)
            { StartCoroutine(_focusOnReception()); }
            else
            {
                ActionsManager.Instance.DuelPlayer?.UI.TogglePlayerName(true);
                ActionsManager.Instance.DuelOppPlayer?.UI.TogglePlayerName(true);

                ActionsUI.Instance.ToggleActionBoard(false);
                ActionsUI.Instance.ClearResolution();

                MatchCamera.Instance.DisplayResolutionView();
                yield return new WaitForSeconds(MatchCamera.Instance.TimeToWait);
            }

            #endregion

            //Player Minion Win Duel --> Continue Pass
            if (playerMinionScore_Int > opponentMinionScore_Int)
            {
                #region Move Ball

                GameObject ball_Gob = GameObject.Find("[Ball]");
                ball_Gob.GetComponent<TrailRenderer>().enabled = true;

                GameObject minion_Gob = GameObject.Find(ExecuteActionTeamBlue_Pr.Team + '-' + ExecuteActionTeamBlue_Pr.Position);
                GameObject minionAnimator_Gob = GameObject.Find(minion_Gob.name + "/[Animator] Character/Character_Mesh_01");
                minionAnimator_Gob.GetComponent<Animator>().SetTrigger("Pass_Ball");

                yield return new WaitForSeconds(0.5f);

                MatchAudio.Instance.Kick();

                StartCoroutine(SprintMovePass("Null"));

                currentPos_Vct = ball_Gob.transform.position;
                targetPos_Vct = GameObject.Find(PassResolution_Prr.Receiver.Team + '-' + PassResolution_Prr.Receiver.Position).GetComponent<Player3D>().BallRefPos_Gob.transform.position;
               // targetPos_Vct = GameObject.Find(PassResolution_Prr.Receiver.Team + '-' + PassResolution_Prr.Receiver.Position).GetComponent<Player3D>().BallRefPos_Gob.transform.position;

                string interceptionState_Str = "None";
                List<string> interceptorLst_Str = new List<string>();

                BallMvtSpeed_Flt = 3.5f;

                // Adjuste Speed Following Framerate Drop
                //if (Application.targetFrameRate < 30)
                //{ frameRateDropSpeed_Flt = BallMvtSpeed_Flt * 2; }

                frameRateDropSpeed_Flt = BallMvtSpeed_Flt;

                //bool startOnce_Bol = false;

                while (Vector3.Distance(currentPos_Vct, targetPos_Vct) > BallTargetStopDistance_Flt)
                {
                    currentPos_Vct = Vector3.MoveTowards(currentPos_Vct, targetPos_Vct, frameRateDropSpeed_Flt * Time.deltaTime);

                    ball_Gob.transform.position = currentPos_Vct;
                    ball_Gob.transform.RotateAround(ball_Gob.transform.position, Vector3.right, 180 * Time.deltaTime);

                    //Fade Out Line Renderer 
                    Color StartColorHolder_Col = lineRenderer_Gob.GetComponent<LineRenderer>().startColor;
                    Color endColorHolder_Col = lineRenderer_Gob.GetComponent<LineRenderer>().endColor;

                    float alpha_Flt = StartColorHolder_Col.a;
                    alpha_Flt -= LineFadeOutSpeed_Flt * Time.deltaTime;
                    StartColorHolder_Col = new Color(StartColorHolder_Col.r, StartColorHolder_Col.g, StartColorHolder_Col.b, alpha_Flt);

                    alpha_Flt = endColorHolder_Col.a;
                    alpha_Flt -= LineFadeOutSpeed_Flt * Time.deltaTime;
                    endColorHolder_Col = new Color(endColorHolder_Col.r, endColorHolder_Col.g, endColorHolder_Col.b, alpha_Flt);

                    lineRenderer_Gob.GetComponent<LineRenderer>().startColor = StartColorHolder_Col;
                    lineRenderer_Gob.GetComponent<LineRenderer>().endColor = endColorHolder_Col;

                    GameObject arrow_Gob = GameObject.Find(lineRenderer_Gob.name + "/Arrow_Pivot_Grp/Arrow");
                    arrow_Gob.GetComponent<SpriteRenderer>().color = endColorHolder_Col;

                    if (interceptionState_Str == "None")
                    {
                        //
                        //---------> Interceptors
                        //

                        foreach (var catch_Var in PassResolution_Prr.Interceptors)
                        {
                            GameObject interceptor_Gob = GameObject.Find(catch_Var.Team + '-' + catch_Var.Position);
                            Vector3 interceptorPos_Vct = new Vector3(interceptor_Gob.transform.position.x, currentPos_Vct.y, interceptor_Gob.transform.position.z);

                            if (Vector3.Distance(currentPos_Vct, interceptorPos_Vct) <= 0.35f && !interceptorLst_Str.Contains(interceptor_Gob.name) && !catch_Var.IsPlayerMoving)
                            {
                                interceptorLst_Str.Add(interceptor_Gob.name);
                                GameObject interceptorAnimator_Gob = GameObject.Find(interceptor_Gob.name + "/[Animator] Character/Character_Mesh_01");
                                interceptorAnimator_Gob.GetComponent<Animator>().SetTrigger("Pass_Intercept");

                                MatchAudio.Instance.Tackle();

                                if (PassResolution_Prr.IsBallIntercepted && catch_Var.Position == PassResolution_Prr.PlayerResolution)
                                {
                                    currentPos_Vct = targetPos_Vct = interceptor_Gob.GetComponent<Player3D>().BallRefPos_Gob.transform.position;
                                    ball_Gob.transform.position = currentPos_Vct;
                                    interceptionState_Str = "Intercepted_By_Interceptor"; print(interceptionState_Str + " // " + interceptor_Gob.name);
                                }

                                if (interceptionState_Str == "None")
                                { StartCoroutine(InstantiateCustomResolutionFX(interceptor_Gob, null, catch_Var, "Lose", true, false, "Null")); }
                                else
                                { StartCoroutine(InstantiateCustomResolutionFX(interceptor_Gob, null, catch_Var, "Win", true, false, "Null")); }
                            }
                        }

                        //
                        //---------> Moving Minions
                        //

                        foreach (var movingMinion_Var in _allMovingMinionsTargets_Dic)
                        {
                            GameObject movingMinion_Gob = movingMinion_Var.Key;
                            Vector3 movingMinionPos_Vct = new Vector3(movingMinion_Gob.transform.position.x, currentPos_Vct.y, movingMinion_Gob.transform.position.z);

                            string oppositeTeam_Str = "TEAM_A";

                            if (GameObject.Find(ExecuteActionTeamBlue_Pr.Team + '-' + ExecuteActionTeamBlue_Pr.Position).name.Contains("TEAM_A"))
                            { oppositeTeam_Str = "TEAM_B"; }

                            if (GameObject.Find(ExecuteActionTeamBlue_Pr.Team + '-' + ExecuteActionTeamBlue_Pr.Position).name.Contains("TEAM_B"))
                            { oppositeTeam_Str = "TEAM_A"; }

                            if (Vector3.Distance(currentPos_Vct, movingMinionPos_Vct) <= 0.5f && !interceptorLst_Str.Contains(movingMinion_Gob.name) && movingMinion_Gob.name.Contains(oppositeTeam_Str)/* && _allMovingMinionsPM_Dic[movingMinion_Gob].IsInterceptor*/)
                            {
                                interceptorLst_Str.Add(movingMinion_Gob.name);
                                GameObject movingMinionAnimator_Gob = GameObject.Find(movingMinion_Gob.name + "/[Animator] Character/Character_Mesh_01");
                                movingMinionAnimator_Gob.GetComponent<Animator>().SetTrigger("Tackle");
                                
                                MatchAudio.Instance.Tackle();

                                if (_allMovingMinionsPM_Dic[movingMinion_Gob].IsInterceptBall)
                                {
                                    currentPos_Vct = targetPos_Vct = movingMinion_Gob.GetComponent<Player3D>().BallRefPos_Gob.transform.position;
                                    ball_Gob.transform.position = currentPos_Vct;

                                    GameObject _ball_Gob = GameObject.Find("[Ball]");
                                    Vector3 _localPosition_Vct = new Vector3(movingMinion_Gob.GetComponent<Player3D>().BallRefPos_Gob.transform.position.x, 0.025f, movingMinion_Gob.GetComponent<Player3D>().BallRefPos_Gob.transform.position.z);

                                    _ball_Gob.transform.SetParent(movingMinion_Gob.GetComponent<Player3D>().BallRefPos_Gob.transform, true);
                                    _ball_Gob.transform.position = _localPosition_Vct;
                                    _ball_Gob.transform.localScale = Vector3.one;

                                    interceptionState_Str = "Intercepted_By_Moving_Minion"; print(interceptionState_Str + " // " + movingMinion_Gob.name);
                                }

                                if (_allMovingMinionsPR_Dic[movingMinion_Gob].ExecuteAction.Score > 0)
                                {
                                    if (interceptionState_Str == "None")
                                    {
                                        StartCoroutine(InstantiateCustomResolutionFX(movingMinion_Gob, _allMovingMinionsPR_Dic[movingMinion_Gob], null, "Lose", true, false, "Null"));
                                    }
                                    else
                                    {
                                        StartCoroutine(InstantiateCustomResolutionFX(movingMinion_Gob, _allMovingMinionsPR_Dic[movingMinion_Gob], null, "Win", true, false, "Null"));
                                    }
                                }
                            }
                        }
                    }

                    yield return 0;
                }

                lineRenderer_Gob.GetComponent<LineRenderer>().enabled = false;
                GameObject.Find(lineRenderer_Gob.name + "/Arrow_Pivot_Grp/Arrow").GetComponent<SpriteRenderer>().enabled = false;

                #endregion

                #region Pass Reach Receiver & Opponent Duel Is Available

                if (!interceptionState_Str.Contains("Intercepted") && PassResolution_Prr.ReceiverOpponentDuel != null)
                {
                    /*int*/ playerMinionScore_Int = 0;
                    /*int*/ opponentMinionScore_Int = 0;

                    /*string*/ playerAnimTrigger_Str = "";
                    /*string*/ opponentAnimTrigger_Str = "";

                    //Trigger Animations
                    if (PassResolution_Prr.ReceiverOpponentDuel != null)
                    {
                        playerMinionScore_Int = PassResolution_Prr.Receiver.ScoreWithPressure;
                        opponentMinionScore_Int = PassResolution_Prr.ReceiverOpponentDuel.ScoreWithPressure;

                        //Get The Duel Winner
                        if (playerMinionScore_Int > opponentMinionScore_Int)
                        {
                            playerAnimTrigger_Str = "Win";
                            opponentAnimTrigger_Str = "Lose";

                            minion_Gob = GameObject.Find(PassResolution_Prr.Receiver.Team + '-' + PassResolution_Prr.Receiver.Position);
                            minionAnimator_Gob = GameObject.Find(minion_Gob.name + "/[Animator] Character/Character_Mesh_01");
                            minionAnimator_Gob.GetComponent<Animator>().SetTrigger("Pressure");

                            minion_Gob = GameObject.Find(PassResolution_Prr.ReceiverOpponentDuel.Team + '-' + PassResolution_Prr.ReceiverOpponentDuel.Position);
                            minionAnimator_Gob = GameObject.Find(minion_Gob.name + "/[Animator] Character/Character_Mesh_01");
                            minionAnimator_Gob.GetComponent<Animator>().SetTrigger("Fail"); print("---------------------------------------------> Player Win // Dispute The Ball Duel 1V1");

                            MatchAudio.Instance.Tackle();
                        }
                        else
                        {
                            playerAnimTrigger_Str = "Lose";
                            opponentAnimTrigger_Str = "Win";

                            minion_Gob = GameObject.Find(PassResolution_Prr.Receiver.Team + '-' + PassResolution_Prr.Receiver.Position);
                            minionAnimator_Gob = GameObject.Find(minion_Gob.name + "/[Animator] Character/Character_Mesh_01");
                            minionAnimator_Gob.GetComponent<Animator>().SetTrigger("Fail");

                            minion_Gob = GameObject.Find(PassResolution_Prr.ReceiverOpponentDuel.Team + '-' + PassResolution_Prr.ReceiverOpponentDuel.Position);
                            minionAnimator_Gob = GameObject.Find(minion_Gob.name + "/[Animator] Character/Character_Mesh_01");
                            minionAnimator_Gob.GetComponent<Animator>().SetTrigger("Pressure"); print("---------------------------------------------> Opponent Win // Dispute The Ball Duel 1V1");


                            GameObject _ball_Gob = GameObject.Find("[Ball]");
                            Vector3 _localPosition_Vct = new Vector3(minion_Gob.GetComponent<Player3D>().BallRefPos_Gob.transform.position.x, 0.025f, minion_Gob.GetComponent<Player3D>().BallRefPos_Gob.transform.position.z);

                            _ball_Gob.transform.SetParent(minion_Gob.GetComponent<Player3D>().BallRefPos_Gob.transform, false);
                            _ball_Gob.transform.position = _localPosition_Vct;
                            _ball_Gob.transform.localScale = Vector3.one;

                            MatchAudio.Instance.Tackle();
                        }
                    }

                    selectedMinion_Gob = GameObject.Find(PassResolution_Prr.Receiver.Team + '-' + PassResolution_Prr.Receiver.Position);
                    PlayerActionResponse selectedMinion_Par = PassResolution_Prr.Receiver;

                    if (selectedMinion_Gob != null && PassResolution_Prr.ReceiverOpponentDuel != null)
                    { StartCoroutine(InstantiateCustomResolutionFX(selectedMinion_Gob, null, selectedMinion_Par, playerAnimTrigger_Str, false, true, "Null")); }

                    selectedMinion_Gob = GameObject.Find(PassResolution_Prr.ReceiverOpponentDuel.Team + '-' + PassResolution_Prr.ReceiverOpponentDuel.Position);
                    selectedMinion_Par = PassResolution_Prr.ReceiverOpponentDuel;

                    if (selectedMinion_Gob != null)
                    { StartCoroutine(InstantiateCustomResolutionFX(selectedMinion_Gob, null, selectedMinion_Par, opponentAnimTrigger_Str, false, true, "Null")); }
                }

                if (!interceptionState_Str.Contains("Intercepted") && PassResolution_Prr.ReceiverOpponentDuel == null)
                {
                    minion_Gob = GameObject.Find(PassResolution_Prr.Receiver.Team + '-' + PassResolution_Prr.Receiver.Position);
                    minionAnimator_Gob = GameObject.Find(minion_Gob.name + "/[Animator] Character/Character_Mesh_01");
                    minionAnimator_Gob.GetComponent<Animator>().SetTrigger("Pass_Reception");
                }

                #endregion
            }
            else //Player Minion Lose Duel --> Stop Pass
            {
                StartCoroutine(SprintMovePass("Null"));
            }

            #endregion

            #region Pass Intercepted By Moving Minion & Wait All Moving Minions Finish Their Move

            yield return new WaitWhile(() => !CheckMinionTarget());

            yield return new WaitWhile(() => !CheckMinionDrawGrp());

            GameObject.Find("[Ball]").transform.SetParent(GameObject.Find("[Team Players Container]")?.transform, true);
            //GameObject.Find("[Ball]").transform.localScale = new Vector3(1.5f, 1.5f, 1.5f);
            GameObject.Find("[Ball]").GetComponent<TrailRenderer>().enabled = false;

            yield return new WaitForSeconds(0.5f);

            foreach (var movingMinion_Var in _allMovingMinionsTargets_Dic)
            {
                //Current Moving Minion Params
                GameObject _movingMinion_Gob = movingMinion_Var.Key;
                GameObject _movingAnimator_Gob = GameObject.Find(_movingMinion_Gob.name + "/[Animator] Character/Character_Mesh_01");
                //GameObject _movingAim_Gob = GameObject.Find(_movingMinion_Gob.name + "/[Animator] Character");

                Animator m_Animator = _movingAnimator_Gob.GetComponent<Animator>();
                AnimatorClipInfo[] m_CurrentClipInfo = m_Animator.GetCurrentAnimatorClipInfo(0);
                //print(_movingMinion_Gob.name + " -----> Current Animation : " + m_CurrentClipInfo[0].clip.name);

                //Activate Idle
                _movingMinion_Gob.GetComponent<Player3D>().CurrentAnim_Str = "Idle";
                _movingAnimator_Gob.GetComponent<Animator>().SetTrigger("Idle_In_Game");
            }

            foreach (var catch_Var in OnFieldLst_Gob)
            { Destroy(catch_Var); }

            OppositionUI.Instance.OnDuelFinished();
            Destroy(gameObject);
            
            #endregion
        }

        //
        //
        // Long Kick
        //
        //

        // Long Kick : Start & Win Duel Against Opponent [Part_1] -> Display Interceptors & Move The Ball To Goalkeeper Position [Part_2] -> Start & Win Duel Against Goalkeeper [Part_3] -> Goal [Part_4] // Resolution_Long_Kick_Case_1
        // Long Kick : Start & Win Duel Against Opponent [Part_1] -> Display Interceptors & Move The Ball To Goalkeeper Position [Part_2] -> Start & Lose Duel Against Goalkeeper [Part_3] // Resolution_Long_Kick_Case_2
        // Long Kick : Start & Win Duel Against Opponent [Part_1] -> Display Interceptors & Move The Ball To The Interceptor [Part_2] -> Move The Ball To The Interceptor [Part_3] // Resolution_Long_Kick_Case_3
        // Long Kick : Start & Lose Duel Against Opponent [Part_1] -> Move The Ball To Opponent Position [Part_2] ----> Go To Duel 1V1  // Resolution_Long_Kick_Case_4
        // Long Kick : No Duel Only Display Selected Minion [Part_1] -> Display Interceptors & Move The Ball To Goalkeeper Position [Part_2] -> Start & Win Duel Against Goalkeeper [Part_3] -> Goal [Part_4] // Resolution_Long_Kick_Case_5
        // Long Kick : No Duel Only Display Selected Minion [Part_1] -> Display Interceptors & Move The Ball To Goalkeeper Position [Part_2] -> Start & Lose Duel Against Goalkeeper [Part_3] // Resolution_Long_Kick_Case_6
        // Long Kick : No Duel Only Display Selected Minion [Part_1] -> Display Interceptors & Move The Ball To Interceptor Position [Part_2] // Resolution_Long_Kick_Case_7
        if (param_Str.Contains("Resolution_Long_Kick"))
        {
            #region First Duel

            int playerMinionScore_Int = 0;
            int opponentMinionScore_Int = 0;

            string playerAnimTrigger_Str = "";
            string opponentAnimTrigger_Str = "";

            //Trigger Animations
            if (ExecuteActionTeamRed_Pr != null)
            {
                playerMinionScore_Int = ExecuteActionTeamBlue_Pr.ExecuteAction.ScoreWithPressure;
                opponentMinionScore_Int = ExecuteActionTeamRed_Pr.ExecuteAction.ScoreWithPressure;

                //Get The Duel Winner
                if (playerMinionScore_Int > opponentMinionScore_Int)
                {
                    playerAnimTrigger_Str = "Win";
                    opponentAnimTrigger_Str = "Lose";

                    GameObject minion_Gob = GameObject.Find(ExecuteActionTeamBlue_Pr.Team + '-' + ExecuteActionTeamBlue_Pr.Position);
                    GameObject minionAnimator_Gob = GameObject.Find(minion_Gob.name + "/[Animator] Character/Character_Mesh_01");
                    minionAnimator_Gob.GetComponent<Animator>().SetTrigger("Pressure");

                    minion_Gob = GameObject.Find(ExecuteActionTeamRed_Pr.Team + '-' + ExecuteActionTeamRed_Pr.Position);
                    minionAnimator_Gob = GameObject.Find(minion_Gob.name + "/[Animator] Character/Character_Mesh_01");
                    minionAnimator_Gob.GetComponent<Animator>().SetTrigger("Fail"); print("---------------------------------------------> Player Win // Dispute The Ball Duel 1V1");

                    MatchAudio.Instance.Tackle();
                }
                else
                {
                    playerAnimTrigger_Str = "Lose";
                    opponentAnimTrigger_Str = "Win";

                    GameObject minion_Gob = GameObject.Find(ExecuteActionTeamBlue_Pr.Team + '-' + ExecuteActionTeamBlue_Pr.Position);
                    GameObject minionAnimator_Gob = GameObject.Find(minion_Gob.name + "/[Animator] Character/Character_Mesh_01");
                    minionAnimator_Gob.GetComponent<Animator>().SetTrigger("Fail");

                    minion_Gob = GameObject.Find(ExecuteActionTeamRed_Pr.Team + '-' + ExecuteActionTeamRed_Pr.Position);
                    minionAnimator_Gob = GameObject.Find(minion_Gob.name + "/[Animator] Character/Character_Mesh_01");
                    minionAnimator_Gob.GetComponent<Animator>().SetTrigger("Pressure"); print("---------------------------------------------> Opponent Win // Dispute The Ball Duel 1V1");

                    MatchAudio.Instance.Tackle();
                }

                //Display Duel On Field Sprite
                GameObject duelField_Gob = Instantiate(PrefabsIdxArray_Gob[Array.IndexOf(PrefabsIdxArray_Str, "Duel_Field_Grp")], GameObject.Find(ExecuteActionTeamBlue_Pr.ExecuteAction.Team + '-' + ExecuteActionTeamBlue_Pr.ExecuteAction.Position).transform.position, Quaternion.identity);
                duelField_Gob.name = "Duel_Field_Case_Player_1";
                OnFieldLst_Gob.Add(duelField_Gob);

                // Orient duel field GO
                if (ExecuteActionTeamBlue_Pr.ExecuteAction.Direction == "TOP")
                { duelField_Gob.transform.rotation = Quaternion.Euler(0, -90, 0); }

                if (ExecuteActionTeamBlue_Pr.ExecuteAction.Direction == "DOWN")
                { duelField_Gob.transform.rotation = Quaternion.Euler(0, 90, 0); }

                if (ExecuteActionTeamBlue_Pr.ExecuteAction.Direction == "LEFT" && !CurrentMode_Str.Contains("PVP"))
                { duelField_Gob.transform.rotation = Quaternion.Euler(0, 180, 0); }

                if (ExecuteActionTeamBlue_Pr.ExecuteAction.Direction == "RIGHT" && !CurrentMode_Str.Contains("PVP"))
                { duelField_Gob.transform.rotation = Quaternion.Euler(0, 0, 0); }

                if (ExecuteActionTeamBlue_Pr.ExecuteAction.Direction == "LEFT" && CurrentMode_Str.Contains("PVP"))
                { duelField_Gob.transform.rotation = Quaternion.Euler(0, 0, 0); }

                if (ExecuteActionTeamBlue_Pr.ExecuteAction.Direction == "RIGHT" && CurrentMode_Str.Contains("PVP"))
                { duelField_Gob.transform.rotation = Quaternion.Euler(0, 180, 0); }
            }

            GameObject selectedMinion_Gob = GameObject.Find(ExecuteActionTeamBlue_Pr.Team + '-' + ExecuteActionTeamBlue_Pr.Position);
            PlayerResponse selectedMinion_Pr = ExecuteActionTeamBlue_Pr;

            if (selectedMinion_Gob != null && ExecuteActionTeamRed_Pr != null)
            { StartCoroutine(InstantiateCustomResolutionFX(selectedMinion_Gob, selectedMinion_Pr, null, playerAnimTrigger_Str, false, false, "Null")); }

            if (ExecuteActionTeamRed_Pr != null)
            {
                selectedMinion_Gob = GameObject.Find(ExecuteActionTeamRed_Pr.Team + '-' + ExecuteActionTeamRed_Pr.Position);
                selectedMinion_Pr = ExecuteActionTeamRed_Pr;
            }

            if (selectedMinion_Gob != null && ExecuteActionTeamRed_Pr != null)
            { StartCoroutine(InstantiateCustomResolutionFX(selectedMinion_Gob, selectedMinion_Pr, null, opponentAnimTrigger_Str, false, false, "Null")); }

            if (ExecuteActionTeamRed_Pr == null)
            {
                StartCoroutine(InstantiateCustomResolutionFX(selectedMinion_Gob, selectedMinion_Pr, null, "Win", false, false, "Null"));
                playerMinionScore_Int = 1;
                opponentMinionScore_Int = 0;
            }

            #endregion

            //Player Minion Win Duel --> Continue Pass
            if (playerMinionScore_Int > opponentMinionScore_Int)
            {
                #region Get The Goal Spot End Target Position

                GameObject goalMidPos_Gob = GameObject.Find("Goal_Red_MID");

                if (CurrentMode_Str.Contains("NPC"))
                { goalMidPos_Gob = GameObject.Find("Goal_Blue_MID"); }

                Vector3 cageSpot_Vct = new Vector3(goalMidPos_Gob.transform.position.x, 0.05f, goalMidPos_Gob.transform.position.z);

                #endregion

                #region Move Line Renderer

                GameObject startPos_Gob = GameObject.Find(ExecuteActionTeamBlue_Pr.Team + '-' + ExecuteActionTeamBlue_Pr.Position);

                lineRenderer_Gob = Instantiate(PrefabsIdxArray_Gob[Array.IndexOf(PrefabsIdxArray_Str, "Line_Renderer_Grp")], GameObject.Find(ExecuteActionTeamBlue_Pr.Team + '-' + ExecuteActionTeamBlue_Pr.Position).transform.position, Quaternion.identity);
                lineRenderer_Gob.name = "Current_Resolution_Path";

                lineRenderer_Gob.GetComponent<LineRenderer>().positionCount = 2;
                lineRenderer_Gob.GetComponent<LineRenderer>().SetPosition(0, startPos_Gob.transform.position);//Start 
                lineRenderer_Gob.GetComponent<LineRenderer>().SetPosition(1, startPos_Gob.transform.position);//End

                OnFieldLst_Gob.Add(lineRenderer_Gob);

                GameObject arrowSprite_Gob = GameObject.Find(lineRenderer_Gob.name + "/Arrow_Pivot_Grp/Arrow");
                GameObject arrowPivot_Gob = GameObject.Find(lineRenderer_Gob.name + "/Arrow_Pivot_Grp");

                MatchAudio.Instance.ArrowAppears();

                if (CurrentMode_Str == "NPC")
                {
                    float alpha = 1.0f;
                    Gradient gradient = new Gradient();
                    gradient.SetKeys(
                        new GradientColorKey[] { new GradientColorKey(new Color(0.3f, 0, 0.1f, 1), 0.0f), new GradientColorKey(new Color(0.8f, 0, 0.2f, 1), 1.0f) },
                        new GradientAlphaKey[] { new GradientAlphaKey(alpha, 0.0f), new GradientAlphaKey(alpha, 1.0f) }
                    );

                    lineRenderer_Gob.GetComponent<LineRenderer>().colorGradient = gradient;
                    arrowSprite_Gob.GetComponent<SpriteRenderer>().color = new Color(1, 0, 0.3098039f, 1);
                }
                else
                {
                    float alpha = 1.0f;
                    Gradient gradient = new Gradient();
                    gradient.SetKeys(
                        new GradientColorKey[] { new GradientColorKey(new Color(0, 0.25f, 1, 1), 0.0f), new GradientColorKey(new Color(0, 0.7f, 1, 1), 1.0f) },
                        new GradientAlphaKey[] { new GradientAlphaKey(alpha, 0.0f), new GradientAlphaKey(alpha, 1.0f) }
                    );

                    lineRenderer_Gob.GetComponent<LineRenderer>().colorGradient = gradient;
                    arrowSprite_Gob.GetComponent<SpriteRenderer>().color = new Color(0, 1f, 1, 1);
                }

                Vector3 currentPos_Vct = lineRenderer_Gob.GetComponent<LineRenderer>().GetPosition(1);
                Vector3 targetPos_Vct = new Vector3(goalMidPos_Gob.transform.position.x, 0.08300001f, goalMidPos_Gob.transform.position.z);

                ArrowMvtSpeed_Flt = 4;

                // Adjuste Speed Following Framerate Drop
                //if (Application.targetFrameRate < 30)
                //{ frameRateDropSpeed_Flt = ArrowMvtSpeed_Flt * 2; }

                float frameRateDropSpeed_Flt = ArrowMvtSpeed_Flt;

                while (Vector3.Distance(currentPos_Vct, targetPos_Vct) > ArrowTargetStopDistance_Flt)
                {
                    currentPos_Vct = Vector3.MoveTowards(currentPos_Vct, targetPos_Vct, frameRateDropSpeed_Flt * Time.deltaTime);
                    lineRenderer_Gob.GetComponent<LineRenderer>().SetPosition(1, currentPos_Vct);

                    arrowSprite_Gob.GetComponent<SpriteRenderer>().enabled = true;
                    arrowPivot_Gob.transform.position = currentPos_Vct;
                    arrowPivot_Gob.transform.LookAt(targetPos_Vct);

                    yield return 0;
                }

                #endregion

                #region Adapt Camera Angle

                if (LongKickResolution_Lkrr.Interceptors.Count == 0)
                { StartCoroutine(_focusOnReception()); }
                else
                {
                    ActionsManager.Instance.DuelPlayer?.UI.TogglePlayerName(true);
                    ActionsManager.Instance.DuelOppPlayer?.UI.TogglePlayerName(true);

                    ActionsUI.Instance.ToggleActionBoard(false);
                    ActionsUI.Instance.ClearResolution();

                    MatchCamera.Instance.DisplayResolutionView();
                    yield return new WaitForSeconds(MatchCamera.Instance.TimeToWait);
                }

                #endregion

                #region Move Ball

                GameObject ball_Gob = GameObject.Find("[Ball]");
                ball_Gob.GetComponent<TrailRenderer>().enabled = true;

                GameObject minion_Gob = GameObject.Find(ExecuteActionTeamBlue_Pr.Team + '-' + ExecuteActionTeamBlue_Pr.Position);
                GameObject minionAnimator_Gob = GameObject.Find(minion_Gob.name + "/[Animator] Character/Character_Mesh_01");
                minionAnimator_Gob.GetComponent<Animator>().SetTrigger("Long_Kick_Ball");

                yield return new WaitForSeconds(0.5f);

                MatchAudio.Instance.Kick();

                //StartCoroutine(MoveDuringPass());

                currentPos_Vct = ball_Gob.transform.position;
                targetPos_Vct = cageSpot_Vct; //GameObject.Find(PassResolution_Prr.Receiver.Team + '-' + PassResolution_Prr.Receiver.Position).GetComponent<Player3D>().BallRefPos_Gob.transform.position;

                float timeStamp_Flt = 0;
                float distance_Flt = (int)Vector3.Distance(currentPos_Vct, targetPos_Vct);

                Vector3 holdStartPos_Vct = currentPos_Vct;
                Vector3 holdEndPos_Vct = targetPos_Vct;
                Vector3 tangentPos_Vct = currentPos_Vct + ((targetPos_Vct - currentPos_Vct).normalized * ((distance_Flt / 100.0f) * 90));// Curve Path At 90% From Current Distance
                tangentPos_Vct = new Vector3(tangentPos_Vct.x, tangentPos_Vct.y + 1.5f, tangentPos_Vct.z);

                List<string> interceptorLst_Str = new List<string>();

                BallMvtSpeed_Flt = 2.5f;

                // Adjuste Speed Following Framerate Drop
                //if (Application.targetFrameRate < 30)
                //{ frameRateDropSpeed_Flt = BallMvtSpeed_Flt * 2; }

                frameRateDropSpeed_Flt = 1;//BallMvtSpeed_Flt;

                string interceptionState_Str = "None";

                while (Vector3.Distance(currentPos_Vct, targetPos_Vct) > BallTargetStopDistance_Flt + 0.1f)
                {
                    timeStamp_Flt += frameRateDropSpeed_Flt * Time.deltaTime;

                    if (timeStamp_Flt < 1.0f)
                    {
                        Vector3 pointA_Vct = Vector3.Lerp(holdStartPos_Vct, tangentPos_Vct, timeStamp_Flt);
                        Vector3 pointB_Vct = Vector3.Lerp(tangentPos_Vct, holdEndPos_Vct, timeStamp_Flt);

                        currentPos_Vct = Vector3.Lerp(pointA_Vct, pointB_Vct, timeStamp_Flt);
                        ball_Gob.transform.position = currentPos_Vct;
                    }

                    //Fade Out Line Renderer 
                    Color StartColorHolder_Col = lineRenderer_Gob.GetComponent<LineRenderer>().startColor;
                    Color endColorHolder_Col = lineRenderer_Gob.GetComponent<LineRenderer>().endColor;

                    float alpha_Flt = StartColorHolder_Col.a;
                    alpha_Flt -= LineFadeOutSpeed_Flt * Time.deltaTime;
                    StartColorHolder_Col = new Color(StartColorHolder_Col.r, StartColorHolder_Col.g, StartColorHolder_Col.b, alpha_Flt);

                    alpha_Flt = endColorHolder_Col.a;
                    alpha_Flt -= LineFadeOutSpeed_Flt * Time.deltaTime;
                    endColorHolder_Col = new Color(endColorHolder_Col.r, endColorHolder_Col.g, endColorHolder_Col.b, alpha_Flt);

                    lineRenderer_Gob.GetComponent<LineRenderer>().startColor = StartColorHolder_Col;
                    lineRenderer_Gob.GetComponent<LineRenderer>().endColor = endColorHolder_Col;

                    GameObject arrow_Gob = GameObject.Find(lineRenderer_Gob.name + "/Arrow_Pivot_Grp/Arrow");
                    arrow_Gob.GetComponent<SpriteRenderer>().color = endColorHolder_Col;

                    if (interceptionState_Str == "None")
                    {
                        //
                        //---------> Interceptors
                        //

                        foreach (var catch_Var in LongKickResolution_Lkrr.Interceptors)
                        {
                            GameObject interceptor_Gob = GameObject.Find(catch_Var.Team + '-' + catch_Var.Position);
                            Vector3 interceptorPos_Vct = new Vector3(interceptor_Gob.transform.position.x, currentPos_Vct.y, interceptor_Gob.transform.position.z);

                            if (Vector3.Distance(new Vector3(currentPos_Vct.x, interceptorPos_Vct.y, currentPos_Vct.z), interceptorPos_Vct) <= 0.5f && !interceptorLst_Str.Contains(interceptor_Gob.name) && !catch_Var.IsPlayerMoving)
                            {
                                interceptorLst_Str.Add(interceptor_Gob.name);
                                GameObject interceptorAnimator_Gob = GameObject.Find(interceptor_Gob.name + "/[Animator] Character/Character_Mesh_01");

                                MatchAudio.Instance.Tackle();

                                if (LongKickResolution_Lkrr.IsBallIntercepted && catch_Var.Position == LongKickResolution_Lkrr.PlayerResolution)
                                {
                                    currentPos_Vct = targetPos_Vct = interceptor_Gob.GetComponent<Player3D>().BallRefPos_Gob.transform.position;
                                    ball_Gob.transform.position = currentPos_Vct;

                                    ball_Gob.transform.SetParent(interceptor_Gob.GetComponent<Player3D>().BallRefPos_Gob.transform.transform, false);
                                    ball_Gob.transform.localPosition = Vector3.zero;
                                    ball_Gob.transform.localScale = new Vector3(0.9f, 0.9f, 0.9f);

                                    interceptionState_Str = "Intercepted_By_Interceptor"; print(interceptionState_Str + " // " + interceptor_Gob.name);
                                }

                                if (interceptionState_Str == "None")
                                {
                                    if (interceptor_Gob.name.Contains("-G"))
                                    {
                                        if (UnityEngine.Random.Range(0, 2) == 0)
                                        { interceptorAnimator_Gob.GetComponent<Animator>().SetTrigger("Long_Kick_Jump_Left"); }
                                        else
                                        { interceptorAnimator_Gob.GetComponent<Animator>().SetTrigger("Long_Kick_Jump_Right"); }
                                    }
                                    else
                                    { interceptorAnimator_Gob.GetComponent<Animator>().SetTrigger("Long_Kick_Intercept"); }

                                    StartCoroutine(InstantiateCustomResolutionFX(interceptor_Gob, null, catch_Var, "Lose", true, false, "Null"));
                                }
                                else
                                {
                                    if (interceptor_Gob.name.Contains("-G"))
                                    { interceptorAnimator_Gob.GetComponent<Animator>().SetTrigger("Long_Kick_Catch"); }
                                    else
                                    { interceptorAnimator_Gob.GetComponent<Animator>().SetTrigger("Long_Kick_Intercept"); }

                                    StartCoroutine(InstantiateCustomResolutionFX(interceptor_Gob, null, catch_Var, "Win", true, false, "Null"));
                                }
                            }
                        }

                        //
                        //---------> Moving Minions
                        //

                        /*foreach (var movingMinion_Var in _allMovingMinions_Dic)
                        {
                            GameObject movingMinion_Gob = movingMinion_Var.Key;
                            Vector3 movingMinionPos_Vct = new Vector3(movingMinion_Gob.transform.position.x, currentPos_Vct.y, movingMinion_Gob.transform.position.z);

                            if (Vector3.Distance(currentPos_Vct, movingMinionPos_Vct) <= 0.25f && !interceptorLst_Str.Contains(movingMinion_Gob.name))
                            {
                                interceptorLst_Str.Add(movingMinion_Gob.name);
                                GameObject movingMinionAnimator_Gob = GameObject.Find(movingMinion_Gob.name + "/[Animator] Character/Character_Mesh_01");
                                movingMinionAnimator_Gob.GetComponent<Animator>().SetTrigger("Tackle");

                                MatchAudio.Instance.Tackle();

                                currentPos_Vct = targetPos_Vct = movingMinion_Gob.GetComponent<Player3D>().BallRefPos_Gob.transform.position;
                                ball_Gob.transform.position = currentPos_Vct;
                                interceptionState_Str = "Intercepted_By_Moving_Minion"; print(interceptionState_Str + " // " + movingMinion_Gob.name);

                                StartCoroutine(InstantiateCustomResolutionFX(movingMinion_Gob, null, null, "Win", true));
                            }
                        }*/
                    }

                    yield return 0;
                }

                //ball_Gob.transform.position = new Vector3(targetPos_Vct.x, ball_Gob.transform.position.y, targetPos_Vct.z);
                lineRenderer_Gob.GetComponent<LineRenderer>().enabled = false;
                GameObject.Find(lineRenderer_Gob.name + "/Arrow_Pivot_Grp/Arrow").GetComponent<SpriteRenderer>().enabled = false;

                #endregion

                #region Play Sound

                if (interceptionState_Str.Contains("Intercepted"))
                { MatchAudio.Instance.GoalMissed(); }
                else
                { MatchAudio.Instance.Goal(); }

                #endregion
            }

            #region Pass Intercepted By Moving Minion & Wait All Moving Minions Finish Their Move

            /*if (interceptionState_Str.Contains("Intercepted_By_Moving_Minion"))
            {


            }*/

            //yield return new WaitWhile(() => !CheckMinionTarget());

            yield return new WaitWhile(() => !CheckMinionDrawGrp());

            GameObject.Find("[Ball]").transform.SetParent(GameObject.Find("[Team Players Container]")?.transform, true);
            //GameObject.Find("[Ball]").transform.localScale = new Vector3(1.5f, 1.5f, 1.5f);
            GameObject.Find("[Ball]").GetComponent<TrailRenderer>().enabled = false;

            yield return new WaitForSeconds(0.5f);

            foreach (var catch_Var in OnFieldLst_Gob)
            { Destroy(catch_Var); }

            OppositionUI.Instance.OnDuelFinished();
            Destroy(gameObject);
            
            #endregion
        }

        //
        //
        // Sprint/Move
        //
        //

        if (param_Str.Contains("Resolution_Sprint"))
        {
            #region Start Sprint/Move
            StartCoroutine(SprintMovePass("Sprint"));

            yield return new WaitWhile(() => !CheckMinionTarget());

            yield return new WaitWhile(() => !CheckMinionDrawGrp());

            GameObject.Find("[Ball]").transform.SetParent(GameObject.Find("[Team Players Container]")?.transform, true);
            //GameObject.Find("[Ball]").transform.localScale = new Vector3(1.5f, 1.5f, 1.5f);
            GameObject.Find("[Ball]").GetComponent<TrailRenderer>().enabled = false;

            yield return new WaitForSeconds(0.5f);

            foreach (var movingMinion_Var in _allMovingMinionsTargets_Dic)
            {
                //Current Moving Minion Params
                GameObject _movingMinion_Gob = movingMinion_Var.Key;
                GameObject _movingAnimator_Gob = GameObject.Find(_movingMinion_Gob.name + "/[Animator] Character/Character_Mesh_01");
                //GameObject _movingAim_Gob = GameObject.Find(_movingMinion_Gob.name + "/[Animator] Character");

                Animator m_Animator = _movingAnimator_Gob.GetComponent<Animator>();
                AnimatorClipInfo[] m_CurrentClipInfo = m_Animator.GetCurrentAnimatorClipInfo(0);
                //print(_movingMinion_Gob.name + " -----> Current Animation : " + m_CurrentClipInfo[0].clip.name);

                //Activate Idle
                _movingMinion_Gob.GetComponent<Player3D>().CurrentAnim_Str = "Idle";
                _movingAnimator_Gob.GetComponent<Animator>().SetTrigger("Idle_In_Game");
            }

            foreach (var catch_Var in OnFieldLst_Gob)
            { Destroy(catch_Var); }

            OppositionUI.Instance.OnDuelFinished();
            Destroy(gameObject);
            #endregion
        }

        print("[ -------------- End Execute Resolution -------------- ]");
    }
}

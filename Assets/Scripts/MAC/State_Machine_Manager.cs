﻿using System.Collections;
using Sportfaction.CasualFantasy.Manager.Profile;
using Sportfaction.CasualFantasy.PVP.Gameplay.Actions;
using Sportfaction.CasualFantasy.PVP.Gameplay.Matchs;
using Sportfaction.CasualFantasy.PVP.Gameplay.Players;
using Sportfaction.CasualFantasy.PVP.Gameplay.UI;

using SportFaction.CasualFootballEngine.ActionService.Enum;
using SportFaction.CasualFootballEngine.ActionService.Response;
using SportFaction.CasualFootballEngine.PlayerService.Enum;
using SportFaction.CasualFootballEngine.PlayerService.Model;
using SportFaction.CasualFootballEngine.TeamService.Enum;

using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class State_Machine_Manager : StateMachineBehaviour
{
    public string SelectedCase_Str = "Null";
    float DelayTimer_Int = 12;
    int InstantiatedNumbersCounter_Int = 1;
    public GameObject ResolutionsManagerGrp_Gob;

    public int FrameCounter_Int = 0;
    public bool StopOnStateUpdate_Bol = false;

    int FrameOffsetEnd_Int = 0;//Original Dice Image Sequence Length 74 -- 2.15

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.GetComponent<Animator>().speed = 1f;
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        /*if (StopOnStateUpdate_Bol)
            return;*/

        FrameCounter_Int++;

        //Debug.Log(stateInfo.length);
        if (SelectedCase_Str == "Instantiate_Numbers")
        {
            GameObject resolutionsManagerGrp_Gob = GameObject.Find("Resolutions_Manager_Grp");

            if (FrameCounter_Int < 40 && resolutionsManagerGrp_Gob.GetComponent<Resolutions_Manager>().CurrentPhase_Str == "Wait")
            {
                DelayTimer_Int++;

                if (DelayTimer_Int > 10)
                {
                    int minRange_Int = (int)resolutionsManagerGrp_Gob.GetComponent<Resolutions_Manager>().CurrentResolutionData_Dic[animator.gameObject.name + "_Score_Min_Range"];
                    int maxRange_Int = (int)resolutionsManagerGrp_Gob.GetComponent<Resolutions_Manager>().CurrentResolutionData_Dic[animator.gameObject.name + "_Score_Max_Range"];

                    resolutionsManagerGrp_Gob.GetComponent<Resolutions_Manager>().InstantiateNumber(animator.gameObject, "Random_Number_Grp", UnityEngine.Random.Range(1, 4), InstantiatedNumbersCounter_Int, minRange_Int, maxRange_Int + 1, Color.white, 1f);

                    DelayTimer_Int = 0;
                    InstantiatedNumbersCounter_Int++;
                }
            }

            //
            //
            //

            if (FrameCounter_Int < 40 && resolutionsManagerGrp_Gob.GetComponent<Resolutions_Manager>().CurrentPhase_Str != "Wait")
            {
                DelayTimer_Int++;

                if (DelayTimer_Int > 8 && resolutionsManagerGrp_Gob.GetComponent<Resolutions_Manager>().CurrentPhase_Str == "Start_Phase")
                {
                    int minRange_Int = resolutionsManagerGrp_Gob.GetComponent<Resolutions_Manager>().PlayerMinRange_Int;
                    int maxRange_Int = resolutionsManagerGrp_Gob.GetComponent<Resolutions_Manager>().PlayerMaxRange_Int;

                    if (animator.gameObject.name.Contains("Opponent"))
                    {
                        minRange_Int = resolutionsManagerGrp_Gob.GetComponent<Resolutions_Manager>().OpponentMinRange_Int;
                        maxRange_Int = resolutionsManagerGrp_Gob.GetComponent<Resolutions_Manager>().OpponentMaxRange_Int;
                    }

                    resolutionsManagerGrp_Gob.GetComponent<Resolutions_Manager>().InstantiateNumber(animator.gameObject, "Random_Number_Grp", UnityEngine.Random.Range(1, 4), InstantiatedNumbersCounter_Int, minRange_Int, maxRange_Int + 1, Color.white, 1f);

                    DelayTimer_Int = 0;
                    InstantiatedNumbersCounter_Int++;
                }
            }
        }
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {     
        if (SelectedCase_Str == "Instantiate_Numbers")
        {
            GameObject resolutionsManagerGrp_Gob = GameObject.Find("Resolutions_Manager_Grp");

            if (resolutionsManagerGrp_Gob.GetComponent<Resolutions_Manager>().CurrentPhase_Str == "Wait")
            {
                int score_Int = (int)resolutionsManagerGrp_Gob.GetComponent<Resolutions_Manager>().CurrentResolutionData_Dic[animator.gameObject.name + "_Score_Before_Pressure"];
                int scoreLevel_Int = (int)resolutionsManagerGrp_Gob.GetComponent<Resolutions_Manager>().CurrentResolutionData_Dic[animator.gameObject.name + "_Score_Level"];

                resolutionsManagerGrp_Gob.GetComponent<Resolutions_Manager>().InstantiateNumber(animator.gameObject, "Chosen_Number_Grp", 1, InstantiatedNumbersCounter_Int, score_Int, score_Int + 1, Color.white, 0);

                animator.GetComponent<Animator>().speed = 0;
                animator.GetComponent<Image>().enabled = false;

                GameObject catch_Gob = GameObject.Find(animator.gameObject.name + "/" + scoreLevel_Int + "_Dice_Img");
                catch_Gob.GetComponent<Image>().enabled = false;

                //Debug.Log("Chosen Text From State Manager Manager // Current Resolution State : " + Resolutions_Manager.Instance.CurrentResolutionCase_Str);

                if (Resolutions_Manager.Instance.CurrentResolutionCase_Str.Contains("Long_Kick"))
                {
                    bool pressureAvailable_Bol = true;
                    bool opponentAvailable_Bol = true;
                    bool interceptorsAvailable_Bol = true;
                    bool skipDiceThrow_Bol = false;

                    // If No Pressure Available
                    if (resolutionsManagerGrp_Gob.GetComponent<Resolutions_Manager>().ExecuteActionTeamBlue_Pr.PressureOpponents.Count == 0)
                    { pressureAvailable_Bol = false; }

                    // If No Opponent Available
                    if (resolutionsManagerGrp_Gob.GetComponent<Resolutions_Manager>().ExecuteActionTeamRed_Pr == null)
                    { opponentAvailable_Bol = false; }

                    if (resolutionsManagerGrp_Gob.GetComponent<Resolutions_Manager>().LongKickResolution_Lkrr.Interceptors.Count == 1)
                    { interceptorsAvailable_Bol = false; }

                    // If No Pressure Available & It No Opponent Available ---> The Skip Dice Throw
                    if (!pressureAvailable_Bol && !opponentAvailable_Bol && !interceptorsAvailable_Bol)
                    { skipDiceThrow_Bol = true; }

                    if (skipDiceThrow_Bol)
                    {
                    }
                }
        
                if (Resolutions_Manager.Instance.CurrentResolutionCase_Str.Contains("Pass"))
                {
                    bool pressureAvailable_Bol = true;
                    bool opponentAvailable_Bol = true;
                    bool interceptorsAvailable_Bol = true;
                    bool skipDiceThrow_Bol = false;

                    // If No Pressure Available
                    if (resolutionsManagerGrp_Gob.GetComponent<Resolutions_Manager>().ExecuteActionTeamBlue_Pr.PressureOpponents.Count == 0)
                    { pressureAvailable_Bol = false; }

                    // If No Opponent Available
                    if (resolutionsManagerGrp_Gob.GetComponent<Resolutions_Manager>().ExecuteActionTeamRed_Pr == null)
                    { opponentAvailable_Bol = false; }

                    if (resolutionsManagerGrp_Gob.GetComponent<Resolutions_Manager>().PassResolution_Prr.Interceptors.Count == 0)
                    { interceptorsAvailable_Bol = false; }

                    // If No Pressure Available & It No Opponent Available ---> The Skip Dice Throw
                    if (!pressureAvailable_Bol && !opponentAvailable_Bol && !interceptorsAvailable_Bol)
                    { skipDiceThrow_Bol = true; }

                    if (skipDiceThrow_Bol)
                    {
                    }
                }
            }

            //
            //
            //

            if (resolutionsManagerGrp_Gob.GetComponent<Resolutions_Manager>().CurrentPhase_Str != "Wait")
            {
                if (resolutionsManagerGrp_Gob.GetComponent<Resolutions_Manager>().CurrentPhase_Str == "Start_Phase")
                {
                    int score_Int = resolutionsManagerGrp_Gob.GetComponent<Resolutions_Manager>().PlayerScoreBeforePressure_Int;
                    int scoreLevel_Int = resolutionsManagerGrp_Gob.GetComponent<Resolutions_Manager>().PlayerDiceScoreLevel_Int;

                    if (animator.gameObject.name.Contains("Opponent"))
                    {
                        score_Int = resolutionsManagerGrp_Gob.GetComponent<Resolutions_Manager>().OpponentScoreBeforePressure_Int;
                        scoreLevel_Int = resolutionsManagerGrp_Gob.GetComponent<Resolutions_Manager>().OpponentDiceScoreLevel_Int;
                    }

                    resolutionsManagerGrp_Gob.GetComponent<Resolutions_Manager>().InstantiateNumber(animator.gameObject, "Chosen_Number_Grp", 1, InstantiatedNumbersCounter_Int, score_Int, score_Int + 1, Color.white, 0);

                    animator.GetComponent<Animator>().speed = 0;
                    animator.GetComponent<Image>().enabled = false;

                    GameObject catch_Gob = GameObject.Find(animator.gameObject.name + "/" + scoreLevel_Int + "_Dice_Img");
                    catch_Gob.GetComponent<Image>().enabled = false;
                }
            }





        }       
    }

    // OnStateMove is called right after Animator.OnAnimatorMove()
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that processes and affects root motion
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK()
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that sets up animation IK (inverse kinematics)
    //}
}

﻿using System.Collections;
using Sportfaction.CasualFantasy.Events;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Sportfaction.CasualFantasy
{
    /// <summary>
    /// Description: SceneManager
    /// Author: Antoine de Lachèze-Murel
    /// </summary>
    public sealed class ScenesMain : MonoBehaviour
    {

        public static ScenesMain Instance;

        private bool _metaScene;

        [Header("Game Events")]
        [SerializeField]
        private GameEvent _onEndLoadAsyncMetaScene;

        #region Public Methods

        public IEnumerator LoadAsyncGameplayScene()
        {
            AsyncOperation asyncLoad = SceneManager.LoadSceneAsync("Gameplay3D", LoadSceneMode.Additive);

            while (!asyncLoad.isDone)
            {
                yield return null;
            }

            SceneManager.UnloadSceneAsync("Meta");
        }

        public IEnumerator UnLoadAsyncGameplayScene()
        {
            AsyncOperation asyncLoad = SceneManager.LoadSceneAsync("Meta", LoadSceneMode.Additive);

            while (!asyncLoad.isDone)
            {
                yield return null;
            }

            SceneManager.UnloadSceneAsync("Gameplay3D");
            SceneManager.UnloadSceneAsync("GameplayLogic");
            SceneManager.UnloadSceneAsync("GameplayUI");
        }

        public IEnumerator LoadAsyncMetaScene()
        {
            if(false == _metaScene)
            {
                _metaScene = true;
                Instance = this;
                AsyncOperation asyncLoad = SceneManager.LoadSceneAsync("Meta", LoadSceneMode.Additive);

                while (!asyncLoad.isDone)
                {
                    yield return null;
                }
                _onEndLoadAsyncMetaScene.Invoke();
            }
        }

        public void UnloadAsyncMetaScene()
        {
            #if DEVELOPMENT_BUILD
                                SceneManager.UnloadSceneAsync("PostSplashScreenDEV");
            #else
                        SceneManager.UnloadSceneAsync("PostSplashScreen");
            #endif
        }

        #endregion

        #region Private Methods

        void Awake()
        {
            Instance = this;

            #if DEVELOPMENT_BUILD
                        SceneManager.LoadSceneAsync("PostSplashScreenDEV", LoadSceneMode.Additive);
            #else
            
                         SceneManager.LoadSceneAsync("PostSplashScreen", LoadSceneMode.Additive); 
            #endif
             SceneManager.LoadSceneAsync("MetaData", LoadSceneMode.Additive);

        }

        #endregion
    }
}

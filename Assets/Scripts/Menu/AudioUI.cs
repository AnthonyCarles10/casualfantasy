using UnityEngine;

namespace Sportfaction.CasualFantasy.Menu
{
    /// <summary>
    /// Description: AudioUI
    /// Author: Antoine de Lachèze-Murel
    /// </summary>
    public class AudioUI : MonoBehaviour
    {
        #region Fields

        [Header("Audio")]

        [SerializeField, Tooltip("Actions source")]
        private AudioSource _actionsSfx = null;

        [SerializeField, Tooltip("Click Audio")]
        private AudioClip _clickAudio = null;

        [SerializeField, Tooltip("Back Audio")]
        private AudioClip _backAudio = null;

        [SerializeField, Tooltip("Drop Audio")]
        private AudioClip _dropAudio = null;

        [SerializeField, Tooltip("Drag Audio")]
        private AudioClip _dragAudio = null;

        [SerializeField, Tooltip("Refresh Audio")]
        private AudioClip _refreshAudio = null;

        #endregion

        #region Public Methods

        public void OnClickSound()
        {
            _actionsSfx.PlayOneShot(_clickAudio);
        }

        public void OnBackSound()
        {
            _actionsSfx.PlayOneShot(_backAudio);
        }

        public void OnDropSound()
        {
            _actionsSfx.PlayOneShot(_dropAudio);
        }

        public void OnDragSound()
        {
            _actionsSfx.PlayOneShot(_dragAudio);
        }

        public void OnRefreshSound()
        {
            _actionsSfx.PlayOneShot(_refreshAudio);
        }

        #endregion

        #region Private Methods



        #endregion
    }

}

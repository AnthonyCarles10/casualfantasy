﻿using UnityEngine;
using UnityEngine.Purchasing;
using UnityEngine.UI;

namespace Sportfaction.CasualFantasy.Shop
{
    public class ShopCashPrefab : MonoBehaviour
    {
        #region Fields

        [SerializeField, Tooltip("_soft")]
        private Text _soft = null;

        [SerializeField, Tooltip("_bonus")]
        private GameObject _bonus = null;

        [SerializeField, Tooltip("_softBeforeBonus")]
        private Text _softBeforeBonus = null;

        [SerializeField, Tooltip("_softBonus")]
        private Text _softBonus = null;

        [SerializeField, Tooltip("Icon")]
        private Image _icon = null;


        [SerializeField, Tooltip("FlagGo")]
        private GameObject _flagGo = null;

        [SerializeField, Tooltip("Flag")]
        private Text _flag = null;

        [SerializeField, Tooltip("Price")]
        private Text _price = null;



        public string ProductId = null;

        #endregion

        #region Public Methods
        public void Init(ShopCashItemData pItem, Product lProduct)
        {
            _soft.text = pItem.Sf;

            if (true == string.IsNullOrEmpty(pItem.SfBeforeBonus))
            {
                _bonus.SetActive(false);
            }
            else
            {
                string lTitle = I2.Loc.LocalizationManager.GetTranslation(pItem.Flag);
                _flag.text = lTitle;
            }

            _softBeforeBonus.text = pItem.SfBeforeBonus;
            _softBonus.text = pItem.SfBonus;

            if (true == string.IsNullOrEmpty(pItem.Flag))
            {
                _flagGo.SetActive(false);
            }

            _icon.sprite = pItem.Icon;
            _price.text = lProduct.metadata.localizedPriceString + " " + lProduct.metadata.isoCurrencyCode;
            ProductId = pItem.ProductId;
        }

        #endregion



    }
}

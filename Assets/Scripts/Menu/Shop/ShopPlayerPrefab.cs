using Sportfaction.CasualFantasy.Players;

using UnityEngine;
using UnityEngine.UI.Extensions;

namespace Sportfaction.CasualFantasy.Menu
{
    public class ShopPlayerPrefab : MonoBehaviour
    {
        #region Fields

        [SerializeField, Tooltip("UI")]
        public GameObject PanelUI = null;

        [SerializeField, Tooltip("3D")]
        public GameObject Panel3D = null;

        [SerializeField, Tooltip("RadarPolygonFill")]
        private RadarPolygon _radarPolygonFill = null;

        [SerializeField, Tooltip("RadarPolygonLine")]
        private RadarPolygon _radarPolygonLine = null;

        [SerializeField, Tooltip("Player Name")]
        private TMPro.TMP_Text _playerName = null;

        [SerializeField, Tooltip("Squad Position")]
        private TMPro.TMP_Text _squadPosition = null;

        [SerializeField, Tooltip("Player Name")]
        private TMPro.TMP_Text _price = null;

        [SerializeField, Tooltip("plaque")]
        public MeshRenderer Plaque = null;

        [SerializeField, Tooltip("shirt")]
        public MeshRenderer Shirt = null;


        public string NameShort = null;
        public string SquadPosition = null;
        public string Position = null;
        public string PlayerId = null;





        #endregion

        #region Public Methods
        public void Init(PlayersItem pData)
        {

            //NameShort = pData.Profile.NameShort;
            //PlayerId = pData.Profile.PlayerId;
            //Position = pData.Profile.Position;

            //_playerName.text = pData.Profile.NameShort;

            //_squadPosition.text = I2.Loc.LocalizationManager.GetTranslation(SquadPosition);

            //_radarPolygonFill.value[0] = pData.Profile.Attack / 100f;
            //_radarPolygonFill.value[1] = pData.Profile.Technical / 100f;
            //_radarPolygonFill.value[2] = pData.Profile.Goalkeeper / 100f;
            //_radarPolygonFill.value[3] = pData.Profile.Defense / 100f; 
            //_radarPolygonFill.value[4] = pData.Profile.FitnessMental / 100f;

            _radarPolygonLine.value[0] = _radarPolygonFill.value[0];
            _radarPolygonLine.value[1] = _radarPolygonFill.value[1];
            _radarPolygonLine.value[2] = _radarPolygonFill.value[2];
            _radarPolygonLine.value[3] = _radarPolygonFill.value[3];
            _radarPolygonLine.value[4] = _radarPolygonFill.value[4];


            //switch (pData.Profile.Level)
            //{
            //    case "0":
            //        _price.text = "0 SF";
            //        break;
            //    case "1":
            //        _price.text = "1 000 SF";
            //        Plaque.material = Resources.Load<Material>("Materials/MAT_plaque_bronze");
            //        break;
            //    case "2":
            //        _price.text = "3 000 SF";
            //        Plaque.material = Resources.Load<Material>("Materials/MAT_socle_silver");
            //        break;
            //    case "3":
            //        _price.text = "10 000 SF";
            //        Plaque.material = Resources.Load<Material>("Materials/MAT_plaque_or");
            //        break;
            //    default:
            //        _playerName.text = "";
            //        _price.text = "";
            //        this.transform.GetComponent<Collider>().enabled = false;
            //        break;
            //}

            if (null != Resources.Load<Material>("Materials/Countries/MAT_" + pData.Profile.OfficialTeamCountryId))
            {
                Shirt.material = Resources.Load<Material>("Materials/Countries/MAT_" + pData.Profile.OfficialTeamCountryId);
            }
        }

        #endregion



    }
}

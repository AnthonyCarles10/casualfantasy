﻿using Sportfaction.CasualFantasy.Manager.Profile;
using Sportfaction.CasualFantasy.Services.Billing;
using Sportfaction.CasualFantasy.Services.Userstracking;
using Sportfaction.CasualFantasy.Shop;

using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Purchasing;
using UnityEngine.UI;

namespace Sportfaction.CasualFantasy.Menu
{
    /// <summary>
    /// Description: MenuUI
    /// Author: Antoine de Lachèze-Murel
    /// </summary>
    public class ShopUI : MonoBehaviour
    {
        #region Fields

		[Header("Data")]
		[SerializeField, Tooltip("Scriptable object contains data of IAP products")]
		private	IAPProductsData		_IAPProductsData	=	null;

        [Header("Link")]
        [SerializeField, Tooltip("CashDatabase")]
        private ShopCashData _shopCashData = null;

        [Header("Parents")]

        [SerializeField, Tooltip("VipPack")]
        private GameObject _vipPack = null;

        [SerializeField, Tooltip("VipPackPrice")]
        private Text _vipPackPrice = null;

        [SerializeField, Tooltip("CashParent")]
        private GameObject _cashParent = null;

        [Header("UI")]

        [SerializeField, Tooltip("Canvas Group")]
        public GameObject CanvasGroup = null;

        [SerializeField, Tooltip("DefaultUI Script")]
        private DefaultUI _defaultUI = null;

        [SerializeField, Tooltip("_loader")]
        private GameObject _loader = null;

        [SerializeField, Tooltip("_backButton")]
        private GameObject _backButton = null;


        #endregion

        #region Public Methods


        public void OnCashPurchaseSucceed()
        {
            _defaultUI.AnimateCanevasIn(_backButton, 0.1f);
            _defaultUI.AnimateCanevasOut(_loader, 0.3f);
        }

        public void OnCashPurchaseFailed()
        {
            _defaultUI.AnimateCanevasIn(_backButton, 0.1f);
            _defaultUI.AnimateCanevasOut(_loader, 0.3f);
        }

        public void OnPurchaseFailedEvent()
        {
            _defaultUI.AnimateCanevasIn(_backButton, 0.1f);
            _defaultUI.AnimateCanevasOut(_loader, 0.3f);
        }

        public void BuyWithCash(string pProductId)
        {

            #if UNITY_IOS
                _defaultUI.AnimateCanevasOut(_backButton, 0.1f);
            #endif

            _defaultUI.AnimateCanevasIn(_loader, 0.3f);

            UserEventsManager.Instance.RecordEvent(new UserActionEvent("purchase_init_" + pProductId));

            IAPStoreManager.Instance.InitiatePurchase(pProductId);

            // string[] m_buttonsArray = new string[] { I2.Loc.LocalizationManager.GetTranslation("No"), I2.Loc.LocalizationManager.GetTranslation("Yes") };
            // AlertDialogs.ShowAlert(
            //    I2.Loc.LocalizationManager.GetTranslation("PurchaseCashTitle"),
            //    I2.Loc.LocalizationManager.GetTranslation("PurchaseCashMessage"),
            //    m_buttonsArray,
            //    (_buttonPressed) => {

            //        if (_buttonPressed == I2.Loc.LocalizationManager.GetTranslation("Yes"))
            //        {
            //            IAPStoreManager.Instance.InitiatePurchase(pProductId);

            //        } else{

            //            EnableCashCollider(0.3f);
            //        }
            //    }
            //);
        }

        public void OnGetProfileEvent()
        {
            if (null != UserData.Instance && null != UserData.Instance.Profile && true == UserData.Instance.Profile.IsVip)
            {
                _vipPack.SetActive(false);
            }
        }

       


        #endregion

        #region Private Methods

        private void Start()
        {
            if (null != UserData.Instance && null != UserData.Instance.Profile && true == UserData.Instance.Profile.IsVip)
            {
                _vipPack.SetActive(false);
            }
            else
            {
				Assert.IsNotNull(_IAPProductsData, "[ShopUI] Start(), _IAPProductsData is null.");

				Assert.IsNotNull(_IAPProductsData.Products, "[ShopUI] Start(), _IAPProductsData.Products is null.");

                Product lProduct = _IAPProductsData.Products.Find(item => item.definition.id == _shopCashData.Items[0].ProductId);
                _vipPackPrice.text = lProduct.metadata.localizedPriceString + " " + lProduct.metadata.isoCurrencyCode;
                _vipPack.name = "shop-cash-" + lProduct.definition.id;
            }
            for (int i = 0; i < _shopCashData.Items.Count - 1; i++)
            {
                InitPrefab(i);
            }
        }

        private void InitPrefab(int pIndex)
        {
			Assert.IsNotNull(_IAPProductsData, "[ShopUI] InitPrefab(), _IAPProductsData is null.");

			Assert.IsNotNull(_IAPProductsData.Products, "[ShopUI] InitPrefab(), _IAPProductsData.Products is null.");

            Product lProduct = _IAPProductsData.Products.Find(item => item.definition.id == _shopCashData.Items[pIndex + 1].ProductId);

            Transform lCashPrefab = _cashParent.transform.GetChild(pIndex);
            ShopCashPrefab lCashPrefabScript = lCashPrefab.GetComponent<ShopCashPrefab>();

            lCashPrefabScript.Init(_shopCashData.Items[pIndex + 1], lProduct);
            lCashPrefab.name = "shop-cash-" + lProduct.definition.id;
        }

        #endregion
    }
}


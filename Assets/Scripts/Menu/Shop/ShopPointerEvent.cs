using UnityEngine;
using UnityEngine.EventSystems;

namespace Sportfaction.CasualFantasy.Menu
{
    /// <summary>
    /// Description: ShopPointerEvent
    /// Author: Antoine de Lachèze-Murel
    /// </summary>
    public class ShopPointerEvent : MonoBehaviour, IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler
    {
    
        #region Fields

        [Header("Links")]

        [SerializeField, Tooltip("ShopUI")]
        private ShopUI _shopUI = null;

        [SerializeField, Tooltip("DefaultUI")]
        private DefaultUI _defaultUI = null;

        [SerializeField, Tooltip("AudioUI")]
        private AudioUI _audioUI = null;

        [SerializeField, ReadOnly, Tooltip("_currentTransform")]
        private Transform _currentTransform = null;

        #endregion

        #region Public Methods

        /// <summary>
        /// Called by the EventSystem when the pointer
        /// enters the object associated with this EventTrigger.
        /// </summary>
        public void OnPointerEnter(PointerEventData eventData)
        {
            if (null != eventData.pointerEnter)
            {
                switch (eventData.pointerEnter.transform.name)
                {
                    case "[Buy]":
                        _audioUI.OnClickSound();
                        _defaultUI.AnimateButtonEnter(eventData.pointerEnter.transform.parent.transform);
                        _currentTransform = eventData.pointerEnter.transform;
                        break;
                    default:
                        if (eventData.pointerEnter.transform.name.Contains("shop-cash"))
                        {
                            _audioUI.OnClickSound();
                            _defaultUI.AnimateButtonEnter(eventData.pointerEnter.transform);
                            _currentTransform = eventData.pointerEnter.transform;
                        }
                        break;
                }
            }

        }

        /// <summary>
        /// Called by the EventSystem when the pointer
        /// enters the object associated with this EventTrigger.
        /// </summary>
        public void OnPointerExit(PointerEventData eventData)
        {
            if (null != _currentTransform)
            {
                _defaultUI.AnimateButtonUp(_currentTransform);
            }
        }

        /// <summary>
        /// Register button presses using the IPointerClickHandler
        /// </summary>
        public void OnPointerClick(PointerEventData eventData)
        {
            if (null != _currentTransform)
            {
                _defaultUI.AnimateButtonUp(_currentTransform);
                Buy();
            }
        }

        #endregion

        #region Private Methods

        private void Buy()
        {
            if (null != _currentTransform)
            {

                string lProductId = _currentTransform.transform.name.Substring(10, _currentTransform.transform.name.Length - 10);

                _shopUI.BuyWithCash(lProductId);
            }
        }

        #endregion
    }
}
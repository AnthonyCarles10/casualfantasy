﻿using Sportfaction.CasualFantasy.Events;
using Sportfaction.CasualFantasy.Menu;
using Sportfaction.CasualFantasy.Menu.Squad;
using Sportfaction.CasualFantasy.Players;
using Sportfaction.CasualFantasy.Services.Userstracking;
using Sportfaction.CasualFantasy.Services;

using DG.Tweening;
//using Facebook.Unity;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Sportfaction.CasualFantasy.Squad
{
	/// <summary>
	/// Description:
	/// Author: Antoine de Lachèze-Murel
	/// </summary>
    #pragma warning disable 414, CS0649
    public sealed class PlayerField : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IDropHandler, IDragHandler, IBeginDragHandler, IEndDragHandler, IPointerDownHandler
    {
        #region Properties

        public GameObject Empty { get { return _empty; } }

        public GameObject Player { get { return _player; } }

        public GameObject Infos { get { return _infos; } }

        public GameObject Droppable { get { return _droppable; } }

        public PlayersItem PlayersItem { get { return _playerItem; } }

        public string PlayerName { get { return _playerName.text; } set { _playerName.text = value; } }

        public string SquadPosition { get { return _squadPosition; }}

        #endregion

        #region Fields

        [Header("Links")]
        [SerializeField, Tooltip("Squad UI")]
        private	SquadUI		_squadUI		=	null;
        [SerializeField, Tooltip("Default UI")]
        private	DefaultUI	_defaultUI		=	null;
        [SerializeField, Tooltip("PopinUI")]
        public	PopinUI		_popinUI		=	null;
        //[SerializeField, Tooltip("Match Engine")]
        //private	MatchEngine	_engine			=	null;
        [SerializeField, Tooltip("PlayerRadar")]
        private	PlayerRadar	_playerRadar	=	null;

        [Header("UI")]

        [SerializeField, Tooltip("Player")]
        private GameObject _player = null;

        [SerializeField, Tooltip("Empty")]
        private GameObject _empty = null;

        [SerializeField, Tooltip("Infos")]
        private GameObject _infos = null;

        [SerializeField, Tooltip("Droppable")]
        private GameObject _droppable = null;

        [SerializeField, Tooltip("Draggable Game Object")]
        private GameObject _draggableGameObject = null;

        [SerializeField, Tooltip("_dragArea")]
        private RectTransform _dragArea;

        [SerializeField, ReadOnly, Tooltip("_beginDrag")]
        private Vector2 _beginDrag;

        [SerializeField, ReadOnly, Tooltip("_offset")]
        private float _offset;

        //[SerializeField, Tooltip("_onEnterPlayer")]
        //private RectTransform _onEnterPlayer = null;
        //[SerializeField, Tooltip("_onEnterPlayerShirt")]
        //private CanvasGroup _onEnterPlayerShirt = null;

        [SerializeField, Tooltip("Player Name")]
        private TMPro.TMP_Text _playerName = null;

        [SerializeField, Tooltip("Plate")]
        private	Image	_plate		=	null;
        [SerializeField, Tooltip("Plate Infos")]
        private	Image	_plateInfos	=	null;

        [SerializeField, Tooltip("Player Name Infos")]
        private TMPro.TMP_Text _playerNameInfos = null;

        [SerializeField, Tooltip("Value")]
        private	Text	_value	=	null;
        [SerializeField, Tooltip("Att")]
        private	Text	_att	=	null;
        [SerializeField, Tooltip("Def")]
        private	Text	_def	=	null;
        [SerializeField, Tooltip("Rec")]
        private	Text	_rec	=	null;
        [SerializeField, Tooltip("Int")]
        private	Text	_int	=	null;

        [SerializeField, Tooltip("_gBlockPass")]
        private	TMPro.TMP_Text	_gBlockPass	=	null;
        [SerializeField, Tooltip("_gBlockSave")]
        private	TMPro.TMP_Text	_gBlockSave	=	null;

        [SerializeField, Tooltip("Squad Position")]
        private string _squadPosition = "";

        [SerializeField, Tooltip("T-Shirt")]
        private	Image	_tShirt				=	null;
        [SerializeField, Tooltip("T-Shirt Draggable")]
        private	Image	_tShirtDraggable	=	null;

		#pragma warning disable 0414
        [SerializeField, ReadOnly, Tooltip("OnEnterPlayer Local Position")]
        private	Vector3	_onEnterPlayerLocalPosition	=	Vector3.zero;
        [SerializeField, ReadOnly, Tooltip("On Focus  Position")]
        private	string	_onFocusPosition			=	"";
        [SerializeField, ReadOnly, Tooltip("On Focus Squad Position")]
        private	string	_onFocusSquadPosition		=	"";
        
		[Header("Data")]
        [SerializeField, ReadOnly, Tooltip("PlayersItem")]
        private	PlayersItem	_playerItem	=	null;
        [SerializeField, ReadOnly, Tooltip("_canOnDrag")]
        private	bool		_canOnDrag	=	false;

        [Header("Drag & Drop")]
        [SerializeField, ReadOnly, Tooltip("Screen Pos")]
        private	Vector3	_screenPos			=	Vector3.zero;
        [SerializeField, ReadOnly, Tooltip("Start Local Position")]
        private	Vector3	_startLocalPosition	=	Vector3.zero;
        [SerializeField, ReadOnly, Tooltip("On Drag")]
        private	bool	_onDrag				=	false;
		#pragma warning restore 0414

        [Header("FX")]
        [SerializeField, Tooltip("_dragAnimation")]
        private	Animator		_dragAnimation		=	null;
        [SerializeField, Tooltip("_removeAnimation")]
        private	Animator		_removeAnimation	=	null;
        [SerializeField, Tooltip("_dropFx")]
        private	ParticleSystem	_dropFx				=	null;

        [Header("GameEvent")]
        [SerializeField]
        private	GameEvent	_onPlayerIsDropped			=	null;
        [SerializeField]
        private	GameEvent	_onPointerEnterPlayerField	=	null;
        [SerializeField]
        private	GameEvent	_onPointerExitPlayerField	=	null;

		private	Dictionary<String, Sprite>	_playerSprites	=	new Dictionary<string, Sprite>();
		
		[Header("Configuration")]
		[SerializeField, Tooltip("Player shirts database")]
		private	PlayersShirtDatabase	_shirtsDatabase	=	null;
		
        //private	Sequence	_sequence	=	null;
        private	Color		_color		=	new Color();
        
        #endregion

        #region Public Methods
        
        /// <summary>
        /// Initializes player infos on field
        /// </summary>
        /// <param name="lPlayer">Player data</param>
        /// <param name="pBuyPlayer">Has player been bought ?</param>
        /// <param name="withDelay">Should delay display ?</param>
        /// <param name="pDictionary">Sprite dictionary</param>
        public	void	Init(PlayersItem lPlayer, bool pBuyPlayer = true, bool withDelay = false, Dictionary<string, Sprite> pDictionary = null)
		{
			if (0 == _playerSprites.Count && null != pDictionary)
				_playerSprites	=	pDictionary;
			
            _playerItem		=	lPlayer;
            _squadPosition	=	this.name;
            _playerItem.Profile.SquadPosition	=	_squadPosition;

            if (true == withDelay)
            {
                _player.SetActive(true);
                StartCoroutine(_defaultUI.AnimateCanevasInWithDelay(_player, 0.5f, 0.3f));
                _defaultUI.AnimateCanevasOut(_empty, 0.5f);
            }
            else
            {
                _defaultUI.AnimateCanevasInOut(_player, _empty, 0.1f);
            }

            InitPrefab(lPlayer, pBuyPlayer);
        }

		/// <summary>
		/// Sets up player item to display
		/// </summary>
		/// <param name="lPlayer">Player infos</param>
		/// <param name="pBuyPlayer">If set to <c>true</c> p buy player.</param>
        public	void	InitPrefab(PlayersItem lPlayer, bool pBuyPlayer = true)
        {
   //         _playerName.text		=	StringExtensions.FirstCharToUpper(lPlayer.Profile.NameField);
   //         _playerNameInfos.text	=	StringExtensions.FirstCharToUpper(lPlayer.Profile.NameField);

   //         _tShirt.sprite	=	_shirtsDatabase.FindShirtByTeamId(lPlayer.Profile.TeamId);

   //         //int lValue = Mathf.RoundToInt(lPlayer.Profile.Value);
   //         //var nfi = (NumberFormatInfo)CultureInfo.InvariantCulture.NumberFormat.Clone();
   //         //nfi.NumberGroupSeparator = " ";
   //         //string formatted = lValue.ToString("n0", nfi);

   //         _value.text	=	lPlayer.Profile.Value.ToString();
   //         _att.text	=	lPlayer.Profile.PvpAttack.ToString();

   //         if ("G" == lPlayer.Profile.Position)
   //         {
   //             _def.text			=	lPlayer.Profile.PvpGoalkeeping.ToString();
   //             _gBlockPass.text	=	lPlayer.Profile.GenericPassChallenge.ToString();
   //             _gBlockSave.text	=	lPlayer.Profile.LongSaveChallenge.ToString();
   //         }
   //         else
   //         {
   //             _playerRadar.Init(lPlayer);
   //             _def.text	=	lPlayer.Profile.PvpDefense.ToString();
     
   //             _int.text	=	lPlayer.Profile.InterceptChallenge.ToString();
   //             _rec.text	=	lPlayer.Profile.ReceiveChallenge.ToString();
   //         }

   //         if(true == pBuyPlayer)
   //         {
   //             _squadUI.BuyPlayer(lPlayer.Profile.Value, lPlayer.Profile.RareLevel);

   //             _squadUI.AddAttack(lPlayer.Profile.PvpAttack);

   //             _squadUI.AddDefense(lPlayer.Profile.PvpDefense);
   //         }

			//Sprite	lSprite	=	null;
			//_playerSprites.TryGetValue("Squad_BG_PlayerName" + lPlayer.Profile.RareLevel, out lSprite);
			
   //         if (null != lSprite)
   //         {
   //             _plate.sprite		=	lSprite;
   //             _plateInfos.sprite	=	lSprite;
   //         }

   //         if(lPlayer.Profile.RareLevel > 1)
   //         {
   //             ColorUtility.TryParseHtmlString("#000000FF", out _color); // Black
   //             _playerName.color		=	_color;
   //             _playerNameInfos.color	=	_color;
   //         }
   //         else
   //         {
   //             ColorUtility.TryParseHtmlString("#FFFFFFFF", out _color); // Black
   //             _playerName.color		=	_color;
   //             _playerNameInfos.color	=	_color;
   //         }
        }

        public	void	ChangeStatusInfosAction()
        {
            if ("" != _playerName.text && true == _squadUI.InfosStatus)
            {
                _defaultUI.AnimateCanevasIn(_infos, 0.3f);

                _defaultUI.AnimateCanevasOut(_player, 0.3f);
            }
            else if("" != _playerName.text)
            {
                _defaultUI.AnimateCanevasOut(_infos, 0.3f);

                _defaultUI.AnimateCanevasIn(_player, 0.3f);
            }
        }

        public	void	IsDroppable()
        {
            //_droppable.SetActive(true);
            //if(null == _squadUI.PlayerIsDraggedFromList)
            //{
            //    if (_squadUI.PlayerIsDraggedFromField.Profile.Position == this.transform.parent.name)
            //    {
            //        _droppable.transform.GetChild(0).gameObject.SetActive(false);
            //        _droppable.transform.GetChild(1).gameObject.SetActive(true);
            //    }
            //    else if (_squadUI.PlayerIsDraggedFromField.Profile.Position != this.transform.parent.name)
            //    {
            //        _droppable.transform.GetChild(0).gameObject.SetActive(true);
            //        _droppable.transform.GetChild(1).gameObject.SetActive(false);
            //    }
            //}
            //else
            //{
            //    if (_squadUI.PlayerIsDraggedFromList.Profile.Position == this.transform.parent.name)
            //    {
            //        _droppable.transform.GetChild(0).gameObject.SetActive(false);
            //        _droppable.transform.GetChild(1).gameObject.SetActive(true);
            //    }
            //    else if (_squadUI.PlayerIsDraggedFromList.Profile.Position != this.transform.parent.name)
            //    {
            //        _droppable.transform.GetChild(0).gameObject.SetActive(true);
            //        _droppable.transform.GetChild(1).gameObject.SetActive(false);
            //    }
            //}

        }

        /// <summary>
        /// Is called when the user has pressed 
        /// the mouse button while
        /// over the GUIElement or Collider.
        /// </summary>
        public	void	OnPointerDown(PointerEventData eventData)
        {
            if (null == eventData.pointerEnter) {
                StartCoroutine(_squadUI.FilterByPosition("NONE"));
            } else {
                switch (eventData.pointerEnter.transform.parent.name)
                {
                    case "G":
                        StartCoroutine(_squadUI.FilterByPosition("G"));
                        break;
                    case "D":
                        StartCoroutine(_squadUI.FilterByPosition("D"));
                        break;
                    case "M":
                        StartCoroutine(_squadUI.FilterByPosition("M"));
                        break;
                    case "A":
                        StartCoroutine(_squadUI.FilterByPosition("A"));
                        break;
                    default:
                        StartCoroutine(_squadUI.FilterByPosition("NONE"));
                        break;
                }
            }
        }

        /// <summary>
        /// Called by the EventSystem
        /// when the pointer enters
        /// the object associated
        /// with this EventTrigger.
        /// </summary>
        public	void	OnPointerEnter(PointerEventData pEventData)
        {
            if(false == _onDrag && true == _squadUI.OnDragPlayerList && false == _player.activeSelf)
            {
                _onPointerEnterPlayerField.Invoke();
            }

            if(true == _squadUI.OnDragPlayerList || true == _squadUI.OnDragPlayerField)
            {
                IsDroppable();
            }
            
            _onFocusPosition = pEventData.pointerEnter.transform.parent.name;
            _onFocusSquadPosition = pEventData.pointerEnter.transform.name;
        }

        /// <summary>
        /// Called by the EventSystem
        /// when the pointer Exit
        /// the object associated
        /// with this EventTrigger.
        /// </summary>
        public	void	OnPointerExit(PointerEventData pEventData)
        {
            if (true == _squadUI.OnDragPlayerList && "" == _playerName.text && false == _onDrag) {

                // _empty.SetActive(true);

                _droppable.SetActive(false);

                _onPointerExitPlayerField.Invoke();
            }

            if (true == _squadUI.OnDragPlayerList || true == _squadUI.OnDragPlayerField)
            {
                _droppable.SetActive(false);
            }
        }

        public	void	OnBeginDrag(PointerEventData eventData)
        {
            //if (true == _empty.activeSelf || false == _squadUI.Edit || true == _squadUI.InfosStatus)
            //{
            //    _canOnDrag = false;
            //}
            //else
            //{
            //    _canOnDrag	=	true;
            //    _squadUI.PlayerIsDraggedFromList	=	null;
            //    _squadUI.PlayerIsDraggedFromField	=	_playerItem;
                
            //    if (false == _squadUI.InfosStatus)
            //    {
            //        _defaultUI.AnimateCanevasIn(_empty, 0.1f, false);
            //    }

            //    _tShirtDraggable.sprite	=	_shirtsDatabase.FindShirtByTeamId(_playerItem.Profile.TeamId);

            //    RectTransformUtility.ScreenPointToLocalPointInRectangle(_dragArea, eventData.position, eventData.pressEventCamera, out _beginDrag);

            //    _draggableGameObject.SetActive(true);
            //    _draggableGameObject.transform.localPosition = _beginDrag;

            //    _defaultUI.AnimateCanevasOut(_player, 0f, false);

            //    _onDrag = true;

            //    _squadUI.OnDragPlayerFieldEvent();

            //    _dragAnimation.SetTrigger("play");
            //}
        }

        /// <summary>
        /// What to do when the event system sends a Drag Event.
        /// </summary>
        public	void	OnDrag(PointerEventData eventData)
        {
            if (_defaultUI.TimePressed > 0.15f && true == _canOnDrag)
            {
                Vector2 lCurrentDrag;
                if (RectTransformUtility.ScreenPointToLocalPointInRectangle(_dragArea, eventData.position, eventData.pressEventCamera, out lCurrentDrag))
                {
                    _draggableGameObject.transform.localPosition = lCurrentDrag;
                }

                _defaultUI.AnimateCanevasIn(_draggableGameObject, 0.3f, false);
                if (null != eventData.pointerEnter && "[Reset]" == eventData.pointerEnter.transform.name) {
                    _squadUI.Reset.transform.GetChild(0).gameObject.SetActive(false);
                    _squadUI.Reset.transform.GetChild(1).gameObject.SetActive(true);
                }
            }

        }

        /// <summary>
        /// Capture the OnEndDrag callback
        /// from the EventSystem and
        /// cancel the listening of drag events.
        /// </summary>
        public	void	OnEndDrag(PointerEventData eventData)
        {
            if (true == _onDrag)
            {
                if (null != eventData.pointerEnter && eventData.pointerEnter.transform.name == "[Reset]")
                {

                    _squadUI.PlayerIsDropped();

                    _defaultUI.AnimateCanevasOut(_draggableGameObject, 0f);

                    ClearPlayer();

                    _squadUI.AutoSave();
                }
                else if(false == _squadUI.InfosStatus)
                {
                    _defaultUI.AnimateCanevasInOut(_player, _empty, 0.1f);
                }

                _squadUI.OnEndDragPlayerField();
            }

            _squadUI.Reset.transform.GetChild(0).gameObject.SetActive(true);
            _squadUI.Reset.transform.GetChild(1).gameObject.SetActive(false);

            if(false == _squadUI.OnSwitch) {
                _draggableGameObject.transform.DOLocalMove(_beginDrag, 0.1f);
                _defaultUI.AnimateCanevasOut(_draggableGameObject, 0.1f);
                StartCoroutine(DelayedOnDragStatus(false, 0.1f));
            } 
            else 
            {
                _defaultUI.AnimateCanevasOut(_draggableGameObject, 0.1f);
                _draggableGameObject.transform.localPosition = _beginDrag;
                _onDrag = false;
                _squadUI.PlayerIsDraggedFromField = null;
            }
           
            // _onDrag = false;

        }

        public IEnumerator DelayedOnDragStatus(bool pStatus, float pDelayTime)
        {
            yield return new WaitForSeconds(pDelayTime);
            _squadUI.PlayerIsDraggedFromField = null;
            _onDrag = pStatus;
        }

        /// <summary>
        /// Called by a BaseInputModule
        /// on a target that can accept a drop.
        /// </summary>
        public	void	OnDrop(PointerEventData eventData)
        {
            //if (null == _squadUI.PlayerIsDraggedFromList || (true == CanDropPlayer() && true == CanBuyPlayer()))
            //{
               

            //    if (null == _squadUI.PlayerIsDraggedFromList)
            //    {
            //        _squadUI.PlayerIsDropped();
            //        PlayersItem lPlayerA = _squadUI.PlayersOnField.Find(item => item.Profile.SquadPosition == _onFocusSquadPosition);
            //        if (lPlayerA != null)
            //        {
            //            _squadUI.SwitchPlayers(lPlayerA, _squadUI.PlayerIsDraggedFromField);
            //        }
            //        else
            //        {
            //            //_squadUI.SwitchPlayers(_onFocusPosition, _onFocusSquadPosition, _squadUI.PlayerIsDraggedFromField);
            //        }
            //    }
            //    else
            //    {
            //        _squadUI.PlayerIsDropped();
            //        if (true == _player.activeSelf)
            //        {
            //            RemovePlayerUpdateDatas(_playerItem);
            //        }

            //        Init(_squadUI.PlayerIsDraggedFromList, true, true);

            //        _squadUI.PlayerIsDraggedFromList.Profile.SquadPosition = _onFocusSquadPosition;

            //        _squadUI.PlayersOnField.Add(_squadUI.PlayerIsDraggedFromList);

            //        _playerItem = _squadUI.PlayersOnField.Find(item => item.Profile.PlayerId == _squadUI.PlayerIsDraggedFromList.Profile.PlayerId);

            //        _onPlayerIsDropped.Invoke();

            //        _dropFx.Play();


            //        if (false == _squadUI.OnSwitch)
            //        {
            //            ChangeStatusInfosAction();
            //        }

            //        UserEventsManager.Instance.RecordEvent(new UserActionEvent("menu_squad_buy_player"));

            //        _squadUI.AutoSave();
            //    }


            //}
            //else if (null != _squadUI.PlayerIsDraggedFromList && this.transform.parent.name != _squadUI.PlayerIsDraggedFromList.Profile.Position)
            //{
            //    if(false == string.IsNullOrEmpty(_squadUI.PlayerIsDraggedFromList.Profile.Name))
            //    {

            //        // _empty.SetActive(true);
            //    }
            //}
            //else if(null != _squadUI.PlayerIsDraggedFromList && false == CanBuyPlayer())
            //{

            //    // _empty.SetActive(true);

            //    _popinUI.NotEnoughBudjet();
            //}
            //_droppable.SetActive(false);
           
        }

        /// <summary>
        /// Checks whether or not player can be dropped
        /// </summary>
        public	bool	CanDropPlayer()
        {
            //return (
            //    true == _draggableGameObject &&
            //    null != _squadUI.PlayerIsDraggedFromList &&
            //    this.transform.parent.name == _squadUI.PlayerIsDraggedFromList.Profile.Position &&
            //    false == _squadUI.PlayersOnField.Exists(item => item.Profile.PlayerId == _squadUI.PlayerIsDraggedFromList.Profile.PlayerId)
            //);
            return false;
        }

        public	bool	CanBuyPlayer()
        {
            bool lValue = false;
            if (true == _player.activeSelf)
            {
                float lCalc = _squadUI.SalaryCap + _playerItem.Profile.Value - _squadUI.PlayerIsDraggedFromList.Profile.Value;
                string lCalcFix = lCalc.ToString("n1");
                lCalc = float.Parse(lCalcFix);
                if (true == lCalc >= 0)
                {
                    lValue = true;
                }
            }
            else
            {
                float lCalc = _squadUI.SalaryCap - _squadUI.PlayerIsDraggedFromList.Profile.Value;
                string lCalcFix = lCalc.ToString("n1");
                lCalc = float.Parse(lCalcFix);
                if (true == lCalc >= 0)
                {
                    lValue = true;
                }
                
            }

            return lValue;
        }

        public	void	ClearInfos()
        {
            _playerName.text	=	"";
            _att.text			=	"";
            _def.text			=	"";
            _value.text			=	"";

            _defaultUI.AnimateCanevasOut(_infos, 0.1f);
        }

        /// <summary>
        /// Clear player from field when it is removed
        /// </summary>
        public	void	ClearPlayer()
        {
            if (null != _playerItem)
            {
                _removeAnimation.SetTrigger("play");

                _defaultUI.AnimateCanevasInOut(_empty, _player, 0.1f);

                ClearInfos();

                RemovePlayerUpdateDatas(_playerItem);

                _playerItem = null;
            }
        }

		#endregion

		#region Private Methods

		/// <summary>
		/// Initializes sequence + sprites
		/// </summary>
		private	void	Start()
        {
            _startLocalPosition = _player.GetComponent<RectTransform>().localPosition;

            //_sequence = DOTween.Sequence();
        }

        /// <summary>
        /// Gets the current position of where the user's touching
        /// </summary>
        /// <returns>The input position</returns>
        private	Vector3	_getInputPosition()
        {
            #if UNITY_EDITOR
                        return Input.mousePosition;
            #else
                            if (0 < Input.touchCount)
                            {
                                return Input.touches[0].position;
                            }
                            else
                            {
                                return Vector3.zero;
                            }
            #endif
        }

        private	void	RemovePlayerUpdateDatas(PlayersItem pPlayer)
        {
            //_squadUI.PlayersOnField.Remove(pPlayer);

            //_squadUI.SellPlayer(pPlayer.Profile.Value, pPlayer.Profile.RareLevel);

            //_squadUI.SubstractAttack(pPlayer.Profile.PvpAttack);

            //_squadUI.SubstractDefense(pPlayer.Profile.PvpDefense);
        }

        #endregion
    }
}

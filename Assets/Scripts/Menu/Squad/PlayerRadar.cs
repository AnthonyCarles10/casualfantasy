﻿using Sportfaction.CasualFantasy.Players;

using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UI.Extensions;

namespace Sportfaction.CasualFantasy.Menu.Squad
{
    /// <summary>
    /// Description: Player Item List
    /// Author: Antoine de Lachèze-Murel
    /// </summary>
    public class PlayerRadar : MonoBehaviour
    {
        #region Fields

        [Header("UI")]
        [SerializeField, Tooltip("Radar BG")]
        private	Image	_radarBG	=	null;
        [SerializeField, Tooltip("LongKick BG")]
        private	Image	_longKickBG	=	null;
        [SerializeField, Tooltip("Dribble BG")]
        private	Image	_dribbleBG	=	null;
        [SerializeField, Tooltip("Pass BG")]
        private	Image	_passBG		=	null;
        [SerializeField, Tooltip("Tackle BG")]
        private	Image	_tackleBG	=	null;
        [SerializeField, Tooltip("Pressing BG")]
        private	Image	_pressingBG	=	null;
        [SerializeField, Tooltip("Counter BG")]
        private	Image	_counterBG	=	null;

		[Header("Action values")]
        [SerializeField, Tooltip("LongKick Value")]
        private	TMPro.TMP_Text	_longKickValue	=	null;
        [SerializeField, Tooltip("Dribble Value")]
        private	TMPro.TMP_Text	_dribbleValue	=	null;
        [SerializeField, Tooltip("Pass Value")]
        private	TMPro.TMP_Text	_passValue		=	null;
        [SerializeField, Tooltip("Tackle Value")]
        private	TMPro.TMP_Text	_tackleValue	=	null;
        [SerializeField, Tooltip("Pressing Value")]
        private	TMPro.TMP_Text	_pressingValue	=	null;
        [SerializeField, Tooltip("Counter Value")]
        private	TMPro.TMP_Text	_counterValue	=	null;

		[Header("Radar UI")]
        [SerializeField, Tooltip("Radar Polygon Fill")]
        private	RadarPolygon	_radarPolygonFill	=	null;
        [SerializeField, Tooltip("Radar Polygon Line")]
        private	RadarPolygon	_radarPolygonLine	=	null;

        private	Color	_color	=	new Color();
		private	Dictionary<string, Sprite>	_sprites	=	new Dictionary<string, Sprite>();

        #endregion

        #region Public Methods

        /// <summary>
        /// Init to set data
        /// <param name="pPlayer">PlayersItem</param>
        /// </summary>
        public	void	Init(PlayersItem pPlayer)
        {
            //_radarPolygonLine.value[0] = pPlayer.Profile.PressingChallenge / 100f;
            //_radarPolygonLine.value[1] = pPlayer.Profile.TackleChallenge / 100f;
            //_radarPolygonLine.value[2] = pPlayer.Profile.LongShotChallenge / 100f;
            //_radarPolygonLine.value[3] = pPlayer.Profile.DribbleChallenge / 100f;
            //_radarPolygonLine.value[4] = pPlayer.Profile.GenericPassChallenge / 100f;
            //_radarPolygonLine.value[5] = pPlayer.Profile.CounterChallenge / 100f;

            //_radarPolygonFill.value[0] = pPlayer.Profile.PressingChallenge / 100f;
            //_radarPolygonFill.value[1] = pPlayer.Profile.TackleChallenge / 100f;
            //_radarPolygonFill.value[2] = pPlayer.Profile.LongShotChallenge / 100f;
            //_radarPolygonFill.value[3] = pPlayer.Profile.DribbleChallenge/ 100f;
            //_radarPolygonFill.value[4] = pPlayer.Profile.GenericPassChallenge / 100f;
            //_radarPolygonFill.value[5] = pPlayer.Profile.CounterChallenge / 100f;


            //_longKickValue.text	=	pPlayer.Profile.LongShotChallenge.ToString();
            //_dribbleValue.text	=	pPlayer.Profile.DribbleChallenge.ToString();
            //_passValue.text		=	pPlayer.Profile.GenericPassChallenge.ToString();
            //_tackleValue.text	=	pPlayer.Profile.TackleChallenge.ToString();
            //_pressingValue.text	=	pPlayer.Profile.PressingChallenge.ToString();
            //_counterValue.text	=	pPlayer.Profile.CounterChallenge.ToString();

            _radarPolygonLine.gameObject.SetActive(false);
            _radarPolygonLine.gameObject.SetActive(true);

            _radarPolygonFill.gameObject.SetActive(false);
            _radarPolygonFill.gameObject.SetActive(true);
        }

        public	void	OnUI()
        {
			Sprite lSprite = null;
			_sprites.TryGetValue("Squad_BG_RadarCircleRed", out lSprite);

            if (null != lSprite)
                _longKickBG.sprite	=	lSprite;

			_sprites.TryGetValue("Squad_BG_RadarCircleGreen", out lSprite);

            if (null != lSprite)
                _dribbleBG.sprite = lSprite;

			_sprites.TryGetValue("Squad_BG_RadarCircleBlue", out lSprite);

            if (null != lSprite)
                _passBG.sprite = lSprite;

			_sprites.TryGetValue("Squad_BG_RadarDefenseRed", out lSprite);

            if (null != lSprite)
                _tackleBG.sprite = lSprite;

			_sprites.TryGetValue("Squad_BG_RadarDefenseGreen", out lSprite);

            if (null != lSprite)
                _pressingBG.sprite = lSprite;

			_sprites.TryGetValue("Squad_BG_RadarDefenseBlue", out lSprite);

            if (null != lSprite)
                _counterBG.sprite = lSprite;

			_sprites.TryGetValue("Squad_BG_RadarOn", out lSprite);

            if (null != lSprite)
                _radarBG.sprite = lSprite;

            ColorUtility.TryParseHtmlString("#2EFEFBBA", out _color); // Blue
            _radarPolygonFill.color = _color;
        }

        public	void	OffUI()
        {
			Sprite lSprite = null;
			_sprites.TryGetValue("Squad_BG_RadarCircleRedOff", out lSprite);

            if (null != lSprite)
                _longKickBG.sprite = lSprite;

			_sprites.TryGetValue("Squad_BG_RadarCircleGreenOff", out lSprite);

            if (null != lSprite)
                _dribbleBG.sprite = lSprite;

			_sprites.TryGetValue("Squad_BG_RadarCircleBlueOff", out lSprite);

            if (null != lSprite)
                _passBG.sprite = lSprite;

			_sprites.TryGetValue("Squad_BG_RadarDefenseRedOff", out lSprite);

            if (null != lSprite)
                _tackleBG.sprite = lSprite;

			_sprites.TryGetValue("Squad_BG_RadarDefenseGreenOff", out lSprite);

            if (null != lSprite)
                _pressingBG.sprite = lSprite;

			_sprites.TryGetValue("Squad_BG_RadarDefenseBlueOff", out lSprite);

            if (null != lSprite)
                _counterBG.sprite = lSprite;

			_sprites.TryGetValue("Squad_BG_RadarOff", out lSprite);

            if (null != lSprite)
                _radarBG.sprite = lSprite;

            ColorUtility.TryParseHtmlString("#FFFFFFBA", out _color); // White
            _radarPolygonFill.color = _color;
        }

		#endregion

		#region Private Methods

		/// <summary>
		/// Initializes sprite dictionary
		/// </summary>
        private	void	Start()
        {
			Sprite[]	lSprites;
			string[]	lPaths	=	new string[]{ "Sprites/Squad/squad-0", "Sprites/Squad/squad-1" };
			
			for (int lj = 0; lj < lPaths.Length; lj++)
			{
				lSprites =	Resources.LoadAll<Sprite>(lPaths[lj]);

				for (int li = 0; li < lSprites.Length; li++)
					_sprites.Add(lSprites[li].name, lSprites[li]);
			}
        }
        
		#endregion
	}
}
﻿using Sportfaction.CasualFantasy.Players;

using DG.Tweening;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;

namespace Sportfaction.CasualFantasy.Menu.Squad
{
	/// <summary>
	/// Description: Players Scrollview
	/// Author: Antoine de Lachèze-Murel
	/// </summary>
	public class PlayersScrollview : MonoBehaviour
	{
		#region Properties

		// Property used to know if is showing the chat frame
		public bool		IsShowing	{ get { return _isShowing; } }
		// Property used to know if is hidding the chat frame
        public  bool    IsHidding   { get { return _isHidding; } }
        
        public	int		Offset		{ set { _offset = value; } }

        public  int     MinPrice    { get { return _minPrice; } set { _minPrice = value;  } }

        public  int     MaxPrice    { get { return _maxPrice; } set { _maxPrice = value;  } }

        public int MinAtt { get { return _minAtt; } set { _minAtt = value; } }

        public int MaxAtt { get { return _maxAtt; } set { _maxAtt = value; } }

        public int MinDef { get { return _minDef; } set { _minDef = value; } }

        public int MaxDef { get { return _maxDef; } set { _maxDef = value; } }

        public int MinG { get { return _minG; } set { _minG = value; } }

        public int MaxG { get { return _maxG; } set { _maxG = value; } }

        public int RareLevel { get { return _rareLevel; } set { _rareLevel = value; } }

        public  string  Position    { get { return _position; } set { _position = value;  } }

        public  string  Team        { get { return _team; } set { _team = value;  } }

        public  string  Order       { get { return _order; } set { _order = value;  } }

        public  string  OrderBy     { get { return _orderBY; } set { _orderBY = value;  } }

        public  string  Keyword     { get { return _keyword; } set { _keyword = value;  } }

        public	bool    InitScrollview	{ get { return _initScrollview; } set { _initScrollview = value;  } }

        #endregion

        #region Fields

        [Header("Links")]
        [SerializeField, Tooltip("SquadUI")]
        private	SquadUI		_squadUI		=	null;
		[SerializeField, Tooltip("PlayersMain")]
        private	PlayersMain	_playersMain	=	null;

  		//[SerializeField, MinValue(0f), Tooltip("Duration to fade in the canvas group")]
		//private	float				_fadeInDuration		=	1f;
		//[SerializeField, MinValue(0f), Tooltip("Duration to fade out the canvas group")]
		//private	float				_fadeOutDuration	=	1f;
		//[SerializeField, Tooltip("Canvas group used to display/hide frame")]
		//private	CanvasGroup			_frameGroup		=	null;
		[SerializeField, Tooltip("Component used to scroll the content")]
		private	ScrollRect			_scrollRect		=	null;

        [SerializeField, Tooltip("Players Api")]
        private List<PlayersItem> _playersApi = new List<PlayersItem>();

        [SerializeField, Tooltip("AudioUI")]
        public AudioUI _audioUI = null;

		#pragma warning disable 0414
		[Header("Informations")]
		[SerializeField, ReadOnly, Tooltip("Is showing frame?")]
		private	bool					_isShowing		=	false;
		[SerializeField, ReadOnly, Tooltip("Is hidding frame?")]
		private	bool					_isHidding		=	false;
		[SerializeField, ReadOnly, Tooltip("Previous scroll position")]
		private	Vector3					_prevScrollPos	=	Vector3.zero;
		[SerializeField, ReadOnly, Tooltip("Current scroll position")]
		private	Vector3					_curScrollPos	=	Vector3.zero;
		[SerializeField, ReadOnly, Tooltip("Transform of the content")]
		private	RectTransform			_content		=	null;
		[SerializeField, ReadOnly, Tooltip("Array contains all players")]
        private	PlayerItemList[]		_players		=	new PlayerItemList[0];

		[Header("First Child")]
		[SerializeField, ReadOnly, Tooltip("First child transform")]
		private	RectTransform	_firstChildTransform	=	null;
		[SerializeField, ReadOnly, Tooltip("First child player")]
        private	PlayerItemList	_firstChildPlayer		=	null;

		[Header("Second Child")]
		[SerializeField, ReadOnly, Tooltip("Last child transform")]
		private	RectTransform	_lastChildTransform		=	null;
		[SerializeField, ReadOnly, Tooltip("Last child player")]
        private	PlayerItemList	_lastChildPlayer		=	null;
		#pragma warning restore 0414

		private	float		_scrollRectTop		=	0f;				// Top of the scroll rect
		private	float		_scrollRectBottom	=	0f;				// Bottom of the scroll rect
		private	float		_firstChildBottom	=	0f;				// Bottom of the first child
		private	float		_lastChildTop		=	0f;             // Top of the last child
		private	int			_topIndex			=	0;				// Index of the top bubble sentence
		private	int			_bottomIndex		=	0;				// Index of the bottom bubble sentence
		//private	Vector3[]	_worldVectors		=	new Vector3[4];	// Array of vectors

        private int         _offset = 0;
        private string      _orderBY = "value";
        private string      _order = "desc";
        private int         _limit = 50;
        private string      _position = "";
        private string      _team = "";
        private string      _keyword = "";
        private int         _minPrice = 0;
        private int         _maxPrice = 4000000;
        private int         _rareLevel = -1;

        private int _minAtt = 0;
        private int _maxAtt = 100;
        private int _minDef = 0;
        private int _maxDef = 100;
        private int _minG = 0;
        private int _maxG = 100;

        private int         _totalCount = 0;
        private bool        _apiLoading = false;
        private bool        _animNormalizedPos = false;
        private bool        _initScrollview = true;
        private float       _scrollDownMax = 2.20f;
        private float       _scrollUpMax = 0f; // Dynmical Value
        private float       _childSizeY = 0f;

        private Vector3 _initLocation = new Vector3();

        private bool _filter = false;
		#endregion

		#region Public Methods

        /// <summary>
        /// Callback Event PlayersMain, OnGetPlayersList Game Event
        /// </summary>
        public void GetPlayersList()
        {
            _totalCount = _playersMain.PlayersList.Metas.TotalCount;
                
		
            if (true == _initScrollview) {
                _playersApi = _playersMain.PlayersList.Items;
                    

                InitScrollRect();

                InitPlayersList();

                InitializeFirstAndLastChild();

                // _curScrollPos.y = _scrollDownMax;

                _scrollRect.normalizedPosition = _curScrollPos;
            } else {
                _playersApi.AddRange(_playersMain.PlayersList.Items); 
            }

            _apiLoading = false;

            _initScrollview = false;

            if(true == _filter) {
                RectTransform lRect = this.GetComponent<RectTransform>();
                lRect.DOLocalMoveX(_initLocation.x, 0.3f);

                lRect = _squadUI.Filters.GetComponent<RectTransform>();
                lRect.DOLocalMoveX(_squadUI.FiltersInitLocation.x, 0.3f);

                _filter = false;
            }
           
        }

        public void UpdatePlayersList()
        {
            foreach(Transform child in _scrollRect.content.transform) {
                child.gameObject.GetComponent<PlayerItemList>().UpdateStatus(child.gameObject.GetComponent<PlayerItemList>().Player);
            }
        }


        /// <summary>
        /// Call api function
        /// </summary>
        public void CallAPI(bool pFilter = false)
        {
            _apiLoading = true;
            Dictionary<string, string> lQueryParams = new Dictionary<string, string>()
                {
                    { "keyword" ,               _keyword },
                    { "offset"  ,               _offset.ToString() },
                    { "orderBY" ,               _orderBY },
                    { "order"   ,               _order },
                    { "position",               _position },
                    { "team"   ,                _team },
                    { "min_price",              _minPrice.ToString() },
                    { "max_price",              _maxPrice.ToString() },
                    { "min_pvp_attack",         _minAtt.ToString() },
                    { "max_pvp_attack",         _maxAtt.ToString() },
                    { "min_pvp_defense",        _minDef.ToString() },
                    { "max_pvp_defense",        _maxDef.ToString() },
                    { "min_pvp_goalkeeping",    _minG.ToString() },
                    { "max_pvp_goalkeeping",    _maxG.ToString() },
                    { "rare_level",             _rareLevel.ToString() },
                    { "limit",                  _limit.ToString() },
                };

            _playersMain.GetPlayersList(lQueryParams);

            if(true == pFilter) {
                RectTransform lRect = this.GetComponent<RectTransform>();
                lRect.DOLocalMoveX(_initLocation.x + lRect.sizeDelta.x, 0.3f);

                lRect = _squadUI.Filters.GetComponent<RectTransform>();
                lRect.DOLocalMoveX(_squadUI.FiltersInitLocation.x + lRect.sizeDelta.x, 0.3f);

                _audioUI.OnRefreshSound();

                _filter = true;

            }
          
        }

        public void InitPlayersScrollview()
        {
            RectTransform lChild = _scrollRect.content.GetChild(0) as RectTransform;

            _childSizeY = lChild.rect.size.y;

            CallAPI();
        }

        //public void ChangeStatusInfosAction()
        //{
        //    foreach (Transform child in _scrollRect.content.transform)
        //    {
        //        child.gameObject.GetComponent<PlayerItemList>().ChangeStatusInfosAction();
        //    }
        //}

        public void Hide()
        {
            RectTransform lRect = this.GetComponent<RectTransform>();
            lRect.DOLocalMoveX(_initLocation.x + lRect.sizeDelta.x, 0.3f);

            lRect = _squadUI.Filters.GetComponent<RectTransform>();
            lRect.DOLocalMoveX(_squadUI.FiltersInitLocation.x + lRect.sizeDelta.x, 0.3f);
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Monobehaviour method used for initialization
        /// </summary>
        private	void	Start()
		{
            RectTransform lRect = this.GetComponent<RectTransform>();
            _initLocation = lRect.localPosition;
            lRect.localPosition = new Vector3(_initLocation.x + lRect.sizeDelta.x, lRect.localPosition.y, lRect.localPosition.z);

             lRect = _squadUI.Filters.GetComponent<RectTransform>();
            _squadUI.FiltersInitLocation = lRect.localPosition;
            lRect.localPosition = new Vector3(_squadUI.FiltersInitLocation.x + lRect.sizeDelta.x, lRect.localPosition.y, lRect.localPosition.z);
        }

		/// <summary>
		/// Initialize scroll rect
		/// </summary>
		private	void	InitScrollRect()
		{
			Assert.IsNotNull(_scrollRect, "[PlayerFrame] InitScrollRect(), _scrollRect is null.");

			_content			=	_scrollRect.content;

			_scrollRect.movementType	=	ScrollRect.MovementType.Unrestricted;
			_scrollRect.horizontal		=	false;
			_scrollRect.vertical		=	true;

			_scrollRect.onValueChanged.AddListener(OnScroll);

			RectTransform	_scrollRectTransform	=	_scrollRect.GetComponent<RectTransform>();

			Assert.IsNotNull(_scrollRectTransform, "[PlayerFrame] InitScrollRect(), _scrollRectTransform is null.");

			_scrollRectTop		=	_scrollRectTransform.TransformPoint(0f, _scrollRectTransform.rect.yMax, 0f).y;
			_scrollRectBottom	=	_scrollRectTransform.TransformPoint(0f, _scrollRectTransform.rect.yMin, 0f).y;

            _curScrollPos.y = _scrollDownMax;
            _scrollRect.normalizedPosition = _curScrollPos;
		}

		/// <summary>
		/// Initialize bubbles choice
		/// </summary>
        private	void	InitPlayersList()
		{
            Assert.IsNotNull(_playersApi, "[PlayerFrame] InitPlayersList(), _playersApi is null.");

            Assert.IsNotNull(_scrollRect, "[PlayerFrame] InitPlayersList(), _scrollRect is null.");

            foreach(Transform child in _scrollRect.content.transform) {
                child.gameObject.SetActive(true);
            }

            _players	=	_scrollRect.GetComponentsInChildren<PlayerItemList>();

            // Debug.Log(_playersApi.Count);
            for (int lIndex = 0; lIndex < _players.Length; ++lIndex)
            {
                Transform lChild = _scrollRect.content.GetChild(lIndex);
                  Vector3 lPos = lChild.gameObject.GetComponent<RectTransform>().localPosition;

                if (_playersApi.Count <= lIndex || _playersApi.Count == 0)
                {
                    lChild.gameObject.SetActive(false);
                }
                else
                {
                    // Debug.Log(lIndex);
                    _players[lIndex].Init(_playersApi[lIndex]);
                    // lChild.gameObject.name = _playersApi[lIndex].PlayerName;
                }

                lChild.gameObject.GetComponent<RectTransform>().localPosition = new Vector3(lPos.x, _childSizeY - _childSizeY * lIndex, lPos.z); 
                
            }

           int	lLength	=	(_playersApi.Count <= _players.Length) ? _playersApi.Count : _players.Length;

            _topIndex = 0;
            _bottomIndex = lLength - 1; 

			
		}


		/// <summary>
		/// Initialize first and last child
		/// </summary>
		private	void	InitializeFirstAndLastChild()
		{
			UpdateFirstChild();

			UpdateLastChild();
		}

		/// <summary>
		/// Update first child
		/// </summary>
		private	void	UpdateFirstChild()
		{
			Assert.IsNotNull(_content, "[PlayerFrame] UpdateFirstChild(), _content is null.");

			Assert.IsFalse(0 == _content.childCount, "[PlayerFrame] UpdateFirstChild(), _content don't have children");

			_firstChildTransform	=	_content.GetChild(0) as RectTransform;

			Assert.IsNotNull(_firstChildTransform, "[PlayerFrame] UpdateFirstChild(), _firstChildTransform is null.");

            _firstChildPlayer		=	_firstChildTransform.GetComponent<PlayerItemList>();

            Assert.IsNotNull(_firstChildPlayer, "[PlayerFrame] UpdateFirstChild(), _firstChildPlayer is null.");

            // Transform lChild = _content.GetChild(0);
            // lChild.gameObject.name = _firstChildPlayer.Name;
		}

		/// <summary>
		/// Update last child
		/// </summary>
		private	void	UpdateLastChild()
		{
			Assert.IsNotNull(_content, "[PlayerFrame] UpdateLastChild(), _content is null.");

			Assert.IsFalse(0 == _content.childCount, "[PlayerFrame] UpdateLastChild(), _content don't have children");

			_lastChildTransform	=	_content.GetChild(_content.childCount - 1) as RectTransform;

			Assert.IsNotNull(_lastChildTransform, "[PlayerFrame] UpdateFirstChild(), _lastChildTransform is null.");

            _lastChildPlayer		=	_lastChildTransform.GetComponent<PlayerItemList>();

            Assert.IsNotNull(_lastChildPlayer, "[PlayerFrame] UpdateFirstChild(), _lastChildPlayer is null.");

            // Transform lChild = _content.GetChild(_content.childCount - 1);
            // lChild.gameObject.name = _lastChildPlayer.Name;
		}

		/// <summary>
		/// Callback invoke when user is scrolling
		/// </summary>
		/// <param name="pPos"></param>
		private	void	OnScroll(Vector2 pPos)
		{
          
			_curScrollPos	=	pPos;

            if (_curScrollPos.y > _prevScrollPos.y)
			{
                OnScrollDown();
			}
            else if (_curScrollPos.y < _prevScrollPos.y)
			{
                OnScrollUp();
			}

            _prevScrollPos = _curScrollPos;


		}

		/// <summary>
		/// Callback invoke when user scroll up
		/// </summary>
		private	void	OnScrollUp()
		{
			Assert.IsNotNull(_firstChildTransform, "[PlayerFrame] OnScrollUp(), _firstChildTransform is null.");

			_firstChildBottom	=	_firstChildTransform.TransformPoint(0f, _firstChildTransform.rect.yMin, 0f).y;


            if (_bottomIndex < _totalCount - 1 && _firstChildBottom > _scrollRectTop)
			{
				Assert.IsNotNull(_lastChildTransform,  "[PlayerFrame] OnScrollUp(), _lastChildTransform is null.");

                //Vector2 lSize = firstChildTransform.size;
                _firstChildTransform.localPosition = new Vector3(_lastChildTransform.localPosition.x, _lastChildTransform.localPosition.y - _childSizeY, _lastChildTransform.localPosition.z);

				//_firstChildTransform.position	=	_lastChildTransform.position;

				_firstChildTransform.ForceUpdateRectTransforms();

				_firstChildTransform.SetAsLastSibling();

				_lastChildTransform	=	_firstChildTransform;
                _lastChildPlayer = _firstChildPlayer;

				IncreaseIndex();

                Assert.IsNotNull(_firstChildPlayer, "[PlayerFrame] OnScrollUp(), _firstChildPlayer is null.");

                _firstChildPlayer.Init(_playersApi[_bottomIndex]);

				UpdateFirstChild();

                //_scrollUpMax = _curScrollPos.y - 0.6f;
                _scrollUpMax = _curScrollPos.y - 1.5f;

				OnScrollUp();
            }
            else if (_bottomIndex >= _totalCount - 1 && _curScrollPos.y < _scrollUpMax && false == _animNormalizedPos)
            {
                Sequence lSequence = DOTween.Sequence();
                _curScrollPos.y = _scrollUpMax;
                lSequence.Insert(0, _scrollRect.DONormalizedPos(_curScrollPos, 1f).OnComplete(EndAnimNormalizedPos));
                _animNormalizedPos = true;
            }

		}

		/// <summary>
		/// Callback invoke when user scroll down
		/// </summary>
		private	void	OnScrollDown()
		{
			Assert.IsNotNull(_lastChildTransform, "[PlayerFrame] OnScrollDown(), _lastChildTransform is null.");

			_lastChildTop	=	_lastChildTransform.TransformPoint(0f, _lastChildTransform.rect.yMax, 0f).y;


            if (_lastChildTop < _scrollRectBottom && _topIndex >  0)
			{
				Assert.IsNotNull(_firstChildTransform, "[PlayerFrame] OnScroll(), _firstChildTransform is null.");

                _lastChildTransform.localPosition = new Vector3(_firstChildTransform.localPosition.x, _firstChildTransform.localPosition.y + _childSizeY, _firstChildTransform.localPosition.z);

				_lastChildTransform.ForceUpdateRectTransforms();

				_lastChildTransform.SetAsFirstSibling();

				_firstChildTransform	=	_lastChildTransform;
				_firstChildPlayer		=	_lastChildPlayer;

				DecreaseIndex();

                Assert.IsNotNull(_lastChildPlayer, "[PlayerFrame] OnScrollDown(), _lastChildPlayer is null.");

                _lastChildPlayer.Init(_playersApi[_topIndex]);

				UpdateLastChild();

				OnScrollDown();
            } else if (_topIndex <=  0 && _curScrollPos.y > _scrollDownMax && false == _animNormalizedPos) {
                Sequence lSequence = DOTween.Sequence();
                _curScrollPos.y = _scrollDownMax;
                lSequence.Insert(0, _scrollRect.DONormalizedPos(_curScrollPos, 1f).OnComplete(EndAnimNormalizedPos));
                _animNormalizedPos = true;
            }
		}

        private void EndAnimNormalizedPos()
        {
            _animNormalizedPos = false;
        }

		/// <summary>
		/// Change rect transform pivot
		/// </summary>
		/// <param name="pRectTransform">Rect transform to modify</param>
		/// <param name="pPivot">New pivot to apply</param>
        private void    ChangePivot(RectTransform pRectTransform, Vector2 pPivot)
		{
			if (null != pRectTransform)
			{
				Vector2	lSize			=	pRectTransform.rect.size;
				Vector2	lDeltaPivot		=	pRectTransform.pivot - pPivot;
                Vector3 lDeltaPosition  =   pRectTransform.rotation * new Vector3(lDeltaPivot.x * lSize.x * pRectTransform.localScale.x, lDeltaPivot.y * lSize.y * pRectTransform.localScale.y);

				// pRectTransform.pivot			=	pPivot;

				pRectTransform.ForceUpdateRectTransforms();

				pRectTransform.localPosition	-=	lDeltaPosition;

				pRectTransform.ForceUpdateRectTransforms();
			}
		}

		/// <summary>
		/// Increase index
		/// </summary>
		private	void	IncreaseIndex()
		{
			++_topIndex;
			++_bottomIndex;

            if (_limit < _totalCount && _bottomIndex >= _playersApi.Count / 2 && false == _apiLoading)
            {
                _offset = _offset + _limit;
                CallAPI();
            }

          
		}

		/// <summary>
		/// Decrease index
		/// </summary>
		private	void	DecreaseIndex()
		{
			--_topIndex;
			--_bottomIndex;
		}

		#endregion
	}
}

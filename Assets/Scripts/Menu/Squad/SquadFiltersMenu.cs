﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Sportfaction.CasualFantasy.Menu.Squad
{
    /// <summary>
    /// Description : ?
    /// Author : Antoine de Lachèze-Murel
    /// </summary>
    public class SquadFiltersMenu: MonoBehaviour, IPointerDownHandler
    {
        #region Fields

        [Header("Links")]
        [SerializeField, Tooltip("SquadFilters")]
        private SquadFilters _squadFilters = null;

        [Header("UI")]
        [SerializeField, Tooltip("PlayersScrollview")]
        private PlayersScrollview _playersScrollview = null;

        [SerializeField, Tooltip("Price")]
        private GameObject _price = null;

        [SerializeField, Tooltip("Att")]
        private GameObject _att = null;

        [SerializeField, Tooltip("_attFilterStatus")]
        private bool _attFilterStatus = false;

        [SerializeField, Tooltip("Def")]
        private GameObject _def = null;

        [SerializeField, Tooltip("_defFilterStatus")]
        private bool _defFilterStatus = false;

        [SerializeField, Tooltip("G")]
        private GameObject _g = null;

        [SerializeField, Tooltip("_gFilterStatus")]
        private bool _gFilterStatus = false;

        [SerializeField, Tooltip("Club")]
        private GameObject _club = null;

        [SerializeField, ReadOnly, Tooltip("priceFilter")]
        private bool _priceFilterStatus = false;

        [SerializeField, ReadOnly, Tooltip("priceFilter")]
        private bool _teamFilterStatus = false;

		[Space(5)]
        [SerializeField, Tooltip("Search")]
        private	Image					_search			=	null;
        [SerializeField, Tooltip("Search Reset")]
        private	Image					_searchReset	=	null;
        [SerializeField, Tooltip("PlayerName")]
        private	TMPro.TMP_InputField	_playerName		=	null;
        [SerializeField, ReadOnly, Tooltip("Previous Search")]
        private	string					_previousSearch	=	"";

		[Space(5)]
        [SerializeField, Tooltip("_gRangeSlider")]
        private	RangeSlider	_gRangeSlider	=	null;
        [SerializeField, Tooltip("_attRangeSlider")]
        private	RangeSlider	_attRangeSlider	=	null;
        [SerializeField, Tooltip("_defRangeSlider")]
        private	RangeSlider	_defRangeSlider	=	null;

		private	Dictionary<string, Sprite>	_sprites	=	new Dictionary<string, Sprite>();
		
        #endregion

        #region Public Methods

        /// <summary>
        /// Is called when the user has pressed 
        /// the mouse button while
        /// over the GUIElement or Collider.
        /// </summary>
        public void OnPointerDown(PointerEventData eventData)
        {
            if(null != eventData.pointerEnter.transform) {
                switch (eventData.pointerEnter.transform.name)
                {
                    case "[Att]":
                        if (false == _attFilterStatus)
                        {
                            _squadFilters.OnAttFilter();
                        }
                        else
                        {
                            DisableAttFilter();
                        }
                        break;
                    case "[Def]":
                        if (false == _defFilterStatus)
                        {
                            _squadFilters.OnDefFilter();
                        }
                        else
                        {
                            DisableDefFilter();
                        }
                        break;
                    case "[G]":
                        if (false == _gFilterStatus)
                        {
                            _squadFilters.OnGFilter();
                        }
                        else
                        {
                            DisableGFilter();
                        }
                        break;
                    case "[RareLevel]":
                        if (false == _priceFilterStatus)
                        {
                            _squadFilters.OnPriceFilter();
                        }
                        else
                        {
                            DisablePriceFilter();
                        }

                        break;
                    case "[Club]":
                        if (false == _teamFilterStatus)
                        {
                            _squadFilters.OnTeamFilter();
                        }
                        else
                        {
                            DisableClubsFilter();
                        }
                        break;
                    case "[Search]":
						//Sprite lSprite;
						//_sprites.TryGetValue("Squad_BG_SearchBarOn", out lSprite);
                        //_search.sprite		=	lSprite;
                        _playerName.ActivateInputField();

                        break;
                    case "[SearchReset]":
						Sprite lSprite;
						_sprites.TryGetValue("Squad_BG_SearchBar", out lSprite);
                        _search.sprite		=	lSprite;
                        _playerName.text	=	"";
                        _playersScrollview.Keyword			=	_playerName.text;
                        _playersScrollview.InitScrollview	=	true;
                        _playersScrollview.CallAPI();
                        _searchReset.raycastTarget	=	false;
                        break;
                }
            }
        }

        public void EnablePriceFilter()
        {
            _price.transform.GetChild(0).gameObject.SetActive(false);
            _price.transform.GetChild(1).gameObject.SetActive(true);


            _priceFilterStatus = true;
        }

        public void EnableClubsFilter()
        {
            _club.transform.GetChild(0).gameObject.SetActive(false);
            _club.transform.GetChild(1).gameObject.SetActive(true);

            _teamFilterStatus = true;
        }

        public void EnableAttFilter()
        {
            _att.transform.GetChild(0).gameObject.SetActive(false);
            _att.transform.GetChild(1).gameObject.SetActive(true);

            _attFilterStatus = true;
        }

        public void EnableDefFilter()
        {
            _def.transform.GetChild(0).gameObject.SetActive(false);
            _def.transform.GetChild(1).gameObject.SetActive(true);

            _defFilterStatus = true;
        }

        public void EnableGFilter()
        {
            _g.transform.GetChild(0).gameObject.SetActive(false);
            _g.transform.GetChild(1).gameObject.SetActive(true);

            _gFilterStatus = true;
        }


        public void DisablePriceFilter()
        {
            _price.transform.GetChild(0).gameObject.SetActive(true);
            _price.transform.GetChild(1).gameObject.SetActive(false);

            _priceFilterStatus = false;
            _playersScrollview.RareLevel = -1;
			_playersScrollview.Offset	=	0;
            _playersScrollview.InitScrollview = true;
            _playersScrollview.CallAPI(true);
        }

        public void DisableClubsFilter()
        {
            _club.transform.GetChild(0).gameObject.SetActive(true);
            _club.transform.GetChild(1).gameObject.SetActive(false);

            _playersScrollview.Team = "";
            _playersScrollview.InitScrollview = true;
            _playersScrollview.CallAPI(true);
            _teamFilterStatus = false;
        }

        public void DisableAttFilter()
        {
            _att.transform.GetChild(0).gameObject.SetActive(true);
            _att.transform.GetChild(1).gameObject.SetActive(false);

            _playersScrollview.MinAtt = 0;
            _playersScrollview.MaxAtt = 100;
            _attRangeSlider.Reset();

            _playersScrollview.InitScrollview = true;
            _playersScrollview.CallAPI(true);
            _attFilterStatus = false;
        }

        public void DisableDefFilter()
        {
            _def.transform.GetChild(0).gameObject.SetActive(true);
            _def.transform.GetChild(1).gameObject.SetActive(false);

            _playersScrollview.MinDef = 0;
            _playersScrollview.MaxDef = 100;
            _defRangeSlider.Reset();

            _playersScrollview.InitScrollview = true;
            _playersScrollview.CallAPI(true);
            _defFilterStatus = false;
        }

        public void DisableGFilter()
        {
            _g.transform.GetChild(0).gameObject.SetActive(true);
            _g.transform.GetChild(1).gameObject.SetActive(false);

            _playersScrollview.MinG = 0;
            _playersScrollview.MaxG = 100;
            _gRangeSlider.Reset();

            _playersScrollview.InitScrollview = true;
            _playersScrollview.CallAPI(true);
            _gFilterStatus = false;
        }

        #endregion

        #region Private Methods

		/// <summary>
		/// Initializes sprite dictionary
		/// </summary>
        private	void	Start()
        {
            //RectTransform lRect = this.GetComponent<RectTransform>();
            //Vector3 lVect = new Vector3(lRect.localPosition.x + lRect.sizeDelta.x, lRect.localPosition.y, lRect.localPosition.z);
            //lRect.localPosition = lVect;


            // _choices.GetComponent<CanvasGroup>().alpha = 0;
            // _choices.GetComponent<RectTransform>().DOLocalMoveY(_choices.localPosition.y + _choices.sizeDelta.y, 0f);
            
			Sprite[]	lSprites;
			string[]	lPaths	=	new string[]{ "Sprites/Squad/squad-0", "Sprites/Squad/squad-1" };
			
			for (int lj = 0; lj < lPaths.Length; lj++)
			{
				lSprites =	Resources.LoadAll<Sprite>(lPaths[lj]);

				for (int li = 0; li < lSprites.Length; li++)
					_sprites.Add(lSprites[li].name, lSprites[li]);
			}
        }

        private	void	Update()
        {
            if (_previousSearch != _playerName.text)
            {
                _previousSearch = _playerName.text;
                _playersScrollview.Keyword = _playerName.text;
                _playersScrollview.InitScrollview = true;
                _playersScrollview.CallAPI();

				Sprite lSprite;

                if("" == _playerName.text)
                {
					_sprites.TryGetValue("Squad_BG_SearchBar", out lSprite);
                    _search.sprite	=	lSprite;
                }
                else
                {
					_sprites.TryGetValue("Squad_BG_SearchBarOn", out lSprite);
                    _search.sprite	=	lSprite;
                    _searchReset.raycastTarget	=	true;
                }
            }
        }
        
        #endregion
    }
}

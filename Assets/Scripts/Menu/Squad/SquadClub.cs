﻿using Sportfaction.CasualFantasy.Championship;
using Sportfaction.CasualFantasy.Players;

using UnityEngine;
using UnityEngine.UI;

namespace Sportfaction.CasualFantasy.Menu.Squad
{
    /// <summary>
    /// Description: Player Item List
    /// Author: Antoine de Lachèze-Murel
    /// </summary>
    public class SquadClub : MonoBehaviour
    {
        #region Fields

        [Header("UI")]
        [SerializeField, Tooltip("Name")]
        private	Text	_name	=	null;
        [SerializeField, Tooltip("T-Shirt")]
        private	Image	_tShirt	=	null;
        
		[Header("Configuration")]
		[SerializeField, Tooltip("Player shirts database")]
		private	PlayersShirtDatabase	_shirtsDatabase	=	null;

        #endregion

        #region Public Methods

        /// <summary>
        /// Init to set data
        /// <param name="_club">OfficialTeamsByCountryItemClub</param> 
        /// </summary>
        public	void	Init(OfficialTeamsByCountryItemClub _club)
        {
            _name.text		=	_club.TeamName;
            _tShirt.sprite	=	_shirtsDatabase.FindShirtByTeamId(_club.TeamId.ToString());
        }

        #endregion
    }
}

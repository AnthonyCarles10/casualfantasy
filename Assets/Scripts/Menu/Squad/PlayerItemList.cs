﻿using Sportfaction.CasualFantasy.Players;
using Sportfaction.CasualFantasy.Services;

using DG.Tweening;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace Sportfaction.CasualFantasy.Menu.Squad
{
    /// <summary>
    /// Description: Player Item List
    /// Author: Antoine de Lachèze-Murel
    /// </summary>
    #pragma warning disable 414, CS0649
    public class PlayerItemList : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
    {
        #region Properties

        public PlayersItem Player { get { return _player; } }

        #endregion

        #region Fields

        [Header("Links")]
        [SerializeField, Tooltip("Squad UI")]
        private	SquadUI		_squadUI		=	null;
        //[SerializeField, Tooltip("Default UI")]
        //private	DefaultUI	_defaultUI		=	null;
        [SerializeField, Tooltip("PlayerRadar")]
        private	PlayerRadar	_playerRadar	=	null;

        [Header("UI")]

        [SerializeField, Tooltip("Right Block")]
        private Image _rightBlock = null;

        [SerializeField, Tooltip("Right G Block")]
        private GameObject _gBlock = null;

        [SerializeField, Tooltip("_gBlockPass")]
        private TMPro.TMP_Text _gBlockPass = null;

        [SerializeField, Tooltip("_gBlockSave")]
        private TMPro.TMP_Text _gBlockSave = null;

        [SerializeField, Tooltip("Right DMA Block")]
        private GameObject _dmaBlock = null;

        [SerializeField, Tooltip("Left Block")]
        private Image _leftBlock = null;

        [SerializeField, Tooltip("Price Block")]
        private Image _priceBlock = null;

        [SerializeField, Tooltip("Draggable Game Object")]
        private GameObject _draggableGameObject = null;

        [SerializeField, Tooltip("ATT Game Object")]
        private GameObject _attGO = null;

        [SerializeField, Tooltip("DEF Game Object")]
        private GameObject _defGO = null;

        [SerializeField, Tooltip("INT Game Object")]
        private GameObject _intGO = null;

        [SerializeField, Tooltip("REC Game Object")]
        private GameObject _recGO = null;

        [SerializeField, Tooltip("G Game Object")]
        private GameObject _gGO = null;

        //[SerializeField, Tooltip("ATT Game Object")]
        //private	Image	_attImage	=	null;
        //[SerializeField, Tooltip("DEF Game Object")]
        //private	Image	_defImage	=	null;
        //[SerializeField, Tooltip("G Game Object")]
        //private	Image	_gImage		=	null;

        [SerializeField, Tooltip("Player Name")]
        private TMPro.TMP_Text _name = null;

        [SerializeField, Tooltip("Price ")]
        private Text _price = null;

        [SerializeField, Tooltip("T-Shirt")]
        private Image _tShirt = null;

        [SerializeField, Tooltip("Plate")]
        private Image _plate = null;

        [SerializeField, Tooltip("Att")]
        private Text _att = null;

        [SerializeField, Tooltip("G")]
        private Text _g = null;

        [SerializeField, Tooltip("Def")]
        private Text _def = null;

        [SerializeField, Tooltip("Rec")]
        private Text _rec = null;

        [SerializeField, Tooltip("Int")]
        private Text _int = null;

        //[SerializeField, Tooltip("Tackle On")]
        //private	GameObject	_rightInfos	=	null;
        //[SerializeField, Tooltip("Actions")]
        //private	GameObject	_actions	=	null;

        [Header("Data")]
        [SerializeField, Tooltip("Player")]
        private PlayersItem _player = null;
		
		[Header("Configuration")]
		[SerializeField, Tooltip("Player shirts database")]
		private	PlayersShirtDatabase	_shirtsDatabase	=	null;

        public	string	Name	=	"";
        private	Color	_color	=	new Color();
		private	Dictionary<string, Sprite>	_playerSprites	=	new Dictionary<string, Sprite>();

        #endregion

        #region Public Methods

        /// <summary>
        /// Is called when the user has pressed 
        /// the mouse button while
        /// over the GUIElement or Collider.
        /// </summary>
        public void OnPointerDown(PointerEventData eventData)
        {
            _leftBlock.transform.DOScale(1.1f, 0.1f);
        }

        /// <summary>
        /// Is called when the user has pressed 
        /// the mouse button while
        /// over the GUIElement or Collider.
        /// </summary>
        public void OnPointerUp(PointerEventData eventData)
        {
            _leftBlock.transform.DOScale(1f, 0.1f);
        }

        /// <summary>
        /// Init to set data
        /// <param name="pPlayer">PlayersItem</param>
        /// </summary>
        public void Init(PlayersItem pPlayer)
        {
   //         _player	=	pPlayer;
   //         _playerRadar.Init(pPlayer);

   //         Name	=	pPlayer.Profile.NameField;

   //         _tShirt.sprite	=	_shirtsDatabase.FindShirtByTeamId(pPlayer.Profile.TeamId);

   //         //int lValue = Mathf.RoundToInt(pPlayer.Profile.Value);
   //         //var nfi = (NumberFormatInfo)CultureInfo.InvariantCulture.NumberFormat.Clone();
   //         //nfi.NumberGroupSeparator = " ";
   //         //string formatted = lValue.ToString("n0", nfi);

   //         _price.text	=	pPlayer.Profile.Value.ToString();

   //         _name.text	=	StringExtensions.FirstCharToUpper(pPlayer.Profile.NameField);

   //         _att.text	=	pPlayer.Profile.PvpAttack.ToString();

   //         _def.text	=	pPlayer.Profile.PvpDefense.ToString();

   //         _g.text		=	pPlayer.Profile.PvpGoalkeeping.ToString();

   //         _int.text	=	pPlayer.Profile.InterceptChallenge.ToString();

   //         _rec.text	=	pPlayer.Profile.ReceiveChallenge.ToString();

   //         _gBlockPass.text	=	pPlayer.Profile.GenericPassChallenge.ToString();
   //         _gBlockSave.text	=	pPlayer.Profile.LongSaveChallenge.ToString();

   //         if ("G" == pPlayer.Profile.Position)
   //         {
   //             _attGO.SetActive(false);
   //             _defGO.SetActive(false);
   //             _gGO.SetActive(true);
   //             _intGO.SetActive(false);
   //             _recGO.SetActive(false);
   //             _dmaBlock.SetActive(false);
   //             _gBlock.SetActive(true);
   //         }
   //         else
   //         {
   //             _attGO.SetActive(true);
   //             _defGO.SetActive(true);
   //             _intGO.SetActive(true);
   //             _recGO.SetActive(true);
   //             _gGO.SetActive(false);
   //             _dmaBlock.SetActive(true);
   //             _gBlock.SetActive(false);
   //         }

			//Sprite lSprite	=	null;
			//_playerSprites.TryGetValue("Squad_BG_PlayerName" + pPlayer.Profile.RareLevel, out lSprite);
			
   //         if(null != lSprite)
   //             _plate.sprite	=	lSprite;

   //         ColorUtility.TryParseHtmlString(pPlayer.Profile.RareLevel > 1 ? "#000000FF" : "#FFFFFFFF", out _color); // Black
   //         _name.color = _color;

   //         if (true == _squadUI.PlayersOnField.Exists(item => item.Profile.PlayerId == pPlayer.Profile.PlayerId))
   //         {
   //             OffUI();
   //             _draggableGameObject.SetActive(false);
   //         }
   //         else
   //         {
   //             OnUI();
   //             _draggableGameObject.SetActive(true);

   //             if (_squadUI.SalaryCap - pPlayer.Profile.Value < 0)
   //             {
   //                 ColorUtility.TryParseHtmlString("#FFFFFFFF", out _color); //Red
   //                 _price.color	=	_color;
			//		_playerSprites.TryGetValue("Squad_BG_PlayerPriceForbidden", out lSprite);
                    
   //                 if (null != lSprite)
   //                     _priceBlock.sprite = lSprite;
   //             }
   //             else
   //             {
   //                 ColorUtility.TryParseHtmlString("#FEFC6EFF", out _color); //Yellow
   //                 _price.color = _color;
   //             }
   //         }
        }

        public	void	UpdateStatus(PlayersItem pPlayer)
        {
            Sprite	lSprite	=	null;
            
            if (true == _squadUI.PlayersOnField.Exists(item => item.Profile.PlayerId == pPlayer.Profile.PlayerId))
            {
                OffUI();
                _draggableGameObject.SetActive(false);
            }
            else
            {
                OnUI();
                _draggableGameObject.SetActive(true);

                if (_squadUI.SalaryCap - pPlayer.Profile.Value < 0)
                {
                   
                    ColorUtility.TryParseHtmlString("#FFFFFFFF", out _color); //Red
                    _price.color	=	_color;
					_playerSprites.TryGetValue("Squad_BG_PlayerPriceForbidden", out lSprite);
                
                    if (null != lSprite)
                        _priceBlock.sprite	=	lSprite;
                }
                else
                {
                    ColorUtility.TryParseHtmlString("#FEFC6EFF", out _color); //Yellow
                    _price.color	=	_color;
                }
            }

        }

        public	void	UpdateUI()
        {
            Sprite lSprite = null;
            if (true == _squadUI.PlayersOnField.Exists(item => item.Profile.PlayerId == _player.Profile.PlayerId))
            {
                OffUI();
                _draggableGameObject.SetActive(false);
            }
            else
            {
                OnUI();
                _draggableGameObject.SetActive(true);

                if (_squadUI.SalaryCap - _player.Profile.Value < 0)
                {
                    ColorUtility.TryParseHtmlString("#FFFFFFFF", out _color); //Red
                    _price.color	=	_color;
					_playerSprites.TryGetValue("Squad_BG_PlayerPriceForbidden", out lSprite);
                    
                    if (null != lSprite)
                        _priceBlock.sprite	=	lSprite;
                }
                else
                {
                    ColorUtility.TryParseHtmlString("#FEFC6EFF", out _color); //Yellow
                    _price.color = _color;
                }
            }
        }

        public	void	ResetUI()
        {
            OnUI();
            _draggableGameObject.SetActive(true);
        }

        public	void	OffUI()
        {
   //         Sprite	lSprite	=	null;

   //         if ("G" == _player.Profile.Position)
   //         {
			//	_playerSprites.TryGetValue("Squad_BG_PlayerBoardGlovesOff", out lSprite);

   //             if (null != lSprite)
   //                 _leftBlock.sprite	=	lSprite;
   //         }
   //         else
   //         {
			//	_playerSprites.TryGetValue("Squad_BG_PlayerBoardOff", out lSprite);

   //             if (null != lSprite)
   //                 _leftBlock.sprite	=	lSprite;
   //         }

			//_playerSprites.TryGetValue("Squad_BG_PlayerBoardScreenOff", out lSprite);

   //         if (null != lSprite)
   //             _rightBlock.sprite	=	lSprite;

			//_playerSprites.TryGetValue("Squad_BG_PlayerPriceOff", out lSprite);

   //         if (null != lSprite)
   //             _priceBlock.sprite	=	lSprite;

   //         _playerRadar.OffUI();
        }

        public	void	OnUI()
        {
   //         Sprite	lSprite	=	null;

   //         if ("G" == _player.Profile.Position)
   //         {
			//	_playerSprites.TryGetValue("Squad_BG_PlayerBoardGlovesOn", out lSprite);

   //             if (null != lSprite)
   //             {
   //                 _leftBlock.sprite = lSprite;
   //             }
   //         }
   //         else
   //         {
			//	_playerSprites.TryGetValue("Squad_BG_PlayerBoardOn", out lSprite);

   //             if (null != lSprite)
   //             {
   //                 _leftBlock.sprite = lSprite;
   //             }
   //         }

			//_playerSprites.TryGetValue("Squad_BG_PlayerBoardScreenOn", out lSprite);

   //         if (null != lSprite)
   //         {
   //             _rightBlock.sprite = lSprite;
   //         }

			//_playerSprites.TryGetValue("Squad_BG_PlayerPriceOn", out lSprite);

   //         if (null != lSprite)
   //         {
   //             _priceBlock.sprite = lSprite;
   //         }

   //         _playerRadar.OnUI();
        }

		#endregion

		#region Private Methods

		/// <summary>
		/// Initializes sprite dictionary
		/// </summary>
		private	void	Start()
		{
			Sprite[]	lSprites;
			string[]	lPaths	=	new string[]{ "Sprites/Player/player-0", "Sprites/Player/player-1", "Sprites/Player/player-2", "Sprites/Squad/squad-0", "Sprites/Squad/squad-1" };
			
			for (int lj = 0; lj < lPaths.Length; lj++)
			{
				lSprites =	Resources.LoadAll<Sprite>(lPaths[lj]);

				for (int li = 0; li < lSprites.Length; li++)
					_playerSprites.Add(lSprites[li].name, lSprites[li]);
			}
		}
		
		#endregion
	}
}

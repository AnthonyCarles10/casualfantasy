﻿using Sportfaction.CasualFantasy.Championship;
using Sportfaction.CasualFantasy.Services.Userstracking;

using DG.Tweening;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Sportfaction.CasualFantasy.Menu.Squad
{
    public class SquadFilters: MonoBehaviour, IPointerDownHandler
    {
    
        #region Fields

        [Header("Links")]

        [SerializeField, Tooltip("PlayersScrollview")]
        private PlayersScrollview _playersScrollview = null;

        [SerializeField, Tooltip("ChampionshipMain")]
        private ChampionshipMain _championshipMain = null;

        [SerializeField, Tooltip("SquadFiltersMenu")]
        private SquadFiltersMenu _squadFiltersMenu = null;

        [SerializeField, Tooltip("DefaultUI")]
        private DefaultUI _defaultUI = null;

        [Header("UI")]

        [SerializeField, Tooltip("Filters")]
        private GameObject _filters = null;

        [SerializeField, Tooltip("Popin")]
        private GameObject _popin = null;

        [SerializeField, Tooltip("Back")]
        private GameObject _back = null;

        //[SerializeField, Tooltip("positionFilter")]
        //private GameObject positionFilter = null;

        [SerializeField, Tooltip("_attFilter")]
        private GameObject _attFilter = null;

        [SerializeField, Tooltip("_attRangeSlider")]
        private RangeSlider _attRangeSlider = null;

        [SerializeField, Tooltip("_defFilter")]
        private GameObject _defFilter = null;

        [SerializeField, Tooltip("_defRangeSlider")]
        private RangeSlider _defRangeSlider = null;

        [SerializeField, Tooltip("_gFilter")]
        private GameObject _gFilter = null;

        [SerializeField, Tooltip("_gRangeSlider")]
        private RangeSlider _gRangeSlider = null;

        [SerializeField, Tooltip("_priceFilter")]
        private GameObject _priceFilter = null;

        [SerializeField, Tooltip("_teamFilterStep1")]
        private CanvasGroup _teamFilterStep1 = null;

        [SerializeField, Tooltip("_teamFilterStep2")]
        private CanvasGroup _teamFilterStep2 = null;

        [SerializeField, Tooltip("ClubColumn1")]
        private GameObject _clubColumn1 = null;

        [SerializeField, Tooltip("ClubColumn2")]
        private GameObject _clubColumn2 = null;

        [SerializeField, Tooltip("ClubColumn3")]
        private GameObject _clubColumn3 = null;

        [SerializeField, Tooltip("ClubPrefab")]
        private GameObject _clubPrefab = null;


        [Header("Data")]

        private bool _filterByClub = false;

        private string _team = "";

        #endregion


        #region Public Methods

        /// <summary>
        /// Is called when the user has pressed 
        /// the mouse button while
        /// over the GUIElement or Collider.
        /// </summary>
        public void OnPointerDown(PointerEventData eventData)
        {
            if(null != eventData.pointerEnter.transform.name)
            {
                if ("[Shadow] (Image)" == eventData.pointerEnter.transform.name)
                {

                    ClosePopin();

                }
                else
                {

                    switch (eventData.pointerEnter.transform.parent.parent.name)
                    {
                        //case "[Position]":
                        //Reset();
                        //if(_posi)
                        //FilterByPosition(eventData.pointerEnter.transform.name);
                        //break;
                        case "[RareLevel]":
                            // Reset();
                            FilterByPrice(eventData.pointerEnter.transform.name);
                            break;
                        case "[Club - Step1]":
                            Reset();
                            FilterByCountry(eventData.pointerEnter.transform.name);
                            break;
                        default:
                            if (eventData.pointerEnter.transform.name.Contains("team-"))
                            {
                                _filterByClub = true;
                                _team = eventData.pointerEnter.transform.name;
                            }
                            break;
                    }
                }
            }

        }

        public void OnPriceFilter()
        {
            Reset();
            _priceFilter.SetActive(true);

            OpenPopin();
        }

        public void OnTeamFilter()
        {
            Reset();
            _teamFilterStep1.gameObject.SetActive(true);

            OpenPopin();
        }

        public void OnAttFilter()
        {
            Reset();
            _attFilter.SetActive(true);

            OpenPopin();
        }

        public void OnDefFilter()
        {
            Reset();
            _defFilter.SetActive(true);

            OpenPopin();
        }

        public void OnGFilter()
        {
            Reset();
            _gFilter.SetActive(true);

            OpenPopin();
        }

        public void OnEndDragRangeSliderCancelEvent()
        {
            ClosePopin();
        }

        public void OnEndDragRangeSliderEvent()
        {
            if (true == _attFilter.activeSelf)
            {
                FilterByAtt();
            }
            else if(true == _defFilter.activeSelf)
            {
                FilterByDef();
            }
            else if (true == _gFilter.activeSelf)
            {
                FilterByG();
            }
        }

        #endregion

        #region Private Methods

        private void OpenPopin()
        {
            _defaultUI.AnimateCanevasOut(_back, 0.1f);
            _filters.SetActive(true);
            Sequence lSequence = DOTween.Sequence();
            lSequence.Append(_filters.GetComponent<CanvasGroup>().DOFade(1f, 0.2f));
            lSequence.Insert(0.1f, _popin.GetComponent<RectTransform>().DOScale(1f, 0.3f));
        }

        private void ClosePopin()
        {
            Sequence lSequence = DOTween.Sequence();
            lSequence.Append(_popin.GetComponent<RectTransform>().DOScale(0, 0.3f));
            lSequence.Insert(0.1f, _filters.GetComponent<CanvasGroup>().DOFade(0f, 0.2f));

            StartCoroutine(_defaultUI.ChangeStatusGameObject(_filters, false, 0.5f));
            _defaultUI.AnimateCanevasIn(_back, 0.1f);
        }

        private void Reset()
        {
            _priceFilter.SetActive(false);
            _defFilter.SetActive(false);
            _attFilter.SetActive(false);
            _gFilter.SetActive(false);
            _teamFilterStep1.gameObject.SetActive(false);
            _teamFilterStep2.gameObject.SetActive(false);

            _teamFilterStep1.alpha = 1;
            _teamFilterStep2.alpha = 0;

            foreach (Transform child in _clubColumn1.transform)
                Destroy(child.gameObject);

            foreach (Transform child in _clubColumn2.transform)
                Destroy(child.gameObject);

            foreach (Transform child in _clubColumn3.transform)
                Destroy(child.gameObject);

           
          

            // ResetFilters();
        }

        //private void ResetFilters()
        //{
        //    _playersScrollview.Position = "";
        //    _playersScrollview.Team = "";
        //    _playersScrollview.MinPrice = 0;
        //    _playersScrollview.MaxPrice = 4000000;
        //}

  //      private void FilterByPosition(string pName)
		//{
        //    //_playersScrollview.OrderBy = "position";
        //    //_playersScrollview.Order = "desc";
        //    switch (pName)
        //    {
        //        case "[G]":
        //            _playersScrollview.Position = "G";
        //            break;
        //        case "[D]":
        //            _playersScrollview.Position = "D";
        //            break;
        //        case "[M]":
        //            _playersScrollview.Position = "M";
        //            break;
        //        case "[A]":
        //            _playersScrollview.Position = "A";
        //            break;
        //    }

        //    _playersScrollview.InitScrollview = true;
        //    _playersScrollview.CallAPI();
        //    ClosePopin();
        //}

       

        private void FilterByPrice(string pName)
        {

            //_playersScrollview.OrderBy = "value";
            //_playersScrollview.Order = "desc";
            switch (pName)
            {
                case "[1]":
                    _playersScrollview.RareLevel = 1;
                    break;
                case "[2]":
                    _playersScrollview.RareLevel = 2;
                    break;
                case "[3]":
                    _playersScrollview.RareLevel = 3;
                    break;
                case "[4]":
                    //_playersScrollview.MaxPrice = 300000;
                    _playersScrollview.RareLevel = 4;
                    break;
            }

            _squadFiltersMenu.EnablePriceFilter();
            _playersScrollview.InitScrollview = true;
            _playersScrollview.CallAPI(true);
            // ClosePopin();

            UserEventsManager.Instance.RecordEvent(new UserActionEvent("menu_squad_filter_price"));

        }

        private void FilterByCountry(string pName)
        {

            _teamFilterStep1.alpha = 1;
            _teamFilterStep2.alpha = 0;

            switch (pName)
            {
                case "[FR]":
                    InitClubs("France");
                    break;
                case "[DE]":
                    InitClubs("Germany");
                    break;
                case "[UK]":
                    InitClubs("England");
                    break;
                case "[ES]":
                    InitClubs("Spain");
                    break;
                case "[IT]":
                    InitClubs("Italy");
                    break;
            }

        }

        private void InitClubs(string pCountry)
        {
           
            OfficialTeamsByCountryItem lResult = _championshipMain.OfficialTeamsByCountry.Items.Find(item => item.Country == pCountry);

            for (int i = 0; i < lResult.Clubs.Count; i++)
            {
                GameObject lPrefab = Instantiate(_clubPrefab);
                lPrefab.GetComponent<SquadClub>().Init(lResult.Clubs[i]);
                lPrefab.name = "team-" + lResult.Clubs[i].TeamId.ToString();

                if (i % 2  == 0 && i % 3 != 0) {
                    lPrefab.transform.SetParent(_clubColumn2.transform, false);
                } else if (i % 3 == 0) {
                    lPrefab.transform.SetParent(_clubColumn3.transform, false);
                } else {
                    lPrefab.transform.SetParent(_clubColumn1.transform, false);
                }
            }
            _teamFilterStep2.gameObject.SetActive(true);
            _teamFilterStep1.DOFade(0, 0.3f);
            _teamFilterStep2.DOFade(1, 0.3f);
            StartCoroutine(_defaultUI.ChangeStatusGameObject(_teamFilterStep1.gameObject, false, 0.3f));
        }

        private void FilterByClub(string pTeam)
        {
            pTeam = pTeam.Substring(5, pTeam.Length - 5);
            _playersScrollview.Team = pTeam;

            _squadFiltersMenu.EnableClubsFilter();
            _playersScrollview.InitScrollview = true;
            _playersScrollview.CallAPI(true);

            // ClosePopin();

            UserEventsManager.Instance.RecordEvent(new UserActionEvent("menu_squad_filter_club"));

        }

        private void FilterByAtt()
        {
            _squadFiltersMenu.EnableAttFilter();
            _playersScrollview.MinAtt = _attRangeSlider.MinValue;
            _playersScrollview.MaxAtt = _attRangeSlider.MaxValue;

            _playersScrollview.InitScrollview = true;
            _playersScrollview.CallAPI(true);

            UserEventsManager.Instance.RecordEvent(new UserActionEvent("menu_squad_filter_attack"));

             // ClosePopin();
        }

        private void FilterByDef()
        {
            _squadFiltersMenu.EnableDefFilter();
            _playersScrollview.MinDef = _defRangeSlider.MinValue;
            _playersScrollview.MaxDef = _defRangeSlider.MaxValue;

            _playersScrollview.InitScrollview = true;
            _playersScrollview.CallAPI(true);

            UserEventsManager.Instance.RecordEvent(new UserActionEvent("menu_squad_filter_defense"));

             // ClosePopin();
        }

        private void FilterByG()
        {
            _squadFiltersMenu.EnableGFilter();
            _playersScrollview.MinG = _gRangeSlider.MinValue;
            _playersScrollview.MaxG = _gRangeSlider.MaxValue;

            _playersScrollview.InitScrollview = true;
            _playersScrollview.CallAPI(true);

            UserEventsManager.Instance.RecordEvent(new UserActionEvent("menu_squad_filter_goalkeeper"));

             // ClosePopin();
        }


        private void Update()
        {
            if (true == _filterByClub && false == (_defaultUI.IsGetMouseButtonDown) && _defaultUI.TimePressed < 0.1f) {
                // Reset();
                FilterByClub(_team);
                _filterByClub = false;
            } 
            else if (true == _filterByClub && false == (_defaultUI.IsGetMouseButtonDown) && _defaultUI.TimePressed > 0.1f)
            {
                _filterByClub = false;
            }
        }



        #endregion
    }
}

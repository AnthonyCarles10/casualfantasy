using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Sportfaction.CasualFantasy.Menu.Squad
{
    /// <summary>
    /// Description: MenuPointerEvent
    /// Author: Antoine de Lachèze-Murel
    /// </summary>
    public class SquadPointerEvent : MonoBehaviour, IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler
    {
    
        #region Fields

        [Header("Links")]

        [SerializeField, Tooltip("SquadUI")]
        private SquadUI _squadUI = null;

        [SerializeField, Tooltip("DefaultUI")]
        private DefaultUI _defaultUI = null;

        [SerializeField, ReadOnly, Tooltip("_currentTransform")]
        private Transform _currentTransform = null;

        #endregion

        #region Public Methods

        /// <summary>
        /// Called by the EventSystem when the pointer
        /// enters the object associated with this EventTrigger.
        /// </summary>
        public void OnPointerEnter(PointerEventData eventData)
        {
            if (null != eventData.pointerEnter)
            {
                switch (eventData.pointerEnter.transform.name)
                {
                    case "[Squad]":
                        _currentTransform = eventData.pointerEnter.transform;
                        break;
                }
            }

        }

        /// <summary>
        /// Called by the EventSystem when the pointer
        /// enters the object associated with this EventTrigger.
        /// </summary>
        public void OnPointerExit(PointerEventData eventData)
        {
           
        }

        /// <summary>
        /// Register button presses using the IPointerClickHandler
        /// </summary>
        public void OnPointerClick(PointerEventData eventData)
        {
            if (null != _currentTransform)
            {
                StartCoroutine(GoTo());
            }
        }

        #endregion

        #region Private Methods

        private IEnumerator GoTo()
        {
            if (null != _currentTransform)
            {
                _defaultUI.EventSystem.SetActive(false);

                yield return new WaitForSeconds(0.1f);

                _defaultUI.EventSystem.SetActive(true);

                switch (_currentTransform.name)
                {
                    case "[Squad]":
                        StartCoroutine(_squadUI.FilterByPosition("NONE"));
                        break;
                }
            }
        }

        #endregion
    }
}
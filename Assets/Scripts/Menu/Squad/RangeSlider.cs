﻿using Sportfaction.CasualFantasy.Events;

using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Sportfaction.CasualFantasy.Menu.Squad
{
    public class RangeSlider: MonoBehaviour, IPointerEnterHandler, IDragHandler, IEndDragHandler
    {
        #region Properties


        public int MinValue { get { return _minCurrentValue; } set { _minCurrentValue = value; } }

        public int MaxValue { get { return _maxCurrentValue; } set { _maxCurrentValue = value; } }

        #endregion

        #region Fields

        [Header("UI")]

        /// <summary>
        /// The area in which we are able to move the dragObject around.
        /// if null: canvas is used
        /// </summary>
        public	RectTransform	dragArea	=	null;

        //[SerializeField, Tooltip("Slider")]
        //private	RectTransform	_slider		=	null;

        [SerializeField, Tooltip("Cursor Left")]
        private	RectTransform	_cursorLeft			=	null;
        [SerializeField, Tooltip("Cursor Left Text")]
        private	Text			_cursorLeftText		=	null;

        [SerializeField, Tooltip("Cursor Right")]
        private	RectTransform	_cursorRight		=	null;
        [SerializeField, Tooltip("Cursor Right Text")]
        private	Text			_cursorRightText	=	null;

        [SerializeField, ReadOnly, Tooltip("Cursor Selected")]
        private string _cursorSelected = "";

        //[SerializeField, ReadOnly, Tooltip("Screen Pos")]
        //private	Vector3	_screenPos	=	Vector3.zero;

        [SerializeField, ReadOnly, Tooltip("Local Position 2D Cursor Left")]
        private Vector2 _localPos2DCursorLeft;

        [SerializeField, ReadOnly, Tooltip("Local Position 3D Cursor Left")]
        private Vector3 _localPos3DCursorLeft;

        [SerializeField, ReadOnly, Tooltip("Local Position 2D Cursor Right")]
        private Vector2 _localPos2DCursorRight;

        [SerializeField, ReadOnly, Tooltip("Local Position 3D Cursor Right")]
        private Vector3 _localPos3DCursorRight;

        [SerializeField, ReadOnly, Tooltip("Min Value")]
        private int _minValue = 0;

        [SerializeField, ReadOnly, Tooltip("Min Current Value")]
        private int _minCurrentValue = 0;

        [SerializeField, ReadOnly, Tooltip("Max Value")]
        private int _maxValue = 100;

        [SerializeField, ReadOnly, Tooltip("Max Current Value")]
        private int _maxCurrentValue = 100;

        [Header("GameEvent")]
        [SerializeField]
        private	GameEvent	_onEndDragRangeSlider		=	null;
        //[SerializeField]
        //private	GameEvent	_onEndDragRangeSliderCancel	=	null;

        //private	bool	_saved	=	false;

		#endregion
		
		#region Public Methods

		/// <summary>
		/// Is called when the user has pressed 
		/// the mouse button while
		/// over the GUIElement or Collider.
		/// </summary>
		//public void OnPointerDown(PointerEventData eventData)
		//{
		//    if (null != eventData.pointerEnter.transform.name)
		//    {
		//        switch (eventData.pointerEnter.transform.name)
		//        {
		//            case "[Validate]":
		//                _onEndDragRangeSlider.Invoke();
		//                _saved = false;
		//                break;
		//            case "[Cancel]":
		//                Reset();
		//                _onEndDragRangeSliderCancel.Invoke();
		//                break;
		//        }
		//    }
		//}

		public void OnBeginDrag(PointerEventData data)
        {
            _localPos3DCursorLeft = _cursorLeft.localPosition;
            RectTransformUtility.ScreenPointToLocalPointInRectangle(dragArea, data.position, data.pressEventCamera, out _localPos2DCursorLeft);

            _localPos3DCursorRight = _cursorRight.localPosition;
            RectTransformUtility.ScreenPointToLocalPointInRectangle(dragArea, data.position, data.pressEventCamera, out _localPos2DCursorRight);

        }
        
        public void OnDrag(PointerEventData eventData)
        {
            if (_cursorSelected == "left")
            {
                Vector2 localPointerPosition;
                if (RectTransformUtility.ScreenPointToLocalPointInRectangle(dragArea, eventData.position, eventData.pressEventCamera, out localPointerPosition))
                {
                    Vector3 offsetToOriginal = localPointerPosition - _localPos2DCursorLeft;
                    Vector3 lNewLocalPosition = _localPos3DCursorLeft + offsetToOriginal;

                    if(lNewLocalPosition.x > -1 * dragArea.sizeDelta.x / 2 && lNewLocalPosition.x < _cursorRight.localPosition.x - _cursorRight.sizeDelta.x) {
                        _cursorLeft.localPosition = new Vector3(lNewLocalPosition.x, _cursorLeft.localPosition.y, _cursorLeft.localPosition.z);
                        UpdateSlider();
                    }
                   
                }
            } else {
                Vector2 localPointerPosition;
                if (RectTransformUtility.ScreenPointToLocalPointInRectangle(dragArea, eventData.position, eventData.pressEventCamera, out localPointerPosition))
                {
                    Vector3 offsetToOriginal = localPointerPosition - _localPos2DCursorRight;
                    Vector3 lNewLocalPosition = _localPos3DCursorRight + offsetToOriginal;

                    if (lNewLocalPosition.x < dragArea.sizeDelta.x / 2 && lNewLocalPosition.x > _cursorLeft.localPosition.x + _cursorLeft.sizeDelta.x)
                    {
                        _cursorRight.localPosition = new Vector3(lNewLocalPosition.x, _cursorRight.localPosition.y, _cursorRight.localPosition.z);
                        UpdateSlider();
                    }
                }
            }
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            _onEndDragRangeSlider.Invoke();
            _cursorSelected = "";

        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            switch (eventData.pointerEnter.transform.name)
            {
                case "[CursorLeft]":
                    _cursorSelected = "left";
                    break;
                case "[CursorRight]":
                    _cursorSelected = "right";
                    break;


            }
        }

        public void Reset()
        {
            _minCurrentValue = _minValue;
            _maxCurrentValue = _maxValue;

            _cursorLeftText.text = _minCurrentValue.ToString();
            _cursorRightText.text = _maxCurrentValue.ToString();

            _cursorRight.localPosition = new Vector3(dragArea.sizeDelta.x / 2, _cursorLeft.localPosition.y, _cursorRight.localPosition.z);
            _cursorLeft.localPosition = new Vector3(-1 * dragArea.sizeDelta.x / 2, _cursorLeft.localPosition.y, _cursorLeft.localPosition.z);

        }

        #endregion

        #region Private Methods

        private void UpdateSlider()
        {
            _minCurrentValue = (int)Mathf.Round(100 * (_cursorLeft.localPosition.x + (dragArea.sizeDelta.x / 2)) / 950);
            _maxCurrentValue = (int)Mathf.Round(100 * (_cursorRight.localPosition.x + (dragArea.sizeDelta.x / 2)) / 950);


            _cursorLeftText.text = _minCurrentValue.ToString();
            _cursorRightText.text = _maxCurrentValue.ToString();
            //if (_cursorLeft.localPosition.x < 0) {
            //    _minCurrentValue = (int)Mathf.Round(100 * (_cursorLeft.localPosition.x + (dragArea.sizeDelta.x / 2)) / 950);
            //} 
            //else if(_cursorLeft.localPosition.x > 0) {
            //    _minCurrentValue = (int)Mathf.Round(100 * (_cursorLeft.localPosition.x + (dragArea.sizeDelta.x / 2)) / 950);
            //}

            //if (_cursorRight.localPosition.x < 0)
            //{
            //    _maxCurrentValue = (int)Mathf.Round(100 * (_cursorRight.localPosition.x + (dragArea.sizeDelta.x / 2)) / 950);

            //}
            //else if (_cursorRight.localPosition.x > 0)
            //{
            //    Debug.Log("Hello");
            //    _maxCurrentValue = (int)Mathf.Round(100 * ((dragArea.sizeDelta.x / 2 - _cursorRight.localPosition.x)) / 950);
            //}


            //  Debug.Log("_minCurrentValue = " + _minCurrentValue + " && _maxCurrentValue = " + _maxCurrentValue);
            //   _slider.localPosition = new Vector3(100f, _slider.localPosition.y, _slider.localPosition.z);
            //   _slider.sizeDelta = new Vector2(dragArea.sizeDelta.x / 2 + _cursorLeft.localPosition.x, _slider.sizeDelta.y);



            // Debug.Log("_cursorLeftX = " + _cursorLeft.localPosition.x + "_cursorRightX = " + _cursorRight.localPosition.x + "_slider.sizeDelta = " + _slider.sizeDelta.x);

            //if (_cursorLeft.localPosition.x < 0 && _cursorRight.localPosition.x > 0)
            //{
            //    _slider.sizeDelta = new Vector2(Mathf.Abs(_cursorLeft.localPosition.x) + Mathf.Abs(_cursorRight.localPosition.x), _slider.sizeDelta.y);
            //}
            //else if (_cursorLeft.localPosition.x < 0 && _cursorRight.localPosition.x < 0)
            //{
            //    _slider.sizeDelta = new Vector2(Mathf.Abs(_cursorLeft.localPosition.x) - Mathf.Abs(_cursorRight.localPosition.x), _slider.sizeDelta.y);
            //}
            //else if (_cursorLeft.localPosition.x > 0 && _cursorRight.localPosition.x > 0)
            //{
            //    _slider.sizeDelta = new Vector2(Mathf.Abs(_cursorRight.localPosition.x) - Mathf.Abs(_cursorLeft.localPosition.x), _slider.sizeDelta.y);
            //}
        }

        #endregion
    }
}

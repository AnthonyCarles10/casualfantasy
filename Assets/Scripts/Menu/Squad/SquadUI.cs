﻿using Sportfaction.CasualFantasy.AppParam;
using Sportfaction.CasualFantasy.Manager.Profile;
using Sportfaction.CasualFantasy.Players;
using Sportfaction.CasualFantasy.Services.Userstracking;
using Sportfaction.CasualFantasy.Squad;

using DG.Tweening;
//using Facebook.Unity;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;
using UnityEngine.UI.Extensions;

namespace Sportfaction.CasualFantasy.Menu.Squad
{
    public class SquadUI: MonoBehaviour
    {
        #region Properties

        public PlayersItem PlayerIsDraggedFromList { get { return _playerIsDraggedFromList; } set { _playerIsDraggedFromList = value; } }

        public PlayersItem PlayerIsDraggedFromField { get { return _playerIsDraggedFromField; } set { _playerIsDraggedFromField = value; } }

        public List<PlayersItem> PlayersOnField { get { return _playersOnField; } }

        public GameObject CanvasGroup { get { return _canvasGroup; } }

        public float SalaryCap { get { return _salaryCap; } set { _salaryCap = value;  }}

        public float SalaryCapMax { get { return _salaryCapMax; }}

        public bool InfosStatus { get { return _infosStatus; }}

        public bool OnDragPlayerList { get { return _onDragPlayerList; }}

        public bool OnDragPlayerField{ get { return _onDragPlayerField; }}

        public bool Edit { get { return _edit; }}

        public bool OnSwitch { get { return _onSwitch; } set { _onSwitch = value; } }

        public GameObject Filters { get { return _filters; }}

        public Vector3 FiltersInitLocation { get { return _filtersInitLocation; } set { _filtersInitLocation = value; } }

        public GameObject Reset { get { return _reset; } set { _reset = value; } }

        public RectTransform Field { get { return _field; } }

        #endregion

        #region Fields

		[Header("App-Params")]
		[SerializeField, Tooltip("Scriptable object contains data about salary")]
		private	AppParamManagerSalaryData	_salaryData	=	null;

		[Header("Data")]
		[SerializeField, Tooltip("Scriptable object contains data about squad players")]
		private	SquadPlayersData	_squadPlayersData	=	null;

        [Header("Links")]

        [SerializeField, Tooltip("Squad Main")]
        public SquadMain _squadMain = null;

        [SerializeField, Tooltip("DefaultUI")]
        private DefaultUI _defaultUI = null;

        [SerializeField, Tooltip("PlayersScrollview")]
        public PlayersScrollview _playersScrollview = null;

        [SerializeField, Tooltip("PopinUI")]
        public PopinUI _popinUI = null;

        [SerializeField, Tooltip("AudioUI")]
        public AudioUI _audioUI = null;

        [Header("UI")]

        [SerializeField, Tooltip("Canvas Group")]
        private GameObject _canvasGroup = null;

        [SerializeField, Tooltip("_title")]
        private GameObject _title = null;

        [SerializeField, Tooltip("G Parent")]
        private GameObject _gParent = null;

        [SerializeField, Tooltip("field")]
        private RectTransform _field = null;

        [SerializeField, ReadOnly, Tooltip("_fieldPosition")]
        private Vector3 _fieldPosition = new Vector3();

        [SerializeField, Tooltip("budget")]
        private RectTransform _budget = null;

        [SerializeField, Tooltip("_explain1")]
        private RectTransform _explain1 = null;

        [SerializeField, Tooltip("_explain2")]
        private RectTransform _explain2 = null;

        [SerializeField, ReadOnly, Tooltip("_budget")]
        private Vector3 _budgetPosition = new Vector3();

        [SerializeField, Tooltip("_infoPlus")]
        private RectTransform _infoPlus = null;

        [SerializeField, Tooltip("_garbage")]
        private RectTransform _garbage = null;

        [SerializeField, Tooltip("Infos Plus Btn")]
        private GameObject _infosPlusBtn = null;

        //[SerializeField, Tooltip("Infos Action")]
        //private	GameObject	_infosAction	=	null;
        //[SerializeField, Tooltip("Infos AttDef")]
        //private	GameObject	_infosAttDef	=	null;

        [SerializeField, Tooltip("D Parent")]
        private GameObject _dParent = null;

        [SerializeField, Tooltip("M Parent")]
        private GameObject _mParent = null;

        [SerializeField, Tooltip("A Parent")]
        private GameObject _aParent = null;

        [SerializeField, Tooltip("FilterGbtn")]
        private GameObject _filterGBtn = null;

        [SerializeField, Tooltip("FilterAttbtn")]
        private GameObject _filterAttBtn = null;

        [SerializeField, Tooltip("FilterDefbtn")]
        private GameObject _filterDefBtn = null;

        [SerializeField, Tooltip("Links Parent")]
        private GameObject _linksParent = null;

        [SerializeField, Tooltip("List Players Parent")]
        private GameObject _listPlayersParent = null;

        [SerializeField, Tooltip("Salary Cap Text")]
        private Text _salaryCapText = null;

        [SerializeField, Tooltip("Salary Cap Max Text")]
        private Text _salaryCapMaxText = null;

        //[SerializeField, Tooltip("Username")]
        //private	TMPro.TMP_Text	_username	=	null;

        //[SerializeField, Tooltip("Total Stars")]
        //private	TMPro.TMP_Text	_totalStars	=	null;
        //[SerializeField, Tooltip("Total Links")]
        //private	TMPro.TMP_Text	_totalLinks	=	null;

        [SerializeField, Tooltip("Radar Polygon Fill")]
        private RadarPolygon _radarPolygonFill = null;

        [SerializeField, Tooltip("Radar Polygon Line")]
        private RadarPolygon _radarPolygonLine = null;

        [SerializeField, Tooltip("Salary Cap Bar Max")]
        private RectTransform _salaryCapBarMax = null;

        [SerializeField, Tooltip("Salary Cap Bar Current")]
        private RectTransform _salaryCapBarCurrent = null;

        [SerializeField, Tooltip("Filters")]
        private GameObject _filters = null;

        [SerializeField, Tooltip("Filters Init Location")]
        private Vector3 _filtersInitLocation = Vector3.zero;

        [SerializeField, Tooltip("Reset")]
        private GameObject _reset = null;

        [SerializeField, ReadOnly,Tooltip("HideScrollview")]
        private bool _hideScrollview = false;

        [Header("Utils")]
        Color _color = new Color();

        [Header("Data")]

        [SerializeField, ReadOnly, Tooltip("PlayerIsDraggedFromList")]
        private PlayersItem _playerIsDraggedFromList = null;

        [SerializeField, ReadOnly, Tooltip("PlayerIsDraggedFromField")]
        private PlayersItem _playerIsDraggedFromField = null;

        [SerializeField, Tooltip("OnDragPlayerList")]
        private bool _onDragPlayerList = false;

        [SerializeField, Tooltip("OnDragPlayerField")]
        private bool _onDragPlayerField = false;

		#pragma warning disable 0414
        [SerializeField, ReadOnly, Tooltip("Players Api")]
        private List<PlayersItem> _playersOnField = new List<PlayersItem>();

        [SerializeField, ReadOnly, Tooltip("Squad Main")]
        public LineUp _lineUp = new LineUp();

        [SerializeField, ReadOnly, Tooltip("Salaray Cap")]
        private float _salaryCap = 0;

        [SerializeField, ReadOnly, Tooltip("Salaray Cap Max")]
        private float _salaryCapMax = 0;

        [SerializeField, ReadOnly, Tooltip("Attack")]
        private float _attack = 0;

        [SerializeField, ReadOnly, Tooltip("Defense")]
        private float _defense = 0;

        [SerializeField, ReadOnly, Tooltip("Defense")]
        private int _attackDefense = 0;

        [SerializeField, ReadOnly, Tooltip("Stars")]
        private int _stars = 0;

        [SerializeField, ReadOnly, Tooltip("Links")]
        private int _links = 0;
		#pragma warning restore 0414

        [SerializeField, Tooltip("Infos Status")]
        private bool _infosStatus = false;

        [SerializeField, Tooltip("Edit")]
        private bool _edit = false;

        [SerializeField, Tooltip("OnSwitch")]
        private bool _onSwitch = false;

        [SerializeField, Tooltip("Positions")]
        private GameObject _positions = null;

        #endregion

        #region Public Methods
        
        /// <summary>
        /// Callback Event PlayersMain, OnGetPlayersList Game Event
        /// </summary>
        public void OnGetPlayersListEvent()
        {
            _playersScrollview.GetPlayersList();
        }

        /// <summary>
        /// Update Link between players squad position
        /// </summary>
        public void LinkPlayers()
        {
            _links = 0;
            foreach (Transform child in _linksParent.transform)
            {
                Char lDelimiter = '-';
                String[] lSquadPositions = child.gameObject.name.Split(lDelimiter);

                PlayersItem lPlayerOne =_playersOnField.Find(item => item.Profile.SquadPosition == lSquadPositions[0]);
                PlayersItem lPlayerTwo = _playersOnField.Find(item => item.Profile.SquadPosition == lSquadPositions[1]);

                if (null != lPlayerOne && null != lPlayerTwo)
                {
                    child.gameObject.SetActive(true);
                    int lLinkValue = 0;

                    if (lPlayerOne.Profile.TeamId == lPlayerTwo.Profile.TeamId)
                    {
                        lLinkValue++;
                        _links++;
                    }

                    //if (lPlayerOne.Profile.ClubChampionshipId == lPlayerTwo.Profile.ClubChampionshipId)
                    //{
                    //    lLinkValue++;
                    //    _links++;
                    //}

                    //if (lPlayerOne.Profile.Country == lPlayerTwo.Profile.Country)
                    //{
                    //    lLinkValue++;
                    //    _links++;
                    //}

                    switch (lLinkValue)
                    {
                        case 0:
                            ColorUtility.TryParseHtmlString("#FF0000FF", out _color); //Red
                            child.gameObject.GetComponent<Image>().color = _color;
                            break;
                        case 1:
                            ColorUtility.TryParseHtmlString("#FF8400FF", out _color); //Orange
                            child.gameObject.GetComponent<Image>().color = _color;
                            break;
                        case 2:
                            ColorUtility.TryParseHtmlString("#FFE300FF", out _color); //Yellow
                            child.gameObject.GetComponent<Image>().color = _color;
                            break;
                        case 3:
                            ColorUtility.TryParseHtmlString("#1EFF00FF", out _color); //Green
                            child.gameObject.GetComponent<Image>().color = _color;
                            break;
                    }
                } else {
                    child.gameObject.SetActive(false);
                }
                // _totalLinks.text = _links.ToString();
            }
        }

        /// <summary>
        /// Update Radar
        /// </summary>
        public void UpdateRadar()
        {
            float lAttack = 0;
            float lTechnical = 0;
            float lGoalkeeper = 0;
            float lDefense = 0;
            float lFitnessMental = 0;

            for (int i = 0; i < _playersOnField.Count; i++)
            {
                //Debug.Log(_playersOnField[i].Profile.Attack);
                //Debug.Log(_playersOnField[i].Profile.Technical);
                //Debug.Log(_playersOnField[i].Profile.Goalkeeper);
                //Debug.Log(_playersOnField[i].Profile.Defense);
                //Debug.Log(_playersOnField[i].Profile.FitnessMental);
                //lAttack += _playersOnField[i].Profile.Attack;
                //lTechnical += _playersOnField[i].Profile.Technical;
                //lGoalkeeper += _playersOnField[i].Profile.Goalkeeper;
                //lDefense += _playersOnField[i].Profile.Defense;
                //lFitnessMental += _playersOnField[i].Profile.FitnessMental;
            }

            if(_playersOnField.Count > 0) {
                lAttack = lAttack / _playersOnField.Count;
                lTechnical = lTechnical / _playersOnField.Count;
                lGoalkeeper = lGoalkeeper / _playersOnField.Count;
                lDefense = lDefense / _playersOnField.Count;
                lFitnessMental = lFitnessMental / _playersOnField.Count;


                _radarPolygonLine.value[0] = lAttack / 100f;
                _radarPolygonLine.value[1] = lTechnical / 100f;
                _radarPolygonLine.value[2] = lGoalkeeper / 100f;
                _radarPolygonLine.value[3] = lDefense / 100f;
                _radarPolygonLine.value[4] = lFitnessMental / 100f;

                _radarPolygonFill.value[0] = lAttack / 100f;
                _radarPolygonFill.value[1] = lTechnical / 100f;
                _radarPolygonFill.value[2] = lGoalkeeper / 100f;
                _radarPolygonFill.value[3] = lDefense / 100f;
                _radarPolygonFill.value[4] = lFitnessMental / 100f;

                _radarPolygonLine.gameObject.SetActive(false);
                _radarPolygonLine.gameObject.SetActive(true);

            } else {
                _radarPolygonLine.value[0] = 0;
                _radarPolygonLine.value[1] = 0;
                _radarPolygonLine.value[2] = 0;
                _radarPolygonLine.value[3] = 0;
                _radarPolygonLine.value[4] = 0;

                _radarPolygonFill.value[0] = 0;
                _radarPolygonFill.value[1] = 0;
                _radarPolygonFill.value[2] = 0;
                _radarPolygonFill.value[3] = 0;
                _radarPolygonFill.value[4] = 0;

                _radarPolygonLine.gameObject.SetActive(false);
                _radarPolygonLine.gameObject.SetActive(true);
            }
        }

        /// <summary>
        /// Buys player dropped on field & updates UI infos
        /// </summary>
        /// <param name="pPrice">Player's price</param>
        /// <param name="pStars">Player's star level ?</param>
        public void BuyPlayer(float pPrice, int pStars)
		{
            _salaryCap -= pPrice;
            string _salaryCapFix = _salaryCap.ToString("n1");
            _salaryCap = float.Parse(_salaryCapFix);

            float lSalaryRest = _salaryCapMax - _salaryCap;
            string lSalaryRestFix = lSalaryRest.ToString("n1");
            lSalaryRest = float.Parse(lSalaryRestFix);

            //Debug.Log("BuyPlayer");
            //Debug.Log("pPrice = " + pPrice);
            //Debug.Log("_salaryCapMax = " + _salaryCapMax);
            //Debug.Log("_salaryCap = " + _salaryCap);
            //Debug.Log("lSalaryRest = " + lSalaryRest);
            //int lValue = Mathf.RoundToInt(_salaryCap);
            //var nfi = (NumberFormatInfo)CultureInfo.InvariantCulture.NumberFormat.Clone();
            //nfi.NumberGroupSeparator = " ";
            //string formatted = lValue.ToString("n0", nfi);
            _salaryCapText.text = _salaryCap.ToString("n1");

            _stars += pStars;
            // _totalStars.text = _stars.ToString();

            _salaryCapBarCurrent.DOSizeDelta(new Vector2((_salaryCapBarMax.sizeDelta.x * (_salaryCapMax - lSalaryRest)) / _salaryCapMax, _salaryCapBarCurrent.sizeDelta.y), 0.5f);

            _playersScrollview.UpdatePlayersList();
        }

        /// <summary>Init
        /// Sells a player when removed from field & updates general infos
        /// <param name="pPrice">int</param>
        /// <param name="pStars">int</param>
        /// </summary>
        public void SellPlayer(float pPrice, int pStars)
        {
         
            _salaryCap += pPrice;
            string _salaryCapFix = _salaryCap.ToString("n1");
            _salaryCap = float.Parse(_salaryCapFix);

            float lSalaryRest = _salaryCapMax - _salaryCap;
            string lSalaryRestFix = lSalaryRest.ToString("n1");
            lSalaryRest = float.Parse(lSalaryRestFix);

            //Debug.Log("SellPlayer");
            //Debug.Log("pPrice = " + pPrice);
            //Debug.Log("_salaryCapMax = " + _salaryCapMax);
            //Debug.Log("_salaryCap = " + _salaryCap);
            //Debug.Log("lSalaryRest = " + lSalaryRest);
            //int lValue = Mathf.RoundToInt(_salaryCap);
            //var nfi = (NumberFormatInfo)CultureInfo.InvariantCulture.NumberFormat.Clone();
            //nfi.NumberGroupSeparator = " ";
            //string formatted = lValue.ToString("n0", nfi);
            _salaryCapText.text = _salaryCap.ToString("n1");

            _stars -= pStars;
            // _totalStars.text = _stars.ToString();

            _playersScrollview.UpdatePlayersList();

            _salaryCapBarCurrent.DOSizeDelta(new Vector2((_salaryCapBarMax.sizeDelta.x * (_salaryCapMax - lSalaryRest)) / _salaryCapMax, _salaryCapBarCurrent.sizeDelta.y), 0.5f);

        }

        /// <summary>
        /// When a player is set on field, update attack infos
        /// <param name="pAttack">Player attack value</param>
        /// </summary>
        public void AddAttack(float pAttack)
        {
            _attack += pAttack;

            _attackDefense = (int)Math.Round((_attack + _defense) / 2);
        }

        /// <summary>
        /// When a player is set on field, update defense infos
        /// <param name="pDefense">Player defense value</param>
        /// </summary>
        public void AddDefense(float pDefense)
        {
            _defense += pDefense;

            _attackDefense = (int)Math.Round((_attack + _defense) / 2);
        }

        /// <summary>
        /// When a player is removed from field, update attack infos
        /// <param name="pAttack">Player attack value</param>
        /// </summary>
        public void SubstractAttack(float pAttack)
        {
            _attack -= pAttack;

            _attackDefense = (int)Math.Round((_attack + _defense) / 2);
        }

        /// <summary>
        /// When a player is removed from field, update defense infos
        /// <param name="pDefense">Player defense value</param>
        /// </summary>
        public void SubstractDefense(float pDefense)
        {
            _defense -= pDefense;

            _attackDefense = (int) Math.Round((_attack + _defense) / 2);
        }

        /// <summary>
        /// When a user triggers reset button,
        /// remove all players from field
        /// </summary>
        public void OnResetField()
        {
            if(false == _onDragPlayerField) 
            {
                _audioUI.OnClickSound();

                _popinUI.ConfirmResetSquad();
               
            }

        }

        public void ConfirmReset()
        {
            _playersOnField = new List<PlayersItem>();

            _defense = 0;
            _attack = 0;
            _attackDefense = 0;
            _salaryCap = _salaryCapMax;

            //int lValue = Mathf.RoundToInt(_salaryCap);
            //var nfi = (NumberFormatInfo)CultureInfo.InvariantCulture.NumberFormat.Clone();
            //nfi.NumberGroupSeparator = " ";
            //string formatted = lValue.ToString("n0", nfi);

            _salaryCapText.text = _salaryCap.ToString();

            foreach (Transform child in _gParent.transform)
            {
                _defaultUI.AnimateCanevasInOut(child.GetComponent<PlayerField>().Empty, child.GetComponent<PlayerField>().Player, 0.3f);
                child.GetComponent<PlayerField>().ClearInfos();
            }

            foreach (Transform child in _dParent.transform)
            {
                _defaultUI.AnimateCanevasInOut(child.GetComponent<PlayerField>().Empty, child.GetComponent<PlayerField>().Player, 0.3f);
                child.GetComponent<PlayerField>().ClearInfos();
            }

            foreach (Transform child in _mParent.transform)
            {
                _defaultUI.AnimateCanevasInOut(child.GetComponent<PlayerField>().Empty, child.GetComponent<PlayerField>().Player, 0.3f);
                child.GetComponent<PlayerField>().ClearInfos();
            }

            foreach (Transform child in _aParent.transform)
            {
                _defaultUI.AnimateCanevasInOut(child.GetComponent<PlayerField>().Empty, child.GetComponent<PlayerField>().Player, 0.3f);
                child.GetComponent<PlayerField>().ClearInfos();
            }

            // LinkPlayers();
            // UpdateRadar();

            foreach (Transform child in _listPlayersParent.transform)
            {
                child.GetComponent<PlayerItemList>().ResetUI();

            }

            _salaryCapBarCurrent.DOSizeDelta(new Vector2(_salaryCapBarMax.sizeDelta.x, _salaryCapBarCurrent.sizeDelta.y), 0.5f);
            _playersScrollview.UpdatePlayersList();
        }

        /// <summary>
        /// When a user triggers play button,
        /// call api to save the line up
        /// </summary>
        public bool OnPlayGame()
        {
            //_lineUp = new LineUp();

            //for (int i = 0; i < _playersOnField.Count; i++)
            //{
            //    LineUpPlayer _lineUpPlayer = new LineUpPlayer();

            //    if(null != _playersOnField[i].Profile.PlayerId && null != _playersOnField[i].Profile.SquadPosition) {
            //        _lineUpPlayer.id = _playersOnField[i].Profile.PlayerId;
            //        _lineUpPlayer.position = _playersOnField[i].Profile.SquadPosition; 
            //        _lineUp.Players.Add(_lineUpPlayer);
            //    }
            //}

            //Debug.Log("_lineUp.Players.Count = " + _lineUp.Players.Count);
            //if (11 == _lineUp.Players.Count) {
            //    return true;
            //} else {
            //    _popinUI.TeamEmpty();
            //    return false;
            //}
            return false;
        }

        public void AutoSave()
        {
            //List<string> positions = new List<string>();

            //_lineUp = new LineUp
            //{
            //    TeamName = "default",
            //    Slot = 0
            //};

            //for (int i = 0; i < _playersOnField.Count; i++)
            //{
            //    LineUpPlayer _lineUpPlayer = new LineUpPlayer();

            //    if (null != _playersOnField[i].Profile.PlayerId && null != _playersOnField[i].Profile.SquadPosition && false == positions.Contains(_playersOnField[i].Profile.SquadPosition))
            //    {
            //        _lineUpPlayer.id = _playersOnField[i].Profile.PlayerId;
            //        _lineUpPlayer.position = _playersOnField[i].Profile.SquadPosition;
            //        _lineUp.Players.Add(_lineUpPlayer);
            //        positions.Add(_playersOnField[i].Profile.SquadPosition);
            //    }
            //}

            //_squadMain.CreateOrUpdateLineUp(_lineUp);

        }

        public void OnOnClickInfoPlusSquad()
        {
            _infosStatus = !_infosStatus;

            if(false == _infosStatus) {
                _audioUI.OnBackSound();
            } else {
                _audioUI.OnClickSound();

                UserEventsManager.Instance.RecordEvent(new UserActionEvent("menu_squad_infos"));
            }

            foreach (Transform child in _gParent.transform)
            {
                child.GetComponent<PlayerField>().ChangeStatusInfosAction();
            }

            foreach (Transform child in _dParent.transform)
            {
                child.GetComponent<PlayerField>().ChangeStatusInfosAction();
            }

            foreach (Transform child in _mParent.transform)
            {
                child.GetComponent<PlayerField>().ChangeStatusInfosAction();
            }

            foreach (Transform child in _aParent.transform)
            {
                child.GetComponent<PlayerField>().ChangeStatusInfosAction();
            }

            // _playersScrollview.ChangeStatusInfosAction();

            if (false == _infosStatus)
            {
                _infosPlusBtn.transform.GetChild(0).gameObject.SetActive(true);
                _infosPlusBtn.transform.GetChild(1).gameObject.SetActive(false);

                //_infosAction.SetActive(false);
                //_infosAttDef.SetActive(true);
            }
            else
            {
                _infosPlusBtn.transform.GetChild(0).gameObject.SetActive(false);
                _infosPlusBtn.transform.GetChild(1).gameObject.SetActive(true);

                //_infosAction.SetActive(true);
                //_infosAttDef.SetActive(false);
            }

        }

        public void OnDragPlayerFieldEvent()
        {
            if (false == _onDragPlayerField)
            {
                _audioUI.OnDragSound();
            }
            _onDragPlayerField = true;
            _reset.GetComponent<RectTransform>().DOScale(1f, 0.3f);
            // _reset.transform.GetChild(0).gameObject.SetActive(false);
            // _reset.transform.GetChild(1).gameObject.SetActive(true);
        }

        public void OnEndDragPlayerField()
        {
            _onDragPlayerField = false;
            _reset.GetComponent<RectTransform>().DOScale(0.7f, 0.3f);
            // _reset.transform.GetChild(0).gameObject.SetActive(true);
            // _reset.transform.GetChild(1).gameObject.SetActive(false);
        }

        public void PlayerListIsDragged(bool pDragStatus)
        {
            if (true == pDragStatus && pDragStatus != _onDragPlayerList)
            {
                _audioUI.OnDragSound();
            }

            _onDragPlayerList = pDragStatus;

        }

        public void PlayerIsDropped()
        {
            _audioUI.OnDropSound();
        }

        public void SwitchPlayers(PlayersItem _playerA, PlayersItem _playerB)
        {
            //if (_playerA.Profile.Position == _playerB.Profile.Position)
            //{
            //    string lSquadPositionPlayerA = _playerA.Profile.SquadPosition;
            //    string lSquadPositionPlayerB = _playerB.Profile.SquadPosition;

            //    PlayerField lPlayerAField = null;
            //    PlayerField lPlayerBField = null;


            //    switch (_playerA.Profile.Position)
            //    {
            //        case "D":
            //            foreach (Transform child in _dParent.transform)
            //            {
            //                if (child.GetComponent<PlayerField>().PlayersItem != null)
            //                {
            //                    if (child.GetComponent<PlayerField>().PlayersItem.Profile.SquadPosition == lSquadPositionPlayerA)
            //                    {
            //                        lPlayerAField = child.GetComponent<PlayerField>();
            //                    }
            //                    else if (child.GetComponent<PlayerField>().PlayersItem.Profile.SquadPosition == lSquadPositionPlayerB)
            //                    {
            //                        lPlayerBField = child.GetComponent<PlayerField>();
            //                    }
            //                }
            //            }
            //            break;
            //        case "M":
            //            foreach (Transform child in _mParent.transform)
            //            {
            //                if (child.GetComponent<PlayerField>().PlayersItem != null)
            //                {
            //                    if (child.GetComponent<PlayerField>().PlayersItem.Profile.SquadPosition == lSquadPositionPlayerA)
            //                    {
            //                        lPlayerAField = child.GetComponent<PlayerField>();
            //                    }
            //                    else if (child.GetComponent<PlayerField>().PlayersItem.Profile.SquadPosition == lSquadPositionPlayerB)
            //                    {
            //                        lPlayerBField = child.GetComponent<PlayerField>();
            //                    }
            //                }
            //            }
            //            break;
            //        case "A":
            //            foreach (Transform child in _aParent.transform)
            //            {
            //                if (child.GetComponent<PlayerField>().PlayersItem != null)
            //                {
            //                    if (child.GetComponent<PlayerField>().PlayersItem.Profile.SquadPosition == lSquadPositionPlayerA)
            //                    {
            //                        lPlayerAField = child.GetComponent<PlayerField>();
            //                    }
            //                    else if (child.GetComponent<PlayerField>().PlayersItem.Profile.SquadPosition == lSquadPositionPlayerB)
            //                    {
            //                        lPlayerBField = child.GetComponent<PlayerField>();
            //                    }
            //                }
            //            }
            //            break;
            //    }

            //    if (lPlayerAField != null && lPlayerBField != null)
            //    {

            //        _playerA.Profile.SquadPosition = lSquadPositionPlayerB;
            //        _playerB.Profile.SquadPosition = lSquadPositionPlayerA;


            //        Vector3 lLocalPositionlPlayerAField = lPlayerAField.gameObject.GetComponent<RectTransform>().localPosition;
            //        Vector3 lLocalPositionlPlayerBField = lPlayerBField.gameObject.GetComponent<RectTransform>().localPosition;


            //        lPlayerAField.gameObject.GetComponent<RectTransform>().DOLocalMoveX(lLocalPositionlPlayerBField.x, 0.3f);
            //        lPlayerBField.gameObject.GetComponent<RectTransform>().localPosition = lLocalPositionlPlayerAField;


            //        _defaultUI.AnimateCanevasIn(lPlayerBField.Player, 0.3f);
            //        StartCoroutine(SwitchPlayersCoroutine(lPlayerAField, _playerB, lPlayerBField, _playerA, lLocalPositionlPlayerAField, lLocalPositionlPlayerBField));
            //    }
            //}
        }
        
        public void SwitchPlayers(string pPosition, string pSquadPosition, PlayersItem _playerB)
        {
            //Debug.Log(pPosition);
            //Debug.Log(pSquadPosition);
            //Debug.Log(_playerB.Profile.Position);
            //Debug.Log(_playerB.Profile.SquadPosition);

            //if (pPosition == _playerB.Profile.Position)
            //{
            //    string lSquadPositionPlayerA = pSquadPosition;
            //    string lSquadPositionPlayerB = _playerB.Profile.SquadPosition;

            //    PlayerField lPlayerAField = null;
            //    PlayerField lPlayerBField = null;


            //    switch (pPosition)
            //    {
            //        case "D":
            //            foreach (Transform child in _dParent.transform)
            //            {
            //                if (child.GetComponent<PlayerField>().SquadPosition == lSquadPositionPlayerA)
            //                {
            //                    lPlayerAField = child.GetComponent<PlayerField>();
            //                }
            //                else if (child.GetComponent<PlayerField>().PlayersItem != null && child.GetComponent<PlayerField>().PlayersItem.Profile.SquadPosition == lSquadPositionPlayerB)
            //                {
            //                    lPlayerBField = child.GetComponent<PlayerField>();
            //                }
            //            }
            //            break;
            //        case "M":
            //            foreach (Transform child in _mParent.transform)
            //            {
            //                if (child.GetComponent<PlayerField>().SquadPosition == lSquadPositionPlayerA)
            //                {
            //                    lPlayerAField = child.GetComponent<PlayerField>();
            //                }
            //                else if (child.GetComponent<PlayerField>().PlayersItem != null && child.GetComponent<PlayerField>().PlayersItem.Profile.SquadPosition == lSquadPositionPlayerB)
            //                {
            //                    lPlayerBField = child.GetComponent<PlayerField>();
            //                }
            //            }
            //            break;
            //        case "A":
            //            foreach (Transform child in _aParent.transform)
            //            {
            //                if (child.GetComponent<PlayerField>().SquadPosition == lSquadPositionPlayerA)
            //                {
            //                    lPlayerAField = child.GetComponent<PlayerField>();
            //                }
            //                else if (child.GetComponent<PlayerField>().PlayersItem != null && child.GetComponent<PlayerField>().PlayersItem.Profile.SquadPosition == lSquadPositionPlayerB)
            //                {
            //                    lPlayerBField = child.GetComponent<PlayerField>();
            //                }
            //            }
            //            break;
            //    }

            //    Debug.Log(lPlayerAField);
            //    Debug.Log(lPlayerBField);

            
            //    if (lPlayerAField != null && lPlayerBField != null)
            //    {

            //        _playerB.Profile.SquadPosition = lSquadPositionPlayerA;

            //        Vector3 lLocalPositionlPlayerAField = lPlayerAField.gameObject.GetComponent<RectTransform>().localPosition;
            //        Vector3 lLocalPositionlPlayerBField = lPlayerBField.gameObject.GetComponent<RectTransform>().localPosition;


            //        lPlayerAField.gameObject.GetComponent<RectTransform>().DOLocalMoveX(lLocalPositionlPlayerBField.x, 0.3f);
            //        lPlayerBField.gameObject.GetComponent<RectTransform>().localPosition = lLocalPositionlPlayerAField;

            //        StartCoroutine(SwitchPlayersCoroutine(lPlayerAField, _playerB, lPlayerBField, lLocalPositionlPlayerAField, lLocalPositionlPlayerBField, lSquadPositionPlayerA));
            //    }
            //}
        }
        
        public void OnGetProfileEvent()
        {
            UpdateSalaryCap();
            Init(); // If LevelUp
        }

        #region Coroutines
        
        public IEnumerator FilterByPosition(string pPosition)
        {
            yield return new WaitForSeconds(0.2f);
            if (false == _onDragPlayerField) {
                if (_playersScrollview.Position == "")
                {
                    ChangeEditStatut(true);
                }

                if (pPosition == _playersScrollview.Position && false == _hideScrollview)
                {
                    // ChangeEditStatut(false);

                    _playersScrollview.Hide();
                    _hideScrollview = true;
                }
                else if(pPosition != "NONE")
                {
                    ChangeEditStatut(true);

                    _playersScrollview.Position =   pPosition;
                    _playersScrollview.Offset   =   0;
                    _playersScrollview.InitScrollview = true;
                    _playersScrollview.CallAPI(true);
                    _hideScrollview = false;
                }
                else
                {
                    ChangeEditStatut(false);

                    _playersScrollview.Hide();
                    _hideScrollview = true;
                }

                _filterAttBtn.SetActive(true);
                _filterDefBtn.SetActive(true);
                _filterGBtn.SetActive(false);

                if (pPosition == "G" && false == _hideScrollview)
                {

                    _filterAttBtn.SetActive(false);
                    _filterDefBtn.SetActive(false);
                    _filterGBtn.SetActive(true);

                    ColorUtility.TryParseHtmlString("#7DF6F4FF", out _color);

                    _positions.transform.GetChild(0).GetComponent<Text>().color = _color;
                    _positions.transform.GetChild(0).GetComponent<RectTransform>().DOScale(1.2f, 0.1f);
                    _positions.transform.GetChild(0).transform.GetChild(0).gameObject.SetActive(true);
                    //foreach (Transform child in _gParent.transform)
                    //{
                    //if (false == child.GetComponent<PlayerField>().Player.activeSelf)
                    //{
                    //    child.GetComponent<PlayerField>().Empty.SetActive(false);
                    //    child.GetComponent<PlayerField>().Droppable.SetActive(true);
                    //}
                    //}
                }
                else
                {
                    ColorUtility.TryParseHtmlString("#FFFFFFFF", out _color);
                    _positions.transform.GetChild(0).GetComponent<Text>().color = _color;
                    _positions.transform.GetChild(0).GetComponent<RectTransform>().DOScale(1f, 0.1f);
                    _positions.transform.GetChild(0).transform.GetChild(0).gameObject.SetActive(false);
                    //foreach (Transform child in _gParent.transform)
                    //{
                    //    if (false == child.GetComponent<PlayerField>().Player.activeSelf)
                    //    {
                    //        child.GetComponent<PlayerField>().Empty.SetActive(true);
                    //        child.GetComponent<PlayerField>().Droppable.SetActive(false);
                    //    }
                    //}
                }

                if (pPosition == "D" && false == _hideScrollview)
                {
                    ColorUtility.TryParseHtmlString("#7DF6F4FF", out _color);
                    _positions.transform.GetChild(1).GetComponent<Text>().color = _color;
                    _positions.transform.GetChild(1).GetComponent<RectTransform>().DOScale(1.2f, 0.1f);
                    _positions.transform.GetChild(1).transform.GetChild(0).gameObject.SetActive(true);
                    //foreach (Transform child in _dParent.transform)
                    //{
                    //    if (false == child.GetComponent<PlayerField>().Player.activeSelf)
                    //    {
                    //        child.GetComponent<PlayerField>().Empty.SetActive(false);
                    //        child.GetComponent<PlayerField>().Droppable.SetActive(true);
                    //    }
                    //}
                }
                else
                {
                    ColorUtility.TryParseHtmlString("#FFFFFFFF", out _color);
                    _positions.transform.GetChild(1).GetComponent<Text>().color = _color;
                    _positions.transform.GetChild(1).GetComponent<RectTransform>().DOScale(1f, 0.1f);
                    _positions.transform.GetChild(1).transform.GetChild(0).gameObject.SetActive(false);
                    //foreach (Transform child in _dParent.transform)
                    //{
                    //    if (false == child.GetComponent<PlayerField>().Player.activeSelf)
                    //    {
                    //        child.GetComponent<PlayerField>().Empty.SetActive(true);
                    //        child.GetComponent<PlayerField>().Droppable.SetActive(false);
                    //    }
                    //}
                }

                if (pPosition == "M" && false == _hideScrollview)
                {
                    ColorUtility.TryParseHtmlString("#7DF6F4FF", out _color);
                    _positions.transform.GetChild(2).GetComponent<Text>().color = _color;
                    _positions.transform.GetChild(2).GetComponent<RectTransform>().DOScale(1.2f, 0.1f);
                    _positions.transform.GetChild(2).transform.GetChild(0).gameObject.SetActive(true);
                    //foreach (Transform child in _mParent.transform)
                    //{
                    //    if (false == child.GetComponent<PlayerField>().Player.activeSelf)
                    //    {
                    //        child.GetComponent<PlayerField>().Empty.SetActive(false);
                    //        child.GetComponent<PlayerField>().Droppable.SetActive(true);
                    //    }
                    //}
                }
                else
                {
                    ColorUtility.TryParseHtmlString("#FFFFFFFF", out _color);
                    _positions.transform.GetChild(2).GetComponent<Text>().color = _color;
                    _positions.transform.GetChild(2).GetComponent<RectTransform>().DOScale(1f, 0.1f);
                    _positions.transform.GetChild(2).transform.GetChild(0).gameObject.SetActive(false);
                    //foreach (Transform child in _mParent.transform)
                    //{
                    //    if (false == child.GetComponent<PlayerField>().Player.activeSelf)
                    //    {
                    //        child.GetComponent<PlayerField>().Empty.SetActive(true);
                    //        child.GetComponent<PlayerField>().Droppable.SetActive(false);
                    //    }
                    //}
                }

                if (pPosition == "A" && false == _hideScrollview)
                {
                    ColorUtility.TryParseHtmlString("#7DF6F4FF", out _color);
                    _positions.transform.GetChild(3).GetComponent<Text>().color = _color;
                    _positions.transform.GetChild(3).GetComponent<RectTransform>().DOScale(1.2f, 0.1f);
                    _positions.transform.GetChild(3).transform.GetChild(0).gameObject.SetActive(true);
                    //foreach (Transform child in _aParent.transform)
                    //{
                    //    if (false == child.GetComponent<PlayerField>().Player.activeSelf)
                    //    {
                    //        child.GetComponent<PlayerField>().Empty.SetActive(false);
                    //        child.GetComponent<PlayerField>().Droppable.SetActive(true);
                    //    }
                    //}
                }
                else
                {
                    ColorUtility.TryParseHtmlString("#FFFFFFFF", out _color);
                    _positions.transform.GetChild(3).GetComponent<Text>().color = _color;
                    _positions.transform.GetChild(3).GetComponent<RectTransform>().DOScale(1f, 0.1f);
                    _positions.transform.GetChild(3).transform.GetChild(0).gameObject.SetActive(false);
                    //foreach (Transform child in _aParent.transform)
                    //{
                    //    if (false == child.GetComponent<PlayerField>().Player.activeSelf)
                    //    {
                    //        child.GetComponent<PlayerField>().Empty.SetActive(true);
                    //        child.GetComponent<PlayerField>().Droppable.SetActive(false);
                    //    }
                    //}
                }
            }

        }

        public IEnumerator SwitchPlayersCoroutine(PlayerField pPlayerFieldA, PlayersItem pPlayerB, PlayerField pPlayerFieldB, PlayersItem pPlayerA, Vector3 lLocalPositionlPlayerAField, Vector3 lLocalPositionlPlayerBField)
        {
            yield return new WaitForSeconds(0.3f);
            pPlayerFieldB.gameObject.GetComponent<RectTransform>().localPosition = lLocalPositionlPlayerBField;
            pPlayerFieldA.gameObject.GetComponent<RectTransform>().localPosition = lLocalPositionlPlayerAField;

            pPlayerFieldA.Init(pPlayerB, false);
            pPlayerFieldB.Init(pPlayerA, false);

            _onSwitch = false;

            AutoSave();
        }

        public IEnumerator SwitchPlayersCoroutine(PlayerField pPlayerFieldA, PlayersItem pPlayerB, PlayerField pPlayerFieldB, Vector3 lLocalPositionlPlayerAField, Vector3 lLocalPositionlPlayerBField, string lSquadPositionPlayerA)
        {
            yield return new WaitForSeconds(0.3f);
            //pPlayerFieldB.gameObject.GetComponent<RectTransform>().localPosition = lLocalPositionlPlayerBField;
            //pPlayerFieldA.gameObject.GetComponent<RectTransform>().localPosition = lLocalPositionlPlayerAField;


            //SellPlayer(pPlayerB.Profile.Value, pPlayerB.Profile.RareLevel);
            //SubstractAttack(pPlayerB.Profile.PvpAttack);
            //SubstractDefense(pPlayerB.Profile.PvpDefense);
            //PlayersOnField.Remove(pPlayerB);
            //pPlayerB.Profile.SquadPosition = lSquadPositionPlayerA;

            //pPlayerFieldA.Init(pPlayerB, true);

            //pPlayerFieldB.ClearInfos();
    
            //_defaultUI.AnimateCanevasInOut(pPlayerFieldB.Empty, pPlayerFieldB.Player, 0.3f);

            //_onSwitch = false;

            //AutoSave();
        }

        #endregion
        
        #endregion

        #region Private Methods

        private void    Start()
		{
            _fieldPosition = _field.localPosition;
            _field.localPosition = Vector3.zero;
            _field.DOScale(1.1f, 0f);
            _budgetPosition = _budget.localPosition;
            _budget.localPosition = new Vector3(0f, _budgetPosition.y, _budgetPosition.z);
            _budget.localScale = Vector3.zero;
            _infoPlus.localScale = Vector3.zero;
            _garbage.localScale = Vector3.zero;
            ChangeEditStatut(false);

            foreach (Transform child in _gParent.transform)
            {
                child.GetComponent<PlayerField>().ClearInfos();
            }

            foreach (Transform child in _dParent.transform)
            {
                child.GetComponent<PlayerField>().ClearInfos();
            }

            foreach (Transform child in _mParent.transform)
            {
                child.GetComponent<PlayerField>().ClearInfos();
            }

            foreach (Transform child in _aParent.transform)
            {
                child.GetComponent<PlayerField>().ClearInfos();
            }

         
            Init();

            _playersScrollview.InitPlayersScrollview();

            Debug.Log("Screen Height : " + Screen.height);
            Debug.Log("Element Size by Screen Height : " + Screen.height / 8);
        }

        private void    UpdateSalaryCap()
        {
			Assert.IsNotNull(_salaryData, "[SquadUI] UpdateSalaryCap(), _salaryData is null.");

			Assert.IsNotNull(UserData.Instance, "[SquadUI] UpdateSalaryCap(), UserData.Instance");

			Assert.IsNotNull(UserData.Instance.Profile, "[SquadUI] UpdateSalaryCap(), UserData.Instance.Profile");

            if(true == UserData.Instance.Profile.IsVip)
            {
                _salaryCapMax = float.Parse(_salaryData.CapVip[UserData.Instance.Profile.Level]);
            }
            else
            {
                _salaryCapMax = float.Parse(_salaryData.Cap[UserData.Instance.Profile.Level]);
            }
           
            _salaryCapMaxText.text = _salaryCapMax.ToString();
        }

        /// <summary>
        /// Init players field with an existing squad
        /// </summary>
        private void    Init()
        {
            UpdateSalaryCap();

			Assert.IsNotNull(_salaryData, "[SquadUI] Init(), _salaryData is null.");

			Assert.IsNotNull(UserData.Instance, "[SquadUI] Init(), UserData.Instance");

			Assert.IsNotNull(UserData.Instance.Profile, "[SquadUI] Init(), UserData.Instance.Profile");

            if (true == UserData.Instance.Profile.IsVip)
            {
                _salaryCap = float.Parse(_salaryData.CapVip[UserData.Instance.Profile.Level]);
            }
            else
            {
                _salaryCap = float.Parse(_salaryData.Cap[UserData.Instance.Profile.Level]);
            }

            _salaryCapText.text = _salaryCap.ToString();

			Assert.IsNotNull(_squadPlayersData, "[SquadUI] Init(), _squadPlayersData is null.");

			Assert.IsNotNull(_squadPlayersData.SquadPlayers, "[SquadUI] Init(), _squadPlayersData.SquadPlayers is null.");

            //List<PlayersItem> lPlayersG = _squadPlayersData.SquadPlayers.FindAll(item => item.Profile.Position == "G");
            //List<PlayersItem> lPlayersD = _squadPlayersData.SquadPlayers.FindAll(item => item.Profile.Position == "D");
            //List<PlayersItem> lPlayersM = _squadPlayersData.SquadPlayers.FindAll(item => item.Profile.Position == "M");
            //List<PlayersItem> lPlayersA = _squadPlayersData.SquadPlayers.FindAll(item => item.Profile.Position == "A");

            _playersOnField = new List<PlayersItem>();
            
			Sprite[]	lSprites;
			string[]	lPaths	=	new string[]{ "Sprites/Player/player-0", "Sprites/Player/player-1", "Sprites/Player/player-2", "Sprites/Squad/squad-0", "Sprites/Squad/squad-1" };
			Dictionary<string, Sprite> lPlayerSprites = new Dictionary<string, Sprite>();
			
			for (int lj = 0; lj < lPaths.Length; lj++)
			{
				lSprites =	Resources.LoadAll<Sprite>(lPaths[lj]);

				for (int li = 0; li < lSprites.Length; li++)
				{
					lPlayerSprites.Add(lSprites[li].name, lSprites[li]);
				}
			}

            foreach (Transform child in _gParent.transform)
            {
                PlayersItem lPlayer = _squadPlayersData.SquadPlayers.Find(item => item.Profile.SquadPosition == child.gameObject.name);
                child.GetComponent<PlayerField>().PlayerName = "";
                if (null != lPlayer)
                {
                    PlayersOnField.Add(lPlayer);
                    child.GetComponent<PlayerField>().Init(lPlayer, true, false, lPlayerSprites);
                }
            }

            foreach (Transform child in _dParent.transform)
            {
                PlayersItem lPlayer = _squadPlayersData.SquadPlayers.Find(item => item.Profile.SquadPosition == child.gameObject.name);
                child.GetComponent<PlayerField>().PlayerName = "";
                if (null != lPlayer)
                {
                    PlayersOnField.Add(lPlayer);
                    child.GetComponent<PlayerField>().Init(lPlayer, true, false, lPlayerSprites);
                }
            }

            foreach (Transform child in _mParent.transform)
            {
                PlayersItem lPlayer = _squadPlayersData.SquadPlayers.Find(item => item.Profile.SquadPosition == child.gameObject.name);
                child.GetComponent<PlayerField>().PlayerName = "";
                if (null != lPlayer)
                {
                    PlayersOnField.Add(lPlayer);
                    child.GetComponent<PlayerField>().Init(lPlayer, true, false, lPlayerSprites);
                }
            }

            foreach (Transform child in _aParent.transform)
            {
                PlayersItem lPlayer = _squadPlayersData.SquadPlayers.Find(item => item.Profile.SquadPosition == child.gameObject.name);
                child.GetComponent<PlayerField>().PlayerName = "";
                if (null != lPlayer)
                {
                    PlayersOnField.Add(lPlayer);
                    child.GetComponent<PlayerField>().Init(lPlayer, true, false, lPlayerSprites);
                }
            }

            foreach (Transform child in _listPlayersParent.transform)
            {
                child.GetComponent<PlayerItemList>().UpdateUI();

            }
        }

        private void    ChangeEditStatut(bool status)
        {
            _edit = status;

            if(true == status)
            {
                _title.SetActive(false);
            }
            else
            {
                _title.SetActive(true);
            }

            foreach (Transform child in _gParent.transform)
            {
                if(false == status) {
                    child.GetComponent<PlayerField>().Player.transform.GetChild(1).gameObject.SetActive(false);
                }
                else
                {
                    child.GetComponent<PlayerField>().Player.transform.GetChild(1).gameObject.SetActive(true);
                }
               
            }

            foreach (Transform child in _dParent.transform)
            {
                if (false == status)
                {
                    child.GetComponent<PlayerField>().Player.transform.GetChild(1).gameObject.SetActive(false);
                }
                else
                {
                    child.GetComponent<PlayerField>().Player.transform.GetChild(1).gameObject.SetActive(true);
                }
            }

            foreach (Transform child in _mParent.transform)
            {
                if (false == status)
                {
                    child.GetComponent<PlayerField>().Player.transform.GetChild(1).gameObject.SetActive(false);
                }
                else
                {
                    child.GetComponent<PlayerField>().Player.transform.GetChild(1).gameObject.SetActive(true);
                }
            }

            foreach (Transform child in _aParent.transform)
            {
                if (false == status)
                {
                    child.GetComponent<PlayerField>().Player.transform.GetChild(1).gameObject.SetActive(false);
                }
                else
                {
                    child.GetComponent<PlayerField>().Player.transform.GetChild(1).gameObject.SetActive(true);
                }
            }


            if(true == status)
            {
                _field.DOLocalMove(_fieldPosition, 0.3f);
                _field.DOScale(1f, 0.3f);

                _budget.DOLocalMoveX(_budgetPosition.x, 0.3f);
                _budget.DOScale(1f, 0.3f);

                _explain1.DOScale(1f, 0.3f);
                _explain2.DOScale(1f, 0.3f);


                _infoPlus.DOScale(1f, 0.3f);

                _garbage.DOScale(1f, 0.3f);
            }
            else
            {
                _field.DOLocalMove(Vector3.zero, 0.3f);
                _field.DOScale(1.1f, 0.3f);

                _budget.DOLocalMoveX(0f, 0.3f);
                _budget.DOScale(0f, 0.3f);

                _explain1.DOScale(0f, 0.3f);
                _explain2.DOScale(0f, 0.3f);

                _infoPlus.DOScale(0f, 0.3f);

                _garbage.DOScale(0f, 0.3f);
            }
        }
        
        #endregion
    }
}

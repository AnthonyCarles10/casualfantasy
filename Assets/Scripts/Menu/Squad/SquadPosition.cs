﻿using Sportfaction.CasualFantasy.Menu.Squad;

using UnityEngine;
using UnityEngine.EventSystems;

namespace Sportfaction.CasualFantasy.Squad
{
	/// <summary>
	/// Description:
	/// Author: Antoine de Lachèze-Murel
	/// </summary>
    public sealed class SquadPosition : MonoBehaviour, IPointerDownHandler
    {
    

		#region Fields

        [Header("Links")]

        [SerializeField, Tooltip("Squad UI")]
        private SquadUI _squadUI = null;


        #endregion

        #region Public Methods
        /// <summary>
        /// Is called when the user has pressed 
        /// the mouse button while
        /// over the GUIElement or Collider.
        /// </summary>
        public void OnPointerDown(PointerEventData eventData)
        {
            if(null != eventData.pointerEnter) {
                switch (eventData.pointerEnter.transform.name)
                {
                    case "[Pos - G] (Text)":
                        StartCoroutine(_squadUI.FilterByPosition("G"));
                        break;
                    case "[Pos - D] (Text)":
                        StartCoroutine(_squadUI.FilterByPosition("D"));
                        break;
                    case "[Pos - M] (Text)":
                        StartCoroutine(_squadUI.FilterByPosition("M"));
                        break;
                    case "[Pos - A] (Text)":
                        StartCoroutine(_squadUI.FilterByPosition("A"));
                        break;
                    default:
                        StartCoroutine(_squadUI.FilterByPosition("NONE"));
                        break;
                }
            }
           
        }

        #endregion

        #region Private Methods

     


        #endregion
    }
}

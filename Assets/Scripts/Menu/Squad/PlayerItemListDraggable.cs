﻿using Sportfaction.CasualFantasy.Events;

using DG.Tweening;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Sportfaction.CasualFantasy.Menu.Squad
{
    /// <summary>
    /// Description: Player Item List
    /// Author: Antoine de Lachèze-Murel
    /// </summary>
    #pragma warning disable 414, CS0649
    public class PlayerItemListDraggable : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
    {
        #region Fields

        [Header("Links")]

        [SerializeField, Tooltip("Squad UI")]
        private SquadUI _squadUI = null;

        [SerializeField, Tooltip("Players Canvas")]
        private CanvasGroup _playersCanvas = null;

        [SerializeField, Tooltip("Scroll Rect")]
        private ScrollRect _scrollRect = null;

        [SerializeField, Tooltip("Default UI")]
        private DefaultUI _defaultUI = null;

        [SerializeField, Tooltip("Default UI")]
        private PlayerItemList _playerItemList = null;

        [Header("UI")]
        //[SerializeField, Tooltip("Animation Game Object")]
        //private	GameObject	_animationGameObject	=	null;

        //private Sequence _sequence = null;

        [Header("Drag & Drop")]

        [SerializeField, ReadOnly,Tooltip("Screen Pos")]
        private Vector3 _screenPos = Vector3.zero;

        [SerializeField, ReadOnly, Tooltip("Start Local Position")]
        private Vector3 _startLocalPosition = Vector3.zero;

        [SerializeField, ReadOnly, Tooltip("On Drag")]
        private bool _onDrag = false;

        [SerializeField, Tooltip("_dragAnimation")]
        private Animator _dragAnimation;

        [Header("GameEvent")]
        [SerializeField]
        private GameEvent _onDropPlayer = null;


        #endregion

        #region Public Methods


        /// <summary>
        /// Is called when the user has pressed 
        /// the mouse button while
        /// over the GUIElement or Collider.
        /// </summary>
        public void OnMouseDown()
        {
            /*if (false == _squadUI.PlayersOnField.Exists(item => item.Profile.PlayerId == _playerItemList.Player.Profile.PlayerId)) {

                 _animationGameObject.SetActive(true);

                 StartCoroutine(TriggerAnimation(0.5f));

                _sequence.Kill();
                 Start New Sequence
                 _sequence.Insert(0.3f, _staticGameObject.GetComponent<RectTransform>().DOScale(1.01f, 0.2f));
                 _sequence.Insert(0.5f, _staticGameObject.GetComponent<CanvasGroup>().DOFade(0f, 0.1f));


                 _sequence.Insert(0.6f, _staticGameObject.GetComponent<RectTransform>().DOScale(1f, 0.1f));
                 _sequence.Insert(0.7f, _staticGameObject.GetComponent<CanvasGroup>().DOFade(1f, 0.2f));
                _sequence.Play();
            }*/
        }

        public void OnBeginDrag(PointerEventData pData)
        {
            this.gameObject.SetActive(true);
            _dragAnimation.SetTrigger("play");
        }

        /// <summary>
        /// What to do when the event system sends a Drag Event.
        /// </summary>
        public void OnDrag(PointerEventData pData)
        {
            //if (_defaultUI.TimePressed > 0.1f && false == _squadUI.PlayersOnField.Exists(item => item.Profile.PlayerId == _playerItemList.Player.Profile.PlayerId)) {


            //    _playersCanvas.blocksRaycasts = false;

            //    _scrollRect.enabled = false;

            //    _squadUI.PlayerIsDraggedFromList = _playerItemList.Player;

            //    _squadUI.PlayerListIsDragged(true);


            //    _playerItemList.OffUI();
            //    // _defaultUI.AnimateCanevasInOutBis(_staticOffGameObject, _staticGameObject, 0.2f);
            //    //_staticGameObject.SetActive(false);

                

            //    //if(false == _onDrag)
            //    //{
            //    //    this.GetComponent<RectTransform>().DOScale(1.4f, 0.2f);
            //    //}

            //    _onDrag = true;

            //    // SetDraggedPosition(pData);

            //    _screenPos = _getInputPosition();
            //    _screenPos.z = 1f;

            //    this.transform.position = Camera.main.ScreenToWorldPoint(_screenPos); 
            //}
        }

        /// <summary>
        /// Capture the OnEndDrag callback
        /// from the EventSystem and
        /// cancel the listening of drag events.
        /// </summary>
        public void OnEndDrag(PointerEventData eventData)
        {
            this.GetComponent<RectTransform>().DOLocalMove(_startLocalPosition, 0.3f);

            //_defaultUI.AnimateCanevasInOutBis(_staticGameObject, _staticOffGameObject, 0.3f);

            // this.GetComponent<RectTransform>().DOScale(1.4f, 0.3f);

            _playersCanvas.blocksRaycasts = true;

            _scrollRect.enabled = true;

            _onDropPlayer.Invoke();

            OnPlayerIsDropped();

            _squadUI.PlayerListIsDragged(false);

            _squadUI.PlayerIsDraggedFromList = null;

            _onDrag = false;

        }

        //private void SetDraggedPosition(PointerEventData data)
        //{

        //    Vector3 lPosition = this.transform.TransformPoint(data.position);
        //    lPosition.z = 0;
        //    // this.transform.position = Camera.main.ScreenToWorldPoint(data.position);
        //    this.transform.position = Camera.main.ScreenToWorldPoint(lPosition);
        //}


      

        /// <summary>
        /// When a player is dropped
        /// </summary>
        public void OnPlayerIsDropped()
        {
            //if (true == _squadUI.PlayersOnField.Exists(item => item.Profile.PlayerId == _playerItemList.Player.Profile.PlayerId)) {
            //    StartCoroutine(_defaultUI.ChangeStatusGameObject(this.gameObject, false, 0f));

            //    this.GetComponent<RectTransform>().localPosition = _startLocalPosition;

            //    _onDrag = false;

            //    _squadUI.PlayerListIsDragged(false);

            //    _squadUI.PlayerIsDraggedFromList = null;

            //    _playersCanvas.blocksRaycasts = true;

            //    _scrollRect.enabled = true;

            //   // _defaultUI.AnimateCanevasInOutBis(_staticOffGameObject, _staticGameObject, 0.3f);

            //    _playerItemList.OffUI();

            //} 
            //else
            //{
            //    _playerItemList.OnUI();
            //    //_defaultUI.AnimateCanevasInOutBis(_staticGameObject, _staticOffGameObject, 0.3f);

            //    this.GetComponent<RectTransform>().DOLocalMove(_startLocalPosition, 0.3f);
            //}
        }

        public void OnPointerEnterPlayerField()
        {
            if (true == _onDrag)
            {
                // this.GetComponent<RectTransform>().DOScale(0f, 0.3f);
            }
        }

        public void OnPointerExitPlayerField()
        {
            if (true == _onDrag)
            {
                // this.GetComponent<RectTransform>().DOScale(1.4f, 0.3f);
            }
        }

        #endregion

        #region Private Methods

        private void Start()
        {
            _startLocalPosition = this.GetComponent<RectTransform>().localPosition;
            //_sequence = DOTween.Sequence();
        }

        /// <summary>
        /// Gets the current position of where the user's touching
        /// </summary>
        /// <returns>The input position</returns>
        private Vector3 _getInputPosition()
        {
            #if UNITY_EDITOR
                return Input.mousePosition;
            #else
                if (0 < Input.touchCount)
                {
                    return Input.touches[0].position;
                }
                else
                {
                    return Vector3.zero;
                }
            #endif
        }

      
        #endregion
    }
}

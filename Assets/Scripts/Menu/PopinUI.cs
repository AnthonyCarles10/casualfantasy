﻿using Sportfaction.CasualFantasy.Menu.Squad;

using DG.Tweening;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Sportfaction.CasualFantasy.Menu
{
    public class PopinUI: MonoBehaviour, IPointerDownHandler
    {
		#region Properties
		
		public	Dictionary<string, Sprite>	Sprites	{ set { _sprites = value; } }
		
		#endregion

		#region Fields

		[Header("Links")]

        [SerializeField, Tooltip("DefaultUI")]
        private DefaultUI _defaultUI = null;

        [SerializeField, Tooltip("SquadUI")]
        private SquadUI _squadUI = null;

        [SerializeField, Tooltip("MenuUI")]
        private MenuUI _menuUI = null;

        [Header("UI")]

        [SerializeField, Tooltip("Popin UI")]
        private GameObject _popinUI = null;

        [SerializeField, Tooltip("Popin")]
        private	Image	_popin	=	null;

        [SerializeField, Tooltip("Team Empty")]
        private GameObject _teamEmpty = null;

        [SerializeField, Tooltip("NotEnoughBudjet")]
        private GameObject _notEnoughBudjet = null;

        [SerializeField, Tooltip("ResetSquad")]
        private GameObject _resetSquad = null;

        [SerializeField, Tooltip("NotEnoughSoft")]
        private GameObject _notEnoughSoft = null;

        [SerializeField, Tooltip("ActiveNotificationsIOS")]
        private GameObject _activeNotificationsIOS	=	null;

		[SerializeField, Tooltip("DisplayChallenges")]
		private	GameObject	_challenges				=	null;

		[SerializeField, Tooltip("PlayersUpdated")]
		private	GameObject	_playersUpdated			=	null;

        [SerializeField, Tooltip("BackButton")]
        private	GameObject	_backButton	=	null;

		private Dictionary<String, Sprite> _sprites = new Dictionary<string, Sprite>();

        #endregion

        #region Public Methods

        /// <summary>
        /// Is called when the user has pressed 
        /// the mouse button while
        /// over the GUIElement or Collider.
        /// </summary>
        public void OnPointerDown(PointerEventData eventData)
        {
            if ("[Shadow] (Image)" == eventData.pointerEnter.transform.name && false == _playersUpdated.activeSelf && false == _challenges.activeSelf)
            {
                ClosePopin();
            } 
            else
            {
                switch (eventData.pointerEnter.name)
                {
                    case "[OK]":
                        ClosePopin();
                        break;
                    case "[OK-bis]":
                        ClosePopin(false);
                        break;
                    case "[no]":
                        ClosePopin();
                        break;
                    case "[yes-resetsquad]":
                        _squadUI.ConfirmReset();
                        ClosePopin();
                        break;
                    case "[yes-goToShop]":
                        _menuUI.GoToShop();
                        ClosePopin();
                        break;
                    case "[yes-activeNotificationsIOS]":
                        _menuUI.ActiveNotificationsIOS();
                        ClosePopin(false);
                        break;
                    case "[no-activeNotificationsIOS]":
                        _menuUI.RefuseNotificationsIOS();
                        ClosePopin(false);
                        break;
                    case "[ok-playerUpdated]":
                        _menuUI.GoToSquad();
                        ClosePopin();
                        break;
                }
            }
        }

        public void NotEnoughBudjet()
        {
            Reset();
            _notEnoughBudjet.SetActive(true);

            OpenPopin();
        }

        public void TeamEmpty()
        {
            Reset();
            _teamEmpty.SetActive(true);

            OpenPopin();
        }

        public void ConfirmResetSquad()
        {
            Reset();
            _resetSquad.SetActive(true);

            OpenPopin();
        }

        public void NotEnoughSoft()
        {
            Reset();
            _notEnoughSoft.SetActive(true);

            OpenPopin();
        }

        public void ActiveNotificationsIOS()
        {
            Reset();
            _activeNotificationsIOS.SetActive(true);

            OpenPopin();
        }
        
        public void DisplayChallenges()
        {
            Reset();
            _challenges.SetActive(true);

            OpenPopin("info");
        }

		public void PlayersUpdated()
		{
            Reset();
            _playersUpdated.SetActive(true);

            OpenPopin("news");
		}

		#endregion

		#region Private Methods

		private	void	OpenPopin(string pType = "warning")
        {
        	if ("info" == pType)
        	{
				_popin.sprite	=	Resources.Load<Sprite>("Sprites/Popin/popin_objectives_bg");
				_popin.rectTransform.sizeDelta	=	new Vector2(1928f, 1329f);
        	}
			else
			{
				Sprite lBG;
				_sprites.TryGetValue("Squad_BG_FilterPopup", out lBG);
				_popin.sprite	=	lBG;
				
				if ("warning" == pType)
					_popin.rectTransform.sizeDelta	=	 new Vector2(1200f, 860f);
				else
					_popin.rectTransform.sizeDelta	=	 new Vector2(1200f, 1200f);
			}
        	
            _popinUI.SetActive(true);
            Sequence lSequence = DOTween.Sequence();
            lSequence.Append(_popinUI.GetComponent<CanvasGroup>().DOFade(1f, 0.2f));
            lSequence.Insert(0.1f, _popin.rectTransform.DOScale(1f, 0.3f));

            _defaultUI.AnimateCanevasOut(_backButton, 0.1f);
        }

        private	void	ClosePopin(bool pShowBackButton = true)
        {
            Sequence lSequence = DOTween.Sequence();
            lSequence.Append(_popin.rectTransform.DOScale(0, 0.3f));
            lSequence.Insert(0.1f, _popinUI.GetComponent<CanvasGroup>().DOFade(0f, 0.2f));

            StartCoroutine(_defaultUI.ChangeStatusGameObject(_popinUI, false, 0.5f));

            if(true == pShowBackButton)
            {
                _defaultUI.AnimateCanevasIn(_backButton, 0.1f);
            }
        }

        private	void	Reset()
        {
			_notEnoughBudjet.SetActive(false);
            _teamEmpty.SetActive(false);
            _resetSquad.SetActive(false);
            _notEnoughSoft.SetActive(false);
            _activeNotificationsIOS.SetActive(false);
            _challenges.SetActive(false);
            _playersUpdated.SetActive(false);
        }
       
        #endregion
    }
}

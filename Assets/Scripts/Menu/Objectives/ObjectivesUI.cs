using Sportfaction.CasualFantasy.Manager.Profile;
using Sportfaction.CasualFantasy.Services.Timers;

using DG.Tweening;
using System.Collections;
using UnityEngine;
using UnityEngine.Assertions;

namespace Sportfaction.CasualFantasy.Menu
{
    /// <summary>
    /// Description: MenuUI
    /// Author: Antoine de Lachèze-Murel
    /// </summary>
    public class ObjectivesUI : MonoBehaviour
    {
    

        #region Fields

        [Header("Links")]

        //[SerializeField, Tooltip("UserMain")]
        //private UserMain _userMain = null;

        [Header("UI")]

        [SerializeField, Tooltip("_levelObjectives")]
        private GameObject _levelObjectives = null;

        [SerializeField, Tooltip("_achievmentsBarMax")]
        private RectTransform _achievmentsBarMax = null;

        [SerializeField, Tooltip("_achievmentsBarMax")]
        private RectTransform _achievmentsBarFill = null;

        [SerializeField, Tooltip("_resetCounterGo")]
        private GameObject _resetCounterGo = null;

        [SerializeField, Tooltip("_resetCounter")]
        private TMPro.TMP_Text _resetCounter = null;

        int lHours = 0;
        int lMinutes = 0;
        int lSeconds = 0;

        #endregion

        #region Public Methods


        #endregion

        #region Private Methods

        private void Start()
        {
            GameObject ltimersManagerGO = GameObject.Find("/Essentials/LocalTimersMgr");
            LocalTimersManager ltimersManager = ltimersManagerGO.GetComponent<LocalTimersManager>();

			Assert.IsNotNull(UserData.Instance, "[ObjectivesUI] Start(), UserData.Instance is null.");

			Assert.IsNotNull(UserData.Instance.Profile, "[ObjectivesUI] Start(), UserData.Instance.Profile is null.");

            if(true == ltimersManager.IsTimerComplete("goals") && UserData.Instance.Profile.StarLevel == 5 && false == UserData.Instance.Profile.StarLevelUp)
            {
                UserData.Instance.Profile.StarLevel = 0;

                UserData.Instance.PersistUserData();
            }

            Init();
        }

        private void Init()
        {
			Assert.IsNotNull(UserData.Instance, "[ObjectivesUI] Init(), UserData.Instance is null.");

			Assert.IsNotNull(UserData.Instance.Profile, "[ObjectivesUI] Init(), UserData.Instance.Profile is null.");

            int lStarLevel = UserData.Instance.Profile.StarLevel;
            //int lStarLevel = 4;

            if (true == UserData.Instance.Profile.StarLevelUp)
            {
                lStarLevel = UserData.Instance.Profile.StarLevel - 1;
            }
            else if (lStarLevel == 5)
            {
                _resetCounterGo.SetActive(true);
            }

            _achievmentsBarFill.DOSizeDelta(new Vector2((_achievmentsBarMax.sizeDelta.x / 4) * lStarLevel, _achievmentsBarFill.sizeDelta.y), 0f);

            for (int i = 1; i <= 5; i++)
            {
                Transform lRaw = _levelObjectives.transform.GetChild(i - 1);

                lRaw.GetComponent<ObjectivesPrefab>().Init(i, lStarLevel);
            }

        }



        public IEnumerator AnimateObjectives()
        {
            yield return new WaitForSeconds(0.5f);

			Assert.IsNotNull(UserData.Instance, "[ObjectivesUI] AnimateObjectives(), UserData.Instance is null.");

			Assert.IsNotNull(UserData.Instance.Profile, "[ObjectivesUI] AnimateObjectives(), UserData.Instance.Profile is null.");

            int lStarLevel = UserData.Instance.Profile.StarLevel;  
            // int lStarLevel = 5;  

            StartCoroutine(_levelObjectives.transform.GetChild(lStarLevel - 1).GetComponent<ObjectivesPrefab>().AnimateV());
           
            yield return new WaitForSeconds(1f);
          
            if(lStarLevel < 5)
            {
                _achievmentsBarFill.DOSizeDelta(new Vector2((_achievmentsBarMax.sizeDelta.x / 4) * lStarLevel, _achievmentsBarFill.sizeDelta.y), 2f);

                yield return new WaitForSeconds(1.5f);

                _levelObjectives.transform.GetChild(lStarLevel).GetComponent<ObjectivesPrefab>().NewObjective();
            }

            if(lStarLevel == 5)
            {
                GameObject ltimersManagerGO = GameObject.Find("/Essentials/LocalTimersMgr");
                LocalTimersManager ltimersManager = ltimersManagerGO.GetComponent<LocalTimersManager>();
                //ltimersManager.StartTimer("goals", 4, 0, 0);

                //if(null != Services.Notifications.NotificationsManager.Instance)
                //{
                //    Services.Notifications.NotificationsManager.Instance.SendLocalNotification("LocalNotifGoals");
                //}
               
                // discard remove all notif


                _resetCounterGo.SetActive(true);
            }
           
        }

        public void OnLocalTimersUpdatedEvent()
        {
            //LocalTimersManager.Instance.GetTimeLeft("goals", ref lHours, ref lMinutes, ref lSeconds);
            _resetCounter.text = lHours + "H" + lMinutes + "m" + lSeconds + "s";
        }

        public void OnLocalTimerComplete()
        {
			Assert.IsNotNull(UserData.Instance, "[ObjectivesUI] OnLocalTimerComplete(), UserData.Instance is null.");

			Assert.IsNotNull(UserData.Instance.Profile, "[ObjectivesUI] OnLocalTimerComplete(), UserData.Instance.Profile is null.");

            UserData.Instance.Profile.StarLevel = 0;

            UserData.Instance.PersistUserData();

            if (null != _resetCounterGo)
			{
			    _resetCounterGo.SetActive(false);
			}

            Init();
        }

        #endregion
    }

}

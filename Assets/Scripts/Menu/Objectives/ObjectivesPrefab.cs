﻿using Sportfaction.CasualFantasy.AppParam;
using Sportfaction.CasualFantasy.Config;

using System.Collections;
using UnityEngine.Assertions;
using UnityEngine;
using UnityEngine.UI;

namespace Sportfaction.CasualFantasy.Menu
{
    public class ObjectivesPrefab : MonoBehaviour
    {
        #region Fields

		[Header("App-Params")]
		[SerializeField, Tooltip("Scriptable object contains data about score")]
		private	AppParamManagerScoreData	_scoreData	=	null;
		[SerializeField, Tooltip("Scriptable object contains data about soft currency")]
		private	AppParamManagerSoftData		_softData	=	null;

        [Header("Links")]

        [SerializeField, Tooltip("DefaultUI")]
        private DefaultUI _defaultUI = null;

        [SerializeField, Tooltip("Trophy")]
        private TMPro.TMP_Text _trophy = null;

        [SerializeField, Tooltip("Soft")]
        private TMPro.TMP_Text _soft = null;

        [SerializeField, Tooltip("_v")]
        private GameObject _v = null;

        [SerializeField, Tooltip("_vAnim")]
        private	Image	_vAnim	=	null;

		[Header("Configuration")]
		[SerializeField, Tooltip("Gameplay data")]
		private	TimingConfig	_timingConfig	=	null;

        #endregion

        #region Public Methods
        public void Init(int pIndex, int current)
        {
			Assert.IsNotNull(_scoreData, "[ObjectivesPrefabs] Init(), _scoreData is null.");

            _trophy.text = "+" + _scoreData.BonusStar[pIndex];

			Assert.IsNotNull(_softData, "[ObjectivesPrefabs] Init(), _softData is null.");

            _soft.text = "+" + _softData.BonusStar[pIndex];

            if (pIndex == 5)
            {
                if(pIndex == current)
                {
                    _defaultUI.AnimateCanevasOut(this.transform.GetChild(0).gameObject, 0.1f);
                    _defaultUI.AnimateCanevasIn(this.transform.GetChild(1).gameObject, 0.1f);
                    _defaultUI.AnimateCanevasOut(this.transform.GetChild(2).gameObject, 0.1f);
                }
                else
                {
                    _defaultUI.AnimateCanevasOut(this.transform.GetChild(0).gameObject, 0.1f);
                    _defaultUI.AnimateCanevasOut(this.transform.GetChild(1).gameObject, 0.1f);
                    _defaultUI.AnimateCanevasIn(this.transform.GetChild(2).gameObject, 0.1f);
                }
            }
            else
            {
                if (pIndex <= current)
                {
                    _defaultUI.AnimateCanevasOut(this.transform.GetChild(0).gameObject, 0.1f);
                    _defaultUI.AnimateCanevasIn(this.transform.GetChild(1).gameObject, 0.1f);
                    _defaultUI.AnimateCanevasOut(this.transform.GetChild(2).gameObject, 0.1f);
                }
                else if (pIndex == current + 1)
                {
                    _defaultUI.AnimateCanevasOut(this.transform.GetChild(0).gameObject, 0.1f);
                    _defaultUI.AnimateCanevasOut(this.transform.GetChild(1).gameObject, 0.1f);
                    _defaultUI.AnimateCanevasIn(this.transform.GetChild(2).gameObject, 0.1f);
                }
                else
                {
                    _defaultUI.AnimateCanevasIn(this.transform.GetChild(0).gameObject, 0.1f);
                    _defaultUI.AnimateCanevasOut(this.transform.GetChild(1).gameObject, 0.1f);
                    _defaultUI.AnimateCanevasOut(this.transform.GetChild(2).gameObject, 0.1f);
                }
            }
        }

        public IEnumerator AnimateV()
        {
            _v.SetActive(false);
			//UtilityMethods.AnimateSprite(_vAnim, "Sprites/Gameplay/VictoryAchievement", _timingConfig.DurationSSObjectiveAnim, false);
			_vAnim.gameObject.SetActive(true);

            yield return new WaitForSeconds(1f);
            _defaultUI.AnimateCanevasOut(this.transform.GetChild(2).gameObject, 0.3f);
            _defaultUI.AnimateCanevasIn(this.transform.GetChild(1).gameObject, 0.1f);
        }

        public void NewObjective()
        {
            _defaultUI.AnimateCanevasOut(this.transform.GetChild(0).gameObject, 0.3f);
            _defaultUI.AnimateCanevasOut(this.transform.GetChild(1).gameObject, 0.3f);
            _defaultUI.AnimateCanevasIn(this.transform.GetChild(2).gameObject, 0.3f);
        }
        
        #endregion

    }
}

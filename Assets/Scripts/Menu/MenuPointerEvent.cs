using UnityEngine;
using UnityEngine.EventSystems;

namespace Sportfaction.CasualFantasy.Menu
{
    /// <summary>
    /// Description: MenuPointerEvent
    /// Author: Antoine de Lachèze-Murel
    /// </summary>
    public class MenuPointerEvent : MonoBehaviour, IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler
    {
    
        #region Fields

        [Header("Links")]

        [SerializeField, Tooltip("MenuUI")]
        private MenuUI _menuUI = null;

        [SerializeField, Tooltip("PopinUI")]
        private PopinUI _popinUI = null;

        [SerializeField, Tooltip("DefaultUI")]
        private DefaultUI _defaultUI = null;

        [SerializeField, Tooltip("AudioUI")]
        private AudioUI _audioUI = null;

        [SerializeField, ReadOnly, Tooltip("_currentTransform")]
        private Transform _currentTransform = null;

        #endregion

        #region Public Methods

        /// <summary>
        /// Called by the EventSystem when the pointer
        /// enters the object associated with this EventTrigger.
        /// </summary>
        public void OnPointerEnter(PointerEventData eventData)
        {
            if (null != eventData.pointerEnter)
            {
                switch (eventData.pointerEnter.transform.name)
                {
                    case "[Back]":
                        _audioUI.OnBackSound();
                        _defaultUI.AnimateButtonEnter(eventData.pointerEnter.transform);
                        _currentTransform = eventData.pointerEnter.transform;
                        break;
                    case "[Avatar]":
                    case "[Match]":
                    case "[Squad]":
                    case "[Shop]":
                    case "[Soft]":
                    case "[Leaderboard]":
                    case "[Tactical]":
                    case "[Objectives]":
                        _audioUI.OnClickSound();
                        _defaultUI.AnimateButtonEnter(eventData.pointerEnter.transform);
                        _currentTransform = eventData.pointerEnter.transform;
                        break;
                }
            }

        }

        /// <summary>
        /// Called by the EventSystem when the pointer
        /// enters the object associated with this EventTrigger.
        /// </summary>
        public void OnPointerExit(PointerEventData eventData)
        {
            if (null != _currentTransform)
            {
                _defaultUI.AnimateButtonUp(_currentTransform);
            }
        }

        /// <summary>
        /// Register button presses using the IPointerClickHandler
        /// </summary>
        public void OnPointerClick(PointerEventData eventData)
        {
            if (null != _currentTransform)
            {
                _defaultUI.AnimateButtonUp(_currentTransform);
                GoTo();
            }
        }

        #endregion

        #region Private Methods

        private void GoTo()
        {
            if (null != _currentTransform)
            {

                switch (_currentTransform.name)
                {

                    case "[Back]":
                        _menuUI.GoToMenu();
                        break;
                    case "[Match]":
                        _menuUI.GoToMatch();
                        break;
                    case "[Avatar]":
                    case "[Soft]":
                    case "[Shop]":
                        _menuUI.GoToShop();
                        break;
                    case "[Squad]":
                        _menuUI.GoToSquad();
                        break;
                    case "[Leaderboard]":
                        _menuUI.GoToLeaderboard();
                        break;
                    case "[Tactical]":
                        _menuUI.GoToTactical();
                        break;
                    case "[Objectives]":
                        _popinUI.DisplayChallenges();
                        break;
    }
            }
        }

        #endregion
    }
}
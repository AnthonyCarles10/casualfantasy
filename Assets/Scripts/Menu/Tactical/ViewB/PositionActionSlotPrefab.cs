﻿using Sportfaction.CasualFantasy.Action;
using Sportfaction.CasualFantasy.Manager.Profile;
using Sportfaction.CasualFantasy.Shop;

using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;

namespace Sportfaction.CasualFantasy.Tactical
{
    /// <summary>
    /// Description: TacticalPositionPrefabViewA
    /// Author: Antoine de Lachèze-Murel
    /// </summary>
    public class PositionActionSlotPrefab : MonoBehaviour
    {
        #region Fields

		[Header("Data")]
		[SerializeField, Tooltip("Scriptable object contains data about manage's product")]
		private	ProductData		_productData	=	null;
		[SerializeField, Tooltip("Scriptable object contains data about actions")]
		private	ActionsData		_actionsData	=	null;

        //[Header("Links")]
        //[SerializeField, ReadOnly, Tooltip("Tactical UI")]
        //private	TacticalUI	_tacticalUI	=	null;

        [Header("UI")]
        [SerializeField, Tooltip("Name")]
        private Text _name = null;

        [SerializeField, Tooltip("Level")]
        private	Text	_level			=	null;
        [SerializeField, Tooltip("Level to unLock")]
        private	Text	_levelToUnlock	=	null;

        [SerializeField, Tooltip("Level")]
        private Text _critical = null;

        [SerializeField, Tooltip("Soft A")]
        private	Text	_softA	=	null;
        [SerializeField, Tooltip("Soft B")]
        private	Text	_softB	=	null;

        [SerializeField, Tooltip("Logo")]
        private Image _logo = null;

        [SerializeField, Tooltip("Shifumi")]
        private Image _shifumi = null;

        [SerializeField, Tooltip("_upgradeBtn")]
        private GameObject _upgradeBtn = null;

        //private ActionsItemData _action = null;
        //private string[] _romanNumerals = new string[] { "0", "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX", "X" };

        #endregion

        #region Public Methods

        public	void	Init(ActionsItemData lAction, TacticalUI pTacticalUI)
        {
			//_action = lAction;
			//_tacticalUI	=	pTacticalUI;
            _name.text	=	I2.Loc.LocalizationManager.GetTranslation(lAction.KeyName + "Title");
            
            _upgradeBtn.transform.GetChild(0).gameObject.name = "action-" + lAction.KeyName;
            _upgradeBtn.transform.GetChild(0).gameObject.SetActive(true);
            _upgradeBtn.transform.GetChild(1).gameObject.SetActive(false);
            _upgradeBtn.transform.GetChild(2).gameObject.SetActive(false);
            _upgradeBtn.transform.GetChild(3).gameObject.SetActive(false);

            if (lAction.CriticalRate > 0)
            {
                _critical.text = I2.Loc.LocalizationManager.GetTranslation("Critical") + " : " + lAction.CriticalRate + "%";
            }
            else
            {
                _critical.text = "";
            }

            string lKeyName = lAction.KeyName.Remove(lAction.KeyName.Length - 1, 1);

			Assert.IsNotNull(_productData, "[PositionActionSlotPrefab] Init(), _productData is null.");

            ProductItemData lProduct = _productData.Items.Find(item => item.Reference == "com.sportfaction.action." + lKeyName + "1");

			Assert.IsNotNull(_actionsData, "[PositionActionSlotPrefab] Init(), _actionsData is null.");

            ActionsItemData lActionNext = _actionsData.Items.Find(item => item.KeyName == lKeyName + "1");

            if (lActionNext != null)
            {
                lAction = lActionNext;
            }

            _levelToUnlock.text = I2.Loc.LocalizationManager.GetTranslation("level") + " " + lAction.LevelToUnlock.ToString();

			Assert.IsNotNull(UserData.Instance, "[PositionActionSlotPrefab] Init(), UserData.Instance is null.");

			Assert.IsNotNull(UserData.Instance.Profile, "[PositionActionSlotPrefab] Init(), UserData.Instance.Profile is null.");

            if (null != lProduct)
            {
                _softA.text = lProduct.PriceSoft.ToString();
                _softB.text = lProduct.PriceSoft.ToString();

                if (int.Parse(UserData.Instance.Profile.Soft) >= lProduct.PriceSoft)
                {
                    _upgradeBtn.transform.GetChild(0).gameObject.SetActive(true);
                    _upgradeBtn.transform.GetChild(1).gameObject.SetActive(false);
                    _upgradeBtn.transform.GetChild(2).gameObject.SetActive(false);
                    _upgradeBtn.transform.GetChild(3).gameObject.SetActive(false);
                }
                else
                {
                    _upgradeBtn.transform.GetChild(0).gameObject.SetActive(false);
                    _upgradeBtn.transform.GetChild(1).gameObject.SetActive(true);
                    _upgradeBtn.transform.GetChild(2).gameObject.SetActive(false);
                    _upgradeBtn.transform.GetChild(3).gameObject.SetActive(false);
                }
               
            }
            if (UserData.Instance.Profile.Level < lAction.LevelToUnlock)
            {
                _upgradeBtn.transform.GetChild(3).gameObject.SetActive(true);
                _upgradeBtn.transform.GetChild(0).gameObject.SetActive(false);
                _upgradeBtn.transform.GetChild(1).gameObject.SetActive(false);
                _upgradeBtn.transform.GetChild(2).gameObject.SetActive(false);
            }
            
            //if (lMetaLevel > 3)
            //{
            //    _upgradeBtn.transform.GetChild(0).gameObject.SetActive(false);
            //    _upgradeBtn.transform.GetChild(1).gameObject.SetActive(false);
            //    _upgradeBtn.transform.GetChild(2).gameObject.SetActive(true);
            //    _upgradeBtn.transform.GetChild(3).gameObject.SetActive(false);
            //}
        }

        public	void	Reset(TacticalUI pTacticalUI)
        {
            //_tacticalUI	=	pTacticalUI;
            _name.text	=	"";
        }


        #endregion
    }
}
﻿using Sportfaction.CasualFantasy.Action;

using DG.Tweening;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Sportfaction.CasualFantasy.Tactical
{
    /// <summary>
    /// Description: TacticalViewB
    /// Author: Antoine de Lachèze-Murel
    /// </summary>
    public class TacticalViewB : MonoBehaviour
    {
        #region Properties

        public	int	CurrentSlide	{ get { return _currentSlide; }  }

        #endregion

        #region Fields

        [Header("Links")]
        [SerializeField, Tooltip("Tactical UI")]
        private	TacticalUI	_tacticalUI	=	null;

        [Header("UI")]
        [SerializeField, Tooltip("_titleDefensive")]
        private GameObject	_titleDefensive	=	null;
        [SerializeField, Tooltip("_titleOffensive")]
        private	GameObject	_titleOffensive	=	null;
        
        [SerializeField, Tooltip("_squadPosition")]
        private Text        _squadPosition  =   null;

        [SerializeField, Tooltip("_attDefautParent")]
        private GameObject  _attDefautParent    =   null;
        [SerializeField, Tooltip("_defDefautParent")]
        private GameObject  _defDefautParent    =   null;

        [Space(8)]
        [SerializeField, Tooltip("Bullets")]
        private Transform       _bullets    =   null;
        [SerializeField, Tooltip("Slider")]
        private RectTransform   _slider     =   null;
        [SerializeField, Tooltip("Previous")]
        private GameObject      _previous   =   null;
        [SerializeField, Tooltip("Next")]
        private GameObject      _next       =   null;

        [Header("Data")]
        [SerializeField, Tooltip("Database of every action data")]
        private ActionsData _actionsDatabase    =   null; //!< Database of every action data
        [SerializeField, ReadOnly, Tooltip("Composition")]
        private int         _currentSlide       =	0;
 
		private Dictionary<string, Sprite>	_sprites	=	new Dictionary<string, Sprite>();
		
        #endregion

        #region Public Methods

        /// <summary>
        /// Starts view B
        /// </summary>
        public	void	StartViewB()
        {
            //InitAttack();
            //InitDefense();

            _squadPosition.text = I2.Loc.LocalizationManager.GetTranslation(_tacticalUI.SquadPositionSelected + "long");
        }

        /// <summary>
        /// Goes to next slide
        /// </summary>
        public	void	Next()
        {
            if (_currentSlide < _slider.gameObject.transform.childCount - 1)
            {
                _currentSlide++;

                UpdateSlider();
            }
        }

        /// <summary>
        /// Goes to previous slide
        /// </summary>
        public	void	Previous()
        {
            if (_currentSlide > 0)
            {
                _currentSlide--;

                UpdateSlider();
            }
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// TODO: comment
        /// </summary>
        private void	Start()
        {
            RectTransform lNextRect = _next.GetComponent<RectTransform>();
            RectTransform lPrevRect = _previous.GetComponent<RectTransform>();
            
            Sequence lSeq = DOTween.Sequence();
            lSeq.Insert(0, lNextRect.DOLocalMoveX(lNextRect.localPosition.x + 10f, 0.5f));
            lSeq.Insert(0.5f, lNextRect.DOLocalMoveX(lNextRect.localPosition.x - 10f, 0.5f));
            lSeq.SetLoops(-1);

            lSeq.Insert(0, lPrevRect.DOLocalMoveX(lPrevRect.localPosition.x + 10f, 0.5f));
            lSeq.Insert(0.5f, lPrevRect.DOLocalMoveX(lPrevRect.localPosition.x - 10f, 0.5f));
            lSeq.SetLoops(-1);
            lSeq.Play();

            _previous.SetActive(false);
            
			Sprite[]	lSprites	=	Resources.LoadAll<Sprite>("Sprites/tactical");
			
			for (int li = 0; li < lSprites.Length; li++)
				_sprites.Add(lSprites[li].name, lSprites[li]);
        }

        /*private	void	InitAttack()
        {
			Assert.IsNotNull(_actionsDatabase, "[TacticalViewB] InitAttack(), _actionsData is null.");

			Assert.IsNotNull(_tacticalMemberActions, "[TacticalViewB] InitAttack(), _tacticalMemberActions is null.");

            var lQuery =
                from tacticalAction in _tacticalMemberActions.Items
                join action in _actionsDatabase.Items on tacticalAction.action_id equals action.Id
                where tacticalAction.type == "A" && tacticalAction.full_position == _tacticalUI.SquadPositionSelected
                orderby action.StackOrder
                select new { TacticalAction = tacticalAction, Action = action };

            List<TacticalMemberItemActions> lTacticalActions = new List<TacticalMemberItemActions>();
            List<ActionsItemData> lActionsItemData = new List<ActionsItemData>();

            foreach (var result in lQuery)
            {
                lTacticalActions.Add(result.TacticalAction);
                lActionsItemData.Add(result.Action);
            }

            int lIndex = 0;
            foreach (Transform child in _attDefautParent.transform)
            {
                if (lIndex < lActionsItemData.Count)
                {
                    child.gameObject.SetActive(true);
                    InitPrefab(lActionsItemData[lIndex], _attDefautParent, lIndex);
                }
                else
                {
                    child.gameObject.SetActive(false);
                }
                lIndex++;
            }

                   
            //for (int lSlotIndex = 1; lSlotIndex <= 3; lSlotIndex++)
            //{
            //    InitPrefab(lTacticalActions, _attDefautParent, lSlotIndex, lIndex);
            //    lIndex++;
            //}

        }

        private	void	InitDefense()
        {
			Assert.IsNotNull(_actionsDatabase, "[TacticalViewB] InitDefense(), _actionsData is null.");

			Assert.IsNotNull(_tacticalMemberActions, "[TacticalViewB] InitDefense(), _tacticalMemberActions is null.");

            var lQuery =
                from tacticalAction in _tacticalMemberActions.Items
                join action in _actionsDatabase.Items on tacticalAction.action_id equals action.Id
                where tacticalAction.type == "D" && tacticalAction.full_position == _tacticalUI.SquadPositionSelected
                orderby action.StackOrder
                select new { TacticalAction = tacticalAction, Action = action };

            List<TacticalMemberItemActions> lTacticalActions = new List<TacticalMemberItemActions>();
            List<ActionsItemData> lActionsItemData = new List<ActionsItemData>();

            foreach (var result in lQuery)
            {
                lTacticalActions.Add(result.TacticalAction);
                lActionsItemData.Add(result.Action);
            }

            int lIndex = 0;
            foreach (Transform child in _defDefautParent.transform)
            {
                if (lIndex < lActionsItemData.Count)
                {
                    child.gameObject.SetActive(true);
                    InitPrefab(lActionsItemData[lIndex], _defDefautParent, lIndex);
                }
                else
                {
                    child.gameObject.SetActive(false);
                }
                lIndex++;
            }

            //List<TacticalMemberItemActions> lTacticalActions = new List<TacticalMemberItemActions>();


            //Debug.Log("lTacticalActions InitDefense > " + lTacticalActions.Count);

            //foreach (var result in lQuery)
            //{
            //    lTacticalActions.Add(result.TacticalAction);
            //}

            //int lIndex = 0;
            //for (int lSlotIndex = 1; lSlotIndex <= 3; lSlotIndex++)
            //{
            //    InitPrefab(lTacticalActions, _defDefautParent, lSlotIndex, lIndex);
            //    lIndex++;
            //}
        }*/

        /// <summary>
        /// Initializes action button prefab
        /// </summary>
        /// <param name="pAction">Action data</param>
        /// <param name="pParent">Prefab parent container</param>
        /// <param name="pIndex">Action index</param>
        private	void	InitPrefab(ActionsItemData pAction, GameObject pParent, int pIndex)
        {
            if(null != pAction)
            {
                Transform lChild = pParent.transform.GetChild(pIndex);
                PositionActionSlotPrefab lScript = lChild.gameObject.GetComponent<PositionActionSlotPrefab>();
                lScript.Init(pAction, _tacticalUI);
                lChild.name = "action-" + pAction.KeyName;
            }
            else
            {
                Transform lChild = pParent.transform.GetChild(pIndex);
                //PositionActionSlotPrefab lScript = lChild.gameObject.GetComponent<PositionActionSlotPrefab>();
                lChild.name = "none";
            }
        }

        /// <summary>
        /// Updates slider UI
        /// </summary>
        private	void	UpdateSlider()
        {
			Sprite lSprite = null;
			
            if(_currentSlide == _slider.gameObject.transform.childCount -1)
            {
                _next.SetActive(false);
                _previous.SetActive(true);
            }
            else if(_currentSlide == 0)
            {
                _next.SetActive(true);
                _previous.SetActive(false);
            }
            else
            {
                _next.SetActive(true);
                _previous.SetActive(true);
            }

            if (_currentSlide == 0)
            {
                _slider.DOLocalMoveX(-1 * _currentSlide * _slider.sizeDelta.x, 0.3f);
            }
            else
            {
                _slider.DOLocalMoveX(-1 * _currentSlide * _slider.sizeDelta.x - 100f, 0.3f);
            }

            foreach (Transform child in _bullets)
            {
				_sprites.TryGetValue("Tactical_Icon_SlideOff", out lSprite);
                child.gameObject.GetComponent<Image>().sprite	=	lSprite;
            }

            if(_currentSlide == 0)
            {
                _tacticalUI.SliderName = "offensive";
                _titleOffensive.transform.GetChild(0).gameObject.SetActive(false);
                _titleOffensive.transform.GetChild(1).gameObject.SetActive(true);
                _titleDefensive.transform.GetChild(0).gameObject.SetActive(true);
                _titleDefensive.transform.GetChild(1).gameObject.SetActive(false);
            }
            else
            {
                _tacticalUI.SliderName = "defensive";
                _titleOffensive.transform.GetChild(0).gameObject.SetActive(true);
                _titleOffensive.transform.GetChild(1).gameObject.SetActive(false);
                _titleDefensive.transform.GetChild(0).gameObject.SetActive(false);
                _titleDefensive.transform.GetChild(1).gameObject.SetActive(true);
            }
            
            _tacticalUI.TacticalViewA.OnFocusSquadPosition();

			_sprites.TryGetValue("Tactical_Icon_SlideOn", out lSprite);
            _bullets.GetChild(_currentSlide).gameObject.GetComponent<Image>().sprite	=	lSprite;
        }

        #endregion
    }
}
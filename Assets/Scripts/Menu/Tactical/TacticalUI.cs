﻿using Sportfaction.CasualFantasy.Action;
using Sportfaction.CasualFantasy.Composition;
using Sportfaction.CasualFantasy.Menu;
using Sportfaction.CasualFantasy.Shop;
using Sportfaction.CasualFantasy.Squad;

using UnityEngine;

namespace Sportfaction.CasualFantasy.Tactical
{
    /// <summary>
    /// Description: TacticalUI
    /// Author: Antoine de Lachèze-Murel
    /// </summary>
    public class TacticalUI : MonoBehaviour
    {
        #region Properties

        public GameObject CanvasGroup { get { return _canvasGroup; } }

        public ShopMain ShopMain { get { return _shopMain; } }

        public SquadMain SquadMain { get { return _squadMain; } }

        public ActionMain ActionMain { get { return _actionMain; } }

        public DefaultUI DefaultUI { get { return _defaultUI; } }

        public CompositionData Composition { get { return _composition; } }

        public TacticalViewA TacticalViewA { get { return _tacticalViewA; } }

        public TacticalViewB TacticalViewB { get { return _tacticalViewB; } }


        public string SquadPositionSelected { get { return _squadPositionSelected; } set { _squadPositionSelected = value; } }

        public ActionsItemData ActionSelected { get { return _actionSelected; } set { _actionSelected = value; } }

        public ActionsItemData ActionToBuy { get { return _actionToBuy; } set { _actionToBuy = value; } }

        public string SliderName { get { return _sliderName; } set { _sliderName = value; } }

        #endregion

        #region Fields

        [Header("Links")]
        [SerializeField, Tooltip("Squad Main")]
        private SquadMain _squadMain = null;

        [SerializeField, Tooltip("Squad Main")]
        private ShopMain _shopMain = null;

        [SerializeField, Tooltip("Action Main")]
        private ActionMain _actionMain = null;

        [SerializeField, Tooltip("Tactical View A")]
        private TacticalViewA _tacticalViewA = null;

        [SerializeField, Tooltip("Tactical View B")]
        private TacticalViewB _tacticalViewB = null;

        [SerializeField, Tooltip("Tactical Popin")]
        private TacticalPopin _tacticalPopin = null;

        [SerializeField, Tooltip("Default UI")]
        private DefaultUI _defaultUI = null;

        [Header("UI")]
        [SerializeField, Tooltip("Canvas Group")]
        private GameObject _canvasGroup = null;

        [SerializeField, Tooltip("View A")]
        private GameObject _viewA = null;

        [SerializeField, Tooltip("View B")]
        private GameObject _viewB = null;

        [SerializeField, Tooltip("Infos")]
        private GameObject _infos = null;

        [Header("Data")]
        [SerializeField, ReadOnly, Tooltip("Squad Position Selected")]
        private string _squadPositionSelected = "";

        [SerializeField, ReadOnly, Tooltip("Action Selected")]
        private ActionsItemData _actionSelected = null;

        [SerializeField, ReadOnly, Tooltip("Action to Buy")]
        private ActionsItemData _actionToBuy = null;

        [SerializeField, Tooltip("Composition")]
        private CompositionData _composition = null;

        [SerializeField, Tooltip("_sliderName")]
        private string _sliderName = "offensive";

        //private	bool	_onGetTacticalActionsEvent	=	false;
        //private	bool	_onGetListMemberCardsEvent	=	false;

        #endregion

        #region Public Methods

        public  void    OnActionPurchaseSucceed()
        {
            _tacticalPopin.ClosePopin();

            //_actionMain.GetTacticalMemberActions();

            _defaultUI.EventSystem.SetActive(true);
        }

        public  void    OnActionPurchaseFailed()
        {
            _defaultUI.EventSystem.SetActive(true);
        }

        public  void    OnGetTacticalMemberActionsEvent()
        {
            _tacticalViewB.StartViewB();

            _tacticalViewA.InitViewA();

        }

        public  void    GoToViewA()
        {
            Reset();

            _defaultUI.AnimateCanevasIn(_viewA);

            _tacticalViewA.InitViewA();
        }

        public  void    GoToViewB()
        {
            _defaultUI.AnimateCanevasInOut(_viewB, _infos, 0.3f);

            _tacticalViewB.StartViewB();
        }

        public  void    OpenPopin()
        {
            _tacticalPopin.InitPopin();
        }

        public  void    StartAnimationBuyAction(string pKeyNameAction)
        {
            _tacticalViewA.StartAnimationBuyAction(pKeyNameAction);
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Reset tactical screen
        /// </summary>
        private void    Reset()
        {
            _actionSelected =   null;
            _actionToBuy    =   null;
            _squadPositionSelected  =   "";
            
            _viewB.GetComponent<CanvasGroup>().alpha = 0;
            _viewB.SetActive(false);
            _infos.SetActive(true);
            _infos.GetComponent<CanvasGroup>().alpha = 1;
        }

        #endregion
    }
}
﻿using Sportfaction.CasualFantasy.Action;
using Sportfaction.CasualFantasy.Menu;
using Sportfaction.CasualFantasy.Services.Userstracking;

using DG.Tweening;
//using Facebook.Unity;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Assertions;

namespace Sportfaction.CasualFantasy.Tactical
{
    /// <summary>
    /// Description: TacticalUI
    /// Author: Antoine de Lachèze-Murel
    /// </summary>
    public class TacticalPointerEvent : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler, IPointerClickHandler
    {
        #region Fields

        [Header("Links")]
        [SerializeField, Tooltip("Tactical View B")]
        private TacticalViewB _tacticalViewB = null;
        [SerializeField, Tooltip("Tactical Popin")]
        private TacticalPopin _tacticalPopin = null;
        [SerializeField, Tooltip("Tactical UI")]
        private TacticalUI _tacticalUI = null;
        [SerializeField, Tooltip("PopinUI")]
        private PopinUI _popinUI = null;

        [Header("UI")]
        [SerializeField, Tooltip("_dragArea")]
        private RectTransform _dragArea;

        [SerializeField, Tooltip("_goDraggable")]
        private RectTransform _goDraggable = null;

        [SerializeField, ReadOnly, Tooltip("_beginDrag")]
        private Vector2 _beginDrag;

        [SerializeField, ReadOnly, Tooltip("_xOffset")]
        private float _xOffset;

        [SerializeField, ReadOnly, Tooltip("_xOffsetBeginDrag")]
        private float _xOffsetBeginDrag;

        [Header("Data")]
        [SerializeField, Tooltip("Scriptable object contains data about actions")]
        private ActionsData     _actionsData    =   null;
		#pragma warning disable 0414
        [SerializeField, ReadOnly, Tooltip("_onDrag")]
        private	bool	_onDrag	=	false;
		#pragma warning restore 0414

        #endregion

        #region Public Methods

        /// <summary>
        /// Is called when the user has pressed 
        /// the mouse button while
        /// over the GUIElement or Collider.
        /// </summary>
        /// <param name="pEventData">Event data</param>
        public  void    OnPointerClick(PointerEventData pEventData)
        {
            if (null != pEventData.pointerEnter)
            {
                switch (pEventData.pointerEnter.transform.name)
                {
                    case "[Previous]":
                        _tacticalViewB.Previous();
                        break;
                    case "[Next]":
                        _tacticalViewB.Next();
                        break;
                    case "[Defensive Title]":
                        _tacticalViewB.Next();
                        break;
                    case "[Offensive Title]":
                        _tacticalViewB.Previous();
                        break;
                    case "[Upgrade-off]":
                        _popinUI.NotEnoughSoft();
                        break;
                    case "[Upgrade] (Image)":

                        UserEventsManager.Instance.RecordEvent(new UserActionEvent("menu_tactical_" + _tacticalUI.ActionToBuy.KeyName + "_upgrade_confirm"));

                        _tacticalPopin.BuyActionWithSoft(_tacticalUI.ActionToBuy);
                        break;
                    case "[Cancel] (Text)":
                        _tacticalPopin.ClosePopin();
                        break;
                    default:
                        string lName = pEventData.pointerEnter.transform.name;
                        if (lName.Contains("tactical-pos-"))
                        {
                            string lPos = lName.Substring(13, lName.Length - 13);
                            _tacticalUI.SquadPositionSelected = lPos;
                            _tacticalUI.TacticalViewA.OnFocusSquadPosition();
                            _tacticalUI.GoToViewB();

                            UserEventsManager.Instance.RecordEvent(new UserActionEvent("menu_tactical_" + lPos));

                        }
                        else if (lName.Contains("action-"))
                        {
                            string  lActionName =   lName.Substring(7, lName.Length - 7);

                            ActionsItemData lActionItem =   _actionsData.GetActionByKeyName(lActionName);
                            _tacticalUI.ActionSelected  =   lActionItem;

                            string  lKeyName    =   _tacticalUI.ActionSelected.KeyName.Remove(_tacticalUI.ActionSelected.KeyName.Length - 1, 1);
                            lKeyName    =   lKeyName + "1";

							Assert.IsNotNull(_actionsData, "[TacticalPointerEvent] OnPointerClick(), _actionsData is null.");

                            ActionsItemData lAction = _actionsData.Items.Find(item => item.KeyName == lKeyName);

                            if (lAction != null)
                            {
                                UserEventsManager.Instance.RecordEvent(new UserActionEvent("menu_tactical_" + lActionName + "_upgrade"));

                                _tacticalUI.ActionToBuy = lAction;
                                _tacticalUI.OpenPopin();
                            }
                        }
                        break;
                }
            }
        }

        /// <summary>
        /// On the begin drag
        /// </summary>
        /// <param name="pEventData">Event data</param>
        public  void    OnBeginDrag(PointerEventData pEventData)
        {
            _onDrag = true;
            _xOffsetBeginDrag = _goDraggable.localPosition.x;
            RectTransformUtility.ScreenPointToLocalPointInRectangle(_dragArea, pEventData.position, pEventData.pressEventCamera, out _beginDrag);
        }

        /// <summary>
        /// On the drag
        /// </summary>
        /// <param name="pEventData">Event data</param>
        public  void    OnDrag(PointerEventData pEventData)
        {
            Vector2 lCurrentDrag;
            if (RectTransformUtility.ScreenPointToLocalPointInRectangle(_dragArea, pEventData.position, pEventData.pressEventCamera, out lCurrentDrag))
            {
                _xOffset = lCurrentDrag.x - _beginDrag.x;

                _goDraggable.localPosition = new Vector3(_xOffsetBeginDrag + _xOffset, _goDraggable.localPosition.y, _goDraggable.localPosition.z);
            }
        }

        /// <summary>
        /// On the end drag
        /// </summary>
        /// <param name="pEventData">Event data</param>
        public  void    OnEndDrag(PointerEventData pEventData)
        {
            _onDrag = false;
            if (_xOffset > 0 && Mathf.Abs(_xOffset) > _goDraggable.sizeDelta.x / 4)
            {
                if (_tacticalViewB.CurrentSlide > 0)
                {
                    _tacticalViewB.Previous();
                }
                else
                {
                    _goDraggable.GetComponent<RectTransform>().DOLocalMoveX(_xOffsetBeginDrag, 0.3f);
                }
            }
            else if (_xOffset < 0 && Mathf.Abs(_xOffset) > _goDraggable.sizeDelta.x / 4)
            {
                if (_tacticalViewB.CurrentSlide < _goDraggable.gameObject.transform.childCount - 1)
                {
                    _tacticalViewB.Next();
                }
                else
                {
                    _goDraggable.GetComponent<RectTransform>().DOLocalMoveX(_xOffsetBeginDrag, 0.3f);
                }
            }
            else
            {
                _goDraggable.GetComponent<RectTransform>().DOLocalMoveX(_xOffsetBeginDrag, 0.3f);
            }
        }

        #endregion

    }
}
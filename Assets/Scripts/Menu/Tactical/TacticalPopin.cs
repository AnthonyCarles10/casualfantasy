﻿using Sportfaction.CasualFantasy.Menu;
using Sportfaction.CasualFantasy.Action;
using Sportfaction.CasualFantasy.Services.Userstracking;
using Sportfaction.CasualFantasy.Shop;

using DG.Tweening;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;

namespace Sportfaction.CasualFantasy.Tactical
{
    /// <summary>
    /// Description: TacticalUI
    /// Author: Antoine de Lachèze-Murel
    /// </summary>
    public class TacticalPopin : MonoBehaviour
    {
        #region Fields

		[SerializeField, Tooltip("Scriptable object contains data about manage's product")]
		private	ProductData		_productData	=	null;

        [Header("Links")]


        [SerializeField, Tooltip("Tactical UI")]
        private TacticalUI _tacticalUI = null;

        [SerializeField, Tooltip("Default UI")]
        private DefaultUI _defaultUI = null;
   
        [Header("UI")]

        [SerializeField, Tooltip("Popin")]
        private GameObject _popin = null;

        [SerializeField, Tooltip("Body")]
        private GameObject _body = null;

        //[SerializeField, Tooltip("_slotA")]
        //private	GameObject	_slotA	=	null;

        [SerializeField, Tooltip("_slotNameA")]
        private Text _slotNameA = null;

        [SerializeField, Tooltip("_slotLevelA")]
        private Text _slotLevelA = null;

        [SerializeField, Tooltip("_slotCriticalA")]
        private Text _slotCriticalA = null;

        [SerializeField, Tooltip("_slotLogoA")]
        private Image _slotLogoA = null;

        [SerializeField, Tooltip("_slotShifumiA")]
        private Image _slotShifumiA = null;

        //[SerializeField, Tooltip("_slotB")]
        //private	GameObject	_slotB	=	null;

        [SerializeField, Tooltip("_slotNameB")]
        private Text _slotNameB = null;

        [SerializeField, Tooltip("_slotLevelB")]
        private Text _slotLevelB = null;

        [SerializeField, Tooltip("_slotCriticalB")]
        private Text _slotCriticalB = null;

        [SerializeField, Tooltip("_slotLogoB")]
        private Image _slotLogoB = null;

        [SerializeField, Tooltip("_slotShifumiB")]
        private Image _slotShifumiB= null;

        [SerializeField, Tooltip("_soft")]
        private Text _soft = null;

        [SerializeField, Tooltip("BackButton")]
        private GameObject _backButton = null;

        //[Header("Data")]
        //[SerializeField, Tooltip("ActionMain")]
        //private	ActionMain	_cardMain	=	null;

        #endregion

        #region Public Methods

        public void InitPopin()
        {
            ActionsItemData _actionA = _tacticalUI.ActionSelected;

           // _title.text = "Améliorer action " + I2.Loc.LocalizationManager.GetTranslation(_actionA.KeyName + "Title");

            _slotNameA.text = I2.Loc.LocalizationManager.GetTranslation(_actionA.KeyName + "Title");
            _slotCriticalA.text = I2.Loc.LocalizationManager.GetTranslation("Critical") + " : " + _actionA.CriticalRate.ToString() + "%";
            //_slotA.transform.GetChild(0).gameObject.SetActive(false);
            //_slotA.transform.GetChild(1).gameObject.SetActive(true);

			Assert.IsNotNull(_productData, "[TacticalPopin] InitPopin(), _productData is null.");

            ProductItemData lProduct = _productData.Items.Find(item => item.Reference == "com.sportfaction.action." + _tacticalUI.ActionToBuy.KeyName);
            _soft.text = lProduct.PriceSoft.ToString();
            _slotNameB.text = I2.Loc.LocalizationManager.GetTranslation(_tacticalUI.ActionToBuy.KeyName + "Title");
            _slotLevelB.text = "Nv 1";
            _slotCriticalB.text = I2.Loc.LocalizationManager.GetTranslation("Critical") + " : "+ _tacticalUI.ActionToBuy.CriticalRate.ToString() + "%";
            //_slotB.transform.GetChild(0).gameObject.SetActive(false);
            //_slotB.transform.GetChild(1).gameObject.SetActive(true);

            OpenPopin();
        }

        public void ClosePopin()
        {
            Sequence lSequence = DOTween.Sequence();
            lSequence.Append(_body.GetComponent<RectTransform>().DOScale(0, 0.3f));
            lSequence.Insert(0.1f, _popin.GetComponent<CanvasGroup>().DOFade(0f, 0.2f));

            StartCoroutine(_defaultUI.ChangeStatusGameObject(_popin, false, 0.5f));

            _defaultUI.AnimateCanevasIn(_backButton, 0.1f);
        }

        #endregion

        #region Private Methods

        private void OpenPopin()
        {
            _defaultUI.AnimateCanevasOut(_backButton, 0.1f);

            _popin.SetActive(true);
            Sequence lSequence = DOTween.Sequence();
            lSequence.Append(_popin.GetComponent<CanvasGroup>().DOFade(1f, 0.2f));
            lSequence.Insert(0.1f, _body.GetComponent<RectTransform>().DOScale(1f, 0.3f));
        }

       

        public void BuyActionWithSoft(ActionsItemData pItem)
        {
            //string[] m_buttonsArray = new string[] { I2.Loc.LocalizationManager.GetTranslation("No"), I2.Loc.LocalizationManager.GetTranslation("Yes") };
            object lAdditonalData = new
            {
                squadPosition = _tacticalUI.SquadPositionSelected
            };

            UserEventsManager.Instance.RecordEvent(new UserActionEvent("buy_product_" + pItem.KeyName + "_" + _tacticalUI.SquadPositionSelected));
           // _tacticalUI.ShopMain.BuyProduct("com.sportfaction.action." + pItem.KeyName, "Action", lAdditonalData);
            _defaultUI.EventSystem.SetActive(false);

            _tacticalUI.StartAnimationBuyAction(pItem.KeyName);

            //AlertDialogs.ShowAlert(
            //     I2.Loc.LocalizationManager.GetTranslation("PurchaseActionTitle"),
            //     I2.Loc.LocalizationManager.GetTranslation("PurchaseActionMessage") + " " + I2.Loc.LocalizationManager.GetTranslation(pItem.KeyName + "Title"),
            //    m_buttonsArray,
            //    (_buttonPressed) => {

            //        if (_buttonPressed == I2.Loc.LocalizationManager.GetTranslation("Yes"))
            //        {
            //            Debug.Log("com.sportfaction.action." + pItem.KeyName);
            //            // Tracking
            //             UserEventsManager.Instance.RecordEvent(new UserActionEvent("buy_product_" + pItem.KeyName + "_" + _tacticalUI.SquadPositionSelected));
            //            _tacticalUI.ShopMain.BuyProduct("com.sportfaction.action." + pItem.KeyName, "Action", lAdditonalData);
            //        }
            //    }
            //);
        }


        #endregion

    }
}
﻿using Sportfaction.CasualFantasy.Action;

using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace Sportfaction.CasualFantasy.Tactical
{
    /// <summary>
    /// Description: TacticalPositionPrefabViewA
    /// Author: Antoine de Lachèze-Murel
    /// </summary>
    public class TacticalPositionPrefabViewA : MonoBehaviour
    {
        #region Fields

        [Header("Links")]
        [SerializeField, Tooltip("Tactical UI")]
        private TacticalUI  _tacticalUI =   null;

        [Header("UI")]
        [SerializeField, Tooltip("Squad Position")]
        private Text        _squadPos   =   null;
        [SerializeField, Tooltip("_offensives")]
        private GameObject  _offensives =   null;
        [SerializeField, Tooltip("_defensives")]
        private GameObject  _defensives =   null;

        [Header("Data")]
        [SerializeField, Tooltip("_squadPosition")]
        private string      _squadPosition      =   null;
        [SerializeField, Tooltip("Database of every action data")]
        private ActionsData _actionsDatabase    =   null; //!< Database of every action data

        private Color   _color;

        #endregion

        #region Public Methods

        /// <summary>
        /// Initializes squad position infos
        /// </summary>
        public  void    Init()
        {
            _squadPos.text  =   I2.Loc.LocalizationManager.GetTranslation(_squadPosition); ;

            //InitOffensives();
            //InitDefensives();
        }

        /// <summary>
        /// Updates selected position actions
        /// </summary>
        public  void    UpdateActive()
        {
            //InitOffensives();
            //InitDefensives();

            if (_tacticalUI.SquadPositionSelected == _squadPosition && _tacticalUI.SliderName == "offensive")
            {
                _offensives.GetComponent<RectTransform>().DOScale(1.4f, 0.3f);
                foreach (Transform child in _offensives.transform)
                {
                    ColorUtility.TryParseHtmlString("#FFFFFFFF", out _color);
                    child.transform.GetChild(0).GetComponent<Image>().color = _color;
                    if(null != child.transform.GetChild(0).transform.GetChild(0))
                    {
                        child.transform.GetChild(0).transform.GetChild(0).gameObject.SetActive(true);
                    }
                }
            }
            else
            {
                _offensives.GetComponent<RectTransform>().DOScale(1f, 0.3f);
                foreach (Transform child in _offensives.transform)
                {
                    ColorUtility.TryParseHtmlString("#858585FF", out _color);
                    child.transform.GetChild(0).GetComponent<Image>().color = _color;
                    if (null != child.transform.GetChild(0).transform.GetChild(0))
                    {
                        child.transform.GetChild(0).transform.GetChild(0).gameObject.SetActive(false);
                    }
                }
            }

            if (_tacticalUI.SquadPositionSelected == _squadPosition && _tacticalUI.SliderName == "defensive")
            {
                _defensives.GetComponent<RectTransform>().DOScale(1.4f, 0.3f);
                foreach (Transform child in _defensives.transform)
                {
                    ColorUtility.TryParseHtmlString("#FFFFFFFF", out _color);
                    child.transform.GetChild(0).GetComponent<Image>().color = _color;
                    if (null != child.transform.GetChild(0).transform.GetChild(0))
                    {
                        child.transform.GetChild(0).transform.GetChild(0).gameObject.SetActive(true);
                    }
                }
            }
            else
            {
                _defensives.GetComponent<RectTransform>().DOScale(1f, 0.3f);
                foreach (Transform child in _defensives.transform)
                {
                    ColorUtility.TryParseHtmlString("#858585FF", out _color);
                    child.transform.GetChild(0).GetComponent<Image>().color = _color;
                    if (null != child.transform.GetChild(0).transform.GetChild(0))
                    {
                        child.transform.GetChild(0).transform.GetChild(0).gameObject.SetActive(false);
                    }
                }
            }

        }

        /// <summary>
        /// Order canvas' sorting order
        /// </summary>
        /// <param name="pSort">Sorting order to set</param>
        public  void    OrderInLayer(int pSort)
        {
            this.GetComponent<Canvas>().sortingOrder = pSort;
        }

        public  void    StartAnimationBuyAction(string pActionKeyName)
        {
            /*string lActionName = pActionKeyName.Remove(pActionKeyName.Length - 1, 1);

			Assert.IsNotNull(_actionsData, "[TacticalPositionPrefabViewA] StartAnimationBuyAction(), _actionsData is null.");

            var lQuery =
              from tacticalAction in _tacticalMemberActions.Items
              join action in _actionsData.Items on tacticalAction.action_id equals action.Id
              where tacticalAction.type == "A" && tacticalAction.full_position == _squadPosition
              orderby action.StackOrder
              select new { TacticalAction = tacticalAction, Action = action };


            List<TacticalMemberItemActions> lTacticalActions = new List<TacticalMemberItemActions>();
            List<ActionsItemData> lActionsItemData = new List<ActionsItemData>();

            foreach (var result in lQuery)
            {
                lTacticalActions.Add(result.TacticalAction);
                lActionsItemData.Add(result.Action);
            }

            int lIndex = 0;
            foreach (Transform child in _offensives.transform)
            {
                if (lIndex < lActionsItemData.Count)
                {
                    string lActionNameChild = lActionsItemData[lIndex].KeyName.Remove(lActionsItemData[lIndex].KeyName.Length - 1, 1);
                    if (lActionNameChild == lActionName)
                    {
                        child.transform.GetChild(2).GetComponent<ParticleSystem>().Play();
                    }
                }

                lIndex++;
            }

             lQuery =
              from tacticalAction in _tacticalMemberActions.Items
              join action in _actionsData.Items on tacticalAction.action_id equals action.Id
              where tacticalAction.type == "D" && tacticalAction.full_position == _squadPosition
              orderby action.StackOrder
              select new { TacticalAction = tacticalAction, Action = action };

            lTacticalActions = new List<TacticalMemberItemActions>();
            lActionsItemData = new List<ActionsItemData>();

            foreach (var result in lQuery)
            {
                lTacticalActions.Add(result.TacticalAction);
                lActionsItemData.Add(result.Action);
            }

            lIndex = 0;
            foreach (Transform child in _defensives.transform)
            {
                if (lIndex < lActionsItemData.Count)
                {
                    string lActionNameChild = lActionsItemData[lIndex].KeyName.Remove(lActionsItemData[lIndex].KeyName.Length - 1, 1);
                    if (lActionNameChild == lActionName)
                    {
                        child.transform.GetChild(2).GetComponent<ParticleSystem>().Play();
                    }
                }

                lIndex++;
            }*/
        }

        #endregion

        #region Private Methods

        /*private void    InitOffensives()
        {
			Assert.IsNotNull(_actionsData, "[TacticalPositionPrefabViewA] InitOffensives(), _actionsData is null.");

			Assert.IsNotNull(_tacticalMemberActions, "[TacticalPositionPrefabViewA] InitOffensives(), _tacticalMemberActions is null.");

            var lQuery =
              from tacticalAction in _tacticalMemberActions.Items
              join action in _actionsData.Items on tacticalAction.action_id equals action.Id
              where tacticalAction.type == "A" && tacticalAction.full_position == _squadPosition
              orderby action.StackOrder
              select new { TacticalAction = tacticalAction, Action = action };


            List<TacticalMemberItemActions> lTacticalActions = new List<TacticalMemberItemActions>();
            List<ActionsItemData> lActionsItemData = new List<ActionsItemData>();

            foreach (var result in lQuery)
            {
                lTacticalActions.Add(result.TacticalAction);
                lActionsItemData.Add(result.Action);
            }


            int lIndex = 0;
            foreach (Transform child in _offensives.transform)
            {
                if(lIndex < lActionsItemData.Count)
				{

					Assert.IsNotNull(UserData.Instance, "[TacticalPositionPrefabViewA] InitOffensive(), UserData.Instance is null.");

					Assert.IsNotNull(UserData.Instance.Profile, "[TacticalPositionPrefabViewA] InitOffensive(), UserData.Instance.Profile is null.");

                    if (UserData.Instance.Profile.Level >= lActionsItemData[lIndex].LevelToUnlock && int.Parse(UserData.Instance.Profile.Soft) >= lActionsItemData[lIndex].Cost)
                    {
                        child.transform.GetChild(1).gameObject.SetActive(true);
                    }
                    else
                    {
                        child.transform.GetChild(1).gameObject.SetActive(false);
                    }

                }
                else
                {
                    child.gameObject.SetActive(false);
                }


                lIndex++;
            }
        }

        private void    InitDefensives()
        {
			Assert.IsNotNull(_actionsData, "[TacticalPositionPrefabViewA] InitDefensives(), _actionsData is null.");

			Assert.IsNotNull(_tacticalMemberActions, "[TacticalPositionPrefabViewA] InitDefensives(), _tacticalMemberActions is null.");
            var lQuery =
              from tacticalAction in _tacticalMemberActions.Items
              join action in _actionsData.Items on tacticalAction.action_id equals action.Id
              where tacticalAction.type == "D" && tacticalAction.full_position == _squadPosition
              orderby action.StackOrder
              select new { TacticalAction = tacticalAction, Action = action };


            List<TacticalMemberItemActions> lTacticalActions = new List<TacticalMemberItemActions>();
            List<ActionsItemData> lActionsItemData = new List<ActionsItemData>();

            foreach (var result in lQuery)
            {
                lTacticalActions.Add(result.TacticalAction);
                lActionsItemData.Add(result.Action);
            }

            int lIndex = 0;
            foreach (Transform child in _defensives.transform)
            {

                if (lIndex < lActionsItemData.Count)
                {
					Assert.IsNotNull(UserData.Instance, "[TacticalPositionPrefabViewA] InitDefensives(), UserData.Instance is null.");

					Assert.IsNotNull(UserData.Instance.Profile, "[TacticalPositionPrefabViewA] InitDefensives(), UserData.Instance.Profile is null.");

                    if (UserData.Instance.Profile.Level >= lActionsItemData[lIndex].LevelToUnlock && int.Parse(UserData.Instance.Profile.Soft) >= lActionsItemData[lIndex].Cost )
                    {
                        child.transform.GetChild(1).gameObject.SetActive(true);
                    }
                    else
                    {
                        child.transform.GetChild(1).gameObject.SetActive(false);
                    }
                }
                else
                {
                    child.gameObject.SetActive(false);
                }

                lIndex++;
            }
        }*/
        
        #endregion
    }
}
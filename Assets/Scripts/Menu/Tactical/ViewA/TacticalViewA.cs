﻿using Sportfaction.CasualFantasy.Action;

using UnityEngine;

namespace Sportfaction.CasualFantasy.Tactical
{
    /// <summary>
    /// Description: TacticalViewA
    /// Author: Antoine de Lachèze-Murel
    /// </summary>
    public class TacticalViewA : MonoBehaviour
    {
        #region Fields

        [Header("Links")]
        [SerializeField, Tooltip("Tactical UI")]
        private TacticalUI  _tacticalUI =   null;

        [Header("UI")]
        [SerializeField, Tooltip("G Parent")]
        private	GameObject	_gParent	=	null;
        [SerializeField, Tooltip("D Parent")]
        private	GameObject	_dParent	=	null;
        [SerializeField, Tooltip("M Parent")]
        private	GameObject	_mParent	=	null;
        [SerializeField, Tooltip("A Parent")]
        private	GameObject	_aParent	=	null;
		
		[Header("Data")]
        [SerializeField, Tooltip("Database of every action data")]
        private ActionsData _actionsDatabase    =   null; //!< Database of every action data

        #endregion

        #region Public Methods

        /// <summary>
        /// Initializes view A
        /// </summary>
        public  void    InitViewA()
        {
            foreach (Transform child in _gParent.transform)
            {
                if (_tacticalUI.SquadPositionSelected == child.gameObject.name)
                {
                    child.transform.GetChild(0).gameObject.SetActive(false);
                    child.transform.GetChild(1).gameObject.SetActive(true);
                }

                child.GetComponent<TacticalPositionPrefabViewA>().Init();
            }

            foreach (Transform child in _dParent.transform)
            {
                if (_tacticalUI.SquadPositionSelected == child.gameObject.name)
                {
                    child.transform.GetChild(0).gameObject.SetActive(false);
                    child.transform.GetChild(1).gameObject.SetActive(true);
                }

                child.GetComponent<TacticalPositionPrefabViewA>().Init();
            }

            foreach (Transform child in _mParent.transform)
            {
                if (_tacticalUI.SquadPositionSelected == child.gameObject.name)
                {
                    child.transform.GetChild(0).gameObject.SetActive(false);
                    child.transform.GetChild(1).gameObject.SetActive(true);
                }

                child.GetComponent<TacticalPositionPrefabViewA>().Init();
            }

            foreach (Transform child in _aParent.transform)
            {
                if (_tacticalUI.SquadPositionSelected == child.gameObject.name)
                {
                    child.transform.GetChild(0).gameObject.SetActive(false);
                    child.transform.GetChild(1).gameObject.SetActive(true);
                }

                child.GetComponent<TacticalPositionPrefabViewA>().Init();
            }
        }

        /// <summary>
        /// Ons the focus squad position
        /// </summary>
        public  void    OnFocusSquadPosition()
        {
            foreach (Transform child in _gParent.transform)
            {
                child.GetComponent<TacticalPositionPrefabViewA>().UpdateActive();

                if ("tactical-pos-" +  _tacticalUI.SquadPositionSelected == child.gameObject.name)
                {
                    child.transform.GetChild(0).gameObject.SetActive(false);
                    child.transform.GetChild(1).gameObject.SetActive(true);
                    child.GetComponent<TacticalPositionPrefabViewA>().OrderInLayer(2);
                }
                else
                {
                    child.transform.GetChild(0).gameObject.SetActive(true);
                    child.transform.GetChild(1).gameObject.SetActive(false);
                    child.GetComponent<TacticalPositionPrefabViewA>().OrderInLayer(1);
                }
            }

            foreach (Transform child in _dParent.transform)
            {
                child.GetComponent<TacticalPositionPrefabViewA>().UpdateActive();

                if ("tactical-pos-" + _tacticalUI.SquadPositionSelected == child.gameObject.name)
                {
                    child.transform.GetChild(0).gameObject.SetActive(false);
                    child.transform.GetChild(1).gameObject.SetActive(true);
                    child.GetComponent<TacticalPositionPrefabViewA>().OrderInLayer(2);
                }
                else
                {
                    child.transform.GetChild(0).gameObject.SetActive(true);
                    child.transform.GetChild(1).gameObject.SetActive(false);
                    child.GetComponent<TacticalPositionPrefabViewA>().OrderInLayer(1);
                }
            }

            foreach (Transform child in _mParent.transform)
            {
                child.GetComponent<TacticalPositionPrefabViewA>().UpdateActive();

                if ("tactical-pos-" + _tacticalUI.SquadPositionSelected == child.gameObject.name)
                {
                    child.transform.GetChild(0).gameObject.SetActive(false);
                    child.transform.GetChild(1).gameObject.SetActive(true);
                    child.GetComponent<TacticalPositionPrefabViewA>().OrderInLayer(2);
                }
                else
                {
                    child.transform.GetChild(0).gameObject.SetActive(true);
                    child.transform.GetChild(1).gameObject.SetActive(false);
                    child.GetComponent<TacticalPositionPrefabViewA>().OrderInLayer(1);
                }
            }

            foreach (Transform child in _aParent.transform)
            {
                child.GetComponent<TacticalPositionPrefabViewA>().UpdateActive();

                if ("tactical-pos-" + _tacticalUI.SquadPositionSelected == child.gameObject.name)
                {
                    child.transform.GetChild(0).gameObject.SetActive(false);
                    child.transform.GetChild(1).gameObject.SetActive(true);
                    child.GetComponent<TacticalPositionPrefabViewA>().OrderInLayer(2);
                }
                else
                {
                    child.transform.GetChild(0).gameObject.SetActive(true);
                    child.transform.GetChild(1).gameObject.SetActive(false);
                    child.GetComponent<TacticalPositionPrefabViewA>().OrderInLayer(1);
                }
            }
        }

        /// <summary>
        /// Starts the view A
        /// </summary>
        public  void    StartViewA()
        {
            foreach (Transform child in _gParent.transform)
            {
                child.transform.GetChild(0).gameObject.SetActive(true);
                child.transform.GetChild(1).gameObject.SetActive(false);
            }

            foreach (Transform child in _dParent.transform)
            {
                child.transform.GetChild(0).gameObject.SetActive(true);
                child.transform.GetChild(1).gameObject.SetActive(false);
            }

            foreach (Transform child in _mParent.transform)
            {
                child.transform.GetChild(0).gameObject.SetActive(true);
                child.transform.GetChild(1).gameObject.SetActive(false);
            }

            foreach (Transform child in _aParent.transform)
            {
                child.transform.GetChild(0).gameObject.SetActive(true);
                child.transform.GetChild(1).gameObject.SetActive(false);
            }
        }

        /// <summary>
        /// Starts buy action animation
        /// </summary>
        /// <param name="pKeyNameAction">Action title</param>
        public  void    StartAnimationBuyAction(string pKeyNameAction)
        {
            foreach (Transform child in _gParent.transform)
            {
                if ("tactical-pos-" + _tacticalUI.SquadPositionSelected == child.gameObject.name)
                {
                    child.GetComponent<TacticalPositionPrefabViewA>().StartAnimationBuyAction(pKeyNameAction);
                }
            }

            foreach (Transform child in _dParent.transform)
            {
                if ("tactical-pos-" + _tacticalUI.SquadPositionSelected == child.gameObject.name)
                {
                    child.GetComponent<TacticalPositionPrefabViewA>().StartAnimationBuyAction(pKeyNameAction);
                }
            }

            foreach (Transform child in _mParent.transform)
            {
                if ("tactical-pos-" + _tacticalUI.SquadPositionSelected == child.gameObject.name)
                {
                    child.GetComponent<TacticalPositionPrefabViewA>().StartAnimationBuyAction(pKeyNameAction);
                }
            }

            foreach (Transform child in _aParent.transform)
            {
                if ("tactical-pos-" + _tacticalUI.SquadPositionSelected == child.gameObject.name)
                {
                    child.GetComponent<TacticalPositionPrefabViewA>().StartAnimationBuyAction(pKeyNameAction);
                }
            }
        }

        #endregion

        #region Private Methods
        
       /*private void InitAttack(Transform pOffensives)
        {
			Assert.IsNotNull(_actionsDatabase, "[TacticalViewA] InitAttack(), _actionsData is null.");

			Assert.IsNotNull(_tacticalMemberActions, "[TacticalViewA] InitAttack(), _tacticalMemberActions is null.");

            var lQuery =
                from tacticalAction in _tacticalMemberActions.Items
                join action in _actionsDatabase.Items on tacticalAction.action_id equals action.Id
                where tacticalAction.type == "A" && tacticalAction.full_position == _tacticalUI.SquadPositionSelected
                orderby action.StackOrder
                select new { TacticalAction = tacticalAction };
            //select new { TacticalAction = tacticalAction, Action = action };

            List<TacticalMemberItemActions> lTacticalActions = new List<TacticalMemberItemActions>();

            foreach (var result in lQuery)
            {
                lTacticalActions.Add(result.TacticalAction);
            }

            /*
			int lIndex = 0;
            for (int lSlotIndex = 0; lSlotIndex < 3; lSlotIndex++)
            {
                //pOffensives.GetChild(lSlotIndex)
                //TacticalMemberItemActions lTacticalAction = lTacticalActions.Find(item => int.Parse(item.slot) == pSlotIndex);
                //InitPrefab(lTacticalActions, _attDefautParent, lSlotIndex, lIndex);
                //lIndex++;
            }
        }*/
        
        #endregion
    }
}
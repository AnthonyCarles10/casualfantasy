using Sportfaction.CasualFantasy.Leaderboard;

using UnityEngine;
using UnityEngine.Assertions;

namespace Sportfaction.CasualFantasy.Menu
{
	/// <summary>
	///  Description : Handles Leaderboard UI display
	/// Author : Antoine Delachèze-Murel
	/// </summary>
    public class LeaderboardUI : MonoBehaviour
    {
        #region Fields

		[SerializeField, Tooltip("Scriptable object contains leaderboard data")]
		private	LeaderboardData		_leaderboardData	=	null;

        [Header("GameObjects")]

        [SerializeField, Tooltip("_bestPlayers")]
        private GameObject _bestPlayers = null;

        [SerializeField, Tooltip("_otherPlayers")]
        private GameObject _otherPlayers = null;

        [Header("UI")]

        [SerializeField, Tooltip("Canvas Group")]
        public GameObject CanvasGroup = null;


        #endregion

        #region Public Methods

        public	void	OnGetOverallRanking()
        {
            foreach(Transform child in _otherPlayers.transform)
            {
                child.gameObject.SetActive(false);
            }

			Assert.IsNotNull(_leaderboardData, "[LeaderboardUI] OnGetOverallRanking(), _leaderboardData is null.");

            for (int i = 0; i < 3; i++)
            {
                Transform lRaw = _bestPlayers.transform.GetChild(i);
                lRaw.GetComponent<LeaderboardRowPrefab>().InitLadderScoreData(_leaderboardData.LeaderboardScoreData[i]);
            }
            for (int i = 3; i < _leaderboardData.LeaderboardScoreData.Count; i++)
            {
                Transform lRaw = _otherPlayers.transform.GetChild(i - 3);
                lRaw.gameObject.SetActive(true);
                lRaw.GetComponent<LeaderboardRowPrefab>().InitLadderScoreData(_leaderboardData.LeaderboardScoreData[i]);
            }
        }

        #endregion

    }
}

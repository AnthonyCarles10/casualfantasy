﻿using Sportfaction.CasualFantasy.Services.Userstracking;
using Sportfaction.CasualFantasy.Manager.Profile;
using Sportfaction.CasualFantasy.PVP.Network;
using Sportfaction.CasualFantasy.Menu.Squad;
using Sportfaction.CasualFantasy.Events;
using Sportfaction.CasualFantasy.Config;
using Sportfaction.CasualFantasy.Menu;
using Sportfaction.CasualFantasy.PVP;

using DG.Tweening;
//using Facebook.Unity;
using Photon.Pun;
using System.Collections;
using Newtonsoft.Json.Linq;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;

namespace Sportfaction.CasualFantasy
{
    /// <summary>
    /// Description: MatchUI
    /// Author: Antoine de Lachèze-Murel
    /// </summary>
    public class MatchUI : MonoBehaviour
    {
        #region Fields

        [Header("Links")]

        //[SerializeField, Tooltip("PUN")]
        //private PUNManager _pun = null;

        [SerializeField, Tooltip("Squad UI")]
        private SquadUI _squadUI = null;

        [SerializeField, Tooltip("Default UI")]
        private DefaultUI _defaultUI = null;

        [SerializeField, Tooltip("AudioUI")]
        public AudioUI _audioUI = null;

        [Header("UI")]

        [SerializeField, Tooltip("CanvasGroup")]
        public GameObject CanvasGroup = null;

        [SerializeField, Tooltip("_avatarB")]
        private Image _avatarB = null;

        [SerializeField, Tooltip("_tshirtB")]
        private Image _tshirtB = null;

        [SerializeField, Tooltip("VS on")]
        private GameObject _vsOn = null;

        [SerializeField, Tooltip("VS off")]
        private GameObject _vsOff = null;

        [SerializeField, Tooltip("ok Button")]
        private GameObject _okButton = null;

        [SerializeField, Tooltip("cancel Button")]
        private GameObject _cancelButton = null;

        [SerializeField, Tooltip("InfosPun")]
        private GameObject _infosPUN = null;

        [SerializeField, Tooltip("InfosPun Text")]
        private TMPro.TMP_Text _infosPUNText = null;

        [SerializeField, Tooltip("Manager 1 Name")]
        private TMPro.TMP_Text _manager1Name = null;

        [SerializeField, Tooltip("Manager 2 Name")]
        private TMPro.TMP_Text _manager2Name = null;

        [SerializeField, Tooltip("BackMenu")]
        private GameObject _backMenu = null;

        [SerializeField, Tooltip("_newField")]
        private Image _currentField = null;

        [SerializeField, Tooltip("_currentFieldText")]
        private TMPro.TMP_Text _currentFieldText = null;

        [SerializeField, Tooltip("_newFieldText")]
        private TMPro.TMP_Text _newFieldText = null;

        [Header("Data")]
		[SerializeField, Tooltip("Scriptable object contains infos about match UI")]
		private	PVPData	_pvpData	=	null;

        [SerializeField, ReadOnly, Tooltip("Bots Pseudo")]
        private string _botsPseudo = "{ \"pseudos\":[\"Ceceg\", \"Gab\", \"Anto\", \"oioi\", \"Eddyth\", \"Kaligan\", \"Lord StClair\", \"zavier92\", \"oliv\", \"Mambo\", \"Xam Xam\",\"kcssp\", \"bracame\", \"Cab.\", \"sissou\", \"Craco\", \"Toniocourc\", \"McHillroyd\", \"philae\", \"AndrewTheKing\", \"Petilouis\",\"Doudou\", \"Captain Chaos\", \"Pitchou\", \"orphila\", \"labuche\", \"Bakugen\", \"Benat\", \"JovyB\", \"brasletti\", \"Winproof\",\"FlyinPoulpus\", \"Hitlight\", \"Jordijo\", \"hola987\", \"Marco\", \"Brigstone\", \"Erom\", \"julo2648\", \"Bel the bull\", \"noradus\",\"dovahkin\", \"bratch\", \"Samrock\", \"Doc Thor\", \"Pat56\", \"Wesker\", \"Azot\", \"KeppCool007\", \"Katim\", \"snobet76\"]}";

        [SerializeField, ReadOnly, Tooltip("Bots Pseudo Array")]
        string[] _botsPseudoArray = new string[] { };

        [Header("Game Events")]

        [SerializeField]
        private GameEvent _onFinishedPreMatchAnimation = null;

		private	bool	_launchSearch	=	true;
		
        #endregion

        #region Public Methods

        public void Play()
        {
            _okButton.SetActive(false);
            _cancelButton.SetActive(true);


            _vsOff.GetComponent<RectTransform>().DOScale(1, 0.3f);

            if (true == _squadUI.OnPlayGame())
            {

                _audioUI.OnClickSound();

				Assert.IsNotNull(_pvpData, "[MatchUI] Play(), _pvpData is null.");

                _pvpData.GameMode = ePVPGameMode.CLASSIC;
                // StadiumsItemData lStadium = _stadiumUI.Stadiums.Find(item => item.Name == "Volgograd");
                // Store.Instance.PVPData.StadiumSelected = lStadium;

                _infosPUNText.text = "";
                _defaultUI.AnimateCanevasIn(_infosPUN);


                //if (null == ConfigData.Instance || true == UserData.Instance.Profile.OnBoarding)
                //{
                //    _pun.LaunchInstantMatchAgainstBot();
                //}
                //else
                //{
                //    if (_launchSearch)
                //    {
                //        _pun.Search();
                //    }
                //}

                _okButton.SetActive(false);
                _cancelButton.SetActive(true);

                _defaultUI.AnimateCanevasOut(_backMenu, 0.3f);
            }
        }
    
        public void Cancel()
        {
            _audioUI.OnBackSound();
            //_pun.Cancel();
            _okButton.SetActive(true);
            _cancelButton.SetActive(false);
            _defaultUI.AnimateCanevasIn(_backMenu, 0.3f);
            _vsOff.GetComponent<RectTransform>().DOScale(0, 0.3f);
        }

        public void OnPUNRoomReady()
        {

            UserEventsManager.Instance.RecordEvent(new UserActionEvent("manager_match_started"));
          

            _cancelButton.SetActive(false);
			_launchSearch = false;

            //if (ePVPGameMode.CLASSIC == Store.Instance.PVPData.GameMode)
            //{
            //    Store.Instance.PVPData.TeamAPlayerPositionDB = Resources.Load<PlayerPositionDatabase>("PVP/Database/Positions 4-4-2");
            //    Store.Instance.PVPData.TeamBPlayerPositionDB = Resources.Load<PlayerPositionDatabase>("PVP/Database/Positions 4-4-2");
            //}
            //else
            //{
            //    Store.Instance.PVPData.TeamAPlayerPositionDB = Resources.Load<PlayerPositionDatabase>("PVP/Database/Positions 4-4-2");
            //    Store.Instance.PVPData.TeamBPlayerPositionDB = Resources.Load<PlayerPositionDatabase>("PVP/Database/Positions 4-4-2");
            //}

            if (true == PhotonNetwork.IsConnected && true == PhotonNetwork.InRoom)
            {
                _manager1Name.text = PhotonNetwork.LocalPlayer.NickName;
                _manager2Name.text = PhotonNetwork.PlayerListOthers[0].NickName;

                UserEventsManager.Instance.RecordEvent(new UserActionEvent("manager_match_pvp_started"));
            }
            else
            {
                Debug.Log("Play with a bot");
                SetBotPseudo();

				Assert.IsNotNull(UserData.Instance, "[MatchUI] OnPUNRoomReady(), UserData.Instance is null.");

                _manager1Name.text = UserData.Instance.Profile.Pseudo;
                _manager2Name.text = _botsPseudoArray[Random.Range(0, _botsPseudoArray.Length)];

				Assert.IsNotNull(_pvpData, "[MatchUI] OnPUNRoomReady(), _pvpData is null.");

                _pvpData.OpponentName = _manager2Name.text;
            }

            StartCoroutine(MatchAnimation(0.7f));
            _defaultUI.AnimateCanevasInOut(_vsOn, _vsOff, 0.5f);

            _tshirtB.color = Color.white;
            _avatarB.color = Color.white;

            _tshirtB.gameObject.transform.GetChild(0).gameObject.SetActive(false);
            _avatarB.gameObject.transform.GetChild(0).gameObject.SetActive(false);

        }

        /// <summary>
        /// Animation
        /// </summary>
        public IEnumerator MatchAnimation(float pDelayTime)
        {
            yield return new WaitForSeconds(pDelayTime);
            _onFinishedPreMatchAnimation.Invoke();
        }

        public void OnGetProfileEvent()
        {
			if (null != _manager1Name)
			{
				Assert.IsNotNull(UserData.Instance, "[MatchUI] OnGetProfileEvent(), UserData.Instance is null.");

				_manager1Name.text	=	UserData.Instance.Profile.Pseudo;
			}

            UpdateField();
        }

		#endregion

		#region Private Methods

		/// <summary>
		/// Set Bot Pseudo Array from json
		/// </summary>
		private void SetBotPseudo()
        {
            JToken lJToken = JToken.Parse(_botsPseudo);
            JArray lArray = lJToken.Value<JArray>("pseudos");

            _botsPseudoArray = new string[lArray.Count];

            for (int lIndex = 0; lIndex < lArray.Count; ++lIndex)
            {
                _botsPseudoArray[lIndex] = lArray[lIndex].Value<string>();
            }

        }

        private void Start()
        {
			if (null != _manager1Name)
			{
				Assert.IsNotNull(UserData.Instance, "[MatchUI] Start(), UserData.Instance is null.");

				_manager1Name.text	=	UserData.Instance.Profile.Pseudo;
			}

			_launchSearch = true;

            UpdateField();
        }

        //private void StartPVPLoading()
        //{
        //    // _defaultUI.AnimateCanevasInOut()
        //    _pun.Search();
        //}

        private	void	UpdateField()
        {
            Sprite	lSprite	=	null;

			Assert.IsNotNull(UserData.Instance, "[MatchUI] UpdateField(), UserData.Instance is null.");

            if (UserData.Instance.Profile.Level == 1)
            {
                lSprite	=	Resources.Load<Sprite>("Sprites/Stadiums/NewField_1");
                _currentFieldText.text	=	I2.Loc.LocalizationManager.GetTranslation("StadiumLevel") + " 1";
                _newFieldText.text		=	"(" + I2.Loc.LocalizationManager.GetTranslation("StadiumNextLevel") + " 2)";
            }
            else if (UserData.Instance.Profile.Level >= 2 && UserData.Instance.Profile.Level < 10)
            {
                lSprite	=	Resources.Load<Sprite>("Sprites/Stadiums/NewField_2");
                _currentFieldText.text	=	I2.Loc.LocalizationManager.GetTranslation("StadiumLevel") + " 2";
                _newFieldText.text		=	"(" + I2.Loc.LocalizationManager.GetTranslation("StadiumNextLevel") + " 10)";
            }
            else if (UserData.Instance.Profile.Level >= 10 && UserData.Instance.Profile.Level < 20)
            {
                lSprite	=	Resources.Load<Sprite>("Sprites/Stadiums/NewField_3");
                _currentFieldText.text	=	I2.Loc.LocalizationManager.GetTranslation("StadiumLevel") + " 10";
                _newFieldText.text		=	"(" + I2.Loc.LocalizationManager.GetTranslation("StadiumNextLevel") + " 20)";
            }
            else if (UserData.Instance.Profile.Level >= 20)
            {
                lSprite	=	Resources.Load<Sprite>("Sprites/Stadiums/NewField_4");
                _currentFieldText.text	=	I2.Loc.LocalizationManager.GetTranslation("StadiumLevel") + " 20";
                _newFieldText.text		=	"";
            }

            if (null != lSprite)
            {
                _currentField.sprite	=	lSprite;
                _currentField.gameObject.SetActive(true);
            }
            else
            {
                _currentField.sprite	=	Resources.Load<Sprite>("Sprites/Stadiums/NewField_1");
                _currentField.gameObject.SetActive(false);
            }
        }
		
		#endregion
    }
}
using Sportfaction.CasualFantasy.Menu.Squad;

using DG.Tweening;
using UnityEngine;

namespace Sportfaction.CasualFantasy.Menu
{
    /// <summary>
    /// Description: MenuUI
    /// Author: Antoine de Lachèze-Murel
    /// </summary>
    public class OnBoardingUI : MonoBehaviour
    {

        #region Fields

        [Header("Links")]

        [SerializeField, Tooltip("DefaultUI")]
        private DefaultUI _defaultUI = null;

        [SerializeField, Tooltip("SquadUI")]
        private SquadUI _squadUI = null;

        [SerializeField, Tooltip("MenuUI")]
        private MenuUI _menuUI = null;


        [Header("UI")]

        [SerializeField, Tooltip("_stepA")]
        private GameObject _stepA = null;

        [SerializeField, Tooltip("_stepB")]
        private GameObject _stepB = null;

        [SerializeField, Tooltip("_stepC")]
        private GameObject _stepC = null;


        #endregion

        #region Public Methods
        public void GoToOnBoardingB()
        {
            _defaultUI.AnimateCanevasInOut(_stepB, _stepA, 0.3f);
            _defaultUI.AnimateCanevasIn(_squadUI.CanvasGroup, 0.3f);
            _squadUI.Field.DOLocalMoveY(_squadUI.Field.localPosition.y - 100f, 0.3f);
        }

        public void GoToOnBoardingC()
        {
            _defaultUI.AnimateCanevasInOut(_stepC, _stepB, 0.3f);
            _squadUI.Field.DOLocalMoveY(_squadUI.Field.localPosition.y - 50f, 0.3f);
            _squadUI.Field.DOScale(0.8f, 0.3f);
        }

        public void EndOnBoarding()
        {
            _squadUI.Field.DOLocalMoveY(_squadUI.Field.localPosition.y + 150f, 0.1f);
            _squadUI.Field.DOScale(1.1f, 0.1f);
            _defaultUI.AnimateCanevasOut(_squadUI.CanvasGroup, 0.1f);
            _defaultUI.AnimateCanevasOut(_stepC, 0.3f);
            _defaultUI.AnimateCanevasOut(_squadUI.CanvasGroup, 0.1f);

            _menuUI.GoToMenu();
        }

        #endregion

        #region Private Methods

        private void Start()
        {
            _defaultUI.AnimateCanevasIn(_stepA, 0.3f);
        }

        #endregion
    }

}

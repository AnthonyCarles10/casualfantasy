using Sportfaction.CasualFantasy.Manager.Profile;
using Sportfaction.CasualFantasy.Services.Userstracking;
using Sportfaction.CasualFantasy.Tactical;
using Sportfaction.CasualFantasy.Menu.Squad;
using Sportfaction.CasualFantasy.Action;
using Sportfaction.CasualFantasy.Leaderboard;
using Sportfaction.CasualFantasy.Config;
using Sportfaction.CasualFantasy.AppParam;

using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;

namespace Sportfaction.CasualFantasy.Menu
{
    /// <summary>
    /// Description: MenuUI
    /// Author: Antoine de Lachèze-Murel
    /// </summary>
    public class MenuUI : MonoBehaviour
    {

        #region Properties

        public GameObject ChoicesCanvas { get { return _choicesCanvas; } }

        #endregion

        #region Fields

		[Header("App-Params")]
		[SerializeField, Tooltip("Scriptable object contains data about salary")]
		private	AppParamManagerSalaryData	_salaryData	=	null;
		//[SerializeField, Tooltip("Scriptable object contains data about score")]
		//private	AppParamManagerScoreData	_scoreData	=	null;
		[SerializeField, Tooltip("Scriptable object contains data about soft currency")]
		private	AppParamManagerSoftData		_softData	=	null;
		[SerializeField, Tooltip("Scriptable object contains data about xp")]
		private	AppParamManagerXpData		_xpData		=	null;

        [Header("Links")]

        [SerializeField, Tooltip("UserMain")]
        private UserMain _userMain = null;

        [SerializeField, Tooltip("ActionMain")]
        private ActionMain _actionMain = null;

        [SerializeField, Tooltip("LeaderboardMain")]
        private LeaderboardMain _leaderboardMain = null;

        [SerializeField, Tooltip("LeaderboardUI Script")]
        private LeaderboardUI _leaderboardUI = null;

        [SerializeField, Tooltip("MatchUI Script")]
        private MatchUI _matchUI = null;

        [SerializeField, Tooltip("ShopUI Script")]
        private ShopUI _shopUI = null;

        [SerializeField, Tooltip("SquadUI Script")]
        private SquadUI _squadUI = null;

        [SerializeField, Tooltip("TacticalUI Script")]
        private TacticalUI _tacticalUI = null;

        [SerializeField, Tooltip("DefaultUI Script")]
        private DefaultUI _defaultUI = null;

        [SerializeField, Tooltip("PopinUI")]
        public PopinUI _popinUI = null;

        [SerializeField, Tooltip("ObjectivesUI")]
        public ObjectivesUI _objectivesUI = null;


        [Header("UI")]


        [SerializeField, Tooltip("OnBoarding Canvas")]
        private GameObject _onBoardingCanvas = null;

        [SerializeField, Tooltip("Choices Canvas")]
        private GameObject _choicesCanvas = null;

        [SerializeField, Tooltip("Avatar Canvas")]
        private GameObject _avatarCanvas = null;

        [SerializeField, Tooltip("Soft Canvas")]
        private GameObject _softCanvas = null;

        [SerializeField, Tooltip("Back Canvas")]
        private GameObject _backCanvas = null;

        [SerializeField, ReadOnly, Tooltip("Current Canvas")]
        private GameObject _currentCanvas = null;

        [SerializeField, Tooltip("Soft")]
        private Text _soft = null;

        [SerializeField, Tooltip("Level")]
        private TMPro.TMP_Text _level = null;

        [SerializeField, Tooltip("LevelBar")]
        private RectTransform _levelBar = null;

        [SerializeField, Tooltip("LevelBar")]
        private RectTransform _levelMaxBar = null;

        [SerializeField, Tooltip("_vip")]
        private GameObject _vip = null;

        [SerializeField, Tooltip("_vip")]
        private GameObject _noVip = null;

        [SerializeField, Tooltip("_notificationsIOS")]
        private GameObject _notifications = null;

        [SerializeField, Tooltip("_nextLevelXpRemaining")]
        private TMPro.TMP_Text _nextLevelXpRemaining = null;

        [SerializeField, Tooltip("_currentBudget")]
        private TMPro.TMP_Text _currentBudget = null;

        [SerializeField, Tooltip("_nextBudget")]
        private TMPro.TMP_Text _nextBudget = null;


        [SerializeField, Tooltip("_nextLevelTitle")]
        private TMPro.TMP_Text _nextLevelTitle = null;

        [SerializeField, Tooltip("_sfBonus")]
        private TMPro.TMP_Text _sfBonus = null;
       
		private Dictionary<string, Sprite> _sprites = new Dictionary<string, Sprite>();
		
        #endregion

        #region Public Methods

        /// <summary>
        /// Go to Shop Page
        /// </summary>
        public void GoToShop()
        {
            if (_currentCanvas != _shopUI.CanvasGroup)
            {
                _defaultUI.AnimateCanevasOut(_currentCanvas, 0.2f);

                _defaultUI.AnimateCanevasOut(_avatarCanvas, 0.2f);
                _defaultUI.AnimateCanevasIn(_backCanvas, 0.2f);

                UserEventsManager.Instance.RecordEvent(new UserActionEvent("menu_shop"));

                _currentCanvas = _shopUI.CanvasGroup;

                _defaultUI.AnimateCanevasIn(_currentCanvas, 0.2f);
            }
        }

        /// <summary>
        /// Go to Squad Page
        /// </summary>
        public void GoToSquad()
        {
            if (_currentCanvas != _squadUI.CanvasGroup)
            {
                _defaultUI.AnimateCanevasOut(_currentCanvas, 0.2f);

                _defaultUI.AnimateCanevasOut(_avatarCanvas, 0.2f);
                _defaultUI.AnimateCanevasOut(_softCanvas, 0.2f);
                _defaultUI.AnimateCanevasIn(_backCanvas, 0.2f);

                UserEventsManager.Instance.RecordEvent(new UserActionEvent("menu_squad"));

                _currentCanvas = _squadUI.CanvasGroup;

                _defaultUI.AnimateCanevasIn(_currentCanvas, 0.2f);
            }
        }

        /// <summary>
        /// Go to tactical Page
        /// </summary>
        public void GoToTactical()
        {
            if (_currentCanvas != _tacticalUI.CanvasGroup)
            {
                _defaultUI.AnimateCanevasOut(_currentCanvas, 0.2f);

                _defaultUI.AnimateCanevasOut(_avatarCanvas, 0.2f);
                _defaultUI.AnimateCanevasIn(_backCanvas, 0.2f);

                UserEventsManager.Instance.RecordEvent(new UserActionEvent("menu_tactical"));

                _currentCanvas = _tacticalUI.CanvasGroup;

                _defaultUI.AnimateCanevasIn(_currentCanvas, 0.2f);

                _tacticalUI.GoToViewA();
            }
        }

        /// <summary>
        /// Go to GoToLeaderboard Page
        /// </summary>
        public void GoToLeaderboard()
        {
            if (_currentCanvas != _leaderboardUI.CanvasGroup)
            {
                _defaultUI.AnimateCanevasOut(_currentCanvas, 0.2f);

                _defaultUI.AnimateCanevasOut(_avatarCanvas, 0.2f);
                _defaultUI.AnimateCanevasIn(_backCanvas, 0.2f);

                UserEventsManager.Instance.RecordEvent(new UserActionEvent("menu_leaderboard"));

                _currentCanvas = _leaderboardUI.CanvasGroup;

                _defaultUI.AnimateCanevasIn(_currentCanvas, 0.2f);
            }
        }

        /// <summary>
        /// Go to Match Page
        /// </summary>
        public void GoToMatch()
        {
            if(_currentCanvas != _matchUI.CanvasGroup)
            {
                _defaultUI.AnimateCanevasOut(_currentCanvas, 0.2f);

                _defaultUI.AnimateCanevasOut(_avatarCanvas, 0.2f);
                _defaultUI.AnimateCanevasOut(_softCanvas, 0.2f);
               
                UserEventsManager.Instance.RecordEvent(new UserActionEvent("menu_match"));

                _currentCanvas = _matchUI.CanvasGroup;

                if (_squadUI.SalaryCap < 0)
                {
                    _popinUI.PlayersUpdated();
                }
                else
                {
                    _defaultUI.AnimateCanevasIn(_backCanvas, 0.2f);
                    _defaultUI.AnimateCanevasIn(_currentCanvas, 0.2f);
                }
            }
        }

        /// <summary>
        /// Go to Menu
        /// </summary>
        public void GoToMenu()
        {
            if (_currentCanvas != _choicesCanvas)
            {
                _defaultUI.AnimateCanevasOut(_currentCanvas, 0.2f);

                _defaultUI.AnimateCanevasIn(_avatarCanvas, 0.2f);
                _defaultUI.AnimateCanevasIn(_softCanvas, 0.2f);
                _defaultUI.AnimateCanevasOut(_backCanvas, 0.2f);

                _currentCanvas = _choicesCanvas;

                _defaultUI.AnimateCanevasIn(_currentCanvas, 0.2f);


                UserEventsManager.Instance.RecordEvent(new UserActionEvent("menu_home"));

                if (null != UserData.Instance && true == UserData.Instance.Profile.StarLevelUp)
                {
                    StartCoroutine(_objectivesUI.AnimateObjectives());
                    UserData.Instance.Profile.StarLevelUp = false;
                    UserData.Instance.PersistUserData();
                }
            }
        }


        public void OnGetProfileEvent()
        {
			Assert.IsNotNull(UserData.Instance, "[MenuUI] OnGetProfileEvent(), UserData.Instance is null.");

            _level.text	=	UserData.Instance.Profile.Level.ToString();
            _soft.text	=	UserData.Instance.Profile.Soft;

            UpdateLevelBar();

            if (true == UserData.Instance.Profile.IsVip)
            {
                _vip.SetActive(true);
                _noVip.SetActive(false);
            }
            else
            {
                _vip.SetActive(false);
                _noVip.SetActive(true);
            }
        }

        public void ActiveNotificationsIOS()
        {
            _notifications.transform.SetParent(null);
            _notifications.SetActive(true);

            _currentCanvas = _leaderboardUI.CanvasGroup;
            _defaultUI.AnimateCanevasIn(_currentCanvas, 0.2f);
            _defaultUI.AnimateCanevasOut(_avatarCanvas, 0.2f);
            _defaultUI.AnimateCanevasIn(_backCanvas, 0.2f);
            _defaultUI.AnimateCanevasIn(_softCanvas, 0.2f);
        }

        public void RefuseNotificationsIOS()
        {
            _currentCanvas = _leaderboardUI.CanvasGroup;
            _defaultUI.AnimateCanevasIn(_currentCanvas, 0.2f);
            _defaultUI.AnimateCanevasOut(_avatarCanvas, 0.2f);
            _defaultUI.AnimateCanevasIn(_backCanvas, 0.2f);
            _defaultUI.AnimateCanevasIn(_softCanvas, 0.2f);
        }


		#endregion

		#region Private Methods

		private void Awake()
		{
			Sprite[]	lSprites;
			string[]	lPaths	=	new string[]{ "Sprites/Squad/squad-0", "Sprites/Squad/squad-1" };
			
			for (int lj = 0; lj < lPaths.Length; lj++)
			{
				lSprites	=	Resources.LoadAll<Sprite>(lPaths[lj]);

				for (int li = 0; li < lSprites.Length; li++)
					_sprites.Add(lSprites[li].name, lSprites[li]);
			}

			_popinUI.Sprites = _sprites;
		}

		private void Start()
        {
            #if UNITY_EDITOR || UNITY_ANDROID
                _notifications.transform.SetParent(null);
                _notifications.SetActive(true);
            #endif
			Assert.IsNotNull(UserData.Instance, "[MenuUI] Start(), UserData.Instance is null.");

            if (null == ConfigData.Instance || true == UserData.Instance.Profile.OnBoarding)
            {
                _currentCanvas = _onBoardingCanvas;
                _defaultUI.AnimateCanevasIn(_onBoardingCanvas, 0.2f);
            }
            else
            {
                if (true == UserData.Instance.Profile.LastSceneGameplay)
                {
                    UserData.Instance.Profile.LastSceneGameplay = false;

                    UserData.Instance.PersistUserData();

                    _userMain.GetProfile();
                    if (Application.platform == RuntimePlatform.IPhonePlayer && true == string.IsNullOrEmpty(UserData.Instance.Profile.IOSId) && false == UserData.Instance.Profile.IosIsDisplayed)
                    {
                        _popinUI.ActiveNotificationsIOS();
                        UserData.Instance.Profile.IosIsDisplayed = true;

                        UserData.Instance.PersistUserData();
                    }
                    else
                    {
                        _currentCanvas = _leaderboardUI.CanvasGroup;

                        _defaultUI.AnimateCanevasIn(_currentCanvas, 0.2f);
                        _defaultUI.AnimateCanevasOut(_avatarCanvas, 0.2f);
                        _defaultUI.AnimateCanevasIn(_backCanvas, 0.2f);

                        _notifications.transform.SetParent(null);
                        _notifications.SetActive(true);
                    }
                }
                else
                {

                    _userMain.GetProfile();

                    if (Application.platform == RuntimePlatform.IPhonePlayer && true == string.IsNullOrEmpty(UserData.Instance.Profile.IOSId) && false == UserData.Instance.Profile.IosIsDisplayed)
                    {
                        _popinUI.ActiveNotificationsIOS();
                        UserData.Instance.Profile.IosIsDisplayed = true;

                        UserData.Instance.PersistUserData();
                    }
                    else
                    {
                        if (!UserData.Instance.Profile.ChallengesExplained)
                        {
                            _popinUI.DisplayChallenges();
                            UserData.Instance.Profile.ChallengesExplained = true;

                            UserData.Instance.PersistUserData();
                        }

                        _notifications.transform.SetParent(null);
                        _notifications.SetActive(true);

                        _currentCanvas = _choicesCanvas;
                        _defaultUI.AnimateCanevasIn(_currentCanvas, 0.2f);
                        _defaultUI.AnimateCanevasIn(_avatarCanvas, 0.2f);
                        _defaultUI.AnimateCanevasIn(_softCanvas, 0.2f);
                    }
                }

            }

            _level.text	=	UserData.Instance.Profile.Level.ToString();
            _soft.text	=	UserData.Instance.Profile.Soft;

            if(true == UserData.Instance.Profile.IsVip)
            {
                _vip.SetActive(true);
                _noVip.SetActive(false);
            }
            else
            {
                _vip.SetActive(false);
                _noVip.SetActive(true);
            }

            UpdateLevelBar();
            //_actionMain.GetTacticalMemberActions();
            _leaderboardMain.GetOverallRankingByTrophy("13", "0");
        }

        private void UpdateLevelBar()
        {
			Assert.IsNotNull(UserData.Instance, "[MenuUI] UpdateLevelBar(), UserData.Instance is null.");

            int lNextLevel = UserData.Instance.Profile.Level + 1;

            //Debug.Log("Level");
            //Debug.Log(Store.Instance.User.Profile.Level);

            //Debug.Log("cap");

			Assert.IsNotNull(_xpData, "[MenuUI] UpdateLevelBar(), _xpData is null.");

            int lCurrentStartCap = int.Parse(_xpData.Cap[UserData.Instance.Profile.Level]);
            int lCurrentXp = int.Parse(UserData.Instance.Profile.Xp) - lCurrentStartCap;
            int lNextXp = int.Parse(_xpData.Cap[UserData.Instance.Profile.Level + 1]) - lCurrentStartCap;
            //Debug.Log(lCurrentStartCap);
            //Debug.Log(lCurrentXp);
            //Debug.Log(lNextXp);


            //Debug.Log("raise");
            //Debug.Log(int.Parse(Store.Instance.AppParam.Manager.Xp.Raise[Store.Instance.User.Profile.Level]));
            //Debug.Log(int.Parse(Store.Instance.AppParam.Manager.Xp.Raise[Store.Instance.User.Profile.Level + 1]));


            _nextLevelTitle.text = I2.Loc.LocalizationManager.GetTranslation("rewardLevel") + " " + lNextLevel.ToString() + " : ";

            if (_xpData.Cap[UserData.Instance.Profile.Level + 1] != null) 
            {
                //int lNextXpCap = int.Parse(_xpData.Cap[UserData.Instance.Profile.Level + 1]);

                float x = lCurrentXp * _levelMaxBar.sizeDelta.x / lNextXp;

                _levelBar.sizeDelta = new Vector2(x, _levelBar.sizeDelta.y);
            }
            else
            {
                _levelBar.sizeDelta = new Vector2(_levelMaxBar.sizeDelta.x, _levelBar.sizeDelta.y);
            }

            int lXpRemaining = int.Parse(_xpData.Cap[UserData.Instance.Profile.Level + 1]) - int.Parse(UserData.Instance.Profile.Xp);

            _nextLevelXpRemaining.text = "-" + lXpRemaining.ToString();

			Debug.Assert(null != _salaryData, "[MenuUI] UpdateLevelBar(), _salaryData is null.");

			Debug.Assert(null != _softData, "[MenuUI] UpdateLevelBar(), _softData is null.");

            if(false == UserData.Instance.Profile.IsVip)
            {
                //for (int i = 1; i < Store.Instance.AppParam.Manager.Salary.Cap.Length; i++)
                //{
                //    Debug.Log("SalaryCap (Lv " + i + ") = " + Store.Instance.AppParam.Manager.Salary.Cap[i]);
                //    Debug.Log("SalaryCapVip (Lv " + i + ") = " + Store.Instance.AppParam.Manager.Salary.CapVip[i]);
                //    Debug.Log("XpCap (Lv " + i + ") = " + Store.Instance.AppParam.Manager.Xp.Cap[i]);
                //    Debug.Log("XpRaise (Lv " + i + ") = " + Store.Instance.AppParam.Manager.Xp.Raise[i]);
                //}
                int lDiffBudget = int.Parse(_salaryData.Cap[UserData.Instance.Profile.Level + 1]) - int.Parse(_salaryData.Cap[UserData.Instance.Profile.Level]);
                _currentBudget.text = "(" + _salaryData.Cap[UserData.Instance.Profile.Level] + "<#40E82AFF>+" + lDiffBudget.ToString() + "</color>)";

                _nextBudget.text = _salaryData.Cap[UserData.Instance.Profile.Level + 1];

                _sfBonus.text = "+" + _softData.BonusLevelup[UserData.Instance.Profile.Level + 1];
            }
            else
            {
                int lDiffBudget = int.Parse(_salaryData.CapVip[UserData.Instance.Profile.Level + 1]) - int.Parse(_salaryData.CapVip[UserData.Instance.Profile.Level]);
                _currentBudget.text = "(" + _salaryData.CapVip[UserData.Instance.Profile.Level] + "<#40E82AFF>+" + lDiffBudget.ToString() + "</color>)";

                _nextBudget.text = _salaryData.CapVip[UserData.Instance.Profile.Level + 1];

                _sfBonus.text = "+" + _softData.BonusLevelupVip[UserData.Instance.Profile.Level + 1];
            }


        }
        
        #endregion
    }

}

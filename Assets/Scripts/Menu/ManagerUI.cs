using Sportfaction.CasualFantasy.Manager.Profile;

using Newtonsoft.Json.Linq;
using UnityEngine;

namespace Sportfaction.CasualFantasy.Menu
{
    /// <summary>
    /// Description: MenuUI
    /// Author: Antoine de Lachèze-Murel
    /// </summary>
    public class ManagerUI : MonoBehaviour
    {
        #region Fields

        [Header("Links")]
        [SerializeField, Tooltip("UserAPICalls")]
        public	UserAPICalls	_userAPICalls	=	null;
        //[SerializeField, Tooltip("MenuUI Script")]
        //private	MenuUI			_menuUI			=	null;
        //[SerializeField, Tooltip("DefaultUI Script")]
        //private	DefaultUI		_defaultUI		=	null;

        [Header("Text")]
        [SerializeField, Tooltip("StatsManagerCurrentRank")]
        private	TMPro.TMP_Text	_statsManagerCurrentRank	=	null;
        [SerializeField, Tooltip("StatsManagerMatchPlayed")]
        private	TMPro.TMP_Text	_statsManagerMatchPlayed	=	null;
        [SerializeField, Tooltip("StatsManagerMatchWon")]
        private	TMPro.TMP_Text	_statsManagerMatchWon		=	null;
        [SerializeField, Tooltip("StatsManagerDraw")]
        private	TMPro.TMP_Text	_statsManagerDraw			=	null;
        [SerializeField, Tooltip("StatsManagerRatio")]
        private	TMPro.TMP_Text	_statsManagerRatio			=	null;
        [SerializeField, Tooltip("StatsManagerGoalScored")]
        private	TMPro.TMP_Text	_statsManagerGoalScored		=	null;
        [SerializeField, Tooltip("StatsManagerGoalConceded")]
        private	TMPro.TMP_Text	_statsManagerGoalConceded	=	null;

        [Header("UI")]
        //[SerializeField, Tooltip("Background")]
        //public	GameObject	Background		=	null;
        [SerializeField, Tooltip("Canevas Group")]
        public	GameObject	CanevasGroup	=	null;

        [Header("Buttons")]
        [SerializeField, Tooltip("Back Button")]
        public	GameObject	BackButton	=	null;

        public	Vector3	BackButtonInitPosition	=	Vector3.zero;

        #endregion

        #region Public Methods

        public	void	Init(string pJson)
        {
            JToken	lJToken	=	JToken.Parse(pJson);

            _statsManagerCurrentRank.text	=	lJToken.Value<string>("rank");
            _statsManagerMatchPlayed.text	=	lJToken.Value<string>("nb_games_played");
            _statsManagerMatchWon.text		=	lJToken.Value<string>("nb_games_won");
            _statsManagerRatio.text			=	lJToken.Value<string>("nb_games_won_ratio");
            _statsManagerDraw.text			=	lJToken.Value<string>("nb_games_draw");
            _statsManagerGoalScored.text	=	lJToken.Value<string>("nb_goals_scored");
            _statsManagerGoalConceded.text	=	lJToken.Value<string>("nb_goals_conceded");
        }
        
        #endregion

        #region Private Methods

        private	void	Start()
        {
			if (null != UserData.Instance)
			{
				_userAPICalls.GetStatsProfile(UserData.Instance.Profile.Id.ToString());
			}
        }

        #endregion
    }
}

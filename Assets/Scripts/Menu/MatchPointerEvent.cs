using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Sportfaction.CasualFantasy.Menu
{
    /// <summary>
    /// Description: MenuPointerEvent
    /// Author: Antoine de Lachèze-Murel
    /// </summary>
    public class MatchPointerEvent : MonoBehaviour, IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler
    {
    
        #region Fields

        [Header("Links")]

        [SerializeField, Tooltip("MatchUI")]
        private MatchUI _matchUI = null;

        [SerializeField, Tooltip("DefaultUI")]
        private DefaultUI _defaultUI = null;

        [SerializeField, Tooltip("AudioUI")]
        private AudioUI _audioUI = null;

        [SerializeField, ReadOnly, Tooltip("_currentTransform")]
        private Transform _currentTransform = null;

        #endregion

        #region Public Methods

        /// <summary>
        /// Called by the EventSystem when the pointer
        /// enters the object associated with this EventTrigger.
        /// </summary>
        public void OnPointerEnter(PointerEventData eventData)
        {
            if (null != eventData.pointerEnter)
            {
                switch (eventData.pointerEnter.transform.name)
                {
                    case "[Play]":
                        _audioUI.OnClickSound();
                        _defaultUI.AnimateButtonEnter(eventData.pointerEnter.transform);
                        _currentTransform = eventData.pointerEnter.transform;
                        break;
                    case "[Cancel]":
                        _audioUI.OnBackSound();
                        _defaultUI.AnimateButtonEnter(eventData.pointerEnter.transform);
                        _currentTransform = eventData.pointerEnter.transform;
                        break;
                }
            }

        }

        /// <summary>
        /// Called by the EventSystem when the pointer
        /// enters the object associated with this EventTrigger.
        /// </summary>
        public void OnPointerExit(PointerEventData eventData)
        {
            if (null != _currentTransform)
            {
                _defaultUI.AnimateButtonUp(_currentTransform);
            }
        }

        /// <summary>
        /// Register button presses using the IPointerClickHandler
        /// </summary>
        public void OnPointerClick(PointerEventData eventData)
        {
            if (null != _currentTransform)
            {
                _defaultUI.AnimateButtonUp(_currentTransform);
                StartCoroutine(GoTo());
            }
        }

        #endregion

        #region Private Methods

        private IEnumerator GoTo()
        {
            if (null != _currentTransform)
            {
                _defaultUI.EventSystem.SetActive(false);

                yield return new WaitForSeconds(0.4f);

                _defaultUI.EventSystem.SetActive(true);

                switch (_currentTransform.name)
                {
                    case "[Play]":
                        _matchUI.Play();
                        break;

                    case "[Cancel]":
                        _matchUI.Cancel();
                        break;
                }
            }
        }

        #endregion
    }
}
﻿using Newtonsoft.Json.Linq;
using Sportfaction.CasualFantasy.Manager.Profile;
using Sportfaction.CasualFantasy.PVP.Network;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Sportfaction.CasualFantasy.Meta
{
    /// <summary>
    /// Description: SplashScreen
    /// Author: Antoine de Lachèze-Murel
    /// </summary>
    public sealed class SplashScreen : MonoBehaviour
    {

        #region Fields

        [Header("Prefab")]
        [SerializeField]
        private GameObject      _essentialsDev;
        [SerializeField]
        private GameObject      _essentialsProd;


        #endregion

        #region Private Methods
    
        void Awake()
        {
            if (null == GameObject.Find("#Essentials"))
            {
                #if DEVELOPMENT_BUILD
                    GameObject lEssentials = Instantiate(_essentialsDev);
                    lEssentials.name = "#Essentials";
                #else
                    GameObject lEssentials = Instantiate(_essentialsProd);
                    lEssentials.name = "#Essentials";
                #endif
            }
        }

        void Start()
        {
            UserData.Instance.LoadPVPRoomName();

            if (UserData.Instance.PvpRoomName != "")
            {
                string lJson = UserData.Instance.LoadUserData();

                if (!string.IsNullOrEmpty(lJson))
                {
                    JToken lJToken = JToken.Parse(lJson);

                    JToken lProfile = lJToken.Value<JToken>("user");

                    UserData.Instance.Profile.ParseFromPlayerPrefs(lProfile);

                    PhotonMain.Instance.Search();

                }
                else
                {
                    SceneManager.LoadScene("Main");
                }

                
            }
           
            else
                SceneManager.LoadScene("Main");
        }

#endregion
    }
}

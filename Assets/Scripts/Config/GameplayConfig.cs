﻿using SportFaction.CasualFootballEngine.ActionService.Enum;
using SportFaction.CasualFootballEngine.PhaseService.Enum;

using UnityEngine;

namespace Sportfaction.CasualFantasy.Config
{
    #pragma warning disable 414, CS0649
	[CreateAssetMenu(fileName = "Gameplay Config", menuName = "CasualFantasy/Config/GameplayConfig")]
	public sealed class GameplayConfig : ScriptableObject
	{
		#region Fields

		[SerializeField, Tooltip("Zoom action")]
		public	float	ZoomAction		=	1.5f;

        [Space(5)]
        [SerializeField, Tooltip("Player focus FOV")]
        public float PlayerFocusFOV     =   18f;

        [Header("Camera limits")]
        [SerializeField, Tooltip("Camera X limits")]
        public  float   CamLeftXLimit   =   1f;
        public  float   CamRightXLimit  =   1f;
        [SerializeField, Tooltip("Camera Z limits")]
        public  float   CamTopZLimit    =   1f;
        public  float   CamBottomZLimit =   1f;

        #endregion

        #region Public Methods

        public  float   GetActionLevelInclination(int pLevel)
        {
            float[] lRotation = new float[4] { 180f, 90f, 45f, 0f };
            int lIndex = pLevel - 1;
            if (lIndex < 0)
                lIndex = 0;
            if (lIndex > 3)
                lIndex = 3;

            return lRotation[lIndex];
        }

        /// <summary>
        /// Gets letter corresponding to specified form level
        /// </summary>
        /// <param name="pForm">Form level value</param>
        public  string  GetForm(int pForm)
        {
            string[] lFormLetters = new string[] { "D", "C", "B", "A" };

            if (pForm < 0)
                return lFormLetters[0];
            if (pForm > 3)
                return lFormLetters[3];

            return lFormLetters[pForm];
        }

        public string GetFormColorString(int pForm)
        {
            string[] lFormColor = new string[] { "Red", "Yellow", "Blue", "Green" };

            if (pForm < 0)
                return lFormColor[0];
            if (pForm > 3)
                return lFormColor[3];

            return lFormColor[pForm];
        }

        public  string  GetRarity(int pRarity)
        {
            string[] lRaritySuffixes = new string[] { "Bronze", "Silver", "Gold", "Epic", "Legendary" };

            if (pRarity < 0)
                return lRaritySuffixes[0];
            if (pRarity > 4)
                return lRaritySuffixes[4];

            return lRaritySuffixes[pRarity];
        }

        #endregion
    }
}

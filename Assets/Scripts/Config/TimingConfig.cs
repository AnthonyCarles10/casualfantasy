﻿using UnityEngine;

namespace Sportfaction.CasualFantasy.Config
{
    #pragma warning disable 414, CS0649
    [CreateAssetMenu(fileName = "Timing Config", menuName = "CasualFantasy/Config/TimingConfig")]
    public sealed class TimingConfig : ScriptableObject
    {
        #region Fields
        
        #region Speeds

        [Header("Speeds")]
        [SerializeField, Tooltip("Player movement speed")]
        public  float   PlayerSpeed             =   5f;
        [SerializeField, ReadOnly, Tooltip("Ball speed")]
        public	float	BallSpeed               =	3.05f;

        [Space(8)]
        [SerializeField, Tooltip("Camera speed")]
        public  float   SelectionCamSpeed   =   10f;
        [SerializeField, Tooltip("Camera speed")]
        public  float   GeneralCamSpeed     =   10f;

        #endregion

        [SerializeField, Tooltip("Delay between projection blink switch")]
        public  float   BlinkSwitchDelay    =   1f;

        #region UI

        [Header("UI")]
        [SerializeField, Tooltip("Board appearance")]
        public  float   BoardAppearance     =   0.5f;
        [SerializeField, Tooltip("Action button gauge fill speed")]
        public  float   ActionBtnFillSpeed  =   0.5f;

        #endregion

        #region Resolution

        [Header("Resolution")]
        [SerializeField, Tooltip("Bonus defense moving")]
        public  float   BonusDefenseMoving      =   1f;
        [SerializeField, Tooltip("Delay between shifumi incrementation")]
        public  float   ShifumiIncrementation   =   0.3f;

        [SerializeField, Tooltip("Waiting time between goal & kick-off")]
        public  float   WaitBeforeKickOff   =   0.3f;

        #endregion

        #region Match End

        [Header("Match End")]
        [SerializeField, ReadOnly, Tooltip("Duration trophy anim")]
        public	float	DurationSSTrophyAnim		=	1.3f;
        [SerializeField, ReadOnly, Tooltip("Duration objective reached")]
        public	float	DurationSSObjectiveAnim     =	2.1f;
        [SerializeField, ReadOnly, Tooltip("Duration SF won")]
        public  float   DurationSSSoftAnim          =   1.1f;
        
        #endregion
        
        #endregion
    }
}

﻿using SportFaction.CasualFootballEngine.ActionService.Enum;
using UnityEditor;
using UnityEngine;

namespace Sportfaction.CasualFantasy.Config
{
    #pragma warning disable 414, CS0649
	[CreateAssetMenu(fileName = "Color Config", menuName = "CasualFantasy/Config/ColorConfig")]
	public sealed class ColorConfig : ScriptableObject
	{
		#region Fields
		
		[Header("Common")]
		[SerializeField, Tooltip("Good score color")]
		public  Color32 GoodScore;
		[SerializeField, Tooltip("Bad score color")]
		public  Color32 BadScore;
		[SerializeField, Tooltip("Yellow color")]
		public	Color32	YellowColor;
        [SerializeField, Tooltip("Tactical color")]
        public  Color32 TacticalColor;

		[Header("Team UI Specific")]
		[SerializeField, Tooltip("Team A color")]
		public  Color   TeamA;
        [SerializeField, Tooltip("Team B color")]
        public  Color   TeamB;
        [SerializeField, Tooltip("Neutral color")]
        public  Color   Neutral;

        [Header("Risk Specific")]
        [SerializeField, Tooltip("Green Line")]
        private Color32 GreenLine;
        [SerializeField, Tooltip("Yellow Line")]
        public  Color32 YellowLine;
        [SerializeField, Tooltip("Orange Line")]
        public  Color32 OrangeLine;
        [SerializeField, Tooltip("Red Line")]
        private Color32 RedLine;

        [Space(5)]
        [SerializeField, Tooltip("TWP Synapse")]
        public  Color32 TWPSynapse;

        [Header("Action/Draw Level Specific")]
        [SerializeField, Tooltip("Action/Draw level colors")]
        public  Color32[]   Levels;
        [SerializeField, Tooltip("Action/Draw level colors off version")]
        public  Color32[]   LevelsOff;

        [Header("Match Outro")]
        [SerializeField, Tooltip("Trophies value")]
        public  Color32     TrophiesValue;

        #endregion

        #region Public Methods

        public  Color32 GetImgColor(bool pActive)
        {
            return pActive ? Color.white : Color.black;
        }

        /// <summary>
        /// Gets team blue/yellow color
        /// </summary>
        /// <param name="pIsBlueManager">Is current user the blue team manager ?</param>
        public  Color32 GetTeamColor(bool pIsBlueManager, bool pActive = true)
		{
            return pIsBlueManager ? TeamA : TeamB;
		}

        /// <summary>
        /// Gets risk color depending on difficulty
        /// </summary>
        /// <param name="pColor">Difficulty color</param>
        public	Color   GetColorDifficulty(eColorDifficulty pColor, bool pActive = true)
		{
            Color32 lOffColor;

			switch (pColor)
			{
				case eColorDifficulty.ORANGE:
                    lOffColor   =   OrangeLine;
                    lOffColor.a =   (byte)150;
					return pActive ? OrangeLine : lOffColor;
				case eColorDifficulty.YELLOW:
                    lOffColor   =   YellowLine;
                    lOffColor.a =   (byte)150;
					return pActive ? YellowLine : lOffColor;
                case eColorDifficulty.RED:
                    lOffColor   =   RedLine;
                    lOffColor.a =   (byte)150;
                    return pActive ? RedLine : lOffColor;
                default:
                    lOffColor   =   GreenLine;
                    lOffColor.a =   (byte)150;
					return pActive ? GreenLine : lOffColor;
			}
        }

        /// <summary>
        /// Gets risk color depending on difficulty
        /// </summary>
        /// <param name="pColor">Difficulty color</param>
        public  Color   GetColorDifficulty(string pColor, bool pActive = true)
        {
            Color32 lOffColor;

            switch (eColorDifficultyMethods.ConvertStringToEnum(pColor))
            {
                case eColorDifficulty.ORANGE:
                    lOffColor   =   OrangeLine;
                    lOffColor.a =   (byte)150;
					return pActive ? OrangeLine : lOffColor;
                case eColorDifficulty.YELLOW:
                    lOffColor   =   YellowLine;
                    lOffColor.a =   (byte)150;
					return pActive ? YellowLine : lOffColor;
                case eColorDifficulty.RED:
                    lOffColor   =   RedLine;
                    lOffColor.a =   (byte)150;
                    return pActive ? RedLine : lOffColor;
                default:
                    lOffColor   =   GreenLine;
                    lOffColor.a =   (byte)150;
					return pActive ? GreenLine : lOffColor;
            }
        }

        public  Color   GetScoreColor(int pScore)
        {
            if (pScore < 20)
                return BadScore;
            
            if (pScore < 60)
                return Color.white;

            return GoodScore;
        }

        /// <summary>
        /// Gets action / form color
        /// </summary>
        /// <param name="pForm">Action / Form level</param>
        /// <param name="pOn">Should return on version ?</param>
        public Color32 GetDrawLevelColor(int pForm, bool pOn = true)
        {
            int lIndex = pForm - 1;

            if (0 > lIndex)
                return pOn ? Levels[0] : LevelsOff[0];

            if (4 <= lIndex)
                return pOn ? Levels[Levels.Length - 1] : LevelsOff[Levels.Length - 1];

            return pOn ? Levels[lIndex] : LevelsOff[lIndex];
        }

        #endregion
    }
}

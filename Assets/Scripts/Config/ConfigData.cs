using Sportfaction.CasualFantasy.Utilities;

using SportFaction.CasualFootballEngine;

using System.Collections.Generic;
using UnityEngine;

namespace Sportfaction.CasualFantasy.Config
{
	/// <summary>
	/// Description: Class used to 
	/// Author: R��mi Carreira
	/// </summary>
    public sealed class ConfigData : Singleton<ConfigData>
    {
		#region Properties

		// Property used to know the environment of this application
		public	eConfig	ENV				{ get { return _env; } }
		// Property used to get the API url used to connect to the dev server
		public	string	API_URL_DEV		{ get { return _APIUrlDev; } }
		// Property used to get the API url used to connect to the prod server
		public	string	API_URL_PROD	{ get { return _APIUrlProd; } }
		// Property used to get the dictionary who contains parameters for facebook
		public	Dictionary<string, string> TrackingParams { get { return _trackingParams; } }

        public  Dictionary<string, string> UserProperties { get { return _userProperties; } }

        public string APPSTORE_URL { get { return "https://itunes.apple.com/us/app/soccer-arena-manager/id1440976503"; } }

        public string GOOGLEPLAY_URL { get { return "https://play.google.com/store/apps/details?id=com.sportfaction.soccerarena.manager"; } }

        public int MAX_SCORE { get { return 900; } }

        #endregion

        #region Variables

        [Header("Settings")]
		[SerializeField, Tooltip("Environment of this application")]
		private eConfig     _env		=	eConfig.NONE;
		[SerializeField, Tooltip("API url used to connect to the dev server")]
		private string		_APIUrlDev	=	"";
		[SerializeField, Tooltip("API url used to connect to the prod server")]
		private	string		_APIUrlProd	=	"";

		// Dictionary contains parameters for tracking
        private Dictionary<string, string> _trackingParams =	new Dictionary<string, string>();

        // Dictionary contains parameters for user tracking tracking
        private Dictionary<string, string> _userProperties =    new Dictionary<string, string>();

        #endregion

        #region Public Methods

        /// <summary>
        /// Add a facebook parameter
        /// </summary>
        /// <param name="pKey">Key of this parameter</param>
        /// <param name="pValue">Value of this parameter</param>
        public	void	AddTrackingParam(string pKey, string pValue)
		{
			if (null != _trackingParams && !_trackingParams.ContainsKey(pKey))
			{
                _trackingParams.Add(pKey, pValue);
			}
		}

        /// <summary>
        /// Add User Tracking Property
        /// </summary>
        /// <param name="pKey">Key of this parameter</param>
        /// <param name="pValue">Value of this parameter</param>
        public void AddTrackingUserProperty(string pKey, string pValue)
        {
            if (null != _userProperties && !_userProperties.ContainsKey(pKey))
            {
                _userProperties.Add(pKey, pValue);
            }
        }

        #endregion

        #region Protected Methods

        /// <summary>
        /// Monobehaviour method called to initialize some settings
        /// </summary>
        protected override void	Awake()
		{
			base.Awake();

			InitTrackingParams();
		}

		#endregion

		#region Private Methods

		/// <summary>
		/// Initialize facebook parameters
		/// </summary>
		private	void InitTrackingParams()
		{
            _trackingParams.Add("env", ENV.ToString());

            #if !UNITY_EDITOR
                #if UNITY_ANDROID
				    _trackingParams.Add("platform", "android");
                #elif UNITY_IOS
				    _trackingParams.Add("platform", "ios");
                #endif
            #endif

            _trackingParams.Add("date", Epoch.GetUnixTimestamp().ToString() + "000");
            _trackingParams.Add("language", Application.systemLanguage.ToString());
            _trackingParams.Add("batteryLevel", SystemInfo.batteryLevel.ToString());
            _trackingParams.Add("deviceModel", SystemInfo.deviceModel);
            _trackingParams.Add("deviceUniqueIdentifier", SystemInfo.deviceUniqueIdentifier);
            _trackingParams.Add("timer", Time.realtimeSinceStartup.ToString());
            _trackingParams.Add("version", Application.version);


            //    _userId = UserData.Instance.Profile.Id;
            //_username = UserData.Instance.Profile.Username;
            //_soft = int.Parse(UserData.Instance.Profile.Soft);
            //_xp = int.Parse(UserData.Instance.Profile.Xp);
            //_nbBuyReal = UserData.Instance.Profile.NbRealPurchase;
            //_isVip = UserData.Instance.Profile.IsVip;
        }

        #endregion
    }
}

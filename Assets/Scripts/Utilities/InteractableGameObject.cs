﻿using System.Collections.Generic;
using DG.Tweening;
using Sportfaction.CasualFantasy.Events;
using Sportfaction.CasualFantasy.PVP.Gameplay.Matchs;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Sportfaction.CasualFantasy.Utilities
{
    /// <summary>
    /// Description: Renders Game Object Clickable
    /// Author: Madeleine Tranier
    /// </summary>
    public class InteractableGameObject : MonoBehaviour
    {
        public bool Active = true;

        #region Variables

        [SerializeField, Tooltip("Game event invoke when GO is clicked")]
        private GameEvent   _onGOClickedEvt    =   null;
        
        private bool _btnDown = false;

        #endregion

        #region Public Methods
        
        public  void    OnMouseDown()
        {
            if (!_isPointerOverObject())
            {
                _btnDown = true;
                ToggleActive(true);
            }
        }
        
        public  void    OnMouseExit()
        {
            _btnDown    =   false;
            ToggleActive(false);
        }

        /// <summary>
        /// Callback used to detect clicks.
        /// </summary>
        public  void    OnMouseUp()
        {
            if (_btnDown && null != _onGOClickedEvt)
            {
                this.gameObject.SetActive(false);
                if (null != MatchAudio.Instance)
                    MatchAudio.Instance.OnTargetValidated();
                _onGOClickedEvt.Invoke();
                _btnDown    =   false;
                ToggleActive(false);
            }
        }


        #endregion

        #region Private Methods

        private void    ToggleActive(bool pActivate)
        {
            if (0 == this.transform.childCount)
                return;

            Transform pTarget = this.transform.GetChild(0).GetChild(0);

            if (null != pTarget)
            {
                if (pActivate)
                {
                    pTarget.localScale = new Vector3(2f, 2f, 1f);
                    //pTarget.GetComponent<Animator>().SetTrigger("Reset");
                    //pTarget.GetComponent<Animator>().enabled = false;
                }
                else if (!pActivate)
                {
                    pTarget.DOScale(new Vector3(1f, 1f, 1f), 0.5f);

                    //if (Active)
                    //{
                    //    pTarget.GetComponent<Animator>().enabled = true;
                    //    pTarget.GetComponent<Animator>().SetTrigger("Rotate");
                    //}
                }
            }
        }

        /// <summary>
        /// Checks whether input is touching another object at the same time
        /// </summary>
        private bool    _isPointerOverObject()
        {
            PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
            eventDataCurrentPosition.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
            List<RaycastResult> results = new List<RaycastResult>();
            EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
            return results.Count > 0;
        }

        #endregion
    }
}

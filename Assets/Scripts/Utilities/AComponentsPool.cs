﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

namespace Sportfaction.CasualFantasy.Utilities
{
	/// <summary>
	/// Description: Abstract class managing components availability
	/// Author: Rémi Carreira
	/// </summary>
	public abstract class AComponentsPool<T> where T : Component
	{
		#region Fields

		private	GameObject	_prefab		=	null;			// Prefab to create
		private	Transform	_parent		=	null;			// Parent for all components created with this pool
		private	List<T>		_components	=	new List<T>();	// All components created

		#endregion

		#region Protected Methods

		/// <summary>
		/// Specific constructor
		/// </summary>
		/// <param name="pPrefab">Prefab to create</param>
		/// <param name="pParent">Parent for all components created with this pool</param>
		/// <param name="pMinComponents">Minimum of components to create</param>
		protected	AComponentsPool(GameObject pPrefab, Transform pParent, uint pMinComponents)
		{
			_prefab	=	pPrefab;
			_parent	=	pParent;

			for (uint lIndex = 0; lIndex < pMinComponents; ++lIndex)
			{
				CreateComponent();
			}
		}

		/// <summary>
		/// Get an unused component
		/// </summary>
		/// <returns>A disable component of type T</returns>
		protected	T	GetUnusedComponent()
		{
			for (int lIndex = 0; lIndex < _components.Count; ++lIndex)
			{
				Assert.IsNotNull(_components[lIndex], "AComponentsPool: GetUnusedComponent() failed, component at index " + lIndex + " is null.");

				if (_components[lIndex] && _components[lIndex].gameObject && false == _components[lIndex].gameObject.activeSelf)
				{
					return _components[lIndex];
				}
			}
			return CreateComponent();
		}

		#endregion

		#region Private Methods

		/// <summary>
		/// Default constructor
		/// </summary>
		private AComponentsPool() {}

		/// <summary>
		/// Create a new component using the prefab
		/// </summary>
		/// <returns>A new component of type T</returns>
		private	T		CreateComponent()
		{
			Assert.IsNotNull(_prefab, "AComponentsPool: CreateComponent() failed, _prefab is null.");

			GameObject	lNewGO	=	MonoBehaviour.Instantiate(_prefab, _parent);

			Assert.IsNotNull(lNewGO, "AComponentsPool: CreateComponent() failed, lNewGO is null.");

			lNewGO.SetActive(false);

			T	lNewComponent	=	lNewGO.GetComponent<T>();

			Assert.IsNotNull(lNewComponent, "AComponentsPool: CreateComponent() failed, lNewComponent is null.");

			_components.Add(lNewComponent);

			return lNewComponent;
		}

		#endregion
	}
}
﻿using UnityEngine;

namespace Sportfaction.CasualFantasy.Utilities
{
	/// <summary>
	/// Description: Make gameObject indestructible
	/// Author: Rémi Carreira
	/// </summary>
	[DisallowMultipleComponent]
	public class IndestructibleGameObject : MonoBehaviour
	{
		#region Private Methods

		/// <summary>
		/// Unity Method | Make gameObject indestructible
		/// </summary>
		private	void	Awake()
		{
			DontDestroyOnLoad(this.gameObject);
		}

		#endregion
	}
}
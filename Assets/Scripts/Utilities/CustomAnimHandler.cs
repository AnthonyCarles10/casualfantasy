﻿using Sportfaction.CasualFantasy.PVP.Gameplay.Actions;
using Sportfaction.CasualFantasy.PVP.Gameplay.Field;
using Sportfaction.CasualFantasy.PVP.Gameplay.Matchs;
using Sportfaction.CasualFantasy.PVP.Gameplay.UI;
using Sportfaction.CasualFantasy.PVP.UI;
using UnityEngine;

namespace Sportfaction.CasualFantasy.PVP.Gameplay
{
    /// <summary>
    /// Class handling animation custom behaviour (pausing/restart/loop/deactivate)
    /// </summary>
    public class CustomAnimHandler : MonoBehaviour
    {
        #region Fields
        
        #endregion

        #region Public Methods

        public  void    Destroy()
        {
            Destroy(this.gameObject);
        }
        
        public  void    Hide()
        {
            this.gameObject.SetActive(false);
        }

        public  void    CueCamera()
        {
            if (null != MatchCamera.Instance)
                MatchCamera.Instance.NextCameraPhase();
        }

        public  void    EndMatchAnimation(string pInstruction = "")
        {
            if (null != MatchMessagesUI.Instance)
                MatchMessagesUI.Instance.EndInfoAnim();

            if (string.IsNullOrEmpty(pInstruction))
                Hide();
        }

        public  void    ActionsLeftAnimation()
        {
            //if (null != MatchMessagesUI.Instance)
            //    MatchMessagesUI.Instance.MakeMsgBlink();
        }

        public  void    StartMatch()
        {
            if (null != MatchManager.Instance)
                MatchManager.Instance.StartMatch();

            this.GetComponent<Animator>().enabled   =   false;
        }

        public  void    MessageDisappeared()
        {
            if (null != MatchAudio.Instance)
                MatchAudio.Instance.OnMessageDisappears();
        }

        public  void    SoundToPlay(string pName)
        {
            if (null != MatchAudio.Instance)
            {
                switch(pName)
                {
                    case "trophy_win":
                    case "trophy_lose":
                        MatchAudio.Instance.OutroTrophy("trophy_win" == pName);
                        break;

                    case "soccer_cubes":
                        MatchAudio.Instance.OutroSoccerCubes();
                        break;
                }
            }
        }

        public  void    UpdateLootbox()
        {
            if (null != EndMatchUI.Instance)
                EndMatchUI.Instance.UpdateFragments();
        }

        public  void    HideFieldBoard()
        {
            if (null != SoccerFieldUI.Instance)
                SoccerFieldUI.Instance.HideFieldBoard();
        }

        public  void    UpdateParent()
        {
            //if (null != ActionsUI.Instance)
            //    ActionsUI.Instance.UpdateTacticalPoints();
        }

        #endregion
    }
}
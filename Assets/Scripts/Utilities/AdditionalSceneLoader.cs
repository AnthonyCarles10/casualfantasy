﻿using Sportfaction.CasualFantasy.PVP.Gameplay.Players;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.SceneManagement;

namespace Sportfaction.CasualFantasy.Utilities
{
    /// <summary>
    /// Description: Load additional scenes on start
    /// Author: Madeleine Tranier
    /// </summary>
    public class AdditionalSceneLoader : MonoBehaviour
    {
        #region Structures

        [System.Serializable]
        public struct SceneData
        {
            #region Variables

            [SerializeField, Tooltip("Level name")]
            public  string          SceneName;
            [SerializeField, Tooltip("Mode to load the scene")]
            public  LoadSceneMode   LoadMode;
            [SerializeField, Tooltip("Load new scene asynchronously ?")]
            public  bool            LoadAsync;
            [SerializeField, Tooltip("Dev mode only ?")]
            public  bool            DevOnly;

            #endregion
        }

        #endregion

        #region Properties

        #pragma warning disable 414, CS0649
        [SerializeField, Tooltip("Scenes to add")]
        private SceneData[] _sceneDatas;

        #endregion

        #region Private Methods

        void Awake()
        {


        }

        /// <summary>
        /// Unity Method | Load new scene
        /// </summary>
        private void    Start()
        {
            for (int li = 0; li < _sceneDatas.Length; li++)
            {
                if (true == _sceneDatas[li].DevOnly)
                {
                    #if DEVELOPMENT_BUILD
                        if (true == _sceneDatas[li].LoadAsync)
                            SceneManager.LoadSceneAsync(_sceneDatas[li].SceneName, _sceneDatas[li].LoadMode);
                        else
                            SceneManager.LoadScene(_sceneDatas[li].SceneName, _sceneDatas[li].LoadMode);
                    #endif
                }
                else
                {
                    if (true == _sceneDatas[li].LoadAsync)
                        SceneManager.LoadSceneAsync(_sceneDatas[li].SceneName, _sceneDatas[li].LoadMode);
                    else
                        SceneManager.LoadScene(_sceneDatas[li].SceneName, _sceneDatas[li].LoadMode);
                }
            }

            // Catch All Teams Players GameObject To Set Text Display To Blank & To Reactivate The Box Collider For Upcoming Selection
            //GameObject[] catchArray_Gob = GameObject.FindGameObjectsWithTag("Minions_Anchor_Points");

            //foreach (var catch_Var in catchArray_Gob)
            //{
            //    //print(catch_Var.transform.parent.transform.gameObject.name);
            //    //print(catch_Var.transform.parent.gameObject.name);

            //    catch_Var.transform.parent.transform.gameObject.GetComponent<BoxCollider>().enabled = false;

            //    catch_Var.transform.parent.transform.gameObject.GetComponent<PlayerUI>().AttDefValueTxt_Gob.name = "Att_Def_Value_Txt_" + catch_Var.transform.parent.transform.gameObject.name;
            //    catch_Var.transform.parent.transform.gameObject.GetComponent<PlayerUI>().AttDefValueTxt_Gob.GetComponent<RectTransform>().localPosition = new Vector3(0, 0, 0);
            //    catch_Var.transform.parent.transform.gameObject.GetComponent<PlayerUI>().AttDefValueTxt_Gob.GetComponent<RectTransform>().localScale = new Vector3(1.087531f, 1.087531f, 1.087531f);

            //    catch_Var.transform.parent.transform.gameObject.GetComponent<PlayerUI>().PercentValueTxt_Gob.name = "Percent_Value_Txt_" + catch_Var.transform.parent.transform.gameObject.name;
            //    catch_Var.transform.parent.transform.gameObject.GetComponent<PlayerUI>().PercentValueTxt_Gob.GetComponent<RectTransform>().localPosition = new Vector3(18, -30, 0);
            //    catch_Var.transform.parent.transform.gameObject.GetComponent<PlayerUI>().PercentValueTxt_Gob.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);
            //}
        }

        #endregion
    }
}
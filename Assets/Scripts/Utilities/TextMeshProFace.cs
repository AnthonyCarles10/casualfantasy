﻿using TMPro;
using UnityEngine;

namespace Sportfaction.CasualFantasy.Utilities
{
	/// <summary>
	/// Description: 
	/// Author: Rémi Carreira
	/// </summary>
	public sealed class TextMeshProFace : MonoBehaviour
	{
		#region Variables

		[Header("Parameters")]
		[SerializeField, Tooltip("Mesh of a text")]
		private	TMP_Text	_textMesh			=	null;
		//[SerializeField, Range(0f, 1f), Tooltip("Softness of the text's face")]
		//private	float		_faceSoftness		=	0f;
		//[SerializeField, Range(-1f, 1f), Tooltip("Dilation of the text's face")]
		//private	float		_faceDilation		=	0f;
		//[SerializeField, Tooltip("Tiling of the text's face texture")]
		//private	Vector2		_faceTextureTiling	=	Vector2.one;
		//[SerializeField, Tooltip("Offset of the text's face texture")]
		//private	Vector2		_faceTextureOffset	=	Vector2.zero;
		//[SerializeField, Tooltip("Speed of the text's face texure")]
		//private	Vector2		_faceTextureSpeed	=	Vector2.zero;
		[SerializeField, Tooltip("Color of the text's face")]
		private	Color32		_faceColor			=	new Color32(255, 255, 255, 255);
		//[SerializeField, Tooltip("Texture of the text's face")]
		//private	Texture2D	_faceTexture		=	null;

		#endregion

		#region Private Methods

		#if UNITY_EDITOR

		/// <summary>
		/// MonoBehaviour method called when parameters changed
		/// </summary>
		private	void	OnValidate()
		{
			if (null != _textMesh)
			{
				_textMesh.faceColor	=	_faceColor;

				//_textMesh.width
				_textMesh.ForceMeshUpdate(true);
			}
		}

		#endif

		#endregion
	}
}

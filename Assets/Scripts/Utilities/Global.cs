﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Sportfaction.CasualFantasy.Utilities
{
    public class Global : MonoBehaviour
    {
        static public Global Instance = null;

        public GameObject ResolutionUIFXManagerPC_Gob = null;
        public GameObject ResolutionUIFXManagerNPC_Gob = null;
        
        private ResourceRequest pRequestPC = null;
        private ResourceRequest pRequestNPC = null;

        /// <summary>
        /// Sets if user is team A manager
        /// </summary>
        private void    Start()
        {
            Instance = this;

            ResolutionUIFXManagerPC_Gob = null;
            ResolutionUIFXManagerNPC_Gob = null;


            pRequestPC = Resources.LoadAsync<GameObject>("Prefabs/Resolution_UI_FX_Manager_PC");
            pRequestNPC = Resources.LoadAsync<GameObject>("Prefabs/Resolution_UI_FX_Manager_NPC");
        }

        private void Update()
        {
            if (null != pRequestNPC && null != pRequestPC)
            {
                if (!pRequestPC.isDone && !pRequestNPC.isDone)
                {
                    Debug.Log("Loading Resolution_UI_FX_Manager_PC : " + pRequestPC.progress + "%");
                    if (pRequestPC.progress >= 0.9f)
                    {
                        ResolutionUIFXManagerPC_Gob = (GameObject)pRequestPC.asset;
                    }

                    Debug.Log("Loading Resolution_UI_FX_Manager_NPC : " + pRequestNPC.progress + "%");
                    if (pRequestNPC.progress >= 0.9f)
                    {
                        ResolutionUIFXManagerNPC_Gob = (GameObject)pRequestNPC.asset;
                    }
                }
            }
        }
    }
}

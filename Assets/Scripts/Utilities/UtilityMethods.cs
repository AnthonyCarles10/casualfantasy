﻿using Sportfaction.CasualFantasy.Events;
using Sportfaction.CasualFantasy.PVP.Gameplay.Matchs;
using Sportfaction.CasualFantasy.PVP.Gameplay.Actions;
using Sportfaction.CasualFantasy.PVP.Gameplay.Players;
using Sportfaction.CasualFantasy.PVP.Gameplay.UI;

using SportFaction.CasualFootballEngine.ActionService.Enum;
using SportFaction.CasualFootballEngine.ActionService.Response;
using SportFaction.CasualFootballEngine.PhaseService.Enum;
using SportFaction.CasualFootballEngine.TeamService.Enum;

using System.Text.RegularExpressions;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;
using SportFaction.CasualFootballEngine.PlayerService.Model;

/// <summary>
/// Description : Handles every utility methods
/// </summary>
/// @author Madeleine Tranier
public  static  class  UtilityMethods
{
    public  static  string      TrimString(string pString)
    {
        return Regex.Replace(pString, "_", "");
    }
    
    public	static	void	    SetText(Text pText, string pString, string pCaller = "[UtilityMethods]")
	{
		Assert.IsNotNull(pText, pCaller + " _setText() failed, pText is null.");

		pText.text = pString;
	}
	
	public	static	void	    SetText(TMPro.TMP_Text pText, string pString, string pCaller = "[UtilityMethods]")
	{
		Assert.IsNotNull(pText, pCaller + " _setText() failed, pText is null.");

		pText.text = pString;
	}

	public	static	void	    InvokeGameEvent(GameEvent pEvent, string pCaller = "[UtilityMethods]")
	{
		Assert.IsNotNull(pEvent, pCaller + " _invokeGameEvent failed, pEvent is null.");

		pEvent.Invoke();
	}
	
	public  static  Vector3     GetLineClosestPoint(Vector3 pLineStart, Vector3 pLineEnd, Vector3 pPoint)
	{
		Vector3	lHeading	=	pLineEnd - pLineStart;
		float	lDistance	=	lHeading.sqrMagnitude;
		Vector3	lDirection	=	lHeading / lDistance;

		lDirection.Normalize();
		
		Vector3	lHs		=	pPoint - pLineStart;
		float	lDotP	=	Vector3.Dot(lHs, lDirection);

		return pLineStart + lDirection * lDotP;
	}
    
    public  static  Material    GetMaterial(Material[] pArray, string pName, Material pDefault = null)
    {
        int lNbMat = pArray.Length;
        
        for (int li = 0; li < lNbMat; li++)
        {
            if (pName == pArray[li].name)
                return pArray[li];
        }
        
        return pDefault;
    }

	public  static  Sprite      GetSprite(Sprite[] pSprites, string pName, Sprite pDefault = null)
	{
		for (int li = 0; li < pSprites.Length; li++)
		{
			if (null != pSprites[li] && pName == pSprites[li].name)
				return pSprites[li];
		}

		return pDefault;
	}

    public  static  AudioClip   FindClip(AudioClip[] pClips, string pName)
    {
        for (int li = 0; li < pClips.Length; li++)
        {
            if (pName == pClips[li].name)
                return pClips[li];
        }

        return null;
    }

    #region Debug Methods

    public  static  void    DebugData(string pData)
    {
        #if DEVELOPMENT_BUILD

        Player lPlayerWithBall = MatchEngine.Instance.Engine.PossessionMain.GetPlayerWithBall();
        PlayerResponse lPlayerWithBallResponse = MatchEngine.Instance.Engine.ActionMain.GetPlayerResponse(lPlayerWithBall.Team, lPlayerWithBall.Position);

        Player lPlayerOpponent = MatchEngine.Instance.Engine.OppositionMain.GetFrontOpponentPlayer(lPlayerWithBall);
        PlayerResponse lPlayerOpponentReponse = null;
        if (null != lPlayerOpponent)
        {
            lPlayerOpponentReponse = MatchEngine.Instance.Engine.ActionMain.GetPlayerResponse(lPlayerOpponent.Team, lPlayerOpponent.Position);
        }

        switch (pData)
        {
            case "POSSESSION":
                //Debug.LogFormat("<color=#FF8C00>Current possession => " + MatchEngine.Instance.Engine.PossessionMain.CurTeamPossession + " " + MatchEngine.Instance.Engine.PossessionMain.CurPlayerPossession + "</color>");
                //Debug.LogFormat("<color=#FF8C00>TWP Value => " + MatchEngine.Instance.Engine.ActionMain.Response.ConfidentGauge + "</color>");

                //ePhase  lCurPhase   =   MatchEngine.Instance.GetCurrentPhase();

                //if (ePhase.TACTICAL_ATTACK == lCurPhase || ePhase.TACTICAL_DEFENSE == lCurPhase || ePhase.ACTION == lCurPhase)
                //    Debug.LogFormat("<color=#FF8C00>Tactical Points : TEAM_A = " + MatchEngine.Instance.Engine.ManagerMain.ManagerTeamA.TacticalPoints + " | TEAM_B = " + MatchEngine.Instance.Engine.ManagerMain.ManagerTeamB.TacticalPoints + "</color>");

                if (null == MatchEngine.Instance.GetDuelPlayer(eTeam.TEAM_A) || null == MatchEngine.Instance.GetDuelPlayer(eTeam.TEAM_B))
                {
                    Debug.LogFormat("<color=#FF8C00>SOLO : " + MatchEngine.Instance.GetDuelPlayer(MatchEngine.Instance.Engine.PossessionMain.CurTeamPossession).Team + "-" + MatchEngine.Instance.GetDuelPlayer(MatchEngine.Instance.Engine.PossessionMain.CurTeamPossession).Position + "</color>");
                }
                else
                {
                    bool lTeamAPossess = eTeam.TEAM_A == MatchEngine.Instance.Engine.PossessionMain.CurTeamPossession;
                    Debug.LogFormat("<color=#FF8C00>DUEL : TEAM_A-" + MatchEngine.Instance.GetDuelPlayer(eTeam.TEAM_A).Position + (lTeamAPossess ? "(o)" : "") + "  VS  TEAM_B-" + MatchEngine.Instance.GetDuelPlayer(eTeam.TEAM_B).Position + (lTeamAPossess ? "" : "(o)") + "</color>");
                }
                break;

            case "RESPONSE_EXECUTION":
                if (ePhase.ACTION == MatchEngine.Instance.CurPhase || ePhase.TACTICAL_ATTACK == MatchEngine.Instance.CurPhase)
                    return;

                Debug.Log("IS SOLO ACTION ? ===> " + MatchEngine.Instance.ResponseAction.IsSoloAction + " | RESPONSE TEAM ===> " + MatchEngine.Instance.ResponseAction.Team + " | RESOLUTION ACTION ===> " + MatchEngine.Instance.Engine.ActionMain.Response.ResolutionAction);
                Debug.Log("##### TEAM A RESPONSE #####");
                if(MatchEngine.Instance.ResponseAction.TeamA.Action == "NONE")
                {
                    Debug.Log("NO DUEL");
                }
                else
                {
                    Debug.Log(MatchEngine.Instance.ResponseAction.TeamA.Action + " (" + MatchEngine.Instance.ResponseAction.TeamA.PlayerActionPosition + ") ");
                }

                foreach (PlayerResponse item in MatchEngine.Instance.ResponseAction.TeamA.Players)
                {
                    if (item.Action == "MOVE")
                    {
                        Debug.Log(" - " + item.Team + "-" + item.Position + " [" + item.Action + "]");
                    }
                    else
                    {
                        Debug.Log(" - " + item.Team + "-" + item.Position + " [" + item.Action + "] -> S(" + item.ExecuteAction.Score + ") / SS(" + item.ExecuteAction.ScoreWithShifumi + ") / SP(" + item.ExecuteAction.ScoreWithPressure + ")");
                    }
                }
             
                Debug.Log("##### TEAM B RESPONSE #####");

                if (MatchEngine.Instance.ResponseAction.TeamB.Action == "NONE")
                {
                    Debug.Log("NO DUEL");
                }
                else
                {
                    Debug.Log(MatchEngine.Instance.ResponseAction.TeamB.Action + " (" + MatchEngine.Instance.ResponseAction.TeamB.PlayerActionPosition + ") ");
                }

                foreach (PlayerResponse item in MatchEngine.Instance.ResponseAction.TeamB.Players)
                {
                    if(item.Action == "MOVE")
                    {
                        Debug.Log(" - " + item.Team + "-" + item.Position + " [" + item.Action + "]");
                    }
                    else
                    {
                        Debug.Log(" - " + item.Team + "-" + item.Position + " [" + item.Action + "] -> S(" + item.ExecuteAction.Score + ") / SS(" + item.ExecuteAction.ScoreWithShifumi + ") / SP(" + item.ExecuteAction.ScoreWithPressure + ")");
                    }
                 
                }
              
                //Debug.Log("##### RESOLUTION #####");
                //Debug.Log(GZipCompressor.Uncompress(lPlayerWithBallResponse.Resolution));

                eAction lAction = (MatchEngine.Instance.ResponseAction.TeamA.BestScore > MatchEngine.Instance.ResponseAction.TeamB.BestScore) ? eActionMethods.ConvertStringToEnum(MatchEngine.Instance.ResponseAction.TeamA.Action) : eActionMethods.ConvertStringToEnum(MatchEngine.Instance.ResponseAction.TeamB.Action);
                if (ePhase.DUEL_RESOLUTION == MatchEngine.Instance.CurPhase || ePhase.TACTICAL_DEFENSE_RESOLUTION == MatchEngine.Instance.CurPhase)
                    lAction = eAction.NONE;

                switch (lAction)
                {
                    case eAction.PASS:
                        PassResolutionResponse passResolution = new PassResolutionResponse();
                        passResolution.Parse(MatchEngine.Instance.Engine.ActionMain.GetPlayerResponseWithAction(eAction.PASS).Resolution);

                        string log = "";

                        Debug.Log("<color=#FF8C00>>>></color>  Passer " + passResolution.Passer.Team + "-" + passResolution.Passer.Position + ": -> S(" + passResolution.Passer.Score + ") / SS(" + passResolution.Passer.ScoreWithShifumi + ") / SP(" + passResolution.Passer.ScoreWithPressure + ")");


                        if (null != lPlayerWithBallResponse && null != lPlayerWithBallResponse.PressureOpponents && lPlayerWithBallResponse.PressureOpponents.Count > 0)
                        {
                            log += "<color=#FF8C00>>>></color> [Pressure Players] Passer: ";
                            foreach (PlayerActionResponse item in lPlayerWithBallResponse.PressureOpponents)
                            {
                                log += item.Team + "-" + item.Position + " (" + item.PressureEffect + ") | ";
                            }

                            Debug.Log(log); log = "";
                        }

                        if (null != lPlayerOpponentReponse && null != lPlayerOpponentReponse.PressureOpponents && lPlayerOpponentReponse.PressureOpponents.Count > 0)
                        {
                            log += "<color=#FF8C00>>>></color> [Pressure Players] Passer Opponent: ";
                            foreach (PlayerActionResponse item in lPlayerOpponentReponse.PressureOpponents)
                            {
                                log += item.Team + "-" + item.Position + " (" + item.PressureEffect + ") | ";
                            }

                            Debug.Log(log); log = "";
                        }

                        if (passResolution.Interceptors.Count > 0)
                        {
                            log += "<color=#FF8C00>>>></color>Interceptors: ";
                            foreach (PlayerActionResponse lInterceptor in passResolution.Interceptors)
                            {
                                log += lInterceptor.Team + "-" + lInterceptor.Position + " (" + lInterceptor.Score + ") | ";
                            }

                            Debug.Log(log); log = "";
                        }

                        if (passResolution.ReceiverOpponentDuel != null)
                        {
                            log += "<color=#FF8C00>>>></color>Receiver's Opponent: ";
                            log += passResolution.ReceiverOpponentDuel.Team + "-" +  passResolution.ReceiverOpponentDuel.Position + " (" + passResolution.ReceiverOpponentDuel.Score + ") | ";

                            Debug.Log(log); log = "";

                        }

                        if (passResolution.ReceiverOpponentsPressure.Count > 0)
                        {
                            log += "<color=#FF8C00>>>></color>[Pressure Players] Receiver: ";
                            foreach (PlayerActionResponse item in passResolution.ReceiverOpponentsPressure)
                            {
                                log += item.Team + "-" +  item.Position + " (" + item.PressureEffect + ") | ";
                            }

                            Debug.Log(log); log = "";
                        }

                        if (passResolution.ReceiverOpponentDuelOpponentsPressure.Count > 0)
                        {
                            log += "<color=#FF8C00>>>></color> [Pressure Player] Receiver's Opponent: ";
                            foreach (PlayerActionResponse item in passResolution.ReceiverOpponentDuelOpponentsPressure)
                            {
                                log += item.Team + "-" + item.Position + " (" + item.PressureEffect + ") | ";
                            }

                            Debug.Log(log); log = "";
                        }

                        Debug.Log("<color=#FF8C00>>>></color>  Receiver: " + passResolution.Receiver.Position + " (" + passResolution.Receiver.ScoreWithPressure + ")");

                        Debug.Log("<color=#FF8C00>Is Ball Intercepted ? </color>" + passResolution.IsBallIntercepted.ToString() + " >> Team Resolution (" + passResolution.TeamResolution + ")" + " Player Resolution (" + passResolution.PlayerResolution + ")");

                        break;
                    case eAction.LONG_KICK:
                        LongKickResolutionResponse longKickResolution = new LongKickResolutionResponse();
                        longKickResolution.Parse(MatchEngine.Instance.Engine.ActionMain.GetPlayerResponseWithAction(eAction.LONG_KICK).Resolution);

                        log = "";

                        Debug.Log("<color=#FF8C00>>>></color> Striker " + longKickResolution.Stricker.Team + "-" + longKickResolution.Stricker.Position + ": -> S(" + longKickResolution.Stricker.Score + ") / SS(" + longKickResolution.Stricker.ScoreWithShifumi + ") / SP(" + longKickResolution.Stricker.ScoreWithPressure +")");

                        if (null != lPlayerWithBallResponse && null != lPlayerWithBallResponse.PressureOpponents && lPlayerWithBallResponse.PressureOpponents.Count > 0)
                        {
                            log += "<color=#FF8C00>>>></color> [Pressure Players] Striker: ";
                            foreach (PlayerActionResponse item in lPlayerWithBallResponse.PressureOpponents)
                            {
                                log += item.Team + "-" + item.Position + " (" + item.PressureEffect + ") | ";
                            }

                            Debug.Log(log); log = "";
                        }

                        if (null != lPlayerOpponentReponse && null != lPlayerOpponentReponse.PressureOpponents && lPlayerOpponentReponse.PressureOpponents.Count > 0)
                        {
                            log += "<color=#FF8C00>>>></color> [Pressure Players] Striker's Opponent: ";
                            foreach (PlayerActionResponse item in lPlayerOpponentReponse.PressureOpponents)
                            {
                                log += item.Team + "-" + item.Position + " (" + item.PressureEffect + ") | ";
                            }

                            Debug.Log(log); log = "";
                        }

                        if (longKickResolution.StrikerOpponentsPressure.Count > 0)
                        {
                            log += "<color=#FF8C00>>>></color> [Pressure Players] Stricker: ";
                            foreach (PlayerActionResponse item in longKickResolution.StrikerOpponentsPressure)
                            {
                                log += item.Team + "-" + item.Position + " (" + item.PressureEffect + ") | ";
                            }

                            Debug.Log(log); log = "";
                        }

                        if (longKickResolution.Interceptors.Count > 0)
                        {
                            log += "<color=#FF8C00>>>></color>  Interceptors: ";
                            foreach (PlayerActionResponse lInterceptor in longKickResolution.Interceptors)
                            {
                                log += lInterceptor.Team + "-" + lInterceptor.Position + " (" + lInterceptor.Score + ") | ";
                            }

                            Debug.Log(log); log = "";
                        }


                        Debug.Log("<color=#FF8C00>>>></color>  GoalKeeper : -> S(" + longKickResolution.GoalKeeper.Score + ") / SS(" + longKickResolution.GoalKeeper.ScoreWithShifumi + ") / SP(" + longKickResolution.GoalKeeper.ScoreWithPressure + ")");

                        Debug.Log("<color=#FF8C00>Is Ball Intercepted ? </color>" + longKickResolution.IsBallIntercepted.ToString() + " >> Team Resolution (" + longKickResolution.TeamResolution + ")" + " Player Resolution (" + longKickResolution.PlayerResolution + ")");

                        break;

                    default:
                        Debug.Log("<color=#FF8C00>>>></color>  Player With Ball " + lPlayerWithBallResponse.Team + "-" + lPlayerWithBallResponse.Position + ": -> S(" + lPlayerWithBallResponse.ExecuteAction.Score + ") / SS(" + lPlayerWithBallResponse.ExecuteAction.ScoreWithShifumi + ") / SP(" + lPlayerWithBallResponse.ExecuteAction.ScoreWithPressure + ")");

                        if (lPlayerOpponentReponse != null)
                        {
                            Debug.Log("<color=#FF8C00>>>></color>  Player With Ball Opponent " + lPlayerOpponentReponse.Team + "-" + lPlayerOpponentReponse.Position + ": -> S(" + lPlayerOpponentReponse.ExecuteAction.Score + ") / SS(" + lPlayerOpponentReponse.ExecuteAction.ScoreWithShifumi + ") / SP(" + lPlayerOpponentReponse.ExecuteAction.ScoreWithPressure + ")");

                        }
                        log = "";
                        if (null != lPlayerWithBallResponse && null != lPlayerWithBallResponse.PressureOpponents && lPlayerWithBallResponse.PressureOpponents.Count > 0)
                        {
                            log += "<color=#FF8C00>>>></color> [Pressure Players] Player With Ball: ";
                            foreach (PlayerActionResponse item in lPlayerWithBallResponse.PressureOpponents)
                            {
                                log += item.Team + "-" + item.Position + " (" + item.PressureEffect + ") | ";
                            }

                            Debug.Log(log); log = "";
                        }

                        if (null != lPlayerOpponentReponse && null != lPlayerOpponentReponse.PressureOpponents && lPlayerOpponentReponse.PressureOpponents.Count > 0)
                        {
                            log += "<color=#FF8C00>>>></color> [Pressure Players] Player With Ball Opponent: ";
                            foreach (PlayerActionResponse item in lPlayerOpponentReponse.PressureOpponents)
                            {
                                log += item.Team + "-" + item.Position + " (" + item.PressureEffect + ") | ";
                            }

                            Debug.Log(log); log = "";
                        }
                    break;
            }
                break;

            case "EXECUTION":
                if (ePhase.DUEL_RESOLUTION == MatchEngine.Instance.CurPhase || ePhase.ACTION_RESOLUTION == MatchEngine.Instance.CurPhase)
                    return;

                ResponseAction      lResponse       =   MatchEngine.Instance.ResponseAction;
                TeamActionResponse  lTeamAResponse  =   lResponse.TeamA;
                TeamActionResponse  lTeamBResponse  =   lResponse.TeamB;

                if (lResponse.IsSoloAction)
                {
                    Debug.LogFormat(
                        "<color=#FF8C00> SOLO ACTION : " + lResponse.Team + " "
                        + (eTeam.TEAM_A.ToString() == lResponse.Team ? lTeamAResponse.Action : lTeamBResponse.Action) + " ("
                        + (eTeam.TEAM_A.ToString() == lResponse.Team ? lTeamAResponse.Players[0].Position : lTeamBResponse.Players[0].Position)
                        + ")</color>"
                    );
                }
                else
                {
                    Debug.LogFormat(
                        "<color=#FF8C00> DUEL : " + lTeamAResponse.Action + "(" + lTeamAResponse.Players[0].Position
                        + ") VS " + lTeamBResponse.Action + "(" + lTeamBResponse.Players[0].Position
                        + ")</color>"
                    );

                    Debug.LogFormat(
                        "<color=#FF8C00> DUEL WINNER ====> " +
                        (lTeamAResponse.Players[0].ExecuteAction.Score > lTeamBResponse.Players[0].ExecuteAction.Score ? "TEAM_A" : "TEAM_B")
                        + "</color>"
                    );
                }

                if (eAction.PASS.ToString() == MatchEngine.Instance.Engine.ActionMain.Response.ResolutionAction)
                {
                    PassResolutionResponse lResolution = new PassResolutionResponse();

                    lResolution.Parse(MatchEngine.Instance.Engine.ActionMain.GetPlayerResponseWithAction(eAction.PASS).Resolution);

                    Debug.Log("<color=#FF8C00>##### PASS RESOLUTION #####</color>");
                    if (lResolution.IsBallIntercepted)
                    {
                        Debug.LogFormat("<color=#FF8C00> BALL INTERCEPTED BY " + MatchEngine.Instance.Engine.PossessionMain.CurPlayerPossession + "</color>");
                    }
                    else if (lResolution.TeamPasser != lResolution.TeamResolution)
                    {
                        Debug.LogFormat("<color=#FF8C00> BALL RECOVERED BY " + MatchEngine.Instance.Engine.PossessionMain.CurPlayerPossession + "</color>");
                    }
                    else
                    {
                        Debug.LogFormat("<color=#FF8C00> PASS TO " + lResolution.Receiver.Position + " ====> SUCCESSFULL !! </color>");
                    }
                }
                else if (eAction.LONG_KICK.ToString() == MatchEngine.Instance.Engine.ActionMain.Response.ResolutionAction)
                {
                    Debug.Log("<color=#FF8C00>##### LONG KICK RESOLUTION #####</color>");
                    LongKickResolutionResponse lResolution = new LongKickResolutionResponse();
                    lResolution.Parse(MatchEngine.Instance.Engine.ActionMain.GetPlayerResponseWithAction(eAction.LONG_KICK).Resolution);

                    if (lResolution.IsBallIntercepted)
                    {
                        Debug.LogFormat("<color=#FF8C00> BALL INTERCEPTED BY " + MatchEngine.Instance.Engine.PossessionMain.CurPlayerPossession + "</color>");
                    }
                    else if (lResolution.IsGoalkeeperStopBall)
                    {
                        Debug.LogFormat("<color=#FF8C00> BALL RECOVERED BY " + MatchEngine.Instance.Engine.PossessionMain.CurPlayerPossession + "</color>");
                    }
                    else
                    {
                        Debug.LogFormat("<color=#FF8C00> GOOOAAAALLLL !! </color>");
                    }
                }
                break;
        }
        #endif
    }

    public  static  void    DebugSimulation(eAction pActionType, PlayerPVP lFocusedPlayer)
    {
        #if DEVELOPMENT_BUILD
        string  debugLine   =   "";

        switch (pActionType)
        {
            case eAction.MOVE:
                if (null != lFocusedPlayer)
                    Debug.LogFormat("<color=#FFDEAD>### MOVING PLAYER : " + lFocusedPlayer.name + " ###</color>");
                else
                    Debug.LogFormat("<color=#FFDEAD>### NO PLAYER SELECTED FOR MOVE SIMULATION ###</color>");

                if (ActionsManager.Instance.MoveSimulation == null || ActionsManager.Instance.MoveSimulation.AvailableFieldCases.Count == 0)
                    Debug.LogFormat("<color=#FFDEAD>### NO AVAILABLE FIELD CASE FOR MOVE ###</color>");

                foreach(AvailableFieldCase pCase in ActionsManager.Instance.MoveSimulation.AvailableFieldCases)
                {
                    Debug.LogFormat("<color=#FFDEAD>** Move " + pCase.Direction + " to (" + pCase.FieldCase.ToString() + ") : Can move ? ==> " + pCase.CanMove + " **</color>");
                    if (pCase.HasPlayer)
                        Debug.LogFormat("<color=#FFDEAD>** Conflict with " + pCase.HasPlayerPosition + " (" + pCase.HasPlayerColor + ") **</color>");
                    Debug.Log("<color=#FFDEAD>---------------------------</color>");
                }
                break;

            case eAction.PASS:
                PassSimulationResponse lSimulation = ActionsManager.Instance.PassSimulation;
                Debug.LogFormat("<color=#FFDEAD>### PASSER : " + MatchEngine.Instance.Engine.PossessionMain.CurTeamPossession + " " + lSimulation.Passer.Position + " ###</color>");

                foreach (SimulatePassReceiver pReceiver in lSimulation.Receivers)
                {
                    Debug.LogFormat(
                        "<color=#FFDEAD>** Receiver " + pReceiver.Receiver.Position + (
                            (null != pReceiver.ReceiverOpponentDuel) ? ("(Duel Opponent : " + pReceiver.ReceiverOpponentDuel.Position + ")") : ""
                        ) + " **</color>"
                    );

                    foreach (PlayerActionResponse pOpponent in pReceiver.Interceptors)
                        debugLine += pOpponent.Team + " " + pOpponent.Position + " | ";
                    if (!string.IsNullOrEmpty(debugLine)) Debug.LogFormat("<color=#FFDEAD>** Interceptors : " + debugLine + " **</color>");

                    Debug.LogFormat("<color=#FFDEAD>** Trajectory difficulty : " + pReceiver.ColorArrow +" **</color>");
                    Debug.LogFormat("<color=#FFDEAD>** Not Intercepted : " + pReceiver.ProbaIsNotIntercepted +"% **</color>");
                    Debug.LogFormat("<color=#FFDEAD>** Won Reception Duel : " + pReceiver.ProbaReceptionSuccess +"% **</color>");
                    Debug.LogFormat("<color=#FFDEAD>### PASS SUCCESS : " + pReceiver.ProbaPassSuccess +"% ###</color>");

                    Debug.Log("<color=#FFDEAD>---------------------------</color>");
                }
                break;

            /*case eAction.LONG_KICK:
                LongKickSimulationResponse lLKSimulation = ActionsManager.Instance.LongKickSimulation;

                Debug.LogFormat("<color=#FFDEAD>### STRIKER : " + lLKSimulation.Stricker.Team + " " + lLKSimulation.Stricker.Position + " (" + lLKSimulation.Stricker.MaxScore + " - " + lLKSimulation.Stricker.DrawLevel + ") ###</color>");

                debugLine = "";
                foreach (PlayerActionResponse pOpponent in lLKSimulation.Trajectories[0].Interceptors)
                    debugLine += pOpponent.Team + " " + pOpponent.Position + "(" + pOpponent.MaxScore + " - " + pOpponent.DrawLevel + ") | ";
                if (!string.IsNullOrEmpty(debugLine)) Debug.LogFormat("<color=#FFDEAD>** Interceptors ** : " + debugLine + "</color>");

                Debug.LogFormat("<color=#FFDEAD>### GOALKEEPER : " + lLKSimulation.GoalKeeper.Team + " " + lLKSimulation.GoalKeeper.Position + " (" + lLKSimulation.GoalKeeper.MaxScore + " - " + lLKSimulation.GoalKeeper.DrawLevel + ") ###</color>");

                Debug.LogFormat("<color=#FFDEAD>** Not Intercepted : " + lLKSimulation.Trajectories[0].ProbaLongKickIsNotIntercepted + "% **</color>");
                Debug.LogFormat("<color=#FFDEAD>### LONG_KICK SUCCESS : " + lLKSimulation.Trajectories[0].ProbaLongKickSuccess + "% ###</color>");
                Debug.Log(ActionsManager.Instance.LongKickSimulation.ConvertToJson());
                break;*/

            default:
                break;
        }
        #endif
    }

    public  static  void    DebugPreparation(eAction pActionType, string pPreparation, PlayerPVP lFocusedPlayer = null)
    {
        #if DEVELOPMENT_BUILD
        string  debugLine   =   "";

        Debug.LogFormat("<color=#FFDEAD>### " + pActionType + " PREPARED ###</color>");
        switch(pActionType)
        {
            case eAction.MOVE:
                MoveResolution lMovePreparation = ActionsManager.Instance.MovePreparation;
                Debug.LogFormat("<color=#FFDEAD>### MOVING PLAYER : " + lFocusedPlayer.name + " ###</color>");

                Debug.LogFormat("<color=#FFDEAD>** Move " + lMovePreparation.Direction + " to (" + lMovePreparation.FieldCaseTarget.ToString() + ") **</color>");
                if (0 < lMovePreparation.Opponents.Count)
                {
                    debugLine = "";
                    foreach (MoveOpponent pOpponent in lMovePreparation.Opponents)
                        debugLine += pOpponent.Opponent.Team + " " + pOpponent.Opponent.Position + " | ";

                    if (!string.IsNullOrEmpty(debugLine)) Debug.LogFormat("<color=#FFDEAD>** Conflict with " + debugLine + " **</color>");
                }

                return;

            case eAction.SPRINT:
                MoveResolution  lSprintPreparation    =   ActionsManager.Instance.SprintPreparation;
                Debug.LogFormat("<color=#FFDEAD>### MOVING PLAYER : " + lFocusedPlayer.name + " ###</color>");

                Debug.LogFormat("<color=#FFDEAD>** Move " + lSprintPreparation.Direction + " to (" + lSprintPreparation.FieldCaseTarget.ToString() + ") **</color>");
                if (0 < lSprintPreparation.Opponents.Count)
                {
                    debugLine   =   "";
                    foreach (MoveOpponent pOpponent in lSprintPreparation.Opponents)
                        debugLine += pOpponent.Opponent.Team + " " + pOpponent.Opponent.Position + " | ";

                    if (!string.IsNullOrEmpty(debugLine)) Debug.LogFormat("<color=#FFDEAD>** Conflict with " + debugLine + " **</color>");
                }

                return;

            case eAction.PASS:
                SimulatePassReceiver    lResponse   =   ActionsManager.Instance.PassPreparation.Receiver;
                Debug.LogFormat("<color=#FFDEAD>### PASSER : " + MatchEngine.Instance.Engine.PossessionMain.CurTeamPossession + " " + MatchEngine.Instance.Engine.PossessionMain.CurPlayerPossession + " ###</color>");

                Debug.LogFormat(
                    "<color=#FFDEAD>** Receiver " + lResponse.Receiver.Position + (
                        (null != lResponse.ReceiverOpponentDuel) ? ("(Duel Opponent : " + lResponse.ReceiverOpponentDuel.Position + ")") : ""
                    ) + " **</color>"
                );

                foreach (PlayerActionResponse pOpponent in lResponse.Interceptors)
                    debugLine += pOpponent.Team + " " + pOpponent.Position + " | ";
                if (!string.IsNullOrEmpty(debugLine)) Debug.LogFormat("<color=#FFDEAD>** Interceptors : " + debugLine + " **</color>");

                Debug.LogFormat("<color=#FFDEAD>** Trajectory difficulty : " + lResponse.ColorArrow + " **</color>");
                Debug.LogFormat("<color=#FFDEAD>** Not Intercepted : " + lResponse.ProbaIsNotIntercepted + "% **</color>");
                Debug.LogFormat("<color=#FFDEAD>** Won Reception Duel : " + lResponse.ProbaReceptionSuccess + "% **</color>");
                Debug.LogFormat("<color=#FFDEAD>### PASS SUCCESS : " + lResponse.ProbaPassSuccess + "% ###</color>");
                return;

            case eAction.LONG_KICK:
                LongKickPreparationResponse lPreparation    =   new LongKickPreparationResponse();
                lPreparation.Parse(pPreparation);
                Debug.LogFormat("<color=#FFDEAD>### STRIKER : " + lPreparation.Stricker.Team + " " + lPreparation.Stricker.Position + "(" + lPreparation.Stricker.MaxScore + ") ###</color>");

                debugLine   =   "";
                foreach (PlayerActionResponse pOpponent in lPreparation.Interceptors)
                    debugLine += pOpponent.Team + " " + pOpponent.Position + "(" + pOpponent.MaxScore + ") | ";
                if (!string.IsNullOrEmpty(debugLine)) Debug.LogFormat("<color=#FFDEAD>** Interceptors : " + debugLine + " **</color>");

                Debug.LogFormat("<color=#FFDEAD>** Goalkeeper : " + lPreparation.GoalKeeper.Team + " " + lPreparation.GoalKeeper.Position + " (" + lPreparation.GoalKeeper.MaxScore + ") **</color>");
                Debug.LogFormat("<color=#FFDEAD>** Trajectory difficulty : " + lPreparation.ColorArrow + " **</color>");
                return;
        }
        #endif
    }

    public  static  void    DebugResolution(eAction pActionType, string pResolution)
    {
        #if DEVELOPMENT_BUILD
        string debugLine = "";

        switch (pActionType)
        {
            case eAction.PASS:
                PassResolutionResponse lResolution = new PassResolutionResponse();
                lResolution.Parse(pResolution);

                Debug.Log("<color=#FF1493>######## PASS RESOLUTION ########</color>");
                foreach (PlayerActionResponse pOpponent in lResolution.ReceiverOpponentsPressure)
                {
                    debugLine += pOpponent.Team + " " + pOpponent.Position + " | ";
                }
                if (!string.IsNullOrEmpty(debugLine))
                    Debug.LogFormat("<color=#FF1493>** Receiver " + lResolution.Receiver.Position + " Pressure Opponents (" + lResolution.ReceiverOpponentsPressure.Count + ") ** : " + debugLine + "</color>");

                if (null != lResolution.ReceiverOpponentDuel)
                {
                    Debug.LogFormat("<color=#FF1493>** Receiver Duel Opponent " + lResolution.ReceiverOpponentDuel.Position + " **</color>");

                    debugLine = "";
                    foreach (PlayerActionResponse pOpponent in lResolution.ReceiverOpponentDuelOpponentsPressure)
                    {
                        debugLine += pOpponent.Team + " " + pOpponent.Position + " | ";
                    }
                    if (!string.IsNullOrEmpty(debugLine))
                        Debug.LogFormat("<color=#FF1493>** Receiver Duel Opponent " + lResolution.Receiver.Position + " Pressure Opponents ** : " + debugLine + "</color>");
                }
                break;
        }
        #endif
    }

    #endregion
}

﻿namespace Sportfaction.CasualFantasy.Utilities
{
	/// <summary>
	/// Description: Static class contains timestamp methods
	/// Author: Rémi Carreira
	/// </summary>
	public static class Epoch
	{
		#region Public Methods

		/// <summary>
		/// Get unix timestamp
		/// </summary>
		/// <returns>Return an integer who represent the actual timestamp</returns>
		public	static int	GetUnixTimestamp()
		{
			return (int)(System.DateTime.UtcNow.Subtract(new System.DateTime(1970, 1, 1)).TotalSeconds);
		}

		#endregion
	}
}
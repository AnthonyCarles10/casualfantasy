﻿using Sportfaction.CasualFantasy.Events;

using UnityEngine;

namespace Sportfaction.CasualFantasy.Utilities
{
    /// <summary>
    /// Description: Custom class handles button click
    /// Author: Rémi Carreira
    /// </summary>
    public class CustomToggle : MonoBehaviour
	{
		#region Variables
		
		[Header("Game Events")]
		[SerializeField, Tooltip("Event to invoke when toggle is on")]
		private	GameEvent	_onToggleOnEvt	=	null;
		[SerializeField, Tooltip("Event to invoke when toggle is off")]
		private	GameEvent	_onToggleOffEvt	=	null;

		#endregion

		#region Public Methods
		
		public	void OnValueChanged(bool pValue)
		{
			Debug.Log("CustomToggle " + pValue);
			
			if (pValue)
				UtilityMethods.InvokeGameEvent(_onToggleOnEvt);
			else
				UtilityMethods.InvokeGameEvent(_onToggleOffEvt);
		}

		#endregion
	}
}

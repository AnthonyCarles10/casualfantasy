﻿using TMPro;
using UnityEngine;

namespace Sportfaction.CasualFantasy.Utilities
{
	/// <summary>
	/// Description: Class handles text mesh pro outline
	/// Author: Rémi Carreira
	/// </summary>
	public sealed class TextMeshProOutline : MonoBehaviour
	{
		#region Variables

		[Header("Parameters")]
		[SerializeField, Tooltip("Mesh of a text")]
		private	TMP_Text	_textMesh			=	null;
		[SerializeField, Tooltip("Color of the text's outline")]
		private	Color32		_outlineColor		=	new Color32(0, 0, 0, 0);
		[SerializeField, Range(0f, 1f), Tooltip("Thickness of the text's outline")]
		private	float		_outlineThickness	=	0f;

		#endregion

		#region Private Methods

		#if UNITY_EDITOR

		// Default color used to know if modification was done
		private	Color32		_defaultColor		=	new Color32(0, 0, 0, 0);
		// Default thickness used to know if modification was done
		private	float		_defaultThickness	=	0f;

		/// <summary>
		/// MonoBehaviour method called when parameters changed
		/// </summary>
		private void OnValidate()
		{
			if (null != _textMesh)
			{
				if (false == _defaultColor.Equals(_outlineColor) || _defaultThickness != _outlineThickness)
				{
					_defaultColor		=	_outlineColor;
					_defaultThickness	=	_outlineThickness;

					_textMesh.outlineColor	=	_outlineColor;
					_textMesh.outlineWidth	=	_outlineThickness;
					_textMesh.ForceMeshUpdate(true);
				}
			}
		}

		#endif

		#endregion
	}
}

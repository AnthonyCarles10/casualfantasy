﻿using UnityEngine;
using UnityEngine.Assertions;
using System.Collections.Generic;

namespace Sportfaction.CasualFantasy.Utilities
{
    /// <summary>
    /// Class handling pool of GameObject
    /// </summary>
    public class GameObjectPool
    {
        #region Fields

        private GameObject          _prefab     =   null; //!< Prefab to create
        private Transform           _parent     =   null; //!< Parent for every GO created with this pool
        private List<GameObject>    _components =   new List<GameObject>(); //!< Every GO created

        #endregion

        #region Protected Methods

        /// <summary>
        /// Specific constructor
        /// </summary>
        /// <param name="pPrefab">Prefab to create</param>
        /// <param name="pParent">Parent for all components created with this pool</param>
        /// <param name="pMinComponents">Minimum of components to create</param>
        public   GameObjectPool(GameObject pPrefab, Transform pParent, uint pMinComponents)
        {
            _prefab =   pPrefab;
            _parent =   pParent;

            for (uint lIndex = 0; lIndex < pMinComponents; ++lIndex)
            {
                CreateGameObject();
            }
        }

        /// <summary>
        /// Get an unused component
        /// </summary>
        /// <returns>A disable component of type T</returns>
        public   GameObject   GetUnusedComponent()
        {
            for (int lIndex = 0; lIndex < _components.Count; ++lIndex)
            {
                Assert.IsNotNull(_components[lIndex], "[GameObjectPool] GetUnusedComponent() failed, component at index " + lIndex + " is null.");

                if (_components[lIndex] && _components[lIndex].gameObject && false == _components[lIndex].gameObject.activeSelf)
                {
                    return _components[lIndex];
                }
            }
            
            return CreateGameObject();
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Default constructor
        /// </summary>
        private GameObjectPool() {}

        /// <summary>
        /// Create a new component using the prefab
        /// </summary>
        /// <returns>A new component of type T</returns>
        private GameObject       CreateGameObject()
        {
            Assert.IsNotNull(_prefab, "[GameObjectPool] CreateGameObject() failed, _prefab is null.");

            GameObject  lNewGO  =   MonoBehaviour.Instantiate(_prefab, _parent);

            Assert.IsNotNull(lNewGO, "[GameObjectPool] CreateGameObject() failed, lNewGO is null.");

            lNewGO.SetActive(false);

            return lNewGO;
        }

        #endregion
    }
}
﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Sportfaction.CasualFantasy.Utilities
{
    /// <summary>
    /// Possible values that determine toast display time length.
    /// </summary>
    public enum eToastMessageLength
    {
        /// <summary>
        /// Show the toast message for a short period of time.
        /// </summary>
        SHORT,

        /// <summary>
        /// Show the toast message for a long period of time.
        /// </summary>
        LONG
    }

    #region Delegates

    /// <summary>
    /// Delegate that will be called after user clicks a button from alert dialog.
    /// </summary>
    /// <param name="_buttonPressed">Title of the button that was pressed.</param>
    public delegate void AlertDialogCompletion(string _buttonPressed);

    #endregion

    /// <summary>
    /// Provides a cross-platform interface for creating and presenting native user interfaces.
    /// </summary>
    public class Dialog : MonoBehaviour
    {
        #region API

        /// <summary>
        /// Shows the toast. A toast is a view containing a quick little message for the user.
        /// </summary>
        /// <param name="_message">The text message in a Toast view.</param>
        /// <param name="_length">Toast view show duration length.</param>
        /// <example>
        /// The following code example demonstrates how to show a short toast message.
        /// <code>
        /// using UnityEngine;
        /// using System.Collections;
        /// using VoxelBusters.NativePlugins;
        /// 
        /// public class ExampleClass : MonoBehaviour 
        /// {
        /// 	public void ShowToastMessage ()
        /// 	{
        /// 		NPBinding.UI.ShowToast("This is a sample message.", eToastMessageLength.SHORT);
        ///     }
        /// }
        /// </code>
        /// </example>
        /// <remarks>
        /// \note This is an Android only feature. On iOS, this call has no effect.
        /// </remarks>
        public virtual void ShowToast(string _message, eToastMessageLength _length)
        { }

        /// <summary>
        /// Sets position of popover controller to specified position.
        /// </summary>
        /// <description>
        /// Popover controllers are used to Pick Media and to present Share options in iPad. 
        /// And by default, popover controllers are set to (0.0, 0.0) position.
        /// </description>
        /// <param name="_position">Screen position where popover is displayed.</param>
        /// <remarks>
        /// \note This is an iOS only feature. On Android, this call has no effect.
        /// </remarks>
        public virtual void SetPopoverPoint(Vector2 _position)
        { }

        /// <summary>
        /// Sets position of popover controller to last touch position.
        /// </summary>
        /// <description>
        /// Popover controllers are used to Pick Media and to present Share options in iPad. 
        /// And by default, popover controllers are set to (0.0, 0.0) position.
        /// </description>
        /// <remarks>
        /// \note This is an iOS only feature. On Android, this call has no effect.
        /// </remarks>
        public void SetPopoverPointAtLastTouchPosition()
        {
            Vector2 _touchPosition = Vector2.zero;

            #if UNITY_EDITOR
            _touchPosition = Input.mousePosition;
            #else
			if (Input.touchCount > 0)
			{
				_touchPosition		= Input.GetTouch(0).position;
				_touchPosition.y	= Screen.height - _touchPosition.y;
			}
            #endif
            // Set popover position
            SetPopoverPoint(_touchPosition);
        }

        #endregion

        #region Constants

        private const string kDefaultTextForButton = "Ok";

        #endregion

        #region Alert Dialog API's

        /// <summary>
        /// Shows an alert dialog with single button.
        /// </summary>
        /// <param name="_title">The string that appears in the title bar.</param>
        /// <param name="_message">Descriptive text that provides more details than the title.</param>
        /// <param name="_button">Title of action button.</param>
        /// <param name="_onCompletion">Callback that will be called after operation is completed.</param>
        /// <example>
        /// The following code example demonstrates how to show alert dialog with single button.
        /// <code>
        /// using UnityEngine;
        /// using System.Collections;
        /// using VoxelBusters.NativePlugins;
        /// 
        /// public class ExampleClass : MonoBehaviour 
        /// {
        /// 	public void ShowAlertDialog ()
        /// 	{
        /// 		NPBinding.UI.ShowAlertDialogWithSingleButton("Test", "This is a sample message.", "Ok", OnButtonPressed);
        ///     }
        /// 
        /// 	private void OnButtonPressed (string _buttonPressed)
        /// 	{
        /// 		Debug.Log("Button pressed: " + _buttonPressed);
        /// 	}
        /// }
        /// </code>
        /// </example>
        public void ShowAlertDialogWithSingleButton(string _title, string _message, string _button, AlertDialogCompletion _onCompletion)
        {
            ShowAlertDialogWithMultipleButtons(_title, _message, new string[] { _button }, _onCompletion);
        }

        /// <summary>
        /// Shows an alert dialog with multiple buttons.
        /// </summary>
        /// <param name="_title">The string that appears in the title bar.</param>
        /// <param name="_message">Descriptive text that provides more details than the title.</param>
        /// <param name="_buttonsList">An array of string values, used as title of action buttons.</param>
        /// <param name="_onCompletion">Callback that will be called after operation is completed.</param>
        /// <example>
        /// The following code example demonstrates how to show alert dialog with 2 action buttons.
        /// <code>
        /// using UnityEngine;
        /// using System.Collections;
        /// using VoxelBusters.NativePlugins;
        /// 
        /// public class ExampleClass : MonoBehaviour 
        /// {
        /// 	public void ShowAlertDialog ()
        /// 	{
        /// 		string[]	_buttons	= new string[] {
        /// 			"Ok",
        /// 			"Cancel"
        /// 		};
        /// 
        /// 		NPBinding.UI.ShowAlertDialogWithMultipleButtons("Test", "This is a sample message.", _buttons, OnButtonPressed);
        ///     }
        /// 
        /// 	private void OnButtonPressed (string _buttonPressed)
        /// 	{
        /// 		Debug.Log("Button pressed: " + _buttonPressed);
        /// 	}
        /// }
        /// </code>
        /// </example>
        public void ShowAlertDialogWithMultipleButtons(string _title, string _message, string[] _buttons, AlertDialogCompletion _onCompletion)
        {
            // Cache callback
            string _callbackTag = CacheAlertDialogCallback(_onCompletion);

            // Show alert
            ShowAlertDialogWithMultipleButtons(_title, _message, _buttons, _callbackTag);
        }

        protected virtual void ShowAlertDialogWithMultipleButtons(string _title, string _message, string[] _buttons, string _callbackTag)
        {
            if (_buttons == null || _buttons.Length == 0)
            {
                _buttons = new string[] {
                    kDefaultTextForButton // Adding default text
				};
            }
            else if (string.IsNullOrEmpty(_buttons[0]))
            {
                _buttons[0] = kDefaultTextForButton;
            }
        }

        #endregion

        #region Events

        private Dictionary<string, AlertDialogCompletion> m_alertDialogCallbackCollection = new Dictionary<string, AlertDialogCompletion>();

        #endregion

        #region Native Callback Methods

        private void AlertDialogClosed(string _jsonStr)
        {
            IDictionary _jsonData = JSONUtility.FromJSON(_jsonStr) as IDictionary;
            string _buttonPressed;
            string _callerTag;

            // Parse received data
            ParseAlertDialogDismissedData(_jsonData, out _buttonPressed, out _callerTag);

            // Get callback
            AlertDialogCompletion _alertCompletionCallback = GetAlertDialogCallback(_callerTag);

            // Completion callback is triggered
            if (_alertCompletionCallback != null)
                _alertCompletionCallback(_buttonPressed);
        }

        #endregion

        #region Parse Methods

        protected virtual void ParseAlertDialogDismissedData(IDictionary _dataDict, out string _buttonPressed, out string _callerTag)
        {
            _buttonPressed = null;
            _callerTag = null;
        }

        #endregion

        #region Callback Handler Methods

        private string CacheAlertDialogCallback(AlertDialogCompletion _newCallback)
        {
            if (_newCallback != null)
            {
                string _tag = System.Guid.NewGuid().ToString();
                m_alertDialogCallbackCollection[_tag] = _newCallback;

                return _tag;
            }

            return string.Empty;
        }

        protected AlertDialogCompletion GetAlertDialogCallback(string _tag)
        {
            if (!string.IsNullOrEmpty(_tag))
            {
                if (m_alertDialogCallbackCollection.ContainsKey(_tag))
                {
                    return m_alertDialogCallbackCollection[_tag] as AlertDialogCompletion;
                }
            }

            return null;
        }

        #endregion

        #region Prompt Dialog API's

        /// <summary>
        /// Shows a prompt dialog that allows the user to enter text.
        /// </summary>
        /// <param name="_title">The string that appears in the title bar.</param>
        /// <param name="_message">Descriptive text that provides more details than the title.</param>
        /// <param name="_placeholder">The string that is displayed when there is no other text in the textfield.</param>
        /// <param name="_buttonsList">An array of string values, used as title of action buttons.</param>
        /// <param name="_onCompletion">Callback that will be called after operation is completed.</param>
        /// <example>
        /// The following code example demonstrates how to prompt user to enter profile name.
        /// <code>
        /// using UnityEngine;
        /// using System.Collections;
        /// using VoxelBusters.NativePlugins;
        /// 
        /// public class ExampleClass : MonoBehaviour 
        /// {
        /// 	public void ShowPromptDialog ()
        /// 	{
        /// 		string[]	_buttons	= new string[] {
        /// 			"Ok",
        /// 			"Cancel"
        /// 		};
        /// 
        /// 		NPBinding.UI.ShowSingleFieldPromptDialogWithPlainText("Profile", "Please enter a profile name to store your game progress.", "username", _buttons, OnButtonPressed);
        ///     }
        /// 
        /// 	private void OnButtonPressed (string _buttonPressed, string _inputText)
        /// 	{
        /// 		Debug.Log("Button pressed: " + _buttonPressed);
        /// 		Debug.Log("Input text: " + _inputText);
        /// 	}
        /// }
        /// </code>
        /// </example>
        public void ShowSingleFieldPromptDialogWithPlainText(string _title, string _message, string _placeholder, string[] _buttons, SingleFieldPromptCompletion _onCompletion)
        {
            ShowSingleFieldPromptDialog(_title, _message, _placeholder, false, _buttons, _onCompletion);
        }

        /// <summary>
        /// Shows a prompt dialog that allows the user to enter obscure text.
        /// </summary>
        /// <param name="_title">The string that appears in the title bar.</param>
        /// <param name="_message">Descriptive text that provides more details than the title.</param>
        /// <param name="_placeholder">The string that is displayed when there is no other text in the textfield.</param>
        /// <param name="_buttonsList">An array of string values, used as title of action buttons.</param>
        /// <param name="_onCompletion">Callback that will be called after operation is completed.</param>
        public void ShowSingleFieldPromptDialogWithSecuredText(string _title, string _message, string _placeholder, string[] _buttons, SingleFieldPromptCompletion _onCompletion)
        {
            ShowSingleFieldPromptDialog(_title, _message, _placeholder, true, _buttons, _onCompletion);
        }

        protected virtual void ShowSingleFieldPromptDialog(string _title, string _message, string _placeholder, bool _useSecureText, string[] _buttonsList, SingleFieldPromptCompletion _onCompletion)
        {
            // Cache callback
            OnSingleFieldPromptClosed = _onCompletion;
        }

        /// <summary>
        /// Shows a prompt dialog that allows the user to enter login details.
        /// </summary>
        /// <param name="_title">The string that appears in the title bar.</param>
        /// <param name="_message">Descriptive text that provides more details than the title.</param>
        /// <param name="_usernamePlaceHolder">The string that is displayed when there is no other text in the username textfield.</param>
        /// <param name="_passwordPlaceHolder">The string that is displayed when there is no other text in the password textfield.</param>
        /// <param name="_buttonsList">An array of string values, used as title of action buttons.</param>
        /// <param name="_onCompletion">Callback that will be called after operation is completed.</param>
        /// <example>
        /// The following code example demonstrates how to prompt user to enter login details.
        /// <code>
        /// using UnityEngine;
        /// using System.Collections;
        /// using VoxelBusters.NativePlugins;
        /// 
        /// public class ExampleClass : MonoBehaviour 
        /// {
        /// 	public void ShowLoginDialog ()
        /// 	{
        /// 		string[]	_buttons	= new string[] {
        /// 			"Ok",
        /// 			"Cancel"
        /// 		};
        /// 
        /// 		NPBinding.UI.ShowSingleFieldPromptDialogWithPlainText("Example Game", "Please enter login details.", "username", "password", _buttons, OnButtonPressed);
        ///     }
        /// 
        /// 	private void OnButtonPressed (string _buttonPressed, string _usernameText, string _passwordText)
        /// 	{
        /// 		Debug.Log("Button pressed: " + _buttonPressed);
        /// 		Debug.Log("Input username is: " + _usernameText);
        /// 		Debug.Log("Input password is: " + _passwordText);
        /// 	}
        /// }
        /// </code>
        /// </example>
        public virtual void ShowLoginPromptDialog(string _title, string _message, string _usernamePlaceHolder, string _passwordPlaceHolder, string[] _buttons, LoginPromptCompletion _onCompletion)
        {
            // Cache callback
            OnLoginPromptClosed = _onCompletion;
        }

        #endregion

        #region Delegates

        /// <summary>
        /// Delegate that will be called after user clicks a button from prompt dialog.
        /// </summary>
        /// <param name="_buttonPressed">Title of the button that was pressed.</param>
        /// <param name="_inputText">Text contents of textfield.</param>
        public delegate void SingleFieldPromptCompletion(string _buttonPressed, string _inputText);

        /// <summary>
        /// Delegate that will be called after user clicks a button from login prompt dialog.
        /// </summary>
        /// <param name="_buttonPressed">Title of the button that was pressed.</param>
        /// <param name="_usernameText">Text contents of username textfield.</param>
        /// <param name="_passwordText">Text contents of password textfield.</param>
        public delegate void LoginPromptCompletion(string _buttonPressed, string _usernameText, string _passwordText);

        #endregion

        #region Events

        protected SingleFieldPromptCompletion OnSingleFieldPromptClosed;
        protected LoginPromptCompletion OnLoginPromptClosed;

        #endregion

        #region Native Callback Methods

        private void SingleFieldPromptDialogClosed(string _jsonStr)
        {
            if (OnSingleFieldPromptClosed != null)
            {
                IDictionary _dataDict = JSONUtility.FromJSON(_jsonStr) as IDictionary;
                string _buttonPressed;
                string _inputText;

                // Parse received data
                ParseSingleFieldPromptClosedData(_dataDict, out _buttonPressed, out _inputText);

                // Completion callback is triggered
                OnSingleFieldPromptClosed(_buttonPressed, _inputText);
            }
        }

        private void LoginPromptDialogClosed(string _jsonStr)
        {
            if (OnLoginPromptClosed != null)
            {
                IDictionary _jsonData = JSONUtility.FromJSON(_jsonStr) as IDictionary;
                string _buttonPressed;
                string _usernameText;
                string _passwordText;

                // Parse received data
                ParseLoginPromptClosedData(_jsonData, out _buttonPressed, out _usernameText, out _passwordText);

                // Completion callback is triggered
                OnLoginPromptClosed(_buttonPressed, _usernameText, _passwordText);
            }
        }

        #endregion

        #region Parse Methods

        protected virtual void ParseSingleFieldPromptClosedData(IDictionary _dataDict, out string _buttonPressed, out string _inputText)
        {
            _buttonPressed = null;
            _inputText = null;
        }

        protected virtual void ParseLoginPromptClosedData(IDictionary _dataDict, out string _buttonPressed, out string _usernameText, out string _passwordText)
        {
            _buttonPressed = null;
            _usernameText = null;
            _passwordText = null;
        }

        #endregion
    }
}
﻿using Sportfaction.CasualFantasy.Events;

using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.EventSystems;

namespace Sportfaction.CasualFantasy.Utilities
{
	/// <summary>
	/// Description: Class handling basic button methods
	/// Author: Rémi Carreira
	/// </summary>
	public class BasicButton : MonoBehaviour, IPointerClickHandler
	{
		#region Fields

		[SerializeField, Tooltip("Game event to invoke when button is clicked")]
		private	GameEvent		_onClick	=	null;

		#endregion

		#region Public Methods

		/// <summary>
		/// Callback function invoke when pointer click
		/// </summary>
		/// <param name="eventData">Data of the pointer</param>
		public void	OnPointerClick(PointerEventData eventData)
		{
			#if UNITY_EDITOR && DEVELOPMENT_BUILD
				Debug.LogFormat("<color=blue>[BasicButton]</color> OnPointerClick() called on " + this.name);
			#endif

			Assert.IsNotNull(_onClick, "[BasicButton] OnPointerClick(), _onClick is null.");

			_onClick.Invoke();
		}

		#endregion
	}
}

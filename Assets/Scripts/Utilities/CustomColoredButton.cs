﻿using UnityEngine;
using UnityEngine.UI;

namespace Sportfaction.CasualFantasy.Utilities
{
    /// <summary>
    /// Description: Custom class handles button design
    /// Author: Madeleine Tranier
    /// </summary>
    public class CustomColoredButton : MonoBehaviour
    {
        #region Variables

        [SerializeField, Tooltip("Button Image")]
        private Image   _btnImage   =   null;
        [SerializeField, Tooltip("Button Text")]
        private Text    _btnText    =   null;

        #endregion

        #region Public Methods
        
        public  void    Setup(Color32 pColor)
        {
            _btnImage.color =   pColor;
            _btnText.color  =   pColor;
        }
        
        #endregion
    }
}

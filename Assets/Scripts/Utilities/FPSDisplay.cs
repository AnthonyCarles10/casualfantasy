﻿using UnityEngine;
using UnityEngine.Assertions;

namespace Sportfaction.CasualFantasy.Utilities
{
	/// <summary>
	/// Description: Class displaying frames by seconds
	/// Author: Rémi Carreira
	/// </summary>
	public class FPSDisplay : MonoBehaviour
	{
		#region Fields

		[Header("Inputs")]
		[SerializeField, Tooltip("Font size")]
		private	int			_fontSize		=	25;
		[SerializeField, Tooltip("Color used when fps are superior or equal 30 fps")]
		private	Color		_goodColor		=	Color.green;
		[SerializeField, Tooltip("Color used when fps are superior or equal 25 fps")]
		private	Color		_mediumColor	=	Color.yellow;
		[SerializeField, Tooltip("Color used when fps are inferior 25 fps")]
		private	Color		_badColor		=	Color.red;

		[Header("Outputs")]
		[SerializeField, ReadOnly, Tooltip("Delta time used for calculations")]
		private	float		_deltaTime	=	0f;
		[SerializeField, ReadOnly, Tooltip("Frames per seconds")]
		private	float		_fps		=	0f;
		[SerializeField, ReadOnly, Tooltip("Text used to display fps")]
		private	string		_text		=	"";

		private	GUIStyle	_style	=	new GUIStyle();	// GUI Style used to display fps
		private	Rect		_rect;						// Structure contains the rect where fps was displayed

		#endregion

		#region Private Methods

		/// <summary>
		/// Monobehaviour method called to initialize gui style
		/// </summary>
		private	void	Awake()
		{
			Assert.IsNotNull(_style, "[FPSDisplay] Awake(), _style is null.");

			_style.alignment		=	TextAnchor.UpperRight;
			_style.fontSize			=	_fontSize;

			_rect	=	new Rect(0, 0, Screen.width, _fontSize);
		}

		/// <summary>
		/// Monobehaviour method called to update frame per seconds
		/// </summary>
		private	void	Update()
		{
			_deltaTime	+=	(Time.unscaledDeltaTime - _deltaTime) * 0.1f;
		}

		/// <summary>
		/// Monobehaviour method called for rendering and handling GUI events.
		/// </summary>
		private void	OnGUI()
		{
			_text	=	string.Format("{0:0.} fps ", _fps);
			_fps	=	1f / _deltaTime;

			UpdateTextColor();

			GUI.Label(_rect, _text, _style);
		}

		/// <summary>
		/// Update the text color according the number of frames per second
		/// </summary>
		private	void	UpdateTextColor()
		{
			Assert.IsNotNull(_style, "[FPSDisplay] UpdateTextColor(), _style is null.");

			if (_fps >= 30f)
			{
				_style.normal.textColor	=	_goodColor;
			}
			else if (_fps >= 25f)
			{
				_style.normal.textColor	=	_mediumColor;
			}
			else
			{
				_style.normal.textColor	=	_badColor;
			}
		}

		#endregion
	}
}

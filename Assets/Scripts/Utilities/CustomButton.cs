﻿using DG.Tweening;
using Sportfaction.CasualFantasy.Events;

using UnityEngine;
using UnityEngine.EventSystems;

namespace Sportfaction.CasualFantasy.Utilities
{
	/// <summary>
	/// Description: Custom class handles button click
	/// Author: Rémi Carreira
	/// </summary>
	public class CustomButton : MonoBehaviour, IPointerDownHandler, IPointerEnterHandler, IPointerExitHandler, IPointerUpHandler
	{
		#region Serialized Fields

		[SerializeField, Tooltip("Game event invoke when button is clicked")]
		private	GameEvent	_onButtonClicked	=	null;
        [SerializeField, Tooltip("Should zoom on touch ?")]
        private bool        _shouldZoom         =   true;

		#endregion
        
        #region Private Fields
        
        private bool    _btnDown    =   false; //!< Is player touched ?
        private bool    _mouseDown  =   false; //!< Is mouse down ?
        
        #endregion

		#region Public Methods

        public  void    OnPointerDown(PointerEventData pEventData)
        {
            _btnDown    =   true;
            _toggleActiveBtn(true);
        }

        public  void    OnPointerEnter(PointerEventData eventData)
        {
            if (null != eventData.pointerPress || _mouseDown || Input.touchCount > 0)
            {
                _btnDown    =   true;
                _toggleActiveBtn(true);
            }
        }

        public  void    OnPointerExit(PointerEventData eventData)
        {
            _btnDown    =   false;
            _toggleActiveBtn(false);
        }

        public  void    OnPointerUp(PointerEventData eventData)
        {
            if (_btnDown && null != _onButtonClicked)
            {
                this.transform.localScale = new Vector3(1f, 1f, 1f);
				_onButtonClicked.Invoke();
            }
            else
                _toggleActiveBtn(false);

            _btnDown    =   false;
        }

        #endregion

		#region Private Methods

        /// <summary>
        /// Checks whether mouse is down or up
        /// </summary>
        private void    Update()
        {
            if (Input.GetMouseButtonDown(0))
            {
                _mouseDown  =   true;
            }
            if (Input.GetMouseButtonUp(0))
            {
                _mouseDown  =   false;
                if (_btnDown)
                    OnPointerUp(null);
            }
        }

        private void    _toggleActiveBtn(bool pActive)
        {
            if (_shouldZoom)
                this.transform.DOScale(pActive ? new Vector3(1.3f, 1.3f, 1f) : new Vector3(1f, 1f, 1f), 0.25f);
        }

        #endregion
    }
}

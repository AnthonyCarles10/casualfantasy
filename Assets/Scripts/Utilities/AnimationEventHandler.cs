﻿using Sportfaction.CasualFantasy.Events;
using UnityEngine;

namespace Sportfaction.CasualFantasy.PVP.Gameplay
{
    /// <summary>
    /// Class handling animation custom behaviour
    /// </summary>
    public sealed class AnimationEventHandler : CustomAnimHandler
    {
        #region Fields

        [SerializeField, Tooltip("Event to send")]
        private GameEvent   _eventToSend    =   null;

        #endregion

        #region Public Methods

        public  void    SendEvent()
        {
            UtilityMethods.InvokeGameEvent(_eventToSend);
        }

        #endregion
    }
}
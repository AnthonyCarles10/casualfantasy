﻿using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.SceneManagement;

namespace Sportfaction.CasualFantasy.Utilities
{
	/// <summary>
	/// Description: Load new scene on start
	/// Author: Rémi Carreira
	/// </summary>
	public class AutomaticSceneLoader : MonoBehaviour
	{
		#region Properties

		[SerializeField, Tooltip("Name of the level to load.")]
		private	string			_sceneName	=	"";	// Name of the scene to load
		[SerializeField, Tooltip("Mode used to load a new scene.")]
		private	LoadSceneMode	_loadMode	=	LoadSceneMode.Single;	// Mode used to load a new scene
		[SerializeField, Tooltip("Load new scene asynchronous")]
		private	bool			_loadAsync	=	false;	// Load new scene asynchronous

		#endregion

		#region Private Methods

		/// <summary>
		/// Unity Method | Load new scene
		/// </summary>
		private	void	Start()
		{
			Assert.IsFalse(string.IsNullOrEmpty(_sceneName), "AutomaticSceneLoader: Start() failed, can't load scene with null or empty scene name.");

			if (true == _loadAsync)
			{
				SceneManager.LoadSceneAsync(_sceneName, _loadMode);
			}
			else
			{
				SceneManager.LoadScene(_sceneName, _loadMode);
			}
		}

		#endregion
	}
}
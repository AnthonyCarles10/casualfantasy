﻿using Sportfaction.CasualFantasy.References;

using System.Reflection;
using UnityEditor;
using UnityEngine;

namespace Sportfaction.CasualFantasy.Editor
{
	/// <summary>
	/// Description: 
	/// Author: Rémi Carreira
	/// </summary>
	public class MatchSettingsWindow : EditorWindow
	{
		#region Variables

		// Integer reference to the first period timer (in milliseconds)
		private	IntReference	_firstPeriodTimerRef	=	null;
		// Integer reference to the second period timer (in milliseconds)
		private	IntReference	_secondPeriodTimerRef	=	null;
		// Integer reference to the half time timer (in milliseconds)
		private	IntReference	_halfTimeTimerRef		=	null;
		// Integer reference to the toss timer (in milliseconds)
		private	IntReference	_tossTimerRef			=	null;
		// Integer reference to the duel duration (in milliseconds)
		private	IntReference	_duelTimerRef			=	null;

		// Temporary background color
		private	Color	_tmpBackgroundColor	=	Color.clear;

		// Current duration for a match (in milliseconds)
		private	int		_matchDuration		=	0;
		// Current duration for first and second period (in milliseconds)
		private	int		_periodsDuration	=	0;
		// Current duration for the half time
		private	int		_halfTimeDuration	=	0;
		// Current duration for the toss
		private	int		_tossDuration		=	0;
		// Duration of a duel (in milliseconds)
		private	int		_duelDuration		=	0;
		// Number of turns
		private	int		_numberOfTurns		=	0;

		#endregion

		#region Public Methods

		/// <summary>
		/// Show a match settings window
		/// </summary>
		[MenuItem("Window/Match settings")]
		public	static	void	ShowWindow()
		{
			EditorWindow.GetWindow(typeof(MatchSettingsWindow), false, "Match Settings", true);
		}

		#endregion

		#region Private Methods

		/// <summary>
		/// Custom editor GUI
		/// </summary>
		private	void	OnGUI()
		{
			EditorGUILayout.LabelField("Settings", EditorStyles.centeredGreyMiniLabel);

			EditorGUILayout.LabelField("", GUI.skin.horizontalSlider);

			ShowReferences();

			EditorGUILayout.LabelField("", GUI.skin.horizontalSlider);

			ShowVariables();

			EditorGUILayout.LabelField("", GUI.skin.horizontalSlider);

			EditorGUILayout.BeginHorizontal();

			ShowSaveButton();

			ShowResetButton();

			EditorGUILayout.EndHorizontal();
		}

		/// <summary>
		/// Show references fields
		/// </summary>
		private	void	ShowReferences()
		{
			EditorGUILayout.LabelField("References", EditorStyles.boldLabel);

			ShowPeriodsTimerRef();

			ShowHalfTimerRef();

			ShowTossTimerRef();

			ShowDuelDurationRef();

			CheckReferences();
		}

		/// <summary>
		/// Show periods timer
		/// </summary>
		private	void	ShowPeriodsTimerRef()
		{
			EditorGUI.BeginChangeCheck();

			_firstPeriodTimerRef	=	EditorGUILayout.ObjectField("First period timer", _firstPeriodTimerRef, typeof(IntReference), true) as IntReference;
			_secondPeriodTimerRef	=	EditorGUILayout.ObjectField("Second period timer", _secondPeriodTimerRef, typeof(IntReference), true) as IntReference;

			if (true == EditorGUI.EndChangeCheck() && null != _firstPeriodTimerRef && null != _secondPeriodTimerRef)
			{
				//_periodsDuration	=	(_firstPeriodTimerRef.DefaultValue <= _secondPeriodTimerRef.DefaultValue) ? _firstPeriodTimerRef.DefaultValue : _secondPeriodTimerRef.DefaultValue;
			}
		}

		/// <summary>
		/// Show half time timer
		/// </summary>
		private	void	ShowHalfTimerRef()
		{
			EditorGUI.BeginChangeCheck();

			_halfTimeTimerRef		=	EditorGUILayout.ObjectField("Half time timer", _halfTimeTimerRef, typeof(IntReference), true) as IntReference;

			if (true == EditorGUI.EndChangeCheck() && null != _halfTimeTimerRef)
			{
				_halfTimeDuration	=	_halfTimeTimerRef.DefaultValue;

				ComputeMatchDuration();
			}

		}

		/// <summary>
		/// Show toss timer
		/// </summary>
		private	void	ShowTossTimerRef()
		{
			EditorGUI.BeginChangeCheck();

			_tossTimerRef			=	EditorGUILayout.ObjectField("Toss timer", _tossTimerRef, typeof(IntReference), true) as IntReference;

			if (true == EditorGUI.EndChangeCheck() && null != _tossTimerRef)
			{
				_tossDuration	=	_tossTimerRef.DefaultValue;

				ComputeMatchDuration();
			}
		}

		/// <summary>
		/// Show duel duration reference
		/// </summary>
		private	void	ShowDuelDurationRef()
		{
			EditorGUI.BeginChangeCheck();

			_duelTimerRef		=	EditorGUILayout.ObjectField("Duel timer", _duelTimerRef, typeof(IntReference), true) as IntReference; 

			if (true == EditorGUI.EndChangeCheck() && null != _duelTimerRef)
			{
				_duelDuration	=	_duelTimerRef.DefaultValue;

				ComputePeriodsDuration();

				ComputeMatchDuration();
			}
		}

		/// <summary>
		/// Check references
		/// </summary>
		private	void	CheckReferences()
		{
			if (null == _firstPeriodTimerRef)
			{
				EditorGUILayout.HelpBox("First period timer reference is missing", MessageType.Error);
				return;
			}
			if (null == _secondPeriodTimerRef)
			{
				EditorGUILayout.HelpBox("Second period timer reference is missing", MessageType.Error);
				return;
			}
			if (null == _halfTimeTimerRef)
			{
				EditorGUILayout.HelpBox("Half time timer reference is missing", MessageType.Error);
				return;
			}
			if (null == _tossTimerRef)
			{
				EditorGUILayout.HelpBox("Toss timer reference is missing", MessageType.Error);
				return;
			}
			if (null == _duelTimerRef)
			{
				EditorGUILayout.HelpBox("Duel timer reference is missing", MessageType.Error);
				return;
			}
		}

		/// <summary>
		/// Show variables fields
		/// </summary>
		private void	ShowVariables()
		{
			EditorGUILayout.LabelField("Variables", EditorStyles.boldLabel);

			GUI.enabled = false;

			ShowMatchDuration();

			ShowPeriodsDuration();

			GUI.enabled	=	(null != _firstPeriodTimerRef && null != _secondPeriodTimerRef && null != _halfTimeTimerRef && null != _tossTimerRef && null != _duelTimerRef);

			ShowHalfTimeDuration();

			ShowTossDuration();

			ShowDuelDuration();

			ShowNumberOfTurns();

			GUI.enabled	=	true;
		}

		/// <summary>
		/// Show match duration
		/// </summary>
		private	void	ShowMatchDuration()
		{
			EditorGUILayout.IntField("Match duration (ms)", _matchDuration);
		}

		/// <summary>
		/// Show periods duration
		/// </summary>
		private	void	ShowPeriodsDuration()
		{
			EditorGUILayout.IntField("Periods duration (ms)", _periodsDuration);
		}

		/// <summary>
		/// Show duel duration
		/// </summary>
		private	void	ShowDuelDuration()
		{
			EditorGUI.BeginChangeCheck();

			_duelDuration = EditorGUILayout.IntField("Duel duration (ms)", _duelDuration);

			if (true == EditorGUI.EndChangeCheck())
			{
				ComputePeriodsDuration();

				ComputeMatchDuration();

				UpdatePeriodsRef();

				Debug.Assert(null != _duelTimerRef, "[MatchSettingsWindow] ShowDuelDuration(), _duelTimerRef is null.");

				_duelTimerRef.GetType().BaseType.GetField("_defaultValue", BindingFlags.NonPublic | BindingFlags.Instance).SetValue(_duelTimerRef, _duelDuration);
			}
		}

		/// <summary>
		/// Show number of turns
		/// </summary>
		private	void	ShowNumberOfTurns()
		{
			EditorGUI.BeginChangeCheck();

			_numberOfTurns = EditorGUILayout.IntField("Number of turns", _numberOfTurns);

			if (true == EditorGUI.EndChangeCheck())
			{
				ComputePeriodsDuration();

				ComputeMatchDuration();

				UpdatePeriodsRef();
			}
		}

		/// <summary>
		/// Show half time duration
		/// </summary>
		private	void	ShowHalfTimeDuration()
		{
			EditorGUI.BeginChangeCheck();

			_halfTimeDuration = EditorGUILayout.IntField("Half time duration (ms)", _halfTimeDuration);

			if (true == EditorGUI.EndChangeCheck())
			{
				ComputeMatchDuration();

				Debug.Assert(null != _halfTimeTimerRef, "[MatchSettingsWindow] ShowHalfTimeDuration(), _halfTimeTimerRef is null.");

				_halfTimeTimerRef.GetType().BaseType.GetField("_defaultValue", BindingFlags.NonPublic | BindingFlags.Instance).SetValue(_halfTimeTimerRef, _halfTimeDuration);
			}
		}

		/// <summary>
		/// Show toss duration
		/// </summary>
		private	void	ShowTossDuration()
		{
			EditorGUI.BeginChangeCheck();

			_tossDuration = EditorGUILayout.IntField("Toss duration (ms)", _tossDuration);

			if (true == EditorGUI.EndChangeCheck())
			{
				ComputeMatchDuration();

				Debug.Assert(null != _tossTimerRef, "[MatchSettingsWindow] ShowTossDuration(), _tossTimerRef is null.");

				_tossTimerRef.GetType().BaseType.GetField("_defaultValue", BindingFlags.NonPublic | BindingFlags.Instance).SetValue(_tossTimerRef, _tossDuration);
			}
		}

		/// <summary>
		/// Show save button
		/// </summary>
		private	void	ShowSaveButton()
		{
			_tmpBackgroundColor	=	GUI.contentColor;

			GUI.backgroundColor	=	Color.green;

			if (true == GUILayout.Button("Save"))
			{
				SaveIntReference(_firstPeriodTimerRef);
				SaveIntReference(_secondPeriodTimerRef);
				SaveIntReference(_halfTimeTimerRef);
				SaveIntReference(_tossTimerRef);
				SaveIntReference(_duelTimerRef);

				AssetDatabase.SaveAssets();
			}

			GUI.backgroundColor	=	_tmpBackgroundColor;
		}

		/// <summary>
		/// Show a button to reset references and variables
		/// </summary>
		private	void	ShowResetButton()
		{
			_tmpBackgroundColor	=	GUI.contentColor;

			GUI.backgroundColor	=	Color.red;

			if (true == GUILayout.Button("Reset"))
			{
				_firstPeriodTimerRef	=	null;
				_secondPeriodTimerRef	=	null;
				_halfTimeTimerRef		=	null;
				_tossTimerRef			=	null;
				_duelTimerRef			=	null;

				_matchDuration			=	0;
				_halfTimeDuration		=	0;
				_tossDuration			=	0;
				_duelDuration			=	0;
				_numberOfTurns			=	0;
			}

			GUI.backgroundColor	=	_tmpBackgroundColor;
		}

		/// <summary>
		/// Compute match duration
		/// </summary>
		private	void	ComputeMatchDuration()
		{
			_matchDuration		=	(_periodsDuration * 2) + _halfTimeDuration + _tossDuration;
		}

		/// <summary>
		/// Compute periods duration
		/// </summary>
		private	void	ComputePeriodsDuration()
		{
			_periodsDuration	=	(_numberOfTurns * _duelDuration) / 2;
		}

		/// <summary>
		/// Update periods references
		/// </summary>
		private	void	UpdatePeriodsRef()
		{
			if (null != _firstPeriodTimerRef)
			{
				_firstPeriodTimerRef.GetType().BaseType.GetField("_defaultValue", BindingFlags.NonPublic | BindingFlags.Instance).SetValue(_firstPeriodTimerRef, _periodsDuration);
			}
			if (null != _secondPeriodTimerRef)
			{
				_secondPeriodTimerRef.GetType().BaseType.GetField("_defaultValue", BindingFlags.NonPublic | BindingFlags.Instance).SetValue(_secondPeriodTimerRef, _periodsDuration);
			}
		}

		/// <summary>
		/// Save integer reference
		/// </summary>
		/// <param name="pReference">Integer reference to save</param>
		private	void	SaveIntReference(IntReference pReference)
		{
			if (null != pReference)
			{
				EditorUtility.SetDirty(pReference);
			}
		}

		#endregion
	}
}

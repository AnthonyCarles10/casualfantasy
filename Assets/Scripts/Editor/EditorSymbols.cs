﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine.Assertions;

namespace Sportfaction.CasualFantasy.Editor
{
	/// <summary>
	/// Description: Editor class used to manage builds infos
	/// Author: Rémi Carreira
	/// </summary>
	[InitializeOnLoad]
	public sealed class EditorSymbols
	{
		#region Variables

		// Development build symbol
		private	readonly static	string	DevSymbol	=	"DEVELOPMENT_BUILD";

		// List contains all current symbols used
		private	static	List<string>	_symbols	=	new List<string>();

		#endregion

		#region Private Methods

		/// <summary>
		/// Constructor
		/// </summary>
		static	EditorSymbols()
		{
			RefreshSymbolsList();

			RemoveDuplicateSymbols();

			ApplyScriptingSymbols();
		}

		/// <summary>
		/// Add development build symbol
		/// </summary>
		[MenuItem("SF/Scripting Symbols/DevelopmentBuild/Add")]
		private	static	void	AddDevelopmentBuildSymbol()
		{
			AddSymbol(DevSymbol);
		}

		/// <summary>
		/// Validate method used to enable/disable button for adding development build
		/// </summary>
		[MenuItem("SF/Scripting Symbols/DevelopmentBuild/Add", true)]
		private	static	bool	Validate_AddDevelopmentBuildSymbol()
		{
			Assert.IsNotNull(_symbols, "[EditorSymbols] Validate_AddDevelopmentBuildSymbol(), _symbols is null.");

			return !_symbols.Contains(DevSymbol);
		}

		/// <summary>
		/// Remove development build symbol
		/// </summary>
		[MenuItem("SF/Scripting Symbols/DevelopmentBuild/Remove")]
		private	static	void	RemoveDevelopmentBuildSymbol()
		{
			RemoveSymbol(DevSymbol);
		}

		/// <summary>
		/// Validate method used to enable/disable button for removing development build
		/// </summary>
		[MenuItem("SF/Scripting Symbols/DevelopmentBuild/Remove", true)]
		private	static	bool	Validate_RemoveDevelopmentBuildSymbol()
		{
			Assert.IsNotNull(_symbols, "[EditorSymbols] Validate_RemoveDevelopmentBuildSymbol(), _symbols is null.");

			return _symbols.Contains(DevSymbol);
		}

		/// <summary>
		/// Add a specific symbol
		/// </summary>
		/// <param name="pSymbol">Symbol to add</param>
		private	static	void	AddSymbol(string pSymbol)
		{
			Assert.IsNotNull(_symbols, "[EditorSymbols] AddSymbol(), _symbols is null.");

			_symbols.Add(DevSymbol);

			ApplyScriptingSymbols();
		}

		/// <summary>
		/// Remove a specific symbol
		/// </summary>
		/// <param name="pSymbol">Symbol to remove</param>
		private	static	void	RemoveSymbol(string pSymbol)
		{
			Assert.IsNotNull(_symbols, "[EditorSymbols] RemoveSymbol(), _symbols is null.");

			_symbols.Remove(DevSymbol);

			ApplyScriptingSymbols();
		}

		/// <summary>
		/// Refresh symbols list
		/// </summary>
		private static	void	RefreshSymbolsList()
		{
			Assert.IsNotNull(_symbols, "[EditorSymbols] RefreshSymbolsList(), _symbols is null.");

			_symbols.Clear();

			string	lSymbolsString	=	PlayerSettings.GetScriptingDefineSymbolsForGroup(EditorUserBuildSettings.selectedBuildTargetGroup);

			_symbols.AddRange(lSymbolsString.Split(';'));
		}

		/// <summary>
		/// Remove duplicate symbols contains on the list
		/// </summary>
		private	static void	RemoveDuplicateSymbols()
		{
			Assert.IsNotNull(_symbols, "[EditorSymbols] RemoveDuplicateSymbols(), _symbols is null.");

			_symbols.Sort();

			int	lIndex	=	0;
			while (lIndex < _symbols.Count - 1)
			{
				if (_symbols[lIndex] == _symbols[lIndex + 1])
				{
					_symbols.RemoveAt(lIndex);
				}
				else
				{
					++lIndex;
				}
			}
		}

		/// <summary>
		/// Apply scripting symbols
		/// </summary>
		private	static	void	ApplyScriptingSymbols()
		{
			PlayerSettings.SetScriptingDefineSymbolsForGroup(EditorUserBuildSettings.selectedBuildTargetGroup, string.Join(";", _symbols.ToArray()));
		}

		#endregion
	}
}

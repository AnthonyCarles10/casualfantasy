﻿//using UnityEngine;
//using UnityEditor;
//using UnityEngine.SceneManagement;
//using UnityEditor.SceneManagement;
//using System.Collections.Generic;

//namespace Sportfaction.CasualFantasy.Editor.Tests
//{
//	/// <summary>
//	/// Description: 
//	/// Author: Rémi Carreira
//	/// </summary>
//	[CreateAssetMenu(fileName = "New Unit Test", menuName = "CasualFantasy/Tests/Unit")]
//	public sealed class UnitTest : ScriptableObject
//	{
//		#region Fields

//		public	int				CurrentIndex	=	0;
//		public	string			ScenePath		=	"";
//		public	List<Unit>		Units;

//		#endregion

//		#region Public Methods

//		public	UnitTest()
//		{
//			Units	=	new List<Unit>();
//		}

//		public	void	LaunchScene()
//		{
//			EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo();

//			EditorSceneManager.sceneOpened	+=	OnSceneOpened;

//			EditorSceneManager.OpenScene(ScenePath, OpenSceneMode.Single);
//		}

//		public	void	Play()
//		{
//			if (eUnit.Pause == Units[CurrentIndex].UnitType)
//			{
//				++CurrentIndex;
//			}

//			while (CurrentIndex < Units.Count && eUnit.Pause != Units[CurrentIndex].UnitType)
//			{
//				Units[CurrentIndex].ExecuteUnit();

//				++CurrentIndex;
//			}
//		}

//		#endregion

//		#region Private Methods

//		private	void	OnEnable()
//		{
//			CurrentIndex	=	0;
//		}

//		private	void	OnSceneOpened(Scene pScene, OpenSceneMode pSceneMode)
//		{
//			EditorSceneManager.sceneOpened	-=	OnSceneOpened;

//			EditorApplication.isPlaying	=	true;
//		}

//		#endregion
//	}
//}
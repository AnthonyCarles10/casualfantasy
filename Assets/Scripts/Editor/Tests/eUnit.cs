﻿namespace Sportfaction.CasualFantasy.Editor.Tests
{
	/// <summary>
	/// Description: Enum define the type of Unit
	/// Author: Rémi Carreira
	/// </summary>
	public enum eUnit
	{
		Pause,
		InvokeGameEvent,
		ChangeBoolReferenceValue,
		ChangeByteReferenceValue,
		ChangeFloatReferenceValue,
		ChangeIntReferenceValue,
		ChangeSByteReferenceValue,
		ChangeStringReferenceValue,
		ChangeUIntReferenceValue,
		ChangePVPEventTypeReferenceValue,
		ChangePVPCardReferenceValue,
		ChangePVPTokenReferenceValue,
	}
}
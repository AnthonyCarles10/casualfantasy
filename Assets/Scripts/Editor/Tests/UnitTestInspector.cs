﻿//using UnityEngine;
//using UnityEditor;
//using Sportfaction.CasualFantasy.Events;
//using Sportfaction.CasualFantasy.References;
//using Sportfaction.CasualFantasy.References.CustomClass;
//using Sportfaction.CasualFantasy.PVP.Gameplay.Cards;
//using Sportfaction.CasualFantasy.PVP.Gameplay.Tokens;

//namespace Sportfaction.CasualFantasy.Editor.Tests
//{
//	/// <summary>
//	/// Description: Class used to override the inspector of Unit Test
//	/// Author: Rémi Carreira
//	/// </summary>
//	[CustomEditor(typeof(UnitTest))]
//	public sealed class UnitTestInspector : UnityEditor.Editor
//	{
//		#region Fields

//		private	Color	_defaultColor	=	Color.white;	// Storage the gui default color

//		#endregion

//		#region Public Methods

//		/// <summary>
//		/// Override Editor method to create a custom inspector
//		/// </summary>
//		public override void OnInspectorGUI()
//		{
//			UnitTest	lUnitTest	=	target as UnitTest;

//			if (null != lUnitTest)
//			{
//				ShowScenePath(lUnitTest);

//				ShowUnits(lUnitTest);

//				ShowButtons(lUnitTest);
//			}
//		}

//		#endregion

//		#region Private Methods

//		/// <summary>
//		/// Display scene path in inspector
//		/// </summary>
//		/// <param name="pUnitTest">Data to modify</param>
//		private	void	ShowScenePath(UnitTest pUnitTest)
//		{
//			EditorGUI.BeginChangeCheck();

//			pUnitTest.ScenePath	=	EditorGUILayout.TextField("Scene Path:", pUnitTest.ScenePath);

//			if (true == EditorGUI.EndChangeCheck())
//			{
//				SaveModifications(pUnitTest);
//			}

//			AddSeparator();
//		}

//		/// <summary>
//		/// Display all units in inspector
//		/// </summary>
//		/// <param name="pUnitTest">Data to modify</param>
//		private	void	ShowUnits(UnitTest pUnitTest)
//		{
//			_defaultColor = GUI.color;

//			EditorGUI.BeginChangeCheck();

//			EditorGUILayout.LabelField("Units");

//			++EditorGUI.indentLevel;

//			EditorGUILayout.LabelField("Size: " + pUnitTest.Units.Count);

//			EditorGUILayout.Space();

//			for (int lIndex = 0; lIndex < pUnitTest.Units.Count; ++lIndex)
//			{
//				ChangeUnitColor(pUnitTest, lIndex);

//				EditorGUILayout.BeginVertical("Box");

//				ShowUnit(pUnitTest, lIndex);

//				if (true == ShowRemoveUnitButton(pUnitTest, lIndex))
//				{
//					break;
//				}

//				EditorGUILayout.EndVertical();

//				AddDoubleSpace();
//			}

//			--EditorGUI.indentLevel;

//			GUI.color	=	_defaultColor;

//			if (true == EditorGUI.EndChangeCheck())
//			{
//				SaveModifications(pUnitTest);
//			}

//			AddSeparator();
//		}


//		/// <summary>
//		/// Display an unit in inspector
//		/// </summary>
//		/// <param name="pUnitTest">Data containing the unit to display</param>
//		/// <param name="pUnitIndex">Index of the unit to display</param>
//		private	void	ShowUnit(UnitTest pUnitTest, int pUnitIndex)
//		{
//			Unit	lUnit	=	pUnitTest.Units[pUnitIndex];

//			EditorGUILayout.LabelField("Unit " + pUnitIndex + ":");

//			++EditorGUI.indentLevel;

//			lUnit.UnitType = (eUnit)EditorGUILayout.EnumPopup("Unit Type", lUnit.UnitType);

//			switch (lUnit.UnitType)
//			{
//				case eUnit.ChangeBoolReferenceValue:
//					lUnit.BoolRefValue		=	EditorGUILayout.ObjectField("Bool Reference to modify", lUnit.BoolRefValue, typeof(BoolReference), false) as BoolReference;
//					lUnit.BoolValue			=	EditorGUILayout.Toggle("Bool Value to use", lUnit.BoolValue);
//					break;
//				case eUnit.ChangeByteReferenceValue:
//					lUnit.ByteRefValue		=	EditorGUILayout.ObjectField("Byte Reference to modify", lUnit.ByteRefValue, typeof(ByteReference), false) as ByteReference;
//					lUnit.ByteValue			=	(byte)EditorGUILayout.IntField("Byte Value to use", lUnit.ByteValue);
//					break;
//				case eUnit.ChangeFloatReferenceValue:
//					lUnit.FloatRefValue		=	EditorGUILayout.ObjectField("Float Reference to modify", lUnit.ByteRefValue, typeof(FloatReference), false) as FloatReference;
//					lUnit.FloatValue		=	EditorGUILayout.FloatField("Float Value to use", lUnit.FloatValue);
//					break;
//				case eUnit.ChangeIntReferenceValue:
//					lUnit.IntRefValue		=	EditorGUILayout.ObjectField("Int Reference to modify", lUnit.ByteRefValue, typeof(IntReference), false) as IntReference;
//					lUnit.IntValue			=	EditorGUILayout.IntField("Int Value to use", lUnit.IntValue);
//					break;
//				case eUnit.ChangeSByteReferenceValue:
//					lUnit.SByteRefValue		=	EditorGUILayout.ObjectField("SByte Reference to modify", lUnit.ByteRefValue, typeof(SByteReference), false) as SByteReference;
//					lUnit.SByteValue		=	(sbyte)EditorGUILayout.IntField("SByte Value to use", lUnit.SByteValue);
//					break;
//				case eUnit.ChangeStringReferenceValue:
//					lUnit.StringRefValue	=	EditorGUILayout.ObjectField("String Reference to modify", lUnit.StringRefValue, typeof(StringReference), false) as StringReference;
//					lUnit.StringValue		=	EditorGUILayout.TextField("String value to use", lUnit.StringValue);
//					break;
//				case eUnit.ChangeUIntReferenceValue:
//					lUnit.UIntRefValue		=	EditorGUILayout.ObjectField("UInt Reference to modify", lUnit.ByteRefValue, typeof(UIntReference), false) as UIntReference;
//					lUnit.UIntValue			=	(uint)EditorGUILayout.LongField("UInt Value to use", lUnit.UIntValue);
//					break;
//				case eUnit.ChangePVPCardReferenceValue:
//					lUnit.PVPCardRef		=	EditorGUILayout.ObjectField("PVP Card Ref to modify", lUnit.PVPCardRef, typeof(PVPCardReference), false) as PVPCardReference;
//					lUnit.PVPCardValue		=	EditorGUILayout.ObjectField("PVP Card Value to use", lUnit.PVPCardValue, typeof(Card), false) as Card;
//					break;
//				case eUnit.ChangePVPTokenReferenceValue:
//					lUnit.PVPTokenRef		=	EditorGUILayout.ObjectField("PVP Token Ref to modify", lUnit.PVPTokenRef, typeof(PVPTokenReference), false) as PVPTokenReference;
//					lUnit.PVPTokenValue		=	EditorGUILayout.ObjectField("PVP Token Value", lUnit.PVPTokenValue, typeof(Token), false) as Token;
//					break;
//				case eUnit.InvokeGameEvent:
//					lUnit.GameEventValue	=	EditorGUILayout.ObjectField("Game Event To Invoke", lUnit.GameEventValue, typeof(GameEvent), false) as GameEvent;
//					break;
//				default:
//					break;
//			}

//			--EditorGUI.indentLevel;
//		}

//		/// <summary>
//		/// Change color used to display unit in inspector
//		/// </summary>
//		/// <param name="pUnitTest">Data containing units</param>
//		/// <param name="pUnitIndex">The current unit index</param>
//		private	void	ChangeUnitColor(UnitTest pUnitTest, int pUnitIndex)
//		{
//			if (true == Application.isPlaying)
//			{
//				if (pUnitIndex > pUnitTest.CurrentIndex)
//				{
//					GUI.color	=	Color.red;
//				}
//				else if (pUnitIndex < pUnitTest.CurrentIndex)
//				{
//					GUI.color	=	Color.green;
//				}
//				else
//				{
//					GUI.color	=	Color.cyan;
//				}
//			}
//		}

//		/// <summary>
//		/// Show remove unit button
//		/// </summary>
//		/// <param name="pUnitTest">Data containing units</param>
//		/// <param name="pUnitIndex">The current unit index</param>
//		/// <returns>True if user remove this unit</returns>
//		private	bool	ShowRemoveUnitButton(UnitTest pUnitTest, int pUnitIndex)
//		{
//			if (false == Application.isPlaying && true == GUILayout.Button("Remove"))
//			{
//				pUnitTest.Units.RemoveAt(pUnitIndex);
//				return true;
//			}
//			return false;
//		}

//		/// <summary>
//		/// Show specific buttons in inspector relative to the current play mode
//		/// </summary>
//		/// <param name="pUnitTest">Data to modify</param>
//		private	void	ShowButtons(UnitTest pUnitTest)
//		{
//			if (false == Application.isPlaying)
//			{
//				ShowEditButtons(pUnitTest);
//			}
//			else
//			{
//				ShowPlayingButtons(pUnitTest);
//			}
//		}

//		/// <summary>
//		/// Specific buttons to show in inspector when application is not playing
//		/// </summary>
//		/// <param name="pUnitTest">Data to modify</param>
//		private	void	ShowEditButtons(UnitTest pUnitTest)
//		{
//			if (GUILayout.Button("Add unit"))
//			{
//				pUnitTest.Units.Add(new Unit());

//				SaveModifications(pUnitTest);
//			}
//			if (GUILayout.Button("Remove all units"))
//			{
//				pUnitTest.Units.Clear();

//				SaveModifications(pUnitTest);
//			}

//			AddSeparator();

//			if (GUILayout.Button("Launch Scene"))
//			{
//				pUnitTest.LaunchScene();
//			}
//		}

//		/// <summary>
//		/// Specific buttons to show in inspector when application is playing
//		/// </summary>
//		/// <param name="pUnitTest">Data to modify</param>
//		private	void	ShowPlayingButtons(UnitTest pUnitTest)
//		{
//			if (pUnitTest.CurrentIndex != pUnitTest.Units.Count)
//			{
//				if (GUILayout.Button("Play"))
//				{
//					pUnitTest.Play();
//				}
//			}
//			else
//			{
//				if (GUILayout.Button("Reset"))
//				{
//					pUnitTest.CurrentIndex	=	0;
//				}
//			}
//		}

//		/// <summary>
//		/// Add seperator in inspector
//		/// </summary>
//		private	void	AddSeparator()
//		{
//			EditorGUILayout.Separator();

//			EditorGUILayout.LabelField("", GUI.skin.horizontalSlider);

//			EditorGUILayout.Separator();
//		}

//		/// <summary>
//		/// Add double spaces in inspector
//		/// </summary>
//		private	void	AddDoubleSpace()
//		{
//			EditorGUILayout.Space();

//			EditorGUILayout.Space();
//		}

//		/// <summary>
//		/// Save all modifications occured to an UnitTest
//		/// </summary>
//		/// <param name="pUnitTest">Data to save</param>
//		private	void	SaveModifications(UnitTest pUnitTest)
//		{
//			EditorUtility.SetDirty(pUnitTest);

//			/*AssetDatabase.SaveAssets();

//			AssetDatabase.Refresh();*/
//		}

//		#endregion
//	}
//}
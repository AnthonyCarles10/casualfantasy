﻿//using UnityEngine.Assertions;
//using Sportfaction.CasualFantasy.Events;
//using Sportfaction.CasualFantasy.References;

//namespace Sportfaction.CasualFantasy.Editor.Tests
//{
//	/// <summary>
//	/// Description: Class with an unit function
//	/// Author: Rémi Carreira
//	/// </summary>
//	[System.Serializable]
//	public class Unit
//	{
//		#region Fields

//		public	eUnit			UnitType		=	eUnit.InvokeGameEvent;	// Define the type of Unit

//		public	GameEvent		GameEventValue	=	null;	// Game Event used to invoke if the unit type is InvokeGameEvent
//		public	BoolReference	BoolRefValue	=	null;	// Reference a boolean to modify if the unit type is ChangeBoolReferenceValue
//		public	ByteReference	ByteRefValue	=	null;	// Reference a byte to modify if the unit type is ChangeByteReferenceValue
//		public	FloatReference	FloatRefValue	=	null;	// Reference a float to modify if the unit type is ChangeFloatReferenceValue
//		public	IntReference	IntRefValue		=	null;	// Reference an int to modify if the unit type is ChangeIntReferenceValue
//		public	SByteReference	SByteRefValue	=	null;	// Reference a sbyte to modify if the unit type is ChangeSByteReferenceValue
//		public	StringReference	StringRefValue	=	null;	// Reference a string to modify if the unit type is ChangeStringReferenceVaue
//		public	UIntReference	UIntRefValue	=	null;	// Reference an uint to modify if the unit type is ChangeUIntReferenceValue

//		public	PVPCardReference		PVPCardRef		=	null;	// Reference a pvp card to modify if the unit type is ChangePVPCardReferenceValue
//		public	PVPTokenReference		PVPTokenRef		=	null;	// Reference a pvp token to modify if the unit type is ChangePVPTokenReferenceValue

//		public	bool	BoolValue		=	false;	// Boolean value used to modify boolean reference if the unit type is ChangeBoolReferenceValue
//		public	byte	ByteValue		=	0;		// Byte value used to modify byte reference if the unit type is ChangeByteReferenceValue		
//		public	float	FloatValue		=	0f;		// Float value used to modify float reference if the unit type is ChangeFloatReferenceValue
//		public	int		IntValue		=	0;		// Int value used to modify int reference if the unit type is ChangeIntReferenceValue
//		public	sbyte	SByteValue		=	0;		// SByte value used to modify sbyte reference if the unit type is ChangeSByteReferenceValue
//		public	string	StringValue		=	"";		// String value used to modify string reference if the unit type is ChangeStringReferenceValue
//		public	uint	UIntValue		=	0;		// Uint value used to modify uint reference if the unit type is ChangeUIntReferenceValue

//		public	Card		PVPCardValue		=	null;				// Card value used to modify pvp card reference
//		public	Token		PVPTokenValue		=	null;				// Card value used to modify pvp token reference

//		#endregion

//		#region Public Methods

//		/// <summary>
//		/// Execute the unit function
//		/// </summary>
//		public virtual void	ExecuteUnit()
//		{
//			switch (UnitType)
//			{
//				case eUnit.ChangeBoolReferenceValue:
//					ChangeBoolReferenceValue();
//					break;
//				case eUnit.ChangeByteReferenceValue:
//					ChangeByteReferenceValue();
//					break;
//				case eUnit.ChangeFloatReferenceValue:
//					ChangeFloatReferenceValue();
//					break;
//				case eUnit.ChangeIntReferenceValue:
//					ChangeIntReferenceValue();
//					break;
//				case eUnit.ChangeSByteReferenceValue:
//					ChangeSByteReferenceValue();
//					break;
//				case eUnit.ChangeStringReferenceValue:
//					ChangeStringReferenceValue();
//					break;
//				case eUnit.ChangeUIntReferenceValue:
//					ChangeUintReferenceValue();
//					break;
//				case eUnit.ChangePVPCardReferenceValue:
//					ChangePVPCardReferenceValue();
//					break;
//				case eUnit.ChangePVPTokenReferenceValue:
//					ChangePVPTokenReferenceValue();
//					break;
//				case eUnit.InvokeGameEvent:
//					InvokeGameEvent();
//					break;
//				default:
//					break;
//			}
//		}

//		#endregion

//		#region Private Methods

//		/// <summary>
//		/// Change bool reference value by the bool value
//		/// </summary>
//		private	void	ChangeBoolReferenceValue()
//		{
//			Assert.IsNotNull(BoolRefValue, "Unit: ChangeBoolReferenceValue() failed, BoolRefValue is null.");

//			BoolRefValue.CurrentValue = BoolValue;
//		}

//		/// <summary>
//		/// Change byte reference value by the byte value
//		/// </summary>
//		private	void	ChangeByteReferenceValue()
//		{
//			Assert.IsNotNull(ByteRefValue, "Unit: ChangeByteReferenceValue() failed, ByteRefValue is null.");

//			ByteRefValue.CurrentValue	=	ByteValue;
//		}

//		/// <summary>
//		/// Change float reference value by the float value
//		/// </summary>
//		private	void	ChangeFloatReferenceValue()
//		{
//			Assert.IsNotNull(FloatRefValue, "Unit: ChangeFloatReferenceValue() failed, FloatRefValue is null.");

//			FloatRefValue.CurrentValue	=	FloatValue;
//		}

//		/// <summary>
//		/// Change int reference value by the int value
//		/// </summary>
//		private	void	ChangeIntReferenceValue()
//		{
//			Assert.IsNotNull(IntRefValue, "Unit: ChangeIntReferenceValue() failed, IntRefValue is null.");

//			IntRefValue.CurrentValue	=	IntValue;
//		}

//		/// <summary>
//		/// Change sbyte reference value by the sbyte value
//		/// </summary>
//		private	void	ChangeSByteReferenceValue()
//		{
//			Assert.IsNotNull(SByteRefValue, "Unit: ChangeSByteReferenceValue() failed, SByteRefValue is null.");

//			SByteRefValue.CurrentValue	=	SByteValue;
//		}

//		/// <summary>
//		/// Change the value in the string reference
//		/// </summary>
//		private	void	ChangeStringReferenceValue()
//		{
//			Assert.IsNotNull(StringRefValue, "Unit: ChangeStringReferenceValue() failed, StringRefValue is null.");

//			StringRefValue.CurrentValue	=	StringValue;
//		}

//		/// <summary>
//		/// Change uint reference value by the uint value
//		/// </summary>
//		private	void	ChangeUintReferenceValue()
//		{
//			Assert.IsNotNull(UIntRefValue, "Unit: ChangeUIntReferenceValue() failed, UIntRefValue is null.");

//			UIntRefValue.CurrentValue	=	UIntValue;
//		}

//		/// <summary>
//		/// Change the value in pvp card reference
//		/// </summary>
//		private	void	ChangePVPCardReferenceValue()
//		{
//			Assert.IsNotNull(PVPCardRef, "Unit: ChangePVPCardReferenceValue() failed, PVPCardRef is null.");

//			PVPCardRef.CurrentValue	=	PVPCardValue;
//		}

//		/// <summary>
//		/// Change the value in pvp token reference 
//		/// </summary>
//		private	void	ChangePVPTokenReferenceValue()
//		{
//			Assert.IsNotNull(PVPTokenRef, "Unit: ChangePVPTokenReferenceValue() failed, PVPTokenRef is null.");

//			PVPTokenRef.CurrentValue	=	PVPTokenValue;
//		}

//		/// <summary>
//		/// Invoke game event value
//		/// </summary>
//		private	void	InvokeGameEvent()
//		{
//			Assert.IsNotNull(GameEventValue, "Unit: ExecuteUnit() failed, GameEventValue is null.");

//			GameEventValue.Invoke();
//		}

//		#endregion
//	}
//}
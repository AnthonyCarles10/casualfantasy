﻿using UnityEditor;

namespace Sportfaction.CasualFantasy.Editor
{
    /// <summary>
    /// Description: Script load when editor is initialized
    /// Author: Rémi Carreira
    /// </summary>
    [InitializeOnLoad]
	public sealed class PreloadKeystore
	{
		#region Static Methods

		/// <summary>
		/// Preload all android keystore values
		/// </summary>
		static	PreloadKeystore()
		{
			PlayerSettings.Android.keystoreName	=	"Certificats/casualfantasyfootball.keystore";
			PlayerSettings.Android.keystorePass	=	"sf2017*";
			PlayerSettings.Android.keyaliasName	=	"casualfantasyfootball";
			PlayerSettings.Android.keyaliasPass	=	"sf2017*";
		}

		#endregion
	}
}
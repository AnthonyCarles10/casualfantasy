﻿using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;

namespace Sportfaction.CasualFantasy.Editor
{
	/// <summary>
	/// Description: Editor class used when play game
	/// Author: Rémi Carreira
	/// </summary>
	[InitializeOnLoad]
	public static class EditorPlayer
	{
		#region Fields

		// Path for all scenes
		private	static readonly string	_scenesPath		=	"Assets/Scenes/";
		// Name of the main scene
		private	static readonly string	_mainSceneName	=	"Splashscreen";
		// Key for enabling/disabling auto play in editor pref
		private	static readonly string	_autoPlayKey	=	"AutoPlay";
		// Key of the last scene in editor pref
		private	static readonly string	_lastSceneKey	=	"LastScene";
		// Enable / Disable auto play
		private	static bool				_autoPlay		=	false;

		#endregion

		#region Public Methods

		/// <summary>
		/// Constructor used to link callbacks
		/// </summary>
		static	EditorPlayer()
		{
			if (false == EditorPrefs.HasKey(_autoPlayKey))
			{
				EditorPrefs.SetBool(_autoPlayKey, true);
			}
			if (true == EditorPrefs.GetBool(_autoPlayKey))
			{
				_autoPlay	=	true;

				EditorApplication.playModeStateChanged	+=	OnPlayModeStateChanged;
			}
		}

		#endregion

		#region Private Methods

		/// <summary>
		/// Button used to enable auto play
		/// </summary>
		[MenuItem("SF/AutoPlay/Enable")]
		private static void	EnableAutoplay()
		{
			_autoPlay	=	true;

			EditorPrefs.SetBool(_autoPlayKey, true);

			EditorApplication.playModeStateChanged	+=	OnPlayModeStateChanged;
		}

		/// <summary>
		/// Method used to enable / disable button
		/// </summary>
		/// <returns>True if auto play is disable</returns>
		[MenuItem("SF/AutoPlay/Enable", true)]
		private static bool	ValidateEnableAutoplay()
		{
			return (false == _autoPlay);
		}

		/// <summary>
		/// Button used to disable auto play
		/// </summary>
		[MenuItem("SF/AutoPlay/Disable")]
		private	static void	DisableAutoplay()
		{
			_autoPlay	=	false;

			EditorPrefs.SetBool(_autoPlayKey, false);

			EditorApplication.playModeStateChanged	-=	OnPlayModeStateChanged;
		}

		/// <summary>
		/// Method used to enable / disable button
		/// </summary>
		/// <returns>True if auto play is enable</returns>
		[MenuItem("SF/AutoPlay/Disable", true)]
		private static bool ValidateDisableAutoplay()
		{
			return (true == _autoPlay);
		}

		/// <summary>
		/// Callback invoke when play mode state changed
		/// </summary>
		/// <param name="pState">New play mode state change</param>
		private	static void	OnPlayModeStateChanged(PlayModeStateChange pState)
		{
			switch (pState)
			{
				case PlayModeStateChange.EnteredEditMode:
					OpenLastScene();
					break;
				case PlayModeStateChange.ExitingEditMode:
					RunMainScene();
					break;
				default:
					break;
			}
		}

		/// <summary>
		/// Open last scene
		/// </summary>
		private	static	void	OpenLastScene()
		{
			if (true == EditorPrefs.HasKey(_lastSceneKey))
			{
				string	pLastScene	=	EditorPrefs.GetString(_lastSceneKey);

				EditorSceneManager.OpenScene(_scenesPath + pLastScene + ".unity");
			}
			else
			{
				Debug.LogError("[EditorPlayer] OpenLastScene(), editor doesn't save the last scene.");
			}
		}

		/// <summary>
		/// Run main scene
		/// </summary>
		private	static	void	RunMainScene()
		{
			string	lCurrentSceneName	=	EditorSceneManager.GetActiveScene().name;

			EditorPrefs.SetString(_lastSceneKey, lCurrentSceneName);

			EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo();

			EditorSceneManager.OpenScene(_scenesPath + _mainSceneName + ".unity");

			EditorApplication.isPlaying = true;
		}

		#endregion
	}
}

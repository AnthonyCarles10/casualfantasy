﻿using Sportfaction.CasualFantasy.Events;
using Sportfaction.CasualFantasy.Manager.Profile;
using Sportfaction.CasualFantasy.Services;

using UnityEngine;

namespace Sportfaction.CasualFantasy.PVP.PreMatch
{
    /// <summary>
    /// Description: Class handling pre match screen ui
    /// Author: Antoine de Lachèze-Murel
    /// </summary>
    public sealed class PreMatchScreenMain : MonoBehaviour
    {
        #region Fields

        [Header("Calls")]
        [SerializeField, Tooltip("Call used to get team A JSON")]
        private    ApiGetCall        _squadPlayersACall    =    null;
        [SerializeField, Tooltip("Call used to get team B JSON")]
        private    ApiGetCall        _squadPlayersBCall    =    null;

        [Header("Composition")]
        [SerializeField, Tooltip("Composition of team A")]
        private   string        _teamAComposition       =   "442f";
        [SerializeField, Tooltip("Composition of team B")]
        private   string        _teamBComposition       =   "433f";

        [Header("Data")]
        [SerializeField, Tooltip("Scriptable object contains infos about match UI")]
        private    PVPData        _pvpData    =    null;

        #endregion

        #region Events

        [Header("Events")]
        [SerializeField, Tooltip("Game event invoke when manager is ready")]
        private GameEvent _onManagerReady = null;

        #endregion

        #region Public Methods

        /// <summary>
        /// Call API to retrieve squad player of team A
        /// </summary>
        //public    void    RetrieveSquadPlayersA()
        //{
        //    Assert.IsNotNull(_squadPlayersACall, "[PreMatchScreenMain] RetrieveSquadPlayersA(), _squadPlayersACall is null.");

        //    if (false == PhotonNetwork.IsConnected || true == PhotonNetwork.IsMasterClient)
        //    {
        //        Assert.IsNotNull(_pvpData, "[PreMatchScreenMain] RetrieveSquadPlayersA(), _pvpData is null.");

        //        _squadPlayersACall.URL    =    ApiConstants.GetSquadPlayersURL + '/' + _pvpData.TeamId;
        //    }
        //    else
        //    {
        //        _squadPlayersACall.URL    =    ApiConstants.GetSquadPlayersURL    + '/' + GetOtherTeamId().ToString();
        //    }

        //    _squadPlayersACall.Authentification    =    true;
        //    _squadPlayersACall.NumberOfRetry    =    2;

        //    _squadPlayersACall.Parameters.Clear();
        //    _squadPlayersACall.Parameters.Add(new ApiGetCall.Param("composition", _teamAComposition));

        //    _squadPlayersACall.CallAPI();
        //}

        /// <summary>
        /// Call API to retrieve squad players B
        /// </summary>
        //public    void    RetrieveSquadPlayersB()
        //{
        //    Assert.IsNotNull(_squadPlayersBCall, "[PreMatchScreenMain] RetrieveSquadPlayersB(), _squadPlayersBCall is null.");

        //    if (true == PhotonNetwork.IsConnected)
        //    {
        //        if (true == PhotonNetwork.IsMasterClient)
        //        {
        //            _squadPlayersBCall.URL    =    ApiConstants.GetSquadPlayersURL    + '/' + GetOtherTeamId().ToString();
        //        }
        //        else
        //        {
        //            Assert.IsNotNull(_pvpData, "[PreMatchScreenMain] RetrieveSquadPlayersB(), _pvpData is null.");

        //            _squadPlayersBCall.URL    =    ApiConstants.GetSquadPlayersURL + '/' + _pvpData.TeamId;
        //        }
        //    }
        //    else
        //    {
        //        if (null == ConfigData.Instance || true == UserData.Instance.Profile.OnBoarding)
        //        {
        //            _squadPlayersBCall.URL = ApiConstants.GetSquadBot + "/" + _teamBComposition + "/" + 0;
        //        }
        //        else
        //        {
        //            Assert.IsNotNull(UserData.Instance, "[PreMatchScreenMain] RetrieveSquadPlayersB(), UserData.Instance is null.");

        //            Assert.IsNotNull(UserData.Instance.Profile, "[PreMatchScreenMain] RetrieveSquadPlayersB(), UserData.Instance.Profile is null.");

        //            _squadPlayersBCall.URL = ApiConstants.GetSquadBot + "/" + _teamBComposition + "/" + 25;
        //        }
        //    }

        //    _squadPlayersBCall.Authentification    =    true;
        //    _squadPlayersBCall.NumberOfRetry    =    2;

        //    _squadPlayersBCall.Parameters.Clear();
        //    _squadPlayersBCall.Parameters.Add(new ApiGetCall.Param("composition", _teamBComposition));

        //    _squadPlayersBCall.CallAPI();
        //}


        /// <summary>
        /// PhotonBehaviour method called when PUN room is ready
        /// </summary>
        public void OnPUNRoomReady()
        {
            _onManagerReady.Invoke();

            //RetrieveSquadPlayersA();

            //RetrieveSquadPlayersB();
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Get team Id of other opponent
        /// </summary>
        /// <param name="pIndex">Index of the opponent</param>
        /// <returns>An integer</returns>
        //private    int        GetOtherTeamId(int pIndex = 0)
        //{
        //    if (true == PhotonNetwork.IsConnected)
        //    {
        //        Assert.IsNotNull(PhotonNetwork.PlayerListOthers, "[PreMatchScreenMain] GetOtherTeamId(), PhotonNetwork.PlayerListOthers is null.");

        //        if (pIndex < PhotonNetwork.PlayerListOthers.Length)
        //        {
        //            Photon.Realtime.Player    lPlayer    =    PhotonNetwork.PlayerListOthers[pIndex];
        //            if (null != lPlayer && null != lPlayer.CustomProperties)
        //            {
        //                if (true == lPlayer.CustomProperties.ContainsKey("TeamId"))
        //                {
        //                    return (int)lPlayer.CustomProperties["TeamId"];
        //                }
        //            }
        //        }
        //    }
        //    return -1;
        //}

        #endregion
    }
}
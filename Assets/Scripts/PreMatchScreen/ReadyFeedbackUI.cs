﻿using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;

namespace Sportfaction.CasualFantasy.PVP.PreMatch
{
	/// <summary>
	/// Description: Class handling ready ui feedback
	/// Author: Rémi Carreira
	/// </summary>
	public sealed class ReadyFeedbackUI : MonoBehaviour
	{
		#region Fields

		[SerializeField, Tooltip("The graphic to modify when manager is or not ready")]
		private	Graphic		_graphic		=	null;
		[SerializeField, Tooltip("Color used to know when the manager is not ready")]
		private	Color		_unreadyColor	=	Color.black;
		[SerializeField, Tooltip("Color used to know when the manager is ready")]
		private	Color		_readyColor		=	Color.white;

		#endregion

		#region Public Methods

		/// <summary>
		/// Callback invoke when manager is ready
		/// </summary>
		public	void	OnManagerIsReady()
		{
			SetGraphicColor(_readyColor);
		}

		#endregion

		#region Private Methods

		/// <summary>
		/// MonoBehaviour used for initialization
		/// </summary>
		private	void	Awake()
		{
			SetGraphicColor(_unreadyColor);
		}

		/// <summary>
		/// Set the graphic color by a new color
		/// </summary>
		/// <param name="pNewColor">New color to use</param>
		private	void	SetGraphicColor(Color pNewColor)
		{
			Assert.IsNotNull(_graphic, "ReadyUI: SetGraphicColor() failed, _graphic is null.");

			_graphic.color	=	pNewColor;
		}

		#endregion
	}
}
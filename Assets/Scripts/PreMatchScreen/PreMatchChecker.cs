﻿using Sportfaction.CasualFantasy.Events;

using Photon.Pun;
using UnityEngine;
using UnityEngine.Assertions;

namespace Sportfaction.CasualFantasy.PVP.PreMatch
{
    /// <summary>
    /// Description: Class check if all data was received from API
    /// Author: Rémi Carreira
    /// </summary>
    [RequireComponent(typeof(PhotonView))]
    public sealed class PreMatchChecker : MonoBehaviourPun
    {
		#region Class

		/// <summary>
		/// Structure contains important event to check
		/// </summary>
		[System.Serializable]
		public sealed class ImportantEvent
		{
			#region Variables

			// Game event to check
			public GameEvent	Event	=	null;
			// Is game event already invoked?
			public	bool		Invoked	=	false;

			#endregion

			#region Public Methods

			/// <summary>
			/// Method called whend this event is invoked
			/// </summary>
			public	void	OnEventInvoked()
			{
				Invoked	=	true;
			}

			#endregion
		}

		#endregion

		#region Variables

		[Header("Variables")]
		[SerializeField, Tooltip("All received events")]
		private	ImportantEvent[]	_importantEvents		=	new ImportantEvent[0];

		#endregion

		#region Events

		[Header("Events")]
		[SerializeField, Tooltip("Game event invoke when manager is ready")]
		private	GameEvent	_onManagerReady			=	null;

		#endregion

		#region Private Methods

		/// <summary>
		/// MonoBehaviour method used to start listening important events
		/// </summary>
		private	void	OnEnable()
		{
			Assert.IsNotNull(_importantEvents, "[PreMatchChecker] OnEnable(), _importantEvents is null.");

			foreach (ImportantEvent lImportantEvent in _importantEvents)
			{
				Assert.IsNotNull(lImportantEvent.Event, "[PreMatchChecker] OnEnable(), lImportantEvent.Event is null.");

				lImportantEvent.Event.StartListening(lImportantEvent.OnEventInvoked);	
				lImportantEvent.Event.StartListening(CheckCurrentManagerReady);
			}
		}

		/// <summary>
		/// MonoBehaviour method used to stop listening important events
		/// </summary>
		private	void	OnDisable()
		{
			Assert.IsNotNull(_importantEvents, "[PreMatchChecker] OnDisable(), _importantEvents is null.");

			foreach (ImportantEvent lImportantEvent in _importantEvents)
			{
				Assert.IsNotNull(lImportantEvent.Event, "[PreMatchChecker] OnDisable(), lEvent.Event is null.");

				lImportantEvent.Event.StopListening(lImportantEvent.OnEventInvoked);	
				lImportantEvent.Event.StopListening(CheckCurrentManagerReady);
			}
		}

        /// <summary>
        /// Check if current manager is ready
        /// </summary>
        private	void	CheckCurrentManagerReady()
		{
			if (true == IsAllEventsInvoked() && null != _onManagerReady)
			{
				_onManagerReady.Invoke();
			}
		}

		/// <summary>
		/// Check if all events was invoked
		/// </summary>
		/// <returns>True if all events was invoked</returns>
		private	bool	IsAllEventsInvoked()
		{
			Assert.IsNotNull(_importantEvents, "[PreMatchChecker] IsAllEventsInvoked(), _importantEvents is null.");

			foreach (ImportantEvent lImportantEvent in _importantEvents)
			{
				if (false == lImportantEvent.Invoked)
				{
					return false;
				}
			}
			return true;
		}

		#endregion
	}
}
﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace Sportfaction.CasualFantasy.PVP.PreMatch
{
    /// <summary>
    /// Description: Class handling pre match screen
    /// Author: Rémi Carreira
    /// </summary>
    public sealed class PreMatchLoader : MonoBehaviour
	{
        #region Public Methods

        static public PreMatchLoader Instance = null;

        #endregion

        #region Fields

        [SerializeField, Tooltip("Loading BG")]
        private GameObject  _loadingBG  =   null;
        
        #endregion
        
        #region Public Methods

        /// <summary>
        /// Load scene with photon if connected, else default load scene
        /// </summary>
        public  void	LoadScene()
		{
            if (null != _loadingBG)
                _loadingBG.SetActive(true);

            //SceneManager.LoadScene("Gameplay3D", LoadSceneMode.Single);
            StartCoroutine(ScenesMain.Instance.LoadAsyncGameplayScene());
        }

        #endregion

        #region Private Methods

        private void Awake()
        {
            Instance = this;
        }

        private void    Start()
        {
            _loadingBG.SetActive(false);
        }
        
        #endregion
    }
}
using Sportfaction.CasualFantasy.Config;
using Sportfaction.CasualFantasy.Services;

using SportFaction.CasualFootballEngine;

using BestHTTP;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Purchasing;
using System;

namespace Sportfaction.CasualFantasy.Shop
{
	/// <summary>
	/// Description: 
	/// Author: Antoine de Lac��ze-Murel
	/// </summary>
	public sealed class ShopAPICalls : MonoBehaviour
	{
        #region Fields

        [SerializeField, Tooltip("ShopMain")]
        private ShopMain _shopMain = null;


        [SerializeField, Tooltip("Product")]
        private Product _product;

        [SerializeField, Tooltip("_reference")]
        private string _reference;


        private string _getProduct;
        private string _getProductList;

        #endregion

        #region Public Methods
        
        /// <summary>
        /// Sets transaction infos to buy product
        /// </summary>
        /// <param name="pProduct">Product data</param>
        public void BuyProductWithRealMoney(Product pProduct)
        {
            _product        =   pProduct;
            string  lUrl    =   Api.Instance.GenerateUrl(ApiConstants.BuyProductWithRealMoney);

            Assert.IsFalse(string.IsNullOrEmpty(lUrl), "[ShopAPICalls] BuyProductWithRealMoney(), lUrl is null or empty.");

            JToken  lReceipt    =   JToken.Parse(pProduct.receipt);

            object lBody = new
            {
                transactionId   =   pProduct.transactionID,
                receipt         =   Application.platform == RuntimePlatform.Android ? pProduct.receipt : lReceipt.Value<string>("Payload"),
                productRef      =   pProduct.definition.id,
                source          =   Application.platform == RuntimePlatform.Android ? "android": "ios",
                price_ht        =   0,
                price_ttc       =   pProduct.metadata.localizedPrice,
                sandbox         =   (null == ConfigData.Instance || eConfig.DEV == ConfigData.Instance.ENV) ? true : false,
            };

            HTTPRequest lRequest = Api.Instance.SetUpRequest(lUrl, true, lBody);

            lRequest.Timeout = TimeSpan.FromSeconds(5);

            StartCoroutine(Api.Instance.SendRequest(lRequest));
        }

        public void BuyProduct(string pProductRef, object pAdditionalData = null)
        {
            _reference = pProductRef;
            string lUrl = Api.Instance.GenerateUrl(ApiConstants.BuyProduct);

            Assert.IsFalse(string.IsNullOrEmpty(lUrl), "[ShopAPICalls] BuyProduct(), lUrl is null or empty.");

            object lBody = null;

            if (null != pAdditionalData)
            {
                lBody = new
                {
                    productRef = pProductRef,
                    additionalData = pAdditionalData
                };
            } else {
                lBody = new
                {
                    productRef = pProductRef,
                };
            }
            

            HTTPRequest lRequest = Api.Instance.SetUpRequest(lUrl, true, lBody);

            lRequest.Timeout = TimeSpan.FromSeconds(5);

            StartCoroutine(Api.Instance.SendRequest(lRequest));

        }

        public void GetProduct(string pProductRef)
        {
            if (null != Api.Instance)
            {
                _getProduct = Api.Instance.GenerateUrl(ApiConstants.GetProduct + "/" + pProductRef);

                Assert.IsFalse(string.IsNullOrEmpty(_getProduct), "[ShopAPICalls] GetProduct(), lUrl is null or empty.");

                HTTPRequest lRequest = Api.Instance.SetUpRequest(_getProduct, Api.Methods.GET, false);

                lRequest.Timeout = TimeSpan.FromSeconds(5);

                StartCoroutine(Api.Instance.SendRequest(lRequest));
            }
        }

        public void GetProductList(string pType = "")
        {
            if (null != Api.Instance)
            {
                Dictionary<string, string> lQueryParams = new Dictionary<string, string>()
                {
                    { "type", pType },
                    { "limit", "100" }
                };

                _getProductList = Api.Instance.GenerateUrl(ApiConstants.GetProduct, lQueryParams);

                Assert.IsFalse(string.IsNullOrEmpty(_getProductList), "[ShopAPICalls] GetProduct(), lUrl is null or empty.");

                HTTPRequest lRequest = Api.Instance.SetUpRequest(_getProductList, Api.Methods.GET, false);

                lRequest.Timeout = TimeSpan.FromSeconds(5);

                StartCoroutine(Api.Instance.SendRequest(lRequest));
            }
        }


        #endregion

        #region Private Methods


        /// <summary>
        /// MonoBehaviour method used to link OnRequestSuccess to API
        /// </summary>
        private void OnEnable()
        {
            if (null != Api.Instance)
            {
                Api.Instance.OnSuccess += OnRequestSuccess;
                Api.Instance.OnBadRequest += OnBadRequest;
            }
        }

        /// <summary>
        /// MonoBehaviour method used to unlink OnRequestSuccess from API
        /// </summary>
        private void OnDisable()
        {
            if (null != Api.Instance)
            {
                Api.Instance.OnSuccess -= OnRequestSuccess;
                Api.Instance.OnBadRequest -= OnBadRequest;
            }
        }

        /// <summary>
        /// Callback invoke when request from API was done successfully
        /// </summary>
        /// <param name="pCurrentUrl">string</param>
        /// <param name="pMethod">HTTPMethods</param>
        /// <param name="pData">string</param>
        private void OnRequestSuccess(string pCurrentUrl, HTTPMethods pMethod, string pData)
        {
            if (true == pCurrentUrl.Contains(ApiConstants.BuyProductWithRealMoney))
            {
                _shopMain.ValidPurchaseWithRealMoney(pData, _product);
            }
            else if (true == pCurrentUrl.Contains(ApiConstants.BuyProduct))
            {
                _shopMain.ValidPurchase(pData,_reference);
            }
            else if (true == pCurrentUrl.Equals(_getProduct))
            {
                _shopMain.HandleJsonProduct(pData);
            }
            else if (true == pCurrentUrl.Equals(_getProductList))
            {
                _shopMain.HandleJsonProductList(pData);
            }
        }

        /// <summary>
        /// Callback invoke when request from API was done successfully
        /// </summary>
        /// <param name="pCurrentUrl">string</param>
        /// <param name="pData">string</param>
        private void OnBadRequest(string pCurrentUrl, string pData)
        {
            if (true == pCurrentUrl.Contains(ApiConstants.BuyProductWithRealMoney))
            {
                _shopMain.NotValidPurchaseWithRealMoney(pData);
            } 
            else if (true == pCurrentUrl.Contains(ApiConstants.BuyProduct))
            {
                _shopMain.NotValidPurchase(_reference);
            }
        }


        #endregion
    }
}
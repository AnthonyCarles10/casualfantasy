﻿using Sportfaction.CasualFantasy.Services;

using Newtonsoft.Json.Linq;
using UnityEngine;

namespace Sportfaction.CasualFantasy.Shop
{
    [System.Serializable]
    public sealed class ProductItemData
    {
        #region Properties

        public int Id { get { return _id; } }

        public string Reference { get { return _reference; } }

        public string Title { get { return _title; } }

        public string Type { get { return _type; } }

        public string Description { get { return _description; } }

        public int Amount { get { return _amount; } }

        public float BasePriceEuros { get { return _basePriceEuros; } }

        public float PriceEuros { get { return _priceEuros; } }

        public float BasePriceHard { get { return _basePriceHard; } }

        public float PriceHard { get { return _priceHard; } }

        public float BasePriceSoft { get { return _basePriceSoft; } }

        public float PriceSoft { get { return _priceSoft; } }

        public float BasePriceThird { get { return _basePriceThird; } }

        public float PriceThird { get { return _priceThird; } }

        public bool Enabled { get { return _enabled; } }

        public int  Duration { get { return _duration; } }

        public int  Stock { get { return _stock; } }

        public int  BigBox { get { return _bigBox; } }

        public int  MediumBox { get { return _mediumBox; } }

        public int  SmallBox { get { return _smallBox; } }

        public int  Hard { get { return _hard; } }

        #endregion

        #region Fields

        [SerializeField, Tooltip("_id")]
        private int _id;

        [SerializeField, Tooltip("_reference")]
        private string _reference;

        [SerializeField, Tooltip("_title")]
        private string _title;

        [SerializeField, Tooltip("_type")]
        private string _type;

        [SerializeField, Tooltip("_description")]
        private string _description;

        [SerializeField, Tooltip("_amount")]
        private int _amount;

        [SerializeField, Tooltip("_basePriceEuros")]
        private float _basePriceEuros;

        [SerializeField, Tooltip("_priceEuros")]
        private float _priceEuros;

        [SerializeField, Tooltip("_basePriceHard")]
        private float _basePriceHard;

        [SerializeField, Tooltip("_priceHard")]
        private float _priceHard;

        [SerializeField, Tooltip("_basePriceSoft")]
        private float _basePriceSoft;

        [SerializeField, Tooltip("_priceSoft")]
        private float _priceSoft;

        [SerializeField, Tooltip("_basePriceThird")]
        private float _basePriceThird;

        [SerializeField, Tooltip("_priceThird")]
        private float _priceThird;

        [SerializeField, Tooltip("_enabled")]
        private bool _enabled;

        [SerializeField, Tooltip("_duration")]
        private int _duration;

        [SerializeField, Tooltip("_stock")]
        private int _stock;

        [SerializeField, Tooltip("_bigBox")]
        private int _bigBox;

        [SerializeField, Tooltip("_mediumBox")]
        private int _mediumBox;

        [SerializeField, Tooltip("_smallBox")]
        private int _smallBox;

        [SerializeField, Tooltip("_hard")]
        private int _hard;


        #endregion

        #region Public Methods

        /// <summary>
        /// Initialize Data with json response from Api
        /// </summary>
        /// <param name="pToken">Specific JsonToken to use</param>
        public void Init(JToken pToken)
        {
            _id = pToken.SafeValue<int>("id", -1);
            _reference = pToken.SafeValue<string>("reference");
            _title = pToken.SafeValue<string>("title");
            _type = pToken.SafeValue<string>("type");
            _description = pToken.Value<string>("description");
            _amount = pToken.Value<int>("amount");
            _basePriceEuros = pToken.Value<float>("base_price_euros");
            _priceEuros = pToken.Value<float>("price_euros");
            _basePriceHard = pToken.Value<float>("base_price_hard");
            _priceHard = pToken.Value<float>("price_hard");
            _basePriceSoft = pToken.Value<float>("base_price_soft");
            _priceSoft = pToken.Value<float>("price_soft");
            _basePriceThird = pToken.Value<float>("base_price_third");
            _priceThird = pToken.Value<float>("price_third");
            _enabled = pToken.Value<bool>("enabled");
            _duration = pToken.Value<int>("duration");
            _stock = pToken.Value<int>("stock");

            JToken lListProducts = pToken.Value<JToken>("product_list");

            _bigBox = lListProducts.Value<int>("big_box");
            _mediumBox = lListProducts.Value<int>("medium_box");
            _smallBox = lListProducts.Value<int>("small_box");
            _hard = lListProducts.Value<int>("hard");
        }

        #endregion
    }
}

﻿using UnityEngine;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;

namespace Sportfaction.CasualFantasy.Shop
{
	/// <summary>
	/// Description: Class handles data about manager's product
	/// Author: Antoine
	/// </summary>
	[CreateAssetMenu(fileName = "Store-ProductData", menuName = "CasualFantasy/Data/Store/Product")]
    public sealed class ProductData : ScriptableObject
    {
        #region Properties

        public List<ProductItemData> Items { get { return _items; } }

        #endregion

        #region Fields

        [SerializeField, Tooltip("_items")]
        private List<ProductItemData> _items = new List<ProductItemData>();

        #endregion

        #region Public Methods

        /// <summary>
        /// Initialize Data with json response from Api
        /// </summary>
        /// <param name="pJson">json response</param>
        public	void			Init(string pJson)
        {
            
            JToken	lJToken			=	JToken.Parse(pJson);
            JArray	lActionsArray	=	lJToken.Value<JArray>("list");
            
            foreach (JToken ljToken in lActionsArray)
            {
                ProductItemData lItem = new ProductItemData();
                lItem.Init(ljToken);
                _items.Add(lItem);
            }

        }
        #endregion
    }
}

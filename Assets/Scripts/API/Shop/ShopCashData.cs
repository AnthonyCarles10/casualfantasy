﻿using UnityEngine;
using System.Collections.Generic;

namespace Sportfaction.CasualFantasy.Shop
{
    [CreateAssetMenu(fileName = "New Squad Alpha Data", menuName = "CasualFantasy/Data/ShopCash")]
    public sealed class ShopCashData : ScriptableObject
    {
        #region Properties

        public List<ShopCashItemData> Items { get { return _items; } }


        #endregion

        #region Fields

        [SerializeField, Tooltip("_items")]
        private List<ShopCashItemData> _items;


        #endregion


    }
}

﻿using UnityEngine;

namespace Sportfaction.CasualFantasy.Shop
{
    [System.Serializable]
    public sealed class ShopCashItemData
    {
        #region Properties

        public string Sf { get { return _sf; } }

        public string SfBeforeBonus { get { return _sfBeforeBonus; } }

        public string SfBonus { get { return _sfBonus; } }

        public string Flag { get { return _flag; } }

        public Sprite Icon { get { return _icon; } }

        public string ProductId { get { return _productId; } }


        #endregion

        #region Fields

        [SerializeField, Tooltip("_sf")]
        private string _sf;

        [SerializeField, Tooltip("_sfBeforeBonus")]
        private string _sfBeforeBonus;

        [SerializeField, Tooltip("_sfBonus")]
        private string _sfBonus;

        [SerializeField, Tooltip("_flag")]
        private string _flag;

        [SerializeField, Tooltip("_icon")]
        private Sprite _icon;

        [SerializeField, Tooltip("_productId")]
        private string _productId;

        #endregion


    }
}

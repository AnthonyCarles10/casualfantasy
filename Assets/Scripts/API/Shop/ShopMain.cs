using Sportfaction.CasualFantasy.Config;
using Sportfaction.CasualFantasy.Events;
using Sportfaction.CasualFantasy.Services;
using Sportfaction.CasualFantasy.Manager.Profile;
using Sportfaction.CasualFantasy.Services.Billing;
using Sportfaction.CasualFantasy.Services.Userstracking;

using SportFaction.CasualFootballEngine;

using Facebook.Unity;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Purchasing;
using Sportfaction.CasualFantasy.Meta;
using System;
using PaperPlaneTools;

namespace Sportfaction.CasualFantasy.Shop
{
    /// <summary>
    /// Description: UserMain
    /// Author: Antoine de Lachèze-Murel
    /// </summary>
    public class ShopMain : PurchaseListener
    {
        #region Variables

		[Header("Parameters")]
        [SerializeField, Tooltip("ShopAPICalls")]
        private ShopAPICalls	_shopAPICalls		=	null;

        [SerializeField, Tooltip("UserMain")]
        private UserMain		_userMain			=	null;

        [SerializeField, Tooltip("IAPStoreManager")]
        public IAPStoreManager	IapStoreManager	=	null;

		[SerializeField, Tooltip("Scriptable object contains data about manage's product")]
		public	ProductData		ProductData		=	null;

        public string ReferencePurchased = "";



		#endregion

		#region Events

		[Header("Events")]
        [SerializeField]
        private GameEvent _onCashPurchaseSucceed = null;

        [SerializeField]
        private GameEvent _onCashPurchaseFailed = null;

        [SerializeField]
        private GameEvent _onProductPurchaseSucceed = null;

        [SerializeField]
        private GameEvent _onProductPurchaseFailed = null; 

        [SerializeField]
        private GameEvent _onProductGetInfos = null;

        [SerializeField]
        private GameEvent _onGetProductList = null;

        public ProductItemData Product = null;

        #endregion

        #region API Calls

        public void BuyProductWithRealMoney(Product pProduct) { _shopAPICalls.BuyProductWithRealMoney(pProduct); }
        public void BuyProduct(string pProductRef, object pAdditionalData = null) { _shopAPICalls.BuyProduct(pProductRef, pAdditionalData); }
        public void GetProductList(string pType = "") { _shopAPICalls.GetProductList(pType); }

        #endregion

        #region Public Methods
        public void ValidPurchaseWithRealMoney(string pJson, Product pProduct)
        {
            #if UNITY_ANDROID
                    if (null != ConfigData.Instance && eConfig.PROD == ConfigData.Instance.ENV)
                {
                    Dictionary<string, object> lDict = new Dictionary<string, object>();
                    foreach (KeyValuePair<string, string> keyValuePair in ConfigData.Instance.TrackingParams)
                    {
                        lDict.Add(keyValuePair.Key, keyValuePair.Value == null ? "" : keyValuePair.Value.ToString());
                    }

                    lDict = new Dictionary<string, object>();
                    foreach (KeyValuePair<string, string> keyValuePair in ConfigData.Instance.UserProperties)
                    {
                        lDict.Add(keyValuePair.Key, keyValuePair.Value == null ? "" : keyValuePair.Value.ToString());
                    }

                    var parameters = lDict;
                    parameters["transactionID"] = pProduct.transactionID;
                    parameters["productRef"] = pProduct.definition.id;


                    FB.LogPurchase((float)pProduct.metadata.localizedPrice, pProduct.metadata.isoCurrencyCode, parameters);
                }       
            #endif

                      string lTag = pProduct.definition.id.Substring(29);
            UserEventsManager.Instance.RecordEvent(new UserActionEvent("buy_product_" + lTag));

            new Alert(I2.Loc.LocalizationManager.GetTranslation("PurchaseIsValidTitle"),
              I2.Loc.LocalizationManager.GetTranslation("PurchaseIsValidMessage"))
              .SetPositiveButton(I2.Loc.LocalizationManager.GetTranslation("PurchaseIsValidMessageButton"), () => {
                  if (lTag == "starter_1")
                  {
                      Debug.Log(pJson);
                      GameObject Home_Grp = UI.Instance.PrefabsIdxArray_Gob[Array.IndexOf(UI.Instance.PrefabsIdxArray_Str, "Home_Grp")];
                      Home_Grp.SetActive(true);
                      UI.Instance.CurrentActive_Go = Home_Grp;

                      MetaData.Instance.LootMain.ParseAskManagerLoot(pJson, "shop");
                  }

                  _onCashPurchaseSucceed.Invoke();
                  _userMain.GetProfile();
              })
              .Show();


            
        }

        public void NotValidPurchaseWithRealMoney(string pData)
        {
     
            //JToken lJToken = JToken.Parse(pData);
            //int lIsValid = lJToken.Value<int>("result");
         

            //AlertDialogs.Instance.ShowAlert(
            //    I2.Loc.LocalizationManager.GetTranslation("PurchaseIsNotValidTitle"),
            //    I2.Loc.LocalizationManager.GetTranslation("PurchaseIsNotValidMessage"),
            //    I2.Loc.LocalizationManager.GetTranslation("PurchaseIsNotValidMessageButton"),
            //    (_buttonPressed) => {}
            //);


            new Alert(I2.Loc.LocalizationManager.GetTranslation("PurchaseIsNotValidTitle"),
             I2.Loc.LocalizationManager.GetTranslation("PurchaseIsNotValidMessage"))
             .SetPositiveButton(I2.Loc.LocalizationManager.GetTranslation("PurchaseIsNotValidMessageButton"), () => {
                 _onCashPurchaseFailed.Invoke();
             })
             .Show();

        }

        public void ValidPurchase(string pJson, string pRef)
        {
  
            new Alert(I2.Loc.LocalizationManager.GetTranslation("PurchaseIsValidTitle"),
           I2.Loc.LocalizationManager.GetTranslation("PurchaseIsValidMessage"))
           .SetPositiveButton(I2.Loc.LocalizationManager.GetTranslation("OkButton"), () => {
               GameObject Home_Grp = UI.Instance.PrefabsIdxArray_Gob[Array.IndexOf(UI.Instance.PrefabsIdxArray_Str, "Home_Grp")];
               Home_Grp.SetActive(true);
               UI.Instance.CurrentActive_Go = Home_Grp;

               MetaData.Instance.LootMain.ParseAskManagerLoot(pJson, "shop");

               ReferencePurchased = pRef;
               _onProductPurchaseSucceed.Invoke();
               _userMain.GetProfile();
           })
           .Show();

            //AlertDialogs.Instance.ShowAlert(
            //    I2.Loc.LocalizationManager.GetTranslation("PurchaseIsValidTitle"),
            //    I2.Loc.LocalizationManager.GetTranslation("PurchaseIsValidMessage"),
            //    I2.Loc.LocalizationManager.GetTranslation("OkButton"),
            //    (_buttonPressed) =>
            //    {
            //        Debug.Log("yoop");

            //    }
            //);


        }

        public void NotValidPurchase(string pRef)
        {


            new Alert(I2.Loc.LocalizationManager.GetTranslation("PurchaseNotEnoughMoneyTitle"),
           I2.Loc.LocalizationManager.GetTranslation("PurchaseNotEnoughMoneyMessage"))
           .SetPositiveButton(I2.Loc.LocalizationManager.GetTranslation("OkButton"), () => {
               _onProductPurchaseFailed.Invoke();
               ReferencePurchased = pRef;
           })
           .Show();

            _onProductPurchaseFailed.Invoke();
            ReferencePurchased = pRef;

        }

        public void HandleJsonProduct(string pData)
        {
            JToken lJToken = JToken.Parse(pData);
            Product.Init(lJToken);

            _onProductGetInfos.Invoke();
        }

		public void HandleJsonProductList(string pData)
		{

			ProductData.Init(pData);

			if (null != _onGetProductList)
			{
				_onGetProductList.Invoke();
			}
		}


        #endregion

        #region Protected Methods

        /// <summary>
        /// MonoBehaviour method used to link OnRequestSuccess to API
        /// </summary>
        protected override void OnEnable()
        {
            if (null != Api.Instance)
            {
                IapStoreManager.OnPurchase += OnRequestPurchase;
            }
        }

		/// <summary>
		/// MonoBehaviour method used to unlink OnRequestSuccess from API
		/// </summary>
		protected override void OnDisable()
        {
            if (null != Api.Instance)
            {
                IapStoreManager.OnPurchase += OnRequestPurchase;
            }
        }

		#endregion

		#region Private Methods

		/// <summary>
		/// Callback invoke when request OnPurchase is trigger
		/// </summary>
		/// <param name="pProduct">Url used</param>
		private void OnRequestPurchase(Product pProduct)
        {
            _shopAPICalls.BuyProductWithRealMoney(pProduct);
        }

        protected override void OnProductPurchased(Product pProduct)
        {
            // Debug.Log("OnProductPurchased Event !!!!!!!");
            _shopAPICalls.BuyProductWithRealMoney(pProduct);
        }

 
        #endregion
    }

}

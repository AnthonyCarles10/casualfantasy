using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

namespace Sportfaction.CasualFantasy.ArenaCup
{
    /// <summary>
    /// Description: Class contains all loot datat
    /// Author: Antoine de Lachèze-Murel
    /// </summary>
	[CreateAssetMenu(fileName = "Store-ArenaCup", menuName = "CasualFantasy/Data/Store/ArenaCup")]
    public sealed class ArenaCupData : ScriptableObject
    {
        #region Properties

        [JsonProperty("rewards")]
        public List<ArenaCupDataReward> Rewards { get { return _rewards; } }

        #endregion

        #region Fields

        [SerializeField, Tooltip("ArenaCupDataReward")]
        private List<ArenaCupDataReward> _rewards = new List<ArenaCupDataReward>();


        #endregion

        #region Public Methods

        /// <summary>
        /// Initialize Rewards data from a Json
        /// </summary>
        /// <param name="pJson">Specific json to use</param>
        public void Parse(string pJson)
        {
            Assert.IsFalse(string.IsNullOrEmpty(pJson), "ArenaCupData: Init(), pJson is null.");

            if (false == pJson.Contains("\"rewards\":"))
            {
                pJson = "{\"rewards\":" + pJson + "}";

            }
            JToken lJToken = JToken.Parse(pJson);
            JArray lrewardsArray = lJToken.Value<JArray>("rewards");

            _rewards = new List<ArenaCupDataReward>();

            foreach (JToken lReward in lrewardsArray)
            {
                ArenaCupDataReward lNewReward = new ArenaCupDataReward();
                lNewReward.Init(lReward);

                _rewards.Add(lNewReward);
            }
        }

        #endregion
    }
    
}
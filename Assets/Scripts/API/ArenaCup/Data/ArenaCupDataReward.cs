using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

namespace Sportfaction.CasualFantasy.ArenaCup
{
    /// <summary>
    /// Description: Arena Cup Data Rewards
    /// Author: Antoine de Lachèze-Murel
    /// </summary>
    public sealed class ArenaCupDataReward
    {

        #region Properties

        [JsonProperty("cycle_leaderboard_key")]
        public string CycleKey { get { return _cycleKey; } }

        [JsonProperty("bracket")]
        public int Bracket { get { return _bracket; } }

        [JsonProperty("min_rank")]
        public int MinRank { get { return _minRank; } }

        [JsonProperty("max_rank")]
        public int MaxRank { get { return _maxRank; } }

        [JsonProperty("name")]
        public string Name { get { return _name; } }

        [JsonProperty("big_box")]
        public int BigBox { get { return _bigBox; } }

        [JsonProperty("medium_box")]
        public int MediumBox { get { return _mediumBox; } }

        [JsonProperty("fragment_box")]
        public int FragmentBox { get { return _fragmentBox; } }

        [JsonProperty("hard")]
        public int Hard { get { return _hard; } }

        #endregion

        #region Fields

        [SerializeField]
        private string _cycleKey;

        [SerializeField]
        private int _bracket;

        [SerializeField]
        private int _minRank;

        [SerializeField]
        private int _maxRank;

        [SerializeField]
        private string _name;

        [SerializeField]
        private int _bigBox;

        [SerializeField]
        private int _mediumBox;

        [SerializeField]
        private int _fragmentBox;

        [SerializeField]
        private int _hard;


        #endregion

        #region Public Methods

        /// <summary>
        /// Initialize Loot data box from a Json
        /// </summary>
        /// <param name="pJson">Specific json to use</param>
        public void Init(JToken pToken)
        {
            _cycleKey = pToken.Value<string>("cycle_leaderboard_key");

            _bracket = pToken.Value<int>("bracket");

            _minRank = pToken.Value<int>("min_rank");

            _maxRank = pToken.Value<int>("max_rank");

            _name = pToken.Value<string>("name");

            _bigBox = pToken.Value<int>("big_box");

            _mediumBox = pToken.Value<int>("medium_box");

            _fragmentBox = pToken.Value<int>("fragment_box");

            _hard = pToken.Value<int>("hard");
        }

        #endregion
    }
}
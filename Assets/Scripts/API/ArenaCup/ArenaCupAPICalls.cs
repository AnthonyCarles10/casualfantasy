using Sportfaction.CasualFantasy.Services;

using BestHTTP;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using System;

namespace Sportfaction.CasualFantasy.ArenaCup
{
	/// <summary>
	/// Description: 
	/// Author: Antoine de Lach�ze-Murel
	/// </summary>
	public sealed class ArenaCupAPICalls : MonoBehaviour
	{
        #region Fields

        [SerializeField, Tooltip("ArenaCupMain")]
        private ArenaCupMain _arenaCupMain = null;

        private string _arenaCupRewardsURL = "";
        private string _getCurrentEndTimestampArenaCupURL = "";

        #endregion

        #region Public Methods

        /// <summary>
        /// Open a Loot box
        /// </summary>
        public void GetRewardsArenaCup()
		{
			if (null != Api.Instance)
			{
                _arenaCupRewardsURL = Api.Instance.GenerateUrl(ApiConstants.GetRewardsArenaCup);

                Assert.IsFalse(string.IsNullOrEmpty(_arenaCupRewardsURL), "[ArenaCupAPICalls] GetRewardsArenaCup(), lUrl is null or empty.");

                HTTPRequest lRequest = Api.Instance.SetUpRequest(_arenaCupRewardsURL, Api.Methods.GET, true);
                lRequest.Timeout = TimeSpan.FromSeconds(5);

                StartCoroutine(Api.Instance.SendRequest(lRequest));
            }
		}


        /// <summary>
        /// Get Current End Timestamp ArenaCup
        /// </summary>
        public void GetCurrentEndTimestampArenaCup()
		{
			if (null != Api.Instance)
			{
                _getCurrentEndTimestampArenaCupURL = Api.Instance.GenerateUrl(ApiConstants.GetCurrentEndTimestampArenaCup);

                Assert.IsFalse(string.IsNullOrEmpty(_getCurrentEndTimestampArenaCupURL), "[ArenaCupAPICalls] GetRewardsArenaCup(), lUrl is null or empty.");

                HTTPRequest lRequest = Api.Instance.SetUpRequest(_getCurrentEndTimestampArenaCupURL, Api.Methods.GET, true);
                lRequest.Timeout = TimeSpan.FromSeconds(5);

                StartCoroutine(Api.Instance.SendRequest(lRequest));
            }
		}




        public void AddEvents()
		{
			if (null != Api.Instance)
			{
				Api.Instance.OnSuccess += OnRequestSuccess;
			}
		}

		#endregion

		#region Private Methods

		/// <summary>
		/// MonoBehaviour method used to link OnRequestSuccess to API
		/// </summary>
		private void OnEnable()
        {
            if (null != Api.Instance)
            {
                Api.Instance.OnSuccess += OnRequestSuccess;
            }
        }

        /// <summary>
        /// MonoBehaviour method used to unlink OnRequestSuccess from API
        /// </summary>
        private void OnDisable()
        {
            if (null != Api.Instance)
            {
                Api.Instance.OnSuccess -= OnRequestSuccess;
            }
        }

        /// <summary>
        /// Send a request to API
        /// </summary>
        /// <param name="pUrl"></param>
        /// <param name="pMethod"></param>
        /// <param name="pAuthenticationRequired"></param>
        private void SendRequest(string pUrl, Api.Methods pMethod, bool pAuthenticationRequired)
		{
			HTTPRequest lRequest = Api.Instance.SetUpRequest(pUrl, pMethod, pAuthenticationRequired);

			Assert.IsNotNull(lRequest, "[LeaderBoardAPICalls] SendRequest(), lRequest is null.");
            lRequest.Timeout = TimeSpan.FromSeconds(5);

            StartCoroutine(Api.Instance.SendRequest(lRequest));
		}

        /// <summary>
        /// Callback invoke when request from API was done successfully
        /// </summary>
        /// <param name="pCurrentUrl">string</param>
        /// <param name="pMethod">HTTPMethods</param>
        /// <param name="pData">string</param>
        private void OnRequestSuccess(string pCurrentUrl, HTTPMethods pMethod, string pData)
        {
            if (true == pCurrentUrl.Equals(_arenaCupRewardsURL))
            {
                _arenaCupMain.ParseRewardsArenaCup(pData);
            }
            if (true == pCurrentUrl.Equals(_getCurrentEndTimestampArenaCupURL))
            {
                _arenaCupMain.ParseCurrentEndTimestampArenaCup(pData);
            }
        }

        #endregion
    }
}

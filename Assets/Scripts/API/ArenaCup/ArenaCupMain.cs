using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Sportfaction.CasualFantasy.Events;
using Sportfaction.CasualFantasy.Services.Notifications;
using Sportfaction.CasualFantasy.Services.Timers;
using UnityEngine;
using UnityEngine.Assertions;

namespace Sportfaction.CasualFantasy.ArenaCup
{
    public class ArenaCupMain : MonoBehaviour
    {

        #region Variables

        [Header("API")]
        [SerializeField]
        private ArenaCupAPICalls _arenaCupAPICalls;

        [SerializeField]
        public ArenaCupData ArenaCupData;

        #endregion

        #region Events

        [Header("Game Events")]
        [SerializeField]
        private GameEvent _onGetRewardsArenaCup;

        [SerializeField]
        private GameEvent _onEndArenaCupTimer;


        #endregion

        #region API Calls

        public void GetRewardsArenaCup() { _arenaCupAPICalls.GetRewardsArenaCup(); }
        public void GetCurrentEndTimestampArenaCup() { _arenaCupAPICalls.GetCurrentEndTimestampArenaCup(); }

		#endregion

		#region Public Methods
		/// <summary>
		/// Parse json to open a loot box
		/// </summary>
		/// <param name="pData">Json to parse</param>
		public void ParseRewardsArenaCup(string pData)
        {

            ArenaCupData = new ArenaCupData();
            ArenaCupData.Parse(pData);

            SaveData();

            _onGetRewardsArenaCup.Invoke();
        }

        /// <summary>
		/// Parse json to open a loot box
		/// </summary>
		/// <param name="pData">Json to parse</param>
		public void ParseCurrentEndTimestampArenaCup(string pJson)
        {


            JToken lJToken = JToken.Parse(pJson);
            string lcurrentTimestamp = lJToken.Value<string>("current_timestamp");
            string lendTimestamp = lJToken.Value<string>("end_timestamp");


            LocalTimerData lTimer =  LocalTimersManager.Instance.StartTimer("EndTimerArenaCup", lendTimestamp, lcurrentTimestamp, _onEndArenaCupTimer);

            //if (null != NotificationsManager.Instance)
            //{
            //    LocalNotificationData lData = NotificationsManager.Instance.Database.GetLocalNotification("medium_box");
            //    lData.Interval = eRepeatInterval.OTHER;
            //    lTimer.TimeLeft.Subtract(TimeSpan.FromHours(3));
            //    lData.OtherInteval = (long)lTimer.TimeLeft.TotalMilliseconds;
            //    NotificationsManager.Instance.SendLocalNotification("arenacup_end");
            //}
        }

        public void LoadData()
        {
            if (PlayerPrefs.HasKey("arenacup"))
            {
                string lJson = PlayerPrefs.GetString("arenacup");


                ArenaCupData = ScriptableObject.CreateInstance<ArenaCupData>();
                ArenaCupData.Parse(lJson);

                _onGetRewardsArenaCup.Invoke();

            }
        }

        public void SaveData()
        {
            PlayerPrefs.SetString("arenacup", JsonConvert.SerializeObject(ArenaCupData));

            PlayerPrefs.Save();
        }

        #endregion

        private void Awake()
        {
            LoadData();
        }
    }
}

﻿using Sportfaction.CasualFantasy.Manager.Profile;
using Sportfaction.CasualFantasy.Services;

using BestHTTP;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using System;

namespace Sportfaction.CasualFantasy.Action
{
	/// <summary>
	/// Class handling every action related API calls
	/// </summary>
	public sealed class ActionAPICalls : MonoBehaviour
	{
        #region Fields

        [Header("Links")]
        [SerializeField, Tooltip("ActionMain")]
        private ActionMain  _actionMain =   null; //!< Class handling action API data management

        private string  _getListActionsURL              =   ""; //!< URL for list of actions retrieval
        private string  _getListPositionsActionsURL     =   ""; //!< URL for actions per minion position retrieval
        private string  _getTacticalAvailableActionsURL =   ""; //!< URL for 
        private string  _getTacticalMemberActionsURL    =   "";
        private string  _getActionsBalancing            =   ""; //!< URL for action balancing retrieval

        #endregion

        #region Public Methods

        /// <summary>
        /// Get List cards
        /// </summary>
        public  void    GetListActions(string pLimit)
		{
			if (null != Api.Instance)
			{
                Dictionary<string, string> lQueryParams = new Dictionary<string, string>()
                {
                    { ApiConstants.GetListActionsLimitKey, pLimit }
                };

                _getListActionsURL = Api.Instance.GenerateUrl(ApiConstants.GetListActions, lQueryParams);

				Assert.IsFalse(string.IsNullOrEmpty(_getListActionsURL), "[ActionAPICalls] GetListActions(), lUrl is null or empty.");

                HTTPRequest lRequest = Api.Instance.SetUpRequest(_getListActionsURL, Api.Methods.GET, true);
                lRequest.Timeout = TimeSpan.FromSeconds(5);

                StartCoroutine(Api.Instance.SendRequest(lRequest));
            }
		}

        /// <summary>
        /// Get List Positions Actions (Minions)
        /// </summary>
        public  void    GetListPositionsActions()
        {
            if (null != Api.Instance)
            {
                _getListPositionsActionsURL = Api.Instance.GenerateUrl(ApiConstants.GetListPositionsActions);

                Assert.IsFalse(string.IsNullOrEmpty(_getListPositionsActionsURL), "[ActionAPICalls] GetListPositionsActions(), lUrl is null or empty.");

                HTTPRequest lRequest = Api.Instance.SetUpRequest(_getListPositionsActionsURL, Api.Methods.GET, true);
                lRequest.Timeout = TimeSpan.FromSeconds(5);

                StartCoroutine(Api.Instance.SendRequest(lRequest));
            }
        }

        /// <summary>
        /// Get Actions Balancing
        /// </summary>
        public  void    GetActionsBalancing()
        {
            if (null != Api.Instance)
            {
                _getActionsBalancing = Api.Instance.GenerateUrl(ApiConstants.GetActionsBalancing);

                Assert.IsFalse(string.IsNullOrEmpty(_getActionsBalancing), "[ActionAPICalls] GetActionsBalancing(), lUrl is null or empty.");

                HTTPRequest lRequest = Api.Instance.SetUpRequest(_getActionsBalancing, Api.Methods.GET, true);
                lRequest.Timeout = TimeSpan.FromSeconds(5);

                StartCoroutine(Api.Instance.SendRequest(lRequest));
            }
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Links class methods to API callbacks
        /// </summary>
        private void    OnEnable()
        {
            if (null != Api.Instance)
            {
                Api.Instance.OnSuccess      +=  OnRequestSuccess;
                Api.Instance.OnBadRequest   +=  OnBadRequestEvent;
            }
        }

        /// <summary>
        /// Unlinks class methods to API callbacks
        /// </summary>
        private void    OnDisable()
        {
            if (null != Api.Instance)
            {
                Api.Instance.OnSuccess      -=  OnRequestSuccess;
                Api.Instance.OnBadRequest   -=  OnBadRequestEvent;
            }
        }

        /// <summary>
        /// Sends request to API
        /// </summary>
        /// <param name="pUrl">Url to call</param>
        /// <param name="pMethod">API call method</param>
        /// <param name="pAuthenticationRequired">Is authentication required ?</param>
        private void    SendRequest(string pUrl, Api.Methods pMethod, bool pAuthenticationRequired)
		{
			HTTPRequest lRequest = Api.Instance.SetUpRequest(pUrl, pMethod, pAuthenticationRequired);
            lRequest.Timeout = TimeSpan.FromSeconds(5);

            Assert.IsNotNull(lRequest, "[LadderAPICalls] SendRequest(), lRequest is null.");

			StartCoroutine(Api.Instance.SendRequest(lRequest));
		}

        /// <summary>
        /// Callback invoked when API request was successfully done
        /// </summary>
        /// <param name="pCurrentUrl">Url called</param>
        /// <param name="pMethod">API call method</param>
        /// <param name="pData">Data to handle</param>
        private void    OnRequestSuccess(string pCurrentUrl, HTTPMethods pMethod, string pData)
        {
            Assert.IsNotNull(_actionMain, "[ActionAPICalls] OnRequestSuccess(), _actionMain is null or empty.");

            if (true == pCurrentUrl.Equals(_getListPositionsActionsURL))
            {
                _actionMain.OnGetListPositionsActions(pData);
            }
            else if (true == pCurrentUrl.Equals(_getListActionsURL))
            {
                _actionMain.OnGetListActions(pData);
            }
            else if (true == pCurrentUrl.Equals(_getActionsBalancing))
            {
                _actionMain.OnGetActionsBalancing(pData);
            }
        }

        /// <summary>
		/// Callback invokeD when request from API returned bad request error
		/// </summary>
		/// <param name="pCurrentUrl">Url called</param>
		/// <param name="pData">Data received</param>
        private void    OnBadRequestEvent(string pCurrentUrl, string pData)
        {
			#if UNITY_EDITOR && DEVELOPMENT_BUILD
		        Debug.LogFormat("<color=red>[CardAPICalls]</color> OnBadRequestEvent(): {0}", pData);
			#endif
        }

		#endregion
    }
}
﻿namespace Sportfaction.CasualFantasy.Action
{
    /// <summary>
    /// Enum containing every type of action
    /// </summary>
    public enum eActionType
    {
        Common,
        Action,
        Malus,
        Bonus
    }
}

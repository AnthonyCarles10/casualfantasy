﻿using Sportfaction.CasualFantasy.Composition;
using Sportfaction.CasualFantasy.Events;

using UnityEngine;

namespace Sportfaction.CasualFantasy.Action
{
    /// <summary>
    /// Class handling every action API data management
    /// </summary>
    public class ActionMain : MonoBehaviour
    {
        #region Fields

        [Header("Links")]
        [SerializeField, Tooltip("ActionAPICalls")]
        private ActionAPICalls  _actionAPICalls =   null; //!< Class handling every action related API calls

        [Header("Data")]
        [SerializeField, Tooltip("ActionData")]
        private ActionsData         _actionsData            =   null;
        [SerializeField, Tooltip("PositionsActionData")]
        private PositionsActions    _positionsActionsData   =   null;
        [SerializeField, Tooltip("ActionsBalancing")]
        private ActionsBalancing    _actionsBalancingData   =   null;
        [SerializeField, Tooltip("Composition")]
        private CompositionData     _composition            =   null;

        [Header("Game Events")]
        [SerializeField]
        private GameEvent   _onGetActionsList           =   null;
        [SerializeField]
        private GameEvent   _onGetPositionsActionsList  =   null;
        [SerializeField]
        private GameEvent   _onGetActionsBalancing      =   null;

        #endregion

        #region Public Methods

        #region API Calls

        public  void    GetListActions(string pLimit) { _actionAPICalls.GetListActions(pLimit); }
        public  void    GetListPositionsActions() { _actionAPICalls.GetListPositionsActions(); }
        public  void    GetActionsBalancing() { _actionAPICalls.GetActionsBalancing(); }

        #endregion
        
        /// <summary>
        /// Parses action list JSON & stores data
        /// </summary>
        /// <param name="pJson">JSON data to parse</param>
        public  void    OnGetListActions(string pJson)
        {	
            _actionsData.Clean();
            _actionsData.Init(pJson);

            GameObject  lPersistableSOCards =   GameObject.Find("/#Essentials/PersistableSO");
            lPersistableSOCards.GetComponent<PersistableSO>().Persist();

            _onGetActionsList.Invoke();
        }
        
        /// <summary>
        /// Parses actions per minion position JSON & stores data
        /// </summary>
        /// <param name="pJson">JSON data to parse</param>
        public  void    OnGetListPositionsActions(string pJson)
        {
            _positionsActionsData.Init(pJson);

            _onGetPositionsActionsList.Invoke();
        }

        /// <summary>
        /// Parses action balancing JSON & stores data
        /// </summary>
        /// <param name="pJson">JSON data to parse</param>
        public  void    OnGetActionsBalancing(string pJson)
        {
            _actionsBalancingData.Parse(pJson);

            _onGetActionsBalancing.Invoke();
        }

        #endregion
    }
}

﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using UnityEngine;

namespace Sportfaction.CasualFantasy.Action
{
    /// <summary>
    /// Class storing action data
    /// </summary>
    [CreateAssetMenu(fileName = "Store-PositionsActions", menuName = "CasualFantasy/Data/Store/PositionsActions")]
    public sealed class PositionsActions : ScriptableObject
    {
        #region Properties

        //! List of actions for minion
        public  List<PositionsActionsItem>  Items   { get { return _items; } }

        #endregion

        #region Fields

        [SerializeField, Tooltip("_items")]
        private List<PositionsActionsItem>  _items  =   new List<PositionsActionsItem>(); //!< List of actions for minion

        #endregion

        #region Public Methods

        /// <summary>
        /// Initializes data from JSON sent by API
        /// </summary>
        /// <param name="pJson">JSON to parse</param>
        public	void    Init(string pJson)
        {
            JToken  lJToken         =   JToken.Parse("{\"items\":" + pJson + "}");
            JArray  lActionsArray   =   lJToken.Value<JArray>("items");

			_items.Clear();

            foreach (JToken pToken in lActionsArray)
            {
                PositionsActionsItem    lItem   =   new PositionsActionsItem(pToken);
                //lItem.Init(pToken);
                _items.Add(lItem);
            }
        }

        /// <summary>
        /// Converts class to JSON
        /// </summary>
        public  string  ConvertToJson()
        {
            return JsonConvert.SerializeObject(this);
        }

        /// <summary>
        /// Gets basic actions for specified minion position & level
        /// </summary>
        /// <param name="pPosition">Minion position</param>
        /// <param name="pLevel">Minion level</param>
        public  List<PositionsActionsItemLevelBasic>    GetListBasicActionsByPositionAndLevel(string pPosition, int pLevel)
        {
            foreach (PositionsActionsItem lItem in _items)
            {
                if (lItem.Position == pPosition)
                {
                    foreach (PositionsActionsItemLevel item in lItem.Levels)
                    {
                        if (item.Level == pLevel)
                        {
                            return item.BasicActions;
                        }
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// Gets tactical actions for specified minion position & level
        /// </summary>
        /// <param name="pPosition">Minion position</param>
        /// <param name="pLevel">Minion level</param>
        public  List<PositionsActionsItemLevelTactical> GetListTacticalActionsByPositionAndLevel(string pPosition, int pLevel)
        {
            foreach (PositionsActionsItem lItem in _items)
            {
                if (lItem.Position == pPosition)
                {
                    foreach (PositionsActionsItemLevel item in lItem.Levels)
                    {
                        if (item.Level == pLevel)
                        {
                            return item.TacticalActions;
                        }
                    }
                }
            }

            return null;
        }


        #endregion
    }
}

﻿using Sportfaction.CasualFantasy.Services;

using SportFaction.CasualFootballEngine.ActionService.Enum;

using Newtonsoft.Json.Linq;
using UnityEngine;

namespace Sportfaction.CasualFantasy.Action
{
	/// <summary>
	/// Class storing action balancing data
	/// </summary>
    /// @author Antoine Delachèze-Murel
    [System.Serializable]
    public sealed class ActionsBalancingItem
	{
        #region Properties

        public  eAction Attack              { get { return _attack; } }
        public  eAction Defense             { get { return _defense; } }

        public  int     AttackProbability   { get { return _attackProbability; } }
        public  int     DefenseProbability  { get { return _defenseProbability; } }

        #endregion

        #region Variables
        
        [SerializeField, ReadOnly]
        private eAction _attack     =   eAction.NONE;
        [SerializeField, ReadOnly]
        private eAction _defense    =   eAction.NONE;

        [SerializeField, ReadOnly]
        private int _attackProbability  =   0;
        [SerializeField, ReadOnly]
        private int _defenseProbability =   0;

        #endregion

        #region Public Methods

        /// <summary>
        /// Initializes class instance by parsing token
        /// </summary>
        /// <param name="pToken">Token to parse</param>
        public ActionsBalancingItem(JToken pToken)
        {
            _attack     =   eActionMethods.ConvertStringToEnum(pToken.SafeValue<string>("attack_action_type"));
            _defense    =   eActionMethods.ConvertStringToEnum(pToken.SafeValue<string>("defense_action_type"));

            _attackProbability  =   pToken.SafeValue<int>("attack_probability");
            _defenseProbability =   pToken.SafeValue<int>("defense_probability");

        }

        #endregion
    }
}
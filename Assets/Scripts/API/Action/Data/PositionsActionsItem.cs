﻿using Newtonsoft.Json.Linq;
using System.Collections.Generic;

namespace Sportfaction.CasualFantasy.Action
{
    /// <summary>
    /// Class storing position action data
    /// </summary>
    public sealed class PositionsActionsItem
    {
        #region Properties

        public  string      Position             { get { return _position; } } 

        public List<PositionsActionsItemLevel> Levels { get { return _levels; } }

        #endregion

        #region Fields

        private string  _position   =   "";

        private List<PositionsActionsItemLevel> _levels = new List<PositionsActionsItemLevel>();

        #endregion

        #region Public Methods

        public  PositionsActionsItem(JToken pItem)
        {
            _position   = pItem.Value<string>("squad_position");

            JArray   lBasicActions   = pItem.Value<JArray>("levels");

            foreach(JToken pToken in lBasicActions)
            {
                PositionsActionsItemLevel lItem = new PositionsActionsItemLevel();
                lItem.Init(pToken);
                _levels.Add(lItem);
            }
        }
        
        /*public    void    Init(JToken pItem)
        {
            _position   = pItem.Value<string>("squad_position");

            JArray   lBasicActions   = pItem.Value<JArray>("levels");

            foreach(JToken pToken in lBasicActions)
            {
                PositionsActionsItemLevel lItem = new PositionsActionsItemLevel();
                lItem.Init(pToken);
                _levels.Add(lItem);
            }
        }*/
   
        #endregion

    }
}
﻿using SportFaction.CasualFootballEngine.ActionService.Enum;

using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Sportfaction.CasualFantasy.Action
{
    /// <summary>
    /// Class containing database of every action in game
    /// </summary>
    /// @author Rémi Carreira
    [CreateAssetMenu(fileName = "New Actions Data", menuName = "CasualFantasy/Data/Actions")]
    public sealed class ActionsData : ScriptableObject
    {
        #region Properties

        public  List<ActionsItemData>   Items   { get { return _items; } } //!< List of actions existing in game

        #endregion

        #region Fields

        [SerializeField, Tooltip("List of actions existing in game")]
        private List<ActionsItemData>   _items  =   new List<ActionsItemData>(); //!< List of actions existing in game

        #endregion

        #region Public Methods

        /// <summary>
        /// Initializes action database from API JSON
        /// </summary>
        /// <param name="pJson">JSON to parse</param>
        public  void            Init(string pJson)
        {
            //pJson = "{\"items\":" + pJson + "}";
            JToken	lJToken			=	JToken.Parse(pJson);
            JArray	lActionsArray	=	lJToken.Value<JArray>("list");

            List<ActionsItemData> lActionsItemData = new List<ActionsItemData>();

            foreach (JToken lActionToken in lActionsArray)
            {
                ActionsItemData lAction = new ActionsItemData();
                bool lValue = lAction.InitAction(lActionToken);

                if(true == lValue)
                {
                    lActionsItemData.Add(lAction);
                }
            
            }

            _items = lActionsItemData.ToList();
        }

        /// <summary>
        /// Gets action item for specified name
        /// </summary>
        /// <param name="pKeyName">Keyname of action to search for</param>
        public  ActionsItemData GetActionByKeyName(string pKeyName)
        {
            return Items.Find(item => item.KeyName == pKeyName);
        }

        /// <summary>
        /// Gets action item for specified type
        /// </summary>
        /// <param name="pAction">Type of action to search for</param>
        public  ActionsItemData GetActionData(eAction pAction)
        {
            return Items.Find(item => item.Type == pAction);
        }

		/// <summary>
		/// Gets action reward for specified type
		/// </summary>
        /// <param name="pActionType">Type of action to check</param>
		public	int             GetActionReward(eAction pActionType)
		{
			ActionsItemData		lData	=	GetActionData(pActionType);
            
			if (null != lData)
			{
				return	lData.SoftAwardOnSuccess;
			}
			return	0;
		}

		/// <summary>
		/// Gets action cost for specified type
		/// </summary>
		/// <param name="pActionType">Type of action to check</param>
		public  ushort          GetActionCost(eAction pActionType)
		{
			ActionsItemData		lData	=	GetActionData(pActionType);
			if (null != lData)
			{
				return	lData.Cost;
			}
			return ushort.MaxValue;
		}

        /// <summary>
        /// Empties action list
        /// </summary>
        public  void            Clean()
        {
            _items.Clear();
        }

		/// <summary>
		/// Retrieves all critical rates
		/// </summary>
		/// <returns>A dictionary (key: eAction, value: Integer list)</returns>
		public	Dictionary<eAction, List<int>>		RetrieveAllCriticalRate()
		{
			Dictionary<eAction, List<int>>		lDictionary	=	new Dictionary<eAction, List<int>>();
            
			if (null != lDictionary)
			{
				foreach (ActionsItemData lItem in _items)
				{
					if (lDictionary.ContainsKey(lItem.Type))
					{
						List<int>	lList	=	lDictionary[lItem.Type];
                        lList.Insert(lList.Count, lItem.CriticalRate);
					}
					else
					{
						List<int>	lList	=	new List<int>();
						if (null != lList)
						{
							lList.Add(lItem.CriticalRate);

							lDictionary.Add(lItem.Type, lList);
						}
					}
				}
			}
			
            return	lDictionary;
		}	

        #endregion
    }
}

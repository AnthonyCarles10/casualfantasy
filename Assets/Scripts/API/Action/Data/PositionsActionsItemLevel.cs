﻿using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using UnityEngine;

namespace Sportfaction.CasualFantasy.Action
{
	/// <summary>
	/// Description: All data used to create a card or a token
	/// Author: Rémi Carreira
	/// </summary>
    [System.Serializable]
    public sealed class PositionsActionsItemLevel
	{
        #region Properties

        public  int         Level                { get { return _level; } } 

        public List<PositionsActionsItemLevelBasic> BasicActions { get { return _basicActions; } } 

		public List<PositionsActionsItemLevelTactical> TacticalActions { get { return _tacticalActions; } } 

        #endregion

        #region Fields

        [SerializeField, ReadOnly]
        private int     _level = 0;

        [SerializeField]
        private List<PositionsActionsItemLevelBasic> _basicActions = new List<PositionsActionsItemLevelBasic>();

        [SerializeField]
        private List<PositionsActionsItemLevelTactical> _tacticalActions = new List<PositionsActionsItemLevelTactical>();

        #endregion

        #region Public Methods

        public	void	Init(JToken pItem)
        {
            _level      = pItem.Value<int>("level");

            JArray   lBasicActions   = pItem.Value<JArray>("basic_actions");

            foreach(JToken pToken in lBasicActions)
            {
                PositionsActionsItemLevelBasic lItem = new PositionsActionsItemLevelBasic();
                bool lValue =  lItem.Init(pToken);

                if(true == lValue)
                {
                    _basicActions.Add(lItem);
                }
           
            }

            JArray   lTacticalActions   = pItem.Value<JArray>("tactical_actions");

            foreach (JToken pToken in lTacticalActions)
            {
                PositionsActionsItemLevelTactical lItem = new PositionsActionsItemLevelTactical();
                lItem.Init(pToken);
                _tacticalActions.Add(lItem);
            }

        }

   
		#endregion

	}
}
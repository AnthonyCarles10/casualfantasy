﻿using SportFaction.CasualFootballEngine.ActionService.Enum;

using Newtonsoft.Json.Linq;
using UnityEngine;

namespace Sportfaction.CasualFantasy.Action
{
    /// <summary>
    /// Description: PositionsActionsItemBasic
    /// Author: Antoine de Lachèze-Murel
    /// </summary>
    [System.Serializable]
    public sealed class PositionsActionsItemLevelBasic
	{
        #region Properties

        public  eAction     Action               { get { return _action; } } 

		public	bool        WithBall             { get { return _withBall; } } 

        #endregion

        #region Fields

        [SerializeField, ReadOnly]
        private	eAction _action = eAction.NONE;

		[SerializeField, ReadOnly]
		private	bool  _withBall   =	false;

        #endregion

        #region Public Methods

        public	bool	Init(JToken pItem)
        {
            if(pItem.Value<string>("action_name") == "SPRINT")
            {
                return false;
            }
            _action = eActionMethods.ConvertStringToEnum(pItem.Value<string>("action_name"));
            _withBall = pItem.Value<bool>("with_ball");

            return true;
        }

   
		#endregion

	}
}
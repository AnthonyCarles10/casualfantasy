﻿using Sportfaction.CasualFantasy.Services;

using SportFaction.CasualFootballEngine.ActionService.Enum;

using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

namespace Sportfaction.CasualFantasy.Action
{
	/// <summary>
	/// Class storing action data
	/// </summary>
    [System.Serializable]
    public sealed class ActionsItemData
	{
        #region Properties

        public  int         Id                      { get { return _id; } } //!< Action ID set by API
		public	bool        IsCardOwned             { get { return _isCardOwned; } set { _isCardOwned = value; } } //!< Does manager own action ?
        public	bool		Attack					{ get { return _attack; } } //!< Is action used for attack ?
        public	bool		Defense					{ get { return _defense; } } //!< Is action used for defense ?
        public	bool		AffectOnePlayer			{ get { return _affectOnePlayer; } } //! Is action applicable on one player only ?
        public	bool		AffectMyPlayers			{ get { return _affectMyPlayers; } } //!< Is action applicable to current manager's players ?
        public	bool		AffectTheirPlayers		{ get { return _affectTheirPlayers; } } //!< Is action applicable to opponent manager's players ?
        public	bool		TeamNeedBall			{ get { return _teamNeedBall; } } //!< Is action applicable to team possessing ball only ?
        public	bool		PlayerNeedBall			{ get { return _playerNeedBall; } } //!< Is action applicable to player possessing ball only ?
        public  bool        PlayerCantPossessBall   { get { return _isPlayerHasCardCantPossessBall; } }
        public  int         LevelToUnlock           { get { return _levelToUnlock; } }
        public  string      Category                { get { return _category; } }
        public	string		KeyName	                { get { return _keyName; } } //!< Action translation key
        public	byte		Point					{ get { return _point; } } //!< Number of points needed to use action
        public	int			Duration				{ get { return _duration; } } //!< Duration of action effect
        public  bool        IsFree                  { get { return _isFree; } } //!< Is action free of use ?
        public  eAction     Type                    { get { return  _type; } } //!< Action type
		public	ushort      Cost				    { get { return _cost; } } //!< Action cost in SF
		public	int		    SoftAwardOnSuccess	    { get { return _softAwardOnSuccess; } } //!< Soft awarded when action is successfully done
        public  int         StackOrder              { get { return _stackOrder; } } //!< Order of action display in action panel list
        public	int		    CriticalRate		    { get { return _criticalRate; } } //!< Action critical rate
        public  eAction[]   UnavailableActions      { get { return _unavailableActions; } } //!< Array of actions this action cannot be used after

        #endregion

        #region Fields

        [SerializeField, ReadOnly, Tooltip("Id of the card.")]
        private	int			_id								=	0;
		[SerializeField, ReadOnly, Tooltip("Is card owned by the manager?")]
		private	bool		_isCardOwned					=	false;
        [SerializeField, Tooltip("Is a useful card for attack?")]
        private	bool		_attack							=	false;
        [SerializeField, Tooltip("Is a useful card for defense?")]
        private	bool		_defense						=	false;
        [SerializeField, Tooltip("Is affecting only one player?")]
        private	bool		_affectOnePlayer				=	false;
        [SerializeField, Tooltip("Can affect my players?")]
        private	bool		_affectMyPlayers				=	false;
        [SerializeField, Tooltip("Can affect their players")]
        private	bool		_affectTheirPlayers				=	false;
        [SerializeField, Tooltip("Manager need ball to place this card")]
        private	bool		_teamNeedBall					=	false;
        [SerializeField, Tooltip("Manager need ball to place this card")]
        private	bool		_playerNeedBall					=	false;
        [SerializeField, Tooltip("Manager which has card can't poccess the ball")]
        private	bool		_isPlayerHasCardCantPossessBall	=	false;
        [SerializeField, Tooltip("How many point used to put this card?")]
        private	byte		_point							=	0;
        [SerializeField, Tooltip("Duration of this card effect in milliseconds. (0 = none)"), MinValue(0)]
        private	int			_duration						=	0;
        [SerializeField, Tooltip("Is card is free?")]
        private	bool		_isFree							=	false;
        [SerializeField, Tooltip("Level To Unlock")]
        private	int         _levelToUnlock                  =	-1;
        [SerializeField, Tooltip("Category")]
        private	string		_category						=	"";
        [SerializeField, Tooltip("_keyName")]
        private	string		_keyName						=	"";
        [SerializeField, Tooltip("Type of the action.")]
        private	eAction		_type							=	eAction.NONE;
        [SerializeField, Tooltip("Cost of this action")]
        private	ushort		_cost							=	0;
		[SerializeField, Tooltip("Soft currency won when this action finish successfully")]
		private	int			_softAwardOnSuccess				=	0;
		[SerializeField, Tooltip("Rate of critical for this action")]
        private int         _criticalRate                   =   0;
        [SerializeField, Tooltip("Stack Order for this action")]
        private	int         _stackOrder                     =	0;
        [SerializeField, Tooltip("Array of action where this action is unavailable for resolution")]
        private	eAction[]	_unavailableActions				=	new eAction[0];

        #endregion

        #region Public Methods

        /// <summary>
        /// Initializes action data from token
        /// </summary>
        /// <param name="pToken">Token to parse</param>
        public	bool	InitAction(JToken pToken)
        {

            //if(pToken.Value<string>("action_type") == "SPRINT")
            //{
            //    return false;
            //}

            _id					=	pToken.Value<int>("id");
            _attack				=	(pToken.Value<string>("is_attack") == "1") ? true : false;
            _defense			=	(pToken.Value<string>("is_defense") == "1") ? true : false;
            _affectOnePlayer	=	(pToken.Value<string>("is_affect_one_players") == "1") ? true : false;
            _affectTheirPlayers	=	(pToken.Value<string>("is_affect_their_players") == "1") ? true : false;
            _playerNeedBall		=	(pToken.Value<string>("is_player_need_ball") == "1") ? true : false;
            _teamNeedBall		=	(pToken.Value<string>("is_team_need_ball") == "1") ? true : false;
            _isPlayerHasCardCantPossessBall	=	(pToken.Value<string>("is_player_has_card_cant_possess_ball") == "1") ? true : false;
            _affectMyPlayers	=	(pToken.Value<string>("is_affect_my_players") == "1") ? true : false;
            _point				=	pToken.Value<byte>("action_point");
            _duration			=	pToken.Value<int>("duration");
            _isFree				=	(pToken.Value<string>("is_free") == "1") ? true : false;
            _levelToUnlock      =	pToken.Value<int>("level_to_unlock");
            _category			=	pToken.Value<string>("category");
            _keyName			=	pToken.Value<string>("key_name");
            _type				=	(eAction)System.Enum.Parse(typeof(eAction), pToken.Value<string>("action_type"));

			_stackOrder			=   pToken.SafeValue("stack_order", 0);
			_softAwardOnSuccess	=	pToken.SafeValue("soft_award_on_success", 0);
			_criticalRate		=	pToken.SafeValue("critical_rate", 0);
			_cost				=	pToken.SafeValue<ushort>("action_point", 0);
            
			InitializeUnavalaibleActions(pToken);

            return true;
        }

        /// <summary>
        /// Checks if action can be used in current situation
        /// </summary>
        /// <param name="pIsSoloAction">Is current situation a solo action phase ?</param>
        public	bool	CanUse(bool pIsSoloAction)
        {
            if (eAction.DRIBBLE == _type)
                return !pIsSoloAction;

            if (eAction.MOVE == _type || eAction.SPRINT == _type)
                return pIsSoloAction;

            return true;
        }

        public Sprite GetIcon(Sprite[] pSprites, bool pForBlueTeam = true)
        {
            if (eAction.MOVE == _type)
                return UtilityMethods.GetSprite(pSprites, "MoveIcon");

            switch(_type)
            {
                case eAction.LONG_KICK:
                    return UtilityMethods.GetSprite(pSprites, "Long_Kick_Icone");

                case eAction.COUNTER:
                case eAction.TACKLE:
                    return UtilityMethods.GetSprite(pSprites, StringExtensions.FirstCharToUpper(_type.ToString()) + "_Icone");

                default:
                    return UtilityMethods.GetSprite(pSprites, StringExtensions.FirstCharToUpper(_type.ToString()) + (pForBlueTeam ? "_Blue" : "_Red") + "_Icone");
            }
        }

		#endregion

		#region Private Methods

		/// <summary>
		/// Initializes actions this action cannot be used after
		/// </summary>
		/// <param name="pToken">Json token containing every unavailable action</param>
		private	void	InitializeUnavalaibleActions(JToken pToken)
		{
			JToken	lActionsToken	=	JToken.Parse(pToken.SafeValue<string>("unavailable_previous_actions"));
			if (null != lActionsToken)
			{
				JArray	lArray	=	lActionsToken.SafeValue<JArray>("actions");
				if (null != lArray)
				{
					List<eAction>	lActions	=	new List<eAction>();
					if (null != lActions)
					{
						foreach (JToken lItem in lArray)
						{
							Assert.IsNotNull(lItem, "[ActionsItemData] InitializeUnavalaibleActions(), lItem is null.");

							eAction	lAction	=	eActionMethods.ConvertStringToEnum(lItem.SafeValue<string>("type"));
							if (eAction.NONE != lAction)
							{
								lActions.Add(lAction);
							}
						}
					}

					_unavailableActions	=	lActions.ToArray();
				}
			}
		}
		
		#endregion
	}
}
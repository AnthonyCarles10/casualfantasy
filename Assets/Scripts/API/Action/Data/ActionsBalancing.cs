﻿using SportFaction.CasualFootballEngine.ActionService.Enum;

using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using UnityEngine;

namespace Sportfaction.CasualFantasy.Action
{
	/// <summary>
	/// Class storing every action balancing data
	/// </summary>
    /// @author Antoine Delachèze-Murel
	[CreateAssetMenu(fileName = "Store-ActionsBalancing", menuName = "CasualFantasy/Data/Store/ActionsBalancing")]
    public sealed class ActionsBalancing : ScriptableObject
    {
        #region Properties

        public List<ActionsBalancingItem> Items { get { return _items; } }

        #endregion

        #region Fields

        [SerializeField, Tooltip("_items")]
        private List<ActionsBalancingItem> _items = new List<ActionsBalancingItem>();

        #endregion

        #region Public Methods

        /// <summary>
        /// Parsing API JSON
        /// </summary>
        /// <param name="pJson">json response</param>
        public	void Parse(string pJson)
        {
            
            pJson = "{\"items\":" + pJson + "}";
            JToken lJToken = JToken.Parse(pJson);
            JArray lActionsArray = lJToken.Value<JArray>("items");

			_items.Clear();

            foreach (JToken ljToken in lActionsArray)
            {
                ActionsBalancingItem lItem = new ActionsBalancingItem(ljToken);

                _items.Add(lItem);
            }
        }
        
        public ActionsBalancingItem GetActionBalancingItem(eAction pAttackAction, eAction pDefenseAction)
        {
            int lNbItems = _items.Count;
            
            for (int li = 0; li < lNbItems; li++)
            {
                if (pAttackAction == _items[li].Attack && pDefenseAction == _items[li].Defense)
                    return _items[li];
            }
            
            return null;
        }

        #endregion
    }
}

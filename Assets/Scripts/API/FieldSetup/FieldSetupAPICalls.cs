﻿using Sportfaction.CasualFantasy.Services;

using BestHTTP;
using UnityEngine;
using UnityEngine.Assertions;

namespace Sportfaction.CasualFantasy.FieldSetup
{
	/// <summary>
	/// Description: 
	/// Author: Antoine de Lachèze-Murel
	/// </summary>
	public sealed class FieldSetupAPICalls : MonoBehaviour
	{
        #region Fields

        [SerializeField, Tooltip("FieldSetupMain")]
        private FieldSetupMain _fieldSetupMain = null;

        [SerializeField]

        private string _getListFieldSetup = "";

        #endregion

        #region Public Methods

        /// <summary>
        /// Get List Field Setup
        /// </summary>
        public void GetListFieldSetup()
		{
			if (null != Api.Instance)
			{
                _getListFieldSetup = Api.Instance.GenerateUrl(ApiConstants.GetListFieldSetup);

				Assert.IsFalse(string.IsNullOrEmpty(_getListFieldSetup), "[FieldSetupAPICalls] GetListFieldSetup(), lUrl is null or empty.");

                HTTPRequest lRequest = Api.Instance.SetUpRequest(_getListFieldSetup, Api.Methods.GET, true);

                StartCoroutine(Api.Instance.SendRequest(lRequest));
            }
		}

        #endregion

        #region Private Methods


        /// <summary>
        /// MonoBehaviour method used to link OnRequestSuccess to API
        /// </summary>
        private void OnEnable()
        {
            if (null != Api.Instance)
            {
                Api.Instance.OnSuccess += OnRequestSuccess;
                Api.Instance.OnBadRequest += OnBadRequestEvent;
            }
        }

        /// <summary>
        /// MonoBehaviour method used to unlink OnRequestSuccess from API
        /// </summary>
        private void OnDisable()
        {
            if (null != Api.Instance)
            {
                Api.Instance.OnSuccess -= OnRequestSuccess;
                Api.Instance.OnBadRequest -= OnBadRequestEvent;
            }
        }

        /// <summary>
        /// Send a request to API
        /// </summary>
        /// <param name="pUrl"></param>
        /// <param name="pMethod"></param>
        /// <param name="pAuthenticationRequired"></param>
        private void SendRequest(string pUrl, Api.Methods pMethod, bool pAuthenticationRequired)
		{
			HTTPRequest lRequest = Api.Instance.SetUpRequest(pUrl, pMethod, pAuthenticationRequired);

			Assert.IsNotNull(lRequest, "[LadderAPICalls] SendRequest(), lRequest is null.");

			StartCoroutine(Api.Instance.SendRequest(lRequest));
		}



        /// <summary>
        /// Callback invoke when request from API was done successfully
        /// </summary>
        /// <param name="pCurrentUrl">string</param>
        /// <param name="pMethod">HTTPMethods</param>
        /// <param name="pData">string</param>
        private void OnRequestSuccess(string pCurrentUrl, HTTPMethods pMethod, string pData)
        {
            if (true == pCurrentUrl.Equals(_getListFieldSetup))
            {
                Assert.IsNotNull(_fieldSetupMain, "[CardAPICalls] OnRequestSuccess(), _cardMain is null or empty.");

                _fieldSetupMain.OnGetListFieldSetup(pData);
            }
        }

        /// <summary>
		/// Callback invoke when request from API was done successfully
		/// </summary>
		/// <param name="pCurrentUrl">Url used</param>
		/// <param name="pData">Data received</param>
        private void OnBadRequestEvent(string pCurrentUrl, string pData)
        {
			#if UNITY_EDITOR && DEVELOPMENT_BUILD
		        Debug.LogFormat("<color=red>[CardAPICalls]</color> OnBadRequestEvent(): {0}", pData);
			#endif
        }

		#endregion
    }
}
﻿using System.Collections.Generic;
using Sportfaction.CasualFantasy.Events;
using Sportfaction.CasualFantasy.References;
using SportFaction.CasualFootballEngine.ActionService.Enum;
using SportFaction.CasualFootballEngine.PlayerService.Enum;
using SportFaction.CasualFootballEngine.TeamService.Enum;
using UnityEngine;

namespace Sportfaction.CasualFantasy.FieldSetup
{
    public class FieldSetupMain : MonoBehaviour
    {

        public static FieldSetupMain Instance;

        public List<FieldSetupDirection> FieldSetupDirectionList = new List<FieldSetupDirection>();

        #region Fields

        [Header("GameObjects")]

        [SerializeField, Tooltip("FieldSetupAPICalls")]
        private FieldSetupAPICalls _fieldSetupAPICalls = null;

        [Header("Data")]

        [SerializeField, Tooltip("FieldSetup Json Ref")]
        private StringReference _fieldSetupJSONRef = null;


        [Header("GameEvent")]

        [SerializeField]
        private GameEvent _onGetListFieldSetup = null;


        #endregion

        #region API Calls

        public void GetListFieldSetup() { _fieldSetupAPICalls.GetListFieldSetup(); }


        #endregion

        #region Public Methods
        /// <summary>
        /// Handle json
        /// </summary>
        /// <param name="pData">Json to handle</param>
        public void OnGetListFieldSetup(string pData)
        {
            _fieldSetupJSONRef.CurrentValue = pData;
            _onGetListFieldSetup.Invoke();
        }

        /// <summary>
        /// Get Player Direction
        /// </summary>
        /// <param name="pTeam">Team A \ Team B</param>
        /// <param name="pFs">Field Setup Name</param>
        /// <param name="pPosition">Position</param>
        public eMove GetPlayerDirection(eTeam pTeam, string pFs, ePosition pPosition)
        {
            if(FieldSetupDirectionList.Count == 0)
            {
                InitFieldSetupDirection();
            }
  
            foreach (FieldSetupDirection direction in FieldSetupDirectionList)
            {
                if (direction.Name == pFs && direction.Team == pTeam)
                {
                    foreach (FieldSetupDirectionPosition item in direction.Positions)
                    {
                        if (item.Position == pPosition)
                        {
                            return item.Direction;
                        }
                    }
                }
            }

            return eMove.NONE;
        }



        public void InitFieldSetupDirection()
        {
            FieldSetupDirectionList = new List<FieldSetupDirection>();

            FieldSetupDirection lFSD = new FieldSetupDirection();
            lFSD.Name = "FS_A_PRE_TOSS";
            lFSD.Team = eTeam.TEAM_A;

            FieldSetupDirectionPosition lPosition = new FieldSetupDirectionPosition();
            lPosition.Position = ePosition.G;
            lPosition.Direction = eMove.TOP_RIGHT;
            lFSD.Positions.Add(lPosition);

            lPosition = new FieldSetupDirectionPosition();
            lPosition.Position = ePosition.DCG;
            lPosition.Direction = eMove.DOWN;
            lFSD.Positions.Add(lPosition);

            lPosition = new FieldSetupDirectionPosition();
            lPosition.Position = ePosition.DCD;
            lPosition.Direction = eMove.TOP;
            lFSD.Positions.Add(lPosition);

            lPosition = new FieldSetupDirectionPosition();
            lPosition.Position = ePosition.DG;
            lPosition.Direction = eMove.DOWN_RIGHT;
            lFSD.Positions.Add(lPosition);

            lPosition = new FieldSetupDirectionPosition();
            lPosition.Position = ePosition.DD;
            lPosition.Direction = eMove.TOP_RIGHT;
            lFSD.Positions.Add(lPosition);

            lPosition = new FieldSetupDirectionPosition();
            lPosition.Position = ePosition.MCG;
            lPosition.Direction = eMove.TOP_LEFT;
            lFSD.Positions.Add(lPosition);

            lPosition = new FieldSetupDirectionPosition();
            lPosition.Position = ePosition.MCD;
            lPosition.Direction = eMove.TOP_RIGHT;
            lFSD.Positions.Add(lPosition);

            lPosition = new FieldSetupDirectionPosition();
            lPosition.Position = ePosition.MG;
            lPosition.Direction = eMove.DOWN_RIGHT;
            lFSD.Positions.Add(lPosition);

            lPosition = new FieldSetupDirectionPosition();
            lPosition.Position = ePosition.MD;
            lPosition.Direction = eMove.TOP_RIGHT;
            lFSD.Positions.Add(lPosition);

            lPosition = new FieldSetupDirectionPosition();
            lPosition.Position = ePosition.ACG;
            lPosition.Direction = eMove.TOP_LEFT;
            lFSD.Positions.Add(lPosition);

            lPosition = new FieldSetupDirectionPosition();
            lPosition.Position = ePosition.ACD;
            lPosition.Direction = eMove.DOWN_RIGHT;
            lFSD.Positions.Add(lPosition);

            FieldSetupDirectionList.Add(lFSD);




            lFSD = new FieldSetupDirection();
            lFSD.Name = "FS_A_PRE_TOSS";
            lFSD.Team = eTeam.TEAM_B;

            lPosition = new FieldSetupDirectionPosition();
            lPosition.Position = ePosition.G;
            lPosition.Direction = eMove.RIGHT;
            lFSD.Positions.Add(lPosition);

            lPosition = new FieldSetupDirectionPosition();
            lPosition.Position = ePosition.DCG;
            lPosition.Direction = eMove.TOP_LEFT;
            lFSD.Positions.Add(lPosition);

            lPosition = new FieldSetupDirectionPosition();
            lPosition.Position = ePosition.DCD;
            lPosition.Direction = eMove.DOWN_RIGHT;
            lFSD.Positions.Add(lPosition);

            lPosition = new FieldSetupDirectionPosition();
            lPosition.Position = ePosition.DG;
            lPosition.Direction = eMove.TOP_LEFT;
            lFSD.Positions.Add(lPosition);

            lPosition = new FieldSetupDirectionPosition();
            lPosition.Position = ePosition.DD;
            lPosition.Direction = eMove.DOWN;
            lFSD.Positions.Add(lPosition);

            lPosition = new FieldSetupDirectionPosition();
            lPosition.Position = ePosition.MCG;
            lPosition.Direction = eMove.LEFT;
            lFSD.Positions.Add(lPosition);

            lPosition = new FieldSetupDirectionPosition();
            lPosition.Position = ePosition.MCD;
            lPosition.Direction = eMove.TOP_LEFT;
            lFSD.Positions.Add(lPosition);

            lPosition = new FieldSetupDirectionPosition();
            lPosition.Position = ePosition.MG;
            lPosition.Direction = eMove.DOWN_RIGHT;
            lFSD.Positions.Add(lPosition);

            lPosition = new FieldSetupDirectionPosition();
            lPosition.Position = ePosition.MD;
            lPosition.Direction = eMove.RIGHT;
            lFSD.Positions.Add(lPosition);

            lPosition = new FieldSetupDirectionPosition();
            lPosition.Position = ePosition.ACG;
            lPosition.Direction = eMove.TOP_LEFT;
            lFSD.Positions.Add(lPosition);

            lPosition = new FieldSetupDirectionPosition();
            lPosition.Position = ePosition.ACD;
            lPosition.Direction = eMove.TOP_LEFT;
            lFSD.Positions.Add(lPosition);

            FieldSetupDirectionList.Add(lFSD);


            lFSD = new FieldSetupDirection();
            lFSD.Name = "FS_A_TOSS_INIT";
            lFSD.Team = eTeam.TEAM_A;

            lPosition = new FieldSetupDirectionPosition();
            lPosition.Position = ePosition.G;
            lPosition.Direction = eMove.TOP_RIGHT;
            lFSD.Positions.Add(lPosition);

            lPosition = new FieldSetupDirectionPosition();
            lPosition.Position = ePosition.DCG;
            lPosition.Direction = eMove.DOWN;
            lFSD.Positions.Add(lPosition);

            lPosition = new FieldSetupDirectionPosition();
            lPosition.Position = ePosition.DCD;
            lPosition.Direction = eMove.TOP;
            lFSD.Positions.Add(lPosition);

            lPosition = new FieldSetupDirectionPosition();
            lPosition.Position = ePosition.DG;
            lPosition.Direction = eMove.DOWN_RIGHT;
            lFSD.Positions.Add(lPosition);

            lPosition = new FieldSetupDirectionPosition();
            lPosition.Position = ePosition.DD;
            lPosition.Direction = eMove.TOP_RIGHT;
            lFSD.Positions.Add(lPosition);

            lPosition = new FieldSetupDirectionPosition();
            lPosition.Position = ePosition.MCG;
            lPosition.Direction = eMove.TOP_LEFT;
            lFSD.Positions.Add(lPosition);

            lPosition = new FieldSetupDirectionPosition();
            lPosition.Position = ePosition.MCD;
            lPosition.Direction = eMove.TOP_RIGHT;
            lFSD.Positions.Add(lPosition);

            lPosition = new FieldSetupDirectionPosition();
            lPosition.Position = ePosition.MG;
            lPosition.Direction = eMove.DOWN_RIGHT;
            lFSD.Positions.Add(lPosition);

            lPosition = new FieldSetupDirectionPosition();
            lPosition.Position = ePosition.MD;
            lPosition.Direction = eMove.TOP_RIGHT;
            lFSD.Positions.Add(lPosition);

            lPosition = new FieldSetupDirectionPosition();
            lPosition.Position = ePosition.ACG;
            lPosition.Direction = eMove.TOP_LEFT;
            lFSD.Positions.Add(lPosition);

            lPosition = new FieldSetupDirectionPosition();
            lPosition.Position = ePosition.ACD;
            lPosition.Direction = eMove.TOP_RIGHT;
            lFSD.Positions.Add(lPosition);

            FieldSetupDirectionList.Add(lFSD);




            lFSD = new FieldSetupDirection();
            lFSD.Name = "FS_A_TOSS_INIT";
            lFSD.Team = eTeam.TEAM_B;

            lPosition = new FieldSetupDirectionPosition();
            lPosition.Position = ePosition.G;
            lPosition.Direction = eMove.RIGHT;
            lFSD.Positions.Add(lPosition);

            lPosition = new FieldSetupDirectionPosition();
            lPosition.Position = ePosition.DCG;
            lPosition.Direction = eMove.TOP_LEFT;
            lFSD.Positions.Add(lPosition);

            lPosition = new FieldSetupDirectionPosition();
            lPosition.Position = ePosition.DCD;
            lPosition.Direction = eMove.DOWN_RIGHT;
            lFSD.Positions.Add(lPosition);

            lPosition = new FieldSetupDirectionPosition();
            lPosition.Position = ePosition.DG;
            lPosition.Direction = eMove.TOP_LEFT;
            lFSD.Positions.Add(lPosition);

            lPosition = new FieldSetupDirectionPosition();
            lPosition.Position = ePosition.DD;
            lPosition.Direction = eMove.DOWN;
            lFSD.Positions.Add(lPosition);

            lPosition = new FieldSetupDirectionPosition();
            lPosition.Position = ePosition.MCG;
            lPosition.Direction = eMove.LEFT;
            lFSD.Positions.Add(lPosition);

            lPosition = new FieldSetupDirectionPosition();
            lPosition.Position = ePosition.MCD;
            lPosition.Direction = eMove.TOP_LEFT;
            lFSD.Positions.Add(lPosition);

            lPosition = new FieldSetupDirectionPosition();
            lPosition.Position = ePosition.MG;
            lPosition.Direction = eMove.DOWN_RIGHT;
            lFSD.Positions.Add(lPosition);

            lPosition = new FieldSetupDirectionPosition();
            lPosition.Position = ePosition.MD;
            lPosition.Direction = eMove.RIGHT;
            lFSD.Positions.Add(lPosition);

            lPosition = new FieldSetupDirectionPosition();
            lPosition.Position = ePosition.ACG;
            lPosition.Direction = eMove.TOP_LEFT;
            lFSD.Positions.Add(lPosition);

            lPosition = new FieldSetupDirectionPosition();
            lPosition.Position = ePosition.ACD;
            lPosition.Direction = eMove.TOP_LEFT;
            lFSD.Positions.Add(lPosition);

            FieldSetupDirectionList.Add(lFSD);

        }

        #endregion

        void Awake()
        {
            Instance = this;

        }

    }
}

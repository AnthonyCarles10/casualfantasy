﻿using System.Collections.Generic;
using Sportfaction.CasualFantasy.Events;

using Sportfaction.CasualFantasy.References;
using SportFaction.CasualFootballEngine.ActionService.Enum;
using SportFaction.CasualFootballEngine.PlayerService.Enum;
using SportFaction.CasualFootballEngine.TeamService.Enum;
using UnityEngine;

namespace Sportfaction.CasualFantasy.FieldSetup
{
    public class FieldSetupDirection
    {

        #region Properties

        public List<FieldSetupDirectionPosition> Positions = new List<FieldSetupDirectionPosition>();

        public eTeam Team;

        public string Name;

        #endregion




    }

    public class FieldSetupDirectionPosition
    {
        #region Properties


        public ePosition Position;

        public eMove Direction;

        #endregion

    }


}
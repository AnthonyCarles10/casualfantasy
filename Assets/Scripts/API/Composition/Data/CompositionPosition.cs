﻿using System.Collections.Generic;
using UnityEngine;

namespace Sportfaction.CasualFantasy.Composition
{
    [System.Serializable]
    public sealed class CompositionPosition
    {
        #region Properties

        public List<CompositionSquadPosition> SquadPositions { get { return _squadPosition; } }

        public string Name { get { return _name; } }

        #endregion

        #region Fields

        [SerializeField, Tooltip("_name")]
        private string _name;

        [SerializeField, Tooltip("_squadPosition")]
        private List<CompositionSquadPosition> _squadPosition;

        #endregion
    }
}

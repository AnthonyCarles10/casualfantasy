﻿using System.Collections.Generic;
using UnityEngine;

namespace Sportfaction.CasualFantasy.Composition
{
    [CreateAssetMenu(fileName = "New Composition Data", menuName = "CasualFantasy/Data/Composition")]
    public sealed class CompositionData : ScriptableObject
    {
        #region Properties

        public List<CompositionPosition> Positions { get { return _positions; } }

        #endregion

        #region Fields

        [SerializeField, Tooltip("_positions")]
        private List<CompositionPosition> _positions;

        #endregion


    }
}

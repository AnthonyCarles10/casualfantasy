﻿using UnityEngine;

namespace Sportfaction.CasualFantasy.Composition
{
    [System.Serializable]
    public sealed class CompositionSquadPosition
    {
        #region Properties

        public string Name { get { return _name; } }

        #endregion

        #region Fields

        [SerializeField, Tooltip("_name")]
        private string _name;

        #endregion

    }
}

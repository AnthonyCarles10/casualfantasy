﻿using Sportfaction.CasualFantasy.Events;
using Sportfaction.CasualFantasy.References;

using UnityEngine;

namespace Sportfaction.CasualFantasy.Field
{
    public class FieldMain : MonoBehaviour
    {
        #region Fields

        [Header("GameObjects")]

        [SerializeField, Tooltip("FieldAPICalls")]
        private FieldAPICalls _fieldAPICalls = null;

        [Header("Data")]

        [SerializeField, Tooltip("FieldZones Json Ref")]
        private StringReference _fieldZonesJSONRef = null;


        [Header("GameEvent")]

        [SerializeField]
        private GameEvent _onGetFieldZones = null;



        #endregion

        #region API Calls

        public void GetFieldZones() { _fieldAPICalls.GetFieldZones(); }


        #endregion

        #region Public Methods
        /// <summary>
        /// Handle json
        /// </summary>
        /// <param name="pData">Json to handle</param>
        public void OnGetListFieldSetup(string pData)
        {
            _fieldZonesJSONRef.CurrentValue = pData;
            _onGetFieldZones.Invoke();
        }

        #endregion

    }
}

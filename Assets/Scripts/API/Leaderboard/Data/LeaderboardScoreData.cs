using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SportFaction.CasualFootballEngine.Utilities;
using UnityEngine;

namespace Sportfaction.CasualFantasy.Leaderboard
{
    [System.Serializable]
    public sealed class LeaderboardScoreData
    {
        #region Properties

        [JsonIgnore]
        public	string	Rank			{ get { return _rank; } }

        [JsonIgnore]
        public	string	Username		{ get { return _username; } }

        [JsonIgnore]
        public	string	Pseudo		    { get { return _pseudo; } }

        [JsonIgnore]
        public	string	Score			{ get { return _score; } }

        [JsonIgnore]
        public	string	RealPoint		{ get { return _realPoint; } }

        [JsonIgnore]
        public	string	ArenaCup		{ get { return _arenaCup; } }

        [JsonIgnore]
        public	string	GamesPlayed		{ get { return _gamesPlayed; } }

        [JsonIgnore]
        public	string	GamesWon		{ get { return _gamesWon; } }

        [JsonIgnore]
        public	string	GamesLost		{ get { return _gamesLost; } }

        [JsonIgnore]
        public	string	GamesDraw		{ get { return _gamesDraw; } }

        [JsonIgnore]
        public	string	RatioGamesWon	{ get { return _ratioGamesWon; } }

        [JsonIgnore]
        public	string	GoalsScored		{ get { return _goalsScored; } }

        [JsonIgnore]
        public	string  GoalsConceded   { get { return _goalsConceded; } }

        #endregion

        #region Fields

        [SerializeField, JsonProperty("rank"), Tooltip("rank")]
        private	string	_rank			=	"0";

        [SerializeField, JsonProperty("username"), Tooltip("username")]
        private	string	_username		=	"";

        [SerializeField, JsonProperty("pseudo"), Tooltip("pseudo")]
        private string _pseudo = "";

        [SerializeField, JsonProperty("score"), Tooltip("score")]
        private	string	_score			=	"0";

        [SerializeField, JsonProperty("real_point"), Tooltip("realPoint")]
        private string _realPoint = "0";

        [SerializeField, JsonProperty("arena_cup"), Tooltip("arenaCup")]
        private string _arenaCup = "0";

        [SerializeField, JsonProperty("nb_games_played"), Tooltip("nb games played")]
		private	string	_gamesPlayed	=	"0";

        [SerializeField, JsonProperty("nb_games_won"), Tooltip("nb games won")]
		private	string	_gamesWon		=	"0";

        [SerializeField, JsonProperty("nb_games_lost"), Tooltip("nb games lost")]
		private	string	_gamesLost		=	"0";

        [SerializeField, JsonProperty("nb_games_draw"), Tooltip("nb games draw")]
		private	string	_gamesDraw		=	"0";

        [SerializeField, JsonProperty("nb_games_won_ratio"), Tooltip("ratio games won")]
		private	string	_ratioGamesWon	=	"0";

        [SerializeField, JsonProperty("nb_goals_scored"), Tooltip("nb goals scored")]
		private	string	_goalsScored	=	"0";

        [SerializeField, JsonProperty("nb_goals_conceded"), Tooltip("nb goals conceded")]
        private string _goalsConceded = "0";

        #endregion

        #region Public Methods

        /// <summary>
        /// Initialize LadderScoreData from a JsonToken
        /// </summary>
        /// <param name="pToken">Specific JsonToken to use</param>
        public void Init(JToken pToken)
        {
            _rank           = pToken.Value<string>("rank");

            _username		=	pToken.SafeValue<string>("username");

            _pseudo		    =	pToken.SafeValue<string>("pseudo");

            _score			=	pToken.SafeValue<string>("score");

            _realPoint	    =	pToken.SafeValue<string>("real_point");

            _arenaCup       =	pToken.SafeValue<string>("arena_cup");

			_gamesPlayed	=	pToken.SafeValue<string>("nb_games_played");

			_gamesWon		=	pToken.SafeValue<string>("nb_games_won");

			_ratioGamesWon	=	pToken.SafeValue<string>("nb_games_won_ratio");

			_gamesLost		=	pToken.SafeValue<string>("nb_games_lost");

			_gamesDraw		=	pToken.SafeValue<string>("nb_games_draw");

			_goalsScored	=	pToken.SafeValue<string>("nb_goals_scored");

			_goalsConceded	=	pToken.SafeValue<string>("nb_goals_conceded");
        }

        #endregion


    }
}

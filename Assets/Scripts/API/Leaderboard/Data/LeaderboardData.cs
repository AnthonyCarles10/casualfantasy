using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

namespace Sportfaction.CasualFantasy.Leaderboard
{
    /// <summary>
    /// Description: Class contains all data Ladder
    /// Author: Antoine de Lachèze-Murel
    /// </summary>
	[CreateAssetMenu(fileName = "Store-Leaderboard", menuName = "CasualFantasy/Data/Store/Leaderboard")]
    public sealed class LeaderboardData : ScriptableObject
    {
        #region Properties

        [JsonIgnore]
        public List<LeaderboardScoreData> LeaderboardScoreData { get { return _leaderboardScoreData; } }

        [JsonIgnore]
        public int Limit { get { return _limit; } }

        [JsonIgnore]
        public int ManagerRank { get { return _managerRank; } }

        [JsonIgnore]
        public int CurrentNumber { get { return _currentNumber; } }

        #endregion

        #region Fields

        [SerializeField, JsonProperty("scores"), Tooltip("Scores")]
        private List<LeaderboardScoreData> _leaderboardScoreData = new List<LeaderboardScoreData>();

        [SerializeField, JsonProperty("limit"), Tooltip("Limit")]
        private int _limit = 0;

        [SerializeField, JsonProperty("rank"), Tooltip("ManagerRank")]
        private int _managerRank = 0;

        [SerializeField, JsonProperty("currentNumber"), Tooltip("CurrentNumber")]
        private int _currentNumber = 0;

        #endregion

        #region Public Methods

        /// <summary>
        /// Initialize Ladder data from a Json
        /// </summary>
        /// <param name="pJson">Specific json to use</param>
        public void Parse(string pJson, int pCurrentNumber = -1)
        {
    

            Assert.IsFalse(string.IsNullOrEmpty(pJson), "LadderData: InitializeLadderData(), pJson is null.");

            JToken lJToken = JToken.Parse(pJson);

            JArray lScoreDataArray = lJToken.Value<JArray>("scores");

            if (pCurrentNumber == -1)
            {
                _currentNumber = lJToken.Value<int>("currentNumber");
            }
            else
            {
                _currentNumber = pCurrentNumber;
            }

            foreach (JToken lscore in lScoreDataArray)
            {
                LeaderboardScoreData lNewLadderScoreData = new LeaderboardScoreData();
                lNewLadderScoreData.Init(lscore);

                _leaderboardScoreData.Add(lNewLadderScoreData);
            }

            _limit = lJToken.Value<int>("limit");

            _managerRank = lJToken.Value<int>("rank");
        }

        #endregion
    }
}
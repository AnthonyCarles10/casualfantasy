using Sportfaction.CasualFantasy.Manager.Profile;

using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;

namespace Sportfaction.CasualFantasy.Leaderboard
{
    /// <summary>
    /// Description: LeaderboardRowPrefab
    /// Author: Antoine de Lachèze-Murel
    /// </summary>
    public class LeaderboardRowPrefab : MonoBehaviour
    {
        #region Fields
        [SerializeField, Tooltip("Highlight")]
        private GameObject _highlight;

        [SerializeField, Tooltip("Rank")]
        private Text _rank;

        [SerializeField, Tooltip("Username")]
        private Text _username;

        [SerializeField, Tooltip("Games played")]
        private Text _gamesPlayed;

        [SerializeField, Tooltip("Games lost")]
        private Text _gamesLost;

        [SerializeField, Tooltip("Games draw")]
        private Text _gamesDraw;

        [SerializeField, Tooltip("Games won")]
        private Text _gamesWon;

        [SerializeField, Tooltip("Ratio games won")]
        private Text _ratioGamesWon;

        [SerializeField, Tooltip("Goals scored")]
        private Text _goalsScored;

        [SerializeField, Tooltip("Score")]
        private Text _score;

        #endregion

        #region Public Methods
        
        public	void	InitLadderScoreData(LeaderboardScoreData pData)
        {
            _rank.text			=	pData.Rank;
            _username.text		=	pData.Username;
			_gamesPlayed.text	=	pData.GamesPlayed;
			_gamesWon.text		=	pData.GamesWon;
			_gamesDraw.text		=	pData.GamesDraw;
			_gamesLost.text		=	pData.GamesLost;
			_ratioGamesWon.text	=	pData.RatioGamesWon;
			_goalsScored.text	=	pData.GoalsScored;
            _score.text			=	pData.Score;

			Assert.IsNotNull(UserData.Instance, "[LeaderboardRowPrefab] InitLadderScoreData(), UserData.Instance is null.");

			Assert.IsNotNull(UserData.Instance.Profile, "[LeaderboardRowPrefab] InitLadderScoreData(), UserData.Instance.Profile is null.");

            if (UserData.Instance.Profile.Rank.ToString() == pData.Rank)
			{
                _highlight.SetActive(true);
            }
            else
            {
                _highlight.SetActive(false);
            }
        }
        
        #endregion
    }

}

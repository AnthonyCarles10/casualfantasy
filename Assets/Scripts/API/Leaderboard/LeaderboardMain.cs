using Newtonsoft.Json;
using Sportfaction.CasualFantasy.Events;
using Sportfaction.CasualFantasy.Manager.Profile;
using Sportfaction.CasualFantasy.Meta;
using UnityEngine;
using UnityEngine.Assertions;

namespace Sportfaction.CasualFantasy.Leaderboard
{
    public class LeaderboardMain : MonoBehaviour
    {

        #region Variables

        [Header("API")]
        [SerializeField]
        private LeaderboardAPICalls _leaderboardAPICalls;

        [Header("DATA")]
        [SerializeField]
        public LeaderboardData LeaderboardData;

        [SerializeField]
        public LeaderboardData LeaderboardArenaCupData;

        [SerializeField]
        public LeaderboardData LeaderboardPreviousArenaCupData = null;

        [SerializeField]
        public int PreviousNumberArenaCup = -1;

        [SerializeField]
        public int CurrentNumberArenaCup = -1;

        #endregion

        #region Events

        [Header("Game Events")]
        [SerializeField]
        private GameEvent _onGetOverallRanking;

        [SerializeField]
        private GameEvent _onGetArenaCupRanking;

        [SerializeField]
        private GameEvent _onGetPreviousArenaCupRanking;

        #endregion

        #region API Calls

        public void GetOverallRankingByTrophy(string pLimit, string pOffset) { _leaderboardAPICalls.GetOverallRankingByTrophy(pLimit, pOffset); }
        public void GetOverallRankingByRP(string pLimit, string pOffset) { _leaderboardAPICalls.GetOverallRankingByRP(pLimit, pOffset); }
        public void GetCurrentNumberArenaCup() { _leaderboardAPICalls.GetCurrentNumberArenaCup(); }
        public void GetPreviousArenaCupRanking(string pLimit, string pOffset, string pNumber) { _leaderboardAPICalls.GetPreviousArenaCupRanking(pLimit, pOffset, pNumber); }
        public void GetArenaCupRanking(string pLimit, string pOffset, string pNumber) { _leaderboardAPICalls.GetArenaCupRanking(pLimit, pOffset, pNumber); }
		public void APIAddEvents() { _leaderboardAPICalls.AddEvents(); }

		#endregion

		#region Public Methods
		/// <summary>
		/// Parse Json
		/// </summary>
		/// <param name="pData">Json to parse</param>
		public void ParseOverallTrophyRanking(string pData)
        {
            LeaderboardData = ScriptableObject.CreateInstance<LeaderboardData>();
            LeaderboardData.Parse(pData);

            PlayerPrefs.SetString("leaderboard", JsonConvert.SerializeObject(LeaderboardData));
            PlayerPrefs.Save();

            if (null != _onGetOverallRanking)
			{
				_onGetOverallRanking.Invoke();
			}
        }

        /// <summary>
        /// Parse Json
        /// </summary>
        /// <param name="pData">Json to parse</param>
        //public void ParseCurrentNumberArenaCup(string pData)
        //{
        //    CurrentNumberArenaCup = int.Parse(pData);
        //    PreviousNumberArenaCup = CurrentNumberArenaCup - 1;


        //    Debug.Log("ADLM > ParseCurrentNumberArenaCup = " + UserData.Instance.Profile.CurrentArenaCup + " / CurrentNumberArenaCup =" + CurrentNumberArenaCup);

        //    if (UserData.Instance.Profile.CurrentArenaCup < CurrentNumberArenaCup && UserData.Instance.Profile.CurrentArenaCup != -1 && UserData.Instance.Profile.NeedToOpenReward == false)
        //    {
        //        Debug.Log("ADLM > Profile = " + UserData.Instance.Profile.CurrentArenaCup + " / CurrentNumberArenaCup =" + CurrentNumberArenaCup);
        //        UserData.Instance.Profile.CurrentArenaCup = CurrentNumberArenaCup;
        //        UserData.Instance.Profile.NeedToOpenReward = true;
        //        UserData.Instance.PersistUserData();
        //    }
        //    else if(UserData.Instance.Profile.CurrentArenaCup == -1 && UserData.Instance.Profile.NeedToOpenReward == false)
        //    {
        //        Debug.Log("ADLM elseif > Profile = " + UserData.Instance.Profile.CurrentArenaCup + " / CurrentNumberArenaCup =" + CurrentNumberArenaCup);
        //        UserData.Instance.Profile.CurrentArenaCup = CurrentNumberArenaCup;
        //        UserData.Instance.PersistUserData();
        //    }

        //    GetArenaCupRanking("8", "0", CurrentNumberArenaCup.ToString());
        //    GetPreviousArenaCupRanking("8", "0", PreviousNumberArenaCup.ToString());

        //}

  //      /// <summary>
		///// Parse Json
		///// </summary>
		///// <param name="pData">Json to parse</param>
		//public void ParseArenaCupRanking(string pData)
  //      {
  //          LeaderboardArenaCupData = ScriptableObject.CreateInstance<LeaderboardData>();
  //          LeaderboardArenaCupData.Parse(pData, CurrentNumberArenaCup);

  //          PlayerPrefs.SetString("leaderboardArenaCup", JsonConvert.SerializeObject(LeaderboardArenaCupData));
  //          PlayerPrefs.Save();

  //          if (null != _onGetArenaCupRanking)
  //          {
  //              _onGetArenaCupRanking.Invoke();
  //          }

  //      }

  //      /// <summary>
  //      /// Parse Json
  //      /// </summary>
  //      /// <param name="pData">Json to parse</param>
  //      public void ParsePreviousArenaCupRanking(string pData)
  //      {
  //          LeaderboardPreviousArenaCupData = ScriptableObject.CreateInstance<LeaderboardData>();
  //          LeaderboardPreviousArenaCupData.Parse(pData, PreviousNumberArenaCup);

  //          PlayerPrefs.SetString("leaderboardPreviousArenaCup", JsonConvert.SerializeObject(LeaderboardPreviousArenaCupData));
  //          PlayerPrefs.Save();

  //          if (null != _onGetPreviousArenaCupRanking)
  //          {
  //              _onGetPreviousArenaCupRanking.Invoke();
  //          }

  //      }

        public void LoadData()
        {
            if (PlayerPrefs.HasKey("leaderboard"))
            {
                string lJson = PlayerPrefs.GetString("leaderboard");

                LeaderboardData = ScriptableObject.CreateInstance<LeaderboardData>();
                LeaderboardData.Parse(lJson);

                _onGetOverallRanking.Invoke();
            }

            //if (PlayerPrefs.HasKey("leaderboardArenaCup"))
            //{
            //    string lJson = PlayerPrefs.GetString("leaderboardArenaCup");

            //    LeaderboardArenaCupData = ScriptableObject.CreateInstance<LeaderboardData>();
            //    LeaderboardArenaCupData.Parse(lJson);

            //    CurrentNumberArenaCup = LeaderboardArenaCupData.CurrentNumber;

            //    _onGetArenaCupRanking.Invoke();
            //}

            //if (PlayerPrefs.HasKey("leaderboardPreviousArenaCup"))
            //{
            //    string lJson = PlayerPrefs.GetString("leaderboardPreviousArenaCup");


            //    Debug.Log(lJson);

            //    LeaderboardPreviousArenaCupData = ScriptableObject.CreateInstance<LeaderboardData>();
            //    LeaderboardPreviousArenaCupData.Parse(lJson);

            //    PreviousNumberArenaCup = LeaderboardPreviousArenaCupData.CurrentNumber;
            //    Debug.Log("PreviousNumberArenaCup = " + PreviousNumberArenaCup);

            //    _onGetPreviousArenaCupRanking.Invoke();
            //}
        }


        #endregion

        private void Awake()
        {
            LoadData();
        }
    }
}

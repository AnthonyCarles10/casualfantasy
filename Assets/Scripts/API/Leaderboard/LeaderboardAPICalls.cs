using Sportfaction.CasualFantasy.Services;

using BestHTTP;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using System;

namespace Sportfaction.CasualFantasy.Leaderboard
{
	/// <summary>
	/// Description: 
	/// Author: Antoine de Lach�ze-Murel
	/// </summary>
	public sealed class LeaderboardAPICalls : MonoBehaviour
	{
        #region Fields

        [SerializeField, Tooltip("LadderMain")]
        private LeaderboardMain _leaderboardMain = null;

        private string _getOverallRankingURL = "";
        private string _getArenaCupRanking = "";
        private string _getPreviousArenaCupRanking = "";
        private string _getCurrentNumberArenaCupURL = "";

        #endregion

        #region Public Methods

        /// <summary>
        /// Synchronize overall rankink of the user by calling API
        /// </summary>
        public void GetOverallRankingByTrophy(string pLimit, string pOffset)
		{
			if (null != Api.Instance)
			{
                Dictionary<string, string> lQueryParams = new Dictionary<string, string>()
                {
                    { ApiConstants.GetOverallRankingLimitKey, pLimit },
                    { ApiConstants.GetOverallRankingOffsetKey, pOffset }
                };

                _getOverallRankingURL = Api.Instance.GenerateUrl(ApiConstants.GetOverallRankingByTrophy, lQueryParams);

                Assert.IsFalse(string.IsNullOrEmpty(_getOverallRankingURL), "[LeaderBoardAPICalls] getOverallRanking(), lUrl is null or empty.");

                HTTPRequest lRequest = Api.Instance.SetUpRequest(_getOverallRankingURL, Api.Methods.GET, true);
                lRequest.Timeout = TimeSpan.FromSeconds(5);

                StartCoroutine(Api.Instance.SendRequest(lRequest));
            }
		}

        /// <summary>
        /// Synchronize overall rankink of the user by calling API
        /// </summary>
        public void GetOverallRankingByRP(string pLimit, string pOffset)
        {
            if (null != Api.Instance)
            {
                Dictionary<string, string> lQueryParams = new Dictionary<string, string>()
                {
                    { ApiConstants.GetOverallRankingLimitKey, pLimit },
                    { ApiConstants.GetOverallRankingOffsetKey, pOffset }
                };

                _getOverallRankingURL = Api.Instance.GenerateUrl(ApiConstants.GetOverallRankingByRP, lQueryParams);

                Assert.IsFalse(string.IsNullOrEmpty(_getOverallRankingURL), "[LeaderBoardAPICalls] getOverallRanking(), lUrl is null or empty.");

                HTTPRequest lRequest = Api.Instance.SetUpRequest(_getOverallRankingURL, Api.Methods.GET, true);
                lRequest.Timeout = TimeSpan.FromSeconds(5);

                StartCoroutine(Api.Instance.SendRequest(lRequest));
            }
        }

        


        /// <summary>
        /// Synchronize overall rankink of the user by calling API
        /// </summary>
        public void GetCurrentNumberArenaCup()
        {
            if (null != Api.Instance)
            {
                _getCurrentNumberArenaCupURL = Api.Instance.GenerateUrl(ApiConstants.GetCurrentNumberArenaCup);

                Assert.IsFalse(string.IsNullOrEmpty(_getCurrentNumberArenaCupURL), "[LeaderBoardAPICalls] getOverallRanking(), lUrl is null or empty.");

                HTTPRequest lRequest = Api.Instance.SetUpRequest(_getCurrentNumberArenaCupURL, Api.Methods.GET, true);
                lRequest.Timeout = TimeSpan.FromSeconds(5);

                StartCoroutine(Api.Instance.SendRequest(lRequest));
            }
        }

        /// <summary>
        /// Synchronize overall rankink of the user by calling API
        /// </summary>
        public void GetArenaCupRanking(string pLimit, string pOffset, string pNumber)
        {
            if (null != Api.Instance)
            {
                Dictionary<string, string> lQueryParams = new Dictionary<string, string>()
                {
                    { ApiConstants.GetOverallRankingLimitKey, pLimit },
                    { ApiConstants.GetOverallRankingOffsetKey, pOffset },
                    { "number", pNumber }
                };

                _getArenaCupRanking = Api.Instance.GenerateUrl(ApiConstants.GetOverallRankingByArenaCup, lQueryParams);

                Assert.IsFalse(string.IsNullOrEmpty(_getArenaCupRanking), "[LeaderBoardAPICalls] GetArenaCupRanking(), lUrl is null or empty.");

                HTTPRequest lRequest = Api.Instance.SetUpRequest(_getArenaCupRanking, Api.Methods.GET, true);
                lRequest.Timeout = TimeSpan.FromSeconds(5);

                StartCoroutine(Api.Instance.SendRequest(lRequest));
            }
        }
        
        /// <summary>
        /// Synchronize overall rankink of the user by calling API
        /// </summary>
        public void GetPreviousArenaCupRanking(string pLimit, string pOffset, string pNumber)
        {
            if (null != Api.Instance)
            {
                Dictionary<string, string> lQueryParams = new Dictionary<string, string>()
                {
                    { ApiConstants.GetOverallRankingLimitKey, pLimit },
                    { ApiConstants.GetOverallRankingOffsetKey, pOffset },
                    { "number",pNumber.ToString() }
                };

                _getPreviousArenaCupRanking = Api.Instance.GenerateUrl(ApiConstants.GetOverallRankingByArenaCup, lQueryParams);

                Assert.IsFalse(string.IsNullOrEmpty(_getPreviousArenaCupRanking), "[LeaderBoardAPICalls] GetPreviousArenaCupRanking(), lUrl is null or empty.");

                HTTPRequest lRequest = Api.Instance.SetUpRequest(_getPreviousArenaCupRanking, Api.Methods.GET, true);
                lRequest.Timeout = TimeSpan.FromSeconds(5);

                StartCoroutine(Api.Instance.SendRequest(lRequest));
            }
        }

        public void AddEvents()
		{
			if (null != Api.Instance)
			{
				Api.Instance.OnSuccess += OnRequestSuccess;
			}
		}

		#endregion

		#region Private Methods

		/// <summary>
		/// MonoBehaviour method used to link OnRequestSuccess to API
		/// </summary>
		private void OnEnable()
        {
            if (null != Api.Instance)
            {
                Api.Instance.OnSuccess += OnRequestSuccess;
            }
        }

        /// <summary>
        /// MonoBehaviour method used to unlink OnRequestSuccess from API
        /// </summary>
        private void OnDisable()
        {
            if (null != Api.Instance)
            {
                Api.Instance.OnSuccess -= OnRequestSuccess;
            }
        }

        /// <summary>
        /// Send a request to API
        /// </summary>
        /// <param name="pUrl"></param>
        /// <param name="pMethod"></param>
        /// <param name="pAuthenticationRequired"></param>
        private void SendRequest(string pUrl, Api.Methods pMethod, bool pAuthenticationRequired)
		{
			HTTPRequest lRequest = Api.Instance.SetUpRequest(pUrl, pMethod, pAuthenticationRequired);

			Assert.IsNotNull(lRequest, "[LeaderBoardAPICalls] SendRequest(), lRequest is null.");

			StartCoroutine(Api.Instance.SendRequest(lRequest));
		}

        /// <summary>
        /// Callback invoke when request from API was done successfully
        /// </summary>
        /// <param name="pCurrentUrl">string</param>
        /// <param name="pMethod">HTTPMethods</param>
        /// <param name="pData">string</param>
        private void OnRequestSuccess(string pCurrentUrl, HTTPMethods pMethod, string pData)
        {
            if (true == pCurrentUrl.Equals(_getOverallRankingURL))
            {
                Assert.IsNotNull(_leaderboardMain, "[LeaderBoardAPICalls] OnRequestSuccess(), _ladderMain is null or empty.");
                _leaderboardMain.ParseOverallTrophyRanking(pData);
            }
            //else if (true == pCurrentUrl.Equals(_getCurrentNumberArenaCupURL))
            //{
            //    Assert.IsNotNull(_leaderboardMain, "[LeaderBoardAPICalls] OnRequestSuccess(), _ladderMain is null or empty.");
            //    _leaderboardMain.ParseCurrentNumberArenaCup(pData);
            //}
            //else if (true == pCurrentUrl.Equals(_getArenaCupRanking))
            //{
            //    Assert.IsNotNull(_leaderboardMain, "[LeaderBoardAPICalls] OnRequestSuccess(), _ladderMain is null or empty.");
            //    _leaderboardMain.ParseArenaCupRanking(pData);
            //}
            //else if (true == pCurrentUrl.Equals(_getPreviousArenaCupRanking))
            //{
            //    Assert.IsNotNull(_leaderboardMain, "[LeaderBoardAPICalls] OnRequestSuccess(), _ladderMain is null or empty.");
            //    _leaderboardMain.ParsePreviousArenaCupRanking(pData);
            //}
        }

        #endregion
    }
}
using Sportfaction.CasualFantasy.Services;

using BestHTTP;
using UnityEngine;
using UnityEngine.Assertions;
using System.Collections.Generic;
using Sportfaction.CasualFantasy.Manager.Profile;
using Sportfaction.CasualFantasy.PVP.Gameplay.Matchs;
using Newtonsoft.Json;
using System;

namespace Sportfaction.CasualFantasy.PVPMatch
{
	/// <summary>
	/// Description: 
	/// Author: Antoine de Lach�ze-Murel
	/// </summary>
	public sealed class MatchAPICalls : MonoBehaviour
	{
        #region Fields

        [SerializeField, Tooltip("MatchMain")]
        private MatchMain _matchMain;

        [SerializeField]

        private string _sendMatchResultURL;

        #endregion

        #region Public Methods

        /// <summary>
        /// SendMatchResult
        /// </summary>
        public void SendMatchResult()
        {
            if (null != Api.Instance)
            {

                Dictionary<string, object> game = new Dictionary<string, object>
                {
                    {"member1_id", UserData.Instance.Profile.Id},
                    {"member2_id", null},
                    {"score1", MatchEngine.Instance.Engine.ScoreMain.TeamAScore},
                    {"score2", MatchEngine.Instance.Engine.ScoreMain.TeamBScore},
                    {"member1_bet", 0},
                    {"member2_bet", 0}
                };


                Dictionary<string, object> body = new Dictionary<string, object>
                {
                    { "game" , game }
                };


                _sendMatchResultURL = Api.Instance.GenerateUrl(ApiConstants.PostPvpgames);

                Assert.IsFalse(string.IsNullOrEmpty(_sendMatchResultURL), "[UserAPICalls] LoginDefault(), lUrl is null or empty.");

                HTTPRequest lRequest = Api.Instance.SetUpRequest(_sendMatchResultURL, true, body);
                lRequest.Timeout = TimeSpan.FromSeconds(5);

                StartCoroutine(Api.Instance.SendRequest(lRequest));
            }
        }

        public void AddEvents()
        {
            if (null != Api.Instance)
            {
                Api.Instance.OnSuccess += OnRequestSuccess;
            }
        }

        #endregion

        #region Private Methods


        /// <summary>
        /// MonoBehaviour method used to link OnRequestSuccess to API
        /// </summary>
        private void OnEnable()
        {
            if (null != Api.Instance)
            {
                Api.Instance.OnSuccess += OnRequestSuccess;
                Api.Instance.OnBadRequest += OnBadRequestEvent;
            }
        }

        /// <summary>
        /// MonoBehaviour method used to unlink OnRequestSuccess from API
        /// </summary>
        private void OnDisable()
        {
            if (null != Api.Instance)
            {
                Api.Instance.OnSuccess -= OnRequestSuccess;
                Api.Instance.OnBadRequest -= OnBadRequestEvent;
            }
        }

        /// <summary>
        /// Send a request to API
        /// </summary>
        /// <param name="pUrl"></param>
        /// <param name="pMethod"></param>
        /// <param name="pAuthenticationRequired"></param>
        private void SendRequest(string pUrl, Api.Methods pMethod, bool pAuthenticationRequired)
		{
			HTTPRequest lRequest = Api.Instance.SetUpRequest(pUrl, pMethod, pAuthenticationRequired);

			Assert.IsNotNull(lRequest, "[MatchAPICalls] SendRequest(), lRequest is null.");
            lRequest.Timeout = TimeSpan.FromSeconds(5);

            StartCoroutine(Api.Instance.SendRequest(lRequest));
		}

   



        /// <summary>
        /// Callback invoke when request from API was done successfully
        /// </summary>
        /// <param name="pCurrentUrl">string</param>
        /// <param name="pMethod">HTTPMethods</param>
        /// <param name="pData">string</param>
        private void OnRequestSuccess(string pCurrentUrl, HTTPMethods pMethod, string pData)
        {
            if (true == pCurrentUrl.Equals(_sendMatchResultURL))
            {
                Assert.IsNotNull(_matchMain, "[MatchAPICalls] OnRequestSuccess(), _matchMain is null or empty.");

                _matchMain.MatchResultSent(pData);
            }
          
        }

        /// <summary>
		/// Callback invoke when request from API was done successfully
		/// </summary>
		/// <param name="pCurrentUrl">Url used</param>
		/// <param name="pData">Data received</param>
        private void OnBadRequestEvent(string pCurrentUrl, string pData)
        {
            if (true == pCurrentUrl.Equals(_sendMatchResultURL))
            {
                #if DEVELOPMENT_BUILD
                    Debug.LogFormat("<color=red>[MatchAPICalls]</color> OnBadRequestEvent(): {0}", pData);
                #endif
            }
        }

		#endregion
    }
}
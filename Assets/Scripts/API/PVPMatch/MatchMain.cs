﻿using System;
using System.Collections.Generic;
using Sportfaction.CasualFantasy.Events;
using Sportfaction.CasualFantasy.PVP.Gameplay.Matchs;
using Sportfaction.CasualFantasy.Services.Timers;
using SportFaction.CasualFootballEngine.MatchService.Model;
using SportFaction.CasualFootballEngine.Utilities;
using UnityEngine;
using UnityEngine.Assertions;

namespace Sportfaction.CasualFantasy.PVPMatch
{
    public class MatchMain : MonoBehaviour
    {

        #region Fields

        [Header("Links")]

        [SerializeField, Tooltip("MatchAPICalls")]
        private MatchAPICalls _matchAPICalls = null;

        [Header("GameEvent")]

        [SerializeField]
        private GameEvent _onMatchResultSent = null;

        #endregion

        #region API Calls

        public void SendMatchResult() { _matchAPICalls.SendMatchResult(); }

        #endregion

        #region Public Methods
        /// <summary>
        /// Handle json and create opposition
        /// </summary>
        /// <param name="pJson">Json to handle</param>
        public void MatchResultSent(string pJson)
        {

            MatchEngine.Instance.Engine.MatchMain.EndMatchInfos = new EndMatchInfos(GZipCompressor.Compress(pJson));

            _onMatchResultSent.Invoke();
        }


        #endregion

    }
}

﻿using BestHTTP;
using UnityEngine;
using UnityEngine.UI;

namespace Sportfaction.CasualFantasy
{
    public class InsertCodeController : MonoBehaviour
    {
        #region Variables

        public InputField WriteCode; // Code to insert for associating a new device
   
        #endregion

        public void OnEnable()
        {
            if (Api.Instance != null)
            {
                Api.Instance.OnSuccess += OnRequestSuccess;
            }
        }


        /// <summary>
        /// Send code to associate a new device
        /// </summary>
        public void AssociateDevice()
        {
            object body = new
            {
                code = WriteCode.text,
                deviceIdToAssociate = SystemInfo.deviceUniqueIdentifier
            };

            HTTPRequest lAssociateDevice = Api.Instance.SetUpRequest(
                Api.Instance.GenerateUrl("users/associate_device"),
                false,
                body
            );

            StartCoroutine(Api.Instance.SendRequest(lAssociateDevice));
        }

        /// <summary>
        /// Callback invoke when request from API was done successfully
        /// </summary>
        /// <param name="pCurrentUrl">string</param>
        /// <param name="pMethod">HTTPMethods</param>
        /// <param name="pData">string</param>
        private void OnRequestSuccess(string pCurrentUrl, HTTPMethods pMethod, string pData)
        {

           // Debug.Log("<< AssociationSuccess >> = " + pData);
           // SceneManager.LoadScene("Menu");
        }
    }
}

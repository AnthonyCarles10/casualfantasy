using Sportfaction.CasualFantasy.Services;

using BestHTTP;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using System;

namespace Sportfaction.CasualFantasy.Manager.Profile
{
	/// <summary>
	/// Description: 
	/// Author: Antoine de Lac��ze-Murel
	/// </summary>
	public sealed class UserAPICalls : MonoBehaviour
	{
        #region Fields
        
        [SerializeField, Tooltip("UserMain")]
        private UserMain    _userMain   =   null;

        private string  _loginDefaultURL    =   "";
        private string  _connectURL         =   "";
        private string  _patchUser          =   "";
        private string  _getProfileURL      =   "";
        private string  _getStatsProfileURL =   "";
        private string _getNextRealGame     =   "";
        private string _getLastRealGame     =   "";
        private string _resetUserTeam =   "";
        private string _resetUserLoots =   "";

        private Dictionary<string, object>  _patchUserBody;
        
        //public InputField GeneratedCode;                 // Code generated to associate a new device
        //public Text timeRemainingGeneratedCode;          // Time remaining to insert on a new device the generated code
        // public PVP.Network.PUNManager _pun;

        //private Dictionary<string, string> _queryParams; // query params for call api
        //private bool _isGeneratedCode;                   // if the code is generated, display time remaining before expiration
        //private AskAssociativeCode _askAssociativeCode;  // Class AskAssociativeCode (Model)

        #endregion

        #region Public Methods

        /// <summary>
        /// Listens to when the GameServices successfully
        /// to get profile
        /// </summary>
        /// <param name="pPlayerId">Player Id</param>
        public void Connect(string pPlayerId)
        {
            if(null != Api.Instance)
            {
                _connectURL = Api.Instance.GenerateUrl(ApiConstants.GetProfile);

                Assert.IsFalse(string.IsNullOrEmpty(_connectURL), "[UserAPICalls] GetProfile(), lUrl is null or empty.");

                #if UNITY_EDITOR
                object body = new
                {
                    game_player_id = ApiConstants.editorGamePlayerId,
                    device_id = ApiConstants.editorDeviceId,
                    language = Application.systemLanguage.ToString()
                };
                #else
                object body = new {
                    gamePlayerId = pPlayerId,
                    device_id = SystemInfo.deviceUniqueIdentifier,
                    language = Application.systemLanguage.ToString()
                };
                #endif

                HTTPRequest lRequest = Api.Instance.SetUpRequest(_connectURL, false, body);

                lRequest.Timeout = TimeSpan.FromSeconds(5);

                StartCoroutine(Api.Instance.SendRequest(lRequest));
            }
            
        }

        /// <summary>
        /// Listens to when the Facebook Connect successfully
        /// to get profile
        /// </summary>
        /// <param name="body">Object</param>
        public void Connect(object body)
        {
            if (null != Api.Instance)
            {
                _connectURL = Api.Instance.GenerateUrl(ApiConstants.GetProfile);

                Assert.IsFalse(string.IsNullOrEmpty(_connectURL), "[UserAPICalls] GetProfile(), lUrl is null or empty.");

                HTTPRequest lRequest = Api.Instance.SetUpRequest(_connectURL, false, body);

                lRequest.Timeout = TimeSpan.FromSeconds(5);

                StartCoroutine(Api.Instance.SendRequest(lRequest));
            }
        }

        /// <summary>
        /// Connect User by username + password
        /// </summary>
        /// <param name="body">Dictionary</param>
        public void LoginDefault(Dictionary<string, string> body)
        {
            if (null != Api.Instance)
            {
                _loginDefaultURL = Api.Instance.GenerateUrl(ApiConstants.LoginDefault);

                Debug.Log(_loginDefaultURL);
                Assert.IsFalse(string.IsNullOrEmpty(_loginDefaultURL), "[UserAPICalls] LoginDefault(), lUrl is null or empty.");

                HTTPRequest lRequest = Api.Instance.SetUpRequest(_loginDefaultURL, false, body);

                lRequest.Timeout = TimeSpan.FromSeconds(5);

                StartCoroutine(Api.Instance.SendRequest(lRequest));
            }
        }

        public void GetProfile()
        {
            if (null != Api.Instance)
            {
                _getProfileURL = Api.Instance.GenerateUrl(ApiConstants.GetProfile);

                Assert.IsFalse(string.IsNullOrEmpty(_getProfileURL), "[UserAPICalls] GetProfile(), lUrl is null or empty.");

                HTTPRequest lRequest = Api.Instance.SetUpRequest(_getProfileURL, Api.Methods.GET, true);

                lRequest.Timeout = TimeSpan.FromSeconds(5);

                StartCoroutine(Api.Instance.SendRequest(lRequest));
            }
        }

        /*// <summary>
        /// Check if is a new User
        /// </summary>
        public void isUserExists()
        {
            if (null != Api.Instance)
            {
                string lUrl = Api.Instance.GenerateUrl(ApiConstants.GetProfile);

                Assert.IsFalse(string.IsNullOrEmpty(lUrl), "[UserAPICalls] isUserExists(), lUrl is null or empty.");

                #if UNITY_EDITOR
                object body = new
                {
                    device_id = ApiConstants.editorDeviceId
                };
                #else
                object body = new
                {
                    device_id = SystemInfo.deviceUniqueIdentifier
                };
                #endif

                HTTPRequest lIsUserExists = Api.Instance.SetUpRequest(lUrl, false, body);

                StartCoroutine(Api.Instance.SendRequest(lIsUserExists, 0));
            }
        }*/

        /// <summary>
        /// Get Stats Manager
        /// </summary>
        /// <param name="pUserId">String</param>
        public void GetStatsProfile(string pUserId)
        {
            if (null != Api.Instance)
            {
                _getStatsProfileURL = Api.Instance.GenerateUrl(ApiConstants.GetStatsProfile + "/" + pUserId);

                Assert.IsFalse(string.IsNullOrEmpty(_getStatsProfileURL), "[UserAPICalls] GetProfile(), lUrl is null or empty.");

                HTTPRequest lRequest = Api.Instance.SetUpRequest(_getStatsProfileURL, Api.Methods.GET, false);

                lRequest.Timeout = TimeSpan.FromSeconds(5);

                StartCoroutine(Api.Instance.SendRequest(lRequest));
            }
        }

        /// <summary>
        /// Change Language for the first Time
        /// </summary>
        /// <param name="body">Object</param>
        public void PatchUser(Dictionary<string, object> body)
        {
            if (null != Api.Instance)
            {
                _patchUserBody = body;

                _patchUser = Api.Instance.GenerateUrl(ApiConstants.PatchProfile);

                Assert.IsFalse(string.IsNullOrEmpty(_patchUser), "[UserAPICalls] PatchProfile(), lUrl is null or empty.");

                HTTPRequest lRequest = Api.Instance.SetUpRequest(_patchUser, true, body, HTTPMethods.Patch);

                lRequest.Timeout = TimeSpan.FromSeconds(5);

                StartCoroutine(Api.Instance.SendRequest(lRequest));
            }
        }


        /// <summary>
        /// Reset Team
        /// </summary>
        /// <param name="body">Object</param>
        public void ResetUserTeam()
        {
            if (null != Api.Instance)
            {
                
                object body = new
                {
                    user_id = UserData.Instance.Profile.Id
                };

                _resetUserTeam = Api.Instance.GenerateUrl(ApiConstants.ResetTeam);

                Assert.IsFalse(string.IsNullOrEmpty(_resetUserTeam), "[UserAPICalls] ResetUserTeam(), lUrl is null or empty.");

                HTTPRequest lRequest = Api.Instance.SetUpRequest(_resetUserTeam, true, body);

                lRequest.Timeout = TimeSpan.FromSeconds(5);

                StartCoroutine(Api.Instance.SendRequest(lRequest));
            }
        }

        /// <summary>
        /// Reset User Loots
        /// </summary>
        /// <param name="body">Object</param>
        public void ResetUserLoots()
        {
            if (null != Api.Instance)
            {

                object body = new
                {
                    user_id = UserData.Instance.Profile.Id
                };

                _resetUserLoots = Api.Instance.GenerateUrl(ApiConstants.ResetLoots);

                Assert.IsFalse(string.IsNullOrEmpty(_resetUserLoots), "[UserAPICalls] ResetUserLoots(), lUrl is null or empty.");

                HTTPRequest lRequest = Api.Instance.SetUpRequest(_resetUserLoots, true, body);

                lRequest.Timeout = TimeSpan.FromSeconds(5);

                StartCoroutine(Api.Instance.SendRequest(lRequest));
            }
        }

        /*public void UpdateTeamCountry()
        {
            if (null != Api.Instance)
            {
                object body = new
                {
                    team_country_id = ""
                };

                string lUrl = Api.Instance.GenerateUrl(ApiConstants.GetProfile);

                Assert.IsFalse(string.IsNullOrEmpty(lUrl), "[UserAPICalls] GetProfile(), lUrl is null or empty.");

                HTTPRequest lGetProfile = Api.Instance.SetUpRequest(lUrl, true, body, HTTPMethods.Patch);

                StartCoroutine(Api.Instance.SendRequest(lGetProfile, 0));
            }
        }*/

        /// <summary>
        /// Get code to associate a new device
        /// </summary>
        public void AskAssociativeCode()
        {
            string lUrl = Api.Instance.GenerateUrl(ApiConstants.AskAssociativeCode);

            Assert.IsFalse(string.IsNullOrEmpty(lUrl), "[UserAPICalls] AskAssociativeCode(), lUrl is null or empty.");

            HTTPRequest lRequest = Api.Instance.SetUpRequest(lUrl, Api.Methods.GET, true);

            lRequest.Timeout = TimeSpan.FromSeconds(5);

            StartCoroutine(Api.Instance.SendRequest(lRequest));
        }

        public void GetLastRealGame()
        {
            if (null != Api.Instance)
            {
                _getLastRealGame = Api.Instance.GenerateUrl(ApiConstants.GetProfile);

                Assert.IsFalse(string.IsNullOrEmpty(_getProfileURL), "[UserAPICalls] GetProfile(), lUrl is null or empty.");

                HTTPRequest lRequest = Api.Instance.SetUpRequest(_getLastRealGame, Api.Methods.GET, true);

                lRequest.Timeout = TimeSpan.FromSeconds(5);

                StartCoroutine(Api.Instance.SendRequest(lRequest));
            }
        }

        public void GetNextRealGame()
        {
            if (null != Api.Instance)
            {
                _getNextRealGame = Api.Instance.GenerateUrl(ApiConstants.GetProfile);

                Assert.IsFalse(string.IsNullOrEmpty(_getProfileURL), "[UserAPICalls] GetProfile(), lUrl is null or empty.");

                HTTPRequest lRequest = Api.Instance.SetUpRequest(_getLastRealGame, Api.Methods.GET, true);

                StartCoroutine(Api.Instance.SendRequest(lRequest));
            }
        }

        public void AddEvents()
        {
            if (null != Api.Instance)
            {
                Api.Instance.OnSuccess += OnRequestSuccess;
                Api.Instance.OnBadRequest += OnBadRequestEvent;
            }
        }

		#endregion

		#region Private Methods

        /// <summary>
        /// MonoBehaviour method used to link OnRequestSuccess to API
        /// </summary>
        private void OnEnable()
        {
            AddEvents();
        }

        /// <summary>
        /// MonoBehaviour method used to unlink OnRequestSuccess from API
        /// </summary>
        private void OnDisable()
        {
            if (null != Api.Instance)
            {
                Api.Instance.OnSuccess -= OnRequestSuccess;
                Api.Instance.OnBadRequest -= OnBadRequestEvent;
            }
        }

        /// <summary>
        /// Callback invoke when request from API was done successfully
        /// </summary>
        /// <param name="pCurrentUrl">string</param>
        /// <param name="pMethod">HTTPMethods</param>
        /// <param name="pData">string</param>
        private void OnRequestSuccess(string pCurrentUrl, HTTPMethods pMethod, string pData)
        {
            if (true == pCurrentUrl.Equals(_connectURL) && pMethod == HTTPMethods.Post)
            {
                _userMain.HandleJsonConnect(pData);
            }
            else if (true == pCurrentUrl.Equals(_loginDefaultURL) && pMethod == HTTPMethods.Post)
            {
                _userMain.HandleJsonConnect(pData);
            }
            else if (true == pCurrentUrl.Equals(_getProfileURL) && pMethod == HTTPMethods.Get)
            {
                _userMain.HandleJsonGetProfile(pData);
            }
            else if (true == pCurrentUrl.Equals(_patchUser) && pMethod == HTTPMethods.Patch)
            {
                _userMain.HandleSuccessPatchUser(_patchUserBody);
            }
            //else if (true == pCurrentUrl.Contains(ApiConstants.AskAssociativeCode))
            //{
            //    _userMain.HandleJsonAskAssociativeCode(pData);
            //}
            else if (true == pCurrentUrl.Equals(_getStatsProfileURL))
            {
                _userMain.HandleJsonGetStatsProfile(pData);
            }
            else if (true == pCurrentUrl.Equals(_getNextRealGame))
            {
                _userMain.ParseNextRealGame(pData);
            }
            else if (true == pCurrentUrl.Equals(_getLastRealGame))
            {
                _userMain.ParseLastRealGame(pData);
            }
        }

        /// <summary>
		/// Callback invoke when request from API was done successfully
		/// </summary>
		/// <param name="pCurrentUrl">Url used</param>
		/// <param name="pData">Data received</param>
        private void OnBadRequestEvent(string pCurrentUrl, string pData)
        {
            if (true == pCurrentUrl.Equals(_loginDefaultURL))
            {
                _userMain.BadCredentials(pData);
            }
        }




        /*// <summary>
        /// Display Generated Code time remaining every seconds
        /// </summary>
        private void UpdateTimeRemainingDisplay()
        {
            if (_isGeneratedCode)
            {
                System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
                dtDateTime = dtDateTime.AddSeconds(_askAssociativeCode.expireAt).ToLocalTime();

                TimeSpan span = dtDateTime.Subtract(DateTime.Now);
                if (span.Seconds < 0)
                {
                    _isGeneratedCode = false;
                    timeRemainingGeneratedCode.text = "-";
                }
                else
                {
                    timeRemainingGeneratedCode.text = span.Minutes.ToString() + "m " + span.Seconds.ToString() + "s";
                }
            }
        }*/

        #endregion
    }
}
﻿using Sportfaction.CasualFantasy.Config;
using Sportfaction.CasualFantasy.Core;
using Sportfaction.CasualFantasy.Events;
using Sportfaction.CasualFantasy.Menu;
using Sportfaction.CasualFantasy.Meta;
using Sportfaction.CasualFantasy.Services;
using Sportfaction.CasualFantasy.Services.Timers;
using Sportfaction.CasualFantasy.Services.Userstracking;

//using Facebook.Unity;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

namespace Sportfaction.CasualFantasy.Manager.Profile
{
    /// <summary>
    /// Description: UserMain
    /// Author: Antoine de Lachèze-Murel
    /// </summary>
    public class UserMain : MonoBehaviour
    {
        #region Fields

        [Header("All Scene")]

        [SerializeField, Tooltip("UserAPICalls")]
        private UserAPICalls _userAPICalls = null;

        [SerializeField, Tooltip("CurrentScene")]
        private string _currentScene = null;

        [Header("Core Scene")]

        [SerializeField, Tooltip("CoreMain (optional)")]
        private CoreMain _coreMain = null;

        [Header("Menu Scene")]

        [SerializeField, Tooltip("ManagerUI")]
        private ManagerUI _managerUI = null;


        [SerializeField, Tooltip("Soft")]
        public TMPro.TMP_Text Soft = null;

        [Header("OnBoarding")]

        [SerializeField, Tooltip("Username Text")]
        private TMPro.TMP_InputField _username = null;


        [SerializeField, Tooltip("Username Error")]
        public GameObject Error = null;

        [SerializeField, Tooltip("Username Error")]
        public TMPro.TMP_Text ErrorTxt = null;

        [Header("PostSplashscren")]

        [SerializeField]
        private GameEvent _onConnect = null;

        [SerializeField]
        private GameEvent _onGetProfile = null;

        [SerializeField]
        private GameEvent _onFirstConnect = null;

        [SerializeField]
        private GameEvent _onChangeUsername = null;

        [SerializeField]
        private GameEvent _onPatchUser = null;

        [SerializeField]
        private GameEvent _onBadCredentials = null;

        #endregion

        #region Public Methods

        #region API Calls

        public void Connect(string pPlayerId) { _userAPICalls.Connect(pPlayerId); }

        public void Connect(object body) { _userAPICalls.Connect(body); }

        public void LoginDefault(Dictionary<string, string> body) { _userAPICalls.LoginDefault(body); }

        public void GetProfile() { _userAPICalls.GetProfile(); }

        public void PatchUser(Dictionary<string, object> pBody) { _userAPICalls.PatchUser(pBody); }

        public void GetLastRealGame() { _userAPICalls.GetLastRealGame(); }

        public void GetNextRealGame() { _userAPICalls.GetNextRealGame(); }

        public void APIAddEvents() { _userAPICalls.AddEvents(); }

        public void ResetUserTeam() { _userAPICalls.ResetUserTeam(); }

        public void ResetUserLoots() { _userAPICalls.ResetUserLoots(); }

        #endregion

        /// <summary>
        /// Handle json and create opposition
        /// </summary>
        /// <param name="pData">Json to handle</param>
        public void HandleJsonConnect(string pData)
        {
            Assert.IsNotNull(UserData.Instance, "[UserMain]: HandleJsonUser(), UserData.Instance is null.");

            UserData.Instance.ParseFromAPI(pData);

            _onConnect.Invoke();

            if (null != ConfigData.Instance && _currentScene != "Menu" && !MetaData.Instance.CoreMain._isUpdateAvailable)
            {
                ConfigData.Instance.AddTrackingUserProperty("userId", UserData.Instance.Profile.Id.ToString());
                ConfigData.Instance.AddTrackingUserProperty("username", UserData.Instance.Profile.Username);
                ConfigData.Instance.AddTrackingUserProperty("pseudo", UserData.Instance.Profile.Pseudo);
                ConfigData.Instance.AddTrackingUserProperty("soft", UserData.Instance.Profile.Soft);
                ConfigData.Instance.AddTrackingUserProperty("xp", UserData.Instance.Profile.Xp);
                ConfigData.Instance.AddTrackingUserProperty("nbRealPurchase", UserData.Instance.Profile.NbRealPurchase.ToString());
                ConfigData.Instance.AddTrackingUserProperty("isVip", UserData.Instance.Profile.IsVip.ToString());
                //ConfigData.Instance.AddTrackingParam("language", Application.systemLanguage.ToString());

                UserEventsManager.Instance.SetUserPropertiesFirebase();


                if(!string.IsNullOrEmpty(UserData.Instance.Profile.SessionLastDate))
                {
                    // Retention  D1, D3, D7, D14, D30
                    DateTime epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

                    TimeSpan lFirstSessionTimeSpan = TimeSpan.FromSeconds(double.Parse(UserData.Instance.Profile.SessionLastDate));
                    DateTime StartDateTime = epoch.Add(lFirstSessionTimeSpan).ToLocalTime();

                    DateTime lNowDateTime = DateTime.Now;

                    TimeSpan lRetention = lNowDateTime - StartDateTime;

                    if (lRetention.Days > 0 && lRetention.Days > UserData.Instance.Profile.Retention)
                    {
                
                        UserData.Instance.Profile.Retention = lRetention.Days;

                        if (UserData.Instance.Profile.Retention <= 30)
                        {
                            UserEventsManager.Instance.RecordEvent(new UserActionEvent("D" + UserData.Instance.Profile.Retention));
                        }

                    }
                }


                if (false == UserData.Instance.IsDeviceIdExists)
                {

                    // UserData.Instance.Profile.OnBoarding = true;

					if (null != _onFirstConnect)
					{
						_onFirstConnect.Invoke();
					}

                    UserEventsManager.Instance.RecordEvent(new UserActionEvent("first_connect"));
                    UserEventsManager.Instance.RecordEvent(new UserActionEvent("D0"));
                    UserData.Instance.Profile.Retention = 0;
                    UserData.Instance.Profile.OnBoardingMeta = true;
                }
                else
                {
                    UserEventsManager.Instance.RecordEvent(new UserActionEvent("user_visit"));

                }

                UserData.Instance.Profile.SessionLastDate = (DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds.ToString();
                UserData.Instance.PersistUserData();
            }

            if (null != Soft)
            {
                Soft.text = UserData.Instance.Profile.Soft;
            }

            GetProfile();
        }

        public void HandleJsonGetProfile(string pJson)
        {
            Assert.IsNotNull(UserData.Instance, "[UserMain]: HandleJsonUser(), UserData.Instance is null.");

            pJson = "{\"user\":" + pJson + "}";

            UserData.Instance.ParseFromAPI(pJson);

            _onGetProfile.Invoke();
        }

        /// <summary>
        /// Handle json
        /// </summary>
        /// <param name="pData">Json to handle</param>
        public void HandleJsonAskAssociativeCode(string pData)
        {
            // Assert.IsNotNull(_user, "[UserMain]: HandleJsonAskAssociativeCode(), _user is null.");

            //_askAssociativeCode = JsonConvert.DeserializeObject<AskAssociativeCode>(pData);

            //_isGeneratedCode = true;
            //GeneratedCode.text = _askAssociativeCode.code;
            //InvokeRepeating("UpdateTimeRemainingDisplay", 0f, 1f);

        }

        /// <summary>
        /// Handle json
        /// </summary>
        /// <param name="pData">Json to handle</param>
        public void HandleJsonGetStatsProfile(string pData)
        {
            _managerUI.Init(pData);

        }

        public void SetOnBoardingCompleted()
        {
            Dictionary<string, object> body = new Dictionary<string, object>
            {
                { "tutorial_completed", true }
            };

            DateTime today = DateTime.Now;
            DateTime After3Days = today.AddDays(3);
            var epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            var unixDateTime = (After3Days.ToUniversalTime() - epoch).TotalSeconds;
            LocalTimersManager.Instance.StartTimer("starter_1", unixDateTime.ToString());

            PatchUser(body);
        }

        public void ChangeLanguage()
        {
            Dictionary<string, object> body = new Dictionary<string, object>
            {
                { "language", Application.systemLanguage.ToString()}
            };

            PatchUser(body);
        }

        public void ChangeUsername()
        {
            Assert.IsNotNull(_username, "[UserMain]: ChangeUsername(), _username is null.");
            Assert.IsNotNull(Error, "[UserMain]: ChangeUsername(), Error is null.");

            if(String.IsNullOrEmpty(_username.text)) {
                Error.SetActive(true);
                ErrorTxt.text = I2.Loc.LocalizationManager.GetTranslation("UsernameEmpty");
            } else {
            
                Dictionary<string, object> body = new Dictionary<string, object>
                {
                    { "username", _username.text }
                };


                PatchUser(body);

                ErrorTxt.text = I2.Loc.LocalizationManager.GetTranslation("UsernameError");
                Error.SetActive(false);
            }
        }

        public void HandleSuccessPatchUser(Dictionary<string, object> pbody)
        {
            bool _isUsernameChanged = false;
            foreach (var param in pbody)
            {
                if("username" == param.Key)
                {
                    _isUsernameChanged = true;
                    #if UNITY_EDITOR && !DEVELOPMENT_BUILD
                        object body = new
                        {
                            device_id = ApiConstants.editorDeviceId
                        };
                        Connect(body);
                    #elif UNITY_EDITOR && DEVELOPMENT_BUILD
                        Dictionary<string, string> body = new Dictionary<string, string>
                        {
                            { "username", param.Value.ToString() },
                            { "password", UserData.Instance.Profile.Password }
                        };
                        LoginDefault(body);
                    #else
                        object body = new
                        {
                            device_id = SystemInfo.deviceUniqueIdentifier
                        };
                        Connect(body);
                    #endif

                    _onChangeUsername.Invoke();
                }
            }

            if(false == _isUsernameChanged)
            {
                GetProfile();
            }

        }

        public void ParseLastRealGame(string pData)
        {

        }

        public void ParseNextRealGame(string pData)
        {

        }

        public void BadCredentials(string pData)
        {
            _onBadCredentials.Invoke();
        }

        #endregion

    }

}

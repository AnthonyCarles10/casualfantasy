using SportFaction.CasualFootballEngine.TeamService.Enum;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using UnityEngine;

namespace Sportfaction.CasualFantasy.Manager.Profile
{
	/// <summary>
	/// Description: 
	/// Author: Antoine
	/// </summary>
    public sealed class UserData : Singleton<UserData>
    {
        #region Properties

        public string           StatusCode              { get { return _statusCode; } }

        public string           Team                    { get { return _team; } set { _team = value; } }

        public bool             RedirectToCodeScreen    { get { return _redirectToCodeScreen; } }

        public bool             IsDeviceIdExists         { get { return _isDeviceIdExists; } set { _isDeviceIdExists = value; } }

		// Property used to get/set the reconnecting user's info
		public	bool			Reconnecting			{ get { return _reconnecting; } set { _reconnecting = value; } }

        public string           PvpRoomName             { get { return _pvpRoomName; } set { _pvpRoomName = value; }}

        public string           AccessToken { get { return _accessToken; } set { _accessToken = value; } }

        public string           RefreshToken { get { return _refreshToken; } set { _refreshToken = value; } }

        public int              RefreshTokenExpiredAt { get { return _refreshTokenExpiredAt; } }

        public UserProfileData  Profile                 { get { return _profile; } }

		// Property used to get the team of the player during the current match
		public	eTeam			PVPTeam					{ get { return _pvpTeam; } set { _pvpTeam = value; } }

        public string           Redirect = "";

        #endregion

        #region Fields

        // String key used to load/save pvp room name
        public	static	readonly	string		PVPRoomNameKey	=	"pvp_room_name";

        [SerializeField, Tooltip("_statusCode")]
        private string          _statusCode;

        [SerializeField, Tooltip("_team")]
        private string _team;

        [SerializeField, Tooltip("_redirectToCodeScreen")]
        private bool            _redirectToCodeScreen;

        [SerializeField, Tooltip("_isDeviceIdExists")]
        private bool            _isDeviceIdExists;

		[SerializeField, Tooltip("Player trying to reconnect to pvp game")]
		private	bool			_reconnecting	=	false;
        [SerializeField, Tooltip("Name of the pvp room")]
        private string          _pvpRoomName	=	"";

        [SerializeField, Tooltip("_accessToken")]
        private string _accessToken;

        [SerializeField, Tooltip("_refreshToken")]
        private string _refreshToken;

        [SerializeField, Tooltip("_refreshTokenExpiredAt")]
        private int _refreshTokenExpiredAt;

        [SerializeField, Tooltip("_profile")]
        private UserProfileData _profile;

		[SerializeField, ReadOnly, Tooltip("Team of the player during the current match")]
		private	eTeam			_pvpTeam	=	eTeam.TEAM_A;

        #endregion

        #region Public Methods

        /// <summary>
        /// Initialize Data with json response from Api
        /// </summary>
        /// <param name="pJson">json response</param>
        public void ParseFromAPI(string pJson)
        {
            JToken lJToken = JToken.Parse(pJson);

            if(pJson.Contains("status_code"))
            {
                _statusCode = lJToken.Value<string>("status_code");

                _redirectToCodeScreen = lJToken.Value<bool>("redirect_to_code_screen");

                _isDeviceIdExists = lJToken.Value<bool>("is_device_id_exists");

                _accessToken = lJToken.Value<string>("access_token");

                _refreshToken = lJToken.Value<string>("refresh_token");

                _refreshTokenExpiredAt = lJToken.Value<int>("refresh_token_expired_at");
            }

            _profile.ParseFromAPI(lJToken.Value<JToken>("user"));

            PersistUserData();
        }


        /// <summary>
        /// Persist UserData to PlayersPref
        /// </summary>
        public void PersistUserData()
        {
            PlayerPrefs.SetString("user", JsonConvert.SerializeObject(UserData.Instance.Profile));

            PlayerPrefs.Save();
        }

        /// <summary>
        /// Load UserData from PlayersPref
        /// </summary>
        public string LoadUserData()
        {
            if(PlayerPrefs.HasKey("user"))
            {
                string lJson = PlayerPrefs.GetString("user");
                lJson = "{\"user\":" + lJson + "}";

                #if UNITY_EDITOR && DEVELOPMENT_BUILD
                    Debug.LogFormat("<color=#00CED1> [UserData] Load Saved Data  </color>");
                    Debug.Log(PlayerPrefs.GetString("user"));
                #endif
                return lJson;
          
            }

            return "";
       
        }

		/// <summary>
		/// Load pvp room name from player preferences
		/// </summary>
		public	void	LoadPVPRoomName()
		{
            if (PlayerPrefs.HasKey(PVPRoomNameKey))
            {
                _pvpRoomName = PlayerPrefs.GetString(PVPRoomNameKey);
            }
		}

		/// <summary>
		/// Save pvp room name in player preferences
		/// </summary>
		public	void	SavePVPRoomName()
		{
			PlayerPrefs.SetString(PVPRoomNameKey, _pvpRoomName);
            PlayerPrefs.Save();
        }

		/// <summary>
		/// Delete pvp room name from player preferences
		/// </summary>
		public	void	DeletePVPRoomName()
		{
			PlayerPrefs.DeleteKey(PVPRoomNameKey);
		}

        #endregion

        #region Private Methods

        private void Start()
        {
            // PlayerPrefs.DeleteKey("user");
            if (PlayerPrefs.HasKey("user"))
            {
                JToken lJToken = JToken.Parse(PlayerPrefs.GetString("user"));
				if (null != lJToken)
				{
					_profile.ParseFromPlayerPrefs(lJToken);
				}

            }
        }

        #endregion
    }
}

using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

namespace Sportfaction.CasualFantasy.Manager
{
    /// <summary>
    /// Description: Class contains all Real Game Data
    /// Author: Antoine de Lachèze-Murel
    /// </summary>
	[CreateAssetMenu(fileName = "Store-RealGameData", menuName = "CasualFantasy/Data/Store/RealGameData")]
    public sealed class RealGameData : ScriptableObject
    {
        #region Properties

        public RealNextGameData RealNextGame { get { return _realNextGame; } }

        #endregion

        #region Fields

        [SerializeField, Tooltip("RealNextGame")]
        private RealNextGameData _realNextGame = new RealNextGameData();

        #endregion

        #region Public Methods

        /// <summary>
        /// Initialize Ladder data from a Json
        /// </summary>
        /// <param name="pJson">Specific json to use</param>
        public void Init(string pJson)
        {
   //         Assert.IsFalse(string.IsNullOrEmpty(pJson), "LadderData: InitializeLadderData(), pJson is null.");

   //         JToken lJToken = JToken.Parse(pJson);

   //         JArray lScoreDataArray = lJToken.Value<JArray>("scores");

			//_leaderboardScoreData.Clear();

   //         foreach (JToken lscore in lScoreDataArray)
   //         {
   //             LeaderboardScoreData lNewLadderScoreData = new LeaderboardScoreData();
   //             lNewLadderScoreData.Init(lscore);

   //             _leaderboardScoreData.Add(lNewLadderScoreData);
   //         }

        }

        #endregion
    }
}
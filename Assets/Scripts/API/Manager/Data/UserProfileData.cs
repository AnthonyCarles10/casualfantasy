using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using UnityEngine;
using UnityEngine.Assertions;

namespace Sportfaction.CasualFantasy.Manager.Profile
{
    [System.Serializable]
    public sealed class UserProfileData
    {
        #region Properties

        [JsonIgnore]
        public int      Id                          { get { return _id; } set { _id = value; } }

        [JsonIgnore]
        public string   Username                    { get { return _username; } set { _username = value; } }

        [JsonIgnore]
        public string   Pseudo                      { get { return _pseudo; } set { _pseudo = value; } }

        [JsonIgnore]
        public string   Password                    { get { return _password; } set { _password = value; } }

        [JsonIgnore]
        public string   IOSId                       { get { return _iOSId; } }

        [JsonIgnore]
        public string   GamePlayerId                { get { return _gamePlayerId; } }

        [JsonIgnore]
        public string   FacebookId                  { get { return _facebookId; } }

        [JsonIgnore]
        public string   Xp                          { get { return _xp; } } // string because by default is null

        [JsonIgnore]
        public string   Soft                        { get { return _soft; } set { _soft = value; } }

        [JsonIgnore]
        public int   Hard                       { get { return _hard; } } // string because by default is null

        [JsonIgnore]
        public int      Fragment                { get { return _fragment; } } // string because by default is null


        [JsonIgnore]
        public string FirstSessionDate              { get { return _firstSessionDate; } }

        [JsonIgnore]
        public string SessionLastDate              { get { return _sessionLastDate; } set { _sessionLastDate = value; } }


        [JsonIgnore]
        public int Retention                     { get { return _retention; } set { _retention = value; } } 


        [JsonIgnore]
        public int      NbGamesPlayed               { get { return _nbGamesPlayed; } }

        // Property used to retrieve the number of real purchase done
        [JsonIgnore]
        public int		NbRealPurchase				{ get { return _nbRealPurchase; } }

        [JsonIgnore]
        public	bool   	IsVip				{ get { return _isVip; } set { _isVip = value; } }

        [JsonIgnore]
        public	int     Level				{ get { return _level; } set { _level = value; } }

        [JsonIgnore]
        public	int     StarLevel			{ get { return _starLevel; } set { _starLevel = value; } }

        [JsonIgnore]
        public	bool    StarLevelUp			{ get { return _starLevelUp; } set { _starLevelUp = value; } }

        [JsonIgnore]
        public  int     Rank                { get { return _rank; } }

        [JsonIgnore]
        public int      Score               { get { return _score; } }

        [JsonIgnore]
        public int      MaxScore            { get { return _maxScore; } }

        [JsonIgnore]
        public int      LastArenaSeasonReward { get { return _lastArenaSeasonReward; } set { _lastArenaSeasonReward = value; } }

        [JsonIgnore]
        public	string  Language		    { get { return _language; } }

        [JsonIgnore]
        public	bool    IosIsDisplayed		{ get { return _iosIsDisplayed; } set { _iosIsDisplayed = value; } }

        [JsonIgnore]
        public  bool    ChallengesExplained { get { return _challengesExplained; } set { _challengesExplained = value; } }

        [JsonIgnore]
        public  bool    LastSceneGameplay { get { return _lastSceneGameplay; } set { _lastSceneGameplay = value; } }

        [JsonIgnore]
        public	bool    OnBoarding { get { return _onBoarding; } set { _onBoarding = value; } }


        [JsonIgnore]
        public bool OnBoardingHome { get { return _onBoardingHome; } set { _onBoardingHome = value; } }

        [JsonIgnore]
        public bool OnBoardingMeta { get { return _onBoardingMeta; } set { _onBoardingMeta = value; } }

        [JsonIgnore]
        public bool NotifMinionsPlayers { get { return _notifMinionsPlayers; } set { _notifMinionsPlayers = value; } }

        [JsonIgnore]
        public bool NotifShop { get { return _notifShop; } set { _notifShop = value; } }

        [JsonIgnore]
        public bool NotifArenaCup { get { return _notifArenaCup; } set { _notifArenaCup = value; } }

        [JsonIgnore]
        public bool NeedToOpenReward { get { return _needToOpenReward; } set { _needToOpenReward = value; } }


        [JsonIgnore]
        public int CurrentArenaCup { get { return _currentArenaCup; } set { _currentArenaCup = value; } }

        [JsonIgnore]
        public int NumberMatchPlayed { get { return _numberMatchPlayed; } set { _numberMatchPlayed = value; } }

        [JsonIgnore]
        public bool NeedToUpdateVersion { get { return _needToUpdateVersion; } set { _needToUpdateVersion = value; } }

        [JsonIgnore]
        public bool VersionUpdated { get { return _versionUpdated; } set { _versionUpdated = value; } }


        [JsonIgnore]
        public	bool    OnEmptyMinions { get { return _onEmptyMinions; } set { _onEmptyMinions = value; } }

        #endregion

        #region Fields

        [SerializeField, JsonProperty("id"), Tooltip("_id")]
        private int     _id;

        [SerializeField, JsonProperty("username"), Tooltip("_username")]
        private string  _username;

        [SerializeField, JsonProperty("pseudo"), Tooltip("_pseudo")]
        private string _pseudo;

        [SerializeField, JsonProperty("password"), Tooltip("password")]
        private string _password;

        [SerializeField, JsonProperty("facebook_id"), Tooltip("facebook_id")]
        private string  _facebookId;

        [SerializeField, JsonProperty("ios_id"), Tooltip("ios_id")]
        private string  _iOSId;

        [SerializeField, JsonProperty("game_player_id"), Tooltip("game_player_id")]
        private string  _gamePlayerId;

        [SerializeField, JsonProperty("xp"), Tooltip("xp")]
        private string  _xp;

        [SerializeField, JsonProperty("soft"), Tooltip("soft")]
        private string  _soft;

        [SerializeField, JsonProperty("hard"), Tooltip("_hard")]
        private int  _hard;

        [SerializeField, JsonProperty("fragment"), Tooltip("fragment")]
        private int _fragment;

        [SerializeField, JsonProperty("firstSessionDate")]
        private string _firstSessionDate;

        [SerializeField, JsonProperty("sessionLastDate")]
        private string _sessionLastDate;

        [SerializeField, JsonProperty("retention")]
        private int _retention = 0;

        [SerializeField, JsonProperty("nb_games_played"), Tooltip("nb_games_played")]
        private int _nbGamesPlayed;

		[SerializeField, JsonProperty("nb_buy_real"), Tooltip("Number of real purchase")]
		private	int	_nbRealPurchase	=	0;

        [SerializeField, JsonProperty("is_vip"), Tooltip("Is Vip")]
        private bool _isVip = false;

        [SerializeField, JsonProperty("level"), Tooltip("Level")]
        private int _level = 0;

        [SerializeField, JsonProperty("star_level"), Tooltip("star_level")]
        private int _starLevel = 0;

        [SerializeField, JsonProperty("star_level_up"), Tooltip("star_level_up")]
        private bool _starLevelUp = false;

        [SerializeField, JsonProperty("rank"), Tooltip("Rank")]
        private int _rank = 0;

        [SerializeField, JsonProperty("score"), Tooltip("Score")]
        private int _score = 0;

        [SerializeField, JsonProperty("max_score"), Tooltip("MaxScore")]
        private int _maxScore = 0;

        [SerializeField, JsonProperty("last_arenaseason_reward"), Tooltip("LastArenaSeasonReward")]
        private int _lastArenaSeasonReward = 0;

        [SerializeField, JsonProperty("language"), Tooltip("Language")]
        private string _language;

        [SerializeField, JsonProperty("ios_is_displayed"), Tooltip("ios_is_displayed")]
        private bool _iosIsDisplayed = false;

		[SerializeField, JsonProperty("ChallengesExplained"),Tooltip("Has seen challenges explanation")]
		private	bool	_challengesExplained	=	false;

        [SerializeField, JsonProperty("last_scene_gameplay"), Tooltip("last_scene_gameplay")]
        private bool _lastSceneGameplay = false;

        [SerializeField, JsonProperty("onBoarding"), Tooltip("Boolean used to know if is on boarding")]
        private bool      _onBoarding =   false;

        [SerializeField, JsonProperty("onBoardingHome")]
        private bool _onBoardingHome = true;

        [SerializeField, JsonProperty("onBoardingMeta")]
        private bool _onBoardingMeta = true;

        [SerializeField, JsonProperty("notifMinionsPlayers")]
        private bool _notifMinionsPlayers = false;

        [SerializeField, JsonProperty("notifShop")]
        private bool _notifShop = false;

        [SerializeField, JsonProperty("notifArenaCup")]
        private bool _notifArenaCup = false;

        [SerializeField, JsonProperty("needToOpenReward")]
        private bool _needToOpenReward = false;

        [SerializeField, JsonProperty("numberMatchPlayed")]
        private int _numberMatchPlayed = 0;

        [SerializeField, JsonProperty("currentArenaCup")]
        private int _currentArenaCup = -1;

        [SerializeField, JsonProperty("needToUpdateVersion")]
        private bool _needToUpdateVersion = false;

        [SerializeField, JsonProperty("versionUpdated")]
        private bool _versionUpdated = false;

        [SerializeField, JsonProperty("onEmptyMinions"), Tooltip("Boolean used to know if is on empty minions")]
        private bool _onEmptyMinions = false;
        

        #endregion

        #region Public Methods

        /// <summary>
        /// Initialize Data with json response from Api
        /// </summary>
        /// <param name="pToken">Specific JsonToken to use</param>
        public  void    ParseFromAPI(JToken pToken)
        {
            Assert.IsNotNull(pToken, "UserProfileData: Init(), pToken is null.");

            _id             =   pToken.Value<int>("id");

            _username       =   pToken.Value<string>("username");

            _pseudo         =   pToken.Value<string>("pseudo");

            _facebookId     =   pToken.Value<string>("facebook_id");

            _iOSId          =   pToken.Value<string>("ios_id");

            _gamePlayerId   =   pToken.Value<string>("game_player_id");

            _xp             =   pToken.Value<string>("xp");

            _soft           =   pToken.Value<string>("soft");

            _hard           =  pToken.Value<int>("hard");

            _fragment       =  pToken.Value<int>("fragment");

            _firstSessionDate =  pToken.Value<string>("date_timestamp");

            _nbGamesPlayed  =   pToken.Value<int>("nb_games_played");

            _nbRealPurchase =   pToken.Value<int>("nb_buy_real");

            _isVip          =   pToken.Value<bool>("is_vip");

            _level          =   pToken.Value<int>("level");

            _rank           =   pToken.Value<int>("rank");

            _score           =   pToken.Value<int>("score");

            _maxScore       =   pToken.Value<int>("max_score");

            _lastArenaSeasonReward =   pToken.Value<int>("last_arenaseason_reward");

            _language       =   pToken.Value<string>("language");

            _onBoarding     =   !pToken.Value<bool>("tutorial_completed");


            if(string.IsNullOrEmpty(_pseudo))
            {
                _pseudo = _username;
            }

            // PARSE FROM PLAYER PREFS
            string lJson = UserData.Instance.LoadUserData();

            if(!string.IsNullOrEmpty(lJson))
            {
                JToken lJToken = JToken.Parse(lJson);

                JToken lProfile = lJToken.Value<JToken>("user");

                ParseFromPlayerPrefs(lProfile);
            }

        }

        public void ParseFromPlayerPrefs(JToken pToken)
        {
            _onEmptyMinions = pToken.Value<bool>("onEmptyMinions");

            _onBoardingHome = pToken.Value<bool>("onBoardingHome");

            _onBoardingMeta = pToken.Value<bool>("onBoardingMeta");

            _retention = pToken.Value<int>("retention");

            _notifMinionsPlayers = pToken.Value<bool>("notifMinionsPlayers");

            _notifShop = pToken.Value<bool>("notifShop");

            _notifArenaCup = pToken.Value<bool>("notifArenaCup");

            _needToOpenReward = pToken.Value<bool>("needToOpenReward");

            _currentArenaCup = pToken.Value<int>("currentArenaCup");

            _numberMatchPlayed = pToken.Value<int>("numberMatchPlayed");

            _needToUpdateVersion = pToken.Value<bool>("needToUpdateVersion");

            _versionUpdated = pToken.Value<bool>("versionUpdated");

            _sessionLastDate = pToken.Value<string>("sessionLastDate");

            _username = pToken.Value<string>("username");

            _password = pToken.Value<string>("password");

            _starLevel = pToken.Value<int>("star_level");

            _starLevelUp = pToken.Value<bool>("star_level_up");

            _iosIsDisplayed = pToken.Value<bool>("ios_is_displayed");

            _challengesExplained = pToken.Value<bool>("challenges_explained");

            _lastSceneGameplay = pToken.Value<bool>("last_scene_gameplay");
        }


        #endregion
    }
}

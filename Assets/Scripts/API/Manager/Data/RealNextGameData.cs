using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

namespace Sportfaction.CasualFantasy.Manager
{
    /// <summary>
    /// Description: Class contains all Real Game Data
    /// Author: Antoine de Lachèze-Murel
    /// </summary>

    public sealed class RealNextGameData 
    {
        #region Properties

        //public string Rank { get { return _rank; } }

        //public string Username { get { return _username; } }

        //public string Score { get { return _score; } }

        //public string RealPoint { get { return _realPoint; } }

        //public string GamesPlayed { get { return _gamesPlayed; } }

        //public string GamesWon { get { return _gamesWon; } }

        //public string GamesLost { get { return _gamesLost; } }

        //public string GamesDraw { get { return _gamesDraw; } }

        //public string RatioGamesWon { get { return _ratioGamesWon; } }

        //public string GoalsScored { get { return _goalsScored; } }

        #endregion

        #region Fields

        //[SerializeField, Tooltip("rank")]
        //private string _rank = "0";

        //[SerializeField, Tooltip("username")]
        //private string _username = "";

        //[SerializeField, Tooltip("score")]
        //private string _score = "0";

        //[SerializeField, Tooltip("realPoint")]
        //private string _realPoint = "0";

        //[SerializeField, Tooltip("nb games played")]
        //private string _gamesPlayed = "0";

        //[SerializeField, Tooltip("nb games won")]
        //private string _gamesWon = "0";

        //[SerializeField, Tooltip("nb games lost")]
        //private string _gamesLost = "0";

        //[SerializeField, Tooltip("nb games draw")]
        //private string _gamesDraw = "0";

        //[SerializeField, Tooltip("ratio games won")]
        //private string _ratioGamesWon = "0";

        //[SerializeField, Tooltip("nb goals scored")]
        //private string _goalsScored = "0";

        #endregion

        #region Public Methods

        /// <summary>
        /// Initialize LadderScoreData from a JsonToken
        /// </summary>
        /// <param name="pToken">Specific JsonToken to use</param>
        public void Init(JToken pToken)
        {
            //_rank = string.Format("{0:#,###0}", int.Parse(pToken.Value<string>("rank")));

            //_username = pToken.SafeValue<string>("username");

            //_score = pToken.SafeValue<string>("score");

            //_realPoint = pToken.SafeValue<string>("real_point");

            //_gamesPlayed = pToken.SafeValue<string>("nb_games_played");

            //_gamesWon = pToken.SafeValue<string>("nb_games_won");

            //_ratioGamesWon = pToken.SafeValue<string>("nb_games_won_ratio");

            //_gamesLost = pToken.SafeValue<string>("nb_games_lost");

            //_gamesDraw = pToken.SafeValue<string>("nb_games_draw");

            //_goalsScored = pToken.SafeValue<string>("nb_goals_scored");
        }

        #endregion

    }
}
﻿using Sportfaction.CasualFantasy.Manager.Profile;
using Sportfaction.CasualFantasy.Services;

using BestHTTP;
using UnityEngine;
using UnityEngine.Assertions;

namespace Sportfaction.CasualFantasy.Positions
{
    /// <summary>
    /// Description: 
    /// Author: Antoine de Lachèze-Murel
    /// </summary>
    public sealed class PositionsAPICalls : MonoBehaviour
	{
        #region Fields

        [SerializeField, Tooltip("PositionsMain")]
        private PositionsMain _positionsMain = null;

        private string _getPositionsManagerURL = "";

        private string _updatePositionManagerURL = "";


        #endregion

        #region Public Methods

        /// <summary>
        /// Get List cards
        /// </summary>
        public void GetPositionsManager()
		{
			if (null != Api.Instance)
			{
                _getPositionsManagerURL = Api.Instance.GenerateUrl(ApiConstants.GetPositionsManager + "/" + UserData.Instance.Profile.Id);

				Assert.IsFalse(string.IsNullOrEmpty(_getPositionsManagerURL), "[PositionsAPICalls] GetPositionsManager(), lUrl is null or empty.");

                HTTPRequest lRequest = Api.Instance.SetUpRequest(_getPositionsManagerURL, Api.Methods.GET, true);

                StartCoroutine(Api.Instance.SendRequest(lRequest));
            }
		}

        /// <summary>
        /// Update Position Manager
        /// </summary>
        public void UpdatePositionManager(string pPosition, int pLevel)
        {
            if (null != Api.Instance)
            {
                _updatePositionManagerURL = Api.Instance.GenerateUrl(ApiConstants.UpdatePositionManager);

                Assert.IsFalse(string.IsNullOrEmpty(_updatePositionManagerURL), "[PositionsAPICalls] UpdatePositionManager(), lUrl is null or empty.");

                object body = new
                {
                    squad_position = pPosition,
                    new_level = pLevel.ToString()
                };

                HTTPRequest lRequest = Api.Instance.SetUpRequest(_updatePositionManagerURL, true, body);

                StartCoroutine(Api.Instance.SendRequest(lRequest));
            }
        }



        #endregion

        #region Private Methods


        /// <summary>
        /// MonoBehaviour method used to link OnRequestSuccess to API
        /// </summary>
        private void OnEnable()
        {
            if (null != Api.Instance)
            {
                Api.Instance.OnSuccess += OnRequestSuccess;
                Api.Instance.OnBadRequest += OnBadRequestEvent;
            }
        }

        /// <summary>
        /// MonoBehaviour method used to unlink OnRequestSuccess from API
        /// </summary>
        private void OnDisable()
        {
            if (null != Api.Instance)
            {
                Api.Instance.OnSuccess -= OnRequestSuccess;
                Api.Instance.OnBadRequest -= OnBadRequestEvent;
            }
        }

        /// <summary>
        /// Send a request to API
        /// </summary>
        /// <param name="pUrl"></param>
        /// <param name="pMethod"></param>
        /// <param name="pAuthenticationRequired"></param>
        private void SendRequest(string pUrl, Api.Methods pMethod, bool pAuthenticationRequired)
		{
			HTTPRequest lRequest = Api.Instance.SetUpRequest(pUrl, pMethod, pAuthenticationRequired);

			Assert.IsNotNull(lRequest, "[LadderAPICalls] SendRequest(), lRequest is null.");

			StartCoroutine(Api.Instance.SendRequest(lRequest));
		}



        /// <summary>
        /// Callback invoke when request from API was done successfully
        /// </summary>
        /// <param name="pCurrentUrl">string</param>
        /// <param name="pMethod">HTTPMethods</param>
        /// <param name="pData">string</param>
        private void OnRequestSuccess(string pCurrentUrl, HTTPMethods pMethod, string pData)
        {
            if (true == pCurrentUrl.Equals(_getPositionsManagerURL))
            {
                Assert.IsNotNull(_positionsMain, "[PositionsAPICalls] OnRequestSuccess(), _positionsMain is null or empty.");

                _positionsMain.OnGetPositionsManager(pData);
            }
            else if (true == pCurrentUrl.Equals(_updatePositionManagerURL))
            {
                Assert.IsNotNull(_positionsMain, "[PositionsAPICalls] OnRequestSuccess(), _positionsMain is null or empty.");

                _positionsMain.OnPositionManagerUpdated();
            }
           
        }

        /// <summary>
		/// Callback invoke when request from API was done successfully
		/// </summary>
		/// <param name="pCurrentUrl">Url used</param>
		/// <param name="pData">Data received</param>
        private void OnBadRequestEvent(string pCurrentUrl, string pData)
        {
			#if UNITY_EDITOR && DEVELOPMENT_BUILD
		        Debug.LogFormat("<color=red>[CardAPICalls]</color> OnBadRequestEvent(): {0}", pData);
			#endif
        }

		#endregion
    }
}
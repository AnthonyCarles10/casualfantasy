﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using UnityEngine;

namespace Sportfaction.CasualFantasy.Positions
{
    /// <summary>
    /// Description: PositionManager
    /// Author: Antoine de Lachèze-Murel
    /// </summary>
    [CreateAssetMenu(fileName = "Store-PositionsManager", menuName = "CasualFantasy/Data/Store/PositionsManager")]
    public sealed class PositionsManager : ScriptableObject
    {
        #region Properties

        public List<PositionsManagerItem> Items { get { return _items; } }

        #endregion

        #region Fields

        [SerializeField, Tooltip("_items")]
        private List<PositionsManagerItem> _items = new List<PositionsManagerItem>();

        #endregion

        #region Public Methods

        /// <summary>
        /// Initialize Data with json response from Api
        /// </summary>
        /// <param name="pJson">json response</param>
        public	void			Init(string pJson)
        {
            
            pJson = "{\"items\":" + pJson + "}";
            JToken lJToken = JToken.Parse(pJson);
            JArray lActionsArray = lJToken.Value<JArray>("items");

			_items.Clear();

            foreach (JToken ljToken in lActionsArray)
            {
                PositionsManagerItem lItem = new PositionsManagerItem();
                lItem.Init(ljToken);
                _items.Add(lItem);
            }

        }

        public string ConvertToJson()
        {
            return JsonConvert.SerializeObject(this);
        }

        public int GetLevelByPosition(string pPosition)
        {
            foreach (PositionsManagerItem lItem in _items)
            {
                if (lItem.Position == pPosition)
                {
                    return lItem.Level;
                }
            }

            return 1;
        }


        #endregion
    }
}

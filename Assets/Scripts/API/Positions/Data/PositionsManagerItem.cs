﻿using Newtonsoft.Json.Linq;
using UnityEngine;

namespace Sportfaction.CasualFantasy.Positions
{
    /// <summary>
    /// Description: PositionsManagerItem
    /// Author: Antoine de Lachèze-Murel
    /// </summary>
    [System.Serializable]
    public sealed class PositionsManagerItem
    {
        #region Properties

        public  string      Position             { get { return _position; } } 

        public int          Level                { get { return _level; } }

        #endregion

        #region Fields

        [SerializeField, ReadOnly]
		private	string  _position   =	"";

        [SerializeField]
        private int _level = 0;

        #endregion
    
        #region Public Methods

        public	void	Init(JToken pItem)
        {
            _position   = pItem.Value<string>("squad_position");
            _level      = pItem.Value<int>("level");

        }

   
		#endregion

	}
}
﻿using Sportfaction.CasualFantasy.Events;

using UnityEngine;

namespace Sportfaction.CasualFantasy.Positions
{
    public class PositionsMain : MonoBehaviour
    {
   

        #region Fields

        [Header("GameObjects")]

        [SerializeField, Tooltip("ActionAPICalls")]
        private PositionsAPICalls _positionsAPICalls = null;

        [Header("Data")]

        [SerializeField, Tooltip("PositionsActionData")]
        private PositionsManager _positionsManager = null;

        [Header("GameEvent")]

        [SerializeField]
        private GameEvent _onGetPositionsManager = null;

        [SerializeField]
        private GameEvent _onPositionsManagerUpdated = null;

        #endregion

        #region API Calls

        public void GetPositionsManager() { _positionsAPICalls.GetPositionsManager(); }
        public void UpdatePositionManager(string pPosition, int pLevel) { _positionsAPICalls.UpdatePositionManager(pPosition, pLevel); }

        #endregion

        #region Public Methods
        /// <summary>
        /// Handle json
        /// </summary>
        /// <param name="pData">Json to handle</param>
        public void OnGetPositionsManager(string pData)
        {
            _positionsManager.Init(pData);

            _onGetPositionsManager.Invoke();
        }

        public void OnPositionManagerUpdated()
        {
            _onPositionsManagerUpdated.Invoke();
        }
        #endregion

    }
}
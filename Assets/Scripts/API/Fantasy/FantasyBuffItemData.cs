﻿using SportFaction.CasualFootballEngine.PlayerService.Model;

using Newtonsoft.Json.Linq;

namespace Sportfaction.CasualFantasy.Fantasy
{
    [System.Serializable]
    public sealed class FantasyBuffItemData
    {
        #region Properties

        public	bool	IsPositive		{ get { return _isPositive; } }	// Property used to know if this buff is positive or negative
        
        public	string	Name			{ get { return _name; } }		// Property used to get the name of this buff

        public	int		CountGeneral	{ get { return _countGeneral; } }

        public	int		Tier			{ get { return _tier; } }

        public	string	Position		{ get { return _position; } }

        public	float[]	Effects			{ get { return _effects; } }

        #endregion

        #region Fields

        private bool _isPositive = false;           // Is Buff positive ?
        private string _name = "";              // Name of this buff
        private int _countGeneral;
        private int _tier;
        private string _position;
        private float[] _effects = new float[0];    // All buff's effects

        #endregion

        #region Public Methods

        /// <summary>
        /// Is buff valid
        /// </summary>
        /// <returns>A boolean</returns>
        public bool IsValid()
        {
            if (true == string.IsNullOrEmpty(_name))
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Initialize buff
        /// </summary>
        /// <param name="pToken">Json token used to initialize this buff</param>
        public void Init(JToken pToken)
        {
            if (null == pToken)
            {
                throw new System.Exception("[Buff] Initialize(), pToken is null.");
            }

            _isPositive = pToken.Value<bool>("is_positive");
            _name = pToken.Value<string>("name");
            _countGeneral = pToken.Value<int?>("count_general") ?? 0;
            _tier = pToken.Value<int?>("tier") ?? 0;
            _position = pToken.Value<string>("position") ?? "";

            InitializeEffect(pToken);
        }
        
        /// <summary>
        /// Specific to on-boarding
        /// </summary>
        /// <param name="pIsPositive">If set to <c>true</c> p is positive.</param>
        /// <param name="pName">P name.</param>
        /// <param name="pCountGeneral">P count general.</param>
        /// <param name="pTier">P tier.</param>
        public void InitFake(bool pIsPositive, string pName, int pCountGeneral, int pTier)
        {
			_isPositive		=	pIsPositive;
			_name			=	pName;
			_countGeneral	=	pCountGeneral;
			_tier			=	pTier;
        }
        
        public void ConvertBuff(Buff pBuff)
        {
			_isPositive	=	pBuff.IsPositive;
			_name		=	pBuff.Name;
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pToken"></param>
        private void InitializeEffect(JToken pToken)
        {
            _effects = new float[(int)eSkill.COUNT];

            if (null == _effects || _effects.Length != (int)eSkill.COUNT)
            {
                throw new System.Exception("[Buff] InitializeEffect(), _effects is invalid.");
            }

            _effects[(int)eSkill.ATTACK_HEADED] = pToken.Value<float>("attack_headed_skill");
            _effects[(int)eSkill.CLOSE_SAVE] = pToken.Value<float>("close_save_skill");
            _effects[(int)eSkill.COMING_OFF] = pToken.Value<float>("coming_off_skill");
            _effects[(int)eSkill.CONCENTRATION] = pToken.Value<float>("concentration_skill");
            _effects[(int)eSkill.CONTROL] = pToken.Value<float>("control_skill");
            _effects[(int)eSkill.CORNER] = pToken.Value<float>("corner_skill");
            _effects[(int)eSkill.DEFENSE_HEADED] = pToken.Value<float>("defense_headed_skill");
            _effects[(int)eSkill.DRIBBLE] = pToken.Value<float>("dribble_skill");
            _effects[(int)eSkill.FAR_SAVE] = pToken.Value<float>("far_save_skill");
            _effects[(int)eSkill.FINISH] = pToken.Value<float>("finish_skill");
            _effects[(int)eSkill.INTERCEPTION] = pToken.Value<float>("interception_skill");
            _effects[(int)eSkill.LONG_PASS] = pToken.Value<float>("long_pass_skill");
            _effects[(int)eSkill.PARAM] = pToken.Value<float>("param_skill");
            _effects[(int)eSkill.PASS] = pToken.Value<float>("pass_skill");
            _effects[(int)eSkill.PERCUSSION] = pToken.Value<float>("percussion_skill");
            _effects[(int)eSkill.RIGOR] = pToken.Value<float>("rigor_skill");
            _effects[(int)eSkill.SHOT] = pToken.Value<float>("shot_skill");
            _effects[(int)eSkill.STRENGTH] = pToken.Value<float>("strength_skill");
            _effects[(int)eSkill.TACKLE] = pToken.Value<float>("tackle_skill");
            _effects[(int)eSkill.VISION] = pToken.Value<float>("vision_skill");
        }

        #endregion
    }
}

﻿using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using UnityEngine;

namespace Sportfaction.CasualFantasy.Fantasy
{
    [System.Serializable]
    public sealed class FantasyBuffData
    {
        #region Properties

        public List<FantasyBuffItemData> Items { get { return _items; } }

        public List<FantasyBuffStepData> Steps { get { return _steps; } }

        #endregion

        #region Fields

        [SerializeField, Tooltip("_items")]
        private List<FantasyBuffItemData> _items = new List<FantasyBuffItemData>();

        private List<FantasyBuffStepData> _steps = new List<FantasyBuffStepData>();

        #endregion

        #region Public Methods

        /// <summary>
        /// Initialize Data with json response from Api
        /// </summary>
        /// <param name="pJson">json response</param>
        public void Init(string pJson)
        {
            
            pJson = "{\"items\":" + pJson + "}";
            JToken lJToken = JToken.Parse(pJson);

            JArray lBuffsArray = lJToken.Value<JArray>("items");

            foreach (JToken lBuff in lBuffsArray)
            {
                FantasyBuffItemData lNewBuff = new FantasyBuffItemData();
                lNewBuff.Init(lBuff);

                _items.Add(lNewBuff);
            }
        }

        public void InitSteps()
        {
            FantasyBuffStepData lBuffStep = new FantasyBuffStepData();
            lBuffStep.Init("scoredgoals", 3);
            _steps.Add(lBuffStep);

            lBuffStep = new FantasyBuffStepData();
            lBuffStep.Init("savedpenalties", 3);
            _steps.Add(lBuffStep);

            lBuffStep = new FantasyBuffStepData();
            lBuffStep.Init("saves", 3);
            _steps.Add(lBuffStep);

            lBuffStep = new FantasyBuffStepData();
            lBuffStep.Init("succesfulpasses", 3);
            _steps.Add(lBuffStep);

            lBuffStep = new FantasyBuffStepData();
            lBuffStep.Init("winnergoal", 1);
            _steps.Add(lBuffStep);

            lBuffStep = new FantasyBuffStepData();
            lBuffStep.Init("assists", 3);
            _steps.Add(lBuffStep);

            lBuffStep = new FantasyBuffStepData();
            lBuffStep.Init("succesfultackles", 3);
            _steps.Add(lBuffStep);

            lBuffStep = new FantasyBuffStepData();
            lBuffStep.Init("succesfulchallenges", 3);
            _steps.Add(lBuffStep);

            lBuffStep = new FantasyBuffStepData();
            lBuffStep.Init("succesfuldribbles", 3);
            _steps.Add(lBuffStep);

            lBuffStep = new FantasyBuffStepData();
            lBuffStep.Init("interceptions", 3);
            _steps.Add(lBuffStep);

            lBuffStep = new FantasyBuffStepData();
            lBuffStep.Init("missedpenalty", 2);
            _steps.Add(lBuffStep);

            lBuffStep = new FantasyBuffStepData();
            lBuffStep.Init("missedopportunity", 3);
            _steps.Add(lBuffStep);

            lBuffStep = new FantasyBuffStepData();
            lBuffStep.Init("errorleadstogoal", 2);
            _steps.Add(lBuffStep);

            lBuffStep = new FantasyBuffStepData();
            lBuffStep.Init("foulcommited", 3);
            _steps.Add(lBuffStep);

            lBuffStep = new FantasyBuffStepData();
            lBuffStep.Init("owngoal", 1);
            _steps.Add(lBuffStep);

            lBuffStep = new FantasyBuffStepData();
            lBuffStep.Init("penaltywon", 2);
            _steps.Add(lBuffStep);

            lBuffStep = new FantasyBuffStepData();
            lBuffStep.Init("gkconcededgoal", 3);
            _steps.Add(lBuffStep);    

            lBuffStep = new FantasyBuffStepData();
            lBuffStep.Init("foulleadstopenalty", 2);
            _steps.Add(lBuffStep);

            lBuffStep = new FantasyBuffStepData();
            lBuffStep.Init("foulleadstoinjury", 1);
            _steps.Add(lBuffStep);

            lBuffStep = new FantasyBuffStepData();
            lBuffStep.Init("cleansheet", 1);
            _steps.Add(lBuffStep);

            lBuffStep = new FantasyBuffStepData();
            lBuffStep.Init("discipline", 4);
            _steps.Add(lBuffStep);
        }

        #endregion
    }
}

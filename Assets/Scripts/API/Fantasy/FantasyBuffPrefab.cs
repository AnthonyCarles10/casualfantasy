using Sportfaction.CasualFantasy.Fantasy;

using SportFaction.CasualFootballEngine.PlayerService.Model;

using UnityEngine;
using UnityEngine.UI;

namespace Sportfaction.CasualFantasy.Menu
{
    public class FantasyBuffPrefab : MonoBehaviour
    {
        #region Fields

        [SerializeField, Tooltip("Background")]
        private	Image	_background	=	null;

        [SerializeField, Tooltip("shirt")]
        private	Image	_icon		=	null;

        public	string	Name		=	null;
        public	char	NameStep;
        public	float[]	Effects		=	new float[0];

        #endregion

        #region Public Methods
        
        public	void	Init(FantasyBuffItemData pData, bool IsBuffed = true)
        {
            Effects = pData.Effects;

            string	lTmpName	=	pData.Name.Remove(pData.Name.Length - 1, 1);
			Name				=	lTmpName.Substring(0, 1).ToUpper() + lTmpName.Substring(1, lTmpName.Length - 1);
            NameStep			=	(false == IsBuffed) ? '0' : pData.Name[pData.Name.Length - 1];


            FantasyBuffData	lFantasyBuffData	=	new FantasyBuffData();
            lFantasyBuffData.InitSteps();

            FantasyBuffStepData	lStep			=	lFantasyBuffData.Steps.Find(item => item.Name == Name.ToLower());
 
            int					lStepsNumber	=	lStep.StepCount;

            if (true == IsBuffed)
            {
                if (null != Resources.Load<Sprite>("Sprites/Fantasy/Fantasy_Icon_" + Name + "On"))
                {
                    _icon.sprite = Resources.Load<Sprite>("Sprites/Fantasy/Fantasy_Icon_" + Name + "On");
                }
                else
                {
                    _icon.sprite = Resources.Load<Sprite>("Sprites/Fantasy/Fantasy_Icon_" + pData.Name + "On");
                }
            }
            else
            {
                if (null != Resources.Load<Sprite>("Sprites/Fantasy/Fantasy_Icon_" + Name + "Off"))
                {
                    _icon.sprite = Resources.Load<Sprite>("Sprites/Fantasy/Fantasy_Icon_" + Name + "Off");
                }
                else
                {
                    _icon.sprite = Resources.Load<Sprite>("Sprites/Fantasy/Fantasy_Icon_" + pData.Name + "Off");
                }
            }

            if (true == pData.IsPositive)
            {
                _background.sprite = Resources.Load<Sprite>("Sprites/Fantasy/Fantasy_BG_Bonus" + lStepsNumber + NameStep);
            }
            else
            {
                _background.sprite = Resources.Load<Sprite>("Sprites/Fantasy/Fantasy_BG_Malus" + lStepsNumber + NameStep);
            }
        }
        
        public void InitBis(Buff pData, bool IsBuffed = true)
        {
            string	lTmpName	=	pData.Name.Remove(pData.Name.Length - 1, 1);
			Name				=	lTmpName.Substring(0, 1).ToUpper() + lTmpName.Substring(1, lTmpName.Length - 1);
            NameStep			=	(false == IsBuffed) ? '0' : pData.Name[pData.Name.Length - 1];


            FantasyBuffData	lFantasyBuffData	=	new FantasyBuffData();
            lFantasyBuffData.InitSteps();

            FantasyBuffStepData	lStep			=	lFantasyBuffData.Steps.Find(item => item.Name == Name);
            int					lStepsNumber	=	null != lStep ? lStep.StepCount : 1;

            if (true == IsBuffed)
            {
                if (null != Resources.Load<Sprite>("Sprites/Fantasy/Fantasy_Icon_" + Name + "On"))
                {
                    _icon.sprite = Resources.Load<Sprite>("Sprites/Fantasy/Fantasy_Icon_" + Name + "On");
                }
                else
                {
                    _icon.sprite = Resources.Load<Sprite>("Sprites/Fantasy/Fantasy_Icon_" + pData.Name + "On");
                }
            }
            else
            {
                if (null != Resources.Load<Sprite>("Sprites/Fantasy/Fantasy_Icon_" + Name + "Off"))
                {
                    _icon.sprite = Resources.Load<Sprite>("Sprites/Fantasy/Fantasy_Icon_" + Name + "Off");
                }
                else
                {
                    _icon.sprite = Resources.Load<Sprite>("Sprites/Fantasy/Fantasy_Icon_" + pData.Name + "Off");
                }
            }

            if (true == pData.IsPositive)
            {
                _background.sprite = Resources.Load<Sprite>("Sprites/Fantasy/Fantasy_BG_Bonus" + lStepsNumber + NameStep);
            }
            else
            {
                _background.sprite = Resources.Load<Sprite>("Sprites/Fantasy/Fantasy_BG_Malus" + lStepsNumber + NameStep);
            }
        }

        #endregion
    }
}

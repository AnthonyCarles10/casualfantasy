﻿namespace Sportfaction.CasualFantasy.Fantasy
{
    [System.Serializable]
    public sealed class FantasyBuffStepData
    {
        #region Properties

        public  string  Name        { get { return _name; } }

        public  int     StepCount   { get { return _stepCount; } }

        #endregion

        #region Fields

        private string  _name       =   "";

        private int     _stepCount  =   0;

        #endregion

        #region Public Methods
        
        /// <summary>
        /// Initializes class instance
        /// </summary>
        /// <param name="pName">Buff name</param>
        /// <param name="pStepCount">Buff level</param>
        public void Init(string pName, int pStepCount)
        {
            _name       =   pName;
            _stepCount  =   pStepCount;
        }

        #endregion
    }
}

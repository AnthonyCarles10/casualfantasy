﻿namespace Sportfaction.CasualFantasy.Fantasy
{
    /// <summary>
    /// Description: Enum contains all player' skills
    /// Author: Rémi Carreira
    /// </summary>
    public enum eSkill
    {
        NONE,

        ATTACK_HEADED,
        CLOSE_SAVE,
        COMING_OFF,
        CONCENTRATION,
        CONTROL,
        CORNER,
        DEFENSE_HEADED,
        DRIBBLE,
        FAR_SAVE,
        FINISH,
        INTERCEPTION,
        LONG_PASS,
        PARAM,
        PASS,
        PERCUSSION,
        RIGOR,
        SHOT,
        STRENGTH,
        TACKLE,
        VISION,

        COUNT,
    };
}
 
﻿using Sportfaction.CasualFantasy.Events;

using System.Collections.Generic;
using UnityEngine;

namespace Sportfaction.CasualFantasy.Fantasy
{
    public class FantasyMain : MonoBehaviour
    {
        [Header("Links")]
        //[SerializeField, Tooltip("FantasyAPICalls")]
        //private FantasyAPICalls _fantasyAPICalls = null;

        [Header("API Data")]
        [SerializeField, Tooltip("Buffs (List)")]
        public List<FantasyBuffItemData> Buffs = new List<FantasyBuffItemData>();

        [Header("Game Events")]
        [SerializeField]
        private GameEvent _onGetBuffsList = null;

        #region API Calls

 

        #endregion

        #region Public Methods

        /// <summary>
        /// Transform Json to Buffs List
        /// Event: _onGetBuffList
        /// </summary>
        /// <param name="pJson">Json</param>
        public void TransformJsonToBuffsList(string pJson)
        {
            FantasyBuffData lFantasyBuffData = new FantasyBuffData();

            lFantasyBuffData.Init(pJson);

            Buffs = lFantasyBuffData.Items;

            _onGetBuffsList.Invoke();
        }

        #endregion
    }
}
 
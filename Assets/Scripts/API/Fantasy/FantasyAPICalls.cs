﻿using Sportfaction.CasualFantasy.Services;

using BestHTTP;
using UnityEngine;

namespace Sportfaction.CasualFantasy.Fantasy
{
    /// <summary>
    /// Description: 
    /// Author: Antoine de Lachèze-Murel
    /// </summary>
    public sealed class FantasyAPICalls : MonoBehaviour
    {

        #region Fields

        [Header("Links")]

        [SerializeField, Tooltip("FantasyMain")]
        private FantasyMain _fantasyMain = null;

        #endregion


        #region Public Methods

    

        #endregion



        #region Private Methods


        /// <summary>
        /// MonoBehaviour method used to link OnRequestSuccess to API
        /// </summary>
        private void OnEnable()
        {
            if (null != Api.Instance)
            {
                Api.Instance.OnSuccess += OnRequestSuccess;
            }
        }

        /// <summary>
        /// MonoBehaviour method used to unlink OnRequestSuccess from API
        /// </summary>
        private void OnDisable()
        {
            if (null != Api.Instance)
            {
                Api.Instance.OnSuccess -= OnRequestSuccess;
            }
        }

        /// <summary>
        /// Callback invoke when request from API was done successfully
        /// </summary>
        /// <param name="pCurrentUrl">string</param>
        /// <param name="pMethod">HTTPMethods</param>
        /// <param name="pData">string</param>
        private void OnRequestSuccess(string pCurrentUrl, HTTPMethods pMethod, string pData)
        {
            if (true == pCurrentUrl.Contains(ApiConstants.GetBuffsList))
            {
                _fantasyMain.TransformJsonToBuffsList(pData);

            }
        }

        #endregion
    }
}

using Sportfaction.CasualFantasy.Events;
using Sportfaction.CasualFantasy.Manager.Profile;
using Sportfaction.CasualFantasy.Services;

using Newtonsoft.Json.Linq;
using UnityEngine;
using UnityEngine.Assertions;
using PaperPlaneTools;

namespace Sportfaction.CasualFantasy.Core
{
    /// <summary>
    /// Class handling whether app has update or not
    /// Author: Antoine de Lachèze-Murel
    /// </summary>
    public class CoreMain : MonoBehaviour
    {
		#region Fields

		#pragma warning disable 0414
		[SerializeField, Tooltip("CoreAPICalls")]
		private	CoreAPICalls	_coreAPICalls	=	null;
		//[SerializeField, Tooltip("UserAPICalls")]
		//private	UserAPICalls	_userAPICalls	=	null;

        [SerializeField, Tooltip("Support")]
        private string      _support = "android";
        [SerializeField, Tooltip("Version")]
        private string      _version = "";
		#pragma warning disable 0414

        [SerializeField, Tooltip("Update Available")]
        public bool        _isUpdateAvailable = false;

        [SerializeField, Tooltip("Force Update")]
        private bool        _isForceUpdate = false;

        [SerializeField, Tooltip("Url PlayStore")]
        private string      _urlPlayStore = "https://play.google.com/store/apps/details?id=com.sportfaction.soccerarena.manager";

        [SerializeField, Tooltip("Url AppStore")]
        private string      _urlAppStore = "https://itunes.apple.com/us/app/soccer-arena-manager/id1440976503";


        [SerializeField]
        private GameEvent _noUpdate = null;

        #endregion

        #region Public Methods

        /// <summary>
        /// Handle json
        /// </summary>
        /// <param name="pData">Json to handle</param>
        /// <param name="pUserAPICalls">Class handling user API calls</param>
        public void HandleIsUpdateAvailable(string pData, UserAPICalls pUserAPICalls)
        {

            //_userAPICalls = pUserAPICalls;
            JToken lJToken = JToken.Parse(pData);
            _isUpdateAvailable = lJToken.Value<bool>("isUpdateAvailable");

            if (true == _isUpdateAvailable)
            {
                _isForceUpdate = lJToken.Value<bool>("forceUpdate");
                if (true == _isForceUpdate) {

                    new Alert(I2.Loc.LocalizationManager.GetTranslation("ApplicationVersionAlertTitle"),
                      I2.Loc.LocalizationManager.GetTranslation("ApplicationVersionAlertMessageForce"))
                       .SetPositiveButton(I2.Loc.LocalizationManager.GetTranslation("UpdateButton"), () => {
                           Application.OpenURL(SystemInfo.operatingSystem.Contains("iOS") ? _urlAppStore : _urlPlayStore);
                           Application.Quit();
                       })
                       .Show();


                } else {

                    UserData.Instance.Profile.NeedToUpdateVersion = true;
                    UserData.Instance.PersistUserData();
                    _noUpdate.Invoke();

                    //new Alert(I2.Loc.LocalizationManager.GetTranslation("ApplicationVersionAlertTitle"),
                    //   I2.Loc.LocalizationManager.GetTranslation("ApplicationVersionAlertMessageForce"))
                    //    .SetPositiveButton(I2.Loc.LocalizationManager.GetTranslation("UpdateButton"), () => {
                    //        Application.OpenURL(SystemInfo.operatingSystem.Contains("iOS") ? _urlAppStore : _urlPlayStore);
                    //        Application.Quit();
                    //    })
                    //     .SetNegativeButton(I2.Loc.LocalizationManager.GetTranslation("CancelButton"), () => {
                    //         _noUpdate.Invoke();
                    //     })
                    //    .Show();
                }
                    
            } else {
                if(true == UserData.Instance.Profile.NeedToUpdateVersion)
                {
                    UserData.Instance.Profile.VersionUpdated = true;
                    UserData.Instance.Profile.NeedToUpdateVersion = false;
                    UserData.Instance.PersistUserData();
                }
          

                _noUpdate.Invoke();
            }

        }

		#endregion

		#region Private Methods

		private void Awake()
		{
            _version = Application.version;

            #if !UNITY_EDITOR
                #if UNITY_ANDROID
							  _support = "android";
                #elif UNITY_IOS
				  _support = "ios";
                #endif
            #endif

            #if !DEVELOPMENT_BUILD
                Assert.IsNotNull(_coreAPICalls, "CoreMain: getManagerProfile() failed, _coreAPICalls is null.");
                _coreAPICalls.IsUpdateAvailable(_support, _version);
            #endif

        }

        #endregion
    }

}

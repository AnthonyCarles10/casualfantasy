using Sportfaction.CasualFantasy.Events;
using Sportfaction.CasualFantasy.Manager.Profile;
using Sportfaction.CasualFantasy.Services;

using BestHTTP;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using System;

namespace Sportfaction.CasualFantasy.Core
{
    /// <summary>
    /// Description: 
    /// Author: Antoine de Lach��ze-Murel
    /// </summary>
    public sealed class CoreAPICalls : MonoBehaviour
	{
        #region Fields
        [SerializeField, Tooltip("CoreMain")]
        private CoreMain _coreMain = null;

        [SerializeField, Tooltip("UserAPICalls")]
        private UserAPICalls _userAPICalls = null;

        [SerializeField, Tooltip("Game event to invoke")]
        private GameEvent _onUpdateStatusReceived = null;

        #endregion

        #region Public Methods

        /// <summary>
        /// Check if a new application version is available.
        /// </summary>
        public void IsUpdateAvailable(string pSupport, string pVersion)
        {
            
            Dictionary<string, string> lQueryParams = new Dictionary<string, string>()
            {
                { ApiConstants.IsUpdateAvailableSupportKey, pSupport },
                { ApiConstants.IsUpdateAvailableVersionKey, pVersion }
            };

            string lUrl = Api.Instance.GenerateUrl(ApiConstants.IsUpdateAvailable, lQueryParams);
            Assert.IsFalse(string.IsNullOrEmpty(lUrl), "[CoreAPICalls] IsUpdateAvailable(), lUrl is null or empty.");

            HTTPRequest lRequest = Api.Instance.SetUpRequest(lUrl, Api.Methods.GET, false);
            lRequest.Timeout = TimeSpan.FromSeconds(5);

            StartCoroutine(Api.Instance.SendRequest(lRequest));
        }

		#endregion

		#region Private Methods

        /// <summary>
        /// MonoBehaviour method used to link OnRequestSuccess to API
        /// </summary>
        private void OnEnable()
        {
            if (null != Api.Instance)
            {
                Api.Instance.OnSuccess += OnRequestSuccess;
            }
        }

        /// <summary>
        /// MonoBehaviour method used to unlink OnRequestSuccess from API
        /// </summary>
        private void OnDisable()
        {
            if (null != Api.Instance)
            {
                Api.Instance.OnSuccess -= OnRequestSuccess;
            }
        }

        /// <summary>
        /// Callback invoke when request from API was done successfully
        /// </summary>
        /// <param name="pCurrentUrl">string</param>
        /// <param name="pMethod">HTTPMethods</param>
        /// <param name="pData">string</param>
        private void OnRequestSuccess(string pCurrentUrl, HTTPMethods pMethod, string pData)
        {
            if (true == pCurrentUrl.Contains(ApiConstants.IsUpdateAvailable))
            {
                _onUpdateStatusReceived.Invoke();
                _coreMain.HandleIsUpdateAvailable(pData, _userAPICalls);
            }
		}

		#endregion
	}
}
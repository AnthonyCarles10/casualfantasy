﻿using Sportfaction.CasualFantasy.Services;

using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using UnityEngine;

namespace Sportfaction.CasualFantasy.Championship
{
    [System.Serializable]
    public sealed class OfficialTeamsByCountryItem
    {
        #region Properties

        public List<OfficialTeamsByCountryItemClub> Clubs { get { return _clubs; } }

        public string Country { get { return _country; } }

        #endregion

        #region Fields

        [SerializeField, Tooltip("_clubs")]
        private List<OfficialTeamsByCountryItemClub> _clubs = new List<OfficialTeamsByCountryItemClub>();

        [SerializeField, Tooltip("_country")]
        private string _country = "";

        #endregion

        #region Public Methods

        /// <summary>
        /// Initializes data with token response from Api
        /// </summary>
        /// <param name="pToken">Token to parse</param>
        public	void Init(JToken pToken)
        { 
            _country = pToken.SafeValue<string>("country");

            JArray lTeamsArray = pToken.Value<JArray>("clubs");

            foreach (JToken lJT in lTeamsArray)
            {
                OfficialTeamsByCountryItemClub lItem = new OfficialTeamsByCountryItemClub();

                lItem.Init(lJT);

                _clubs.Add(lItem);
            }
        }

        #endregion
    }
}

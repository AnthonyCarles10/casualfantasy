﻿using Sportfaction.CasualFantasy.Services;

using Newtonsoft.Json.Linq;
using UnityEngine;

namespace Sportfaction.CasualFantasy.Championship
{
    [System.Serializable]
    public sealed class OfficialTeamsByCountryItemClub
    {
        #region Properties


        public int TeamId { get { return _teamId; } }

        public int OfficialTeamOptaId { get { return _officialTeamOptaId; } }

        public int ChampionshipId { get { return _championshipId; } }

        public string TeamName { get { return _teamName; } }

        public string Country { get { return _country; } }

        #endregion

        #region Fields

        [SerializeField, Tooltip("_teamId")]
        private int _teamId = -1;

        [SerializeField, Tooltip("_officialTeamOptaId")]
        private int _officialTeamOptaId = -1;

        [SerializeField, Tooltip("_championshipId")]
        private int _championshipId = -1;

        [SerializeField, Tooltip("_teamName")]
        private string _teamName = "";

        [SerializeField, Tooltip("_country")]
        private string _country = "";

        #endregion

        #region Public Methods

        /// <summary>
        /// Initializes data with token response from Api
        /// </summary>
        /// <param name="pToken">Token to parse</param>
        public	void Init(JToken pToken)
        {  
            _teamId             =   pToken.SafeValue<int>("team_id", -1);

            _teamName           =   pToken.SafeValue<string>("team_name");

            _officialTeamOptaId =   pToken.SafeValue<int>("official_team_opta_id", -1);

            _championshipId     =   pToken.SafeValue<int>("championship_id", -1);

            _country            =   pToken.SafeValue<string>("country");
        }

        #endregion
    }
}

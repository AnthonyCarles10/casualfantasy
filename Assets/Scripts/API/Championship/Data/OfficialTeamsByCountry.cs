﻿using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using UnityEngine;

namespace Sportfaction.CasualFantasy.Championship
{
    [System.Serializable]
    public sealed class OfficialTeamsByCountry
    {
        #region Properties

        public List<OfficialTeamsByCountryItem> Items { get { return _items; } }

        #endregion

        #region Fields

        [SerializeField, Tooltip("_items")]
        private List<OfficialTeamsByCountryItem> _items = new List<OfficialTeamsByCountryItem>();

        #endregion

        #region Public Methods

        /// <summary>
        /// Initialize Data with json response from Api
        /// </summary>
        /// <param name="pJson">json response</param>
        public	void			Init(string pJson)
        {
            
            JToken lJToken = JToken.Parse(pJson);
            JArray lTeamsArray = lJToken.Value<JArray>("items");

            foreach (JToken lJT in lTeamsArray)
            {
                OfficialTeamsByCountryItem lItem = new OfficialTeamsByCountryItem();
                lItem.Init(lJT);
                _items.Add(lItem);
            }

        }


        #endregion
    }
}

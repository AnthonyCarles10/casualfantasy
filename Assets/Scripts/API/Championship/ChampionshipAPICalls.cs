using Sportfaction.CasualFantasy.Services;

using BestHTTP;
using UnityEngine;
using UnityEngine.Assertions;
using System;

namespace Sportfaction.CasualFantasy.Championship
{
	/// <summary>
	/// Description: 
	/// Author: Antoine de Lach�ze-Murel
	/// </summary>
	public sealed class ChampionshipAPICalls : MonoBehaviour
	{
        #region Fields

        [SerializeField, Tooltip("ChampionshipMain")]
        private ChampionshipMain _championshipMain = null;

        [SerializeField]

        private string _getOfficialTeamsByCountryURL = "";

        private string _getChampionshipNextPlayerFormUpdateURL = "";

        #endregion

        #region Public Methods

        /// <summary>
        /// Get List cards
        /// </summary>
        public void GetOfficialTeamsByCountry()
		{
			if (null != Api.Instance)
			{
                _getOfficialTeamsByCountryURL = Api.Instance.GenerateUrl(ApiConstants.GetOfficialTeamsByCountry);

				Assert.IsFalse(string.IsNullOrEmpty(_getOfficialTeamsByCountryURL), "[ChampionshipAPICalls] GetOfficialTeamsByCountry(), lUrl is null or empty.");

                HTTPRequest lRequest = Api.Instance.SetUpRequest(_getOfficialTeamsByCountryURL, Api.Methods.GET, true);
                lRequest.Timeout = TimeSpan.FromSeconds(5);

                StartCoroutine(Api.Instance.SendRequest(lRequest));
            }
		}

        /// <summary>
        /// Get Championship Next Player Form Update Timer
        /// </summary>
        public void GetChampionshipNextPlayerFormUpdate(string pCountry)
        {
            if (null != Api.Instance)
            {
                _getChampionshipNextPlayerFormUpdateURL = Api.Instance.GenerateUrl("championships/" + pCountry + "/next_player_form_update");

                HTTPRequest lRequest = Api.Instance.SetUpRequest(_getChampionshipNextPlayerFormUpdateURL, Api.Methods.GET, true);
                lRequest.Timeout = TimeSpan.FromSeconds(5);

                StartCoroutine(Api.Instance.SendRequest(lRequest));
            }
        }

        public void AddEvents()
        {
            if (null != Api.Instance)
            {
                Api.Instance.OnSuccess += OnRequestSuccess;
            }
        }

        #endregion

        #region Private Methods


        /// <summary>
        /// MonoBehaviour method used to link OnRequestSuccess to API
        /// </summary>
        private void OnEnable()
        {
            if (null != Api.Instance)
            {
                Api.Instance.OnSuccess += OnRequestSuccess;
                Api.Instance.OnBadRequest += OnBadRequestEvent;
            }
        }

        /// <summary>
        /// MonoBehaviour method used to unlink OnRequestSuccess from API
        /// </summary>
        private void OnDisable()
        {
            if (null != Api.Instance)
            {
                Api.Instance.OnSuccess -= OnRequestSuccess;
                Api.Instance.OnBadRequest -= OnBadRequestEvent;
            }
        }

        /// <summary>
        /// Send a request to API
        /// </summary>
        /// <param name="pUrl"></param>
        /// <param name="pMethod"></param>
        /// <param name="pAuthenticationRequired"></param>
        private void SendRequest(string pUrl, Api.Methods pMethod, bool pAuthenticationRequired)
		{
			HTTPRequest lRequest = Api.Instance.SetUpRequest(pUrl, pMethod, pAuthenticationRequired);

			Assert.IsNotNull(lRequest, "[ChampionshipAPICalls] SendRequest(), lRequest is null.");
            lRequest.Timeout = TimeSpan.FromSeconds(5);

            StartCoroutine(Api.Instance.SendRequest(lRequest));
		}

   



        /// <summary>
        /// Callback invoke when request from API was done successfully
        /// </summary>
        /// <param name="pCurrentUrl">string</param>
        /// <param name="pMethod">HTTPMethods</param>
        /// <param name="pData">string</param>
        private void OnRequestSuccess(string pCurrentUrl, HTTPMethods pMethod, string pData)
        {
            if (true == pCurrentUrl.Equals(_getOfficialTeamsByCountryURL))
            {
                Assert.IsNotNull(_championshipMain, "[ChampionshipAPICalls] OnRequestSuccess(), _championshipMain is null or empty.");

                _championshipMain.HandleJsonListOfficialTeamsByCountry(pData);
            }
            else if (true == pCurrentUrl.Contains("next_player_form_update") && pCurrentUrl.Contains("championships"))
            {
                Assert.IsNotNull(_championshipMain, "[ChampionshipAPICalls] OnRequestSuccess(), _championshipMain is null or empty.");

                if(pCurrentUrl.Contains("France"))
                {
                    _championshipMain.ParseChampionshipNextPlayerFormUpdate(pData, "France");
                }
                else if (pCurrentUrl.Contains("Germany"))
                {
                    _championshipMain.ParseChampionshipNextPlayerFormUpdate(pData, "Germany");
                }
                else if (pCurrentUrl.Contains("Italy"))
                {
                    _championshipMain.ParseChampionshipNextPlayerFormUpdate(pData, "Italy");
                }
                else if (pCurrentUrl.Contains("England"))
                {
                    _championshipMain.ParseChampionshipNextPlayerFormUpdate(pData, "England");
                }
                else if (pCurrentUrl.Contains("Spain"))
                {
                    _championshipMain.ParseChampionshipNextPlayerFormUpdate(pData, "Spain");
                }
            }
        }

        /// <summary>
		/// Callback invoke when request from API was done successfully
		/// </summary>
		/// <param name="pCurrentUrl">Url used</param>
		/// <param name="pData">Data received</param>
        private void OnBadRequestEvent(string pCurrentUrl, string pData)
        {
            if (true == pCurrentUrl.Equals(_getOfficialTeamsByCountryURL))
            {
                #if DEVELOPMENT_BUILD
                    Debug.LogFormat("<color=red>[ChampionshipAPICalls]</color> OnBadRequestEvent(): {0}", pData);
                #endif
            }
        }

		#endregion
    }
}
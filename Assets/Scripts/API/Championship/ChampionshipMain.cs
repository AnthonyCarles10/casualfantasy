﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Sportfaction.CasualFantasy.Events;
using Sportfaction.CasualFantasy.Services.Timers;
using UnityEngine;
using UnityEngine.Assertions;

namespace Sportfaction.CasualFantasy.Championship
{
    public class ChampionshipMain : MonoBehaviour
    {
        #region Properties

        public OfficialTeamsByCountry OfficialTeamsByCountry { get { return _officialTeamsByCountry; } }

        public string FranceNextUpdate;

        public string SpainNextUpdate;

        public string GermanyNextUpdate;

        public string EnglandNextUpdate;

        public string ItalyNextUpdate;

        #endregion

        #region Fields

        [Header("Links")]

        [SerializeField, Tooltip("ChampionshipAPICalls")]
        private ChampionshipAPICalls _championshipAPICalls = null;

        [Header("Data")]

        [SerializeField, Tooltip("ActionsData")]
        private OfficialTeamsByCountry _officialTeamsByCountry = null;

        [Header("GameEvent")]

        [SerializeField]
        private GameEvent _onGetListOfficialTeamsByCountry = null;

        #endregion

        #region API Calls

        public void GetOfficialTeamsByCountry() { _championshipAPICalls.GetOfficialTeamsByCountry(); }
        public void GetChampionshipNextPlayerFormUpdate(string pCountry) { _championshipAPICalls.GetChampionshipNextPlayerFormUpdate(pCountry); }
        public void APIAddEvents() { _championshipAPICalls.AddEvents(); }

        #endregion

        #region Public Methods
        /// <summary>
        /// Handle json and create opposition
        /// </summary>
        /// <param name="pJson">Json to handle</param>
        public void HandleJsonListOfficialTeamsByCountry(string pJson)
        {
            pJson = "{\"items\":" + pJson + "}";

            _officialTeamsByCountry = new OfficialTeamsByCountry();
            _officialTeamsByCountry.Init(pJson);

            _onGetListOfficialTeamsByCountry.Invoke();
        }

        public void ParseChampionshipNextPlayerFormUpdate(string pJson, string pCountry)
        {
            JToken lJToken = JToken.Parse(pJson);
            string lcurrentTimestamp = lJToken.Value<string>("current_timestamp");
            string lformUpdateTimestamp = lJToken.Value<string>("form_update_timestamp");

            if (false == string.IsNullOrEmpty(pJson))
            {
           
                switch (pCountry)
                {
                    case "France":

                        LocalTimersManager.Instance.StartTimer("FranceChampionship", lformUpdateTimestamp, lcurrentTimestamp);

                        FranceNextUpdate = pJson;

                        PlayerPrefs.SetString("FranceNextUpdate", pJson);

                        PlayerPrefs.Save();
                        break;

                    case "Germany":

                        LocalTimersManager.Instance.StartTimer("GermanyChampionship", lformUpdateTimestamp, lcurrentTimestamp);

                        GermanyNextUpdate = pJson;

                        PlayerPrefs.SetString("GermanyNextUpdate", pJson);

                        PlayerPrefs.Save();
                        break;

                    case "Italy":

                        LocalTimersManager.Instance.StartTimer("ItalyChampionship", lformUpdateTimestamp, lcurrentTimestamp);

                        ItalyNextUpdate = pJson;

                        PlayerPrefs.SetString("ItalyChampionship", pJson);

                        PlayerPrefs.Save();

                        break;

                    case "England":

                        LocalTimersManager.Instance.StartTimer("EnglandChampionship", lformUpdateTimestamp, lcurrentTimestamp);

                        EnglandNextUpdate = pJson;

                        PlayerPrefs.SetString("EnglandNextUpdate", pJson);

                        PlayerPrefs.Save();
                        break;

                    case "Spain":

                        LocalTimersManager.Instance.StartTimer("SpainChampionship", lformUpdateTimestamp, lcurrentTimestamp);

                        SpainNextUpdate = pJson;

                        PlayerPrefs.SetString("SpainNextUpdate", pJson);

                        PlayerPrefs.Save();

                        break;
                }
            }
        }

        public void LoadData()
        {
            if (PlayerPrefs.HasKey("FranceNextUpdate"))
            {
                JToken lJToken = JToken.Parse(PlayerPrefs.GetString("FranceNextUpdate"));

                string lcurrentTimestamp = lJToken.Value<string>("current_timestamp");

                string lformUpdateTimestamp = lJToken.Value<string>("form_update_timestamp");

                LocalTimersManager.Instance.StartTimer("FranceChampionship", lformUpdateTimestamp, lcurrentTimestamp);

                FranceNextUpdate = PlayerPrefs.GetString("FranceNextUpdate");

            }

            if (PlayerPrefs.HasKey("GermanyNextUpdate"))
            {
                JToken lJToken = JToken.Parse(PlayerPrefs.GetString("GermanyNextUpdate"));

                string lcurrentTimestamp = lJToken.Value<string>("current_timestamp");

                string lformUpdateTimestamp = lJToken.Value<string>("form_update_timestamp");

                LocalTimersManager.Instance.StartTimer("GermanyChampionship", lformUpdateTimestamp, lcurrentTimestamp);

                FranceNextUpdate = PlayerPrefs.GetString("GermanyNextUpdate");

            }

            if (PlayerPrefs.HasKey("ItalyNextUpdate"))
            {
                JToken lJToken = JToken.Parse(PlayerPrefs.GetString("ItalyNextUpdate"));

                string lcurrentTimestamp = lJToken.Value<string>("current_timestamp");

                string lformUpdateTimestamp = lJToken.Value<string>("form_update_timestamp");

                LocalTimersManager.Instance.StartTimer("ItalyChampionship", lformUpdateTimestamp, lcurrentTimestamp);

                FranceNextUpdate = PlayerPrefs.GetString("ItalyNextUpdate");

            }

            if (PlayerPrefs.HasKey("EnglandNextUpdate"))
            {
                JToken lJToken = JToken.Parse(PlayerPrefs.GetString("EnglandNextUpdate"));

                string lcurrentTimestamp = lJToken.Value<string>("current_timestamp");

                string lformUpdateTimestamp = lJToken.Value<string>("form_update_timestamp");

                LocalTimersManager.Instance.StartTimer("EnglandChampionship", lformUpdateTimestamp, lcurrentTimestamp);

                FranceNextUpdate = PlayerPrefs.GetString("EnglandNextUpdate");

            }

            if (PlayerPrefs.HasKey("SpainNextUpdate"))
            {
                JToken lJToken = JToken.Parse(PlayerPrefs.GetString("SpainNextUpdate"));

                string lcurrentTimestamp = lJToken.Value<string>("current_timestamp");

                string lformUpdateTimestamp = lJToken.Value<string>("form_update_timestamp");

                LocalTimersManager.Instance.StartTimer("SpainChampionship", lformUpdateTimestamp, lcurrentTimestamp);

                FranceNextUpdate = PlayerPrefs.GetString("SpainNextUpdate");

            }


        }

        #endregion

        #region Private Methods

        //private void Start()
        //{
        //    Assert.IsNotNull(_championshipAPICalls, "ChampionshipMain: Start(), _cardAPICalls is null.");
        //    GetOfficialTeamsByCountry();
        //}



        private void Awake()
        {
            LoadData();
        }

        #endregion
    }
}

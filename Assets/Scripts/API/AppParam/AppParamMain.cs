﻿using Sportfaction.CasualFantasy.Events;
using Sportfaction.CasualFantasy.Services;

using Newtonsoft.Json.Linq;
using UnityEngine;
using UnityEngine.Assertions;

namespace Sportfaction.CasualFantasy.AppParam
{
    public class AppParamMain : MonoBehaviour
    {
        #region Fields

		// Constant contains the number of levels
		private	const int	NumberOfLevels	=	100;

		[Header("Data")]
		[SerializeField, Tooltip("Scriptable object contains data about manager' salary")]
		private	AppParamManagerSalaryData	_salaryData	=	null;
		[SerializeField, Tooltip("Scriptable object contains data about manager' score")]
		private AppParamManagerScoreData	_scoreData	=	null;
		[SerializeField, Tooltip("Scriptable object contains data about manager' soft")]
		private	AppParamManagerSoftData		_softData	=	null;
		[SerializeField, Tooltip("Scriptable object contains data about manager' xp")]
		private	AppParamManagerXpData		_xpData		=	null;

        [Header("GameObjects")]
        [SerializeField, Tooltip("ActionAPICalls")]
        private AppParamAPICalls _appParamAPICalls = null;

		#endregion
     
		#region Events

		[Header("GameEvent")]
		[SerializeField]
		private GameEvent _onGetAppParamManagerList = null;

        #endregion


        #region API Calls

        public void GetAppParamManagerList() { _appParamAPICalls.GetAppParamManagerList(); }

        #endregion


        #region Public Methods
        /// <summary>
        /// Handle json
        /// </summary>
        /// <param name="pData">Json to handle</param>
        public void OnGetAppParamManagerList(string pData)
        {
			Assert.IsNotNull(_salaryData, "[AppParamMain] OnGetAppParamManagerList(), _salaryData is null.");

			_salaryData.InitializeArrays(NumberOfLevels);

			Assert.IsNotNull(_scoreData, "[AppParamMain] OnGetAppParamManagerList(), _scoreData is null.");

			_scoreData.InitializeArrays(NumberOfLevels);

			Assert.IsNotNull(_softData, "[AppParamMain] OnGetAppParamManagerList(), _softData is null.");

			_softData.InitializeArrays(NumberOfLevels);

			Assert.IsNotNull(_xpData, "[AppParamMain] OnGetAppParamManagerList(), _xpData is null.");

			_xpData.InitializeArrays(NumberOfLevels);

			JToken	lToken	=	JToken.Parse(pData);
			if (null != lToken)
			{
				JArray	lTeamsArray	=	lToken.Value<JArray>("list");
				if (null != lTeamsArray)
				{
					string	lName	=	"";
					string	lValue	=	"";

					foreach (JToken lCurToken in lTeamsArray)
					{
						if (null != lCurToken)
						{
							lName	=	lCurToken.SafeValue<string>("name");
							lValue	=	lCurToken.SafeValue<string>("value");

							if(lName.Contains("manager_salary_"))
							{
								_salaryData.InitializeValue(lName, lValue);
							}
							else if (lName.Contains("manager_score_"))
							{
								_scoreData.InitializeValue(lName, lValue);
							}
							else if (lName.Contains("manager_soft_"))
							{
								_softData.InitializeValue(lName, lValue);
							}
							else if(lName.Contains("manager_xp_"))
							{
								_xpData.InitializeValue(lName, lValue);
							}
						}
					}
				}
			}

			if (null != _onGetAppParamManagerList)
			{
				_onGetAppParamManagerList.Invoke();
			}
        }

        #endregion

    }
}

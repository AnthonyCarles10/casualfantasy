﻿using System.Text.RegularExpressions;
using UnityEngine;

namespace Sportfaction.CasualFantasy.AppParam
{
	/// <summary>
	/// Description: Class handles data about manager' score
	/// Author: Antoine
	/// </summary>
	[CreateAssetMenu(fileName = "Store-AppParam-ScoreData", menuName = "CasualFantasy/Data/Store/AppParam/Score")]
    public sealed class AppParamManagerScoreData : ScriptableObject
    {
        #region Properties

        public string Won { get { return _won; } }

        public string Lost { get { return _lost; } }

        public string Draw { get { return _draw; } }

        public string[] BonusStar { get { return _bonusStar; } set { _bonusStar = value; } }

        #endregion

        #region Fields

        [SerializeField, Tooltip("_won")]
        private string _won = "0";

        [SerializeField, Tooltip("_lost")]
        private string _lost = "0";

        [SerializeField, Tooltip("_draw")]
        private string _draw = "0";

        [SerializeField, Tooltip("_bonusStar")]
        private string[] _bonusStar;

		private	Regex	_bonusStarRegex	=	new Regex(@"manager_score_bonus_star_\d+$");

        #endregion

        #region Public Methods

		/// <summary>
		/// Initialize arrays
		/// </summary>
		/// <param name="pLength">Length of arrays</param>
		public	void	InitializeArrays(int pLength)
		{
			_bonusStar	=	new string[pLength];
		}

		/// <summary>
		/// Initialize a value
		/// </summary>
		/// <param name="pName">Name contains some informations</param>
		/// <param name="pValue">New value to set</param>
		public	void	InitializeValue(string pName, string pValue)
        {
            switch (pName)
            {
                case "manager_score_won_game":
                    _won = pValue;
                    break;
                case "manager_score_lost_game":
                    _lost = pValue;
                    break;
                case "manager_score_draw_game":
                    _draw = pValue;
                    break;
                default:
                    if (null != _bonusStarRegex && _bonusStarRegex.Match(pName).Success)
                    {
						if (null != _bonusStar)
						{
							Match	lMatch = Regex.Match(pName, @"\d+");
							if (null != lMatch)
							{
								_bonusStar[int.Parse(lMatch.Value)] = pValue;
							}
						}
                    }
                    break;
            }

        }

        #endregion
    }
}

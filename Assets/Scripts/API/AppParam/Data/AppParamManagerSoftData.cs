﻿using System.Text.RegularExpressions;
using UnityEngine;

namespace Sportfaction.CasualFantasy.AppParam
{
	/// <summary>
	/// Description: Class handles data about manager' soft currency
	/// Author: Antoine
	/// </summary>
	[CreateAssetMenu(fileName = "Store-AppParam-SoftData", menuName = "CasualFantasy/Data/Store/AppParam/Soft")]
    public sealed class AppParamManagerSoftData : ScriptableObject
    {
        #region Properties

        public string[] BonusLevelupVip { get { return _bonusLevelupVip; } set { _bonusLevelupVip = value; } }

        public string[] BonusLevelup { get { return _bonusLevelup; } set { _bonusLevelup = value; } }

        public string[] BonusStar { get { return _bonusStar; } set { _bonusStar = value; } }

        public string BonusVip { get { return _bonusVip; } set { _bonusVip = value; } }

        public string BonusGoal { get { return _bonusGoal; } set { _bonusGoal = value; } }

        #endregion

        #region Fields

        [SerializeField, Tooltip("_bonusLevelupVip")]
        private string[] _bonusLevelupVip;

        [SerializeField, Tooltip("_bonusLevelup")]
        private string[] _bonusLevelup;

        [SerializeField, Tooltip("_bonusStar")]
        private string[] _bonusStar;

        [SerializeField, Tooltip("_bonusVip")]
        private string _bonusVip;

        [SerializeField, Tooltip("_bonusGoal")]
        private string _bonusGoal;

		private	Regex	_VIPsoftBonusLevelUpRegex	=	new Regex(@"manager_soft_bonus_levelup_vip_\d+$");
		private	Regex	_softBonusLevelUpRegex		=	new Regex(@"manager_soft_bonus_levelup_\d+$");
		private	Regex	_softBonusStarRegex			=	new Regex(@"manager_soft_bonus_star_\d+$");

        #endregion

        #region Public Methods

		/// <summary>
		/// Initialize arrays
		/// </summary>
		/// <param name="pLength">Length of arrays</param>
		public	void	InitializeArrays(int pLength)
		{
			_bonusStar			=	new string[pLength];
			_bonusLevelup		=	new string[pLength];
			_bonusLevelupVip	=	new string[pLength];
		}

		/// <summary>
		/// Initialize a value
		/// </summary>
		/// <param name="pName">Name contains some informations</param>
		/// <param name="pValue">New value to set</param>
		public	void	InitializeValue(string pName, string pValue)
        {
            if (pName.Equals("manager_soft_bonus_vip"))
            {
                _bonusVip = pValue;
            }
            else if (pName.Equals("manager_soft_goal_bonus"))
            {
                _bonusGoal = pValue;
            }
            else if (null != _VIPsoftBonusLevelUpRegex && _VIPsoftBonusLevelUpRegex.Match(pName).Success)
            {
				if (null != _bonusLevelupVip)
				{
					Match	lMatch = Regex.Match(pName, @"\d+");
					if (null != lMatch)
					{
						_bonusLevelupVip[int.Parse(lMatch.Value)] = pValue;
					}
				}
            }
            else if (null != _softBonusLevelUpRegex && _softBonusLevelUpRegex.Match(pName).Success)
            {
				if (null != _bonusLevelup)
				{
					Match	lMatch = Regex.Match(pName, @"\d+");
					if (null != lMatch)
					{
						_bonusLevelup[int.Parse(lMatch.Value)] = pValue;
					}
				}
            }
            else if (null != _softBonusStarRegex && _softBonusStarRegex.Match(pName).Success)
            {
				if (null != _bonusStar)
				{
					Match	lMatch = Regex.Match(pName, @"\d+");
					if (null != lMatch)
					{
						_bonusStar[int.Parse(lMatch.Value)] = pValue;
					}
				}
            }
        }



        #endregion
    }
}

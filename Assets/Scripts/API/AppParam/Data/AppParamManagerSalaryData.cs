﻿using System.Text.RegularExpressions;
using UnityEngine;

namespace Sportfaction.CasualFantasy.AppParam
{
	/// <summary>
	/// Description: Class contains data about manager' salary
	/// Author: Antoine 
	/// </summary>
	[CreateAssetMenu(fileName = "Store-AppParam-SalaryData", menuName = "CasualFantasy/Data/Store/AppParam/Salary")]
	public sealed class AppParamManagerSalaryData : ScriptableObject
	{
		#region Properties

		// Property used to get the list of salary cap for a normal manager
		public string[] Cap			{ get { return _cap; } }
		// Property used to get the list of salary cap for a vip manager
		public string[]	CapVip		{ get { return _capVip; } }
		// Property used to get the list of salary raise for a normal manager
		public string[]	Raise		{ get { return _raise; } }
		// Property used to get the list of	salary raise for a vip manager
		public string[]	RaiseVip	{ get { return _raiseVip; } }


        #endregion

        #region Fields

		[SerializeField, Tooltip("_cap")]
		private string[]	_cap		=	null;
		[SerializeField, Tooltip("_capVip")]
		private string[]	_capVip		=	null;
		[SerializeField, Tooltip("_raise")]
		private string[]	_raise		=	null;
		[SerializeField, Tooltip("_raiseVip")]
		private string[]	_raiseVip	=	null;

		private	Regex	_salaryCapRegex			=	new Regex(@"manager_salary_cap_\d+$");
		private	Regex	_VIPSalaryCapRegex		=	new Regex(@"manager_salary_cap_vip_\d+$");
		private	Regex	_salaryRaiseRegex		=	new Regex(@"manager_salary_raise_\d+$");
		private	Regex	_VIPSalaryRaiseRegex	=	new Regex(@"manager_salary_raise_vip_\d+$");

        #endregion

        #region Public Methods

		/// <summary>
		/// Initialize arrays
		/// </summary>
		/// <param name="pLength">Length of arrays</param>
		public	void	InitializeArrays(int pLength)
		{
			_cap		=	new string[pLength];
			_capVip		=	new string[pLength];
			_raise		=	new string[pLength];
			_raiseVip	=	new string[pLength];
		}

		/// <summary>
		/// Initialize a value
		/// </summary>
		/// <param name="pName">Name contains some informations</param>
		/// <param name="pValue">New value to set</param>
		public	void	InitializeValue(string pName, string pValue)
		{
			if (null != _VIPSalaryCapRegex && _VIPSalaryCapRegex.Match(pName).Success)
			{
				if (null != _capVip)
				{
					Match	lMatch = Regex.Match(pName, @"\d+");
					if (null != lMatch)
					{
						_capVip[int.Parse(lMatch.Value)] = pValue;
					}
				}
			}
			else if (null != _VIPSalaryRaiseRegex && _VIPSalaryRaiseRegex.Match(pName).Success)
			{
				if (null != _raiseVip)
				{
					Match	lMatch = Regex.Match(pName, @"\d+");
					if (null != lMatch)
					{
						_raiseVip[int.Parse(lMatch.Value)] = pValue;
					}
				}
			}
			else if (null != _salaryCapRegex && _salaryCapRegex.Match(pName).Success)
			{
				if (null != _cap)
				{
					Match	lMatch = Regex.Match(pName, @"\d+");
					if (null != lMatch)
					{
						_cap[int.Parse(lMatch.Value)] =	pValue;
					}
				}
			}
			else if(null != _salaryRaiseRegex && _salaryRaiseRegex.Match(pName).Success)
			{
				if (null != _raise)
				{
					Match	lMatch = Regex.Match(pName, @"\d+");
					if (null != lMatch)
					{
						_raise[int.Parse(lMatch.Value)] = pValue;
					}
				}
			}
        }

        #endregion
    }
}

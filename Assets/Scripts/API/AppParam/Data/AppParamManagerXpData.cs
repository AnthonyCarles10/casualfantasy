﻿using System.Text.RegularExpressions;
using UnityEngine;

namespace Sportfaction.CasualFantasy.AppParam
{
	/// <summary>
	/// Description: Class handles data about manager' xp
	/// Author: Antoine
	/// </summary>
	[CreateAssetMenu(fileName = "Store-AppParam-XPData", menuName = "CasualFantasy/Data/Store/AppParam/XP")]
    public sealed class AppParamManagerXpData : ScriptableObject
    {
        #region Properties

        public string BonusVip { get { return _bonusVip; } }

        public string WonGame { get { return _wonGame; } }

        public string LostGame { get { return _lostGame; } }

        public string DrawGame { get { return _drawGame; } }

        public string[] Cap { get { return _cap; } set { _cap = value; } }

        public string[] Raise { get { return _raise; } set { _raise = value; } }

        #endregion

        #region Fields

        [SerializeField, Tooltip("_bonusVip")]
        private string _bonusVip = "0";

        [SerializeField, Tooltip("_wonGame")]
        private string _wonGame = "0";

        [SerializeField, Tooltip("_lostGame")]
        private string _lostGame = "0";

        [SerializeField, Tooltip("_drawGame")]
        private string _drawGame = "0";

        [SerializeField, Tooltip("_cap")]
        private string[] _cap;

        [SerializeField, Tooltip("_raise")]
        private string[] _raise;

		private	Regex	_xpCapRegex		=	new Regex(@"manager_xp_cap_\d+$");
		private	Regex	_xpRaiseRegex	=	new Regex(@"manager_xp_raise_\d+$");

        #endregion

        #region Public Methods

		/// <summary>
		/// Initialize arrays
		/// </summary>
		/// <param name="pLength">Length of arrays</param>
		public	void	InitializeArrays(int pLength)
		{
			_cap	=	new string[pLength];
			_raise	=	new string[pLength];
		}

		/// <summary>
		/// Initialize a value
		/// </summary>
		/// <param name="pName">Name contains some informations</param>
		/// <param name="pValue">New value to set</param>
		public	void	InitializeValue(string pName, string pValue)
        {
            if (pName.Equals("manager_xp_bonus_vip"))
            {
                _bonusVip = pValue;
            }
            else if (pName.Equals("manager_xp_lost_game"))
            {
                _lostGame = pValue;
            }
            else if (pName.Equals("manager_xp_won_game"))
            {
                _wonGame = pValue;
            }
            else if (pName.Equals("manager_xp_draw_game"))
            {
                _drawGame = pValue;
            }
            else if (null != _xpCapRegex && _xpCapRegex.Match(pName).Success)
            {
				if (null != _cap)
				{
					Match lMatch = Regex.Match(pName, @"\d+");
					if (null != lMatch)
					{
						_cap[int.Parse(lMatch.Value)] = pValue;
					}
				}
            }
            else if (null != _xpRaiseRegex && _xpRaiseRegex.Match(pName).Success)
            {
				if (null != _raise)
				{
					Match lMatch = Regex.Match(pName, @"\d+");
					if (null != lMatch)
					{
						_raise[int.Parse(lMatch.Value)] = pValue;
					}
				}
            }
        }

        #endregion
    }
}

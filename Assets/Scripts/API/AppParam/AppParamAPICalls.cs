﻿using Sportfaction.CasualFantasy.Services;

using BestHTTP;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using System;

namespace Sportfaction.CasualFantasy.AppParam
{
    /// <summary>
    /// Description: 
    /// Author: Antoine de Lachèze-Murel
    /// </summary>
    public sealed class AppParamAPICalls : MonoBehaviour
	{
        #region Fields

        [SerializeField, Tooltip("AppParamMain")]
        private AppParamMain _appParamMain = null;

        [SerializeField]

        private string _getAppParamManagerList = "";


        #endregion

        #region Public Methods

        /// <summary>
        /// Get App Param List
        /// </summary>
        public void GetAppParamManagerList()
		{
			if (null != Api.Instance)
			{
                Dictionary<string, string> lQueryParams = new Dictionary<string, string>()
                {
                    { "keyword", "manager_" },
                    { "limit", "1000" }
                };

                _getAppParamManagerList = Api.Instance.GenerateUrl(ApiConstants.GetAppParams, lQueryParams);

				Assert.IsFalse(string.IsNullOrEmpty(_getAppParamManagerList), "[AppParamAPICalls] GetAppParamList(), _getAppParamManagerList is null or empty.");

                HTTPRequest lRequest = Api.Instance.SetUpRequest(_getAppParamManagerList, Api.Methods.GET, false);
                lRequest.Timeout = TimeSpan.FromSeconds(5);

                StartCoroutine(Api.Instance.SendRequest(lRequest));
            }
		}

        #endregion

        #region Private Methods


        /// <summary>
        /// MonoBehaviour method used to link OnRequestSuccess to API
        /// </summary>
        private void OnEnable()
        {
            if (null != Api.Instance)
            {
                Api.Instance.OnSuccess += OnRequestSuccess;
                Api.Instance.OnBadRequest += OnBadRequestEvent;
            }
        }

        /// <summary>
        /// MonoBehaviour method used to unlink OnRequestSuccess from API
        /// </summary>
        private void OnDisable()
        {
            if (null != Api.Instance)
            {
                Api.Instance.OnSuccess -= OnRequestSuccess;
                Api.Instance.OnBadRequest -= OnBadRequestEvent;
            }
        }

        /// <summary>
        /// Send a request to API
        /// </summary>
        /// <param name="pUrl"></param>
        /// <param name="pMethod"></param>
        /// <param name="pAuthenticationRequired"></param>
        private void SendRequest(string pUrl, Api.Methods pMethod, bool pAuthenticationRequired)
		{
			HTTPRequest lRequest = Api.Instance.SetUpRequest(pUrl, pMethod, pAuthenticationRequired);
            lRequest.Timeout = TimeSpan.FromSeconds(5);

            Assert.IsNotNull(lRequest, "[AppParamAPICalls] SendRequest(), lRequest is null.");

			StartCoroutine(Api.Instance.SendRequest(lRequest));
		}



        /// <summary>
        /// Callback invoke when request from API was done successfully
        /// </summary>
        /// <param name="pCurrentUrl">string</param>
        /// <param name="pMethod">HTTPMethods</param>
        /// <param name="pData">string</param>
        private void OnRequestSuccess(string pCurrentUrl, HTTPMethods pMethod, string pData)
        {
            if (true == pCurrentUrl.Equals(_getAppParamManagerList))
            {
                Assert.IsNotNull(_appParamMain, "[AppParamAPICalls] OnRequestSuccess(), _appParamMain is null or empty.");

                _appParamMain.OnGetAppParamManagerList(pData);
            }
        }

        /// <summary>
		/// Callback invoke when request from API was done successfully
		/// </summary>
		/// <param name="pCurrentUrl">Url used</param>
		/// <param name="pData">Data received</param>
        private void OnBadRequestEvent(string pCurrentUrl, string pData)
        {
			#if UNITY_EDITOR && DEVELOPMENT_BUILD
		        Debug.LogFormat("<color=red>[AppParamAPICalls]</color> OnBadRequestEvent(): {0}", pData);
			#endif
        }

		#endregion
    }
}
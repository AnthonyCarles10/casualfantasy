﻿using Sportfaction.CasualFantasy.Events;
using Sportfaction.CasualFantasy.Manager.Profile;

using Newtonsoft.Json.Linq;
using UnityEngine.Assertions;
using UnityEngine;
using Newtonsoft.Json;
using Sportfaction.CasualFantasy.PVP.Gameplay.Matchs;

using SportFaction.CasualFootballEngine.TeamService.Enum;
using SFManager = SportFaction.CasualFootballEngine.ManagerService.Model.Manager;
using Sportfaction.CasualFantasy.PVP.Gameplay.Matchs;



namespace Sportfaction.CasualFantasy.Squad
{
    public class SquadMain : MonoBehaviour
    {
		#region Variables

		[Header("Data")]

		[SerializeField, Tooltip("Scriptable object contains data about squad players")]
		public	SquadPlayersData	SquadPlayersData;

		[SerializeField, Tooltip("Scriptable object contains infos about match UI")]
		public	PVPData				PvpData;

        [Header("Links")]

        [SerializeField, Tooltip("SquadAPICalls")]
        private SquadAPICalls _squadAPICalls;

		#endregion

		#region Events

        [Header("Game Events")]

        [SerializeField]
        private GameEvent _onGetSquadPlayers = null;

        [SerializeField]
        private GameEvent _onGetTeamId = null;

        [SerializeField]
        private GameEvent _onCreateOrUpdateLineUp = null;

        [SerializeField]
        private GameEvent _onEmptyTeam = null;

		#endregion


        #region API Calls

        // public void GetSquadCollection() { _squadAPICalls.GetSquadCollection(); }
        // public void GetCategories() { _squadAPICalls.GetCategories(); }
        public void GetTeam() { _squadAPICalls.GetTeam(); }

        public void GetSquadPlayers(int pTeamId, string pComposition = "442f") {
            if (pTeamId != -1)
            {
                _squadAPICalls.GetSquadPlayers(pTeamId, pComposition);
            }
        
        }

        public void CreateOrUpdateLineUp(LineUp pLineUp) { _squadAPICalls.CreateOrUpdateLineUp(pLineUp); }

        public void APIAddEvents() { _squadAPICalls.AddEvents(); }

        #endregion


        #region Public Methods

        /// <summary>
        /// Get Collection Player by Id
        /// Event: _onGetCollectionPlayer
        /// </summary>
        /// <param name="pJson">Squad players JSON</param>
        public void GetSquadPlayers(string pJson)
        {
			Assert.IsNotNull(SquadPlayersData, "[SquadMain] GetSquadPlayers(), _squadPlayersData is null.");

            SquadPlayersData.Init(pJson);

			if (null != _onGetSquadPlayers)
			{
				_onGetSquadPlayers.Invoke();
			}

             MatchEngine.Instance.Engine.LoadSquadTeamA(pJson);

        }


        /// <summary>
        /// Get Team Id
        /// Event: _onGetTeamId
        /// </summary>
        /// <param name="pJson">Team id JSON</param>
        public void GetTeamId(string pJson)
        {
            
            if("false" != pJson)
            {
                JToken team = JToken.Parse(pJson);

                Debug.Log("ManagerId = " + UserData.Instance.Profile.Id);

				Assert.IsNotNull(PvpData, "[SquadMain] GetTeamId(), _pvpData is null.");

                PvpData.TeamId = team.Value<int>("id");


                MatchEngine.Instance.InitEngine();

                SFManager lManagerTeamA = new SFManager();
                lManagerTeamA.Id = UserData.Instance.Profile.Id.ToString();
                lManagerTeamA.Name = UserData.Instance.Profile.Pseudo;
                lManagerTeamA.TeamId = PvpData.TeamId.ToString();
                lManagerTeamA.Team = eTeam.TEAM_A.ToString();

                MatchEngine.Instance.Engine.ManagerMain.SetManagerA(lManagerTeamA);

                Debug.Log("TeamId = " + team.Value<int>("id"));

                _onGetTeamId.Invoke();
            }
            else
            {
                Debug.Log("Empty Team");
                _onEmptyTeam.Invoke();
            }
        }

        /// <summary>
        /// Create Or Update LineUp
        /// Event: _onCreateOrUpdateLineUp
        /// <param name="pJson">Lineup JSON</param>
        /// </summary>
        public void CreateOrUpdateLineUp(string pJson)
        {
            JToken team = JToken.Parse(pJson);

			Assert.IsNotNull(PvpData, "[SquadMain] CreateOrUpdateLineUp(), _pvpData is null.");

            PvpData.TeamId = team.Value<int>("teamId");

			if (null != _onCreateOrUpdateLineUp)
			{
				_onCreateOrUpdateLineUp.Invoke();
			}

            GetSquadPlayers(PvpData.TeamId);

      
     
        }

  
        /// <summary>
        /// Persist UserData to PlayersPref
        /// </summary>
        public void PersistSquadData()
        {
            PlayerPrefs.SetString("squad", JsonConvert.SerializeObject(SquadPlayersData));
        }

        /// <summary>
        /// Load UserData from PlayersPref
        /// </summary>
        public void LoadSquadData()
        {
            string pJson = PlayerPrefs.GetString("squad");

            if(pJson != null)
            {
                SquadPlayersData.Init(pJson);
            }
            

            Debug.Log("_squadPlayersData = " + SquadPlayersData.SquadPlayers.Count);
        }

        #endregion

        void Start()
        {
            //LoadSquadData();
        }
    }
}
 
﻿using Sportfaction.CasualFantasy.Manager.Profile;
using Sportfaction.CasualFantasy.Services;

using BestHTTP;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using System;

namespace Sportfaction.CasualFantasy.Squad
{
    /// <summary>
    /// Description: 
    /// Author: Antoine de Lacèze-Murel
    /// </summary>
    public sealed class SquadAPICalls : MonoBehaviour
    {

        #region Fields

        [Header("Links")]

        [SerializeField, Tooltip("SquadMain")]
        private SquadMain _squadMain = null;


        private string _getSquadPlayersURL = "";

        private string _createOrUpdateLineUp = "";

        private string _getTeamURL = "";

        #endregion


        #region Public Methods

        /// <summary>
        /// Get Squad Collection
        /// </summary>
        //public void GetSquadCollection()
        //{
        //    if (null != Api.Instance)
        //    {
        //        string lUrl = Api.Instance.GenerateUrl(ApiConstants.GetSquadCollection + "/" + Store.Instance.PVPData.TeamId);

        //        Assert.IsFalse(string.IsNullOrEmpty(lUrl), "[SquadAPICalls] GetSquadCollection(), lUrl is null or empty.");

        //        HTTPRequest lGetSquadCollection = Api.Instance.SetUpRequest(lUrl, Api.Methods.GET, false);

        //        StartCoroutine(Api.Instance.SendRequest(lGetSquadCollection, 5));
        //    }
        //}

        /// <summary>
        /// Get Categories
        /// </summary>
        //public void GetCategories()
        //{
        //    if (null != Api.Instance)
        //    {
        //        string lUrl = Api.Instance.GenerateUrl(ApiConstants.GetPVPCategories);

        //        Assert.IsFalse(string.IsNullOrEmpty(lUrl), "[SquadAPICalls] GetCategories(), lUrl is null or empty.");

        //        HTTPRequest lGetCategories = Api.Instance.SetUpRequest(lUrl, Api.Methods.GET, false);

        //        StartCoroutine(Api.Instance.SendRequest(lGetCategories, 5));
        //    }
        //}

        /// <summary>
        /// Get Team
        /// </summary>
        public void GetTeam()
        {
            if (null != Api.Instance)
            {
				Assert.IsNotNull(UserData.Instance, "[SquadAPICalls] GetTeam(), UserData.Instance is null.");

                _getTeamURL = Api.Instance.GenerateUrl(ApiConstants.GetTeam + "/" + UserData.Instance.Profile.Id);

                Assert.IsFalse(string.IsNullOrEmpty(_getTeamURL), "[SquadAPICalls] GetTeam(), lUrl is null or empty.");

                HTTPRequest lRequest = Api.Instance.SetUpRequest(_getTeamURL, Api.Methods.GET, false);

                lRequest.Timeout = TimeSpan.FromSeconds(5);

                StartCoroutine(Api.Instance.SendRequest(lRequest));
            }
        }


        public void GetSquadPlayers(int pTeamId, string pComposition)
        {
            if (null != Api.Instance)
            {
                Dictionary<string, string> lQueryParams = new Dictionary<string, string>()
                {
                    { "composition", pComposition }
                };

                _getSquadPlayersURL = Api.Instance.GenerateUrl(ApiConstants.GetSquadPlayersURL + "/" + pTeamId, lQueryParams);

                Assert.IsFalse(string.IsNullOrEmpty(_getSquadPlayersURL), "[SquadAPICalls] GetSquadPlayers(), lUrl is null or empty.");

                HTTPRequest lRequest = Api.Instance.SetUpRequest(_getSquadPlayersURL, Api.Methods.GET, false);

                lRequest.Timeout = TimeSpan.FromSeconds(5);

                StartCoroutine(Api.Instance.SendRequest(lRequest));
            }
        }

        public void CreateOrUpdateLineUp(LineUp pLineUp)
        {

            if (null != Api.Instance)
            {
                _createOrUpdateLineUp = Api.Instance.GenerateUrl(ApiConstants.CreateOrUpdateLineUp);

                Assert.IsFalse(string.IsNullOrEmpty(_createOrUpdateLineUp), "[SquadAPICalls] CreateOrUpdateLineUp(), lUrl is null or empty.");


                object body = new
                {
                    teamName = pLineUp.TeamName,
                    slot = pLineUp.Slot,
                    // players = JsonConvert.SerializeObject(pLineUp.Players)
                    players = pLineUp.Players.ToArray()
                };

                Debug.Log(body.ToString());

                HTTPRequest lRequest = Api.Instance.SetUpRequest(_createOrUpdateLineUp, true, body);

                lRequest.Timeout = TimeSpan.FromSeconds(5);

                StartCoroutine(Api.Instance.SendRequest(lRequest));
            } 
        }

        public void AddEvents()
        {
            if (null != Api.Instance)
            {
                Api.Instance.OnSuccess += OnRequestSuccess;
            }
        }

        #endregion



        #region Private Methods


        /// <summary>
        /// MonoBehaviour method used to link OnRequestSuccess to API
        /// </summary>
        private void OnEnable()
        {
            if (null != Api.Instance)
            {
                Api.Instance.OnSuccess += OnRequestSuccess;
            }
        }

        /// <summary>
        /// MonoBehaviour method used to unlink OnRequestSuccess from API
        /// </summary>
        private void OnDisable()
        {
            if (null != Api.Instance)
            {
                Api.Instance.OnSuccess -= OnRequestSuccess;
            }
        }

        /// <summary>
        /// Callback invoke when request from API was done successfully
        /// </summary>
        /// <param name="pCurrentUrl">string</param>
        /// <param name="pMethod">HTTPMethods</param>
        /// <param name="pData">string</param>
        private void OnRequestSuccess(string pCurrentUrl, HTTPMethods pMethod, string pData)
        {
            //if (true == pCurrentUrl.Contains(ApiConstants.GetSquadCollection + "/" + Store.Instance.PVPData.TeamId))
            //{
            //    _squadMain.TransformJsonToPlayersAndChallengesList(pData);

            //}

            if (true == pCurrentUrl.Equals(_getSquadPlayersURL))
            {
                _squadMain.GetSquadPlayers(pData); 
            }
            else if (true == pCurrentUrl.Equals(_createOrUpdateLineUp))
            {
                _squadMain.CreateOrUpdateLineUp(pData);
            } 
            else if (true == pCurrentUrl.Equals(_getTeamURL))
            {
                _squadMain.GetTeamId(pData);
            }


        }

        /// <summary>
        /// Callback invoke when request from API was done successfully
        /// </summary>
        /// <param name="pCurrentUrl">Url used</param>
        /// <param name="pData">Data received</param>
        private void OnBadRequestEvent(string pCurrentUrl, string pData)
        {
            if (true == pCurrentUrl.Equals(_createOrUpdateLineUp))
            {
                Debug.LogError(pData);
            }
        }

        #endregion
    }
}

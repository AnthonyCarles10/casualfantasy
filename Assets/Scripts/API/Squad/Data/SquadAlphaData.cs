﻿using System.Collections.Generic;
using UnityEngine;

namespace Sportfaction.CasualFantasy.Squad
{
    [CreateAssetMenu(fileName = "New Squad Alpha Data", menuName = "CasualFantasy/Data/SquadAlpha")]
    public sealed class SquadAlphaData : ScriptableObject
    {
        #region Properties

        public List<SquadAlphaPlayerData> Players { get { return _players; } }

        public int Level { get { return _level; } }

        #endregion

        #region Fields

        [SerializeField, Tooltip("_players")]
        private List<SquadAlphaPlayerData> _players;

        [SerializeField, Tooltip("level")]
        private int _level;


        #endregion


    }
}

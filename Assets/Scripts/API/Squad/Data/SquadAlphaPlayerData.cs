﻿using UnityEngine;

namespace Sportfaction.CasualFantasy.Squad
{
    [System.Serializable]
    public sealed class SquadAlphaPlayerData
    {
        #region Properties

        public int id { get { return _id; } set { _id = value; } }

        public string position { get { return _position; } }

        //public string composition { get { return _composition; } }

        #endregion

        #region Fields

        [SerializeField, Tooltip("id")]
        private int _id;

        [SerializeField, Tooltip("position")]
        private string _position;

        //[SerializeField, Tooltip("composition")]
        //private string _composition;


        #endregion


    }
}

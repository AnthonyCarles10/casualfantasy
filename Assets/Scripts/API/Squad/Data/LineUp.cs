﻿using System.Collections.Generic;
using UnityEngine;

namespace Sportfaction.CasualFantasy.Squad
{
    [System.Serializable]
    public sealed class LineUp
    {
        #region Properties


        public List<LineUpPlayer> Players { get { return _players; } set { _players = value; } }

        public int Slot { get { return _slot; } set { _slot = value; } }

        public string TeamName { get { return _teamName; } set { _teamName = value; } }


        #endregion

        #region Fields

        [SerializeField, Tooltip("_lineUpPlayer")]
        private List<LineUpPlayer> _players = new List<LineUpPlayer>();

        [SerializeField, Tooltip("_slot")]
        private int _slot;

        [SerializeField, Tooltip("_teamName")]
        private string _teamName;

        #endregion


        #region Public Methods

        public void ConvertToJson(string pJson)
        {


        }

        #endregion
    }
}

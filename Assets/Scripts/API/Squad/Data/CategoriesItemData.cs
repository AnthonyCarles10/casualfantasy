﻿using Newtonsoft.Json.Linq;
using UnityEngine;

namespace Sportfaction.CasualFantasy.Squad
{
    [System.Serializable]
    public sealed class CategoriesItemData
    {
        #region Properties

        public string Name { get { return _name; } }

        public string Skill1 { get { return _skill1; } }

        public string Skill2 { get { return _skill2; } }

        public string Skill3 { get { return _skill3; } }

        public string Skill4 { get { return _skill4; } }

        public string Skill5 { get { return _skill5; } }

        #endregion

        #region Fields

        [SerializeField, Tooltip("_name")]
        private string _name;

        [SerializeField, Tooltip("_skill1")]
        private string _skill1;

        [SerializeField, Tooltip("_skill2")]
        private string _skill2;

        [SerializeField, Tooltip("_skill3")]
        private string _skill3;

        [SerializeField, Tooltip("_skill4")]
        private string _skill4;

        [SerializeField, Tooltip("_skill5")]
        private string _skill5;

        #endregion


        #region Public Methods

        /// <summary>
        /// Initialize Data with json response from Api
        /// </summary>
        /// <param name="pToken">Specific JsonToken to use</param>
        public void Init(JToken pToken)
        {
            _name = pToken.Value<string>("name");
            _skill1 = pToken.Value<string>("skill1");
            _skill2 = pToken.Value<string>("skill2");
            _skill3 = pToken.Value<string>("skill3");
            _skill4 = pToken.Value<string>("skill4");
            _skill5 = pToken.Value<string>("skill5");
        }

        #endregion
    }
}

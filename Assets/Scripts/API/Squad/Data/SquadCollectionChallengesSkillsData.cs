﻿using Newtonsoft.Json.Linq;
using UnityEngine;

namespace Sportfaction.CasualFantasy.Squad
{
    [System.Serializable]
    public sealed class SquadCollectionChallengesSkillsData
    {
        #region Properties

        public string Name { get { return _name; } }

        #endregion

        #region Fields

        [SerializeField, Tooltip("_name")]
        private string _name;

        #endregion


        #region Public Methods

        /// <summary>
        /// Initialize Data with json response from Api
        /// </summary>
        /// <param name="pToken">Specific JsonToken to use</param>
        public void Init(JToken pToken)
        {
            _name = pToken.Value<string>("name");
        }

        #endregion
    }
}

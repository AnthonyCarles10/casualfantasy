﻿using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using UnityEngine;

namespace Sportfaction.CasualFantasy.Squad
{
    [System.Serializable]
    public sealed class CategoriesData
    {
        #region Properties

        public List<CategoriesItemData> Categories { get { return _categories; } }

        #endregion

        #region Fields

        [SerializeField, Tooltip("_categories")]
        private List<CategoriesItemData> _categories = new List<CategoriesItemData>();

        #endregion

        #region Public Methods

        /// <summary>
        /// Initialize Data with json response from Api
        /// </summary>
        /// <param name="pJson">json response</param>
        public void Init(string pJson)
        {
            
            pJson = "{\"items\":" + pJson + "}";
            JToken lJToken = JToken.Parse(pJson);

            JArray lCategoriesArray = lJToken.Value<JArray>("items");

            foreach (JToken lCategory in lCategoriesArray)
            {
                CategoriesItemData lNewCategory = new CategoriesItemData();
                lNewCategory.Init(lCategory);

                _categories.Add(lNewCategory);
            }

        }

        #endregion
    }
}

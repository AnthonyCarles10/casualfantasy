﻿using Sportfaction.CasualFantasy.Players;

using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using UnityEngine;

namespace Sportfaction.CasualFantasy.Squad
{
    [System.Serializable]
    public sealed class SquadCollectionData
    {
        #region Properties

        public List<PlayersItem> Players { get { return _players; } }

        public List<SquadCollectionChallengesData> Challenges { get { return _challenges; } }

        #endregion

        #region Fields

        [SerializeField, Tooltip("_players")]
        private List<PlayersItem> _players = new List<PlayersItem>();

        [SerializeField, Tooltip("_challenges")]
        private List<SquadCollectionChallengesData> _challenges = new List<SquadCollectionChallengesData>();

        #endregion

        #region Public Methods

        /// <summary>
        /// Initialize Data with json response from Api
        /// </summary>
        /// <param name="pJson">json response</param>
        public void Init(string pJson)
        {
            JToken lJToken = JToken.Parse(pJson);

            JArray lPlayersArray = lJToken.Value<JArray>("players");

            foreach (JToken lPlayer in lPlayersArray)
            {
                PlayersItem lNewPlayer = new PlayersItem();
                lNewPlayer.Init(lPlayer);

                _players.Add(lNewPlayer);
            }


            //JArray lChallengesArray = lJToken.Value<JArray>("challenges");

            //foreach (JToken lChallenge in lChallengesArray)
            //{
            //    SquadCollectionChallengesData lNewChallenge = new SquadCollectionChallengesData();
            //    lNewChallenge.Init(lChallenge);

            //    _challenges.Add(lNewChallenge);
            //}

        }

        #endregion
    }
}

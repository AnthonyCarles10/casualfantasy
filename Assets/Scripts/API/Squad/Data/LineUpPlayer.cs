﻿using Newtonsoft.Json;
using UnityEngine;

namespace Sportfaction.CasualFantasy.Squad
{
    [System.Serializable]
    public sealed class LineUpPlayer
    {
        #region Properties

        public string id { get { return _id; } set { _id = value; } }

        [JsonProperty("full_position")]
        public string position { get { return _position; } set { _position = value; } }


        #endregion

        #region Fields

        [SerializeField, Tooltip("_id")]
        private string _id;

        [SerializeField, Tooltip("_position")]
        private string _position;

        #endregion


        #region Public Methods


        #endregion
    }
}

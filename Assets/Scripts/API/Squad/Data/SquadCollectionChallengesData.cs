﻿using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using UnityEngine;

namespace Sportfaction.CasualFantasy.Squad
{
    [System.Serializable]
    public sealed class SquadCollectionChallengesData
    {
        #region Properties

        public string Name { get { return _name; } }

        public List<SquadCollectionChallengesSkillsData> Skills{ get { return _skills; } }

        #endregion

        #region Fields

        [SerializeField, Tooltip("_name")]
        private string _name;

        [SerializeField, Tooltip("_skills")]
        private List<SquadCollectionChallengesSkillsData> _skills = new List<SquadCollectionChallengesSkillsData>();

        #endregion


        #region Public Methods

        /// <summary>
        /// Initialize Data with json response from Api
        /// </summary>
        /// <param name="pToken">Specific JsonToken to use</param>
        public void Init(JToken pToken)
        {
            _name = pToken.Value<string>("name");

            JArray lSkillsArray = pToken.Value<JArray>("skills");

            foreach (JToken lSkill in lSkillsArray)
            {
                SquadCollectionChallengesSkillsData lNewSkill = new SquadCollectionChallengesSkillsData();
                lNewSkill.Init(lSkill);

                _skills.Add(lNewSkill);
            }
          
        }

        #endregion
    }
}

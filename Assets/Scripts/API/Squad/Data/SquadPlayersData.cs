﻿using Sportfaction.CasualFantasy.Players;

using System.Collections.Generic;
using UnityEngine;

namespace Sportfaction.CasualFantasy.Squad
{
	/// <summary>
	/// Description: Class contains data about squad players
	/// Author: Rémi Carreira
	/// </summary>
	[CreateAssetMenu(fileName = "Store-SquadPlayers", menuName = "CasualFantasy/Data/Store/SquadPlayers")]
	public class SquadPlayersData : ScriptableObject
	{
		#region Properties

		// Property used to get the list of players in the squad
		public	List<PlayersItem>	SquadPlayers	{ get { return _squadPlayers; } }

		#endregion

		#region Variables

		[SerializeField, Tooltip("List of players in the squad")]
        private List<PlayersItem>	_squadPlayers		=	null;

		// Data about the squad collection
		private	SquadCollectionData	_squadCollection	=	null;

		#endregion

		#region Public Methods

		/// <summary>
		/// Initialize squad players data
		/// </summary>
		/// <param name="pJson">Json used for initialization</param>
		public	void	Init(string pJson)
		{
			_squadCollection = new SquadCollectionData();
			if (null != _squadCollection)
			{
				_squadCollection.Init(pJson);

				_squadPlayers	=	_squadCollection.Players;
			}
		}

		#endregion
	}
}
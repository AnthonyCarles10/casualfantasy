using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

namespace Sportfaction.CasualFantasy.Loot
{
    /// <summary>
    /// Description: Loot Box Data
    /// Author: Antoine de Lachèze-Murel
    /// </summary>
    public sealed class LootTimersDataBox
    {
        #region Properties

        public string Name { get { return _name; } }

        public int DateConsumptionTimestamp { get { return _dateConsumptionTimestamp; } }

        public int NextOpenTimestamp { get { return _nextOpenTimestamp; } }

        #endregion

        #region Fields

        [SerializeField]
        private string _name;

        [SerializeField]
        private int _dateConsumptionTimestamp;

        [SerializeField]
        private int _nextOpenTimestamp = 0;


        #endregion

        #region Public Methods

        /// <summary>
        /// Initialize Loot data box from a Json
        /// </summary>
        /// <param name="pJson">Specific json to use</param>
        public void Init(JToken pToken)
        {

            _name = pToken.Value<string>("key_name");

            _dateConsumptionTimestamp = pToken.Value<int>("date_consumption_timestamp");

            _nextOpenTimestamp = pToken.Value<int>("next_open_timestamp");
        }

        #endregion
    }
}
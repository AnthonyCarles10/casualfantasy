using Newtonsoft.Json.Linq;
using Sportfaction.CasualFantasy.Meta;
using Sportfaction.CasualFantasy.Players;
using Sportfaction.CasualFantasy.Services;
using SportFaction.CasualFootballEngine.PlayerService.Model;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

namespace Sportfaction.CasualFantasy.Loot
{
    /// <summary>
    /// Description: Class contains all loot datat
    /// Author: Antoine de Lachèze-Murel
    /// </summary>
	[CreateAssetMenu(fileName = "Store-Loot", menuName = "CasualFantasy/Data/Store/Loot")]
    public sealed class LootData : ScriptableObject
    {
        #region Properties

        public string Type { get { return _type; } }

        public bool IsConsumed { get { return _isConsumed; } }

        public List<PlayersItem> GoalKeepers { get { return _goalKeepers; } }

        public List<PlayersItem> FullBacks { get { return _fullBacks; } }

        public List<PlayersItem> MidFields { get { return _midFields; } }

        public List<PlayersItem> Forwards { get { return _forwards; } }

        public int Hard { get { return _hard; } }

        public int Fragment { get { return _fragment; } }

        public PlayersItem Player { get { return _player; } }

        public List<LootData> BigBox { get { return _bigBox; } }

        public List<LootData> MediumBox { get { return _mediumBox; } }

        public List<LootData> SmallBox { get { return _smallBox; } }


        #endregion

        #region Fields

        [SerializeField]
        private string _type = "";

        [SerializeField]
        private bool _isConsumed;

        [SerializeField]
        private List<PlayersItem> _goalKeepers = new List<PlayersItem>();

        [SerializeField]
        private List<PlayersItem> _fullBacks = new List<PlayersItem>();

        [SerializeField]
        private List<PlayersItem> _midFields = new List<PlayersItem>();

        [SerializeField]
        private List<PlayersItem> _forwards = new List<PlayersItem>();

        [SerializeField]
        private int _hard;

        [SerializeField]
        private int _fragment;

        [SerializeField]
        private PlayersItem _player;

        [SerializeField]
        private List<LootData> _bigBox = new List<LootData>();

        [SerializeField]
        private List<LootData> _mediumBox = new List<LootData>();

        [SerializeField]
        private List<LootData> _smallBox = new List<LootData>();

        #endregion

        #region Public Methods

        /// <summary>
        /// Initialize Loot data from a Json
        /// </summary>
        /// <param name="pJson">Specific json to use</param>
        public void Parse(string pJson, string pType)
        {

            Assert.IsFalse(string.IsNullOrEmpty(pJson), "LootData: Init(), pJson is null.");

            Debug.Log(pJson);
            JToken lJToken = JToken.Parse(pJson);

            _type = pType;

            _isConsumed = lJToken.SafeValue<int>("is_consumed") == 0 ? false : true;

            JToken lContent = lJToken.Value<JToken>("content");

            _hard = lContent.SafeValue<int>("hard");

            _fragment = lContent.SafeValue<int>("fragment");


            if(lContent.SafeValue<JToken>("player") != null && true== lContent.SafeValue<JToken>("player").HasValues)
            {
              
                JToken lPlayerToken = lContent.SafeValue<JToken>("player");

                Debug.Log(lPlayerToken.ToString());
                _player = new PlayersItem();
                _player.Init(lPlayerToken);

                Debug.Log("ADLM = " + _player.Profile.Name);
            }
     
            //int lIsCycleLeaderboard = lJToken.SafeValue<int>("is_cycle_leaderboard");


            if (null != lContent.Value<JArray>("big_box") || null != lContent.Value<JArray>("medium_box") || null != lContent.Value<JArray>("small_box"))
            {

                JArray lBigBox = lContent.Value<JArray>("big_box");

                if (null != lContent.Value<JArray>("big_box") && true == lBigBox.HasValues)
                {
                    foreach (JToken item in lBigBox)
                    {
                        LootData lNew = new LootData();
                        lNew.ParseBox(item, "big");

                        _bigBox.Add(lNew);
                    }
                }


                JArray lMediumBox = lContent.Value<JArray>("medium_box");

                if (null != lContent.Value<JArray>("medium_box") && true == lMediumBox.HasValues)
                { 
                    foreach (JToken item in lMediumBox)
                    {
                        LootData lNew = new LootData();
                        lNew.ParseBox(item, "medium");

                        _mediumBox.Add(lNew);
                    }
                }

                JArray lSmallBox = lContent.Value<JArray>("small_box");

                if (null != lContent.Value<JArray>("small_box") && true == lSmallBox.HasValues)
                {
                    foreach (JToken item in lSmallBox)
                    {
                        LootData lNew = new LootData();
                        lNew.ParseBox(item, "small");

                        _smallBox.Add(lNew);
                    }
                }
            }
            else
            {
                InitPlayers(lContent);
            }

        }

        #endregion

        private void InitPlayers(JToken pContent)
        {
            JToken lPlayers = pContent.Value<JToken>("players");

            JArray lGoalKeepersArray = lPlayers.Value<JArray>("G");

            foreach (JToken item in lGoalKeepersArray)
            {
                PlayersItem lNew = new PlayersItem();
                lNew.Init(item);

                if (false == MetaData.Instance.PlayersMain.PlayersManager.Ids.Contains(lNew.Profile.PlayerId))
                {
                    _goalKeepers.Add(lNew);
                }
            }

            JArray lFullBackArray = lPlayers.Value<JArray>("D");

            foreach (JToken item in lFullBackArray)
            {
                PlayersItem lNew = new PlayersItem();
                lNew.Init(item);

                if (false == MetaData.Instance.PlayersMain.PlayersManager.Ids.Contains(lNew.Profile.PlayerId))
                {
                    _fullBacks.Add(lNew);
                }
            }

            JArray lMidFieldArray = lPlayers.Value<JArray>("M");

            foreach (JToken item in lMidFieldArray)
            {
                PlayersItem lNew = new PlayersItem();
                lNew.Init(item);

                if (false == MetaData.Instance.PlayersMain.PlayersManager.Ids.Contains(lNew.Profile.PlayerId))
                {
                    _midFields.Add(lNew);
                }
            }

            JArray lForwardArray = lPlayers.Value<JArray>("A");

            foreach (JToken item in lForwardArray)
            {
                PlayersItem lNew = new PlayersItem();
                lNew.Init(item);

                if (false == MetaData.Instance.PlayersMain.PlayersManager.Ids.Contains(lNew.Profile.PlayerId))
                {
                    _forwards.Add(lNew);
                }
            }

        }

        private  void ParseBox(JToken pToken, string pType)
        {
            _type = pType;

            _hard = pToken.SafeValue<int>("hard");

            _fragment = pToken.SafeValue<int>("fragment");

            InitPlayers(pToken);

        }
    }
}
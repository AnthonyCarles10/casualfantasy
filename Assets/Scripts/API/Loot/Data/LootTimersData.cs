using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

namespace Sportfaction.CasualFantasy.Loot
{
    /// <summary>
    /// Description: Class contains all loot datat
    /// Author: Antoine de Lachèze-Murel
    /// </summary>
	[CreateAssetMenu(fileName = "Store-LootTimers", menuName = "CasualFantasy/Data/Store/LootTimers")]
    public sealed class LootTimersData : ScriptableObject
    {
        #region Properties

        public List<LootTimersDataBox> Box { get { return _box; } }

        public int CurrentTimestamp { get { return _currentTimestamp; } }

        #endregion

        #region Fields

        [SerializeField, Tooltip("LootDataBox")]
        private List<LootTimersDataBox> _box =  new List<LootTimersDataBox>();


        [SerializeField, Tooltip("LootDataBox")]
        private int  _currentTimestamp;


        #endregion

        #region Public Methods

        /// <summary>
        /// Initialize Loot data from a Json
        /// </summary>
        /// <param name="pJson">Specific json to use</param>
        public void Parse(string pJson)
        {
            Assert.IsFalse(string.IsNullOrEmpty(pJson), "LootData: Init(), pJson is null.");

            JToken lJToken = JToken.Parse(pJson);

            _currentTimestamp = lJToken.Value<int>("current_timestamp");

            JArray lLootDataArray = lJToken.Value<JArray>("loot_timers");

            _box.Clear();

            foreach (JToken loot in lLootDataArray)
            {
                LootTimersDataBox lLootData = new LootTimersDataBox();

                lLootData.Init(loot);

                _box.Add(lLootData);
            }
        }

        #endregion
    }
}
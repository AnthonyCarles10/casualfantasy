using Sportfaction.CasualFantasy.Services;

using BestHTTP;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using System;

namespace Sportfaction.CasualFantasy.Loot
{
	/// <summary>
	/// Description: 
	/// Author: Antoine de Lach�ze-Murel
	/// </summary>
	public sealed class LootAPICalls : MonoBehaviour
	{
        #region Fields

        [SerializeField, Tooltip("LootMain")]
        private LootMain _lootMain = null;

        private string _askManagerLoot = "";

        private string _askManagerLootType = "";

        private string _getLootTimers = "";

        private string _preloadLoot = "";

        private string _unlockRewardArenaSeasonURL = "";

        #endregion

        #region Public Methods

        /// <summary>
        /// Open a Loot box
        /// </summary>
        public void AskManagerLoot(string pType, int pNumber)
		{
			if (null != Api.Instance)
			{
                _askManagerLootType = pType;

                Dictionary<string, string> lQueryParams = new Dictionary<string, string>();
                if (pType == "arenacup")
                {
                    lQueryParams = new Dictionary<string, string>()
                    {
                        { "is_cycle_leaderboard", "1" },
                        { "number", pNumber.ToString() }
                    };
                }

                Debug.Log("ADLM ASK LOOT");
                _askManagerLoot = Api.Instance.GenerateUrl(ApiConstants.AskManagerLoot + "/" + pType, lQueryParams); 

                Assert.IsFalse(string.IsNullOrEmpty(_askManagerLoot), "[LootAPICalls] OpenLoot(), lUrl is null or empty.");

                HTTPRequest lRequest = Api.Instance.SetUpRequest(_askManagerLoot, Api.Methods.GET, true);
                lRequest.Timeout = TimeSpan.FromSeconds(30);

                StartCoroutine(Api.Instance.SendRequest(lRequest));
            }
		}

        /// <summary>
        /// Open a Loot box
        /// </summary>
        public void GetLootTimers()
        {
            if (null != Api.Instance)
            {
                _getLootTimers = Api.Instance.GenerateUrl(ApiConstants.GetLootTimers);

                Assert.IsFalse(string.IsNullOrEmpty(_getLootTimers), "[LootAPICalls] OpenLoot(), lUrl is null or empty.");

                HTTPRequest lRequest = Api.Instance.SetUpRequest(_getLootTimers, Api.Methods.GET, true);
                lRequest.Timeout = TimeSpan.FromSeconds(5);

                StartCoroutine(Api.Instance.SendRequest(lRequest));
            }
        }

        /// <summary>
        /// Preload Loot
        /// </summary>
        public void PreloadLoot()
        {
            if (null != Api.Instance)
            {
                _preloadLoot = Api.Instance.GenerateUrl(ApiConstants.PreloadLoot);

                Assert.IsFalse(string.IsNullOrEmpty(_preloadLoot), "[LootAPICalls] PreloadLoot(), lUrl is null or empty.");

                HTTPRequest lRequest = Api.Instance.SetUpRequest(_preloadLoot, Api.Methods.GET, true);
                lRequest.Timeout = TimeSpan.FromSeconds(5);

                StartCoroutine(Api.Instance.SendRequest(lRequest, true));
            }
        }


        /// <summary>
        /// Unlock reward Arena Season
        /// </summary>
        public void UnlockRewardArenaSeason(int pLevel)
        {
            if (null != Api.Instance)
            {

                Dictionary<string, string> lQueryParams = new Dictionary<string, string>();
       
                lQueryParams = new Dictionary<string, string>()
                {
                    { "level", pLevel.ToString() }
                };

                _unlockRewardArenaSeasonURL = Api.Instance.GenerateUrl(ApiConstants.UnlockRewardArenaSeason, lQueryParams);

                _askManagerLootType = "arenaseason";

                Assert.IsFalse(string.IsNullOrEmpty(_unlockRewardArenaSeasonURL), "[ArenaCupAPICalls] GetRewardsArenaCup(), lUrl is null or empty.");

                HTTPRequest lRequest = Api.Instance.SetUpRequest(_unlockRewardArenaSeasonURL, Api.Methods.GET, true);
                lRequest.Timeout = TimeSpan.FromSeconds(5);

                StartCoroutine(Api.Instance.SendRequest(lRequest));
            }
        }





        public void AddEvents()
		{
			if (null != Api.Instance)
			{
				Api.Instance.OnSuccess += OnRequestSuccess;
			}
		}

		#endregion

		#region Private Methods

		/// <summary>
		/// MonoBehaviour method used to link OnRequestSuccess to API
		/// </summary>
		private void OnEnable()
        {
            if (null != Api.Instance)
            {
                Api.Instance.OnSuccess += OnRequestSuccess;
            }
        }

        /// <summary>
        /// MonoBehaviour method used to unlink OnRequestSuccess from API
        /// </summary>
        private void OnDisable()
        {
            if (null != Api.Instance)
            {
                Api.Instance.OnSuccess -= OnRequestSuccess;
            }
        }

        /// <summary>
        /// Send a request to API
        /// </summary>
        /// <param name="pUrl"></param>
        /// <param name="pMethod"></param>
        /// <param name="pAuthenticationRequired"></param>
        private void SendRequest(string pUrl, Api.Methods pMethod, bool pAuthenticationRequired)
		{
			HTTPRequest lRequest = Api.Instance.SetUpRequest(pUrl, pMethod, pAuthenticationRequired);

			Assert.IsNotNull(lRequest, "[LeaderBoardAPICalls] SendRequest(), lRequest is null.");
            lRequest.Timeout = TimeSpan.FromSeconds(5);

            StartCoroutine(Api.Instance.SendRequest(lRequest));
		}

        /// <summary>
        /// Callback invoke when request from API was done successfully
        /// </summary>
        /// <param name="pCurrentUrl">string</param>
        /// <param name="pMethod">HTTPMethods</param>
        /// <param name="pData">string</param>
        private void OnRequestSuccess(string pCurrentUrl, HTTPMethods pMethod, string pData)
        {
            if (true == pCurrentUrl.Equals(_askManagerLoot) || true == pCurrentUrl.Equals(_unlockRewardArenaSeasonURL))
            {
                Assert.IsNotNull(_lootMain, "[LootAPICalls] OnRequestSuccess(), _lootMain is null or empty.");

                _lootMain.ParseAskManagerLoot(pData, _askManagerLootType);

            }
            else if  (true == pCurrentUrl.Equals(_getLootTimers))
            {
                Assert.IsNotNull(_lootMain, "[LootAPICalls] OnRequestSuccess(), _lootMain is null or empty.");
                _lootMain.ParseLootTimers(pData);
            }
        }

        #endregion
    }
}

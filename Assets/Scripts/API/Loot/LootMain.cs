using Sportfaction.CasualFantasy.Events;
using Sportfaction.CasualFantasy.Services.Notifications;
using Sportfaction.CasualFantasy.Services.Timers;
using UnityEngine;
using UnityEngine.Assertions;

namespace Sportfaction.CasualFantasy.Loot
{
    public class LootMain : MonoBehaviour
    {

        #region Variables

        [Header("API")]
        [SerializeField]
        private LootAPICalls _lootAPICalls;

        [Header("DATA")]
        [SerializeField]
        public LootTimersData LootTimersData;

        [SerializeField]
        public LootData LootData;

        #endregion

        #region Events

        [Header("Game Events")]
        [SerializeField]
        private GameEvent _onGetLootData;
        [SerializeField]
        private GameEvent _onGetLootTimers;

        #endregion

        #region API Calls

        public void PreloadLoot() { _lootAPICalls.PreloadLoot(); }
        public void GetLootTimers() { _lootAPICalls.GetLootTimers(); }
        public void AskManagerLoot(string pType, int pNumber = 0) { _lootAPICalls.AskManagerLoot(pType, pNumber); }
        public void UnlockRewardArenaSeason(int pLevel) { _lootAPICalls.UnlockRewardArenaSeason(pLevel); }

        #endregion

        #region Public Methods
        /// <summary>
        /// Parse json to open a loot box
        /// </summary>
        /// <param name="pData">Json to parse</param>
        public void ParseLootTimers(string pData)
        {
    
            LootTimersData = ScriptableObject.CreateInstance<LootTimersData>();

            LootTimersData.Parse(pData);

            foreach (LootTimersDataBox item in LootTimersData.Box)
            {
                if (item.NextOpenTimestamp != 0 && !string.IsNullOrEmpty(item.Name))
                {

                    LocalTimerData lTimer = LocalTimersManager.Instance.StartTimer(item.Name, item.NextOpenTimestamp.ToString(), LootTimersData.CurrentTimestamp.ToString());

                    // TODO
                    //if (null != NotificationsManager.Instance)
                    //{
                    //    LocalNotificationData lData = NotificationsManager.Instance.Database.GetLocalNotification("medium_box");
                    //    lData.Delay = (long)lTimer.TimeLeft.TotalMilliseconds;
                    //    NotificationsManager.Instance.SendLocalNotification("medium_box");
                    //}
                }
             
            }

            _onGetLootTimers.Invoke();
        }

        /// <summary>
        /// Parse json to open a loot box
        /// </summary>
        /// <param name="pData">Json to parse</param>
        public void ParseAskManagerLoot(string pData, string pType)
        {

            LootData = ScriptableObject.CreateInstance<LootData>();

            LootData.Parse(pData, pType);

            _onGetLootData.Invoke();
        }

        #endregion
    }
}

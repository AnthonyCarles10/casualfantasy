﻿using Sportfaction.CasualFantasy.Services;

using BestHTTP;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using System;

namespace Sportfaction.CasualFantasy.Players
{
    /// <summary>
    /// Description: 
    /// Author: Antoine de Lacèze-Murel
    /// </summary>
    public sealed class PlayersAPICalls : MonoBehaviour
    {

        #region Fields

        [SerializeField, Tooltip("PlayersMain")]
        private PlayersMain _playersMain = null;

        private string _playersId;
        private string _urlGetPlayers;
        private string _urlGetPlayersList;
        private string _urlGetPlayersFantasyList;
        private string _urlGetPlayerLastGame;
        private string _urlGetPlayersManager;


        #endregion


        #region Public Methods

        /// <summary>
        /// Get Players
        /// </summary>
        public void GetPlayers(string pPlayersId)
        {
            if (null != Api.Instance)
            {
                _playersId = pPlayersId;

                _urlGetPlayers = Api.Instance.GenerateUrl(ApiConstants.GetPlayers + "/" + _playersId);

                Assert.IsFalse(string.IsNullOrEmpty(_urlGetPlayers), "[PlayersAPICalls] GetSquadCollection(), lUrl is null or empty.");

                HTTPRequest lRequest = Api.Instance.SetUpRequest(_urlGetPlayers, Api.Methods.GET, true);
                lRequest.Timeout = TimeSpan.FromSeconds(5);

                StartCoroutine(Api.Instance.SendRequest(lRequest));
            }
        }

        /// <summary>
        /// Get Players
        /// </summary>
        public void GetPlayerLastGame(string pPlayerId)
        {
            if (null != Api.Instance)
            {
			
                _urlGetPlayerLastGame = Api.Instance.GenerateUrl(ApiConstants.GetPlayerLastGame + "/" + pPlayerId);

                Assert.IsFalse(string.IsNullOrEmpty(_urlGetPlayerLastGame), "[PlayersAPICalls] GetPlayersLastGame(), lUrl is null or empty.");

                HTTPRequest lRequest = Api.Instance.SetUpRequest(_urlGetPlayerLastGame, Api.Methods.GET, true);
                lRequest.Timeout = TimeSpan.FromSeconds(5);

                StartCoroutine(Api.Instance.SendRequest(lRequest));
            }
        }

        /// <summary>
        /// Get Players
        /// </summary>
        public void GetPlayersList(Dictionary<string, string> pQueryParams)
        {
            if (null != Api.Instance)
            {
                pQueryParams["typeList"] = "minions";
                _urlGetPlayersList = Api.Instance.GenerateUrl(ApiConstants.GetPlayers, pQueryParams);

                Assert.IsFalse(string.IsNullOrEmpty(_urlGetPlayersList), "[PlayersAPICalls] GetPlayersList(), lUrl is null or empty.");

                HTTPRequest lRequest = Api.Instance.SetUpRequest(_urlGetPlayersList, Api.Methods.GET, true);

                lRequest.Timeout = TimeSpan.FromSeconds(5);

                StartCoroutine(Api.Instance.SendRequest(lRequest));
            }
        }

        /// <summary>
        /// Get Players
        /// </summary>
        public void GetPlayersFantasyList(Dictionary<string, string> pQueryParams)
        {
            if (null != Api.Instance)
            {
                pQueryParams["typeList"] = "fantasy";
                _urlGetPlayersFantasyList = Api.Instance.GenerateUrl(ApiConstants.GetPlayers, pQueryParams);

                Assert.IsFalse(string.IsNullOrEmpty(_urlGetPlayersFantasyList), "[PlayersAPICalls] GetPlayersFantasyList(), lUrl is null or empty.");

                HTTPRequest lRequest = Api.Instance.SetUpRequest(_urlGetPlayersFantasyList, Api.Methods.GET, true);

                lRequest.Timeout = TimeSpan.FromSeconds(5);

                StartCoroutine(Api.Instance.SendRequest(lRequest));
            }
        }

        /// <summary>
        /// Get Players
        /// </summary>
        public void GetPlayersManager()
        {
            if (null != Api.Instance)
            {
                _urlGetPlayersManager = Api.Instance.GenerateUrl(ApiConstants.GetPlayersManager);

                Assert.IsFalse(string.IsNullOrEmpty(_urlGetPlayersManager), "[PlayersAPICalls] GetPlayersFantasyList(), lUrl is null or empty.");

                HTTPRequest lRequest = Api.Instance.SetUpRequest(_urlGetPlayersManager, Api.Methods.GET, true);

                lRequest.Timeout = TimeSpan.FromSeconds(5);

                StartCoroutine(Api.Instance.SendRequest(lRequest));
            }
        }



        public void AddEvents()
        {
            if (null != Api.Instance)
            {
                Api.Instance.OnSuccess += OnRequestSuccess;
            }
        }

        #endregion

        #region Private Methods


        /// <summary>
        /// MonoBehaviour method used to link OnRequestSuccess to API
        /// </summary>
        private void OnEnable()
        {
            if (null != Api.Instance)
            {
                Api.Instance.OnSuccess += OnRequestSuccess;
            }
        }

        /// <summary>
        /// MonoBehaviour method used to unlink OnRequestSuccess from API
        /// </summary>
        private void OnDisable()
        {
            if (null != Api.Instance)
            {
                Api.Instance.OnSuccess -= OnRequestSuccess;
            }
        }

        /// <summary>
        /// Callback invoke when request from API was done successfully
        /// </summary>
        /// <param name="pCurrentUrl">string</param>
        /// <param name="pMethod">HTTPMethods</param>
        /// <param name="pData">string</param>
        private void OnRequestSuccess(string pCurrentUrl, HTTPMethods pMethod, string pData)
        {
            if (true == pCurrentUrl.Equals(_urlGetPlayers))
            {
				_playersMain.TransformJsonToPlayers(pData);
            }
            else if (true == pCurrentUrl.Contains("typeList=minions") && true == pCurrentUrl.Contains("api/players"))
            {
				_playersMain.TransformJsonToPlayersList(pData);
            }
            else if (true == pCurrentUrl.Contains("typeList=fantasy") && true == pCurrentUrl.Contains("api/players"))
            {
                _playersMain.ParseToPlayersFantasyList(pData);
            }
            else if (true == pCurrentUrl.Equals(_urlGetPlayerLastGame))
            {
				_playersMain.ParsePlayerLastGame(pData);
            }
            else if(true == pCurrentUrl.Equals(_urlGetPlayersManager))
            {
                _playersMain.ParsePlayersManager(pData);
            }

        }

        #endregion
    }
}
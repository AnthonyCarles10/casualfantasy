﻿using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using UnityEngine;

namespace Sportfaction.CasualFantasy.Players
{
    [CreateAssetMenu(fileName = "Store-PlayerLastGame", menuName = "CasualFantasy/Data/Store/PlayerLastGame")]
    public sealed class PlayerLastGame : ScriptableObject
    {
        #region Properties

        public PlayerLastGameSkills Skills   { get { return _skills; } }

        public PlayerLastGameData   GameData { get { return _gameData; } }

        public PlayerNextGameData   NextGameData { get { return _nextGameData; } }

        public int RealPoint { get { return _realPoint; } }

        public int RealPointGlobal { get { return _realPointGlobal; } }


        #endregion

        #region Fields

        [SerializeField, Tooltip("_skills")]
        private PlayerLastGameSkills _skills =   new PlayerLastGameSkills();

        [SerializeField, Tooltip("_gameData")]
        private PlayerLastGameData _gameData = new PlayerLastGameData();

        [SerializeField, Tooltip("_realPoint")]
        private int _realPoint = 0;

        [SerializeField, Tooltip("_realPointGlobal")]
        private int _realPointGlobal = 0;

        [SerializeField, Tooltip("_nextGameData")]
        private PlayerNextGameData _nextGameData = new PlayerNextGameData();

        #endregion

        #region Public Methods

        /// <summary>
        /// Initializes data with json response from Api
        /// </summary>
        /// <param name="pJson">Player list JSON to parse</param>
        public void Init(string pJson)
        {
            // DATA
            JToken lJToken = JToken.Parse(pJson);

            _realPoint = lJToken.Value<int>("real_point");

            _realPointGlobal = lJToken.Value<int>("real_point_global");

            //SKILLS
            JToken lSkillsToken = lJToken.Value<JToken>("skills");
            _skills.Init(lSkillsToken);

            //GAME DATA
            JToken lGameDataToken = lJToken.Value<JToken>("game_data");
            _gameData.Init(lGameDataToken);

            //NEXT GAME

            if(true == lJToken.Value<JToken>("next_game").HasValues)
            {
                JToken lNextGame = lJToken.Value<JToken>("next_game");
                _nextGameData.Init(lNextGame);
            }
           
        }

        #endregion
    }
}

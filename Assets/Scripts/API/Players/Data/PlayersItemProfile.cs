﻿using Sportfaction.CasualFantasy.Services;

using Newtonsoft.Json.Linq;
using UnityEngine;

namespace Sportfaction.CasualFantasy.Players
{
    [System.Serializable]
    public sealed class PlayersItemProfile
    {
        #region Properties

        public int Id { get { return _id; } }

        public string TeamId { get { return _teamId; } }

        public string PlayerId { get { return _playerId; } }

        public float Value { get { return _value; } }

        public string isOnSale { get { return _isOnsale; } }

        public string Position { get { return _position; } }

        public string SquadPosition { get { return _squadPosition; } set { _squadPosition = value; } }

        public string FullPosition { get { return _fullPosition; } }

        public string TeamName { get { return _teamName; } }

        public string OfficialTeamOptaId { get { return _officialTeamOptaId; } }

        public string OfficialTeamId { get { return _officialTeamId; } }

        public string OfficialTeamCountryId { get { return _officialTeamCountryId; } }

        public string OptaId { get { return _optaId; } }

        public string OldOptaId { get { return _oldOptaId; } }

        public string SdapiId { get { return _sdapiId; } }

        public string Name { get { return _name; } }

        public string NameShort { get { return _nameShort; } }

        public string NameField { get { return _nameField; } }

        public string OldValue { get { return _oldValue; } }

        public string status { get { return _status; } }

        public string Injured { get { return _injured; } }

        public string DateInjury { get { return _dateInjury; } }

        public string DateStartSuspended { get { return _dateStartSuspended; } }

        public string DateEndSuspended { get { return _dateEndSuspended; } }

        public string PseudoTwitter { get { return _pseudoTwitter; } }

        public string BirthDate { get { return _birthDate; } }

        public string Weight { get { return _weight; } }

        public string Height { get { return _height; } }

        public string Number { get { return _number; } }

        public string JoinDate { get { return _joinDate; } }

        public string Country { get { return _country; } }

        public float Finish { get { return _finish; } }

        public float Distribution { get { return _distribution; } }

        public float Goalkeeping { get { return _goalkeeping; } }

        public float Danger { get { return _danger; } }

        public float Percussion { get { return _percussion; } }

        public float Recovery { get { return _recovery; } }

        public float Strictness { get { return _strictness; } }

        public float TechnicalProfile { get { return _technicalProfile; } }

        public float VisionProfile { get { return _visionProfile; } }

        public float ColdBloodProfile { get { return _coldBloodProfile; } }

        public float GoalkeepingProfile { get { return _goalkeeping_profile; } }

        public float PassProfile { get { return _passProfile; } }

        public float SpeedProfile { get { return _speedProfile; } }

        public float ExplosivityProfile { get { return _explosivityProfile; } }

        public float RiskProfile { get { return _riskProfile; } }

        public float TacticalProfile { get { return _tacticalProfile; } }

        public float PositioningProfile { get { return _positioningProfile; } }

        public float StrengthProfile { get { return _strengthProfile; } }

        public float TimingProfile { get { return _timingProfile; } }

        public float ControlSkill { get { return _controlSkill; } }

        public float PassSkill { get { return _passSkill; } }

        public float LongPassSkill { get { return _longPassSkill; } }

        public float VerticalPassSkill { get { return _verticalPassSkill; } }

        public float BackPassSkill { get { return _backPassSkill; } }

        public float ShotSkill { get { return _shotSkill; } }

        public float PenaltySkill { get { return _penaltySkill; } }

        public float DribbleSkill { get { return _dribbleSkill; } }

        public float VisionSkill { get { return _visionSkill; } }

        public float SaveSkill { get { return _saveSkill; } }

        public float FootGameSkill { get { return _footGameSkill; } }

        public float TackleSkill { get { return _tackleSkill; } }

        public float PrecisionProfile { get { return _precisionProfile; } }

        public float FreeKickSkill { get { return _freeKickSkill; } }

        public float FinishSkill { get { return _finishSkill; } }

        public float InterceptionSkill { get { return _interceptionSkill; } }

        public float DefenseHeadedSkill { get { return _defenseHeadedSkill; } }

        public float AttackHeadedSkill { get { return _attackHeadedSkill; } }

        public float CloseEfficiencySkill { get { return _closeEfficiencySkill; } }

        public float FarEfficiencySkill { get { return _farEfficiencySkill; } }

        public float DiveSkill { get { return _diveSkill; } }

        public float CloseSaveSkill { get { return _closeSaveSkill; } }

        public float FarSaveSkill { get { return _farSaveSkill; } }

        public float FreeKickSaveSkill { get { return _freeKickSaveSkill; } }

        public float StandingSaveSkill { get { return _standingSaveSkill; } }

        public float CloseGoalSkill { get { return _closeGoalSkill; } }

        public float FarGoalSkill { get { return _farGoalSkill; } }

        public float HandRelaunchSkill { get { return _handRelaunchSkill; } }

        public float ComingOffSkill { get { return _comingOffSkill; } }

        public float StrengthSkill { get { return _strengthSkill; } }

        public float AttackPositioningSkill { get { return _attackPositioningSkill; } }

        public float DefensePositioningSkill { get { return _defensePositioningSkill; } }

        public float ReactivitySkill { get { return _reactivitySkill; } }

        public float PercussionSkill { get { return _percussionSkill; } }

        public float RigorSkill { get { return _rigorSkill; } }

        public float CornerSkill { get { return _cornerSkill; } }

        public float ParamSkill { get { return _paramSkill; } }

        public float ConcentrationSkill { get { return _concentrationSkill; } }

        public float Technical { get { return _technical; } }

        public float Attack { get { return _attack; } }

        public float FitnessMental { get { return _fitness_mental; } }

        public float Goalkeeper { get { return _goalkeeper; } }

        public float Defense { get { return _defense; } }

        public float Param { get { return _param; } }

        public string Score { get { return _score; } }

        public string Level { get { return _level; } }

        public string NextGame { get { return _nextGame; } }

        public int PvpAttack { get { return _pvpAttack; } }

        public int PvpDefense { get { return _pvpDefense; } }

        public int PvpGoalkeeping { get { return _pvpGoalkeeping; } }

        public int RareLevel { get { return _rareLevel; } }

        public int ClubChampionshipId { get { return _clubChampionshipId; } }

        public int LongShotChallenge { get { return _longShotChallenge; } }

        public int LongSaveChallenge { get { return _longSaveChallenge; } }

        public int TackleChallenge { get { return _tackleChallenge; } }

        public int DribbleChallenge { get { return _dribbleChallenge; } }

        public int PressingChallenge { get { return _pressingChallenge; } }

        public int ShortPassChallenge { get { return _shortPassChallenge; } }

        public int LongPassChallenge { get { return _longPassChallenge; } }

        public int CounterChallenge { get { return _counterChallenge; } }

        public int ReceiveChallenge { get { return _receiveChallenge; } }

        public int InterceptChallenge { get { return _interceptChallenge; } }

        public int GenericPassChallenge { get { return _genericPassChallenge; } }


        #endregion

        #region Fields

        [SerializeField, Tooltip("_id")]
        private int _id;

        [SerializeField, Tooltip("_teamId")]
        private string _teamId;

        [SerializeField, Tooltip("_playerId")]
        private string _playerId = null;

        [SerializeField, Tooltip("_value")]
        private float _value;

        [SerializeField, Tooltip("_isOnsale")]
        private string _isOnsale;

        [SerializeField, Tooltip("_position")]
        private string _position;

        [SerializeField, Tooltip("_squadPosition")]
        private string _squadPosition;

        [SerializeField, Tooltip("_fullPosition")]
        private string _fullPosition;

        [SerializeField, Tooltip("_teamName")]
        private string _teamName;

        [SerializeField, Tooltip("_officialTeamOptaId")]
        private string _officialTeamOptaId;

        [SerializeField, Tooltip("_officialTeamId")]
        private string _officialTeamId;

        [SerializeField, Tooltip("_officialTeamCountryId")]
        private string _officialTeamCountryId;

        [SerializeField, Tooltip("_optaId")]
        private string _optaId;

        [SerializeField, Tooltip("_oldOptaId")]
        private string _oldOptaId;

        [SerializeField, Tooltip("_sdapiId")]
        private string _sdapiId;

        [SerializeField, Tooltip("_name")]
        private string _name;

        [SerializeField, Tooltip("_nameShort")]
        private string _nameShort;

        [SerializeField, Tooltip("_nameField")]
        private string _nameField;

        [SerializeField, Tooltip("_oldValue")]
        private string _oldValue;

        [SerializeField, Tooltip("_status")]
        private string _status;

        [SerializeField, Tooltip("_injured")]
        private string _injured;

        [SerializeField, Tooltip("_dateInjury")]
        private string _dateInjury;

        [SerializeField, Tooltip("_dateStartSuspended")]
        private string _dateStartSuspended;

        [SerializeField, Tooltip("_dateEndSuspended")]
        private string _dateEndSuspended;

        [SerializeField, Tooltip("_pseudoTwitter")]
        private string _pseudoTwitter;

        [SerializeField, Tooltip("_birthDate")]
        private string _birthDate;

        [SerializeField, Tooltip("_weight")]
        private string _weight;

        [SerializeField, Tooltip("_height")]
        private string _height;

        [SerializeField, Tooltip("_number")]
        private string _number;

        [SerializeField, Tooltip("_joinDate")]
        private string _joinDate;

        [SerializeField, Tooltip("_country")]
        private string _country;

        [SerializeField, Tooltip("_finish")]
        private float _finish;

        [SerializeField, Tooltip("_distribution")]
        private float _distribution;

        [SerializeField, Tooltip("_goalkeeping")]
        private float _goalkeeping;

        [SerializeField, Tooltip("_danger")]
        private float _danger;

        [SerializeField, Tooltip("_percussion")]
        private float _percussion;

        [SerializeField, Tooltip("_recovery")]
        private float _recovery;

        [SerializeField, Tooltip("_strictness")]
        private float _strictness;

        [SerializeField, Tooltip("_technicalProfile")]
        private float _technicalProfile;

        [SerializeField, Tooltip("_visionProfile")]
        private float _visionProfile;

        [SerializeField, Tooltip("_coldBloodProfile")]
        private float _coldBloodProfile;

        [SerializeField, Tooltip("_goalkeeping_profile")]
        private float _goalkeeping_profile;

        [SerializeField, Tooltip("_passProfile")]
        private float _passProfile;

        [SerializeField, Tooltip("_speedProfile")]
        private float _speedProfile;

        [SerializeField, Tooltip("_explosivityProfile")]
        private float _explosivityProfile;

        [SerializeField, Tooltip("_riskProfile")]
        private float _riskProfile;

        [SerializeField, Tooltip("_tacticalProfile")]
        private float _tacticalProfile;

        [SerializeField, Tooltip("_positioningProfile")]
        private float _positioningProfile;

        [SerializeField, Tooltip("_strengthProfile")]
        private float _strengthProfile;

        [SerializeField, Tooltip("_timingProfile")]
        private float _timingProfile;

        [SerializeField, Tooltip("_controlSkill")]
        private float _controlSkill;

        [SerializeField, Tooltip("_passSkill")]
        private float _passSkill;

        [SerializeField, Tooltip("_longPassSkill")]
        private float _longPassSkill;

        [SerializeField, Tooltip("_verticalPassSkill")]
        private float _verticalPassSkill;

        [SerializeField, Tooltip("_backPassSkill")]
        private float _backPassSkill;

        [SerializeField, Tooltip("_shotSkill")]
        private float _shotSkill;

        [SerializeField, Tooltip("_penaltySkill")]
        private float _penaltySkill;

        [SerializeField, Tooltip("_dribbleSkill")]
        private float _dribbleSkill;

        [SerializeField, Tooltip("_visionSkill")]
        private float _visionSkill;

        [SerializeField, Tooltip("_saveSkill")]
        private float _saveSkill;

        [SerializeField, Tooltip("_footGameSkill")]
        private float _footGameSkill;

        [SerializeField, Tooltip("_tackleSkill")]
        private float _tackleSkill;

        [SerializeField, Tooltip("_precisionProfile")]
        private float _precisionProfile;

        [SerializeField, Tooltip("_freeKickSkill")]
        private float _freeKickSkill;

        [SerializeField, Tooltip("_finishSkill")]
        private float _finishSkill;

        [SerializeField, Tooltip("_interceptionSkill")]
        private float _interceptionSkill;

        [SerializeField, Tooltip("_defenseHeadedSkill")]
        private float _defenseHeadedSkill;

        [SerializeField, Tooltip("_attackHeadedSkill")]
        private float _attackHeadedSkill;

        [SerializeField, Tooltip("_closeEfficiencySkill")]
        private float _closeEfficiencySkill;

        [SerializeField, Tooltip("_farEfficiencySkill")]
        private float _farEfficiencySkill;

        [SerializeField, Tooltip("_diveSkill")]
        private float _diveSkill;

        [SerializeField, Tooltip("_closeSaveSkill")]
        private float _closeSaveSkill;

        [SerializeField, Tooltip("_farSaveSkill")]
        private float _farSaveSkill;

        [SerializeField, Tooltip("_freeKickSaveSkill")]
        private float _freeKickSaveSkill;

        [SerializeField, Tooltip("_standingSaveSkill")]
        private float _standingSaveSkill;

        [SerializeField, Tooltip("_closeGoalSkill")]
        private float _closeGoalSkill;

        [SerializeField, Tooltip("_farGoalSkill")]
        private float _farGoalSkill;

        [SerializeField, Tooltip("_handRelaunchSkill")]
        private float _handRelaunchSkill;

        [SerializeField, Tooltip("_comingOffSkill")]
        private float _comingOffSkill;

        [SerializeField, Tooltip("_strengthSkill")]
        private float _strengthSkill;

        [SerializeField, Tooltip("_attackPositioningSkill")]
        private float _attackPositioningSkill;

        [SerializeField, Tooltip("_defensePositioningSkill")]
        private float _defensePositioningSkill;

        [SerializeField, Tooltip("_reactivitySkill")]
        private float _reactivitySkill;

        [SerializeField, Tooltip("_percussionSkill")]
        private float _percussionSkill;

        [SerializeField, Tooltip("_rigorSkill")]
        private float _rigorSkill;

        [SerializeField, Tooltip("_cornerSkill")]
        private float _cornerSkill;

        [SerializeField, Tooltip("_paramSkill")]
        private float _paramSkill;

        [SerializeField, Tooltip("_concentrationSkill")]
        private float _concentrationSkill;

        [SerializeField, Tooltip("_technical")]
        private float _technical;

        [SerializeField, Tooltip("_attack")]
        private float _attack;

        [SerializeField, Tooltip("_fitness_mental")]
        private float _fitness_mental;

        [SerializeField, Tooltip("_goalkeeper")]
        private float _goalkeeper;

        [SerializeField, Tooltip("_defense")]
        private float _defense;

        [SerializeField, Tooltip("_param")]
        private float _param;

        [SerializeField, Tooltip("_score")]
        private string _score;

        [SerializeField, Tooltip("_level")]
        private string _level;

        [SerializeField, Tooltip("_nextGame")]
        private string _nextGame;

        [SerializeField, Tooltip("_pvpAttack")]
        private int _pvpAttack;

        [SerializeField, Tooltip("_pvpDefense")]
        private int _pvpDefense;

        [SerializeField, Tooltip("_pvpGoalkeeping")]
        private int _pvpGoalkeeping;


        [SerializeField, Tooltip("_rareLevel")]
        private int _rareLevel;

        [SerializeField, Tooltip("_clubChampionshipId")]
        private int _clubChampionshipId;

        [SerializeField, Tooltip("_longShotChallenge")]
        private int _longShotChallenge;

        [SerializeField, Tooltip("_longSaveChallenge")]
        private int _longSaveChallenge;

        [SerializeField, Tooltip("_tackleChallenge")]
        private int _tackleChallenge;

        [SerializeField, Tooltip("_dribbleChallenge")]
        private int _dribbleChallenge;

        [SerializeField, Tooltip("_pressingChallenge")]
        private int _pressingChallenge;

        [SerializeField, Tooltip("_shortPassChallenge")]
        private int _shortPassChallenge;

        [SerializeField, Tooltip("_longPassChallenge")]
        private int _longPassChallenge;

        [SerializeField, Tooltip("_counterChallenge")]
        private int _counterChallenge;

        [SerializeField, Tooltip("_receiveChallenge")]
        private int _receiveChallenge;

        [SerializeField, Tooltip("_interceptChallenge")]
        private int _interceptChallenge;

        [SerializeField, Tooltip("_genericPassChallenge")]
        private int _genericPassChallenge;


        #endregion


        #region Public Methods

        /// <summary>
        /// Initialize Data with json response from Api
        /// </summary>
        /// <param name="pToken">Specific JsonToken to use</param>
        public void Init(JToken pToken)
        {
            _id = pToken.SafeValue<int>("id", -1);
            _teamId = pToken.SafeValue<string>("team_id");
            _playerId = pToken.SafeValue<string>("player_id");
            _value = pToken.SafeValue<float>("value", 0);
            _isOnsale = pToken.SafeValue<string>("is_onsale");
            _position = pToken.SafeValue<string>("position");
            _squadPosition = pToken.SafeValue<string>("squad_position");
            _fullPosition = pToken.SafeValue<string>("full_position");
            _teamName = pToken.SafeValue<string>("team_name");
            _officialTeamOptaId = pToken.SafeValue<string>("official_team_opta_id");
            _officialTeamId = pToken.SafeValue<string>("official_team_id");
            _officialTeamCountryId = pToken.SafeValue<string>("official_team_country_id");
            _optaId = pToken.SafeValue<string>("opta_id");
            _oldOptaId = pToken.SafeValue<string>("old_opta_id");
            _sdapiId = pToken.SafeValue<string>("sdapi_id");
            _name = pToken.SafeValue<string>("name");
            _nameShort = pToken.SafeValue<string>("name_short");
            _nameField = pToken.SafeValue<string>("name_field");
            _oldValue = pToken.SafeValue<string>("old_value");
            _status = pToken.SafeValue<string>("status");
            _injured = pToken.SafeValue<string>("injured");
            _dateInjury = pToken.SafeValue<string>("date_injury");
            _dateStartSuspended = pToken.SafeValue<string>("date_start_suspended");
            _dateEndSuspended = pToken.SafeValue<string>("date_end_suspended");
            _pseudoTwitter = pToken.SafeValue<string>("pseudo_twitter");
            _birthDate = pToken.SafeValue<string>("birth_date");
            _weight = pToken.SafeValue<string>("weight");
            _height = pToken.SafeValue<string>("height");
            _number = pToken.SafeValue<string>("number");
            _joinDate = pToken.SafeValue<string>("join_date");
            _country = pToken.SafeValue<string>("country");
            _finish = pToken.SafeValue<float>("finish", 0);
            _distribution = pToken.SafeValue<float>("distribution", 0);
            _goalkeeping = pToken.SafeValue<float>("goalkeeping", 0);
            _danger = pToken.SafeValue<float>("danger", 0);
            _percussion = pToken.SafeValue<float>("percussion", 0);
            _recovery = pToken.SafeValue<float>("recovery", 0);
            _strictness = pToken.SafeValue<float>("strictness");
            _technicalProfile = pToken.SafeValue<float>("technical_profile", 0);
            _visionSkill = pToken.SafeValue<float>("vision_profile", 0);
            _coldBloodProfile = pToken.SafeValue<float>("cold_blood_profile", 0);
            _goalkeeping_profile = pToken.SafeValue<float>("goalkeeping_profile", 0);
            _passProfile = pToken.SafeValue<float>("pass_profile", 0);
            _speedProfile = pToken.SafeValue<float>("speed_profile", 0);
            _explosivityProfile = pToken.SafeValue<float>("explosivity_profile", 0);
            _riskProfile = pToken.SafeValue<float>("risk_profile", 0);
            _technicalProfile = pToken.SafeValue<float>("tactical_profile", 0);
            _positioningProfile = pToken.SafeValue<float>("positioning_profile", 0);
            _strengthProfile = pToken.SafeValue<float>("strength_profile", 0);
            _timingProfile = pToken.SafeValue<float>("timing_profile", 0);
            _controlSkill = pToken.SafeValue<float>("control_skill", 0);
            _passSkill = pToken.SafeValue<float>("pass_skill", 0);
            _longPassSkill = pToken.SafeValue<float>("long_pass_skill", 0);
            _verticalPassSkill = pToken.SafeValue<float>("vertical_pass_skill", 0);
            _backPassSkill = pToken.SafeValue<float>("back_pass_skill", 0);
            _shotSkill = pToken.SafeValue<float>("shot_skill", 0);
            _penaltySkill = pToken.SafeValue<float>("penalty_skill", 0);
            _dribbleSkill = pToken.SafeValue<float>("dribble_skill", 0);
            _visionSkill = pToken.SafeValue<float>("vision_skill", 0);
            _saveSkill = pToken.SafeValue<float>("save_skill", 0);
            _footGameSkill = pToken.SafeValue<float>("foot_game_skill", 0);
            _tackleSkill = pToken.SafeValue<float>("tackle_skill");
            _precisionProfile = pToken.SafeValue<float>("precision_profile", 0);
            _freeKickSkill = pToken.SafeValue<float>("free_kick_skill", 0);
            _finishSkill = pToken.SafeValue<float>("finish_skill", 0);
            _interceptionSkill = pToken.SafeValue<float>("interception_skill", 0);
            _defenseHeadedSkill = pToken.SafeValue<float>("defense_headed_skill", 0);
            _attackHeadedSkill = pToken.SafeValue<float>("attack_headed_skill", 0);
            _closeEfficiencySkill = pToken.SafeValue<float>("close_efficiency_skill", 0);
            _farEfficiencySkill = pToken.SafeValue<float>("far_efficiency_skill", 0);
            _diveSkill = pToken.SafeValue<float>("dive_skill", 0);
            _closeSaveSkill = pToken.SafeValue<float>("close_save_skill", 0);
            _farSaveSkill = pToken.SafeValue<float>("far_save_skill", 0);
            _freeKickSaveSkill = pToken.SafeValue<float>("free_kick_save_skill", 0);
            _standingSaveSkill = pToken.SafeValue<float>("standing_save_skill", 0);
            _closeGoalSkill = pToken.SafeValue<float>("close_goal_skill", 0);
            _farGoalSkill = pToken.SafeValue<float>("far_goal_skill", 0);
            _handRelaunchSkill = pToken.SafeValue<float>("hand_relaunch_skill", 0);
            _comingOffSkill = pToken.SafeValue<float>("coming_off_skill");
            _strengthSkill = pToken.SafeValue<float>("strength_skill", 0);
            _attackPositioningSkill = pToken.SafeValue<float>("attack_positioning_skill", 0);
            _defensePositioningSkill = pToken.SafeValue<float>("defense_positioning_skill", 0);
            _reactivitySkill = pToken.SafeValue<float>("reactivity_skill", 0);
            _percussionSkill = pToken.SafeValue<float>("percussion_skill", 0);
            _rigorSkill = pToken.SafeValue<float>("rigor_skill", 0);
            _cornerSkill = pToken.SafeValue<float>("corner_skill", 0);
            _paramSkill = pToken.SafeValue<float>("param_skill", 0);
            _concentrationSkill = pToken.SafeValue<float>("param_skill", 0);
            _technical = pToken.SafeValue<float>("technical", 0);
            _attack = pToken.SafeValue<float>("attack", 0);
            _fitness_mental = pToken.SafeValue<float>("fitness_mental", 0);
            _goalkeeper = pToken.SafeValue<float>("goalkeeper", 0);
            _defense = pToken.SafeValue<float>("defense", 0);
            _param = pToken.SafeValue<float>("param", 0);
            _score = pToken.SafeValue<string>("score");
            _level = pToken.SafeValue<string>("level");
            _nextGame = pToken.SafeValue<string>("next_game");

            _pvpAttack = pToken.SafeValue<int>("pvp_attack", 0);
            _pvpDefense = pToken.SafeValue<int>("pvp_defense", 0);
            _pvpGoalkeeping = pToken.SafeValue<int>("pvp_goalkeeping", 0);
            _rareLevel = pToken.SafeValue<int>("rare_level", 0);
            _clubChampionshipId = pToken.SafeValue<int>("club_championship_id", 0);
            _longShotChallenge = pToken.SafeValue<int>("long_shot_challenge", 0);
            _longSaveChallenge = pToken.SafeValue<int>("long_save_challenge", 0);
            _tackleChallenge = pToken.SafeValue<int>("tackle_challenge", 0);
            _dribbleChallenge = pToken.SafeValue<int>("dribble_challenge", 0);
            _pressingChallenge = pToken.SafeValue<int>("pressing_challenge", 0);
            _shortPassChallenge = pToken.SafeValue<int>("short_pass_challenge", 0);
            _longPassChallenge = pToken.SafeValue<int>("long_pass_challenge", 0);
            _counterChallenge = pToken.SafeValue<int>("counter_challenge", 0);
            _receiveChallenge = pToken.SafeValue<int>("receive_challenge", 0);
            _interceptChallenge = pToken.SafeValue<int>("intercept_challenge", 0);
            _genericPassChallenge = pToken.SafeValue<int>("generic_pass_challenge", 0);

        }

        #endregion
    }
}

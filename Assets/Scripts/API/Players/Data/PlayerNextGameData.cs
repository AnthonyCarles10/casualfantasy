﻿using Sportfaction.CasualFantasy.Services;

using Newtonsoft.Json.Linq;
using UnityEngine;
using System.Collections.Generic;

namespace Sportfaction.CasualFantasy.Players
{
    [System.Serializable]
    public sealed class PlayerNextGameData
    {
        #region Properties

        public string Date { get { return _date; } }


        #endregion

        #region Fields

        [SerializeField, Tooltip("date")]
        private string _date;




        #endregion


        #region Public Methods

        /// <summary>
        /// Initialize Data with json response from Api
        /// </summary>
        /// <param name="pToken">Specific JsonToken to use</param>
        public void Init(JToken pToken)
        {

            _date = pToken.SafeValue<string>("date");
        }

        #endregion
    }
}

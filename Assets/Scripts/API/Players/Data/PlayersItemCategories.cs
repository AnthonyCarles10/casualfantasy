﻿using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using UnityEngine;

namespace Sportfaction.CasualFantasy.Players
{
    [System.Serializable]
    public sealed class PlayersItemCategories
    {
        #region Properties

        public string Name { get { return _name; } }

        public List<PlayersItemCategoriesSkills> Skills { get { return _skills; } }

        #endregion

        #region Fields

        [SerializeField, Tooltip("_name")]
        private string _name;

        [SerializeField, Tooltip("_skills")]
        private List<PlayersItemCategoriesSkills> _skills = new List<PlayersItemCategoriesSkills>();

        #endregion


        #region Public Methods

        /// <summary>
        /// Initialize Data with json response from Api
        /// </summary>
        /// <param name="pToken">Specific JsonToken to use</param>
        public void Init(JToken pToken)
        {
            _name = pToken.Value<string>("name");


            JArray lSkillsArray = pToken.Value<JArray>("skills");

  
            foreach (JToken lSkill in lSkillsArray)
            {
                PlayersItemCategoriesSkills lNewSkill = new PlayersItemCategoriesSkills();
                lNewSkill.Init(lSkill);

                _skills.Add(lNewSkill);
            }

        }

        #endregion
    }
}

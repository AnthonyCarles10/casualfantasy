﻿using Sportfaction.CasualFantasy.Services;

using Newtonsoft.Json.Linq;
using UnityEngine;
using System.Collections.Generic;

namespace Sportfaction.CasualFantasy.Players
{
    [System.Serializable]
    public sealed class PlayerLastGameData
    {
        #region Properties

        public string Date { get { return _date; } }

        public string ChampionshipName { get { return _championshipName; } }

        public int TeamAId { get { return _teamAId; } }

        public string TeamAName { get { return _teamAName; } }

        public int TeamAScore { get { return _teamAScore; } }

        public int TeamBId { get { return _teamBId; } }

        public string TeamBName { get { return _teamBName; } }

		public int TeamBScore { get { return _teamBScore; } }

        public int Form { get { return _form; } }

        public List<PlayerLastGameDataGoal> Goals { get { return _goals; } }

        #endregion

        #region Fields

        [SerializeField, Tooltip("date")]
        private string _date;

        [SerializeField, Tooltip("championshipName")]
        private string _championshipName;

        [SerializeField, Tooltip("TeamAId")]
        private int _teamAId;

        [SerializeField, Tooltip("teamAName")]
        private string _teamAName;

		[SerializeField, Tooltip("teamAScore")]
		private int _teamAScore;

        [SerializeField, Tooltip("TeamBId")]
        private int _teamBId;

        [SerializeField, Tooltip("teamBName")]
        private string _teamBName;

		[SerializeField, Tooltip("teamBScore")]
		private int _teamBScore;

        [SerializeField, Tooltip("form")]
        private int _form;

        [SerializeField, Tooltip("goals")]
        private List<PlayerLastGameDataGoal> _goals = new List<PlayerLastGameDataGoal>();


        #endregion


        #region Public Methods

        /// <summary>
        /// Initialize Data with json response from Api
        /// </summary>
        /// <param name="pToken">Specific JsonToken to use</param>
        public void Init(JToken pToken)
        {

            _date = pToken.SafeValue<string>("date");
            _championshipName = pToken.SafeValue<string>("championship_name");
            _teamAId = pToken.SafeValue<int>("team1_id");
            _teamAName = pToken.SafeValue<string>("team1_name");
			_teamAScore = pToken.SafeValue<int>("team1_score",0);
            _teamBId = pToken.SafeValue<int>("team2_id");
            _teamBName = pToken.SafeValue<string>("team2_name");
			_teamBScore = pToken.SafeValue<int>("team2_score",0);
			_form = pToken.SafeValue<int>("form", 0);

            JArray lGoals = pToken.Value<JArray>("goals");

            foreach (JToken lGoal in lGoals)
            {
                PlayerLastGameDataGoal lNewGoal = new PlayerLastGameDataGoal();
                lNewGoal.Init(lGoal);

                _goals.Add(lNewGoal);
            }
        }

        #endregion
    }
}

﻿namespace Sportfaction.CasualFantasy.Players
{
    /// <summary>
    /// Description: Enum contains all players' position
    /// Author: Rémi Carreira
    /// </summary>
    public enum ePosition
    {
        NONE,

        G,
        D,
        M,
        A,

        DG,
        DCG,
        DC,
        DCD,
        DD,
        LG,
        MDCG,
        MDC,
        MDCD,
        LD,
        MG,
        MCG,
        MC,
        MCD,
        MD,
        MOG,
        MOCG,
        MOC,
        MOCD,
        MOD,
        ACG,
        AC,
        ACD,
        AILG,
        BG,
        B,
        BD,
        AILD,

        COUNT,
    }
}

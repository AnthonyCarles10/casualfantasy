﻿using Sportfaction.CasualFantasy.Services;

using Newtonsoft.Json.Linq;
using UnityEngine;

namespace Sportfaction.CasualFantasy.Players
{
    [System.Serializable]
    public sealed class PlayerLastGameSkills
    {
        #region Properties

        public int Goals { get { return _goals; } }

        public int PenaltySave { get { return _penaltySave; } }

        public int GoalAssist { get { return _goalAssist; } }

        public int AccuratePassTr3 { get { return _accuratePassTr3; } }

        public int MinsPlayed1 { get { return _minsPlayed1; } }

        public int Tackle { get { return _tackle; } }

        public int Saves { get { return _saves; } }

        public int BallRecovery { get { return _ballRecovery; } }

        public int Interception { get { return _interception; } }

        public int KeyPass { get { return _keyPass; } }

        public int WonContest { get { return _wonContest; } }

        public int AccurateCrossNocorner { get { return _accurateCrossNocorner; } }

        public int GoalsConcOnfield { get { return _goalsConcOnfield; } }

        public int TotalOffside { get { return _totalOffside; } }

        public int ShotOffTarget { get { return _shotOffTarget; } }

        public int Fouls { get { return _fouls; } }

        public int Dispossessed { get { return _dispossessed; } }

        public int BigChanceMissed { get { return _bigChanceMissed; } }

        public int YellowCard { get { return _yellowCard; } }

        public int RedCard { get { return _redCard; } }

        public int NoMinsPlayed { get { return _noMinsPlayed; } }

        #endregion

        #region Fields

        [SerializeField, Tooltip("goals")]
        private int _goals;

        [SerializeField, Tooltip("penaltySave")]
        private int _penaltySave;

        [SerializeField, Tooltip("goalAssist")]
        private int _goalAssist;

        [SerializeField, Tooltip("accuratePassTr3")]
        private int _accuratePassTr3;

        [SerializeField, Tooltip("minsPlayed1")]
        private int _minsPlayed1;

        [SerializeField, Tooltip("tackle")]
        private int _tackle;

        [SerializeField, Tooltip("saves")]
        private int _saves;

        [SerializeField, Tooltip("ballRecovery")]
        private int _ballRecovery;

        [SerializeField, Tooltip("interception")]
        private int _interception;

        [SerializeField, Tooltip("keyPass")]
        private int _keyPass;

        [SerializeField, Tooltip("wonContest")]
        private int _wonContest;

        [SerializeField, Tooltip("accurateCrossNocorner")]
        private int _accurateCrossNocorner;

        [SerializeField, Tooltip("goalsConcOnfield")]
        private int _goalsConcOnfield;

        [SerializeField, Tooltip("totalOffside")]
        private int _totalOffside;

        [SerializeField, Tooltip("shotOffTarget")]
        private int _shotOffTarget;

        [SerializeField, Tooltip("fouls")]
        private int _fouls;

        [SerializeField, Tooltip("dispossessed")]
        private int _dispossessed;

        [SerializeField, Tooltip("bigChanceMissed")]
        private int _bigChanceMissed;

        [SerializeField, Tooltip("yellowCard")]
        private int _yellowCard;

        [SerializeField, Tooltip("redCard")]
        private int _redCard;

        [SerializeField, Tooltip("noMinsPlayed")]
        private int _noMinsPlayed;




        #endregion


        #region Public Methods

        /// <summary>
        /// Initialize Data with json response from Api
        /// </summary>
        /// <param name="pToken">Specific JsonToken to use</param>
        public void Init(JToken pToken)
        {

            _goals = pToken.SafeValue<int>("goals", 0);
            _penaltySave = pToken.SafeValue<int>("penaltySave", 0);
            _goalAssist = pToken.SafeValue<int>("goalAssist", 0);
            _accuratePassTr3 = pToken.SafeValue<int>("accuratePassTr3", 0);
            _minsPlayed1 = pToken.SafeValue<int>("minsPlayed1", 0);
            _tackle = pToken.SafeValue<int>("tackle", 0);
            _saves = pToken.SafeValue<int>("saves", 0);
            _ballRecovery = pToken.SafeValue<int>("ballRecovery", 0);
            _interception = pToken.SafeValue<int>("interception", 0);
            _keyPass = pToken.SafeValue<int>("keyPass", 0);
            _wonContest = pToken.SafeValue<int>("wonContest", 0);
            _accurateCrossNocorner = pToken.SafeValue<int>("accurateCrossNocorner", 0);
            _goalsConcOnfield = pToken.SafeValue<int>("goalsConcOnfield", 0);
            _totalOffside = pToken.SafeValue<int>("totalOffside", 0);
            _shotOffTarget = pToken.SafeValue<int>("shotOffTarget", 0);
            _fouls = pToken.SafeValue<int>("fouls", 0);
            _dispossessed = pToken.SafeValue<int>("dispossessed", 0);
            _bigChanceMissed = pToken.SafeValue<int>("bigChanceMissed", 0);
            _yellowCard = pToken.SafeValue<int>("yellowCard", 0);
            _redCard = pToken.SafeValue<int>("redCard", 0);
            _noMinsPlayed = pToken.SafeValue<int>("noMinsPlayed", 0);


        }

        #endregion
    }
}

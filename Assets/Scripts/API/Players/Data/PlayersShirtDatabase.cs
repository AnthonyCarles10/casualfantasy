﻿using UnityEngine;

namespace Sportfaction.CasualFantasy.Players
{
    [CreateAssetMenu(fileName = "New player shirt database", menuName = "CasualFantasy/Database/PlayersShirt")]
	public class PlayersShirtDatabase : ScriptableObject
	{
		#region Variables

		[SerializeField, Tooltip("Default shirt")]
		private	Sprite		_defaultShirt	=	null;
		[SerializeField, Tooltip("List of shirts")]
		private	Sprite[]	_shirts;

		#endregion

		#region Public Methods
		
		/// <summary>
		/// Retrieves player shirt sprite by player team ID
		/// </summary>
		/// <param name="pTeamId">Team ID to search for</param>
		public	Sprite	FindShirtByTeamId(string pTeamId)
		{
			for (int li = 0; li < _shirts.Length; li++)
			{
				if ("Player_BG_" + pTeamId == _shirts[li].name)
					return _shirts[li];
			}

			return _defaultShirt;
		}
		
		#endregion
	}
}

﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using UnityEngine;

namespace Sportfaction.CasualFantasy.Players
{
    [CreateAssetMenu(fileName = "Store-Players-Manager", menuName = "CasualFantasy/Data/Store/Players-Manager")]
    public sealed class PlayersManager : ScriptableObject
    {
        #region Properties
        [JsonIgnore]
        public List<string> Ids   { get { return _ids; } set { _ids = value; } }

        #endregion

        #region Fields

        [SerializeField, JsonProperty("ids"), Tooltip("ids")]
        private List<string> _ids = null;

        #endregion

        #region Public Methods

        /// <summary>
        /// Initializes data with json response from Api
        /// </summary>
        /// <param name="pJson">Player list JSON to parse</param>
        public void Parse(string pJson)
        {
            JToken lJToken = JToken.Parse(pJson);
            JArray lArray = lJToken.Value<JArray>("ids");
            _ids = lArray.ToObject<List<string>>();

        }

        #endregion
    }
}

﻿using Sportfaction.CasualFantasy.Services;

using Newtonsoft.Json.Linq;
using UnityEngine;
using System.Collections.Generic;

namespace Sportfaction.CasualFantasy.Players
{
    [System.Serializable]
    public sealed class PlayerLastGameDataGoal
    {
        #region Properties

        public string NameField { get { return _nameField; } }

        public int OfficialTeamId { get { return _officialTeamId; } }

        public int Score { get { return _score; } }


        #endregion

        #region Fields

        [SerializeField, Tooltip("name_field")]
        private string _nameField;

        [SerializeField, Tooltip("officialTeamId")]
        private int _officialTeamId;

        [SerializeField, Tooltip("score")]
        private int _score;

        #endregion


        #region Public Methods

        /// <summary>
        /// Initialize Data with json response from Api
        /// </summary>
        /// <param name="pToken">Specific JsonToken to use</param>
        public void Init(JToken pToken)
        {
            _nameField = pToken.SafeValue<string>("name_field");

            _officialTeamId = pToken.SafeValue<int>("official_team_id");

            _score = pToken.SafeValue<int>("score");
        }

        #endregion
    }
}

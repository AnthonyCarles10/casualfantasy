﻿using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using UnityEngine;

namespace Sportfaction.CasualFantasy.Players
{
    [CreateAssetMenu(fileName = "Store-Players", menuName = "CasualFantasy/Data/Store/Players")]
    public sealed class PlayersData : ScriptableObject
    {
        #region Properties

        public  List<PlayersItem>   Items   { get { return _items; } set { _items = value; } }

        public  PlayersMetas        Metas   { get { return _metas; } set { _metas = value; } }

        #endregion

        #region Fields

        [SerializeField, Tooltip("_items")]
        private List<PlayersItem>   _items  =   new List<PlayersItem>();
        [SerializeField, Tooltip("_metas")]
        private PlayersMetas        _metas  =   new PlayersMetas();

        #endregion

        #region Public Methods

        /// <summary>
        /// Initializes data with json response from Api
        /// </summary>
        /// <param name="pJson">Player list JSON to parse</param>
        public void Init(string pJson)
        {
            // PLAYERS LIST
            JToken lJToken = JToken.Parse(pJson);

            if (false == pJson.Contains("\"list\":"))
            {
                pJson = "{\"list\":" + pJson + "}";
                lJToken = JToken.Parse(pJson);
            }

            JArray lPlayersArray = lJToken.Value<JArray>("list");

            foreach (JToken lPlayer in lPlayersArray)
            {
                PlayersItem lNewPlayer = new PlayersItem();
                lNewPlayer.Init(lPlayer);

                _items.Add(lNewPlayer);
            }

            //METAS
            if (null != lJToken.Value<JToken>("metas"))
            {
                JToken lMetasToken = lJToken.Value<JToken>("metas");
                _metas.Init(lMetasToken);
            }
        }

        #endregion
    }
}

﻿using Sportfaction.CasualFantasy.Fantasy;

using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using UnityEngine;
using SportFaction.CasualFootballEngine.PlayerService.Model;
using SportFaction.CasualFootballEngine.TeamService.Enum;

namespace Sportfaction.CasualFantasy.Players
{
    [System.Serializable]
    public sealed class PlayersItem
    {
        #region Properties

        public Player Profile { get { return _profile; } }

        public string JsonProfile { get { return _jsonProfile; } }

        public List<PlayersItemCategories> Categories { get { return _categories; } }

        public List<FantasyBuffItemData> Buffs { get { return _buffs; } }

        #endregion


        #region Fields

        [SerializeField, Tooltip("_profile")]
        private Player _profile = new Player();

        [SerializeField, Tooltip("_jsonProfile")]
        private string _jsonProfile = "";

        [SerializeField, Tooltip("_categories")]
        private List<PlayersItemCategories> _categories = new List<PlayersItemCategories>();

        [SerializeField, Tooltip("_buffs")]
        private List<FantasyBuffItemData> _buffs = new List<FantasyBuffItemData>();

        #endregion


        #region Public Methods

        /// <summary>
        /// Initialize Data with json response from Api
        /// </summary>
        /// <param name="pToken">Specific JsonToken to use</param>
        public void Init(JToken pToken)
        {

            string lJson = pToken.ToString();

            /* Profile */
            if(false == lJson.Contains("\"player\":")) {
                lJson = "{\"player\":" + lJson + "}";
                pToken = JToken.Parse(lJson);
            }
            _jsonProfile = pToken.Value<JToken>("player").ToString();

            _profile.Initialize(eTeam.NONE, pToken.Value<JToken>("player"), new JArray());

            /* Categories */

            if (true == lJson.Contains("\"categories\":"))
            {
                JArray lCategoriesArray = pToken.Value<JArray>("categories");
                foreach (JToken lCategory in lCategoriesArray)
                {
                    PlayersItemCategories lNewCategory = new PlayersItemCategories();
                    lNewCategory.Init(lCategory);

                    _categories.Add(lNewCategory);
                }
            }

            /* Buffs */

            if (true == lJson.Contains("\"buffs\":"))
            {
                JArray lBuffArray = pToken.Value<JArray>("buffs");
                foreach (JToken lBuff in lBuffArray)
                {
                    FantasyBuffItemData lnewBuff = new FantasyBuffItemData();
                    lnewBuff.Init(lBuff);

                    _buffs.Add(lnewBuff);
                }
            }
        }

        #endregion



    }
}

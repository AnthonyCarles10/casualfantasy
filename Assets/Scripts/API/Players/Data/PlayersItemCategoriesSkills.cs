﻿using Newtonsoft.Json.Linq;
using UnityEngine;

namespace Sportfaction.CasualFantasy.Players
{
    [System.Serializable]
    public sealed class PlayersItemCategoriesSkills
    {
        #region Properties

        public string Name { get { return _name; } }

        public string Value { get { return _value; } }

        public string IsBuffed { get { return _isBuffed; } }

        #endregion

        #region Fields

        [SerializeField, Tooltip("_name")]
        private string _name;

        [SerializeField, Tooltip("_value")]
        private string _value;

        [SerializeField, Tooltip("_isBuffed")]
        private string _isBuffed;

        #endregion


        #region Public Methods

        /// <summary>
        /// Initialize Data with json response from Api
        /// </summary>
        /// <param name="pToken">Specific JsonToken to use</param>
        public void Init(JToken pToken)
        {
            _name = pToken.Value<string>("name");
            _value = pToken.Value<string>("value");
            _isBuffed = pToken.Value<string>("is_buffed");
        }

        #endregion
    }
}

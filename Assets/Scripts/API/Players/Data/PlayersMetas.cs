﻿using Newtonsoft.Json.Linq;
using UnityEngine;

namespace Sportfaction.CasualFantasy.Players
{
    [System.Serializable]
    public sealed class PlayersMetas
    {
        #region Properties

        public int TotalCount { get { return _totalCount; } }

        public int Offset { get { return _offset; } }

        #endregion

        #region Fields

        [SerializeField, Tooltip("_totalCount")]
        private int _totalCount;

        [SerializeField, Tooltip("_offset")]
        private int _offset;

        #endregion


        #region Public Methods

        /// <summary>
        /// Initialize Data with json response from Api
        /// </summary>
        /// <param name="pToken">Specific JsonToken to use</param>
        public void Init(JToken pToken)
        {
            _totalCount = pToken.Value<int>("XTotalCount");
            _offset = pToken.Value<int>("current");

        }

        #endregion
    }
}

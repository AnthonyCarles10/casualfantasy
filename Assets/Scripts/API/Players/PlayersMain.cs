﻿using Newtonsoft.Json;
using Sportfaction.CasualFantasy.Events;
using Sportfaction.CasualFantasy.Players;

using System.Collections.Generic;
using UnityEngine;

namespace Sportfaction.CasualFantasy.Players
{
    public class PlayersMain : MonoBehaviour
    {
        [Header("Links")]

        [SerializeField, Tooltip("PlayersAPICalls")]
        private PlayersAPICalls _playersAPICalls = null;

        [Header("API Data")]

        public PlayersManager PlayersManager;

        public PlayersData Players;

        public PlayersData PlayersList;

        public PlayersData PlayersFantasyList;

        public PlayerLastGame PlayerLastGame;


        [Header("Game Events")]
        [SerializeField]
        private GameEvent _onGetPlayersManager = null;
        [SerializeField]
        private GameEvent _onGetPlayers = null;
        [SerializeField]
        private GameEvent _onGetPlayersList = null;
        [SerializeField]
        private GameEvent _onGetPlayersFantasyList = null;
        [SerializeField]
        private GameEvent _onGetPlayerLastGame = null;


        #region Public Methods


        #region API Calls

        public void GetPlayersManager() { _playersAPICalls.GetPlayersManager(); }
        public void GetPlayerLastGame(string pPlayerId) { _playersAPICalls.GetPlayerLastGame(pPlayerId); }
        public void GetPlayers(string pPlayersId) { _playersAPICalls.GetPlayers(pPlayersId); }
        public void GetPlayersList(Dictionary<string, string> pQueryParam) { _playersAPICalls.GetPlayersList(pQueryParam); }
        public void GetPlayersFantasyList(Dictionary<string, string> pQueryParam) { _playersAPICalls.GetPlayersFantasyList(pQueryParam); }
        public void APIAddEvents() { _playersAPICalls.AddEvents(); }

        #endregion

        /// <summary>
        /// Transform Json To Players
        /// Event: _onGetPlayers
        /// </summary>
        /// <param name="pJson">Json</param>
        public void TransformJsonToPlayers(string pJson)
        {
            Players.Init(pJson);

            _onGetPlayers.Invoke();
        }

        public void TransformJsonToPlayersList(string pJson)
        {
            PlayersList = ScriptableObject.CreateInstance<PlayersData>();

            PlayersList.Init(pJson);

            _onGetPlayersList.Invoke();
        }

        public void ParseToPlayersFantasyList(string pJson)
        {
            PlayersFantasyList = ScriptableObject.CreateInstance<PlayersData>();

            PlayersFantasyList.Init(pJson);

            _onGetPlayersFantasyList.Invoke();
        }

        public void ParsePlayerLastGame(string pJson)
        {
            PlayerLastGame = ScriptableObject.CreateInstance<PlayerLastGame>();

            PlayerLastGame.Init(pJson);

            _onGetPlayerLastGame.Invoke();
        }

        public void ParsePlayersManager(string pJson)
        {
            PlayersManager = ScriptableObject.CreateInstance<PlayersManager>();

            PlayersManager.Parse(pJson);

            PlayerPrefs.SetString("playersmanager", JsonConvert.SerializeObject(PlayersManager));

            PlayerPrefs.Save();

            _onGetPlayersManager.Invoke();
        }


        public void LoadData()
        {
            if (PlayerPrefs.HasKey("playersmanager"))
            {
                string lJson = PlayerPrefs.GetString("playersmanager");

                PlayersManager = ScriptableObject.CreateInstance<PlayersManager>();

                PlayersManager.Parse(lJson);

                _onGetPlayersManager.Invoke();
            }


        }
        #endregion

        private void Awake()
        {
            LoadData();
        }

    }
}
 
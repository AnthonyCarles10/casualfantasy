using Newtonsoft.Json;
using Sportfaction.CasualFantasy.Events;
using UnityEngine;

namespace Sportfaction.CasualFantasy.ArenaSeason
{
    public class ArenaSeasonMain : MonoBehaviour
    {

        #region Variables

        [Header("API")]
        [SerializeField]
        private ArenaSeasonAPICalls _arenaSeasonAPICalls;

        [SerializeField]
        public ArenaSeasonData ArenaSeasonData;

        #endregion

        #region Events

        [Header("Game Events")]

        [SerializeField]
        private GameEvent _onGetRewardsArenaSeason;


        #endregion

        #region API Calls

        public void GetRewardsArenaSeason() { _arenaSeasonAPICalls.GetRewardsArenaSeason(); }

		#endregion

		#region Public Methods
	

        /// <summary>
		/// Parse json rewards arena season
		/// </summary>
		/// <param name="pData">Json to parse</param>
		public void ParseRewardsArenaSeason(string pData)
        {

            ArenaSeasonData = ScriptableObject.CreateInstance<ArenaSeasonData>();
            ArenaSeasonData.Parse(pData);

            SaveData();

            _onGetRewardsArenaSeason.Invoke();
        }

      

        public void LoadData()
        {
            if (PlayerPrefs.HasKey("arenaseason"))
            {
                string lJson = PlayerPrefs.GetString("arenaseason");

                ArenaSeasonData = ScriptableObject.CreateInstance<ArenaSeasonData>();
                ArenaSeasonData.Parse(lJson);

                _onGetRewardsArenaSeason.Invoke();

            }
        }

        public void SaveData()
        {
            PlayerPrefs.SetString("arenaseason", JsonConvert.SerializeObject(ArenaSeasonData));

            PlayerPrefs.Save();
        }

        #endregion

        private void Awake()
        {
            LoadData();
        }
    }
}

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

namespace Sportfaction.CasualFantasy.ArenaSeason
{
    /// <summary>
    /// Description: Class contains all loot data
    /// Author: Antoine de Lachèze-Murel
    /// </summary>
	[CreateAssetMenu(fileName = "Store-ArenaSeason", menuName = "CasualFantasy/Data/Store/ArenaSeason")]
    public sealed class ArenaSeasonData : ScriptableObject
    {
        #region Properties

        [JsonProperty("rewards")]
        public List<ArenaSeasonDataReward> Rewards { get { return _rewards; } }

        #endregion

        #region Fields

        [SerializeField, Tooltip("ArenaSeasonDataReward")]
        private List<ArenaSeasonDataReward> _rewards = new List<ArenaSeasonDataReward>();


        #endregion

        #region Public Methods

        /// <summary>
        /// Initialize Rewards data from a Json
        /// </summary>
        /// <param name="pJson">Specific json to use</param>
        public void Parse(string pJson)
        {
            JToken lJToken = JToken.Parse(pJson);

            JArray lrewardsArray = lJToken.Value<JArray>("rewards");

            _rewards = new List<ArenaSeasonDataReward>();

            foreach (JToken lReward in lrewardsArray)
            {
                ArenaSeasonDataReward lNewReward = new ArenaSeasonDataReward();
                lNewReward.Init(lReward);

                _rewards.Add(lNewReward);
            }
        }

        #endregion
    }
    
}
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Sportfaction.CasualFantasy.Players;
using UnityEngine;

namespace Sportfaction.CasualFantasy.ArenaSeason
{
    /// <summary>
    /// Description: Arena Season Data Rewards
    /// Author: Antoine de Lachèze-Murel
    /// </summary>
    [System.Serializable]
    public sealed class ArenaSeasonDataReward
    {

        #region Properties

        [JsonProperty("bracket")]
        public int Bracket { get { return _bracket; } }

        [JsonProperty("min_score")]
        public int MinScore { get { return _minScore; } }

        [JsonProperty("max_score")]
        public int MaxScore { get { return _maxScore; } }

        [JsonProperty("name")]
        public string Name { get { return _name; } }

        [JsonProperty("big_box")]
        public int BigBox { get { return _bigBox; } }

        [JsonProperty("medium_box")]
        public int MediumBox { get { return _mediumBox; } }

        [JsonProperty("small_box")]
        public int SmallBox { get { return _smallBox; } }

        [JsonProperty("fragment_box")]
        public int FragmentBox { get { return _fragmentBox; } }

        [JsonIgnore]
        public PlayersItem Player { get { return _player; } }

        [JsonProperty("player_json")]
        public string PlayerJson { get { return _playerJson; } }

        [JsonProperty("hard")]
        public int Hard { get { return _hard; } }

        #endregion

        #region Fields

        [SerializeField]
        private int _bracket;

        [SerializeField]
        private int _minScore;

        [SerializeField]
        private int _maxScore;

        [SerializeField]
        private string _name;

        [SerializeField]
        private int _bigBox;

        [SerializeField]
        private int _mediumBox;

        [SerializeField]
        private int _smallBox;

        [SerializeField]
        private int _fragmentBox;

        [SerializeField]
        private PlayersItem _player;

        [SerializeField]
        private string _playerJson;

        [SerializeField]
        private int _hard;


        #endregion

        #region Public Methods

        /// <summary>
        /// Initialize Loot data box from a Json
        /// </summary>
        /// <param name="pJson">Specific json to use</param>
        public void Init(JToken pToken)
        {
            _bracket = pToken.Value<int>("bracket");
                
            _minScore = pToken.Value<int>("min_score");

            _maxScore = pToken.Value<int>("max_score");

            _name = pToken.Value<string>("name");

            _bigBox = pToken.Value<int>("big_box");

            _mediumBox = pToken.Value<int>("medium_box");

            _smallBox = pToken.Value<int>("small_box");

            _playerJson = pToken.Value<string>("player_json");


            if (false == string.IsNullOrEmpty(_playerJson))
            {
                _player = new PlayersItem();
                _player.Init(_playerJson);
            }
            else if (pToken.Value<JToken>("player") != null && true == pToken.Value<JToken>("player").HasValues)
            {
      
                JToken lPlayerToken = pToken.Value<JToken>("player");

                _player = new PlayersItem();
                _player.Init(lPlayerToken);
                _playerJson = pToken.Value<JToken>("player").ToString();
            }

          
            _hard = pToken.Value<int>("hard");
        }

        #endregion
    }
}
using Sportfaction.CasualFantasy.Services;

using BestHTTP;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using System;

namespace Sportfaction.CasualFantasy.ArenaSeason
{
	/// <summary>
	/// Description: 
	/// Author: Antoine de Lach�ze-Murel
	/// </summary>
	public sealed class ArenaSeasonAPICalls : MonoBehaviour
	{
        #region Fields

        [SerializeField, Tooltip("ArenaSeasonMain")]
        private ArenaSeasonMain _arenaSeasonMain = null;

        private string _arenaSeasonRewardsURL = "";

        #endregion

        #region Public Methods




        /// <summary>
        /// Get Reward Arena Season
        /// </summary>
        public void GetRewardsArenaSeason()
		{
			if (null != Api.Instance)
			{
                _arenaSeasonRewardsURL = Api.Instance.GenerateUrl(ApiConstants.GetRewardsArenaSeason);

                Assert.IsFalse(string.IsNullOrEmpty(_arenaSeasonRewardsURL), "[ArenaCupAPICalls] GetRewardsArenaCup(), lUrl is null or empty.");

                HTTPRequest lRequest = Api.Instance.SetUpRequest(_arenaSeasonRewardsURL, Api.Methods.GET, true);
                lRequest.Timeout = TimeSpan.FromSeconds(5);

                StartCoroutine(Api.Instance.SendRequest(lRequest));
            }
		}




        public void AddEvents()
		{
			if (null != Api.Instance)
			{
				Api.Instance.OnSuccess += OnRequestSuccess;
			}
		}

		#endregion

		#region Private Methods

		/// <summary>
		/// MonoBehaviour method used to link OnRequestSuccess to API
		/// </summary>
		private void OnEnable()
        {
            if (null != Api.Instance)
            {
                Api.Instance.OnSuccess += OnRequestSuccess;
            }
        }

        /// <summary>
        /// MonoBehaviour method used to unlink OnRequestSuccess from API
        /// </summary>
        private void OnDisable()
        {
            if (null != Api.Instance)
            {
                Api.Instance.OnSuccess -= OnRequestSuccess;
            }
        }

        /// <summary>
        /// Send a request to API
        /// </summary>
        /// <param name="pUrl"></param>
        /// <param name="pMethod"></param>
        /// <param name="pAuthenticationRequired"></param>
        private void SendRequest(string pUrl, Api.Methods pMethod, bool pAuthenticationRequired)
		{
			HTTPRequest lRequest = Api.Instance.SetUpRequest(pUrl, pMethod, pAuthenticationRequired);

			Assert.IsNotNull(lRequest, "[LeaderBoardAPICalls] SendRequest(), lRequest is null.");
            lRequest.Timeout = TimeSpan.FromSeconds(5);

            StartCoroutine(Api.Instance.SendRequest(lRequest));
		}

        /// <summary>
        /// Callback invoke when request from API was done successfully
        /// </summary>
        /// <param name="pCurrentUrl">string</param>
        /// <param name="pMethod">HTTPMethods</param>
        /// <param name="pData">string</param>
        private void OnRequestSuccess(string pCurrentUrl, HTTPMethods pMethod, string pData)
        {
            if (true == pCurrentUrl.Equals(_arenaSeasonRewardsURL))
            {
                _arenaSeasonMain.ParseRewardsArenaSeason(pData);
            }

        }

        #endregion
    }
}

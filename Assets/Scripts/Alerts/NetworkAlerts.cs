﻿using Sportfaction.CasualFantasy.Services;

using Photon.Pun;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Sportfaction.CasualFantasy.Alerts
{
	/// <summary>
	/// Description: Handle all network alerts
	/// Author: Rémi Carreira
	/// </summary>
	public sealed class NetworkAlerts : MonoBehaviour
	{
		#region Private Methods

		/// <summary>
		/// Unity Method | Start listening network events
		/// </summary>
		private	void	OnEnable()
		{
			NetworkChecker.StartListeningNetworkConnectivity(OnNetworkConnectivityChanged);
		}

		/// <summary>
		/// Unity Method | Stop listening network events
		/// </summary>
		private	void	OnDisable()
		{
			NetworkChecker.StopListeningNetworkConnectivity(OnNetworkConnectivityChanged);
		}

		/// <summary>
		/// Event callback | Show alert if network is not connected
		/// </summary>
		/// <param name="pIsConnected">True if network is connected</param>
		private	void	OnNetworkConnectivityChanged(bool pIsConnected)
		{
			if (false == pIsConnected)
			{
				ShowNetworkConnectivityAlert();
			}
		}

		/// <summary>
		/// Event callback | Show alert if network is not connected
		/// </summary>
		/// <param name="pButton">Button name pressed</param>
		private	void	TryAgainNetworkConnectivity(string pButton)
		{
			// Usefull for alpha test (To Remove)

			if (PhotonNetwork.IsConnected)
			{
				PhotonNetwork.Disconnect();
			}

			SceneManager.LoadScene("Splashscreen");

			/*

			if (false == NetworkChecker.IsConnected)
			{
				ShowNetworkConnectivityAlert();
			}

			*/
		}

		private	void	ShowNetworkConnectivityAlert()
        {
			//AlertDialogs.Instance.ShowAlert(
   //             I2.Loc.LocalizationManager.GetTranslation("NetworkConnectivityAlertTitle"),
   //             I2.Loc.LocalizationManager.GetTranslation("NetworkConnectivityAlertMessage"),
   //             I2.Loc.LocalizationManager.GetTranslation("NetworkConnectivityAlertButton"),
   //             TryAgainNetworkConnectivity
   //         );
		}

		#endregion
	}
}
﻿using Sportfaction.CasualFantasy.Manager.Profile;

using SportFaction.CasualFootballEngine.TeamService.Enum;

using Photon.Pun;
using TMPro;
using UnityEngine;
using UnityEngine.Assertions;

namespace Sportfaction.CasualFantasy.Debugs.UI
{
	/// <summary>
	/// Description: Class check manager's team
	/// Author: Rémi Carreira
	/// </summary>
	public sealed class ManagerTeamChecker : MonoBehaviour
	{
		#region Variables

		[Header("Parameters")]
		[SerializeField, Tooltip("Text mesh used to display the manager team")]
		private	TextMeshProUGUI		_teamTextMesh	=	null;

		#endregion

		#region Public Methods

		/// <summary>
		/// Check manager team
		/// </summary>
		[ContextMenu("Check Manager Team")]
		public	void	CheckManagerTeam()
		{
			if (PhotonNetwork.IsConnected)
			{
				if (null != UserData.Instance)
				{
					if (UserData.Instance.PVPTeam == eTeam.TEAM_A)
					{
						SetTextMesh("Team A", Color.cyan);
					}
					else
					{
						SetTextMesh("Team B", Color.magenta);
					}
				}
			}
			else
			{
				SetTextMesh("Team A", Color.cyan);
			}
		}

		#endregion

		#region Private Methods

		/// <summary>
		/// MonoBehaviour method used to check manager team
		/// </summary>
		private	void	OnEnable()
		{
			CheckManagerTeam();
		}

		/// <summary>
		/// Set text mesh
		/// </summary>
		/// <param name="pText">Text to set</param>
		/// <param name="pColor">Color to set</param>
		private	void	SetTextMesh(string pText, Color pColor)
		{
			Assert.IsNotNull(_teamTextMesh, "[ManagerTeamChecker] SetTextMesh(), _teamTextMesh is null.");

			_teamTextMesh.text	=	pText;
			_teamTextMesh.color	=	pColor;
		}

		#endregion
	}
}
﻿using Photon.Pun;
using TMPro;
using UnityEngine.Assertions;
using UnityEngine.UI;
using UnityEngine;

namespace Sportfaction.CasualFantasy.Debugs.UI
{
    /// <summary>
    /// Description: Class check photon master status
    /// Author: Rémi Carreira
    /// </summary>
    [RequireComponent(typeof(PhotonView))]
	public sealed class PhotonMasterChecker : MonoBehaviourPunCallbacks
	{
		#region Variables

		[Header("Parameters")]
		[SerializeField, Tooltip("Toggle used to indicate if is master")]
		private	Toggle				_masterToggle	=	null;
		[SerializeField, Tooltip("Text mesh pro gui used to indicate if is master")]
		private	TextMeshProUGUI		_masterTextMesh	=	null;

		#endregion

		#region Public Methods
		
		[ContextMenu("Check Player Status")]
		public	void	CheckPlayerStatus()
		{
			if (true == PhotonNetwork.IsConnected && false == PhotonNetwork.IsMasterClient)
			{
				UpdateToggle(false);

				UpdateTextMesh("Client");

				UpdateTextMeshColor(Color.red);
			}
			else
			{
				UpdateToggle(true);

				UpdateTextMesh("Master");

				UpdateTextMeshColor(Color.green);
			}
		}

		#endregion

		#region Private Methods

		/// <summary>
		/// MonoBehaviour used to check if is master
		/// </summary>
		private	void	OnEnable()
		{
			CheckPlayerStatus();
		}

		/// <summary>
		/// MonoBehaviour method called every frame if the Behaviour is enabled
		/// </summary>
		private void LateUpdate()
		{
			CheckPlayerStatus();
		}

		/// <summary>
		/// Update toggle value
		/// </summary>
		/// <param name="pIsOn">Is toggle on or off?</param>
		private	void	UpdateToggle(bool pIsOn)
		{
			Assert.IsNotNull(_masterToggle, "[PhotonMasterChecker] UpdateToggle(), _masterToggle is null.");

			_masterToggle.isOn	=	pIsOn;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="pText"></param>
		private	void	UpdateTextMesh(string pText)
		{
			Assert.IsNotNull(_masterTextMesh, "[PhotonMasterChecker] UpdateTextMesh(), _masterTextMesh is null.");

			_masterTextMesh.text	=	pText;
		}

		/// <summary>
		/// Update the color of the text mesh
		/// </summary>
		/// <param name="pNewColor">New color of this text mesh</param>
		private	void	UpdateTextMeshColor(Color pNewColor)
		{
			Assert.IsNotNull(_masterTextMesh, "[PhotonMasterChecker] UpdateTextMeshColor(), _masterTextMesh is null.");

			_masterTextMesh.color	=	pNewColor;
		}

		#endregion
	}
}

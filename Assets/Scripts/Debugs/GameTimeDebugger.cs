﻿using Photon.Pun;
using UnityEngine;
using UnityEngine.Assertions;

namespace Sportfaction.CasualFantasy.Debugs
{
	/// <summary>
	/// Description: Class handles game time
	/// Author: Rémi Carreira
	/// </summary>
	[RequireComponent(typeof(PhotonView))]
	public class GameTimeDebugger : MonoBehaviourPun
	{
		#region Public Methods

		/// <summary>
		/// Set TimeScale
		/// </summary>
		/// <param name="pValue">New value of the time scale</param>
		public	void	SetTimeScale(float pValue)
		{
			if (PhotonNetwork.IsConnected)
			{
				Assert.IsNotNull(photonView, "[GameTimeDebugger] SetTimeScale(), photonView is null.");

				photonView.RPC("RPC_SetTimeScale", RpcTarget.All, pValue);
			}
			else
			{
				RPC_SetTimeScale(pValue);
			}
		}

		#endregion

		#region Private Methods

		/// <summary>
		/// RPC used to set time scale of the game
		/// </summary>
		/// <param name="pValue">New value of the time scale</param>
		[PunRPC]
		private	void	RPC_SetTimeScale(float pValue)
		{
			Time.timeScale	=	pValue;
		}

		#endregion
	}
}

﻿using Sportfaction.CasualFantasy.PVP.Gameplay.Matchs;

using UnityEngine;

namespace Sportfaction.CasualFantasy.Debugs
{
	/// <summary>
	/// Description: Class debug match's timers
	/// Author: Rémi Carreira
	/// </summary>
	public class MatchTimersDebugger : MonoBehaviour
	{
		#region Variables

		[Header("Informations")]
		[SerializeField, ReadOnly, Tooltip("Component handles match's timers")]
		private	MatchTimers		_matchTimers	=	null;

		#endregion

		#region Public Methods

		/// <summary>
		/// Resume match timers
		/// </summary>
		[ContextMenu("Resume")]
		public	void	Resume()
		{
			RetrieveMatchTimers();

			if (null != _matchTimers)
			{
				//_matchTimers.ResumeCurrentTimer();
			}
		}

		/// <summary>
		/// Pause match timers
		/// </summary>
		[ContextMenu("Pause")]
		public	void	Pause()
		{
			RetrieveMatchTimers();

			if (null != _matchTimers)
			{
				//_matchTimers.PauseCurrentTimer();
			}
		}

		#endregion

		#region Private Methods

		/// <summary>
		/// Retrieve match timers component
		/// </summary>
		private	void	RetrieveMatchTimers()
		{
			if (null == _matchTimers)
			{
				_matchTimers	=	GameObject.FindObjectOfType<MatchTimers>();

			}
		}

		#endregion
	}
}

﻿
using System.Collections;
using DG.Tweening;
using Sportfaction.CasualFantasy.ArenaSeason;
using Sportfaction.CasualFantasy.Manager.Profile;
using SportFaction.CasualFootballEngine.PlayerService.Model;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Sportfaction.CasualFantasy.Meta
{
    /// <summary>
    /// Description: Loot Box Card
    /// Author: Antoine de Lachèze-Murel
    /// </summary>
    public class ArenaSeasonRewardItem : MonoBehaviour, IPointerClickHandler
    {

        private int _lIndex;
        #region Public Methods

        

        /// <summary>
        /// Init
        /// </summary>
        public bool Init(ArenaSeasonDataReward pData, int pBracketMin, int pBracketMax, int pStep, int pIndex, bool pCanGetReward)
        {

            _lIndex = pIndex;
            bool lNeedToGetReward = false;
            // Debug.Log("ADLM = " + pData.Name + " / " + pBracketMin + " / " + pBracketMax + " / " + pStep + " / ");
            int pStepBefore = pStep - ((pStep - pBracketMin) * 2);

    
            Sprite[] lSpritesMenu = Resources.LoadAll<Sprite>("Sprites/Meta/Menu_3_SS");
            Sprite[] lSpritesMenuBis = Resources.LoadAll<Sprite>("Sprites/Meta/Menu_2_SS");
            Sprite[] lSpritesShop = Resources.LoadAll<Sprite>("Sprites/Meta/Shop");

            transform.Find("RewardDone_Grp/Name_Txt").GetComponent<TMPro.TMP_Text>().text = I2.Loc.LocalizationManager.GetTranslation(pData.Name);
            transform.Find("RewardCurrent_Grp/Name_Txt").GetComponent<TMPro.TMP_Text>().text = I2.Loc.LocalizationManager.GetTranslation(pData.Name);
            transform.Find("RewardNext_Grp/Name_Txt").GetComponent<TMPro.TMP_Text>().text = I2.Loc.LocalizationManager.GetTranslation(pData.Name);

            transform.Find("RewardDone_Grp/Lootbox_Grp").gameObject.SetActive(false);
            transform.Find("RewardCurrent_Grp/Lootbox_Grp").gameObject.SetActive(false);
            transform.Find("RewardNext_Grp/Lootbox_Grp").gameObject.SetActive(false);

            if (pData.BigBox > 0)
            {
                transform.Find("RewardDone_Grp/Lootbox_Grp").gameObject.SetActive(true);
                transform.Find("RewardCurrent_Grp/Lootbox_Grp").gameObject.SetActive(true);
                transform.Find("RewardNext_Grp/Lootbox_Grp").gameObject.SetActive(true);

                Sprite lSprite = UtilityMethods.GetSprite(lSpritesShop, "UltraStadibox_Icone");
                transform.Find("RewardDone_Grp/Lootbox_Grp/Lootbox_Img").GetComponent<Image>().sprite = lSprite;
                transform.Find("RewardCurrent_Grp/Lootbox_Grp/Lootbox_Img").GetComponent<Image>().sprite = lSprite;
                transform.Find("RewardNext_Grp/Lootbox_Grp/Lootbox_Img").GetComponent<Image>().sprite = lSprite;

                transform.Find("RewardDone_Grp/Lootbox_Grp/Lootbox_Txt").GetComponent<TMPro.TMP_Text>().text = "x" + pData.BigBox.ToString();
                transform.Find("RewardCurrent_Grp/Lootbox_Grp/Lootbox_Txt").GetComponent<TMPro.TMP_Text>().text = "x" + pData.BigBox.ToString();
                transform.Find("RewardNext_Grp/Lootbox_Grp/Lootbox_Txt").GetComponent<TMPro.TMP_Text>().text = "x" + pData. BigBox.ToString();
            }
            else if (pData.MediumBox > 0)
            {
                transform.Find("RewardDone_Grp/Lootbox_Grp").gameObject.SetActive(true);
                transform.Find("RewardCurrent_Grp/Lootbox_Grp").gameObject.SetActive(true);
                transform.Find("RewardNext_Grp/Lootbox_Grp").gameObject.SetActive(true);

                Sprite lSprite = UtilityMethods.GetSprite(lSpritesShop, "SuperStadibox_Icone");
                transform.Find("RewardDone_Grp/Lootbox_Grp/Lootbox_Img").GetComponent<Image>().sprite = lSprite;
                transform.Find("RewardCurrent_Grp/Lootbox_Grp/Lootbox_Img").GetComponent<Image>().sprite = lSprite;
                transform.Find("RewardNext_Grp/Lootbox_Grp/Lootbox_Img").GetComponent<Image>().sprite = lSprite;

                transform.Find("RewardDone_Grp/Lootbox_Grp/Lootbox_Txt").GetComponent<TMPro.TMP_Text>().text = "x" + pData.MediumBox.ToString();
                transform.Find("RewardCurrent_Grp/Lootbox_Grp/Lootbox_Txt").GetComponent<TMPro.TMP_Text>().text = "x" + pData.MediumBox.ToString();
                transform.Find("RewardNext_Grp/Lootbox_Grp/Lootbox_Txt").GetComponent<TMPro.TMP_Text>().text = "x" + pData.MediumBox.ToString();
            }
            else if(pData.SmallBox > 0)
            {
                transform.Find("RewardDone_Grp/Lootbox_Grp").gameObject.SetActive(true);
                transform.Find("RewardCurrent_Grp/Lootbox_Grp").gameObject.SetActive(true);
                transform.Find("RewardNext_Grp/Lootbox_Grp").gameObject.SetActive(true);

                Sprite lSprite = UtilityMethods.GetSprite(lSpritesMenuBis, "StadiBox");
                transform.Find("RewardDone_Grp/Lootbox_Grp/Lootbox_Img").GetComponent<Image>().sprite = lSprite;
                transform.Find("RewardCurrent_Grp/Lootbox_Grp/Lootbox_Img").GetComponent<Image>().sprite = lSprite;
                transform.Find("RewardNext_Grp/Lootbox_Grp/Lootbox_Img").GetComponent<Image>().sprite = lSprite;

                transform.Find("RewardDone_Grp/Lootbox_Grp/Lootbox_Txt").GetComponent<TMPro.TMP_Text>().text = "x" + pData.SmallBox.ToString();
                transform.Find("RewardCurrent_Grp/Lootbox_Grp/Lootbox_Txt").GetComponent<TMPro.TMP_Text>().text = "x" + pData.SmallBox.ToString();
                transform.Find("RewardNext_Grp/Lootbox_Grp/Lootbox_Txt").GetComponent<TMPro.TMP_Text>().text = "x" + pData.SmallBox.ToString();
            }
            else if (null != pData.Player && false == string.IsNullOrEmpty(pData.Player.Profile.PlayerId))
            {
                transform.Find("RewardDone_Grp/MinionPlayers_Card").gameObject.SetActive(true);
                transform.Find("RewardDone_Grp/MinionPlayers_Card").GetComponent<MinionsPlayersScrollviewItem>().Init(pData.Player);
                transform.Find("RewardDone_Grp/MinionPlayers_Card").GetComponent<Image>().raycastTarget = false;
                transform.Find("RewardDone_Grp/MinionPlayers_Card/Fade_Img").gameObject.SetActive(false);
                transform.Find("RewardCurrent_Grp/MinionPlayers_Card").gameObject.SetActive(true);
                transform.Find("RewardCurrent_Grp/MinionPlayers_Card").GetComponent<MinionsPlayersScrollviewItem>().Init(pData.Player);
                transform.Find("RewardCurrent_Grp/MinionPlayers_Card").GetComponent<Image>().raycastTarget = false;
                transform.Find("RewardCurrent_Grp/MinionPlayers_Card/Fade_Img").gameObject.SetActive(false);
                transform.Find("RewardNext_Grp/MinionPlayers_Card").gameObject.SetActive(true);
                transform.Find("RewardNext_Grp/MinionPlayers_Card").GetComponent<MinionsPlayersScrollviewItem>().Init(pData.Player);
                transform.Find("RewardNext_Grp/MinionPlayers_Card").GetComponent<Image>().raycastTarget = false;
                transform.Find("RewardNext_Grp/MinionPlayers_Card/Fade_Img").gameObject.SetActive(false);
            }

            if (pData.Hard > 0)
            {
                transform.Find("RewardDone_Grp/Hard_Grp").gameObject.SetActive(true);
                transform.Find("RewardDone_Grp/Hard_Grp/Hard_Txt").GetComponent<TMPro.TMP_Text>().text = "x" + pData.Hard.ToString();
                transform.Find("RewardCurrent_Grp/Hard_Grp/Hard_Txt").GetComponent<TMPro.TMP_Text>().text = "x" + pData.Hard.ToString();
                transform.Find("RewardNext_Grp/Hard_Grp/Hard_Txt").GetComponent<TMPro.TMP_Text>().text = "x" + pData.Hard.ToString();
            }
            else
            {
                transform.Find("RewardDone_Grp/Hard_Grp").gameObject.SetActive(false);
                transform.Find("RewardCurrent_Grp/Hard_Grp").gameObject.SetActive(false);
                transform.Find("RewardNext_Grp/Hard_Grp").gameObject.SetActive(false);
            }

        


            //if (pData.FragmentBox > 0)
            //{
            //    transform.Find("RewardDone_Grp/Fragments_Grp").gameObject.SetActive(true);
            //    transform.Find("RewardDone_Grp/Fragments_Grp/Fragments_Txt").GetComponent<TMPro.TMP_Text>().text = "x" + pData.FragmentBox.ToString();
            //    transform.Find("RewardCurrent_Grp/Fragments_Grp/Fragments_Txt").GetComponent<TMPro.TMP_Text>().text = "x" + pData.FragmentBox.ToString();
            //    transform.Find("RewardNext_Grp/Fragments_Grp/Fragments_Txt").GetComponent<TMPro.TMP_Text>().text = "x" + pData.FragmentBox.ToString();
            //}
            //else
            //{
            //    transform.Find("RewardDone_Grp/Fragments_Grp").gameObject.SetActive(false);
            //}

            transform.Find("GaugeBlack_Grp/CurrentPosition_Grp/Trophy_Txt").GetComponent<TMPro.TMP_Text>().text = UserData.Instance.Profile.Score.ToString();
            transform.Find("GaugeBlack_Grp/Trophy_Txt").GetComponent<TMPro.TMP_Text>().text = pData.MaxScore.ToString();


            if(pStep <= UserData.Instance.Profile.MaxScore)
            {
                transform.Find("RewardDone_Grp").gameObject.SetActive(true);
                transform.Find("RewardCurrent_Grp").gameObject.SetActive(false);
                transform.Find("RewardNext_Grp").gameObject.SetActive(false);

                if(UserData.Instance.Profile.LastArenaSeasonReward < (_lIndex+1) && UserData.Instance.Profile.MaxScore >= pData.MaxScore)
                {
                    if(true == pCanGetReward)
                    {
                        transform.Find("RewardDone_Grp/Check_Img").gameObject.SetActive(false);
                        transform.Find("RewardDone_Grp/GetRewardArenaSeason_Btn").gameObject.SetActive(true);

                    }
                    else
                    {
                        transform.Find("RewardDone_Grp/Check_Img").gameObject.SetActive(false);
                        transform.Find("RewardDone_Grp/GetRewardArenaSeason_Btn").gameObject.SetActive(false);

                    }

                    lNeedToGetReward = true;

                }
                else
                {
                    transform.Find("RewardDone_Grp/Check_Img").gameObject.SetActive(true);
                    transform.Find("RewardDone_Grp/GetRewardArenaSeason_Btn").gameObject.SetActive(false);
                }
            }
            else if (UserData.Instance.Profile.MaxScore < pStep && UserData.Instance.Profile.MaxScore >= pStepBefore)
            {
                transform.Find("RewardDone_Grp").gameObject.SetActive(false);
                transform.Find("RewardCurrent_Grp").gameObject.SetActive(true);
                transform.Find("RewardNext_Grp").gameObject.SetActive(false);
            }
            else
            {
                transform.Find("RewardDone_Grp").gameObject.SetActive(false);
                transform.Find("RewardCurrent_Grp").gameObject.SetActive(false);
                transform.Find("RewardNext_Grp").gameObject.SetActive(true);
                transform.Find("GaugeBlack_Grp/CurrentPosition_Grp").gameObject.SetActive(true);
            }

            if(UserData.Instance.Profile.Score >= pBracketMin && UserData.Instance.Profile.Score <= pBracketMax)
            {
                if(pIndex == 0 && UserData.Instance.Profile.Score < pStep)
                {
                    transform.Find("GaugeBlack_Grp/CurrentPosition_Grp").gameObject.SetActive(false);
                }
                else
                {
                    transform.Find("GaugeBlack_Grp/CurrentPosition_Grp").gameObject.SetActive(true);
                }
                transform.GetComponent<Canvas>().sortingOrder = 2;
            }
            else
            {
                transform.Find("GaugeBlack_Grp/CurrentPosition_Grp").gameObject.SetActive(false);
                transform.GetComponent<Canvas>().sortingOrder = 1;
            }

            transform.Find("GaugeBlack_Grp/GreenGauge_Img").gameObject.SetActive(true);
            int lWidthGreenGauge = 0;

            // 448 width
            if (UserData.Instance.Profile.Score >= pBracketMin && UserData.Instance.Profile.Score < pStep)
            {
                lWidthGreenGauge = 224 - ((pStep - UserData.Instance.Profile.Score) * 100 / (pStep - pBracketMin)) * 224 / 100;
            }
            else if(UserData.Instance.Profile.Score >= pStep && UserData.Instance.Profile.Score <= pBracketMax)
            {
                if(pBracketMax - pStep > 0)
                {
                    lWidthGreenGauge = 224 + ((UserData.Instance.Profile.Score - pStep) * 100 / (pBracketMax - pStep)) * 224 / 100;
                }
                else
                {
                    lWidthGreenGauge = 224;
                }
            }
            else if(UserData.Instance.Profile.Score > pBracketMax)
            {
                lWidthGreenGauge = 448;
            }
            else
            {
                transform.Find("GaugeBlack_Grp/GreenGauge_Img").gameObject.SetActive(false);
                lWidthGreenGauge = 0;
            }



            RectTransform lRectGreenGauge_Img = transform.Find("GaugeBlack_Grp/GreenGauge_Img").transform.GetComponent<RectTransform>();
            lRectGreenGauge_Img.sizeDelta = new Vector2(lWidthGreenGauge, lRectGreenGauge_Img.rect.height);


            RectTransform lRectCurrentPosition_Grp = transform.Find("GaugeBlack_Grp/CurrentPosition_Grp").transform.GetComponent<RectTransform>();
            lRectCurrentPosition_Grp.anchoredPosition = new Vector2(lWidthGreenGauge - 152, lRectCurrentPosition_Grp.anchoredPosition.y);



            transform.Find("GaugeBlack_Grp/RedGauge_Img").gameObject.SetActive(true);
            int lWidthRedGauge = 0;
            // 448 width
            if (UserData.Instance.Profile.MaxScore >= pBracketMin && UserData.Instance.Profile.MaxScore < pStep)
            {
                lWidthRedGauge = 224 - ((pStep - UserData.Instance.Profile.MaxScore) * 100 / (pStep - pBracketMin)) * 224 / 100;

                Sprite lSprite = UtilityMethods.GetSprite(lSpritesMenu, "CheckPoint_Off");
                transform.Find("GaugeBlack_Grp/CheckPoint_Img").GetComponent<Image>().sprite = lSprite;
            }
            else if (UserData.Instance.Profile.MaxScore >= pStep && UserData.Instance.Profile.MaxScore < pBracketMax)
            {
                Sprite lSprite = UtilityMethods.GetSprite(lSpritesMenu, "CheckPoint_On");
                transform.Find("GaugeBlack_Grp/CheckPoint_Img").GetComponent<Image>().sprite = lSprite;
                if (pBracketMax - pStep > 0)
                {
                    lWidthRedGauge = 224 + ((UserData.Instance.Profile.MaxScore - pStep) * 100 / (pBracketMax - pStep)) * 224 / 100;
                }
                else
                {
                    lWidthRedGauge = 224;
                }
            }
            else if (UserData.Instance.Profile.MaxScore > pBracketMax)
            {
                Sprite lSprite = UtilityMethods.GetSprite(lSpritesMenu, "CheckPoint_On");
                transform.Find("GaugeBlack_Grp/CheckPoint_Img").GetComponent<Image>().sprite = lSprite;

                lWidthRedGauge = 448;
            }
            else
            {
                transform.Find("GaugeBlack_Grp/RedGauge_Img").gameObject.SetActive(false);
                lWidthRedGauge = 0;

                Sprite lSprite = UtilityMethods.GetSprite(lSpritesMenu, "CheckPoint_Off");
                transform.Find("GaugeBlack_Grp/CheckPoint_Img").GetComponent<Image>().sprite = lSprite;
            }

            RectTransform lRectRedGauge_Img = transform.Find("GaugeBlack_Grp/RedGauge_Img").transform.GetComponent<RectTransform>();
            lRectRedGauge_Img.sizeDelta = new Vector2(lWidthRedGauge, lRectRedGauge_Img.rect.height);




            return lNeedToGetReward;


        }

        public void OnPointerClick(PointerEventData pointerEventData)
        {

            if (DefaultUIAnimation.Instance.TimePressed < 0.15)
            {
                UI.Instance.StartLoader();
                transform.Find("RewardDone_Grp/GetRewardArenaSeason_Btn").gameObject.SetActive(false);
                MetaData.Instance.LootMain.UnlockRewardArenaSeason(_lIndex + 1);
                UserData.Instance.Profile.LastArenaSeasonReward = _lIndex + 1;
            }
        }


        #endregion

    }
}

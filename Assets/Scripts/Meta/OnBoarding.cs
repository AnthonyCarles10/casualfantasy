﻿
using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using Sportfaction.CasualFantasy.Loot;
using Sportfaction.CasualFantasy.Manager.Profile;
using UnityEngine;
using UnityEngine.UI;

namespace Sportfaction.CasualFantasy.Meta
{
    /// <summary>
    /// Description: OnBoarding
    /// Author: Antoine de Lachèze-Murel
    /// </summary>
    public class OnBoarding : MonoBehaviour
    {
        public static OnBoarding Instance;

        /* POPIN */
        private GameObject _currentPopin_Gob = null;

        public GameObject CreateManager1Popin_Gob;
        public GameObject Home1Popin_Gob;
        public GameObject Home2Popin_Gob;
        public GameObject Home3Popin_Gob;
        public GameObject Home4Popin_Gob;
        public GameObject Formation1Popin_Gob;
        public GameObject Formation2Popin_Gob;
        public GameObject Fantasy1Popin_Gob;
        public GameObject Matchmaking1Popin_Gob;
        public GameObject BlackFade_Gob;
        public GameObject LootBox_Gob;

        /* BTN */

        private GameObject _currentBtn_Gob = null;

        public GameObject MinionsPlayers_Btn;
        public GameObject CloseMinionsPlayers_Btn;
        public GameObject Fantasy_Btn;
        public GameObject CloseFantasy_Btn;
        public GameObject Match_Btn;
        public GameObject MatchmakingClose_Btn;

        Sequence Sequence = null;

        [SerializeField]
        private TMPro.TMP_InputField _pseudo;

        private bool _onGetProfileUsername;



  

        #region Game Event Methods

        public void OnGetProfile_GE()
        {
            if(false == _onGetProfileUsername)
            {
                Home1Popin_Gob.SetActive(false);
                Home1Popin_Gob.transform.Find("Popin_Grp/Message_Txt").GetComponent<TMPro.TMP_Text>().text = String.Format(I2.Loc.LocalizationManager.GetTranslation("onboarding_home_1"), UserData.Instance.Profile.Pseudo);

                GameObject Home_Grp = UI.Instance.PrefabsIdxArray_Gob[Array.IndexOf(UI.Instance.PrefabsIdxArray_Str, "Home_Grp")];
                Home_Grp.transform.Find("PlayerStats_Grp/Name_Grp/Username_Txt").GetComponent<TMPro.TMP_Text>().text = UserData.Instance.Profile.Pseudo;

                StartCoroutine(OpenPopin("Home1"));

                _onGetProfileUsername = true;
            }
       
        }

        public void OnLootboxIsClosed_GE()
        {
            Debug.Log("Onboarding OnLootboxIsClosed_GE");
            GameObject LootBox_Grp = UI.Instance.PrefabsIdxArray_Gob[Array.IndexOf(UI.Instance.PrefabsIdxArray_Str, "LootBox_Grp")];
            LootBox_Grp.SetActive(false);

            GameObject MinionsPlayers_Grp = UI.Instance.PrefabsIdxArray_Gob[Array.IndexOf(UI.Instance.PrefabsIdxArray_Str, "MinionsPlayers_Grp")];
            MinionsPlayers_Grp.SetActive(true);
            UI.Instance.CurrentActive_Go = MinionsPlayers_Grp;

            MetaData.Instance.GetSquadPlayers();

            StartCoroutine(OpenPopin("Formation2"));
        }

        public void OnGetLootData_GE()
        {

            LootData lData = MetaData.Instance.LootMain.LootData;

            if (lData.Type == "init")
            {
                MetaData.Instance.Filter = true;
                MetaData.Instance.GetPlayersList();

                MetaData.Instance.SquadMain.GetTeam();
                StartCoroutine(UI.Instance.GoToLootBox());

                GameObject MinionsPlayers_Grp = UI.Instance.PrefabsIdxArray_Gob[Array.IndexOf(UI.Instance.PrefabsIdxArray_Str, "MinionsPlayers_Grp")];
                MinionsPlayers_Grp.SetActive(false);

                if (lData.MediumBox.Count == 0 && lData.BigBox.Count == 0)
                {
                    LootBox_Gob.SetActive(true);
                    LootBox.Instance.Init(lData, false);
                }


            }


        }


        #endregion

        #region Public Methods

        public void Init()
        {
            CreateManager1Popin_Gob.GetComponent<RectTransform>().sizeDelta = new Vector2(0, 0); 
            CreateManager1Popin_Gob.SetActive(false);

            Home1Popin_Gob.SetActive(false);

            Home2Popin_Gob.SetActive(false);

            Home3Popin_Gob.SetActive(false);

            Home4Popin_Gob.SetActive(false);

            Formation1Popin_Gob.SetActive(false);

            Formation2Popin_Gob.SetActive(false);

            BlackFade_Gob.SetActive(false);

            Matchmaking1Popin_Gob.SetActive(false);

            Fantasy1Popin_Gob.SetActive(false);

            StartCoroutine(OpenPopin("CreateManager1"));
        }

        public IEnumerator SetPseudo()
        {
            yield return new WaitForSeconds(0f); // wait button animation

            if (string.IsNullOrEmpty(_pseudo.text))
            {

            }
            else
            {
                Dictionary<string, object> body = new Dictionary<string, object>
                {
                    { "pseudo", _pseudo.text }
                };

                MetaData.Instance.UserMain.PatchUser(body);
            }
        }

        public IEnumerator OpenInitBox()
        {
            if (_currentPopin_Gob != null)
            {
                StartCoroutine(ClosePopin());
            }
            yield return new WaitForSeconds(0.15f); // wait popin close animation

            MetaData.Instance.LootMain.AskManagerLoot("init");
        }

        public IEnumerator ActiveButton(string pName)
        {
            GetCurrentBtnGob(pName);

            if (_currentPopin_Gob != null)
            {
                StartCoroutine(ClosePopin());
            }
            yield return new WaitForSeconds(0.15f); // wait popin close animation

            _currentBtn_Gob.name = "OnBoarding_" + pName;
            _currentBtn_Gob.transform.Find("Notif_Grp").gameObject.SetActive(true);

            Canvas lCanvas = _currentBtn_Gob.AddComponent<Canvas>() as Canvas;
            lCanvas.overrideSorting = true;
            lCanvas.sortingOrder = 1;

            GraphicRaycaster lGraphicRaycaster = _currentBtn_Gob.AddComponent<GraphicRaycaster>() as GraphicRaycaster;

            BlackFade_Gob.SetActive(true);

            Sequence = DOTween.Sequence();
            Sequence.Insert(0.5f, _currentBtn_Gob.transform.DOScale(1.2f, 0.5f));
            Sequence.Insert(1f, _currentBtn_Gob.transform.DOScale(1f, 0.5f));
            Sequence.SetLoops(-1);
        }

        public IEnumerator OnClickButton(string pName, string pOpenPopinName = "" , string pActiveButtonName = "")
        {
            yield return new WaitForSeconds(1f); // wait 
            BlackFade_Gob.SetActive(false);
            _currentBtn_Gob.transform.Find("Notif_Grp").gameObject.SetActive(false);
            GraphicRaycaster lGraphicRaycaster = _currentBtn_Gob.GetComponent<GraphicRaycaster>();
            Destroy(lGraphicRaycaster);
            Canvas lCanvas = _currentBtn_Gob.GetComponent<Canvas>();
            Destroy(lCanvas);
            _currentBtn_Gob.name = pName;
            Sequence.Kill();

            if (false == string.IsNullOrEmpty(pOpenPopinName))
            {
                StartCoroutine(OpenPopin(pOpenPopinName));
            }
            else if (false == string.IsNullOrEmpty(pActiveButtonName))
            {
                StartCoroutine(ActiveButton(pActiveButtonName));
            }
        }

        public IEnumerator OpenPopin(string pName)
        {
            yield return new WaitForSeconds(0.1f); // wait button animation

            if(_currentPopin_Gob != null)
            {
                StartCoroutine(ClosePopin());
            }
            yield return new WaitForSeconds(0.15f); // wait popin close animation
            GetCurrentGob(pName);

            _currentPopin_Gob.SetActive(true);
            _currentPopin_Gob.transform.Find("Popin_Grp").transform.localPosition = new Vector3(0, 0, 0);
            _currentPopin_Gob.transform.Find("Popin_Grp").GetComponent<Animator>().SetTrigger("Open");
        }

        public IEnumerator ClosePopin()
        {
            _currentPopin_Gob.transform.Find("Popin_Grp").GetComponent<Animator>().SetTrigger("Close");

            yield return new WaitForSeconds(0.15f); // wait popin close animation
            _currentPopin_Gob.SetActive(false);
            _currentPopin_Gob = null;
        }


        #endregion

        void GetCurrentGob(string pName)
        {

            switch (pName)
            {
                case "CreateManager1":
                    _currentPopin_Gob = CreateManager1Popin_Gob;
                    break;
                case "Home1":
                    _currentPopin_Gob = Home1Popin_Gob;
                    GameObject Home_Grp = UI.Instance.PrefabsIdxArray_Gob[Array.IndexOf(UI.Instance.PrefabsIdxArray_Str, "Home_Grp")];
                    DefaultUIAnimation.Instance.FadeInCanvasGroup(Home_Grp.gameObject);
                    UI.Instance.CurrentActive_Go = Home_Grp;
                    break;
                case "Home2":
                    _currentPopin_Gob = Home2Popin_Gob;
                    break;
                case "Formation1":
                    _currentPopin_Gob = Formation1Popin_Gob;
                    break;
                case "Formation2":
                    _currentPopin_Gob = Formation2Popin_Gob;
                    break;
                case "Home3":
                    _currentPopin_Gob = Home3Popin_Gob;
                    break;
                case "Home4":
                    _currentPopin_Gob = Home4Popin_Gob;
                    break;
                case "Fantasy1":
                    _currentPopin_Gob = Fantasy1Popin_Gob;
                    break;
                case "Matchmaking1":
                    _currentPopin_Gob = Matchmaking1Popin_Gob;
                    break;

                }
        }

        void GetCurrentBtnGob(string pName)
        {
            switch (pName)
            {
                case "Open_MinionsPlayers_Window_Btn":
                    _currentBtn_Gob = MinionsPlayers_Btn;
                    break;
                case "Close_Minions_Players_Window_Btn":
                    _currentBtn_Gob = CloseMinionsPlayers_Btn;
                    break;
                case "Open_Fantasy_Window_Btn":
                    _currentBtn_Gob = Fantasy_Btn;
                    break;
                case "Close_Fantasy_Window_Btn":
                    _currentBtn_Gob = CloseFantasy_Btn;
                    break;
                case "Open_Match_Window_Btn":
                    _currentBtn_Gob = Match_Btn;
                    break;
            }
        }

        void Start()
        {
        }

        void Awake()
        {
            Instance = this;
        }



    }
}
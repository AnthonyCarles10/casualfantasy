﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using Sportfaction.CasualFantasy.Events;
using Sportfaction.CasualFantasy.Leaderboard;
using Sportfaction.CasualFantasy.Loot;
using Sportfaction.CasualFantasy.Manager.Profile;
using Sportfaction.CasualFantasy.Services.Notifications;
using Sportfaction.CasualFantasy.Services.Timers;
using UnityEngine;
using UnityEngine.UI;

namespace Sportfaction.CasualFantasy.Meta
{
    /// <summary>
    /// Description: Matchmaking
    /// Author: MAC
    /// </summary>
    public sealed class Home : MonoBehaviour
    {
        public static Home Instance;

        public GameObject LootBox_Gob;

        public GameObject DayVLG_Gob;
        public GameObject HourVLG_Gob;
        public GameObject MinVLG_Gob;
        public GameObject SecVLG_Gob;
        //public GameObject ArenaCupTimer_Gob;
        //public GameObject ArenaCupPopin_Gob;
        public GameObject NeedToUpdateAppPopin_Gob;
        public GameObject AppUpdatedPopin_Gob;

        public GameObject LootboxSmall_Gob;
        public GameObject LootboxMedium_Gob;

        //public GameObject NotifArenaCup_Gob;
        public GameObject NotifShop_Gob;
        public GameObject NotifMinionsPlayers_Gob;

        public GameObject Loader_Grp;

        public GameObject WarningNotEnoughPlayers_Gob;
    

        //public GameEvent OnEndArenaCupTimer;

        private bool _lootBoxIsClosed = false;


        #region Public Methods


        public void OnLocalTimersUpdated_GE()
        {

            // MEDIUM BOX (PURPLE)
            if (false == LocalTimersManager.Instance.IsLocalTimerExists("medium"))
            {
                this.transform.Find("LootBoxPurple_Grp").GetComponent<Image>().raycastTarget = true;
                this.transform.Find("LootBoxPurple_Grp/Timer_Grp").gameObject.SetActive(false);
                this.transform.Find("LootBoxPurple_Grp/Number_Img").gameObject.SetActive(true);
            }
            else
            {
                this.transform.Find("LootBoxPurple_Grp").GetComponent<Image>().raycastTarget = false;
                this.transform.Find("LootBoxPurple_Grp/Timer_Grp").gameObject.SetActive(true);
                this.transform.Find("LootBoxPurple_Grp/Number_Img").gameObject.SetActive(false);
                TimeSpan lTimeLeft = LocalTimersManager.Instance.GetTimeLeft("medium");
                this.transform.Find("LootBoxPurple_Grp/Timer_Grp/Timer_Txt").GetComponent<TMPro.TMP_Text>().text = lTimeLeft.ToString("hh") + "H "+ lTimeLeft.ToString("mm") + "min "  + lTimeLeft.ToString("ss") + "sec";
            }
            this.transform.Find("LootBoxPurple_Grp").gameObject.SetActive(true);

            //if (LocalTimersManager.Instance.IsLocalTimerExists("EndTimerArenaCup"))
            //{
            //    TimeSpan lTimeLeft = LocalTimersManager.Instance.GetTimeLeft("EndTimerArenaCup");
            //    string lTime = "";
            //    if (lTimeLeft.Days > 0)
            //    {
            //        lTime += lTimeLeft.ToString("dd") + string.Format(I2.Loc.LocalizationManager.GetTranslation("day_short")) + " ";
            //    }
            //    if (lTimeLeft.Hours > 0)
            //    {
            //        lTime += lTimeLeft.ToString("hh") + "H ";
            //    }
            //    if (lTimeLeft.Minutes > 0)
            //    {
            //        lTime += lTimeLeft.ToString("mm") + "min ";
            //    }
            //    lTime += lTimeLeft.ToString("ss") + "sec";
            //    ArenaCupTimer_Gob.transform.Find("Timer_Txt").GetComponent<TMPro.TMP_Text>().text = string.Format(I2.Loc.LocalizationManager.GetTranslation("home_arenacup_ended_title"), lTime);

            //}
            if (LocalTimersManager.Instance.IsLocalTimerExists("FranceChampionship"))
            {
                TimeSpan lTimeLeft = LocalTimersManager.Instance.GetTimeLeft("FranceChampionship");
                DayVLG_Gob.transform.GetChild(1).transform.Find("Value_Txt").GetComponent<TMPro.TMP_Text>().text = lTimeLeft.ToString("dd");
                HourVLG_Gob.transform.GetChild(1).transform.Find("Value_Txt").GetComponent<TMPro.TMP_Text>().text = lTimeLeft.ToString("hh");
                MinVLG_Gob.transform.GetChild(1).transform.Find("Value_Txt").GetComponent<TMPro.TMP_Text>().text = lTimeLeft.ToString("mm");
                SecVLG_Gob.transform.GetChild(1).transform.Find("Value_Txt").GetComponent<TMPro.TMP_Text>().text = lTimeLeft.ToString("ss");
            }

            if (LocalTimersManager.Instance.IsLocalTimerExists("ItalyChampionship"))
            {
                TimeSpan lTimeLeft = LocalTimersManager.Instance.GetTimeLeft("ItalyChampionship");
                DayVLG_Gob.transform.GetChild(2).transform.Find("Value_Txt").GetComponent<TMPro.TMP_Text>().text = lTimeLeft.ToString("dd");
                HourVLG_Gob.transform.GetChild(2).transform.Find("Value_Txt").GetComponent<TMPro.TMP_Text>().text = lTimeLeft.ToString("hh");
                MinVLG_Gob.transform.GetChild(2).transform.Find("Value_Txt").GetComponent<TMPro.TMP_Text>().text = lTimeLeft.ToString("mm");
                SecVLG_Gob.transform.GetChild(2).transform.Find("Value_Txt").GetComponent<TMPro.TMP_Text>().text = lTimeLeft.ToString("ss");
            }

            if (LocalTimersManager.Instance.IsLocalTimerExists("GermanyChampionship"))
            {
                TimeSpan lTimeLeft = LocalTimersManager.Instance.GetTimeLeft("GermanyChampionship");
                DayVLG_Gob.transform.GetChild(3).transform.Find("Value_Txt").GetComponent<TMPro.TMP_Text>().text = lTimeLeft.ToString("dd");
                HourVLG_Gob.transform.GetChild(3).transform.Find("Value_Txt").GetComponent<TMPro.TMP_Text>().text = lTimeLeft.ToString("hh");
                MinVLG_Gob.transform.GetChild(3).transform.Find("Value_Txt").GetComponent<TMPro.TMP_Text>().text = lTimeLeft.ToString("mm");
                SecVLG_Gob.transform.GetChild(3).transform.Find("Value_Txt").GetComponent<TMPro.TMP_Text>().text = lTimeLeft.ToString("ss");
            }

            if (LocalTimersManager.Instance.IsLocalTimerExists("SpainChampionship"))
            {
                TimeSpan lTimeLeft = LocalTimersManager.Instance.GetTimeLeft("SpainChampionship");
                DayVLG_Gob.transform.GetChild(4).transform.Find("Value_Txt").GetComponent<TMPro.TMP_Text>().text = lTimeLeft.ToString("dd");
                HourVLG_Gob.transform.GetChild(4).transform.Find("Value_Txt").GetComponent<TMPro.TMP_Text>().text = lTimeLeft.ToString("hh");
                MinVLG_Gob.transform.GetChild(4).transform.Find("Value_Txt").GetComponent<TMPro.TMP_Text>().text = lTimeLeft.ToString("mm");
                SecVLG_Gob.transform.GetChild(4).transform.Find("Value_Txt").GetComponent<TMPro.TMP_Text>().text = lTimeLeft.ToString("ss");
            }

            if (LocalTimersManager.Instance.IsLocalTimerExists("EnglandChampionship"))
            {
                TimeSpan lTimeLeft = LocalTimersManager.Instance.GetTimeLeft("EnglandChampionship");
                DayVLG_Gob.transform.GetChild(5).transform.Find("Value_Txt").GetComponent<TMPro.TMP_Text>().text = lTimeLeft.ToString("dd");
                HourVLG_Gob.transform.GetChild(5).transform.Find("Value_Txt").GetComponent<TMPro.TMP_Text>().text = lTimeLeft.ToString("hh");
                MinVLG_Gob.transform.GetChild(5).transform.Find("Value_Txt").GetComponent<TMPro.TMP_Text>().text = lTimeLeft.ToString("mm");
                SecVLG_Gob.transform.GetChild(5).transform.Find("Value_Txt").GetComponent<TMPro.TMP_Text>().text = lTimeLeft.ToString("ss");
            }

        }

        public void OnGetProfile_GE()
        {
            UpdateFragment();
            UpdateHard();
        }

        public void OnLootboxIsClosed_GE()
        {
            EnableRaycastAllLootbox();
            UpdateNotif();

            if (false == LocalTimersManager.Instance.IsLocalTimerExists("medium"))
            {
                this.transform.Find("LootBoxPurple_Grp").GetComponent<Image>().raycastTarget = true;
                this.transform.Find("LootBoxPurple_Grp/Timer_Grp").gameObject.SetActive(false);
                this.transform.Find("LootBoxPurple_Grp/Number_Img").gameObject.SetActive(true);
            }
            else
            {
                this.transform.Find("LootBoxPurple_Grp").GetComponent<Image>().raycastTarget = false;
                this.transform.Find("LootBoxPurple_Grp/Timer_Grp").gameObject.SetActive(true);
                this.transform.Find("LootBoxPurple_Grp/Number_Img").gameObject.SetActive(false);
                TimeSpan lTimeLeft = LocalTimersManager.Instance.GetTimeLeft("medium");
                this.transform.Find("LootBoxPurple_Grp/Timer_Grp/Timer_Txt").GetComponent<TMPro.TMP_Text>().text = lTimeLeft.ToString("hh") + "H " + lTimeLeft.ToString("mm") + "min " + lTimeLeft.ToString("ss") + "sec";
            }


        }

    



        public IEnumerator OpenPurpleBox()
        {
            //DefaultUIAnimation.Instance.FadeInCanvasGroup(Home.Instance.Loader_Grp, 0.3f);

            DisableRaycastAllLootbox();

            yield return new WaitForSeconds(0.1f); // wait button animation
            MetaData.Instance.LootMain.AskManagerLoot("medium");

        }

        public IEnumerator OpenBlueBox()
        {
            //DefaultUIAnimation.Instance.FadeInCanvasGroup(Home.Instance.Loader_Grp, 0.3f);

            DisableRaycastAllLootbox();

            yield return new WaitForSeconds(0.1f); // wait button animation
            MetaData.Instance.LootMain.AskManagerLoot("small");
        }


       

        public void DisableRaycastAllLootbox()
        {
            LootboxSmall_Gob.GetComponent<Image>().raycastTarget = false;
            LootboxMedium_Gob.GetComponent<Image>().raycastTarget = false;
        }

        public void EnableRaycastAllLootbox()
        {
            //UpdateFragment();
            LootboxMedium_Gob.GetComponent<Image>().raycastTarget = true;
            LootboxSmall_Gob.GetComponent<Image>().raycastTarget = true;
        }
     
        //public IEnumerator GetRewardsArenaCup()
        //{
        //    DefaultUIAnimation.Instance.FadeInCanvasGroup(Home.Instance.Loader_Grp, 0.3f);

        //    UserData.Instance.Profile.NeedToOpenReward = false;
        //    UserData.Instance.PersistUserData();

        //    Debug.Log(" ADLM > GetRewardsArenaCup Previous Number = " + Data.Instance.LeaderboardMain.PreviousNumberArenaCup);
        //    Data.Instance.LootMain.AskManagerLoot("arenacup", Data.Instance.LeaderboardMain.PreviousNumberArenaCup);

        //    yield return new WaitForSeconds(0.1f); // wait button animation

        //    StartCoroutine(ClosePopinArenaCup());
      
        //}

        public void UpdateNotif()
        {
            Sequence lSequence = DOTween.Sequence();
      
            //if (true == UserData.Instance.Profile.NotifArenaCup)
            //{
            //    NotifArenaCup_Gob.SetActive(true);
            //    lSequence.Insert(1, NotifArenaCup_Gob.transform.DOScale(1.1f, 0.5f));
            //    lSequence.Insert(1.5f, NotifArenaCup_Gob.transform.DOScale(1f, 0.5f));
            //    lSequence.SetLoops(-1);
            //}
            //else
            //{
            //    NotifArenaCup_Gob.SetActive(false);
            //}

            if (true == UserData.Instance.Profile.NotifMinionsPlayers)
            {
                NotifMinionsPlayers_Gob.SetActive(true);
                lSequence.Insert(1, NotifMinionsPlayers_Gob.transform.DOScale(1.1f, 0.5f));
                lSequence.Insert(1.5f, NotifMinionsPlayers_Gob.transform.DOScale(1f, 0.5f));
                lSequence.SetLoops(-1);
            }
            else
            {
                NotifMinionsPlayers_Gob.SetActive(false);
            }

            if (true == UserData.Instance.Profile.NotifShop)
            {
                NotifShop_Gob.SetActive(true);
                lSequence.Insert(1, NotifShop_Gob.transform.DOScale(1.1f, 0.5f));
                lSequence.Insert(1.5f, NotifShop_Gob.transform.DOScale(1f, 0.5f));
                lSequence.SetLoops(-1);
            }
            else
            {
                NotifShop_Gob.SetActive(false);
            }
        }

        public IEnumerator OpenWarningNotEnoughPlayersPopin()
        {
            yield return new WaitForSeconds(0.1f); // wait button animation

            while (MetaData.Instance.LeaderboardMain.LeaderboardPreviousArenaCupData == null)
            {
                yield return new WaitForSeconds(0.3f); // wait button animation
            }

            WarningNotEnoughPlayers_Gob.SetActive(true);
            WarningNotEnoughPlayers_Gob.transform.Find("Popin_Grp").transform.localPosition = new Vector3(0, 0, 0);
            WarningNotEnoughPlayers_Gob.transform.Find("Popin_Grp").GetComponent<Animator>().SetTrigger("Open");


        }

        public IEnumerator CloseWarningNotEnoughPlayersPopin()
        {

            WarningNotEnoughPlayers_Gob.SetActive(true);
            WarningNotEnoughPlayers_Gob.transform.Find("Popin_Grp").GetComponent<Animator>().SetTrigger("Close");

            yield return new WaitForSeconds(0.15f); // wait popin close animation

            WarningNotEnoughPlayers_Gob.SetActive(false);
        }


        public IEnumerator OpenNeedToUpdateAppPopin()
        {
            yield return new WaitForSeconds(0.1f);

            NeedToUpdateAppPopin_Gob.SetActive(true);
            NeedToUpdateAppPopin_Gob.transform.Find("Popin_Grp").transform.localPosition = new Vector3(0, 0, 0);
            NeedToUpdateAppPopin_Gob.transform.Find("Popin_Grp").GetComponent<Animator>().SetTrigger("Open");
            //DefaultUIAnimation.Instance.FadeOutCanvasGroup(Loader_Grp, 0.3f);
        
          
        }

        public IEnumerator CloseNeedToUpdateAppPopin()
        {
            NeedToUpdateAppPopin_Gob.SetActive(true);
            NeedToUpdateAppPopin_Gob.transform.Find("Popin_Grp").GetComponent<Animator>().SetTrigger("Close");

            yield return new WaitForSeconds(0.15f); // wait popin close animation

            NeedToUpdateAppPopin_Gob.SetActive(false);
        }

        public IEnumerator OpenAppUpdatedPopin()
        {
            yield return new WaitForSeconds(0.1f);

            AppUpdatedPopin_Gob.SetActive(true);
            AppUpdatedPopin_Gob.transform.Find("Popin_Grp").transform.localPosition = new Vector3(0, 0, 0);
            AppUpdatedPopin_Gob.transform.Find("Popin_Grp").GetComponent<Animator>().SetTrigger("Open");
            //DefaultUIAnimation.Instance.FadeOutCanvasGroup(Loader_Grp, 0.3f);


        }

        public IEnumerator CloseAppUpdatedPopin()
        {
            AppUpdatedPopin_Gob.SetActive(true);
            AppUpdatedPopin_Gob.transform.Find("Popin_Grp").GetComponent<Animator>().SetTrigger("Close");

            MetaData.Instance.LootMain.AskManagerLoot("app_update");
            //DefaultUIAnimation.Instance.FadeInCanvasGroup(Loader_Grp, 0.3f);

            yield return new WaitForSeconds(0.15f); // wait popin close animation

            AppUpdatedPopin_Gob.SetActive(false);

        }


        #endregion

        #region Private Methods

      

        //public IEnumerator OpenPopinArenaCup()
        //{
        //    DefaultUIAnimation.Instance.FadeOutCanvasGroup(Loader_Grp, 0.3f);
        //    yield return new WaitForSeconds(0.1f); // wait button animation

        //    while (Data.Instance.LeaderboardMain.LeaderboardPreviousArenaCupData == null)
        //    {
        //        yield return new WaitForSeconds(0.3f); // wait button animation
        //    }

        //    //Debug.Log(Data.Instance.LeaderboardMain.LeaderboardPreviousArenaCupData.ManagerRank);
        //    if (Data.Instance.LeaderboardMain.LeaderboardPreviousArenaCupData.ManagerRank != -1)
        //    {
        //        ArenaCupPopin_Gob.SetActive(true);
        //        ArenaCupPopin_Gob.transform.Find("Popin_Grp/Reward_Grp").DOScale(1.2f, 0.5f);
        //        ArenaCupPopin_Gob.transform.Find("Popin_Grp").transform.localPosition = new Vector3(0, 0, 0);
        //        ArenaCupPopin_Gob.transform.Find("Popin_Grp").GetComponent<Animator>().SetTrigger("Open");
        //    }
        //}

        //private IEnumerator ClosePopinArenaCup()
        //{

        //    ArenaCupPopin_Gob.SetActive(true);
        //    ArenaCupPopin_Gob.transform.Find("Popin_Grp").GetComponent<Animator>().SetTrigger("Close");

        //    yield return new WaitForSeconds(0.15f); // wait popin close animation

        //    ArenaCupPopin_Gob.SetActive(false);
        //}

      
        void Awake()
        {
            Instance = this;
         
            UpdateHard();

            UpdateFragment();

            this.transform.Find("LootBoxPurple_Grp").gameObject.SetActive(false);

        }

        private void Start()
        {
            MetaData.Instance.OnGetLeaderboardTrophy_GE();

            transform.Find("PlayerStats_Grp/Name_Grp/Username_Txt").GetComponent<TMPro.TMP_Text>().text = UserData.Instance.Profile.Pseudo;

            transform.Find("PlayerStats_Grp/Trophy_Grp/Trophy_Txt").GetComponent<TMPro.TMP_Text>().text = UserData.Instance.Profile.Score.ToString();

            LocalTimersManager.Instance.LoadLocalTimers();

            UpdateNotif();

            if (true == UserData.Instance.Profile.NeedToUpdateVersion)
            {
                StartCoroutine(OpenNeedToUpdateAppPopin());
            }
            else if (true == UserData.Instance.Profile.VersionUpdated)
            {
                StartCoroutine(OpenAppUpdatedPopin());
            }
        }

        private void UpdateFragment(int pFragment = 0)
        {
            int lFragment = UserData.Instance.Profile.Fragment + pFragment;
            int modulo = lFragment % 100;
            int number = (lFragment - modulo) / 100;

            this.transform.Find("LootBoxBlue_Grp/Fragments_Grp/Count_Txt").GetComponent<TMPro.TMP_Text>().text = modulo + "/100";
            this.transform.Find("LootBoxBlue_Grp/Number_Img/Count_Txt").GetComponent<TMPro.TMP_Text>().text = number.ToString();

            if (number == 0)
            {
                this.transform.Find("LootBoxBlue_Grp").GetComponent<Image>().raycastTarget = false;
                this.transform.Find("LootBoxBlue_Grp/Number_Img").gameObject.SetActive(false);
            }
            else
            {
                this.transform.Find("LootBoxBlue_Grp").GetComponent<Image>().raycastTarget = true;
                this.transform.Find("LootBoxBlue_Grp/Number_Img").gameObject.SetActive(true);

            }

        }

        private void UpdateHard(int pHard = 0)
        {
            int lHard = UserData.Instance.Profile.Hard + pHard;

            transform.Find("Hard_Currency_Grp/Value_Grp/Value_Txt").GetComponent<TMPro.TMP_Text>().text = lHard.ToString();

        }


        #endregion





    }
}
﻿
using System;
using System.Collections;
using DG.Tweening;
using Sportfaction.CasualFantasy.Events;
using Sportfaction.CasualFantasy.Loot;
using Sportfaction.CasualFantasy.Players;
using UnityEngine;
using UnityEngine.UI;

namespace Sportfaction.CasualFantasy.Meta
{
    /// <summary>
    /// Description: LootBox Animation
    /// Author: Antoine de Lachèze-Murel
    /// </summary>
    public class LootBox : MonoBehaviour
    {
        public static LootBox Instance;

        #region Public Methods

        public GameObject Previous_Gob;
        public GameObject Box_Gob;
        public GameObject G_Gob;
        public GameObject D_Gob;
        public GameObject M_Gob;
        public GameObject A_Gob;
        public GameObject Hard_Gob;
        public GameObject Fireworks;

        public GameObject Player_Prefab;

        public Color _whiteColor;
        public Color _blueColor;
        public Color _purpleColor;
        public Color _yellowColor;

        [SerializeField]
        private GameEvent _lootBoxIsClosed;

        #endregion

        private LootData _lootData = null;

        private bool _closeLootBox;

        public bool ActiveTransition;

        public bool NoBox;

        public void Init(LootData pData, bool pCloseLootBox = true)
        {
            NoBox = false;
            _lootData = pData;
            _closeLootBox = pCloseLootBox;
            ActiveTransition = false;

            Box_Gob.SetActive(true);
            Box_Gob.GetComponent<CanvasGroup>().alpha = 1;

            G_Gob.SetActive(false);
            G_Gob.GetComponent<CanvasGroup>().alpha = 0;

            D_Gob.SetActive(false);
            D_Gob.GetComponent<CanvasGroup>().alpha = 0;

            M_Gob.SetActive(false);
            M_Gob.GetComponent<CanvasGroup>().alpha = 0;

            A_Gob.SetActive(false);
            A_Gob.GetComponent<CanvasGroup>().alpha = 0;

            Hard_Gob.SetActive(false);
            Hard_Gob.GetComponent<CanvasGroup>().alpha = 0;

            Sprite[] lSpritesMenu = Resources.LoadAll<Sprite>("Sprites/Meta/Lootbox");
            switch (pData.Type)
            {
                case "starter":
                    Box_Gob.transform.Find("LootBox_Bottom").GetComponent<Image>().sprite = UtilityMethods.GetSprite(lSpritesMenu, "LootBox_Starter_Bottom");
                    Box_Gob.transform.Find("LootBox_Ballray").GetComponent<Image>().color = Color.white;
                    Box_Gob.transform.Find("LootBox_Elipse").GetComponent<Image>().color = Color.white;
                    Box_Gob.transform.Find("LootBox_Ray").GetComponent<Image>().color = Color.white;
                    Box_Gob.transform.Find("LootboxFX_Grp").GetComponent<Image>().color = Color.white;
                    Box_Gob.transform.Find("LootboxFX_Grp/FXStar_Img").GetComponent<Image>().color = Color.white;
                    Box_Gob.transform.Find("LootboxFX_Grp/FXStar_2_Img").GetComponent<Image>().color = Color.white;
                
                        break;
                case "small":
                    Box_Gob.transform.Find("LootBox_Bottom").GetComponent<Image>().sprite = UtilityMethods.GetSprite(lSpritesMenu, "LootBox_Regular_Bottom");
                    Box_Gob.transform.Find("LootBox_Ballray").GetComponent<Image>().color = _blueColor;
                    Box_Gob.transform.Find("LootBox_Elipse").GetComponent<Image>().color = _blueColor;
                    Box_Gob.transform.Find("LootBox_Ray").GetComponent<Image>().color = _blueColor;
                    Box_Gob.transform.Find("LootboxFX_Grp").GetComponent<Image>().color = _blueColor;
                    Box_Gob.transform.Find("LootboxFX_Grp/FXStar_Img").GetComponent<Image>().color = _blueColor;
                    Box_Gob.transform.Find("LootboxFX_Grp/FXStar_2_Img").GetComponent<Image>().color = _blueColor;
                    break;
                case "medium":
                    Box_Gob.transform.Find("LootBox_Bottom").GetComponent<Image>().sprite = UtilityMethods.GetSprite(lSpritesMenu, "LootBox_Super_Bottom");
                    Box_Gob.transform.Find("LootBox_Ballray").GetComponent<Image>().color = _purpleColor;
                    Box_Gob.transform.Find("LootBox_Elipse").GetComponent<Image>().color = _purpleColor;
                    Box_Gob.transform.Find("LootBox_Ray").GetComponent<Image>().color = _purpleColor;
                    Box_Gob.transform.Find("LootboxFX_Grp").GetComponent<Image>().color = _purpleColor;
                    Box_Gob.transform.Find("LootboxFX_Grp/FXStar_Img").GetComponent<Image>().color = _purpleColor;
                    Box_Gob.transform.Find("LootboxFX_Grp/FXStar_2_Img").GetComponent<Image>().color = _purpleColor;
                    break;
                case "big":
                    Box_Gob.transform.Find("LootBox_Bottom").GetComponent<Image>().sprite = UtilityMethods.GetSprite(lSpritesMenu, "LootBox_Ultra_Bottom");
                    Box_Gob.transform.Find("LootBox_Ballray").GetComponent<Image>().color = _yellowColor;
                    Box_Gob.transform.Find("LootBox_Elipse").GetComponent<Image>().color = _yellowColor;
                    Box_Gob.transform.Find("LootBox_Ray").GetComponent<Image>().color = _yellowColor;
                    Box_Gob.transform.Find("LootboxFX_Grp").GetComponent<Image>().color = _yellowColor;
                    Box_Gob.transform.Find("LootboxFX_Grp/FXStar_Img").GetComponent<Image>().color = _yellowColor;
                    Box_Gob.transform.Find("LootboxFX_Grp/FXStar_2_Img").GetComponent<Image>().color = _yellowColor;
                    break;
            }

            if (null != pData.Player)
            {
                switch (pData.Player.Profile.GlobalPosition.ToString())
                {
                    case "G":
                        _lootData.GoalKeepers.Add(pData.Player);
                        break;
                    case "D":
                        _lootData.FullBacks.Add(pData.Player);
                        break;
                    case "M":
                        _lootData.MidFields.Add(pData.Player);
                        break;
                    case "A":
                        _lootData.Forwards.Add(pData.Player);
                        break;
                }

                NoBox = true;
            }

            DefaultUIAnimation.Instance.DeleteAllChilds(G_Gob.transform.Find("List_Grp").gameObject);
            foreach (PlayersItem player in pData.GoalKeepers)
            {
                InitPlayer(G_Gob.transform.Find("List_Grp").gameObject, player);
            }

            DefaultUIAnimation.Instance.DeleteAllChilds(D_Gob.transform.Find("List_Grp").gameObject);
            foreach (PlayersItem player in pData.FullBacks)
            {
                InitPlayer(D_Gob.transform.Find("List_Grp").gameObject, player);
            }

            
            DefaultUIAnimation.Instance.DeleteAllChilds(M_Gob.transform.Find("List_Grp").gameObject);
            foreach (PlayersItem player in pData.MidFields)
            {
                InitPlayer(M_Gob.transform.Find("List_Grp").gameObject, player);
            }

         
            DefaultUIAnimation.Instance.DeleteAllChilds(A_Gob.transform.Find("List_Grp").gameObject);
            foreach (PlayersItem player in pData.Forwards)
            {
                InitPlayer(A_Gob.transform.Find("List_Grp").gameObject, player);
            }

          

            this.transform.Find("Hard_LootBox_Grp/Hard_Txt").GetComponent<TMPro.TMP_Text>().text = "x" + pData.Hard;

            //Debug.Log("G (" + pData.GoalKeepers.Count + ") / " + "D (" + pData.GoalKeepers.Count + ") / " + "M (" + pData.GoalKeepers.Count + ") / " + "A (" + pData.GoalKeepers.Count + ") / " + pData.Hard);

          
        }

        public IEnumerator OpenLootBox()
        {

            yield return new WaitForSeconds(0.1f);

            Sequence lSequence = DOTween.Sequence();

            lSequence.Insert(0, Box_Gob.GetComponent<RectTransform>().DOShakeAnchorPos(0.5f, 75.0f, 10, 180, true, true));
            Box_Gob.GetComponent<Animation>().Play("LootBox_Opening");
            DefaultUIAnimation.Instance.FadeOutCanvasGroup(LootBoxAnimation.Instance.MessageOpenLoot);

            LootBoxCard[] lPlayers = G_Gob.GetComponentsInChildren<LootBoxCard>();
            foreach (LootBoxCard item in lPlayers)
            {
                item.Init();
            }

            lPlayers = D_Gob.GetComponentsInChildren<LootBoxCard>();
            foreach (LootBoxCard item in lPlayers)
            {
                item.Init();
            }

            lPlayers = M_Gob.GetComponentsInChildren<LootBoxCard>();
            foreach (LootBoxCard item in lPlayers)
            {
                item.Init();
            }

            lPlayers = A_Gob.GetComponentsInChildren<LootBoxCard>();
            foreach (LootBoxCard item in lPlayers)
            {
                item.Init();
            }

        }

        public void ShowGoalKeeperLootBox()
        {
            if (_lootData.GoalKeepers.Count == 0)
            {
                ShowFullBackLootBox();
            }
            else
            {
                if (_lootData.GoalKeepers.Count == 1)
                {
                    G_Gob.transform.Find("Header_Grp/Title_Txt").GetComponent<TMPro.TMP_Text>().text = I2.Loc.LocalizationManager.GetTranslation("Glong");
                }
                else
                {
                    G_Gob.transform.Find("Header_Grp/Title_Txt").GetComponent<TMPro.TMP_Text>().text = I2.Loc.LocalizationManager.GetTranslation("GlongPlural");
                }

                StartCoroutine(ShowCards(G_Gob));
            }

        }

        public void ShowFullBackLootBox()
        {
            if (_lootData.FullBacks.Count == 0)
            {
                ShowMidfieldLootBox();
            }
            else
            {
                if (_lootData.FullBacks.Count == 1)
                {
                    D_Gob.transform.Find("Header_Grp/Title_Txt").GetComponent<TMPro.TMP_Text>().text = I2.Loc.LocalizationManager.GetTranslation("Dlong");
                }
                else
                {
                    D_Gob.transform.Find("Header_Grp/Title_Txt").GetComponent<TMPro.TMP_Text>().text = I2.Loc.LocalizationManager.GetTranslation("DlongPlural");
                }

                StartCoroutine(ShowCards(D_Gob));
            }

        }

        public void ShowMidfieldLootBox()
        {
            if (_lootData.MidFields.Count == 0)
            {
                ShowForwardLootBox();
            }
            else
            {
                if (_lootData.MidFields.Count == 1)
                {
                    M_Gob.transform.Find("Header_Grp/Title_Txt").GetComponent<TMPro.TMP_Text>().text = I2.Loc.LocalizationManager.GetTranslation("Mlong");
                }
                else
                {
                    M_Gob.transform.Find("Header_Grp/Title_Txt").GetComponent<TMPro.TMP_Text>().text = I2.Loc.LocalizationManager.GetTranslation("MlongPlural");
                }

                StartCoroutine(ShowCards(M_Gob));
            }
        }

        public void ShowForwardLootBox()
        {
            if (_lootData.Forwards.Count == 0)
            {
                ShowHardLootBox();
            }
            else
            {
                if (_lootData.Forwards.Count == 1)
                {
                    A_Gob.transform.Find("Header_Grp/Title_Txt").GetComponent<TMPro.TMP_Text>().text = I2.Loc.LocalizationManager.GetTranslation("Along");
                }
                else
                {
                    A_Gob.transform.Find("Header_Grp/Title_Txt").GetComponent<TMPro.TMP_Text>().text = I2.Loc.LocalizationManager.GetTranslation("AlongPlural");
                }
                StartCoroutine(ShowCards(A_Gob));
            }
         
        } 

        public void ShowHardLootBox()
        {
            if (_lootData.Hard == 0)
            {
                if (false == ActiveTransition)
                {
                    ActiveTransition = true;
                    StartCoroutine(CloseLootBox());
                }
            
            }
            else
            {
                Hard_Gob.SetActive(true);
                Sequence lSequence = DOTween.Sequence();
                lSequence.Insert(0, Previous_Gob.GetComponent<CanvasGroup>().DOFade(0, 0.1f));
                lSequence.Insert(0.1f, Hard_Gob.GetComponent<CanvasGroup>().DOFade(1f, 0.1f));
                Previous_Gob = Hard_Gob;
               
            }
          
        }

        public IEnumerator CloseLootBox()
        {
           
            yield return new WaitForSeconds(0.1f);

            _lootBoxIsClosed.Invoke();

            if (true == _closeLootBox)
            {
                UI.Instance.GoToPreviousWindow();
            }
            else
            {
                Hard_Gob.SetActive(true);
                Sequence lSequence = DOTween.Sequence();

                if(Previous_Gob != null)
                {
        
                    lSequence.Insert(0, Previous_Gob.GetComponent<CanvasGroup>().DOFade(0, 0.1f));
                    lSequence.Insert(0.3f, Box_Gob.GetComponent<CanvasGroup>().DOFade(1f, 0.1f));
         
               
                    yield return new WaitForSeconds(0.2f);


                    Previous_Gob = null;

                }
                else
                {
                    lSequence.Insert(0, Box_Gob.GetComponent<CanvasGroup>().DOFade(1f, 0.3f));
                    yield return new WaitForSeconds(0.3f);
                }
                ActiveTransition = false;
            }
        }


        private IEnumerator ShowCards(GameObject pCurrent)
        {
            pCurrent.SetActive(true);
            Sequence lSequence = DOTween.Sequence();

            if(Previous_Gob != null)
            {
                lSequence.Insert(0, Previous_Gob.GetComponent<CanvasGroup>().DOFade(0, 0.1f));
            }
        
            lSequence.Insert(0.1f, pCurrent.GetComponent<CanvasGroup>().DOFade(1f, 0.1f));
            Previous_Gob = pCurrent;

            yield return new WaitForSeconds(0.2f);

            LootBoxCard[] lPlayers = pCurrent.GetComponentsInChildren<LootBoxCard>();
            foreach (LootBoxCard item in lPlayers)
            {
                if (item == null)
                    break;

                lSequence = DOTween.Sequence();
                lSequence.Insert(0, item.GetComponent<CanvasGroup>().DOFade(1f, 0.3f));

                yield return new WaitForSeconds(0.1f);
                item.Show();
            }
        }
        private void InitPlayer(GameObject pParent, PlayersItem pPlayer)
        {
            GameObject lPrefab = Instantiate(Player_Prefab);
            lPrefab.name = "#" + pPlayer.Profile.PlayerId;
            lPrefab.GetComponent<MinionsPlayersScrollviewItem>().Init(pPlayer);
            lPrefab.GetComponent<Image>().raycastTarget = false;
            lPrefab.transform.SetParent(pParent.transform, false);
            lPrefab.AddComponent<LootBoxCard>().Init();

        }


        void Awake()
        {
            Instance = this;
            ColorUtility.TryParseHtmlString("#FFFFFF", out _whiteColor);
            _whiteColor.a = 0;
            ColorUtility.TryParseHtmlString("#00F5FF", out _blueColor);
            _blueColor.a = 0;
            ColorUtility.TryParseHtmlString("#FFD300", out _yellowColor);
            _yellowColor.a = 0;
            ColorUtility.TryParseHtmlString("#EC00FF", out _purpleColor);
            _purpleColor.a = 0;
        }


    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using Sportfaction.CasualFantasy.ArenaCup;
using Sportfaction.CasualFantasy.ArenaSeason;
using Sportfaction.CasualFantasy.Leaderboard;
using Sportfaction.CasualFantasy.Manager.Profile;
using Sportfaction.CasualFantasy.Services.Timers;
using UnityEngine;
using UnityEngine.UI;

namespace Sportfaction.CasualFantasy.Meta
{
    /// <summary>
    /// Description: ArenaSeason
    /// Author: Antoine de Lachèze-Murel
    /// </summary>
    public sealed class ArenaSeason : MonoBehaviour
    {
        public static ArenaSeason Instance;

        #region Public Fields

        public GameObject RewardScroll_Grp;
        public GameObject Reward_Prefab;
        public GameObject Reward_Grp;
  
        #endregion

        #region Private Fields
        #endregion


        #region Public Methods



        public void InitArenaSeason()
        {
            foreach (Transform child in RewardScroll_Grp.transform)
            {
                Destroy(child.gameObject);
            }

            List<ArenaSeasonDataReward> lData = MetaData.Instance.ArenaSeasonMain.ArenaSeasonData.Rewards;

            int lBracketMin = 0;
            int lBracketMax = 0;

            bool lNeedToGetReward = true;
            bool lCanGetReward = true;
            for (int i = 0; i < lData.Count; i++)
            {
                if (i == 0)
                {
                    lBracketMin = 0;
                    lBracketMax = lData[i].MaxScore + ((lData[i + 1].MaxScore - lData[i].MaxScore) / 2);
                }
                else if(i < lData.Count - 1)
                {
                    lBracketMin = lBracketMax + 1;
                    lBracketMax = lData[i].MaxScore + ((lData[i + 1].MaxScore - lData[i].MaxScore) / 2);
                }
                else
                {
                    lBracketMin = lBracketMax + 1;
                    lBracketMax = lData[i].MaxScore;
                }

                lNeedToGetReward = InitReward(RewardScroll_Grp, lData[i], lBracketMin, lBracketMax, lData[i].MaxScore, i, lCanGetReward);

                lCanGetReward = (lNeedToGetReward == false);
            }

            float lWidth = Reward_Prefab.transform.GetComponent<RectTransform>().rect.width * MetaData.Instance.ArenaSeasonMain.ArenaSeasonData.Rewards.Count;

            RectTransform lRectReward_Grp = Reward_Grp.transform.GetComponent<RectTransform>();
            lRectReward_Grp.sizeDelta = new Vector2(lWidth, lRectReward_Grp.rect.height);

         
            float lScrollX = -1 * ((UserData.Instance.Profile.LastArenaSeasonReward -1) * Reward_Prefab.transform.GetComponent<RectTransform>().rect.width);
            lRectReward_Grp.anchoredPosition = new Vector2(lScrollX, lRectReward_Grp.anchoredPosition.y);
        }


        #endregion

        #region Private Methods


        private bool InitReward(GameObject pParent, ArenaSeasonDataReward pReward, int pBracketMin, int pBracketMax, int pStep, int pIndex, bool pCanGetReward)
        {
          
            GameObject lPrefab = Instantiate(Reward_Prefab);
            bool lNeedToGetReward = lPrefab.GetComponent<ArenaSeasonRewardItem>().Init(pReward, pBracketMin, pBracketMax, pStep, pIndex, pCanGetReward);
            //lPrefab.GetComponent<Image>().raycastTarget = false;
            lPrefab.transform.name = "#Reward-" + pReward.MaxScore;
            lPrefab.transform.SetParent(pParent.transform, false);

            return lNeedToGetReward;

        }

        private void    Start()
        {
            InitArenaSeason();
        }

        private void    Awake()
        {

            Instance = this;


        }

        #endregion
    }
}
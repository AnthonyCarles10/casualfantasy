﻿
using System.Collections;
using DG.Tweening;
using SportFaction.CasualFootballEngine.PlayerService.Model;
using UnityEngine;
using UnityEngine.UI;

namespace Sportfaction.CasualFantasy.Meta
{
    /// <summary>
    /// Description: Loot Box Card
    /// Author: Antoine de Lachèze-Murel
    /// </summary>
    public class LootBoxCard : MonoBehaviour
    {

        #region Public Methods

      

        /// <summary>
        /// Init
        /// </summary>
        public void Init()
        {

            transform.GetComponent<Image>().enabled = false;
            transform.Find("Position_Txt").gameObject.SetActive(false);
            transform.Find("Name_Txt").gameObject.SetActive(false);
            transform.Find("Form_Grp").gameObject.SetActive(false);
            transform.Find("Score_Txt").gameObject.SetActive(false);
            transform.Find("Score_Att_Txt").gameObject.SetActive(false);
            transform.Find("Score_Def_Txt").gameObject.SetActive(false);
            transform.Find("Championship_Txt").gameObject.SetActive(false);
            transform.Find("Club_Txt").gameObject.SetActive(false);
            transform.Find("Flag_Img").gameObject.SetActive(false);
            transform.Find("Fade_Img").gameObject.SetActive(false);
            transform.Find("Lock_Grp").gameObject.SetActive(false);
            transform.Find("OnField_Grp").gameObject.SetActive(false);

            transform.GetComponent<CanvasGroup>().alpha = 0;
            transform.Find("FileUp").gameObject.SetActive(true);
            transform.Find("Particle System").gameObject.SetActive(false);

        }

        public void Show()
        {
            Sprite[] lSpritesMenu = Resources.LoadAll<Sprite>("Sprites/Meta/Menu_2_SS");

            GetComponent<Image>().enabled = true;
            //GetComponent<Image>().sprite = UtilityMethods.GetSprite(lSpritesMenu, "Card_Rarity_" + (_player.RareLevel + 1) + "_Bck");


            transform.Find("Position_Txt").gameObject.SetActive(true);
            transform.Find("Name_Txt").gameObject.SetActive(true);
            transform.Find("Form_Grp").gameObject.SetActive(true);
            transform.Find("Score_Txt").gameObject.SetActive(true);
            transform.Find("Score_Att_Txt").gameObject.SetActive(true);
            transform.Find("Score_Def_Txt").gameObject.SetActive(true);
            //transform.Find("Championship_Txt").gameObject.SetActive(true);
            transform.Find("Club_Txt").gameObject.SetActive(true);
            transform.Find("Flag_Img").gameObject.SetActive(true);

            Sequence lSequence = DOTween.Sequence();
            lSequence.Insert(0, transform.Find("FileUp").GetComponent<CanvasGroup>().DOFade(0f, 0.1f));

            transform.Find("Particle System").gameObject.SetActive(true);
            transform.Find("Particle System").GetComponent<ParticleSystem>().Play();
        }

      
        #endregion

    }
}

﻿using System;
using System.Collections.Generic;
using Sportfaction.CasualFantasy.Action;
using Sportfaction.CasualFantasy.AppParam;
using Sportfaction.CasualFantasy.ArenaCup;
using Sportfaction.CasualFantasy.ArenaSeason;
using Sportfaction.CasualFantasy.Championship;
using Sportfaction.CasualFantasy.Core;
using Sportfaction.CasualFantasy.Events;
using Sportfaction.CasualFantasy.Leaderboard;
using Sportfaction.CasualFantasy.Loot;
using Sportfaction.CasualFantasy.Manager.Profile;
using Sportfaction.CasualFantasy.Players;
using Sportfaction.CasualFantasy.Positions;
using Sportfaction.CasualFantasy.Services.Billing;
using Sportfaction.CasualFantasy.Shop;
using Sportfaction.CasualFantasy.Squad;
using UnityEngine;


namespace Sportfaction.CasualFantasy.Meta
{
    /// <summary>
    /// Description: Data
    /// Author: Antoine de Lachèze-Murel
    /// </summary>
    public sealed class MetaData : MonoBehaviour
    {
        public static MetaData Instance;

        #region Fields

        [Header("Main")]
        [SerializeField]
        public ActionMain       ActionMain;
        [SerializeField]
        public AppParamMain     AppParamMain;
        [SerializeField]
        public ArenaSeasonMain  ArenaSeasonMain;
        [SerializeField]
        public ChampionshipMain ChampionshipMain;
        [SerializeField]
        public CoreMain         CoreMain;
        [SerializeField]
        public LeaderboardMain  LeaderboardMain;
        [SerializeField]
        public LootMain         LootMain;
        [SerializeField]
        public PlayersMain      PlayersMain;
        [SerializeField]
        public PositionsMain    PositionsMain;
        [SerializeField]    
        public UserMain         UserMain;
        [SerializeField]
        public ShopMain         ShopMain;
        [SerializeField]
        public SquadMain        SquadMain;
        [SerializeField]
        public ArenaCupMain     ArenaCupMain;
    

        [Header("ScriptableObject")]

        [SerializeField]
        public PVPData              PVPData;
        [SerializeField]
        public IAPProductsData      IAPProductsData;


        [Header("Local Data")]
        [SerializeField]
        public List<PlayersItem> PlayersTeamSelected = new List<PlayersItem>();

        [Header("Prefab")]
        [SerializeField]
        private GameObject      _essentials;

        [Header("Game Events")]
        [SerializeField]
        private GameEvent       _onGetProfile;

        [Header("Selected")]
        [SerializeField]
        public PlayersItem          FantasyPlayerScrollviewSelected;
        [SerializeField]
        public PlayersItem          MinionScrollviewSelected;
        [SerializeField]
        public string               MinionScrollviewPlayerIdSelected;
        [SerializeField]
        public PlayersItem          MinionFieldSelected;
        [SerializeField]
        public string               MinionFieldPositionSelected = "G";
        [SerializeField]
        public string               MinionFieldSquadPositionSelected;
        [SerializeField]
        public bool                 Filter; // false
        [SerializeField]
        public bool                 FilterFantasy; // false

        public Dictionary<string, string> QueryParams = new Dictionary<string, string>()
        {
            { "keyword" , "" },
            { "offset"  , "0" },
            { "order_by" , "score" },
            { "order"   , "desc" },
            { "position", "" },
            { "team"    , "" },
            { "is_owned", "1" },
            { "rare_level", "-1" },
            { "limit"   , "50" },
        };

        public Dictionary<string, string> QueryParamsFantasy = new Dictionary<string, string>()
        {
            { "keyword" , "" },
            { "offset"  , "0" },
            { "order_by" , "score" },
            { "order"   , "desc" },
            { "position", "" },
            { "team"    , "" },
            { "is_owned", "1" },
            { "rare_level", "-1" },
            { "limit"   , "50" },
        };

        public Dictionary<int, string> FormLevelLetter = new Dictionary<int, string>()
        {
            { 0, "D" },
            { 1, "C" },
            { 2, "B" },
            { 3, "A" },
        };

        public Dictionary<int, string> FormLevelPrecisionPoints = new Dictionary<int, string>()
        {
            { 0, "0" },
            { 1, "5" },
            { 2, "10" },
            { 3, "20" },
            { 4, "30" },
        };

        public Dictionary<string, string> MissFlag = new Dictionary<string, string>()
        {
            { "usa", "usa" },
            { "republic-of-ireland", "ireland" },
            { "côte-d'ivoire", "ivory-coast" },
            { "congo-dr", "democratic-republic-of-congo" },
            { "french-guiana", "france" },
            { "northen-ireland", "united-kingdom" },
            { "korea-republic", "south-korea" },
            { "china-pr", "china" },
            { "guadeloupe", "france" },
            { "north-macedonia", "republic-of-macedonia" },
            { "cape-verde-islands", "cape-verde" },
        };

        #endregion

        #region Public Methods Game Event

        public void OnConnectEvent_GE()
        {
           UserMain.GetProfile();
        }


        //public void OnEndArenaCupTimer_GE()
        //{
        //    Debug.Log("OnEndArenaCupTimer_GE");
        //    // if(Data.Instance.LeaderboardMain.LeaderboardPreviousArenaCupData.ManagerRank)
        //    UserData.Instance.Profile.NeedToOpenReward = true;
        //    UserData.Instance.PersistUserData();

        //    ArenaCupMain.GetCurrentEndTimestampArenaCup();
        //    UserData.Instance.Profile.CurrentArenaCup = -1;
        //    LeaderboardMain.LeaderboardArenaCupData = null;
        //    LeaderboardMain.LeaderboardPreviousArenaCupData = null;
        //    LeaderboardMain.GetCurrentNumberArenaCup();

        //    GameObject Home_Grp = UI.Instance.PrefabsIdxArray_Gob[Array.IndexOf(UI.Instance.PrefabsIdxArray_Str, "Home_Grp")];

        //    if (false == Home_Grp.activeSelf)
        //    {
        //        Home_Grp.SetActive(true);
        //        DefaultUIAnimation.Instance.FadeInCanvasGroup(Home.Instance.Loader_Grp, 0.3f);
        //        StartCoroutine(UI.Instance.GoToHome());
        //        StartCoroutine(Home.Instance.OpenPopinArenaCup());
        //    }
        //    else
        //    {
        //        DefaultUIAnimation.Instance.FadeInCanvasGroup(Home.Instance.Loader_Grp, 0.3f);
        //        StartCoroutine(Home.Instance.OpenPopinArenaCup());
        //    }

        //}
        public void OnGetProfile_GE()
        {
            if (null != UI.Instance)
            {
                GameObject Shop_Grp = UI.Instance.PrefabsIdxArray_Gob[Array.IndexOf(UI.Instance.PrefabsIdxArray_Str, "Shop_Grp")];

                if (false == Shop_Grp.activeSelf)
                {
                    Shop_Grp.SetActive(true);
                    Shop_Grp.GetComponent<Shop>().Hard_Gob.transform.Find("Value_Grp/Value_Txt").GetComponent<TMPro.TMP_Text>().text = UserData.Instance.Profile.Hard.ToString();
                    Shop_Grp.SetActive(false);
                }
                else
                {
                    Shop_Grp.GetComponent<Shop>().Hard_Gob.transform.Find("Value_Grp/Value_Txt").GetComponent<TMPro.TMP_Text>().text = UserData.Instance.Profile.Hard.ToString();
                }

                GameObject Home_Grp = UI.Instance.PrefabsIdxArray_Gob[Array.IndexOf(UI.Instance.PrefabsIdxArray_Str, "Home_Grp")];

                if (false == Home_Grp.activeSelf)
                {
                    Home_Grp.SetActive(true);
                    Home_Grp.GetComponent<Home>().transform.Find("Hard_Currency_Grp/Value_Grp/Value_Txt").GetComponent<TMPro.TMP_Text>().text = UserData.Instance.Profile.Hard.ToString();
                    Home_Grp.SetActive(false);
                }
                else
                {
                    Home_Grp.GetComponent<Home>().transform.Find("Hard_Currency_Grp/Value_Grp/Value_Txt").GetComponent<TMPro.TMP_Text>().text = UserData.Instance.Profile.Hard.ToString();
                }
            }
        }

        //public void OnGetRewardsArenaCup_GE()
        //{
        //    GetLeaderboardByArenaCup();
        //}

        public void OnGetRewardsArenaSeason_GE()
        {
            //GetLeaderboardByArenaCup();
        }

        public void OnGetLeaderboardTrophy_GE()
        {
            if (null != UI.Instance)
            {
                GameObject Ranking_Grp = UI.Instance.PrefabsIdxArray_Gob[Array.IndexOf(UI.Instance.PrefabsIdxArray_Str, "Ranking_Grp")];
                if (false == Ranking_Grp.gameObject.activeSelf)
                {
                    Ranking_Grp.gameObject.SetActive(true);
                    Ranking.Instance.InitTrophyRanking();
                    Ranking_Grp.gameObject.SetActive(false);
                }
                else
                {
                    Ranking.Instance.InitTrophyRanking();
                }

                GameObject Home_Grp = UI.Instance.PrefabsIdxArray_Gob[Array.IndexOf(UI.Instance.PrefabsIdxArray_Str, "Home_Grp")];

                if (false == Home_Grp.activeSelf)
                {
                    Home_Grp.SetActive(true);
                    Home_Grp.GetComponent<Home>().transform.Find("PlayerStats_Grp/Rank_Grp/Rank_Txt").GetComponent<TMPro.TMP_Text>().text = "#" + LeaderboardMain.LeaderboardData.ManagerRank.ToString();

                    Home_Grp.SetActive(false);
                }
                else
                {
                    Home_Grp.GetComponent<Home>().transform.Find("PlayerStats_Grp/Rank_Grp/Rank_Txt").GetComponent<TMPro.TMP_Text>().text = "#" + LeaderboardMain.LeaderboardData.ManagerRank.ToString();
                }
            }
        }

        //public void OnGetLeaderboardArenaCup_GE()
        //{
        //    GameObject Ranking_Grp = UI.Instance.PrefabsIdxArray_Gob[Array.IndexOf(UI.Instance.PrefabsIdxArray_Str, "Ranking_Grp")];
        //    if (false == Ranking_Grp.gameObject.activeSelf)
        //    {
        //        Ranking_Grp.gameObject.SetActive(true);
        //        Ranking.Instance.InitArenaCupRanking();
        //        Ranking_Grp.gameObject.SetActive(false);
        //    }
        //    else
        //    {
        //        Ranking.Instance.InitArenaCupRanking();
        //    }
        //}

        //public void OnGetPreviousLeaderboardArenaCup_GE()
        //{
        //    GameObject Ranking_Grp = UI.Instance.PrefabsIdxArray_Gob[Array.IndexOf(UI.Instance.PrefabsIdxArray_Str, "Ranking_Grp")];
        //    if (false == Ranking_Grp.gameObject.activeSelf)
        //    {
        //        Ranking_Grp.gameObject.SetActive(true);
        //        Ranking.Instance.PreviousArenaCup();
        //        Ranking_Grp.gameObject.SetActive(false);
        //    }
        //    else
        //    {
        //        Ranking.Instance.PreviousArenaCup();
        //    }
        //}

        public void OnGetPlayersList_GE()
        {
            if (null != UI.Instance)
            {
                GameObject MinionsPlayers_Grp = UI.Instance.PrefabsIdxArray_Gob[Array.IndexOf(UI.Instance.PrefabsIdxArray_Str, "MinionsPlayers_Grp")];

                if (false == MinionsPlayers_Grp.activeSelf)
                {
                    MinionsPlayers_Grp.SetActive(true);
                    MinionsPlayers_Grp.GetComponent<MinionsPlayersScrollview>().Init();
                    DefaultUIAnimation.Instance.FadeOutCanvasGroup(MinionsPlayers.Instance.LoaderGrp_Gob);
                    MinionsPlayers_Grp.SetActive(false);
                }
                else
                {
                    MinionsPlayers_Grp.GetComponent<MinionsPlayersScrollview>().Init();
                    DefaultUIAnimation.Instance.FadeOutCanvasGroup(MinionsPlayers.Instance.LoaderGrp_Gob);
                }
            }
        }

        public void OnGetPlayersFantasyList_GE()
        {
            if (null != UI.Instance)
            {
                GameObject Fantasy_Grp = UI.Instance.PrefabsIdxArray_Gob[Array.IndexOf(UI.Instance.PrefabsIdxArray_Str, "Fantasy_Grp")];

                if (false == Fantasy_Grp.activeSelf)
                {
                    Fantasy_Grp.SetActive(true);
                    Fantasy_Grp.GetComponent<FantasyPlayersScrollview>().Init();
                    DefaultUIAnimation.Instance.FadeOutCanvasGroup(FantasyPlayers.Instance.LoaderGrp_Gob);
                    Fantasy_Grp.SetActive(false);
                }
                else
                {
                    Fantasy_Grp.GetComponent<FantasyPlayersScrollview>().Init();
                    DefaultUIAnimation.Instance.FadeOutCanvasGroup(FantasyPlayers.Instance.LoaderGrp_Gob);
                }
            }
        }

        public void OnGetPlayerLastGame_GE()
        {

            if (null != UI.Instance)
            {
                GameObject Fantasy_Grp = UI.Instance.PrefabsIdxArray_Gob[Array.IndexOf(UI.Instance.PrefabsIdxArray_Str, "Fantasy_Grp")];

                if (false == Fantasy_Grp.activeSelf)
                {
                    Fantasy_Grp.SetActive(true);
                    FantasyPlayers.Instance.GetPlayerLastGame();
                    Fantasy_Grp.SetActive(false);
                }
                else
                {
                    FantasyPlayers.Instance.GetPlayerLastGame();
                }
            }

           
        }

        public void OnGetProductsList_GE()
        {
            if (null != UI.Instance)
            {
                GameObject Shop_Grp = UI.Instance.PrefabsIdxArray_Gob[Array.IndexOf(UI.Instance.PrefabsIdxArray_Str, "Shop_Grp")];

                if (false == Shop_Grp.activeSelf)
                {
                    Shop_Grp.SetActive(true);
                    Shop.Instance.Init();
                    Shop_Grp.SetActive(false);
                }
                else
                {
                    Shop.Instance.Init();
                }
            }
        }


        public void OnGetTeamId_GE()
        {
            if(PVPData.TeamId != -1)
            {
                SquadMain.GetSquadPlayers(PVPData.TeamId);
            }
        }

        public void OnGetSquadPlayers_GE()
        {
            if (null != UI.Instance)
            {
                PlayersTeamSelected = SquadMain.SquadPlayersData.SquadPlayers;

                GameObject MinionsPlayers_Grp = UI.Instance.PrefabsIdxArray_Gob[Array.IndexOf(UI.Instance.PrefabsIdxArray_Str, "MinionsPlayers_Grp")];

                if (false == MinionsPlayers_Grp.activeSelf)
                {
                    MinionsPlayers_Grp.SetActive(true);
                    MinionsPlayers_Grp.GetComponent<MinionsPlayers>().Init();
                    MinionsPlayers_Grp.SetActive(false);
                }
                else
                {
                    MinionsPlayers_Grp.GetComponent<MinionsPlayers>().Init();
                }
            }

        }

        #endregion


        #region Public Methods

        public void GetPlayersList()
        {
            if (null != UI.Instance)
            {
                GameObject MinionsPlayers_Grp = UI.Instance.PrefabsIdxArray_Gob[Array.IndexOf(UI.Instance.PrefabsIdxArray_Str, "MinionsPlayers_Grp")];

                if (false == MinionsPlayers_Grp.activeSelf)
                {
                    MinionsPlayers_Grp.SetActive(true);
                    DefaultUIAnimation.Instance.FadeInCanvasGroup(MinionsPlayers.Instance.LoaderGrp_Gob);
                    MinionsPlayers_Grp.SetActive(false);
                }
                else
                {
                    DefaultUIAnimation.Instance.FadeInCanvasGroup(MinionsPlayers.Instance.LoaderGrp_Gob);
                }

                MetaData.Instance.MinionScrollviewPlayerIdSelected = "";
                PlayersMain.GetPlayersList(QueryParams);
            }
        }

        public void GetPlayersFantasyList()
        {
            if (null != UI.Instance)
            {
                GameObject Fantasy_Grp = UI.Instance.PrefabsIdxArray_Gob[Array.IndexOf(UI.Instance.PrefabsIdxArray_Str, "Fantasy_Grp")];

                if (false == Fantasy_Grp.activeSelf)
                {
                    Fantasy_Grp.SetActive(true);
                    DefaultUIAnimation.Instance.FadeInCanvasGroup(FantasyPlayers.Instance.LoaderGrp_Gob);
                    Fantasy_Grp.SetActive(false);
                }
                else
                {
                    DefaultUIAnimation.Instance.FadeInCanvasGroup(FantasyPlayers.Instance.LoaderGrp_Gob);
                }

                PlayersMain.GetPlayersFantasyList(QueryParamsFantasy);
            }
        }

        public void GetLeaderboardByRP()
        {
            LeaderboardMain.GetOverallRankingByRP("13", "0");
        }

        public void GetLeaderboardByTrophy()
        {
            LeaderboardMain.GetOverallRankingByTrophy("13", "0");
        }

        public void GetLeaderboardByArenaCup()
        {
            LeaderboardMain.GetCurrentNumberArenaCup();
        }

        public void GetPlayerLastGame(string pPlayerId)
        {

           PlayersMain.GetPlayerLastGame(pPlayerId);
            
        }

        public void SaveMinionsPlayers()
        {
            List<string> positions = new List<string>();

            LineUp lineUp = new LineUp
            {
                TeamName = "default",
                Slot = 0
            };

            for (int i = 0; i < PlayersTeamSelected.Count; i++)
            {
                LineUpPlayer _lineUpPlayer = new LineUpPlayer();

                if (null != PlayersTeamSelected[i].Profile.PlayerId && null != PlayersTeamSelected[i].Profile.SquadPosition && false == positions.Contains(PlayersTeamSelected[i].Profile.SquadPosition))
                {
                    _lineUpPlayer.id = PlayersTeamSelected[i].Profile.PlayerId;
                    _lineUpPlayer.position = PlayersTeamSelected[i].Profile.SquadPosition;
                    lineUp.Players.Add(_lineUpPlayer);
                    positions.Add(PlayersTeamSelected[i].Profile.SquadPosition);
                }
            }

            SquadMain.CreateOrUpdateLineUp(lineUp);
        }

        public void GetSquadPlayers()
        {
            SquadMain.GetSquadPlayers(PVPData.TeamId);
        }

        public void SetPlayerToMinion()
        {
            bool lFindPlayer = false;
            for (int i = 0; i < PlayersTeamSelected.Count; i++)
            {
                if (PlayersTeamSelected[i].Profile.SquadPosition == MinionFieldSquadPositionSelected)
                {
                    PlayersTeamSelected[i] = MinionScrollviewSelected;
                    lFindPlayer = true;
                }
            }

            if (false == lFindPlayer)
            {
                PlayersTeamSelected.Add(MinionScrollviewSelected);
            }

            MinionsPlayers.Instance.UpdateTeamPlayersField();

        }

        #endregion

        #region Private Methods
        void Awake()
        {
            Instance = this;
        }
        #endregion
    }
}

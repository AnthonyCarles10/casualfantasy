﻿using System;
using System.Collections;
using DG.Tweening;
using Sportfaction.CasualFantasy.Players;
using Sportfaction.CasualFantasy.Services;
using SportFaction.CasualFootballEngine.PlayerService.Enum;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Sportfaction.CasualFantasy.Meta
{
    /// <summary>
    /// Description: Fantasy Players Scrollview Item
    /// Author: Antoine de Lachèze-Murel
    /// </summary>
    public class FantasyPlayersScrollviewItem : MonoBehaviour, IPointerClickHandler
    {

        public PlayersItem Player;

        private Color _greenColor;

      

        #region Public Methods

        /// <summary>
        /// Init to set data
        /// <param name="pPlayer">PlayersItem</param>
        /// </summary>
        public void Init(PlayersItem pPlayer)
        {
            Player = pPlayer;
            UpdateUI();
        }

        public void UpdateUI()
        {

            Sprite[] lSpritesMenu = Resources.LoadAll<Sprite>("Sprites/Meta/Menu_2_SS");

            this.transform.Find("Selected_Bkg").gameObject.SetActive(false);

            this.transform.Find("Score_Txt").GetComponent<TMPro.TMP_Text>().text = Player.Profile.Score.ToString();

            this.transform.Find("Position_Txt").GetComponent<TMPro.TMP_Text>().text = I2.Loc.LocalizationManager.GetTranslation(Player.Profile.GlobalPosition + "short");

            this.transform.Find("Name_Txt").GetComponent<TMPro.TMP_Text>().text = true == String.IsNullOrEmpty(Player.Profile.NameField) ? Player.Profile.ShortName : Player.Profile.NameField;

            this.transform.Find("Championship_Txt").GetComponent<TMPro.TMP_Text>().text = I2.Loc.LocalizationManager.GetTranslation(Player.Profile.ChampionshipName);

            this.transform.Find("Club_Txt").GetComponent<TMPro.TMP_Text>().text = Player.Profile.TeamName;

            this.transform.Find("OnField_Grp").gameObject.SetActive(false);

            this.transform.Find("Lock_Grp").gameObject.SetActive(false);

            Sprite[] lSpritesMenuFlag = Resources.LoadAll<Sprite>("Sprites/Meta/Flag");
            string country = Player.Profile.Country.Replace(" ", "-").ToLower();

            Sprite lSprite = UtilityMethods.GetSprite(lSpritesMenuFlag, country);

            if (lSprite == null)
            {
                if (MetaData.Instance.MissFlag.ContainsKey(country))
                {
                    country = MetaData.Instance.MissFlag[country];
                    lSprite = UtilityMethods.GetSprite(lSpritesMenuFlag, country);
                }
            }

            if (lSprite == null)
            {
                Debug.Log("Country Flag Missed : " + country);
            }
            Image lImage = this.transform.Find("Flag_Img").GetComponent<Image>();
            lImage.sprite = UtilityMethods.GetSprite(lSpritesMenuFlag, country);
            Color lColor = lImage.color;
            if (lSprite == null)
            {
                lColor.a = 0;
            }
            else
            {
                lColor.a = 1f;
            }
            lImage.color = lColor;



            //this.transform.Find("Form_Grp/Form_Txt").GetComponent<TMPro.TMP_Text>().text = Data.Instance.FormLevelLetter[Player.Profile.FormLevel];

            switch (MetaData.Instance.FormLevelLetter[Player.Profile.FormLevel])
            {
                case "A":
                    this.transform.Find("Form_Grp/Form_Img").GetComponent<Image>().sprite = UtilityMethods.GetSprite(lSpritesMenu, "Form_Flame_Player_A");
                    break;
                case "B":
                    this.transform.Find("Form_Grp/Form_Img").GetComponent<Image>().sprite = UtilityMethods.GetSprite(lSpritesMenu, "Form_Flame_Player_B");
                    break;
                case "C":
                    this.transform.Find("Form_Grp/Form_Img").GetComponent<Image>().sprite = UtilityMethods.GetSprite(lSpritesMenu, "Form_Flame_Player_C");
                    break;
                case "D":
                    this.transform.Find("Form_Grp/Form_Img").GetComponent<Image>().sprite = UtilityMethods.GetSprite(lSpritesMenu, "Form_Flame_Player_D");
                    break;
            }


            //this.transform.Find("Form_Grp/Form_Label_Grp/ALevel_Grp/CurrentValue_Grp").gameObject.SetActive(false);
            //this.transform.Find("Form_Grp/Form_Label_Grp/ALevel_Grp/Title_Txt").GetComponent<TMPro.TMP_Text>().fontSize = 50;
            //this.transform.Find("Form_Grp/Form_Label_Grp/ALevel_Grp/Title_Txt").GetComponent<TMPro.TMP_Text>().color = Color.black;
            //this.transform.Find("Form_Grp/Form_Label_Grp/ALevel_Grp/Title_Txt").GetComponent<TMPro.TMP_Text>().fontSharedMaterial = FantasyPlayers.Instance.RobotoBoldWithoutStroke;

            //this.transform.Find("Form_Grp/Form_Label_Grp/BLevel_Grp/CurrentValue_Grp").gameObject.SetActive(false);
            //this.transform.Find("Form_Grp/Form_Label_Grp/BLevel_Grp/Title_Txt").GetComponent<TMPro.TMP_Text>().fontSize = 50;
            //this.transform.Find("Form_Grp/Form_Label_Grp/BLevel_Grp/Title_Txt").GetComponent<TMPro.TMP_Text>().color = Color.black;
            //this.transform.Find("Form_Grp/Form_Label_Grp/BLevel_Grp/Title_Txt").GetComponent<TMPro.TMP_Text>().fontSharedMaterial = FantasyPlayers.Instance.RobotoBoldWithoutStroke;

            //this.transform.Find("Form_Grp/Form_Label_Grp/CLevel_Grp/CurrentValue_Grp").gameObject.SetActive(false);
            //this.transform.Find("Form_Grp/Form_Label_Grp/CLevel_Grp/Title_Txt").GetComponent<TMPro.TMP_Text>().fontSize = 50;
            //this.transform.Find("Form_Grp/Form_Label_Grp/CLevel_Grp/Title_Txt").GetComponent<TMPro.TMP_Text>().color = Color.black;
            //this.transform.Find("Form_Grp/Form_Label_Grp/CLevel_Grp/Title_Txt").GetComponent<TMPro.TMP_Text>().fontSharedMaterial = FantasyPlayers.Instance.RobotoBoldWithoutStroke;

            //this.transform.Find("Form_Grp/Form_Label_Grp/DLevel_Grp/CurrentValue_Grp").gameObject.SetActive(false);
            //this.transform.Find("Form_Grp/Form_Label_Grp/DLevel_Grp/Title_Txt").GetComponent<TMPro.TMP_Text>().fontSize = 50;
            //this.transform.Find("Form_Grp/Form_Label_Grp/DLevel_Grp/Title_Txt").GetComponent<TMPro.TMP_Text>().color = Color.black;
            //this.transform.Find("Form_Grp/Form_Label_Grp/DLevel_Grp/Title_Txt").GetComponent<TMPro.TMP_Text>().fontSharedMaterial = FantasyPlayers.Instance.RobotoBoldWithoutStroke;


            //Debug.Log("Player Form Level : " + Player.Profile.FormLevel);


            //switch (Player.Profile.FormLevel)
            //{
            //    case 3:
            //        this.transform.Find("Form_Grp/Form_Label_Grp/ALevel_Grp/CurrentValue_Grp/Title_Txt").GetComponent<TMPro.TMP_Text>().text = Player.Profile.Form.ToString();
            //        this.transform.Find("Form_Grp/Form_Label_Grp/ALevel_Grp/CurrentValue_Grp").gameObject.SetActive(true);
            //        this.transform.Find("Form_Grp/Form_Label_Grp/ALevel_Grp/Title_Txt").GetComponent<TMPro.TMP_Text>().fontSize = 80;
            //        this.transform.Find("Form_Grp/Form_Label_Grp/ALevel_Grp/Title_Txt").GetComponent<TMPro.TMP_Text>().color = _greenColor;
            //        this.transform.Find("Form_Grp/Form_Label_Grp/ALevel_Grp/Title_Txt").GetComponent<TMPro.TMP_Text>().fontSharedMaterial = FantasyPlayers.Instance.RobotoBold;


            //        break;
            //    case 2:
            //        this.transform.Find("Form_Grp/Form_Label_Grp/BLevel_Grp/CurrentValue_Grp/Title_Txt").GetComponent<TMPro.TMP_Text>().text = Player.Profile.Form.ToString();
            //        this.transform.Find("Form_Grp/Form_Label_Grp/BLevel_Grp/CurrentValue_Grp").gameObject.SetActive(true);
            //        this.transform.Find("Form_Grp/Form_Label_Grp/BLevel_Grp/Title_Txt").GetComponent<TMPro.TMP_Text>().fontSize = 80;
            //        this.transform.Find("Form_Grp/Form_Label_Grp/BLevel_Grp/Title_Txt").GetComponent<TMPro.TMP_Text>().color = _greenColor;
            //        this.transform.Find("Form_Grp/Form_Label_Grp/BLevel_Grp/Title_Txt").GetComponent<TMPro.TMP_Text>().fontSharedMaterial = FantasyPlayers.Instance.RobotoBold;
            //        break;
            //    case 1:
            //        this.transform.Find("Form_Grp/Form_Label_Grp/CLevel_Grp/CurrentValue_Grp/Title_Txt").GetComponent<TMPro.TMP_Text>().text = Player.Profile.Form.ToString();
            //        this.transform.Find("Form_Grp/Form_Label_Grp/CLevel_Grp/CurrentValue_Grp").gameObject.SetActive(true);
            //        this.transform.Find("Form_Grp/Form_Label_Grp/CLevel_Grp/Title_Txt").GetComponent<TMPro.TMP_Text>().fontSize = 80;
            //        this.transform.Find("Form_Grp/Form_Label_Grp/CLevel_Grp/Title_Txt").GetComponent<TMPro.TMP_Text>().color = _greenColor;
            //        this.transform.Find("Form_Grp/Form_Label_Grp/CLevel_Grp/Title_Txt").GetComponent<TMPro.TMP_Text>().fontSharedMaterial = FantasyPlayers.Instance.RobotoBold;
            //        break;
            //    case 0:
            //        this.transform.Find("Form_Grp/Form_Label_Grp/DLevel_Grp/CurrentValue_Grp/Title_Txt").GetComponent<TMPro.TMP_Text>().text = Player.Profile.Form.ToString();
            //        this.transform.Find("Form_Grp/Form_Label_Grp/DLevel_Grp/CurrentValue_Grp").gameObject.SetActive(true);
            //        this.transform.Find("Form_Grp/Form_Label_Grp/DLevel_Grp/Title_Txt").GetComponent<TMPro.TMP_Text>().fontSize = 80;
            //        this.transform.Find("Form_Grp/Form_Label_Grp/DLevel_Grp/Title_Txt").GetComponent<TMPro.TMP_Text>().color = _greenColor;
            //        this.transform.Find("Form_Grp/Form_Label_Grp/DLevel_Grp/Title_Txt").GetComponent<TMPro.TMP_Text>().fontSharedMaterial = FantasyPlayers.Instance.RobotoBold;
            //        break;
            //    default:
            //        this.transform.Find("Form_Grp/Form_Label_Grp/DLevel_Grp/CurrentValue_Grp/Title_Txt").GetComponent<TMPro.TMP_Text>().text = Player.Profile.Form.ToString();
            //        this.transform.Find("Form_Grp/Form_Label_Grp/DLevel_Grp/CurrentValue_Grp").gameObject.SetActive(true);
            //        this.transform.Find("Form_Grp/Form_Label_Grp/DLevel_Grp/Title_Txt").GetComponent<TMPro.TMP_Text>().fontSize = 80;
            //        this.transform.Find("Form_Grp/Form_Label_Grp/DLevel_Grp/Title_Txt").GetComponent<TMPro.TMP_Text>().color = _greenColor;
            //        this.transform.Find("Form_Grp/Form_Label_Grp/DLevel_Grp/Title_Txt").GetComponent<TMPro.TMP_Text>().fontSharedMaterial = FantasyPlayers.Instance.RobotoBold;
            //        break;
            //}



            if(true == IsPlayerItem(Player.Profile.PlayerId))
            {
                //Player is on Field

                //this.transform.GetComponent<Image>().sprite = UtilityMethods.GetSprite(lSpritesMenu, "Card_Green");
                this.transform.Find("OnField_Grp").gameObject.SetActive(true);
                this.transform.Find("Fade_Img").gameObject.SetActive(false);

            }
            else if(true == Player.Profile.IsOwned)
            {
                //Player is owned

                //this.transform.GetComponent<Image>().sprite = UtilityMethods.GetSprite(lSpritesMenu, "Card_Blue");
                this.transform.Find("Fade_Img").gameObject.SetActive(false);
            }
            else
            {
                //this.transform.GetComponent<Image>().sprite = UtilityMethods.GetSprite(lSpritesMenu, "Card_Grey");
                this.transform.Find("Lock_Grp").gameObject.SetActive(true);
                this.transform.Find("Fade_Img").gameObject.SetActive(true);
            }

            this.transform.GetComponent<Image>().sprite = UtilityMethods.GetSprite(lSpritesMenu, "Card_Rarity_" + (Player.Profile.RareLevel + 1) + "_Bck");

            // if selected Card
            if (false == string.IsNullOrEmpty(MetaData.Instance.MinionScrollviewPlayerIdSelected) && Player.Profile.PlayerId == MetaData.Instance.MinionScrollviewPlayerIdSelected)
            {
                this.transform.Find("Selected_Bkg").gameObject.SetActive(true);
            }



        }

        public void OnPointerClick(PointerEventData pointerEventData)
        {
 
            if(DefaultUIAnimation.Instance.TimePressed < 0.15)
            {
                MetaData.Instance.FantasyPlayerScrollviewSelected = Player;

                StartCoroutine(AnimateCard());

                this.transform.Find("Selected_Bkg").gameObject.SetActive(true);

                MetaData.Instance.GetPlayerLastGame(Player.Profile.PlayerId);

            }
        }


        #endregion

        private IEnumerator AnimateCard()
        {
            Sprite[] lSpritesMenu = Resources.LoadAll<Sprite>("Sprites/Meta/Menu_SS");


            //Debug.Log(FantasyPlayers.Instance.PlayersScrollGrp_Gob.GetComponentsInChildren<FantasyPlayersScrollviewItem>().Length);

            foreach (FantasyPlayersScrollviewItem item in FantasyPlayers.Instance.PlayersScrollGrp_Gob.GetComponentsInChildren<FantasyPlayersScrollviewItem>())
            {
                //if (item != this && item.transform.localPosition.y != 0f && false == IsPlayerItem(item.Player.Profile.PlayerId))
                //{
                //    item.transform.DOLocalMoveY(0f, 0.3f);
                //    item.transform.GetComponent<Image>().sprite = UtilityMethods.GetSprite(lSpritesMenu, "Player_Card");
                //}

                if (item != this)
                {
                    item.transform.Find("Selected_Bkg").gameObject.SetActive(false);
                }

            }

            //this.transform.GetComponent<Image>().sprite = UtilityMethods.GetSprite(lSpritesMenu, "Player_Card_Selection");

            this.transform.parent.transform.parent.GetComponent<ScrollRect>().enabled = false;
            //this.transform.GetComponent<RectTransform>().DOLocalMoveY(20f, 0.3f);
            yield return new WaitForSeconds(0.3f); // wait animation transition
            this.transform.parent.transform.parent.GetComponent<ScrollRect>().enabled = true;
        }


        private bool IsPlayerItem(string pPlayerId)
        {
            foreach (PlayersItem item in MetaData.Instance.PlayersTeamSelected)
            {
                if (item.Profile.PlayerId == pPlayerId)
                {
                    return true;
                }
            }

            return false;
        }

        private void Awake()
        {
            ColorUtility.TryParseHtmlString("#7FFF00", out _greenColor);

        }


    }
}

using DG.Tweening;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace Sportfaction.CasualFantasy.Meta
{
    /// <summary>
    /// Description: DefaultUIAnimation
    /// Author: Antoine de Lachèze-Murel
    /// </summary>
    public class DefaultUIAnimation : MonoBehaviour
    {
        public static DefaultUIAnimation Instance;

        #region Properties

        public float TimePressed { get { return _timePressed; } }

        public bool IsGetMouseButtonDown { get { return _isGetMouseButtonDown; } }

        public Vector2 TouchDeltaPosition { get { return _touchDeltaPosition; } }

        public GameObject EventSystem { get { return _eventSystem; } }

        #endregion

        #region Fields

        [Header("UI")]

        [SerializeField, Tooltip("Event System")]
        private GameObject _eventSystem = null;

        [Header("Touch Movements")]

        [SerializeField, ReadOnly, Tooltip("Time Pressed")]
        private float _timePressed = 0f;

        [SerializeField, ReadOnly, Tooltip("TouchDeltaPosition")]
        private Vector2 _touchDeltaPosition = new Vector2();

        [SerializeField, ReadOnly, Tooltip("Is Get Mouse Button Down")]
        private bool _isGetMouseButtonDown = false;

        #endregion

        #region Public Methods
        
        /// <summary>
        /// Animation: FadeIn / FadeOut Backgrounds
        /// <param name="pBackgroundIn">GO to fade in</param>
        /// <param name="pBackgroundOut">GO to fade out</param>
        /// <param name="pDelayTime">Delay of fade animation</param>
        /// </summary>
        public void FadeInOutBackground(GameObject pBackgroundIn, GameObject pBackgroundOut, float pDelayTime = 1f)
        {
            pBackgroundIn.SetActive(true);
            if(pBackgroundIn.transform.childCount > 0) {
                foreach (Transform child in pBackgroundIn.transform)
                {
                    if(null != child.GetComponent<Image>())
                    {
                        Image lBackgroundIn = child.GetComponent<Image>();
                        lBackgroundIn.DOFade(1f, pDelayTime); 
                    }
                }

            } else {
                if (null != pBackgroundIn.GetComponent<Image>())
                {
                    Image lBackgroundIn = pBackgroundIn.GetComponent<Image>();
                    lBackgroundIn.DOFade(1f, pDelayTime);
                }
            }

            if (pBackgroundOut.transform.childCount > 0)
            {
                foreach (Transform child in pBackgroundOut.transform)
                {
                    if (null != child.GetComponent<Image>())
                    {
                        Image lBackgroundOut = child.GetComponent<Image>();
                        lBackgroundOut.DOFade(0f, pDelayTime);
                    }
                }
                StartCoroutine(ChangeStatusGameObject(pBackgroundOut, false, pDelayTime));
            }
            else
            {
                if (null != pBackgroundOut.GetComponent<Image>())
                {
                    Image lBackgroundOut = pBackgroundOut.GetComponent<Image>();
                    lBackgroundOut.DOFade(0f, pDelayTime);
                }

            }
        }

        /// <summary>
        /// Animation: FadeIn Backgrounds
        /// <param name="pBackgroundIn">GO to fade in</param>
        /// <param name="pDelayTime">Delay of fade animation</param>
        /// </summary>
        public void FadeInBackground(GameObject pBackgroundIn, float pDelayTime = 1f)
        {
            pBackgroundIn.SetActive(true);
            if (pBackgroundIn.transform.childCount > 0)
            {
                foreach (Transform child in pBackgroundIn.transform)
                {
                    if (null != child.GetComponent<Image>())
                    {
                        Image lBackgroundIn = child.GetComponent<Image>();
                        lBackgroundIn.DOFade(1f, pDelayTime);
                    }
                }

            }
            else
            {
                if (null != pBackgroundIn.GetComponent<Image>())
                {
                    Image lBackgroundIn = pBackgroundIn.GetComponent<Image>();
                    lBackgroundIn.DOFade(1f, pDelayTime);
                }
            }

        }

        /// <summary>
        /// Animation: FadeOut Backgrounds
        /// <param name="pBackgroundOut">GO to fade out</param>
        /// <param name="pDelayTime">Delay of fade animation</param>
        /// </summary>
        public void FadeOutBackground(GameObject pBackgroundOut, float pDelayTime = 1f)
        {
            if (pBackgroundOut.transform.childCount > 0)
            {
                foreach (Transform child in pBackgroundOut.transform)
                {
                    Image lBackgroundOut = child.GetComponent<Image>();
                    lBackgroundOut.DOFade(0f, pDelayTime);
                }
                StartCoroutine(ChangeStatusGameObject(pBackgroundOut, false, pDelayTime));
            }
            else
            {
                Image lBackgroundOut = pBackgroundOut.GetComponent<Image>();
                lBackgroundOut.DOFade(0f, pDelayTime);
            }
        }

        /// <summary>
        /// Animation: FadeIn / FadeOut Canvas Group
        /// <param name="pCanvasGroupIn">GameObject</param>
        /// <param name="pCanvasGroupOut">GameObject</param>
        /// <param name="pDelayTime">Delay of fade animation</param>
        /// <param name="pDisableEvent">Should broadcast event when animation finished ?</param>
        /// </summary>
        public void FadeInOutCanvasGroup(GameObject pCanvasGroupIn, GameObject pCanvasGroupOut, float pDelayTime = 0.3f, bool pDisableEvent = true)
        {
          
            pCanvasGroupIn.SetActive(true);
            StartCoroutine(ChangeStatusGameObject(pCanvasGroupOut, false, pDelayTime));

            // Enable In Canvas and Disbale Out Canvas After Animation (1s)
            // StartCoroutine(ChangeStatusCanvasInOut(pCanvasGroupIn, pCanvasGroupOut, 1f));

            CanvasGroup lCanvasGroupIn = pCanvasGroupIn.GetComponent<CanvasGroup>();
            CanvasGroup lCanvasGroupOut = pCanvasGroupOut.GetComponent<CanvasGroup>();

            Sequence lSequence = DOTween.Sequence();
            lSequence.Insert(0, lCanvasGroupOut.DOFade(0f, pDelayTime));
            lSequence.Insert(0, lCanvasGroupIn.DOFade(1f, pDelayTime));


            if (true == pDisableEvent)
            {
                _eventSystem.SetActive(false);
                StartCoroutine(ActivateEventSytemWithDelay(pDelayTime));
            }
        }

        /// <summary>
        /// Animation: FadeIn Canvas Group
        /// <param name="pCanvasGroupIn">GameObject</param>
        /// <param name="pDelayTime">Delay of fade animation</param>
        /// <param name="pDisableEvent">Should broadcast event when animation finished ?</param>
        /// </summary>
        public void FadeInCanvasGroup(GameObject pCanvasGroupIn, float pDelayTime = 1f, bool pDisableEvent = true)
        {     
            pCanvasGroupIn.SetActive(true);

            CanvasGroup lCanvasGroupIn = pCanvasGroupIn.GetComponent<CanvasGroup>();

            lCanvasGroupIn.DOFade(1f, pDelayTime);

            if (true == pDisableEvent)
            {
                _eventSystem.SetActive(false);
                StartCoroutine(ActivateEventSytemWithDelay(pDelayTime));
            }
        }

        /// <summary>
        /// Animation: FadeOut Canvas Group
        /// <param name="pCanvasGroupOut">GameObject</param>
        /// <param name="pDelayTime">Delay of fade animation</param>
        /// <param name="pDisableEvent">Should broadcast event when animation finished ?</param>
        /// </summary>
        public void FadeOutCanvasGroup(GameObject pCanvasGroupOut, float pDelayTime = 1f, bool pDisableEvent = true)
        {

            StartCoroutine(ChangeStatusGameObject(pCanvasGroupOut, false, pDelayTime));

            CanvasGroup lCanvasGroupOut = pCanvasGroupOut.GetComponent<CanvasGroup>();

            lCanvasGroupOut.DOFade(0, pDelayTime);

            if (true == pDisableEvent)
            {
                _eventSystem.SetActive(false);
                StartCoroutine(ActivateEventSytemWithDelay(pDelayTime));
            }
        }
        
        /// <summary>
        /// Animation: Move and Rotate back Button to his position
        /// <param name="pBackButton">GameObject</param>
        /// <param name="moveToPosition">Vector3</param>
        /// </summary>
        //public void Animate3DBackButton(GameObject pBackButton, Vector3 moveToPosition)
        //{
        //    pBackButton.GetComponent<Collider>().enabled = false;

        //    RectTransform lBackButton = pBackButton.GetComponent<RectTransform>();

        //    Sequence lSequence = DOTween.Sequence();
        //    lSequence.Append(lBackButton.DOLocalRotate(new Vector3(0, -540f, lBackButton.localRotation.z), 1f, RotateMode.FastBeyond360));
        //    lSequence.Insert(0, lBackButton.DOLocalMove(moveToPosition, 1f));
        //    lSequence.SetEase(Ease.InOutSine);
        //    lSequence.Play();

        //    StartCoroutine(ChangeStatusCollider(pBackButton, true, 1f));
        //}

        /// <summary>
        /// Animation: Button go to positon And Rotate
        /// <param name="pButton">GameObject</param>
        /// <param name="moveToPosition">Vector3</param>
        /// </summary>
        //public void Animate3DButton(GameObject pButton, Vector3 moveToPosition)
        //{
        //    StartCoroutine(ChangeStatusGameObject(pButton, true, 1f));

        //    RectTransform lLadderButton = pButton.GetComponent<RectTransform>();

        //    Sequence lSequence = DOTween.Sequence();
        //    lSequence.Append(lLadderButton.DOLocalRotate(new Vector3(0, -540f, lLadderButton.rotation.z), 1f, RotateMode.FastBeyond360));
        //    lSequence.Insert(0, lLadderButton.DOLocalMove(moveToPosition, 1.0f));
        //    lSequence.SetEase(Ease.InOutSine);
        //    lSequence.Play();
        //}

        public void AnimateButtonEnter(Transform pButton)
        {
            Sequence lSequence = DOTween.Sequence();
            lSequence.Insert(0, pButton.DOScale(1.1f, 0.1f));
            lSequence.Play();
        }

        public void AnimateButtonUp(Transform pButton)
        {
            Sequence lSequence = DOTween.Sequence();
            lSequence.Insert(0.1f, pButton.DOScale(1f, 0.1f));
            lSequence.Play();
        }

        /// <summary>
        /// Delete parent's children
        /// <param name="pObj">GameObject</param>
        /// </summary>
        public void DeleteAllChilds(GameObject pObj)
        {
            if (pObj != null)
            {
                for (int i = 0; i < pObj.transform.childCount; i++)
                {
                    Destroy(pObj.transform.GetChild(i).gameObject, 0f);
                }
            }
            else Debug.LogError("GameObject does not exist anymore!");
        }

        #region Coroutines
        
        /// <summary>
        /// Animation: FadeIn Canvas Group
        /// <param name="pCanvasGroupIn">GO to fade in</param>
        /// <param name="pDelayTime">Delay of fade animation</param>
        /// <param name="pAnimDuration">Duration of fade animation</param>
        /// <param name="pDisableEvent">Should broadcast event when animation finished ?</param>
        /// </summary>
        public IEnumerator FadeInCanevasWithDelay(GameObject pCanvasGroupIn, float pDelayTime = 1f, float pAnimDuration = 1f, bool pDisableEvent = true)
        {
            yield return new WaitForSeconds(pDelayTime);

            CanvasGroup lCanvasGroupIn = pCanvasGroupIn.GetComponent<CanvasGroup>();

            lCanvasGroupIn.DOFade(1f, pAnimDuration);

            StartCoroutine(ActivateEventSytemWithDelay(pAnimDuration));

            if (true == pDisableEvent)
            {
                _eventSystem.SetActive(false);
                StartCoroutine(ActivateEventSytemWithDelay(pAnimDuration));
            }
        }

        public IEnumerator ChangeStatusGameObject(GameObject pCanvas, bool pStatus, float pDelayTime)
        {
            yield return new WaitForSeconds(pDelayTime);
            pCanvas.SetActive(pStatus);
        }

        public IEnumerator ChangePositionGameObject(GameObject pGo, Vector3 pVect, float pDelayTime)
        {
            yield return new WaitForSeconds(pDelayTime);
            pGo.GetComponent<RectTransform>().localPosition = pVect;
        }

        /// <summary>
        /// Displays/Hides specified game object after specified delay
        /// </summary>
        /// <param name="pGo">GO to show/hide</param>
        /// <param name="pStatus">Should GO be displayed ?</param>
        /// <param name="pDelayTime">Delay time before display change</param>
        public IEnumerator DelayedStatus(GameObject pGo, bool pStatus, float pDelayTime)
        {
            yield return new WaitForSeconds(pDelayTime);
            pGo.SetActive(pStatus);
        }

        public IEnumerator ActivateEventSytemWithDelay(float pDelayTime)
        {
            yield return new WaitForSeconds(pDelayTime);
            _eventSystem.SetActive(true);
        }

        /// <summary>
        /// Animation: Enable / Disable button
        /// <param name="pButton">Button to enable/disable</param>
        /// <param name="pStatus">Should button be enabled ?</param>
        /// <param name="pDelayTime">Delay time before status change</param>
        /// </summary>
        public IEnumerator ChangeInteractableButton(Button pButton, bool pStatus, float pDelayTime)
        {
            yield return new WaitForSeconds(pDelayTime);
            pButton.interactable = pStatus;
        }

        /// <summary>
        /// Animation: Enable / Disable GO
        /// <param name="pGo">Button GO to enable/disable</param>
        /// <param name="pStatus">Should button be enabled ?</param>
        /// <param name="pDelayTime">Delay time before status change</param>
        /// </summary>
        public IEnumerator ChangeStatusCollider(GameObject pGo, bool pStatus, float pDelayTime)
        {
            yield return new WaitForSeconds(pDelayTime);
            pGo.GetComponent<Collider>().enabled = pStatus;
        }

        #endregion

        #endregion

        #region Private Methods
        
        private void        Update()
        {
            if (true == _isGetMouseButtonDown)
            {
                _timePressed += Time.deltaTime;
            } else {
                _timePressed = 0;
            }

            if (Input.GetMouseButtonDown(0))
            {
                _isGetMouseButtonDown = true;
            }

            if (Input.GetMouseButtonUp(0))
            {
                _isGetMouseButtonDown = false;
            }

        }

        void Awake()
        {
            Instance = this;
        }
        
        #endregion
    }

}

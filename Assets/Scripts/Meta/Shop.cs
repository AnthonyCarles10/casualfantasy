﻿
using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using Sportfaction.CasualFantasy.Manager.Profile;
using Sportfaction.CasualFantasy.Services.Billing;
using Sportfaction.CasualFantasy.Services.Timers;
using Sportfaction.CasualFantasy.Services.Userstracking;
using Sportfaction.CasualFantasy.Shop;
using UnityEngine;
using UnityEngine.Purchasing;

namespace Sportfaction.CasualFantasy.Meta
{
    /// <summary>
    /// Description: LootBox Animation
    /// Author: Antoine de Lachèze-Murel
    /// </summary>
    public class Shop : MonoBehaviour
    {
        public static Shop Instance;

        #region Public Methods
 
        public GameObject MediumBox_Gob;
        public GameObject BigBox_Gob;
        public GameObject PromoPack_Gob;
        public GameObject BuyHardCurrencyGrp_Gob;
        public GameObject Hard_Gob;

        public GameObject Loader_Gob;

        #endregion

        public void OnLocalTimersUpdated_GE()
        {
            if (true == LocalTimersManager.Instance.IsLocalTimerExists("starter_1"))
            {
                TimeSpan lTimeLeft = LocalTimersManager.Instance.GetTimeLeft("starter_1");
                string lTime = "";
                if (lTimeLeft.Days > 0)
                {
                    lTime += lTimeLeft.ToString("dd") + string.Format(I2.Loc.LocalizationManager.GetTranslation("day_short")) + " ";
                }
                if (lTimeLeft.Hours > 0)
                {
                    lTime += lTimeLeft.ToString("hh") + "H ";
                }
                if (lTimeLeft.Minutes > 0)
                {
                    lTime += lTimeLeft.ToString("mm") + "min ";
                }
                lTime += lTimeLeft.ToString("ss") + "sec";
                PromoPack_Gob.transform.Find("Ended_Timer_Grp/Timer_Txt").GetComponent<TMPro.TMP_Text>().text = string.Format(I2.Loc.LocalizationManager.GetTranslation("home_arenacup_ended_title"), lTime);
                PromoPack_Gob.SetActive(true);
            }
            else
            {
                PromoPack_Gob.SetActive(false);
            }

        }

        public void OnCashPurchaseSucceed_GE()
        {
            //BackButton_Gob.SetActive(true);
            DefaultUIAnimation.Instance.FadeOutCanvasGroup(Loader_Gob, 0.3f);
        }

        public void OnCashPurchaseFailed_GE()
        {
            //BackButton_Gob.SetActive(true);
            DefaultUIAnimation.Instance.FadeOutCanvasGroup(Loader_Gob, 0.3f);
        }

        public void OnProductPurchaseFailedEvent_GE()
        {
            //BackButton_Gob.SetActive(true);
            DefaultUIAnimation.Instance.FadeOutCanvasGroup(Loader_Gob, 0.3f);
        }

        public void OnProductPurchaseSucceedEvent_GE()
        {
            //BackButton_Gob.SetActive(true);
            DefaultUIAnimation.Instance.FadeOutCanvasGroup(Loader_Gob, 0.3f);
        }

        public void OnPurchaseFailedEvent_GE()
        {
            //BackButton_Gob.SetActive(true);
            DefaultUIAnimation.Instance.FadeOutCanvasGroup(Loader_Gob, 0.3f);
        }

        public void BuyProductWithHardCurrency(string pRef)
        {
        
            string lTag = pRef.Substring(29);
            UserEventsManager.Instance.RecordEvent(new UserActionEvent("purchase_init_" + lTag));
            //ProductItemData lProduct = Data.Instance.ShopMain.ProductData.Items.Find(item => item.Reference == pRef);
            DefaultUIAnimation.Instance.FadeInCanvasGroup(Loader_Gob, 0.3f);
            MetaData.Instance.ShopMain.BuyProduct(pRef, null);
        }

        public void BuyProductWithRealMoney(string pRef)
        {

            string lTag = pRef.Substring(29);
            UserEventsManager.Instance.RecordEvent(new UserActionEvent("purchase_init_" + lTag));
            DefaultUIAnimation.Instance.FadeInCanvasGroup(Loader_Gob, 0.3f);
            IAPStoreManager.Instance.InitiatePurchase(pRef);
        }

        public void Init()
        {

            List<ProductItemData> lProductsData= MetaData.Instance.ShopMain.ProductData.Items;
            List<Product> lIAPProducts = MetaData.Instance.IAPProductsData.Products;

            //Product lProduct = Data.Instance.IAPProductsData.Products.Find(item => item.ref == Data.Instance.ShopCashData.Items[i].re);
            for (int i = 0; i <= lProductsData.Count - 1; i++)
            {
                Product lProduct = lIAPProducts.Find(item => item.definition.id == lProductsData[i].Reference);
                if(lProduct != null)
                {
                    switch (lProductsData[i].Reference)
                    {
                        case "com.sportfaction.soccerarena.starter_1":
                            PromoPack_Gob.transform.Find("Price_Txt").GetComponent<TMPro.TMP_Text>().text = lProduct.metadata.localizedPriceString + " " + lProduct.metadata.isoCurrencyCode;
                            PromoPack_Gob.transform.Find("Star_Img/Value_Txt").GetComponent<TMPro.TMP_Text>().text = "-66%";
                            PromoPack_Gob.name = lProductsData[i].Reference;
                            break;
                        case "com.sportfaction.soccerarena.hard_currency_1":
                            BuyHardCurrencyGrp_Gob.transform.GetChild(0).Find("Price_Txt").GetComponent<TMPro.TMP_Text>().text = lProduct.metadata.localizedPriceString + " " + lProduct.metadata.isoCurrencyCode;
                            BuyHardCurrencyGrp_Gob.transform.GetChild(0).Find("Hard_Txt").GetComponent<TMPro.TMP_Text>().text = lProductsData[i].Hard.ToString();
                            BuyHardCurrencyGrp_Gob.transform.GetChild(0).Find("Star_Img").gameObject.SetActive(false);
                            BuyHardCurrencyGrp_Gob.transform.GetChild(0).name = lProductsData[i].Reference;
                            break;
                        case "com.sportfaction.soccerarena.hard_currency_2":
                            BuyHardCurrencyGrp_Gob.transform.GetChild(1).Find("Price_Txt").GetComponent<TMPro.TMP_Text>().text = lProduct.metadata.localizedPriceString + " " + lProduct.metadata.isoCurrencyCode;
                            BuyHardCurrencyGrp_Gob.transform.GetChild(1).Find("Hard_Txt").GetComponent<TMPro.TMP_Text>().text = lProductsData[i].Hard.ToString();
                            BuyHardCurrencyGrp_Gob.transform.GetChild(1).Find("Star_Img").gameObject.SetActive(true);
                            BuyHardCurrencyGrp_Gob.transform.GetChild(1).Find("Star_Img/Value_Txt").GetComponent<TMPro.TMP_Text>().text = "+30%";
                            BuyHardCurrencyGrp_Gob.transform.GetChild(1).name = lProductsData[i].Reference;
                            break;
                        case "com.sportfaction.soccerarena.hard_currency_3":
                            BuyHardCurrencyGrp_Gob.transform.GetChild(2).Find("Price_Txt").GetComponent<TMPro.TMP_Text>().text = lProduct.metadata.localizedPriceString + " " + lProduct.metadata.isoCurrencyCode;
                            BuyHardCurrencyGrp_Gob.transform.GetChild(2).Find("Hard_Txt").GetComponent<TMPro.TMP_Text>().text = lProductsData[i].Hard.ToString();
                            BuyHardCurrencyGrp_Gob.transform.GetChild(2).Find("Star_Img").gameObject.SetActive(true);
                            BuyHardCurrencyGrp_Gob.transform.GetChild(2).Find("Star_Img/Value_Txt").GetComponent<TMPro.TMP_Text>().text = "+60%";
                            BuyHardCurrencyGrp_Gob.transform.GetChild(2).name = lProductsData[i].Reference;
                            break;
                        case "com.sportfaction.soccerarena.hard_currency_4":
                            BuyHardCurrencyGrp_Gob.transform.GetChild(3).Find("Price_Txt").GetComponent<TMPro.TMP_Text>().text = lProduct.metadata.localizedPriceString + " " + lProduct.metadata.isoCurrencyCode;
                            BuyHardCurrencyGrp_Gob.transform.GetChild(3).Find("Hard_Txt").GetComponent<TMPro.TMP_Text>().text = lProductsData[i].Hard.ToString();
                            BuyHardCurrencyGrp_Gob.transform.GetChild(3).Find("Star_Img").gameObject.SetActive(true);
                            BuyHardCurrencyGrp_Gob.transform.GetChild(3).Find("Star_Img/Value_Txt").GetComponent<TMPro.TMP_Text>().text = "+75%";
                            BuyHardCurrencyGrp_Gob.transform.GetChild(3).name = lProductsData[i].Reference;
                            break;
                    }
                }
                else
                {
                    switch (lProductsData[i].Reference)
                    {
                        case "com.sportfaction.soccerarena.player_box_medium":
                            MediumBox_Gob.transform.Find("Hard_Grp/Hard_Txt").GetComponent<TMPro.TMP_Text>().text = lProductsData[i].PriceHard.ToString();
                            MediumBox_Gob.name = lProductsData[i].Reference;
                            break;
                        case "com.sportfaction.soccerarena.player_box_big":
                            BigBox_Gob.transform.Find("Hard_Grp/Hard_Txt").GetComponent<TMPro.TMP_Text>().text = lProductsData[i].PriceHard.ToString();
                            BigBox_Gob.name = lProductsData[i].Reference;
                            break;
                    }
                }


            }

        }

        void Start()
        {
            transform.Find("Hard_Currency_Grp/Value_Grp/Value_Txt").GetComponent<TMPro.TMP_Text>().text = UserData.Instance.Profile.Hard.ToString();
        }
        void Awake()
        {
            Instance = this;
        }

    }
}

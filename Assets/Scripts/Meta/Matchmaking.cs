﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using Newtonsoft.Json.Linq;
using Photon.Pun;
using Sportfaction.CasualFantasy.Config;
using Sportfaction.CasualFantasy.Events;
using Sportfaction.CasualFantasy.Manager.Profile;
using Sportfaction.CasualFantasy.PVP.Gameplay.Matchs;
using Sportfaction.CasualFantasy.PVP.Network;
using Sportfaction.CasualFantasy.Services;
using SportFaction.CasualFootballEngine.PhaseService.Enum;
using SportFaction.CasualFootballEngine.TeamService.Enum;
using UnityEngine;

namespace Sportfaction.CasualFantasy.Meta
{
    /// <summary>
    /// Description: Matchmaking
    /// Author: Antoine de Lachèze-Murel
    /// </summary>
    public sealed class Matchmaking : MonoBehaviour
    {
        public static Matchmaking Instance;

        #region Fields

        [Header("GO_Str_Arr")]
        public GameObject[] PrefabsIdxArray_Gob;
        public string[] PrefabsIdxArray_Str;

        [Header("Data")]
        [SerializeField, Tooltip("Scriptable object contains infos about match UI")]
        private PVPData _pvpData = null;

        [SerializeField, ReadOnly, Tooltip("Bots Pseudo")]
        private string _botsPseudo = "{ \"pseudos\":[\"Ceceg\", \"Gab\", \"Anto\", \"oioi\", \"Eddyth\", \"Kaligan\", \"Lord StClair\", \"zavier92\", \"oliv\", \"Mambo\", \"Xam Xam\",\"kcssp\", \"bracame\", \"Cab.\", \"sissou\", \"Craco\", \"Toniocourc\", \"McHillroyd\", \"philae\", \"AndrewTheKing\", \"Petilouis\",\"Doudou\", \"Captain Chaos\", \"Pitchou\", \"orphila\", \"labuche\", \"Bakugen\", \"Benat\", \"JovyB\", \"brasletti\", \"Winproof\",\"FlyinPoulpus\", \"Hitlight\", \"Jordijo\", \"hola987\", \"Marco\", \"Brigstone\", \"Erom\", \"julo2648\", \"Bel the bull\", \"noradus\",\"dovahkin\", \"bratch\", \"Samrock\", \"Doc Thor\", \"Pat56\", \"Wesker\", \"Azot\", \"KeppCool007\", \"Katim\", \"snobet76\"]}";

        [SerializeField, ReadOnly, Tooltip("Bots Pseudo Array")]
        string[] _botsPseudoArray = new string[] { };

        bool _opponentFinded = false;

        // Coroutine used for the countdown before playing against a bot
        public IEnumerator CountdownCoroutine = null;

        public GameObject BlueScoreBar;
        public GameObject RedScoreBar;


        private float _scoreBarWidthMax = 0;

        private int _scoreTeamBlue = 0;

        private int _scoreTeamRed = 0;

        private int _startCountdown = -1;

        #endregion

        #region Public Methods

        public void Cancel()
        {

            this.transform.Find("Header_Grp/Title_Txt").GetComponent<TMPro.TMP_Text>().text = I2.Loc.LocalizationManager.GetTranslation("title_window_matchmaking");
            PhotonMain.Instance.Cancel();

            if (null != CountdownCoroutine)
            {
                StopCoroutine(CountdownCoroutine);

                CountdownCoroutine = null;
            }
            _opponentFinded = false;
            _startCountdown = 5;
        }

        public IEnumerator SearchOpponent()
        {
            GameObject BlueSideGrp_Gob = PrefabsIdxArray_Gob[Array.IndexOf(PrefabsIdxArray_Str, "Blue_Side_Grp")];
            DefaultUIAnimation.Instance.FadeInCanvasGroup(BlueSideGrp_Gob);

            GameObject RedSideSearchGrp_Gob = PrefabsIdxArray_Gob[Array.IndexOf(PrefabsIdxArray_Str, "Red_Side_Search_Grp")];
            DefaultUIAnimation.Instance.FadeInCanvasGroup(RedSideSearchGrp_Gob);

            this.transform.Find("Red_Side_Search_Grp/Search_Grp").GetComponent<Animation>().enabled = true;

            RectTransform BlueScoreBarRect = BlueScoreBar.GetComponent<RectTransform>();
            BlueScoreBarRect.sizeDelta = new Vector2(0, BlueScoreBarRect.sizeDelta.y);
            Sequence lBlueSideScoreBar = DOTween.Sequence();
            float x = (float)((_scoreTeamBlue * _scoreBarWidthMax) / ConfigData.Instance.MAX_SCORE);
            x = (x > _scoreBarWidthMax) ? _scoreBarWidthMax : x;
            lBlueSideScoreBar.Insert(0, BlueScoreBarRect.DOSizeDelta(new Vector2(x, BlueScoreBarRect.sizeDelta.y), 2f));


            Transform ScoreTxt = this.transform.Find("Blue_Side_Grp/Team_Score_Grp/Score_Txt");
            ScoreTxt.GetComponent<TMPro.TMP_Text>().text = "0";
            int lCpt = 0; double lScore = 0;
            double lInterval = _scoreTeamBlue / 120;
            Debug.Log(lInterval);
            do
            {
                lScore += lInterval;
                ScoreTxt.GetComponent<TMPro.TMP_Text>().text = ((int)lScore).ToString();
                yield return new WaitForSeconds(0.01f);

                lCpt++;
            } while (lCpt <= 120);
            ScoreTxt.GetComponent<TMPro.TMP_Text>().text = _scoreTeamBlue.ToString();

            if (null != UserData.Instance)
            {
                if(true == UserData.Instance.Profile.OnBoarding)
                {
                    PhotonMain.Instance.LaunchInstantMatchAgainstBot();
                }
                else
                {
                    switch (UserData.Instance.Redirect)
                    {
                        case "matchmaking_pvp":
                            PhotonMain.Instance.Search();
                            UserData.Instance.Redirect = "";
                            break;
                        case "matchmaking_bot":
                            yield return new WaitForSeconds(2f);
                            PhotonMain.Instance.LaunchInstantMatchAgainstBot();
                            UserData.Instance.Redirect = "";
                            break;
                        default:
                            yield return new WaitForSeconds(2f);
                            PhotonMain.Instance.LaunchInstantMatchAgainstBot();
                            //PhotonMain.Instance.Search();
                            break;
                    }
                }
               
            }
        }

        /// <summary>
        /// PhotonBehaviour method called when PUN room is ready
        /// </summary>
        public IEnumerator OnOpponentFinded()
        {

            _opponentFinded = true;
        Audio.Instance.OnOpponentFinded();

            StartCoroutine(InitValuesTeamRed());

            GameObject RedSideSearchGrp_Gob = PrefabsIdxArray_Gob[Array.IndexOf(PrefabsIdxArray_Str, "Red_Side_Search_Grp")];
            this.transform.Find("Red_Side_Search_Grp/Search_Grp").GetComponent<Animation>().enabled = false;

            GameObject RedSideGrp_Gob = PrefabsIdxArray_Gob[Array.IndexOf(PrefabsIdxArray_Str, "Red_Side_Grp")];

            DefaultUIAnimation.Instance.FadeInOutCanvasGroup(RedSideGrp_Gob, RedSideSearchGrp_Gob);

            Sequence lSequence = DOTween.Sequence();
            lSequence.Insert(0, this.transform.Find("Header_Grp/Title_Txt").GetComponent<RectTransform>().DOScale(1.1f, 0.5f));
            lSequence.Insert(0.5f, this.transform.Find("Header_Grp/Title_Txt").GetComponent<RectTransform>().DOScale(1f, 0.5f));
            lSequence.SetLoops(-1);


            CountdownCoroutine  =   _countdownMatch();
            if (this.enabled)
                StartCoroutine(CountdownCoroutine);
            yield return null;
            //_startCountdown = 5;
            //do
            //{

            //    if (CountdownCoroutine == null)
            //    {
            //        break;
            //    }
            //    else
            //    {
            //        this.transform.Find("Header_Grp/Title_Txt").GetComponent<TMPro.TMP_Text>().text = I2.Loc.LocalizationManager.GetTranslation("matchmaking_begin_timer") + " " + _startCountdown + "s";
            //    }
            //    //if(_startCountdown == -1)
            //    // {
            //    //     _startCountdown = 5;
            //    //     this.transform.Find("Header_Grp/Title_Txt").GetComponent<TMPro.TMP_Text>().text = I2.Loc.LocalizationManager.GetTranslation("title_window_matchmaking");
            //    //     break;
            //    // }

            //    yield return new WaitForSeconds(1f);
            //    _startCountdown--;

            //} while (_startCountdown != 0);


            //if (null != CountdownCoroutine && _startCountdown == 0)
            //{
            //    this.transform.Find("Header_Grp/Title_Txt").GetComponent<TMPro.TMP_Text>().text = I2.Loc.LocalizationManager.GetTranslation("matchmaking_begin_timer") + " " + _startCountdown + "s";
            //    Audio.Instance.OnReadyToStartMatch();

            //    yield return new WaitForSeconds(0.5f);

            //    PhotonMain.Instance.LoadGamePlay();
            //}
        }

        private IEnumerator _countdownMatch(int pCountdownValue = 5)
        {
            _startCountdown = pCountdownValue;
            while(_startCountdown > 0)
            {
                //Debug.Log("[Matchmaking] _countdownMatch() " + _startCountdown);
                this.transform.Find("Header_Grp/Title_Txt").GetComponent<TMPro.TMP_Text>().text = I2.Loc.LocalizationManager.GetTranslation("matchmaking_begin_timer") + " " + _startCountdown + "s";
                yield return new WaitForSeconds(1f);
                _startCountdown--;
            }

            CountdownCoroutine  =   null;

            this.transform.Find("Header_Grp/Title_Txt").GetComponent<TMPro.TMP_Text>().text = I2.Loc.LocalizationManager.GetTranslation("matchmaking_begin_timer") + " " + _startCountdown + "s";
            Audio.Instance.OnReadyToStartMatch();

            yield return new WaitForSeconds(0.5f);

            PhotonMain.Instance.LoadGamePlay();
        }

        #endregion

        #region Private Methods

        void Start()
        {
            _scoreBarWidthMax = BlueScoreBar.GetComponent<RectTransform>().sizeDelta.x;
            InitValuesTeamBlue();
        }

        void InitValuesTeamBlue()
        {
            _scoreTeamBlue = 0;

            GameObject VSBlueGrp_Gob = PrefabsIdxArray_Gob[Array.IndexOf(PrefabsIdxArray_Str, "VS_Blue_Grp")];

            if (eTeam.TEAM_B == UserData.Instance.PVPTeam)
            {
                VSBlueGrp_Gob.transform.Find("Title_Txt").GetComponent<TMPro.TMP_Text>().text = MatchEngine.Instance.Engine.ManagerMain.ManagerTeamB.Name;
            }
            else
            {
                VSBlueGrp_Gob.transform.Find("Title_Txt").GetComponent<TMPro.TMP_Text>().text = UserData.Instance.Profile.Pseudo;
            }


            MatchmakingPlayer[] lPlayers = this.transform.Find("Blue_Side_Grp").GetComponentsInChildren<MatchmakingPlayer>();

   
            for (int i = 0; i < lPlayers.Length; i++)
            {
                if (eTeam.TEAM_B == UserData.Instance.PVPTeam)
                {
                    _scoreTeamBlue += lPlayers[i].GetComponent<MatchmakingPlayer>().Init(eTeam.TEAM_B);
                }
                else
                {
                    _scoreTeamBlue += lPlayers[i].GetComponent<MatchmakingPlayer>().Init(eTeam.TEAM_A);
                }
            }



        }

        private IEnumerator InitValuesTeamRed()
        {
            _scoreTeamRed = 0;

            GameObject VSRedGrp_Gob = PrefabsIdxArray_Gob[Array.IndexOf(PrefabsIdxArray_Str, "VS_Red_Grp")];

            if (eTeam.TEAM_B == UserData.Instance.PVPTeam)
            {
                VSRedGrp_Gob.transform.Find("Title_Txt").GetComponent<TMPro.TMP_Text>().text = MatchEngine.Instance.Engine.ManagerMain.ManagerTeamA.Name;
            }
            else
            {
                VSRedGrp_Gob.transform.Find("Title_Txt").GetComponent<TMPro.TMP_Text>().text = MatchEngine.Instance.Engine.ManagerMain.ManagerTeamB.Name;
            }

            if (false == PhotonNetwork.IsConnected)
            {
                VSRedGrp_Gob.transform.Find("Title_Txt").GetComponent<TMPro.TMP_Text>().text = _pvpData.OpponentName;
            }


            MatchmakingPlayer[] lPlayers = this.transform.Find("Red_Side_Grp").GetComponentsInChildren<MatchmakingPlayer>();


            for (int i = 0; i < lPlayers.Length; i++)
            {
                if (eTeam.TEAM_B == UserData.Instance.PVPTeam)
                {
                    _scoreTeamRed += lPlayers[i].GetComponent<MatchmakingPlayer>().Init(eTeam.TEAM_A);
                }
                else
                {
                    _scoreTeamRed += lPlayers[i].GetComponent<MatchmakingPlayer>().Init(eTeam.TEAM_B);
                }
            }

            _scoreTeamRed = (_scoreTeamRed > 900) ? 900 : _scoreTeamRed;

            RectTransform RedScoreBarRect = RedScoreBar.GetComponent<RectTransform>();
            Sequence lRedSideScoreBar = DOTween.Sequence();
            RedScoreBarRect.sizeDelta = new Vector2(0, RedScoreBarRect.sizeDelta.y);
            float x = ((_scoreTeamRed * _scoreBarWidthMax) / ConfigData.Instance.MAX_SCORE);
            x = (x > _scoreBarWidthMax) ? _scoreBarWidthMax : x;
            lRedSideScoreBar.Insert(0, RedScoreBarRect.DOSizeDelta(new Vector2(x, RedScoreBarRect.sizeDelta.y), 2f));


            Transform ScoreTxt = this.transform.Find("Red_Side_Grp/Team_Score_Grp/Score_Txt");
            ScoreTxt.GetComponent<TMPro.TMP_Text>().text = "0";
            int lCpt = 0; double lScore = 0;
            double lInterval = _scoreTeamRed / 120;

            do
            {
                lScore += lInterval;
                ScoreTxt.GetComponent<TMPro.TMP_Text>().text = ((int)lScore).ToString();
                yield return new WaitForSeconds(0.01f);

                lCpt++;
            } while (lCpt <= 120);
            ScoreTxt.GetComponent<TMPro.TMP_Text>().text = _scoreTeamRed.ToString();


        }

        /// <summary>
        /// Set Bot Pseudo Array from json
        /// </summary>
        void SetBotPseudo()
        {
            JToken lJToken = JToken.Parse(_botsPseudo);
            JArray lArray = lJToken.Value<JArray>("pseudos");

            _botsPseudoArray = new string[lArray.Count];

            for (int lIndex = 0; lIndex < lArray.Count; ++lIndex)
            {
                _botsPseudoArray[lIndex] = lArray[lIndex].Value<string>();
            }

            _pvpData.OpponentName = _botsPseudoArray[UnityEngine.Random.Range(0, _botsPseudoArray.Length)];
        }

        void Awake()
        {
            Instance = this;

            //--------------------------------------------------------------------> Store GameObject Names For Array Index Of (Search By Name On A Specific Array)
            PrefabsIdxArray_Str = new string[PrefabsIdxArray_Gob.Length];

            for (int i = 0; i < PrefabsIdxArray_Str.Length; i++)
            { PrefabsIdxArray_Str[i] = PrefabsIdxArray_Gob[i].name; }

            GameObject RedSideGrp_Gob = PrefabsIdxArray_Gob[Array.IndexOf(PrefabsIdxArray_Str, "Red_Side_Grp")];
            RedSideGrp_Gob.SetActive(false);
            RedSideGrp_Gob.GetComponent<CanvasGroup>().alpha = 0;

            GameObject RedSideSearchGrp_Gob = PrefabsIdxArray_Gob[Array.IndexOf(PrefabsIdxArray_Str, "Red_Side_Search_Grp")];
            RedSideSearchGrp_Gob.SetActive(false);
            RedSideSearchGrp_Gob.GetComponent<CanvasGroup>().alpha = 0;

            GameObject BlueSideGrp_Gob = PrefabsIdxArray_Gob[Array.IndexOf(PrefabsIdxArray_Str, "Blue_Side_Grp")];
            BlueSideGrp_Gob.SetActive(false);
            BlueSideGrp_Gob.GetComponent<CanvasGroup>().alpha = 0;

            GameObject VSBlueGrp_Gob = PrefabsIdxArray_Gob[Array.IndexOf(PrefabsIdxArray_Str, "VS_Blue_Grp")];
            VSBlueGrp_Gob.transform.Find("Title_Txt").GetComponent<TMPro.TMP_Text>().text = "";

            GameObject VSRedGrp_Gob = PrefabsIdxArray_Gob[Array.IndexOf(PrefabsIdxArray_Str, "VS_Red_Grp")];
            VSRedGrp_Gob.transform.Find("Title_Txt").GetComponent<TMPro.TMP_Text>().text = "";


            SetBotPseudo();
        }

        public void UI_Manager(GameObject button_Gob)
        {
            if (button_Gob.name == "")
            {
                #region //--------------------------------------------------------------------> 
                #endregion
            }
        }
        #endregion
    }
}
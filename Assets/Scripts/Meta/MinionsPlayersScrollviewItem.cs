﻿using System;
using System.Collections;
using DG.Tweening;
using Sportfaction.CasualFantasy.Players;
using Sportfaction.CasualFantasy.Services;
using SportFaction.CasualFootballEngine.PlayerService.Enum;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Sportfaction.CasualFantasy.Meta
{
    /// <summary>
    /// Description: Minions Players Scrollview Item
    /// Author: Antoine de Lachèze-Murel
    /// </summary>
    public class MinionsPlayersScrollviewItem : MonoBehaviour, IPointerClickHandler
    {
        public PlayersItem Player;

        private Color _pinkColor;
        private Color _blueColor;
        private Color _grayColor;

        #region Public Methods

        /// <summary>
        /// Init to set data
        /// <param name="pPlayer">PlayersItem</param>
        /// </summary>
        public void Init(PlayersItem pPlayer)
        {
            Player = pPlayer;
            UpdateUI();
        }

        public void UpdateUI()
        {
            Sprite[] lSpritesMenu = Resources.LoadAll<Sprite>("Sprites/Meta/Menu_2_SS");

            this.transform.Find("Selected_Bkg").gameObject.SetActive(false);

            this.transform.Find("Score_Txt").GetComponent<TMPro.TMP_Text>().text = Player.Profile.Score.ToString();

            this.transform.Find("Score_Att_Txt").GetComponent<TMPro.TMP_Text>().text = Player.Profile.Attack.ToString();

            this.transform.Find("Score_Def_Txt").GetComponent<TMPro.TMP_Text>().text = Player.Profile.Defense.ToString();

            this.transform.Find("Position_Txt").GetComponent<TMPro.TMP_Text>().text = I2.Loc.LocalizationManager.GetTranslation(Player.Profile.GlobalPosition + "short");

            this.transform.Find("Name_Txt").GetComponent<TMPro.TMP_Text>().text = true == String.IsNullOrEmpty(Player.Profile.NameField) ? Player.Profile.ShortName : Player.Profile.NameField;

            this.transform.Find("Championship_Txt").GetComponent<TMPro.TMP_Text>().text = I2.Loc.LocalizationManager.GetTranslation(Player.Profile.ChampionshipName);

            this.transform.Find("Club_Txt").GetComponent<TMPro.TMP_Text>().text = Player.Profile.TeamName;

            //this.transform.Find("Form_Grp/Form_Txt").GetComponent<TMPro.TMP_Text>().text = Data.Instance.FormLevelLetter[Player.Profile.FormLevel];

            switch (MetaData.Instance.FormLevelLetter[Player.Profile.FormLevel])
            {
                case "A":
                    this.transform.Find("Form_Grp/Form_Img").GetComponent<Image>().sprite = UtilityMethods.GetSprite(lSpritesMenu, "Form_Flame_Player_A");
                    break;
                case "B":
                    this.transform.Find("Form_Grp/Form_Img").GetComponent<Image>().sprite = UtilityMethods.GetSprite(lSpritesMenu, "Form_Flame_Player_B");
                    break;
                case "C":
                    this.transform.Find("Form_Grp/Form_Img").GetComponent<Image>().sprite = UtilityMethods.GetSprite(lSpritesMenu, "Form_Flame_Player_C");
                    break;
                case "D":
                    this.transform.Find("Form_Grp/Form_Img").GetComponent<Image>().sprite = UtilityMethods.GetSprite(lSpritesMenu, "Form_Flame_Player_D");
                    break;
            }

            this.transform.Find("OnField_Grp").gameObject.SetActive(false);

            this.transform.Find("Lock_Grp").gameObject.SetActive(false);


            Sprite[] lSpritesMenuFlag = Resources.LoadAll<Sprite>("Sprites/Meta/Flag");
            string country = Player.Profile.Country.Replace(" ", "-").ToLower();

            Sprite lSprite = UtilityMethods.GetSprite(lSpritesMenuFlag, country);

            if (lSprite == null)
            {
                if (MetaData.Instance.MissFlag.ContainsKey(country))
                {
                    country = MetaData.Instance.MissFlag[country];
                    lSprite = UtilityMethods.GetSprite(lSpritesMenuFlag, country);
                }
            }

            if (lSprite == null)
            {
                Debug.Log("Country Flag Missed : " + country);
            }

            Image lImage = this.transform.Find("Flag_Img").GetComponent<Image>();
            lImage.sprite = UtilityMethods.GetSprite(lSpritesMenuFlag, country);
            Color lColor = lImage.color;
            if (lSprite == null)
            {
                lColor.a = 0;
            }
            else
            {
                lColor.a = 1f;
            }
            lImage.color = lColor;


            switch (Player.Profile.FormLevel)
            {
                case 0:
                    this.transform.Find("Form_Grp/Form_Bar_Grp/From_Bar_Lv4_Grp/lv4_Img").gameObject.SetActive(false);
                    this.transform.Find("Form_Grp/Form_Bar_Grp/From_Bar_Lv4_Grp/lv3_Img").gameObject.SetActive(false);
                    this.transform.Find("Form_Grp/Form_Bar_Grp/From_Bar_Lv4_Grp/lv2_Img").gameObject.SetActive(false);
                    this.transform.Find("Form_Grp/Form_Bar_Grp/From_Bar_Lv4_Grp/lv1_Img").gameObject.SetActive(false);
                    break;
                case 1:
                    this.transform.Find("Form_Grp/Form_Bar_Grp/From_Bar_Lv4_Grp/lv4_Img").gameObject.SetActive(false);
                    this.transform.Find("Form_Grp/Form_Bar_Grp/From_Bar_Lv4_Grp/lv3_Img").gameObject.SetActive(false);
                    this.transform.Find("Form_Grp/Form_Bar_Grp/From_Bar_Lv4_Grp/lv2_Img").gameObject.SetActive(false);
                    this.transform.Find("Form_Grp/Form_Bar_Grp/From_Bar_Lv4_Grp/lv1_Img").gameObject.SetActive(true);
                    break;
                case 2:
                    this.transform.Find("Form_Grp/Form_Bar_Grp/From_Bar_Lv4_Grp/lv4_Img").gameObject.SetActive(false);
                    this.transform.Find("Form_Grp/Form_Bar_Grp/From_Bar_Lv4_Grp/lv3_Img").gameObject.SetActive(false);
                    this.transform.Find("Form_Grp/Form_Bar_Grp/From_Bar_Lv4_Grp/lv2_Img").gameObject.SetActive(true);
                    this.transform.Find("Form_Grp/Form_Bar_Grp/From_Bar_Lv4_Grp/lv1_Img").gameObject.SetActive(true);
                    break;
                case 3:
                    this.transform.Find("Form_Grp/Form_Bar_Grp/From_Bar_Lv4_Grp/lv4_Img").gameObject.SetActive(false);
                    this.transform.Find("Form_Grp/Form_Bar_Grp/From_Bar_Lv4_Grp/lv3_Img").gameObject.SetActive(true);
                    this.transform.Find("Form_Grp/Form_Bar_Grp/From_Bar_Lv4_Grp/lv2_Img").gameObject.SetActive(true);
                    this.transform.Find("Form_Grp/Form_Bar_Grp/From_Bar_Lv4_Grp/lv1_Img").gameObject.SetActive(true);
                    break;
                case 4:
                    this.transform.Find("Form_Grp/Form_Bar_Grp/From_Bar_Lv4_Grp/lv4_Img").gameObject.SetActive(true);
                    this.transform.Find("Form_Grp/Form_Bar_Grp/From_Bar_Lv4_Grp/lv3_Img").gameObject.SetActive(true);
                    this.transform.Find("Form_Grp/Form_Bar_Grp/From_Bar_Lv4_Grp/lv2_Img").gameObject.SetActive(true);
                    this.transform.Find("Form_Grp/Form_Bar_Grp/From_Bar_Lv4_Grp/lv1_Img").gameObject.SetActive(true);
                    break;
                default:
                    this.transform.Find("Form_Grp/Form_Bar_Grp/From_Bar_Lv4_Grp/lv4_Img").gameObject.SetActive(false);
                    this.transform.Find("Form_Grp/Form_Bar_Grp/From_Bar_Lv4_Grp/lv3_Img").gameObject.SetActive(false);
                    this.transform.Find("Form_Grp/Form_Bar_Grp/From_Bar_Lv4_Grp/lv2_Img").gameObject.SetActive(false);
                    this.transform.Find("Form_Grp/Form_Bar_Grp/From_Bar_Lv4_Grp/lv1_Img").gameObject.SetActive(false);
                    break;
            }

       

            if(true == IsPlayerItem(Player.Profile.PlayerId))
            {
                //Player is on Field

                //this.transform.GetComponent<Image>().sprite = UtilityMethods.GetSprite(lSpritesMenu, "Card_Green");
                this.transform.Find("OnField_Grp").gameObject.SetActive(true);
                this.transform.Find("Fade_Img").gameObject.SetActive(false);

                this.transform.Find("Score_Att_Txt").GetComponent<TMPro.TMP_Text>().color = _pinkColor;
                this.transform.Find("Score_Def_Txt").GetComponent<TMPro.TMP_Text>().color = _blueColor;
            }
            else if(true == Player.Profile.IsOwned)
            {
                //Player is owned

                //this.transform.GetComponent<Image>().sprite = UtilityMethods.GetSprite(lSpritesMenu, "Card_Blue");

                this.transform.Find("Fade_Img").gameObject.SetActive(false);

                this.transform.Find("Score_Att_Txt").GetComponent<TMPro.TMP_Text>().color = _pinkColor;
                this.transform.Find("Score_Def_Txt").GetComponent<TMPro.TMP_Text>().color = _blueColor;

            }
            else
            {
                //this.transform.GetComponent<Image>().sprite = UtilityMethods.GetSprite(lSpritesMenu, "Card_Grey");
                this.transform.Find("Lock_Grp").gameObject.SetActive(true);
                this.transform.Find("Fade_Img").gameObject.SetActive(true);

                this.transform.Find("Score_Att_Txt").GetComponent<TMPro.TMP_Text>().color = _grayColor;
                this.transform.Find("Score_Def_Txt").GetComponent<TMPro.TMP_Text>().color = _grayColor;

                this.transform.GetComponent<Image>().color = Color.white;
                this.transform.Find("Score_Txt").GetComponent<TMPro.TMP_Text>().color = Color.white;
                this.transform.Find("Position_Txt").GetComponent<TMPro.TMP_Text>().color = Color.white;
                this.transform.Find("Name_Txt").GetComponent<TMPro.TMP_Text>().color = Color.white;
            }

            this.transform.GetComponent<Image>().sprite = UtilityMethods.GetSprite(lSpritesMenu, "Card_Rarity_" + (Player.Profile.RareLevel + 1) + "_Bck" );


            // if selected Card
            if (false == string.IsNullOrEmpty(MetaData.Instance.MinionScrollviewPlayerIdSelected) && Player.Profile.PlayerId == MetaData.Instance.MinionScrollviewPlayerIdSelected)
            {
                this.transform.Find("Selected_Bkg").gameObject.SetActive(true);
            }


            //if (false == string.IsNullOrEmpty(Data.Instance.CardPlayerIdSelected) && Player.Profile.PlayerId == Data.Instance.CardPlayerIdSelected)
            //{
            //    //Vector3 lVector = new Vector3(this.transform.localPosition.x, 20, this.transform.localPosition.z);
            //    //this.transform.localPosition = lVector;
            //    this.transform.GetComponent<Image>().sprite = UtilityMethods.GetSprite(lSpritesMenu, "Player_Card_Selection");
            //    this.transform.Find("Selected_Bkg").gameObject.SetActive(true);
            //}
            //else
            //{
            //    //Vector3 lVector = new Vector3(this.transform.localPosition.x, 0, this.transform.localPosition.z);
            //    // this.transform.DOLocalMoveY(0f, 0.1f);
            //    this.transform.GetComponent<Image>().sprite = UtilityMethods.GetSprite(lSpritesMenu, "Card_Grey");
            //}

            //Player is on Field
           // if (true == IsPlayerItem(Player.Profile.PlayerId))
            //{
            //    this.transform.GetComponent<Image>().sprite = UtilityMethods.GetSprite(lSpritesMenu, "Card_Green");
            //    this.transform.Find("Fade_Img").gameObject.SetActive(false);

            //    this.transform.Find("Score_Att_Txt").GetComponent<TMPro.TMP_Text>().color = _pinkColor; 
            //    this.transform.Find("Score_Def_Txt").GetComponent<TMPro.TMP_Text>().color = _blueColor;
            //}
            //else if (true == string.IsNullOrEmpty(Data.Instance.CardPlayerIdSelected) && Player.Profile.PlayerId != Data.Instance.CardPlayerIdSelected)
            //{
            //    this.transform.GetComponent<Image>().sprite = UtilityMethods.GetSprite(lSpritesMenu, "Card_Grey");
            //    this.transform.Find("Fade_Img").gameObject.SetActive(true);

            //    this.transform.Find("Score_Att_Txt").GetComponent<TMPro.TMP_Text>().color = _grayColor;
            //    this.transform.Find("Score_Def_Txt").GetComponent<TMPro.TMP_Text>().color = _grayColor;

            //    this.transform.GetComponent<Image>().color = Color.white;
            //    this.transform.Find("Score_Txt").GetComponent<TMPro.TMP_Text>().color = Color.white;
            //    this.transform.Find("Position_Txt").GetComponent<TMPro.TMP_Text>().color = Color.white;
            //    this.transform.Find("Name_Txt").GetComponent<TMPro.TMP_Text>().color = Color.white;
            //    //this.transform.localPosition = new Vector3(this.transform.localPosition.x, 0f, this.transform.localPosition.z);
            //}

         

        }

        public void OnPointerClick(PointerEventData pointerEventData)
        {
            if(DefaultUIAnimation.Instance.TimePressed < 0.15 && false == IsPlayerItem(Player.Profile.PlayerId))
            {
                MinionsPlayers.Instance.PrevisionScore = 0;

                MinionsPlayersScrollview.Instance.SelectedMinion = this;
                MinionsPlayersScrollview.Instance.SelectedMinionPlayerId = Player.Profile.PlayerId;
                MinionsPlayersScrollview.Instance.IsSelectedMinionOnScreen = true;

                MinionsPlayers.Instance.LineRendererScroll_Gob.SetActive(true);
                // convert screen coords

                UpdateLineRender();

                Player.Profile.SquadPosition = MetaData.Instance.MinionFieldSquadPositionSelected;

                MetaData.Instance.MinionScrollviewSelected = Player;
                MetaData.Instance.MinionScrollviewPlayerIdSelected = Player.Profile.PlayerId;


                MinionsPlayers.Instance.LoadData(MetaData.Instance.MinionFieldSquadPositionSelected);

                //MinionsPlayers.Instance.BackBtn_Gob.SetActive(true);

                StartCoroutine(AnimateCard());

                this.transform.Find("Selected_Bkg").gameObject.SetActive(true);

              

                MinionsPlayers.Instance.MinionSelectedListGrp_Gob.SetActive(true);
                Sequence lSequence = DOTween.Sequence();
                lSequence.Insert(0.2f, MinionsPlayers.Instance.MinionSelectedListGrp_Gob.GetComponent<CanvasGroup>().DOFade(1, 0.3f));


                MinionsPlayers.Instance.MinionsPlayersAssignDataBtn_Gob.SetActive(true);
                if (false == Player.Profile.IsOwned || MinionsPlayers.Instance.TeamScore > MinionsPlayers.Instance.MaxScore)
                {
                    MinionsPlayers.Instance.MinionsPlayersAssignDataBtn_Gob.transform.Find("Lock_Img").gameObject.SetActive(true);
                    MinionsPlayers.Instance.MinionsPlayersAssignDataBtn_Gob.transform.GetComponent<Image>().color = _grayColor;
                    MinionsPlayers.Instance.MinionsPlayersAssignDataBtn_Gob.transform.Find("Validate_Txt").GetComponent<TMPro.TMP_Text>().color = _grayColor;
                }
                else
                {
                    MinionsPlayers.Instance.MinionsPlayersAssignDataBtn_Gob.transform.Find("Lock_Img").gameObject.SetActive(false);
                    MinionsPlayers.Instance.MinionsPlayersAssignDataBtn_Gob.transform.GetComponent<Image>().color = Color.white;
                    MinionsPlayers.Instance.MinionsPlayersAssignDataBtn_Gob.transform.Find("Validate_Txt").GetComponent<TMPro.TMP_Text>().color = Color.white;
                }

                if (null != MetaData.Instance.MinionFieldSelected)
                {
                    MinionsPlayers.Instance.UpdateTeamScore(true, Player.Profile.Score - MetaData.Instance.MinionFieldSelected.Profile.Score);
                }
                else
                {
                    MinionsPlayers.Instance.UpdateTeamScore(true, Player.Profile.Score);
                }
            }
        }

        public void UpdateLineRender()
        {
            if (true == MinionsPlayersScrollview.Instance.IsSelectedMinionOnScreen)
            {
                MinionsPlayers.Instance.LineRendererScroll_Gob.SetActive(true);

                Vector2 adjustedPosition = MinionsPlayers.Instance.Camera.WorldToScreenPoint(transform.position);

                adjustedPosition.x *= UI.Instance.GetComponent<CanvasScaler>().referenceResolution.x / (float)MinionsPlayers.Instance.Camera.pixelWidth;

                adjustedPosition.y *= UI.Instance.GetComponent<CanvasScaler>().referenceResolution.y / (float)MinionsPlayers.Instance.Camera.pixelHeight;

                adjustedPosition.x = (float)System.Math.Ceiling(adjustedPosition.x);
                adjustedPosition.y = (float)System.Math.Ceiling(adjustedPosition.y);

                adjustedPosition.x -= 0f;
                adjustedPosition.y += 153f;

                LineRenderer lLine = MinionsPlayers.Instance.LineRendererScroll_Gob.GetComponent<LineRenderer>();
                //lLine.SetPosition(2, adjustedPosition);
                //adjustedPosition.y -= 13f;
                //lLine.SetPosition(3, adjustedPosition);
                lLine.transform.position = MinionsPlayers.Instance.MinionSelectedListGrp_Gob.transform.GetChild(2).transform.position;
                lLine.SetPosition(0, MinionsPlayers.Instance.MinionSelectedListGrp_Gob.transform.GetChild(2).transform.position);
                Vector3 lMidPos = MinionsPlayers.Instance.MinionSelectedListGrp_Gob.transform.GetChild(2).transform.position + new Vector3(0f, -0.03f, 0f);
                Vector3 lMidPos2 = new Vector3(transform.position.x, lMidPos.y, transform.position.z);
                lLine.SetPosition(1, lMidPos);
                lLine.SetPosition(2, lMidPos2);
                lLine.SetPosition(3, transform.position);

                this.transform.Find("Selected_Bkg").gameObject.SetActive(true);
            }
            else
            {
                MinionsPlayers.Instance.LineRendererScroll_Gob.SetActive(false);
            }

         
        }

        #endregion

        private IEnumerator AnimateCard()
        {
            Sprite[] lSpritesMenu = Resources.LoadAll<Sprite>("Sprites/Meta/Menu_SS");

            foreach (MinionsPlayersScrollviewItem item in this.transform.parent.GetComponentsInChildren<MinionsPlayersScrollviewItem>())
            {
                //if (item != this && item.transform.localPosition.y != 0f && false == IsPlayerItem(item.Player.Profile.PlayerId))
                //{
                //    item.transform.DOLocalMoveY(0f, 0.3f);
                //    item.transform.GetComponent<Image>().sprite = UtilityMethods.GetSprite(lSpritesMenu, "Player_Card");
                //}

                if (item != this)
                {
                    item.transform.Find("Selected_Bkg").gameObject.SetActive(false);
                }

            }

            //this.transform.GetComponent<Image>().sprite = UtilityMethods.GetSprite(lSpritesMenu, "Player_Card_Selection");

            this.transform.parent.transform.parent.GetComponent<ScrollRect>().enabled = false;
            //this.transform.GetComponent<RectTransform>().DOLocalMoveY(20f, 0.3f);
            yield return new WaitForSeconds(0.3f); // wait animation transition
            this.transform.parent.transform.parent.GetComponent<ScrollRect>().enabled = true;
        }

        private bool IsPlayerItem(string pPlayerId)
        {
            foreach (PlayersItem item in MetaData.Instance.PlayersTeamSelected)
            {
                if (item.Profile.PlayerId == pPlayerId)
                {
                    return true;
                }
            }

            return false;
        }

        private void Start()
        {
            ColorUtility.TryParseHtmlString("#F897D0", out _pinkColor);
            ColorUtility.TryParseHtmlString("#00EEDB", out _blueColor);
            ColorUtility.TryParseHtmlString("#8B8B8B", out _grayColor);
        }
    }
}
﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using Sportfaction.CasualFantasy.ArenaCup;
using Sportfaction.CasualFantasy.Leaderboard;
using Sportfaction.CasualFantasy.Manager.Profile;
using Sportfaction.CasualFantasy.Services.Timers;
using UnityEngine;
using UnityEngine.UI;

namespace Sportfaction.CasualFantasy.Meta
{
    /// <summary>
    /// Description: Matchmaking
    /// Author: MAC
    /// </summary>
    public sealed class Ranking : MonoBehaviour
    {
        public static Ranking Instance;

        #region Public Fields

        public GameObject Trophy_Gob;
        public GameObject ArenaCup_Gob;

        public GameObject TrophyBtn_Gob;
        public GameObject ArenaCupBtn_Gob;

        public GameObject ArrayTrophy_Gob;
        public GameObject ArrayArenaCup_Gob;

        public GameObject ArenaCupRewardsList_Gob;

        public GameObject ArenaCupTimer_Gob;

        public  LineRenderer    ArenaCupLR;
        public  GameObject      RankDot_Gob;

        #endregion

        #region Private Fields

        private Color _blueColor;
        private Color _blueFontColor;
        private Color _grayOddColor;
        private Color _grayEvenColor;
        private Color _grayFontColor;
        private Transform _curManagerRankTranform;
        private Transform _curRankTransform;

        #endregion

        #region Public Methods


        public void OnLocalTimersUpdated_GE()
        {
            if (LocalTimersManager.Instance.IsLocalTimerExists("EndTimerArenaCup"))
            {
                TimeSpan lTimeLeft = LocalTimersManager.Instance.GetTimeLeft("EndTimerArenaCup");
                string lTime = "";
                if(lTimeLeft.Days > 0)
                {
                    lTime += lTimeLeft.ToString("dd") + string.Format(I2.Loc.LocalizationManager.GetTranslation("day_short")) + " ";
                }
                if (lTimeLeft.Hours > 0)
                {
                    lTime += lTimeLeft.ToString("hh") + "H ";
                }
                if (lTimeLeft.Minutes > 0)
                {
                    lTime += lTimeLeft.ToString("mm") + "min ";
                }
                lTime += lTimeLeft.ToString("ss") + "sec";
                ArenaCupTimer_Gob.transform.Find("Timer_Txt").GetComponent<TMPro.TMP_Text>().text = string.Format(I2.Loc.LocalizationManager.GetTranslation("leaderboard_arenacup_ended_title"), lTime);
            }
        }

        //public IEnumerator ShowTrophy()
        //{
        //    Sequence lSequence = DOTween.Sequence();

        //    lSequence.Insert(0, TrophyBtn_Gob.GetComponent<CanvasGroup>().DOFade(0.5f, 0.2f));
        //    //lSequence.Insert(0, ArenaCup_Gob.GetComponent<CanvasGroup>().DOFade(0, 0.2f));

        //    yield return new WaitForSeconds(0.2f);

        //    //ArenaCup_Gob.SetActive(false);
        //    Trophy_Gob.SetActive(true);

        //    lSequence.Insert(0, Trophy_Gob.GetComponent<CanvasGroup>().DOFade(1f, 0.2f));
        //    //lSequence.Insert(0, ArenaCupBtn_Gob.GetComponent<CanvasGroup>().DOFade(1f, 0.2f));
        //}

        //public IEnumerator ShowArenaCup()
        //{
        //    Sequence lSequence = DOTween.Sequence();

        //    lSequence.Insert(0, Trophy_Gob.GetComponent<CanvasGroup>().DOFade(0, 0.2f));
        //    lSequence.Insert(0, ArenaCupBtn_Gob.GetComponent<CanvasGroup>().DOFade(0.5f, 0.2f));

        //    yield return new WaitForSeconds(0.2f);

        //    Trophy_Gob.SetActive(false);
        //    ArenaCup_Gob.SetActive(true);

        //    lSequence.Insert(0, TrophyBtn_Gob.GetComponent<CanvasGroup>().DOFade(1f, 0.2f));
        //    lSequence.Insert(0, ArenaCup_Gob.GetComponent<CanvasGroup>().DOFade(1f, 0.2f));

        //    Data.Instance.ArenaCupMain.GetRewardsArenaCup();
        //}

        public void InitTrophyRanking()
        {
            List<LeaderboardScoreData> lData = MetaData.Instance.LeaderboardMain.LeaderboardData.LeaderboardScoreData;
            InitArrayWithDataList("Trophy", lData, ArrayTrophy_Gob, MetaData.Instance.LeaderboardMain.LeaderboardData.Limit + 2, MetaData.Instance.LeaderboardMain.LeaderboardData.ManagerRank);

            //Trophy_Gob.transform.Find("Loader_Grp").gameObject.SetActive(false);
            DefaultUIAnimation.Instance.FadeOutCanvasGroup(Trophy_Gob.transform.Find("Loader_Grp").gameObject, 0.3f);
    
        }

        //public void InitArenaCupRanking()
        //{
        //    List<ArenaCupDataReward> lArenaCupDataRewards = Data.Instance.ArenaCupMain.ArenaCupData.Rewards;
        //    lArenaCupDataRewards = lArenaCupDataRewards.OrderByDescending(x => x.MaxRank).ToList();

        //    ArenaCupRewardsList_Gob.transform.GetChild(0).GetComponent<RankingArenaCupReward>().Init(lArenaCupDataRewards[0]);
        //    ArenaCupRewardsList_Gob.transform.GetChild(1).GetComponent<RankingArenaCupReward>().Init(lArenaCupDataRewards[1]);
        //    ArenaCupRewardsList_Gob.transform.GetChild(2).GetComponent<RankingArenaCupReward>().Init(lArenaCupDataRewards[2]);

        //    List<LeaderboardScoreData> lData = Data.Instance.LeaderboardMain.LeaderboardArenaCupData.LeaderboardScoreData;
        //    InitArrayWithDataList("ArenaCup", lData, ArrayArenaCup_Gob, Data.Instance.LeaderboardMain.LeaderboardArenaCupData.Limit + 2, Data.Instance.LeaderboardMain.LeaderboardArenaCupData.ManagerRank);

        //    //ArenaCup_Gob.transform.Find("Loader_Grp").gameObject.SetActive(false);
        //    DefaultUIAnimation.Instance.FadeOutCanvasGroup(ArenaCup_Gob.transform.Find("Loader_Grp").gameObject, 0.3f);
        //}

        public void PreviousArenaCup()
        {
            int lManagerRank = MetaData.Instance.LeaderboardMain.LeaderboardPreviousArenaCupData.ManagerRank;

            List<ArenaCupDataReward> lArenaCupDataRewards = MetaData.Instance.ArenaCupMain.ArenaCupData.Rewards;

            lArenaCupDataRewards = lArenaCupDataRewards.OrderByDescending(x => x.MaxRank).ToList();
            // lArenaCupDataRewards = lArenaCupDataRewards.orderBy()
            for (int i = 0; i < lArenaCupDataRewards.Count; i++)
            {
                if (lManagerRank >= lArenaCupDataRewards[i].MinRank && lManagerRank <= lArenaCupDataRewards[i].MaxRank)
                {
                    GameObject MenuHome_Grp = UI.Instance.PrefabsIdxArray_Gob[Array.IndexOf(UI.Instance.PrefabsIdxArray_Str, "Home_Grp")];
                    GameObject RewardHome = MenuHome_Grp.transform.Find("ArenaCupPopin/Popin_Grp/Reward_Grp").gameObject;
                    RewardHome.SetActive(true);

                    RewardHome.GetComponent<RankingArenaCupReward>().Init(lArenaCupDataRewards[i]);

                    RewardHome.transform.Find("Rank_Range_Grp/Range_Txt").localScale = new Vector3(1f, 1f, 1f);
                    RewardHome.transform.Find("Rank_Range_Grp/Name_Txt").localScale = new Vector3(1f, 1f, 1f);
                    RewardHome.transform.Find("Rank_Range_Grp/Name_Txt").GetComponent<TMPro.TMP_Text>().color = Color.white;
                    RewardHome.transform.Find("Rank_Range_Grp/StarActive_Img").gameObject.SetActive(true);
                    RewardHome.transform.Find("Rank_Range_Grp/Range_Txt").gameObject.SetActive(false);
                    //RewardHome.transform.localScale = new Vector2(1f, 1f);
                }
            }
        }

        private void InitArrayWithDataList(string pType, List<LeaderboardScoreData> pData, GameObject pArray_Gop, int pLimit, int pRank)
        {
            if (null != pArray_Gop.transform.Find("Hand_VLG"))
            {
                GameObject lHand_Gob = pArray_Gop.transform.Find("Hand_VLG").gameObject;

                int lIndex = 0;
                for (int i = 1; i < pLimit; i++)
                {
                    if (i <= 3 && i >= 1 || i < pLimit && i >= 5)
                    {
                        GameObject lGob = lHand_Gob.transform.GetChild(i).gameObject;

                        lGob.transform.Find("Hand_Img").gameObject.SetActive(false);
                        if (lIndex <= pData.Count - 1 && pRank == int.Parse(pData[lIndex].Rank))
                        {
                            lGob.transform.Find("Hand_Img").gameObject.SetActive(true);
                        }

                        lIndex++;
                    }
                }
            }

            if (null != pArray_Gop.transform.Find("Rank_VLG"))
            {
                GameObject lRank_Gob = pArray_Gop.transform.Find("Rank_VLG").gameObject;

                int lIndex = 0;
                for (int i = 1; i < pLimit; i++)
                {
                    if (i <= 3 && i >= 1 || i < pLimit && i >= 5)
                    {
                        GameObject lGob = lRank_Gob.transform.GetChild(i).gameObject;

                        if(lIndex <= pData.Count - 1)
                            lGob.transform.Find("Value_Txt").GetComponent<TMPro.TMP_Text>().text = "#" + pData[lIndex].Rank;
                        else
                            lGob.transform.Find("Value_Txt").GetComponent<TMPro.TMP_Text>().text = "#-";

                        if (lIndex <= pData.Count - 1 && pRank == int.Parse(pData[lIndex].Rank))
                        {
                            _curManagerRankTranform =   lGob.transform;
                            lGob.transform.Find("Value_Txt").GetComponent<TMPro.TMP_Text>().color = _blueFontColor;
                            //if(pType == "ArenaCup")
                            //{
                            //    IsRewardArenaCup(int.Parse(pData[lIndex].Rank));
                            //}
                        }

                        lIndex++;
                    }
                }
            }

            if (null != pArray_Gop.transform.Find("Name_VLG"))
            {
                GameObject lName_Gob = pArray_Gop.transform.Find("Name_VLG").gameObject;

                int lIndex = 0;
                for (int i = 1; i < pLimit; i++)
                {
                    if (i <= 3 && i >= 1 || i < pLimit && i >= 5)
                    {
                        GameObject lGob = lName_Gob.transform.GetChild(i).gameObject;
                        if(lIndex <= pData.Count - 1)
                            lGob.transform.Find("Value_Txt").GetComponent<TMPro.TMP_Text>().text = string.IsNullOrEmpty(pData[lIndex].Pseudo) ? pData[lIndex].Username : pData[lIndex].Pseudo ;
                        else
                            lGob.transform.Find("Value_Txt").GetComponent<TMPro.TMP_Text>().text = "-";

                        if (lIndex <= pData.Count - 1 && pRank == int.Parse(pData[lIndex].Rank))
                        {
                            lGob.GetComponent<Image>().color = _blueColor;
                            lGob.transform.Find("Value_Txt").GetComponent<TMPro.TMP_Text>().color = _blueFontColor;
                        }
                        else if (i % 2 == 0)
                        {
                            lGob.GetComponent<Image>().color = _grayEvenColor;
                            lGob.transform.Find("Value_Txt").GetComponent<TMPro.TMP_Text>().color = _grayFontColor;
                        }
                        else
                        {
                            lGob.GetComponent<Image>().color = _grayOddColor;
                            lGob.transform.Find("Value_Txt").GetComponent<TMPro.TMP_Text>().color = _grayFontColor;
                        }

                        lIndex++;

                    }
                }
            }

            if (null != pArray_Gop.transform.Find("Score_VLG"))
            {
                GameObject lTrophy_Gob = pArray_Gop.transform.Find("Score_VLG").gameObject;

                int lIndex = 0;
                for (int i = 1; i < pLimit; i++)
                {
                    if (i <= 3 && i >= 1 || i < pLimit && i >= 5)
                    {
                        GameObject lGob = lTrophy_Gob.transform.GetChild(i).gameObject;
                        if (lIndex <= pData.Count - 1)
                            lGob.transform.Find("Value_Txt").GetComponent<TMPro.TMP_Text>().text = pData[lIndex].Score;
                        else
                            lGob.transform.Find("Value_Txt").GetComponent<TMPro.TMP_Text>().text = "-";

                        if (lIndex <= pData.Count - 1 && pRank == int.Parse(pData[lIndex].Rank))
                        {
                            lGob.GetComponent<Image>().color = _blueColor;
                            lGob.transform.Find("Value_Txt").GetComponent<TMPro.TMP_Text>().color = _blueFontColor;
                        }
                        else if (i % 2 == 0)
                        {
                            lGob.GetComponent<Image>().color = _grayEvenColor;
                            lGob.transform.Find("Value_Txt").GetComponent<TMPro.TMP_Text>().color = Color.white;
                        }
                        else
                        {
                            lGob.GetComponent<Image>().color = _grayOddColor;
                            lGob.transform.Find("Value_Txt").GetComponent<TMPro.TMP_Text>().color = Color.white;
                        }

                        lIndex++;
                    }
                }
            }

            if (null != pArray_Gop.transform.Find("Played_VLG"))
            {
                GameObject lPlayed_Gob = pArray_Gop.transform.Find("Played_VLG").gameObject;

                int lIndex = 0;
                for (int i = 1; i < pLimit; i++)
                {
                    if (i <= 3 && i >= 1 || i < pLimit && i >= 5)
                    {
                        GameObject lGob = lPlayed_Gob.transform.GetChild(i).gameObject;
                        if (lIndex <= pData.Count - 1)
                            lGob.transform.Find("Value_Txt").GetComponent<TMPro.TMP_Text>().text = pData[lIndex].GamesPlayed;
                        else
                            lGob.transform.Find("Value_Txt").GetComponent<TMPro.TMP_Text>().text = "-";

                        if (lIndex <= pData.Count - 1 && pRank == int.Parse(pData[lIndex].Rank))
                        {
                            lGob.GetComponent<Image>().color = _blueColor;
                            lGob.transform.Find("Value_Txt").GetComponent<TMPro.TMP_Text>().color = _blueFontColor;
                        }
                        else if (i % 2 == 0)
                        {
                            lGob.GetComponent<Image>().color = _grayEvenColor;
                            lGob.transform.Find("Value_Txt").GetComponent<TMPro.TMP_Text>().color = _grayFontColor;
                        }
                        else
                        {
                            lGob.GetComponent<Image>().color = _grayOddColor;
                            lGob.transform.Find("Value_Txt").GetComponent<TMPro.TMP_Text>().color = _grayFontColor;
                        }

                        lIndex++;
                    }
                }
            }

            if (null != pArray_Gop.transform.Find("Won_Draw_Lost_VLG"))
            {
                GameObject lWon_Draw_Lost_Gob = pArray_Gop.transform.Find("Won_Draw_Lost_VLG").gameObject;

                int lIndex = 0;
                for (int i = 1; i < pLimit; i++)
                {
                    if (i <= 3 && i >= 1 || i < pLimit && i >= 5)
                    {
                        GameObject lGob = lWon_Draw_Lost_Gob.transform.GetChild(i).gameObject;
                        if (lIndex <= pData.Count - 1)
                            lGob.transform.Find("Value_Txt").GetComponent<TMPro.TMP_Text>().text = pData[lIndex].GamesWon + " / " + pData[lIndex].GamesDraw + " / " + pData[lIndex].GamesLost;
                        else
                            lGob.transform.Find("Value_Txt").GetComponent<TMPro.TMP_Text>().text = "-";

                        if (lIndex <= pData.Count - 1 && pRank == int.Parse(pData[lIndex].Rank))
                        {
                            lGob.GetComponent<Image>().color = _blueColor;
                            lGob.transform.Find("Value_Txt").GetComponent<TMPro.TMP_Text>().color = _blueFontColor;
                        }
                        else if (i % 2 == 0)
                        {
                            lGob.GetComponent<Image>().color = _grayEvenColor;
                            lGob.transform.Find("Value_Txt").GetComponent<TMPro.TMP_Text>().color = _grayFontColor;
                        }
                        else
                        {
                            lGob.GetComponent<Image>().color = _grayOddColor;
                            lGob.transform.Find("Value_Txt").GetComponent<TMPro.TMP_Text>().color = _grayFontColor;
                        }

                        lIndex++;
                    }
                }
            }

            if (null != pArray_Gop.transform.Find("Goals_Scored_VLG"))
            {
                GameObject lGoals_Scored_Gob = pArray_Gop.transform.Find("Goals_Scored_VLG").gameObject;

                int lIndex = 0;
                for (int i = 1; i < pLimit; i++)
                {
                    if (i <= 3 && i >= 1 || i < pLimit && i >= 5)
                    {
                        GameObject lGob = lGoals_Scored_Gob.transform.GetChild(i).gameObject;
                        if (lIndex <= pData.Count - 1)
                            lGob.transform.Find("Value_Txt").GetComponent<TMPro.TMP_Text>().text = pData[lIndex].GoalsScored;
                        else
                            lGob.transform.Find("Value_Txt").GetComponent<TMPro.TMP_Text>().text = "-";

                        if (lIndex <= pData.Count - 1 && pRank == int.Parse(pData[lIndex].Rank))
                        {
                            lGob.GetComponent<Image>().color = _blueColor;
                            lGob.transform.Find("Value_Txt").GetComponent<TMPro.TMP_Text>().color = _blueFontColor;
                        }
                        else if (i % 2 == 0)
                        {
                            lGob.GetComponent<Image>().color = _grayEvenColor;
                            lGob.transform.Find("Value_Txt").GetComponent<TMPro.TMP_Text>().color = _grayFontColor;
                        }
                        else
                        {
                            lGob.GetComponent<Image>().color = _grayOddColor;
                            lGob.transform.Find("Value_Txt").GetComponent<TMPro.TMP_Text>().color = _grayFontColor;
                        }

                        lIndex++;
                    }
                }
            }
            if (null != pArray_Gop.transform.Find("Goals_Conceded_VLG"))
            {
                GameObject lGoals_Conceded_Gob = pArray_Gop.transform.Find("Goals_Conceded_VLG").gameObject;

                int lIndex = 0;
                for (int i = 1; i < pLimit; i++)
                {
                    if (i <= 3 && i >= 1 || i < pLimit && i >= 5)
                    {
                        GameObject lGob = lGoals_Conceded_Gob.transform.GetChild(i).gameObject;
                        if (lIndex <= pData.Count - 1)
                            lGob.transform.Find("Value_Txt").GetComponent<TMPro.TMP_Text>().text = pData[lIndex].GoalsConceded;
                        else
                            lGob.transform.Find("Value_Txt").GetComponent<TMPro.TMP_Text>().text = "-";

                        if (lIndex <= pData.Count - 1 && pRank == int.Parse(pData[lIndex].Rank))
                        {
                            lGob.GetComponent<Image>().color = _blueColor;
                            lGob.transform.Find("Value_Txt").GetComponent<TMPro.TMP_Text>().color = _blueFontColor;
                        }
                        else if (i % 2 == 0)
                        {
                            lGob.GetComponent<Image>().color = _grayEvenColor;
                            lGob.transform.Find("Value_Txt").GetComponent<TMPro.TMP_Text>().color = _grayFontColor;
                        }
                        else
                        {
                            lGob.GetComponent<Image>().color = _grayOddColor;
                            lGob.transform.Find("Value_Txt").GetComponent<TMPro.TMP_Text>().color = _grayFontColor;
                        }

                        lIndex++;
                    }
                }
            }

            if(null != pArray_Gop.transform.Find("Goals_Average_VLG"))
            {
                GameObject lGoals_Average_Gob = pArray_Gop.transform.Find("Goals_Average_VLG").gameObject;

                int lIndex = 0;
                for (int i = 1; i < pLimit; i++)
                {
                    if (i <= 3 && i >= 1 || i < pLimit && i >= 5)
                    {
                        GameObject lGob = lGoals_Average_Gob.transform.GetChild(i).gameObject;
                        if (lIndex <= pData.Count - 1)
                        {
                            if(int.Parse(pData[lIndex].RatioGamesWon) >= 0)
                            {
                                lGob.transform.Find("Value_Txt").GetComponent<TMPro.TMP_Text>().text = "+" + System.Math.Abs(int.Parse(pData[lIndex].RatioGamesWon));
                            }
                            else
                            {
                                lGob.transform.Find("Value_Txt").GetComponent<TMPro.TMP_Text>().text = "-" + System.Math.Abs(int.Parse(pData[lIndex].RatioGamesWon));
                            }
                        }
                        else
                            lGob.transform.Find("Value_Txt").GetComponent<TMPro.TMP_Text>().text = "-";

                        if (lIndex <= pData.Count - 1 && pRank == int.Parse(pData[lIndex].Rank))
                        {
                            lGob.GetComponent<Image>().color = _blueColor;
                            lGob.transform.Find("Value_Txt").GetComponent<TMPro.TMP_Text>().color = _blueFontColor;
                        }
                        else if (i % 2 == 0)
                        {
                            lGob.GetComponent<Image>().color = _grayEvenColor;
                            lGob.transform.Find("Value_Txt").GetComponent<TMPro.TMP_Text>().color = _grayFontColor;
                        }
                        else
                        {
                            lGob.GetComponent<Image>().color = _grayOddColor;
                            lGob.transform.Find("Value_Txt").GetComponent<TMPro.TMP_Text>().color = _grayFontColor;
                        }

                        lIndex++;
                    }
                }
            }

            // _placeLR();
        }

        //private void IsRewardArenaCup(int pRank)
        //{
        //    //int lIndex = 0;
        //    List<ArenaCupDataReward> lArenaCupDataRewards = Data.Instance.ArenaCupMain.ArenaCupData.Rewards;

        //    lArenaCupDataRewards = lArenaCupDataRewards.OrderByDescending(x => x.MaxRank).ToList(); 
        //                                              // lArenaCupDataRewards = lArenaCupDataRewards.orderBy()
        //    for (int i = 0; i < lArenaCupDataRewards.Count; i++)
        //    {

        //        if (pRank >= lArenaCupDataRewards[i].MinRank && pRank <= lArenaCupDataRewards[i].MaxRank)
        //        {
        //            if(i==0)
        //            {
        //                ArenaCupRewardsList_Gob.transform.GetChild(0).GetComponent<RankingArenaCupReward>().Init(lArenaCupDataRewards[i]);
        //                ArenaCupRewardsList_Gob.transform.GetChild(0).GetComponent<RankingArenaCupReward>().Active();
        //                _curRankTransform = ArenaCupRewardsList_Gob.transform.GetChild(0);
        //                ArenaCupRewardsList_Gob.transform.GetChild(1).GetComponent<RankingArenaCupReward>().Init(lArenaCupDataRewards[i + 1]);
        //                ArenaCupRewardsList_Gob.transform.GetChild(2).GetComponent<RankingArenaCupReward>().Init(lArenaCupDataRewards[i + 2]);
        //            }
        //            else if(i == lArenaCupDataRewards.Count - 1)
        //            {
        //                ArenaCupRewardsList_Gob.transform.GetChild(0).GetComponent<RankingArenaCupReward>().Init(lArenaCupDataRewards[i-2]);
        //                ArenaCupRewardsList_Gob.transform.GetChild(1).GetComponent<RankingArenaCupReward>().Init(lArenaCupDataRewards[i-1]);
        //                ArenaCupRewardsList_Gob.transform.GetChild(2).GetComponent<RankingArenaCupReward>().Init(lArenaCupDataRewards[i]);
        //                ArenaCupRewardsList_Gob.transform.GetChild(2).GetComponent<RankingArenaCupReward>().Active();
        //                _curRankTransform = ArenaCupRewardsList_Gob.transform.GetChild(2);
        //            }
        //            else
        //            {
        //                ArenaCupRewardsList_Gob.transform.GetChild(0).GetComponent<RankingArenaCupReward>().Init(lArenaCupDataRewards[i - 1]);
        //                ArenaCupRewardsList_Gob.transform.GetChild(1).GetComponent<RankingArenaCupReward>().Init(lArenaCupDataRewards[i]);
        //                ArenaCupRewardsList_Gob.transform.GetChild(1).GetComponent<RankingArenaCupReward>().Active();
        //                _curRankTransform = ArenaCupRewardsList_Gob.transform.GetChild(1);
        //                ArenaCupRewardsList_Gob.transform.GetChild(2).GetComponent<RankingArenaCupReward>().Init(lArenaCupDataRewards[i +1]);

        //            }

        //        }
        //    }

        //}

        #endregion

        #region Private Methods

        private void    Start()
        {
            //TrophyBtn_Gob.GetComponent<CanvasGroup>().alpha = 0.5f;
            Trophy_Gob.SetActive(true);
            Trophy_Gob.GetComponent<CanvasGroup>().alpha = 1f;

            //ArenaCupBtn_Gob.GetComponent<CanvasGroup>().alpha = 1f;
            //ArenaCup_Gob.GetComponent<CanvasGroup>().alpha = 0;
        }

        private void    Awake()
        {
            //DefaultUIAnimation.Instance.FadeInCanvasGroup(Trophy_Gob.transform.Find("Loader_Grp").gameObject, 0.3f);
            //DefaultUIAnimation.Instance.FadeInCanvasGroup(ArenaCup_Gob.transform.Find("Loader_Grp").gameObject, 0.3f);
            Instance = this;

            ColorUtility.TryParseHtmlString("#2A2A2A", out _grayOddColor);
            ColorUtility.TryParseHtmlString("#1B1B1B", out _grayEvenColor);
            ColorUtility.TryParseHtmlString("#266575", out _blueColor);
            ColorUtility.TryParseHtmlString("#5FFAF3", out _blueFontColor);
            ColorUtility.TryParseHtmlString("#9B9B9B", out _grayFontColor);
        }

        //private void    _placeLR()
        //{
        //    RankDot_Gob.SetActive(null != _curManagerRankTranform && null != _curRankTransform);
        //    ArenaCupLR.gameObject.SetActive(null != _curManagerRankTranform && null != _curRankTransform);

        //    if (null != _curManagerRankTranform && null != _curRankTransform)
        //    {
        //        RankDot_Gob.transform.position  =   _curManagerRankTranform.position;

        //        ArenaCupLR.SetPosition(0, _curManagerRankTranform.position);
        //        ArenaCupLR.SetPosition(1, _curManagerRankTranform.position + new Vector3(-0.02f, 0f, 0f));

        //        Vector3 lMidPos = new Vector3(_curManagerRankTranform.position.x - 0.02f, _curRankTransform.position.y - 0.055f, _curRankTransform.position.z);
        //        ArenaCupLR.SetPosition(2, lMidPos);
        //        ArenaCupLR.SetPosition(3, _curRankTransform.position + new Vector3(0f, -0.055f, 0f));

        //        ArenaCupLR.transform.position = _curManagerRankTranform.position;
        //        ArenaCupLR.gameObject.SetActive(true);
        //    }
        //}

        #endregion
    }
}
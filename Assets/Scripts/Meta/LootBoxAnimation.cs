﻿using System.Collections;
using DG.Tweening;
using UnityEngine;

namespace Sportfaction.CasualFantasy.Meta
{
    /// <summary>
    /// Description: LootBox
    /// Author: Antoine de Lachèze-Murel
    /// </summary>
    public class LootBoxAnimation : MonoBehaviour
    {
        public static LootBoxAnimation Instance;
        public GameObject MessageOpenLoot;

        #region Public Methods

        public void BoxIsOpened()
        {
            LootBox.Instance.Previous_Gob = LootBox.Instance.Box_Gob;
            LootBox.Instance.ShowGoalKeeperLootBox();
        }

        public void PlayFireworks()
        {
            LootBox.Instance.Fireworks.SetActive(true);
            LootBox.Instance.Fireworks.GetComponent<ParticleSystem>().Play();
        }


        void Awake()
        {
            Instance = this;
        }

        #endregion

    }
}

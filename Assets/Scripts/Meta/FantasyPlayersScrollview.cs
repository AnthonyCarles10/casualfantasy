﻿using Sportfaction.CasualFantasy.Players;

using DG.Tweening;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;
using Sportfaction.CasualFantasy.Menu.Squad;
using System;

namespace Sportfaction.CasualFantasy.Meta
{
    /// <summary>
    /// Description: Players Scrollview
    /// Author: Antoine de Lachèze-Murel
    /// </summary>
    public class FantasyPlayersScrollview : MonoBehaviour
    {

        public static FantasyPlayersScrollview Instance;

        #region Properties

        // Property used to know if is showing the chat frame
        public bool IsShowing { get { return _isShowing; } }
        // Property used to know if is hidding the chat frame
        public bool IsHidding { get { return _isHidding; } }

        public int Offset { set { _offset = value; } }

        public int MinPrice { get { return _minPrice; } set { _minPrice = value; } }

        public int MaxPrice { get { return _maxPrice; } set { _maxPrice = value; } }

        public int MinAtt { get { return _minAtt; } set { _minAtt = value; } }

        public int MaxAtt { get { return _maxAtt; } set { _maxAtt = value; } }

        public int MinDef { get { return _minDef; } set { _minDef = value; } }

        public int MaxDef { get { return _maxDef; } set { _maxDef = value; } }

        public int MinG { get { return _minG; } set { _minG = value; } }

        public int MaxG { get { return _maxG; } set { _maxG = value; } }

        public int RareLevel { get { return _rareLevel; } set { _rareLevel = value; } }

        public string Position { get { return _position; } set { _position = value; } }

        public string Team { get { return _team; } set { _team = value; } }

        public string Order { get { return _order; } set { _order = value; } }

        public string OrderBy { get { return _orderBY; } set { _orderBY = value; } }

        public string Keyword { get { return _keyword; } set { _keyword = value; } }

        public bool InitScrollview { get { return _initScrollview; } set { _initScrollview = value; } }

        #endregion

        #region Fields



        private ScrollRect _scrollRect = null;

        [SerializeField, Tooltip("Players Api")]
        private List<PlayersItem> _playersApi = new List<PlayersItem>();

        //[SerializeField, Tooltip("AudioUI")]
        //public AudioUI _audioUI = null;

#pragma warning disable 0414
        [Header("Informations")]
        [SerializeField, ReadOnly, Tooltip("Is showing frame?")]
        private bool _isShowing = false;
        [SerializeField, ReadOnly, Tooltip("Is hidding frame?")]
        private bool _isHidding = false;
        [SerializeField, ReadOnly, Tooltip("Previous scroll position")]
        private Vector3 _prevScrollPos = Vector3.zero;
        [SerializeField, ReadOnly, Tooltip("Current scroll position")]
        private Vector3 _curScrollPos = Vector3.zero;
        [SerializeField, ReadOnly, Tooltip("Transform of the content")]
        private RectTransform _content = null;
        [SerializeField, ReadOnly, Tooltip("Array contains all players")]
        private FantasyPlayersScrollviewItem[] _players = new FantasyPlayersScrollviewItem[0];

        [Header("First Child")]
        [SerializeField, ReadOnly, Tooltip("First child transform")]
        private RectTransform _firstChildTransform = null;
        [SerializeField, ReadOnly, Tooltip("First child player")]
        private FantasyPlayersScrollviewItem _firstChildPlayer = null;

        [Header("Second Child")]
        [SerializeField, ReadOnly, Tooltip("Last child transform")]
        private RectTransform _lastChildTransform = null;
        [SerializeField, ReadOnly, Tooltip("Last child player")]
        private FantasyPlayersScrollviewItem _lastChildPlayer = null;
#pragma warning restore 0414

        private float _scrollRectLeft = 0f;              // Top of the scroll rect
        private float _scrollRectRight = 0f;               // Bottom of the scroll rect
        private float _firstChildBottom = 0f;               // Bottom of the first child
        private float _lastChildTop = 0f;             // Top of the last child
        private int _topIndex = 0;              // Index of the top bubble sentence
        private int _bottomIndex = 0;               // Index of the bottom bubble sentence
                                                    //private	Vector3[]	_worldVectors		=	new Vector3[4];	// Array of vectors

        private int _offset = 0;
        private string _orderBY = "value";
        private string _order = "desc";
        private int _limit = 50;
        private string _position = "";
        private string _team = "";
        private string _keyword = "";
        private int _minPrice = 0;
        private int _maxPrice = 4000000;
        private int _rareLevel = -1;

        private int _minAtt = 0;
        private int _maxAtt = 100;
        private int _minDef = 0;
        private int _maxDef = 100;
        private int _minG = 0;
        private int _maxG = 100;

        private int _totalCount = 0;
        private bool _apiLoading = false;
        private bool _animNormalizedPos = false;
        private bool _initScrollview = true;
        private float _scrollLeftMaxLeft =  -0.25f; // 2.20f
        private float _scrollRightMax = 0f; // Dynmical Value
        private float _childSizeX = 235f;

        private Vector3 _initLocation = new Vector3();

        #endregion

        #region Public Methods

        /// <summary>
        /// Init Scrollview
        /// </summary>
        public void Init()
        {
            //_totalCount = Data.Instance._playersMain.PlayersList.Metas.TotalCount;

            if (true == _initScrollview )
            {
                _playersApi = MetaData.Instance.PlayersMain.PlayersFantasyList.Items;


                InitScrollRect();

                InitPlayersList();

                InitializeFirstAndLastChild();

                _scrollRect.normalizedPosition = _curScrollPos;
            }

            if (false == MetaData.Instance.FilterFantasy && false == _initScrollview)
            {
                _playersApi.AddRange(MetaData.Instance.PlayersMain.PlayersFantasyList.Items);
            }
            else
            {

                _totalCount = MetaData.Instance.PlayersMain.PlayersFantasyList.Metas.TotalCount;

                #if (UNITY_EDITOR || UNITY_STANDALONE) && DEVELOPMENT_BUILD
                    Debug.Log("TotalCount = " + _totalCount);
                #endif

                _scrollRect.StopMovement();

                if (_totalCount <= 7)
                {
                    _scrollRect.enabled = false;
                }
                else
                {
                    _scrollRect.enabled = true;
                }

                _offset = 0;

                _playersApi = MetaData.Instance.PlayersMain.PlayersFantasyList.Items;

                InitScrollRect();

                InitPlayersList();

                InitializeFirstAndLastChild();

                MetaData.Instance.FilterFantasy = false;

            }

            _apiLoading = false;

            _initScrollview = false;

        }

        /// <summary>
        /// Call api function
        /// </summary>
        public void CallAPI(bool pFilter = false)
        {
            _apiLoading = true;

            MetaData.Instance.FilterFantasy = false;
            MetaData.Instance.QueryParamsFantasy["offset"] = _offset.ToString();

            MetaData.Instance.GetPlayersFantasyList();

        }





        #endregion

        #region Private Methods

        /// <summary>
        /// Monobehaviour method used for initialization
        /// </summary>
        private void Start()
        {
            //RectTransform lRect = this.transform.Find("PlayersScroll_Grp").GetComponent<RectTransform>();
            //_initLocation = lRect.localPosition;
            //lRect.localPosition = new Vector3(_initLocation.x + lRect.sizeDelta.x, lRect.localPosition.y, lRect.localPosition.z);

        }

        /// <summary>
        /// Initialize scroll rect
        /// </summary>
        private void InitScrollRect()
        {
            Assert.IsNotNull(_scrollRect, "[PlayerFrame] InitScrollRect(), _scrollRect is null.");

            _content = _scrollRect.content;

            _scrollRect.movementType = ScrollRect.MovementType.Unrestricted;

            _scrollRect.horizontal = true;

            _scrollRect.vertical = false;

            _scrollRect.onValueChanged.AddListener(OnScroll);

            RectTransform _scrollRectTransform = _scrollRect.GetComponent<RectTransform>();

            Assert.IsNotNull(_scrollRectTransform, "[PlayerFrame] InitScrollRect(), _scrollRectTransform is null.");

            _scrollRectLeft = _scrollRectTransform.TransformPoint(_scrollRectTransform.rect.xMin, 0f, 0f).x;

            _scrollRectRight = _scrollRectTransform.TransformPoint(_scrollRectTransform.rect.xMax, 0f, 0f).x;

            _curScrollPos.x = _scrollLeftMaxLeft;

            _scrollRect.normalizedPosition = _curScrollPos;
        }


        public void UpdateData()
        {
            _players = _scrollRect.GetComponentsInChildren<FantasyPlayersScrollviewItem>();

            for (int lIndex = 0; lIndex < _players.Length; ++lIndex)
            {
                _players[lIndex].Init(_playersApi[lIndex]);
            }
        }
        /// <summary>
        /// Initialize bubbles choice
        /// </summary>
        private void InitPlayersList()
        {
            Assert.IsNotNull(_playersApi, "[PlayerFrame] InitPlayersList(), _playersApi is null.");

            Assert.IsNotNull(_scrollRect, "[PlayerFrame] InitPlayersList(), _scrollRect is null.");


            foreach (Transform child in _scrollRect.content.transform)
            {
                child.gameObject.SetActive(true);
            }

            _players = _scrollRect.GetComponentsInChildren<FantasyPlayersScrollviewItem>();

          

            // Debug.Log(_playersApi.Count);
            for (int lIndex = 0; lIndex < _players.Length; ++lIndex)
            {
                Transform lChild = _scrollRect.content.GetChild(lIndex);

                Vector3 lPos = lChild.gameObject.GetComponent<RectTransform>().localPosition;

                if (_playersApi.Count <= lIndex || _playersApi.Count == 0)
                {
                    lChild.gameObject.SetActive(false);
                }
                else
                {
                    _players[lIndex].Init(_playersApi[lIndex]);
                }

                lChild.gameObject.GetComponent<RectTransform>().localPosition = new Vector3(_scrollRect.GetComponent<RectTransform>().rect.xMin + _childSizeX * lIndex, lPos.y, lPos.z);

            }


            if(_playersApi.Count > 0)
            {
                MetaData.Instance.GetPlayerLastGame(_playersApi[0].Profile.PlayerId);
                _players[0].transform.Find("Selected_Bkg").gameObject.SetActive(true);
                MetaData.Instance.FantasyPlayerScrollviewSelected = _playersApi[0];
            }

            int lLength = (_playersApi.Count <= _players.Length) ? _playersApi.Count : _players.Length;

            _topIndex = 0;

            _bottomIndex = lLength - 1;


        }


        /// <summary>
        /// Initialize first and last child
        /// </summary>
        private void InitializeFirstAndLastChild()
        {
            UpdateFirstChild();

            UpdateLastChild();
           
        }

        /// <summary>
        /// Update first child
        /// </summary>
        private void UpdateFirstChild()
        {
            Assert.IsNotNull(_content, "[PlayerFrame] UpdateFirstChild(), _content is null.");

            Assert.IsFalse(0 == _content.childCount, "[PlayerFrame] UpdateFirstChild(), _content don't have children");

            _firstChildTransform = _content.GetChild(0) as RectTransform;

            Assert.IsNotNull(_firstChildTransform, "[PlayerFrame] UpdateFirstChild(), _firstChildTransform is null.");

            _firstChildPlayer = _firstChildTransform.GetComponent<FantasyPlayersScrollviewItem>();

            Assert.IsNotNull(_firstChildPlayer, "[PlayerFrame] UpdateFirstChild(), _firstChildPlayer is null.");


        }

        /// <summary>
        /// Update last child
        /// </summary>
        private void UpdateLastChild()
        {
            Assert.IsNotNull(_content, "[PlayerFrame] UpdateLastChild(), _content is null.");

            Assert.IsFalse(0 == _content.childCount, "[PlayerFrame] UpdateLastChild(), _content don't have children");

            _lastChildTransform = _content.GetChild(_content.childCount - 1) as RectTransform;

            Assert.IsNotNull(_lastChildTransform, "[PlayerFrame] UpdateFirstChild(), _lastChildTransform is null.");

            _lastChildPlayer = _lastChildTransform.GetComponent<FantasyPlayersScrollviewItem>();

            Assert.IsNotNull(_lastChildPlayer, "[PlayerFrame] UpdateFirstChild(), _lastChildPlayer is null.");


        }

        /// <summary>
        /// Callback invoke when user is scrolling
        /// </summary>
        /// <param name="pPos"></param>
        private void OnScroll(Vector2 pPos)
        {

            _curScrollPos = pPos;

            if (_curScrollPos.x > _prevScrollPos.x)
            {
                OnScrollLeft();
            }
            else if (_curScrollPos.x < _prevScrollPos.x)
            {
                OnScrollRight();
            }

            _prevScrollPos = _curScrollPos;

        }

        /// <summary>
        /// Callback invoke when user scroll up
        /// </summary>
        private void OnScrollLeft()
        {
            Assert.IsNotNull(_firstChildTransform, "[PlayerFrame] OnScrollUp(), _firstChildTransform is null.");

            _firstChildBottom = _firstChildTransform.TransformPoint(_firstChildTransform.rect.xMax, 0f, 0f).x;
            // Debug.Log("_topIndex = " + _topIndex + " / _firstChildBottom = " + _firstChildBottom + " / _scrollRectLeft = " + _scrollRectLeft);


            if (_bottomIndex < _totalCount - 1 && _firstChildBottom < _scrollRectLeft)
            {
                Assert.IsNotNull(_lastChildTransform, "[PlayerFrame] OnScrollLeft(), _lastChildTransform is null.");

                _firstChildTransform.localPosition = new Vector3(_lastChildTransform.localPosition.x + _childSizeX, _lastChildTransform.localPosition.y, _lastChildTransform.localPosition.z);

                _firstChildTransform.ForceUpdateRectTransforms();

                _firstChildTransform.SetAsLastSibling();

                _lastChildTransform = _firstChildTransform;

                _lastChildPlayer = _firstChildPlayer;

                IncreaseIndex();

                Assert.IsNotNull(_firstChildPlayer, "[PlayerFrame] OnScrollUp(), _firstChildPlayer is null.");

                _firstChildPlayer.Init(_playersApi[_bottomIndex]);

                UpdateFirstChild();

                //_scrollRightMax = _curScrollPos.y - 0.6f;
                _scrollRightMax = _curScrollPos.x + 0.60f;

                OnScrollLeft();
            }
            else if (_bottomIndex >= _totalCount - 1 && _curScrollPos.x > _scrollRightMax && false == _animNormalizedPos)
            {
                Sequence lSequence = DOTween.Sequence();

                _curScrollPos.x = _scrollRightMax;

                lSequence.Insert(0, _scrollRect.DONormalizedPos(_curScrollPos, 1f).OnComplete(EndAnimNormalizedPos));

                _animNormalizedPos = true;
            }

        }

        /// <summary>
        /// Callback invoke when user scroll down
        /// </summary>
        private void OnScrollRight()
        {
            Assert.IsNotNull(_lastChildTransform, "[PlayerFrame] OnScrollDown(), _lastChildTransform is null.");

            _lastChildTop = _lastChildTransform.TransformPoint(_lastChildTransform.rect.xMin, 0f, 0f).x;

            if (_lastChildTop > _scrollRectRight && _topIndex > 0)
            {
                Assert.IsNotNull(_firstChildTransform, "[PlayerFrame] OnScroll(), _firstChildTransform is null.");

                _lastChildTransform.localPosition = new Vector3(_firstChildTransform.localPosition.x - _childSizeX, _firstChildTransform.localPosition.y, _firstChildTransform.localPosition.z);

                _lastChildTransform.ForceUpdateRectTransforms();

                _lastChildTransform.SetAsFirstSibling();

                _firstChildTransform = _lastChildTransform;

                _firstChildPlayer = _lastChildPlayer;

                DecreaseIndex();

                Assert.IsNotNull(_lastChildPlayer, "[PlayerFrame] OnScrollDown(), _lastChildPlayer is null.");

                _lastChildPlayer.Init(_playersApi[_topIndex]);

                UpdateLastChild();

                OnScrollRight();
            }
            else if (_topIndex <= 0 && _curScrollPos.x < _scrollLeftMaxLeft && false == _animNormalizedPos)
            {
                Sequence lSequence = DOTween.Sequence();

                _curScrollPos.x = _scrollLeftMaxLeft;

                lSequence.Insert(0, _scrollRect.DONormalizedPos(_curScrollPos, 1f).OnComplete(EndAnimNormalizedPos));

                _animNormalizedPos = true;
            }
        }

        private void EndAnimNormalizedPos()
        {
            _animNormalizedPos = false;
        }


        /// <summary>
        /// Increase index
        /// </summary>
        private void IncreaseIndex()
        {
            ++_topIndex;

            ++_bottomIndex;


            if (_bottomIndex >= ((_limit + _offset) / 2) && _limit + _offset < _totalCount && false == _apiLoading)
            {
                // Debug.Log("_bottomIndex (" + _bottomIndex + ") >= (_limit + _offset) / 2 (" + ((_limit + _offset) / 2) + ")  && _limit + _offset (" + (_limit + _offset) + ") < _totalCount (" + _totalCount + ") && false == _apiLoading (" + _apiLoading + ")");
                _offset += _limit;

               
                CallAPI();
            }
        }

        /// <summary>
        /// Decrease index
        /// </summary>
        private void DecreaseIndex()
        {
            --_topIndex;
            --_bottomIndex;
        }

        void Awake()
        {
            Instance = this;

            Transform lScrollView = this.transform.Find("PlayersScroll_Grp/Players_ScrollView");

            _scrollRect = lScrollView.GetComponent<ScrollRect>();
        }

        #endregion
    }
}

﻿using Sportfaction.CasualFantasy.PVP.Gameplay.Matchs;
using SportFaction.CasualFootballEngine.PlayerService.Model;
using SportFaction.CasualFootballEngine.TeamService.Enum;
using UnityEngine;
using UnityEngine.UI;

namespace Sportfaction.CasualFantasy.Meta
{
    /// <summary>
    /// Description: Prefab Script Matchmaking Player
    /// Author: Antoine de Lachèze-Murel
    /// </summary>
    public sealed class MatchmakingPlayer : MonoBehaviour
    {
        public static Matchmaking Instance;


        #region Public Methods

        public int Init(eTeam pTeam)
        {


            if (null != MatchEngine.Instance && null != MatchEngine.Instance.Engine)
            {
                Sprite[] lSpritesMenu = Resources.LoadAll<Sprite>("Sprites/Meta/Menu_2_SS");
                Player lPlayer = MatchEngine.Instance.Engine.TeamMain.GetPlayer(pTeam.ToString(), this.gameObject.name);

                if (lPlayer != null)
                {

                    //this.transform.Find("Player_Grp/PlayerForm/Form_Txt").GetComponent<TMPro.TMP_Text>().text = Data.Instance.FormLevelLetter[lPlayer.FormLevel];

                    this.transform.Find("Player_Grp/PlayerName/Name_Txt").GetComponent<TMPro.TMP_Text>().text = lPlayer.NameField;

                    this.transform.Find("Player_Grp/PlayerScore/Score_Txt").GetComponent<TMPro.TMP_Text>().text = lPlayer.Score.ToString();

                    this.transform.Find("Player_Grp/Shirt").GetComponent<Image>().sprite = UtilityMethods.GetSprite(lSpritesMenu, "Shirt_Rarity_" + (lPlayer.RareLevel + 1) + "_Bck");

                    GameObject Player_Gob = this.transform.Find("Player_Grp").gameObject;


                    // Sprite[] lSpritesGameplay = Resources.LoadAll<Sprite>("Sprites/Gameplay/gameplay");

                    switch (MetaData.Instance.FormLevelLetter[lPlayer.FormLevel])
                    {
                        case "A":
                            Player_Gob.transform.Find("PlayerForm/ScoreForm_Img").GetComponent<Image>().sprite = UtilityMethods.GetSprite(lSpritesMenu, "Form_Flame_Player_A");
                            break;
                        case "B":
                            Player_Gob.transform.Find("PlayerForm/ScoreForm_Img").GetComponent<Image>().sprite = UtilityMethods.GetSprite(lSpritesMenu, "Form_Flame_Player_B");
                            break;
                        case "C":
                            Player_Gob.transform.Find("PlayerForm/ScoreForm_Img").GetComponent<Image>().sprite = UtilityMethods.GetSprite(lSpritesMenu, "Form_Flame_Player_C");
                            break;
                        case "D":
                            Player_Gob.transform.Find("PlayerForm/ScoreForm_Img").GetComponent<Image>().sprite = UtilityMethods.GetSprite(lSpritesMenu, "Form_Flame_Player_D");
                            break;
                    }

                    //if (pTeam == eTeam.TEAM_A)
                    //{
                    //    switch (Data.Instance.FormLevelLetter[lPlayer.FormLevel])
                    //    {
                    //        case "A":
                    //            Player_Gob.transform.Find("PlayerForm/ScoreForm_Img").GetComponent<Image>().sprite = UtilityMethods.GetSprite(lSpritesMenu, "Form_Flame_Player_A");
                    //            break;
                    //        case "B":
                    //            Player_Gob.transform.Find("PlayerForm/ScoreForm_Img").GetComponent<Image>().sprite = UtilityMethods.GetSprite(lSpritesMenu, "Form_Flame_Player_B");
                    //            break;
                    //        case "C":
                    //            Player_Gob.transform.Find("PlayerForm/ScoreForm_Img").GetComponent<Image>().sprite = UtilityMethods.GetSprite(lSpritesMenu, "Form_Flame_Player_C");
                    //            break;
                    //        case "D":
                    //            Player_Gob.transform.Find("PlayerForm/ScoreForm_Img").GetComponent<Image>().sprite = UtilityMethods.GetSprite(lSpritesMenu, "Form_Flame_Player_D");
                    //            break;
                    //    }
                    //}
                    //else
                    //{
                    //    switch (Data.Instance.FormLevelLetter[lPlayer.FormLevel])
                    //    {
                    //        case "A":
                    //            Player_Gob.transform.Find("PlayerForm/ScoreForm_Img").GetComponent<Image>().sprite = UtilityMethods.GetSprite(lSpritesMenu, "Form_Flame_Adv_A");
                    //            break;
                    //        case "B":
                    //            Player_Gob.transform.Find("PlayerForm/ScoreForm_Img").GetComponent<Image>().sprite = UtilityMethods.GetSprite(lSpritesMenu, "Form_Flame_Adv_B");
                    //            break;
                    //        case "C":
                    //            Player_Gob.transform.Find("PlayerForm/ScoreForm_Img").GetComponent<Image>().sprite = UtilityMethods.GetSprite(lSpritesMenu, "Form_Flame_Adv_C");
                    //            break;
                    //        case "D":
                    //            Player_Gob.transform.Find("PlayerForm/ScoreForm_Img").GetComponent<Image>().sprite = UtilityMethods.GetSprite(lSpritesMenu, "Form_Flame_Adv_D");
                    //            break;
                    //    }
                    //}



                    return lPlayer.Score;

                }
            }

            return 0;
        }


        #endregion

    }
}
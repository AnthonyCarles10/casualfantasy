﻿using UnityEngine;

namespace Sportfaction.CasualFantasy.Meta
{
    /// <summary>
    /// Class handling every sound FX during a match
    /// </summary>
	public sealed class Audio : MonoBehaviour
    {
        public static Audio Instance;

        #region Fields

        [SerializeField]
        private AudioSource _sfx = null;

        [SerializeField]
        private AudioClip[] _clips;

        #endregion

        #region Public Methods

        #region Game Event Listeners

      

        #endregion

        #region User Interaction Sounds

        /// <summary>
        /// Plays tap SFX when user taps on action button
        /// </summary>
        public void OnClickEnter()
        {
            _sfx.PlayOneShot(_findClip("MetaButtonTap"));
        }

        public void OnClickBack()
        {
            _sfx.PlayOneShot(_findClip("MetaBackButtonTap"));
        }

        public void OnOpponentFinded()
        {
            _sfx.PlayOneShot(_findClip("OpponentFound"));
        }

        public void OnReadyToStartMatch()
        {
            _sfx.PlayOneShot(_findClip("MatchMakingEnd"));
        }



        #endregion


        #endregion

        #region Private Methods

        private void Start()
        {
            //ClapStart();
        }

        /// <summary>
        /// Finds AudioClip by name
        /// </summary>
        /// <param name="pName">Clip name to find</param>
        private AudioClip _findClip(string pName)
        {
            for (int li = 0; li < _clips.Length; li++)
            {
                if (pName == _clips[li].name)
                    return _clips[li];
            }

            return null;
        }

        void Awake()
        {

            Instance = this;

        }

        #endregion
    }
}
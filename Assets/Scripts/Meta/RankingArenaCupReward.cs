﻿using System;
using System.Collections;
using DG.Tweening;
using Sportfaction.CasualFantasy.ArenaCup;
using Sportfaction.CasualFantasy.Players;
using Sportfaction.CasualFantasy.Services;


using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Sportfaction.CasualFantasy.Meta
{
    /// <summary>
    /// Description: Minions Players Scrollview Item
    /// Author: Antoine de Lachèze-Murel
    /// </summary>
    public class RankingArenaCupReward : MonoBehaviour
    {


        private Color _grayColor;
        private Color _gray2Color;
        private Color _bronzeColor;
        private Color _silverColor;
        private Color _goldColor;
        private Color _diamantColor;
        private ArenaCupDataReward _arenaCupDataReward;


        /// <summary>
        /// Init to set data
        /// <param name="pPlayer">PlayersItem</param>
        /// </summary>
        public void Init(ArenaCupDataReward pArenaCupDataReward)
        {

            ColorUtility.TryParseHtmlString("#7E7E7E", out _grayColor);
            ColorUtility.TryParseHtmlString("#757575", out _gray2Color);
            ColorUtility.TryParseHtmlString("#5E4239", out _bronzeColor);
            ColorUtility.TryParseHtmlString("#4B4B4B", out _silverColor);
            ColorUtility.TryParseHtmlString("#5E5539", out _goldColor);
            ColorUtility.TryParseHtmlString("#39465E", out _diamantColor);

            Sprite[] lSpritesMenu = Resources.LoadAll<Sprite>("Sprites/Meta/Menu_2_SS");

            this.transform.Find("Reward_Bkg").GetComponent<Image>().sprite = UtilityMethods.GetSprite(lSpritesMenu, "Rank_Bck");
            this.transform.localScale = new Vector3(0.8f, 0.8f, 1f);

            //this.transform.Find("Lootbox_Grp/Lootbox_Img").GetComponent<Image>().color = _grayColor;
            //this.transform.Find("Lootbox_Grp/Lootbox_Txt").GetComponent<TMPro.TMP_Text>().color = _grayColor;

            //this.transform.Find("Hard_Grp/Hard_Img").GetComponent<Image>().color = _grayColor;
            //this.transform.Find("Hard_Grp/Hard_Txt").GetComponent<TMPro.TMP_Text>().color = _grayColor;

            this.transform.Find("Rank_Range_Grp/StarActive_Img").gameObject.SetActive(false);
            //this.transform.Find("Rank_Range_Grp/Star_Img").GetComponent<Image>().color = _grayColor;

            this.transform.Find("Rank_Range_Grp/Range_Txt").gameObject.SetActive(true);

            this.transform.Find("Rank_Range_Grp/Range_Txt").GetComponent<TMPro.TMP_Text>().color = _gray2Color;
            this.transform.Find("Rank_Range_Grp/Name_Txt").GetComponent<TMPro.TMP_Text>().color = _gray2Color;
            this.transform.Find("Rank_Range_Grp/Range_Txt").localScale = new Vector3(1.2f, 1.2f, 1f);
            this.transform.Find("Rank_Range_Grp/Name_Txt").localScale = new Vector3(1.2f, 1.2f, 1f);

            //this.transform.Find("Fragments_Grp/Fragments_Img").GetComponent<Image>().color = _grayColor;
            //this.transform.Find("Fragments_Grp/Fragments_Txt").GetComponent<TMPro.TMP_Text>().color = _grayColor;

            //this.transform.Find("Rank_Range_Grp/Range_Txt").GetComponent<TMPro.TMP_Text>().color = _grayColor;
            //this.transform.Find("Rank_Range_Grp/Name_Txt").GetComponent<TMPro.TMP_Text>().color = _grayColor;

            InitValue(pArenaCupDataReward);

        }

        public void InitValue(ArenaCupDataReward pArenaCupDataReward)
        {
            _arenaCupDataReward = pArenaCupDataReward;

            if(_arenaCupDataReward.Name == "starter3")
            {
                this.transform.Find("Rank_Range_Grp/Range_Txt").GetComponent<TMPro.TMP_Text>().text = "#[" + _arenaCupDataReward.MinRank + "- ...]";
            }
            else
            {
                this.transform.Find("Rank_Range_Grp/Range_Txt").GetComponent<TMPro.TMP_Text>().text = "#[" + _arenaCupDataReward.MinRank + "-" + _arenaCupDataReward.MaxRank + "]";
            }

            this.transform.Find("Rank_Range_Grp/Name_Txt").GetComponent<TMPro.TMP_Text>().text = I2.Loc.LocalizationManager.GetTranslation(_arenaCupDataReward.Name);


            Sprite[] lSpritesMenu = Resources.LoadAll<Sprite>("Sprites/Meta/Menu_2_SS");
            Sprite[] lSpritesLeaderboard = Resources.LoadAll<Sprite>("Sprites/Meta/Shop_Leaderboard_SS");

            switch (_arenaCupDataReward.Name)
            {
                case "legend0":
                case "legend1":
                case "legend2":
                case "legend3":
                    this.transform.Find("Rank_Range_Grp/Star_Img").GetComponent<Image>().sprite = UtilityMethods.GetSprite(lSpritesMenu, "Rank_Diamant");
                    this.transform.Find("Reward_Bkg").GetComponent<Image>().color = _diamantColor;
                    break;
                case "master0":
                case "master1":
                case "master2":
                case "master3":
                    this.transform.Find("Rank_Range_Grp/Star_Img").GetComponent<Image>().sprite = UtilityMethods.GetSprite(lSpritesMenu, "Rank_Gold");
                    this.transform.Find("Reward_Bkg").GetComponent<Image>().color = _goldColor;
                    break;
                case "pro0":
                case "pro1":
                case "pro2":
                case "pro3":
                    this.transform.Find("Rank_Range_Grp/Star_Img").GetComponent<Image>().sprite = UtilityMethods.GetSprite(lSpritesMenu, "Rank_Silver");
                    this.transform.Find("Reward_Bkg").GetComponent<Image>().color = _silverColor;
                    break;
                case "starter0":
                case "starter1":
                case "starter2":
                case "starter3":
                    this.transform.Find("Rank_Range_Grp/Star_Img").GetComponent<Image>().sprite = UtilityMethods.GetSprite(lSpritesMenu, "Rank_Bronze");
                    this.transform.Find("Reward_Bkg").GetComponent<Image>().color = _bronzeColor;
                    break;
            }

            if (_arenaCupDataReward.BigBox > 0)
            {
                this.transform.Find("Lootbox_Grp/Lootbox_Txt").GetComponent<TMPro.TMP_Text>().text = "x" + _arenaCupDataReward.BigBox;
                this.transform.Find("Lootbox_Grp/Lootbox_Img").GetComponent<Image>().sprite = UtilityMethods.GetSprite(lSpritesLeaderboard, "UltraStadibox_Icone");
                this.transform.Find("Lootbox_Grp").gameObject.SetActive(true);
            }
            else if (_arenaCupDataReward.MediumBox > 0)
            {
                this.transform.Find("Lootbox_Grp/Lootbox_Txt").GetComponent<TMPro.TMP_Text>().text = "x" + _arenaCupDataReward.MediumBox;
                this.transform.Find("Lootbox_Grp/Lootbox_Img").GetComponent<Image>().sprite = UtilityMethods.GetSprite(lSpritesLeaderboard, "SuperStadibox_Icone");
                this.transform.Find("Lootbox_Grp").gameObject.SetActive(true);
            }
            else
            {
                this.transform.Find("Lootbox_Grp").gameObject.SetActive(false);
            }

            this.transform.Find("Hard_Grp/Hard_Txt").GetComponent<TMPro.TMP_Text>().text = "x" + _arenaCupDataReward.Hard;

            this.transform.Find("Fragments_Grp/Fragments_Txt").GetComponent<TMPro.TMP_Text>().text = "x" + _arenaCupDataReward.FragmentBox;

        }

        public void Active()
        {
            Sprite[] lSpritesMenu = Resources.LoadAll<Sprite>("Sprites/Meta/Menu_2_SS");
            this.transform.localScale = new Vector3(1f, 1f, 1f);

            this.transform.Find("Rank_Range_Grp/Range_Txt").localScale = new Vector3(1f, 1f, 1f);
            this.transform.Find("Rank_Range_Grp/Name_Txt").localScale = new Vector3(1f, 1f, 1f);
            this.transform.Find("Rank_Range_Grp/Name_Txt").GetComponent<TMPro.TMP_Text>().color = Color.white;
            //this.transform.Find("Lootbox_Grp/Lootbox_Img").GetComponent<Image>().color = Color.white;
            //this.transform.Find("Lootbox_Grp/Lootbox_Txt").GetComponent<TMPro.TMP_Text>().color = Color.white;

            //this.transform.Find("Hard_Grp/Hard_Img").GetComponent<Image>().color = _grayColor;
            //this.transform.Find("Hard_Grp/Hard_Txt").GetComponent<TMPro.TMP_Text>().color = Color.white;

            this.transform.Find("Rank_Range_Grp/StarActive_Img").gameObject.SetActive(true);
            //this.transform.Find("Rank_Range_Grp/Star_Img").GetComponent<Image>().color = Color.white;

            this.transform.Find("Rank_Range_Grp/Range_Txt").gameObject.SetActive(false);
            //this.transform.Find("Rank_Range_Grp/Range_Txt").GetComponent<TMPro.TMP_Text>().color = Color.white;
            //this.transform.Find("Rank_Range_Grp/Name_Txt").GetComponent<TMPro.TMP_Text>().color = Color.white;

            //this.transform.Find("Fragments_Grp/Fragments_Img").GetComponent<Image>().color = Color.white;
            //this.transform.Find("Fragments_Grp/Fragments_Txt").GetComponent<TMPro.TMP_Text>().color = Color.white;


            GameObject MenuHome_Grp = UI.Instance.PrefabsIdxArray_Gob[Array.IndexOf(UI.Instance.PrefabsIdxArray_Str, "Home_Grp")];
            GameObject RewardHome = MenuHome_Grp.transform.Find("Home_ArenaCup_Grp/Reward_Grp").gameObject;
            RewardHome.SetActive(true);

            RewardHome.GetComponent<RankingArenaCupReward>().Init(_arenaCupDataReward);

            RewardHome.transform.Find("Rank_Range_Grp/Range_Txt").localScale = new Vector3(1f, 1f, 1f);
            RewardHome.transform.Find("Rank_Range_Grp/Name_Txt").localScale = new Vector3(1f, 1f, 1f);
            RewardHome.transform.Find("Rank_Range_Grp/Name_Txt").GetComponent<TMPro.TMP_Text>().color = Color.white;
            RewardHome.transform.Find("Rank_Range_Grp/StarActive_Img").gameObject.SetActive(true);
            RewardHome.transform.Find("Rank_Range_Grp/Range_Txt").gameObject.SetActive(false);

        }

    }

}
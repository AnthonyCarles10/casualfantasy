﻿using System;
using System.Collections;
using System.Collections.Generic;
using Sportfaction.CasualFantasy.Config;
using Sportfaction.CasualFantasy.Events;
using Sportfaction.CasualFantasy.Leaderboard;
using Sportfaction.CasualFantasy.Loot;
using Sportfaction.CasualFantasy.Manager.Profile;
using Sportfaction.CasualFantasy.PVP.Network;
using Sportfaction.CasualFantasy.Services;
using Sportfaction.CasualFantasy.Services.Timers;
using Sportfaction.CasualFantasy.Services.Userstracking;
using Sportfaction.CasualFantasy.Shop;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Sportfaction.CasualFantasy.Meta
{
    /// <summary>
    /// Description: Matchmaking
    /// Author: Antoine de Lachèze-Murel
    /// </summary>
    public sealed class UI : MonoBehaviour, IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler
    {
        public static UI Instance;

        #region Fields

        [Header("GO_Str_Arr")]
        public GameObject[] PrefabsIdxArray_Gob;
        public string[] PrefabsIdxArray_Str;

        [Header("DATA")]

        [SerializeField, Tooltip("Leaderboard")]
        public LeaderboardData LeaderboardData;


        public Camera MainCamera;

        public GameObject BG;

        private Transform _currentTransform;

        private Transform _pointerClickTransform;

        public GameObject PreviousActive_Go;

        public GameObject CurrentActive_Go;

        public GameObject PushNotifications;

        public GameObject Loader_Grp;

        public bool LootBoxIsClosed;

        private bool _goToTransitionIsActive;

        private bool _endBigBoxOpened;

        private bool _endMediumBoxOpened;

        private bool _endSmallBoxOpened;



        #endregion

        #region Public Methods

        /// <summary>
        /// Go To Home Window
        /// </summary>
        public IEnumerator GoToHome()
        {
     
            GameObject Home_Grp = PrefabsIdxArray_Gob[Array.IndexOf(PrefabsIdxArray_Str, "Home_Grp")];

            if (true == _goToTransitionIsActive)
            {
                yield return new WaitForSeconds(0f); 

            }
            _goToTransitionIsActive = true;

            DesactivateAllRaycast();

            yield return new WaitForSeconds(0.1f); // wait button animation

            Home_Grp.SetActive(true);

            DefaultUIAnimation.Instance.FadeInOutCanvasGroup(Home_Grp.gameObject, CurrentActive_Go.gameObject);

            yield return new WaitForSeconds(0.3f); // wait window animation transition


            CurrentActive_Go.SetActive(false);
            
            CurrentActive_Go = Home_Grp;

            Home_Grp.GetComponent<CanvasGroup>().blocksRaycasts = true;

            Home_Grp.SetActive(true);

            _goToTransitionIsActive = false;

        }

        /// <summary>
        /// Go To Matchmaking Window
        /// </summary>
        public IEnumerator GoToOnBoarding()
        {
            if (true == _goToTransitionIsActive)
            {
                yield return new WaitForSeconds(0f);

            }
            _goToTransitionIsActive = true;

            DesactivateAllRaycast();

            yield return new WaitForSeconds(0.1f); // wait button animation

            GameObject OnBoarding_Grp = PrefabsIdxArray_Gob[Array.IndexOf(PrefabsIdxArray_Str, "OnBoarding_Grp")];

            OnBoarding_Grp.SetActive(true);

            DefaultUIAnimation.Instance.FadeInOutCanvasGroup(OnBoarding_Grp, CurrentActive_Go.gameObject);

            yield return new WaitForSeconds(0.3f); // wait window animation transition

            CurrentActive_Go.SetActive(false);

            CurrentActive_Go = OnBoarding_Grp;

            OnBoarding_Grp.GetComponent<CanvasGroup>().blocksRaycasts = true;

            _goToTransitionIsActive = false;
        }

        /// <summary>
        /// Go To Matchmaking Window
        /// </summary>
        public IEnumerator GoToMatchmaking(bool pSearchOpponent = true)
        {
            GameObject Matchmaking_Grp = PrefabsIdxArray_Gob[Array.IndexOf(PrefabsIdxArray_Str, "Matchmaking_Grp")];

            if (true == _goToTransitionIsActive)
            {
                yield return new WaitForSeconds(0f);

            }
            _goToTransitionIsActive = true;
            
            DesactivateAllRaycast();

            yield return new WaitForSeconds(0.1f); // wait button animation

            if(MetaData.Instance.SquadMain.SquadPlayersData.SquadPlayers.Count < 11)
            {
                StartCoroutine(Home.Instance.OpenWarningNotEnoughPlayersPopin());

                GameObject Home_Grp = PrefabsIdxArray_Gob[Array.IndexOf(PrefabsIdxArray_Str, "Home_Grp")];

                Home_Grp.GetComponent<CanvasGroup>().blocksRaycasts = true;
            }
            else
            {
       
                Matchmaking_Grp.SetActive(true);

                DefaultUIAnimation.Instance.FadeInOutCanvasGroup(Matchmaking_Grp, CurrentActive_Go.gameObject);

                yield return new WaitForSeconds(0.3f); // wait window animation transition

                CurrentActive_Go.SetActive(false);

                CurrentActive_Go = Matchmaking_Grp;

                if (true == pSearchOpponent)
                {
                    StartCoroutine(Matchmaking_Grp.GetComponent<Matchmaking>().SearchOpponent());
                }
                Matchmaking_Grp.GetComponent<CanvasGroup>().blocksRaycasts = true;

            }

            _goToTransitionIsActive = false;

        }


        /// <summary>
        /// Go To Ranking Window
        /// </summary>
        public IEnumerator GoToRanking(string pView = "trophy")
        {
            GameObject Ranking_Grp = PrefabsIdxArray_Gob[Array.IndexOf(PrefabsIdxArray_Str, "Ranking_Grp")];

            if (true == _goToTransitionIsActive)
            {
                yield return new WaitForSeconds(0f);

            }
            _goToTransitionIsActive = true;

            DesactivateAllRaycast();

            yield return new WaitForSeconds(0.1f); // wait button animation

            //UserData.Instance.Profile.NotifArenaCup = false;
            //UserData.Instance.PersistUserData();
            //Home.Instance.UpdateNotif();

            Ranking_Grp.SetActive(true);

            DefaultUIAnimation.Instance.FadeInOutCanvasGroup(Ranking_Grp.gameObject, CurrentActive_Go.gameObject);

            yield return new WaitForSeconds(0.3f); // wait window animation transition

            CurrentActive_Go.SetActive(false);

            CurrentActive_Go = Ranking_Grp;

            Ranking_Grp.GetComponent<CanvasGroup>().blocksRaycasts = true;


            //if(pView == "arenacup")
            //{
            //    Data.Instance.ArenaCupMain.GetRewardsArenaCup();
            //    StartCoroutine(Ranking.Instance.ShowArenaCup());
            //}
            //else
            //{
            //    Data.Instance.GetLeaderboardByTrophy();
            //}

            MetaData.Instance.GetLeaderboardByTrophy();

            _goToTransitionIsActive = false;
        }

        /// <summary>
        /// Go To Ranking Window
        /// </summary>
        public IEnumerator GoToArenaSeason()
        {
            GameObject ArenaSeason_Grp = PrefabsIdxArray_Gob[Array.IndexOf(PrefabsIdxArray_Str, "ArenaSeason_Grp")];

            if (true == _goToTransitionIsActive)
            {
                yield return new WaitForSeconds(0f);

            }
            _goToTransitionIsActive = true;

            DesactivateAllRaycast();

            yield return new WaitForSeconds(0.1f); // wait button animation

            //UserData.Instance.Profile.NotifArenaCup = false;
            //UserData.Instance.PersistUserData();
            //Home.Instance.UpdateNotif();

            ArenaSeason_Grp.SetActive(true);

            DefaultUIAnimation.Instance.FadeInOutCanvasGroup(ArenaSeason_Grp.gameObject, CurrentActive_Go.gameObject);

            yield return new WaitForSeconds(0.3f); // wait window animation transition

            CurrentActive_Go.SetActive(false);

            CurrentActive_Go = ArenaSeason_Grp;

            ArenaSeason_Grp.GetComponent<CanvasGroup>().blocksRaycasts = true;

            _goToTransitionIsActive = false;

        }


        /// <summary>
        /// Go To Minions Players Window
        /// </summary>
        public IEnumerator GoToMinionsPlayers()
        {
            GameObject MinionsPlayers_Grp = PrefabsIdxArray_Gob[Array.IndexOf(PrefabsIdxArray_Str, "MinionsPlayers_Grp")];

            if (true == _goToTransitionIsActive)
            {
                yield return new WaitForSeconds(0f);

            }
            _goToTransitionIsActive = true;

            DesactivateAllRaycast();

            UserData.Instance.Profile.NotifMinionsPlayers = false;
            UserData.Instance.PersistUserData();
            Home.Instance.UpdateNotif();

            yield return new WaitForSeconds(0.1f); // wait button animation

    
            MinionsPlayers_Grp.SetActive(true);

            MetaData.Instance.Filter = true;
            MetaData.Instance.GetPlayersList();

            DefaultUIAnimation.Instance.FadeInOutCanvasGroup(MinionsPlayers_Grp.gameObject, CurrentActive_Go.gameObject);

            yield return new WaitForSeconds(0.3f); // wait window animation transition

            CurrentActive_Go.SetActive(false);

            CurrentActive_Go = MinionsPlayers_Grp;

            MinionsPlayers_Grp.GetComponent<CanvasGroup>().blocksRaycasts = true;

            _goToTransitionIsActive = false;


        }

        /// <summary>
        /// Go To Fantasy Window
        /// </summary>
        public IEnumerator GoToFantasy()
        {
            GameObject Fantasy_Grp = PrefabsIdxArray_Gob[Array.IndexOf(PrefabsIdxArray_Str, "Fantasy_Grp")];

            if (true == _goToTransitionIsActive)
            {
                yield return new WaitForSeconds(0f);

            }
            _goToTransitionIsActive = true;

            DesactivateAllRaycast();

            yield return new WaitForSeconds(0.1f); // wait button animation

            Fantasy_Grp.SetActive(true);

            MetaData.Instance.Filter = true;
            MetaData.Instance.GetPlayersFantasyList();

            DefaultUIAnimation.Instance.FadeInOutCanvasGroup(Fantasy_Grp.gameObject, CurrentActive_Go.gameObject);

            yield return new WaitForSeconds(0.3f); // wait window animation transition

            CurrentActive_Go.SetActive(false);

            CurrentActive_Go = Fantasy_Grp;

            Fantasy_Grp.GetComponent<CanvasGroup>().blocksRaycasts = true;

            _goToTransitionIsActive = false;

        }

        /// <summary>
        /// Go To Shop Window
        /// </summary>
        public IEnumerator GoToShop()
        {
            GameObject Shop_Grp = PrefabsIdxArray_Gob[Array.IndexOf(PrefabsIdxArray_Str, "Shop_Grp")];

            if (true == _goToTransitionIsActive)
            {
                yield return new WaitForSeconds(0f);

            }
            _goToTransitionIsActive = true;

            DesactivateAllRaycast();

            UserData.Instance.Profile.NotifShop = false;
            UserData.Instance.PersistUserData();
            Home.Instance.UpdateNotif();

            yield return new WaitForSeconds(0.1f); // wait button animation

            Shop_Grp.SetActive(true);

            DefaultUIAnimation.Instance.FadeInOutCanvasGroup(Shop_Grp.gameObject, CurrentActive_Go.gameObject);

            yield return new WaitForSeconds(0.3f); // wait window animation transition

            CurrentActive_Go.SetActive(false);

            CurrentActive_Go = Shop_Grp;

            Shop_Grp.GetComponent<CanvasGroup>().blocksRaycasts = true;

            _goToTransitionIsActive = false;
        }

        /// <summary>
        /// Go To Home Window
        /// </summary>
        public IEnumerator GoToLootBox()
        {

            GameObject LootBox_Grp = PrefabsIdxArray_Gob[Array.IndexOf(PrefabsIdxArray_Str, "LootBox_Grp")];

            if (true == _goToTransitionIsActive)
            {
                yield return new WaitForSeconds(0f);

            }
            _goToTransitionIsActive = true;


            DesactivateAllRaycast();

            yield return new WaitForSeconds(0.1f); // wait button animation

            LootBox_Grp.SetActive(true);

            DefaultUIAnimation.Instance.FadeInOutCanvasGroup(LootBox_Grp.gameObject, CurrentActive_Go.gameObject, 0.1f);

            yield return new WaitForSeconds(0.1f); // wait window animation transition

            //CurrentActive_Go.SetActive(false);

            CurrentActive_Go.GetComponent<CanvasGroup>().blocksRaycasts = false;

            CurrentActive_Go = LootBox_Grp;

            if (true == LootBox.Instance.NoBox)
            {
                LootBox.Instance.Box_Gob.SetActive(false);
                LootBox.Instance.ShowGoalKeeperLootBox();
                LootBox.Instance.Fireworks.SetActive(true);
                LootBox.Instance.Fireworks.GetComponent<ParticleSystem>().Play();
            }
            else
            {
                LootBox_Grp.transform.Find("Box_Animation").GetComponent<Animation>().Play("LootBox_Idl");
            }

            LootBox_Grp.GetComponent<CanvasGroup>().blocksRaycasts = true;

            DefaultUIAnimation.Instance.FadeInCanvasGroup(LootBoxAnimation.Instance.MessageOpenLoot);

            _goToTransitionIsActive = false;


            GameObject OnBoarding_Grp = PrefabsIdxArray_Gob[Array.IndexOf(UI.Instance.PrefabsIdxArray_Str, "OnBoarding_Grp")];

            if(OnBoarding_Grp.activeSelf == false)
            {
                GameObject Home_Grp = PrefabsIdxArray_Gob[Array.IndexOf(PrefabsIdxArray_Str, "Home_Grp")];

                Home_Grp.SetActive(true);
            }
            else
            {

                GameObject MinionsPlayers_Grp = PrefabsIdxArray_Gob[Array.IndexOf(PrefabsIdxArray_Str, "MinionsPlayers_Grp")];

                MinionsPlayers_Grp.SetActive(true);

                MinionsPlayers_Grp.GetComponent<CanvasGroup>().alpha = 1;
            }
   

        }



        /// <summary>
        /// Go To Previous Window
        /// </summary>
        public void GoToPreviousWindow() {

            if (false == _goToTransitionIsActive)
            {
                _goToTransitionIsActive = true;

                StartCoroutine(GoToPreviousWindow_IE());
            }

        }
        /// <summary>
        /// IEnumerator > Go To Previous Window
        /// </summary>
        public IEnumerator GoToPreviousWindow_IE()
        {
            DesactivateAllRaycast();

            PreviousActive_Go.SetActive(true);

            switch (PreviousActive_Go.name)
            {
                case "ArenaSeason_Grp":
                    ArenaSeason.Instance.InitArenaSeason();
                    break;
            }


            DefaultUIAnimation.Instance.FadeInOutCanvasGroup(PreviousActive_Go, CurrentActive_Go.gameObject);

            yield return new WaitForSeconds(0.3f); // wait window animation transition

            CurrentActive_Go.GetComponent<CanvasGroup>().blocksRaycasts = false;

            CurrentActive_Go.SetActive(false);

            CurrentActive_Go = PreviousActive_Go;

            CurrentActive_Go.GetComponent<CanvasGroup>().blocksRaycasts = true;

            _goToTransitionIsActive = false;

        }


        /// <summary>
        /// Called by the EventSystem when the pointer
        /// enters the object associated with this EventTrigger.
        /// </summary>
        public void OnPointerEnter(PointerEventData eventData)
        {
            //Debug.Log("UI - OnPointerEnter = " + eventData.pointerEnter.transform.name);
            if (null != eventData.pointerEnter)
            {
                switch (eventData.pointerEnter.transform.name)
                {
                    // HOME
                    case "Close_Ranking_Window_Btn":
                    case "Close_Minions_Players_Window_Btn":
                    case "Close_Matchmaking_Window_Btn":
                    case "Close_Shop_Window_Btn":
                    case "Close_Fantasy_Window_Btn":
                        Audio.Instance.OnClickBack();
                        DefaultUIAnimation.Instance.AnimateButtonEnter(eventData.pointerEnter.transform);
                        _currentTransform = eventData.pointerEnter.transform;

                        break;
                    case "Open_Ranking_Window_Btn":
                    case "Open_ArenaSeason_Window_Btn":
                    case "Open_Match_Window_Btn":
                    case "Open_MinionsPlayers_Window_Btn":
               
                    case "Open_Shop_Window_Btn":
                    case "Open_Fantasy_Window_Btn":
                    case "PlayerStats_Grp":
                    case "Hard_Currency_Grp":
                    case "LootBoxBlue_Grp":
                    case "LootBoxPurple_Grp":
                    //case "ArenaCupPopinGetReward":
                    case "WarningNotEnoughPlayers_Btn":
                    case "NeedToUpdateAppBlack_Bkg":
                    case "NeedToUpdate_btn":
                    case "AppUpdated_btn":
                    // ONBOARDING
                    case "OnBoarding_Open_MinionsPlayers_Window_Btn":
                    case "OnBoarding_Open_Match_Window_Btn":
                    case "OnBoarding_Close_Minions_Players_Window_Btn":
                    case "OnBoarding_Open_Fantasy_Window_Btn":
                    case "OnBoarding_Close_Fantasy_Window_Btn":
                    case "CreateManager1_Next_Btn":
                    case "Home1_Next_Btn":
                    case "Home2_Next_Btn":
                    case "Home3_Next_Btn":
                    case "Home4_Next_Btn":
                    case "Formation1_Next_Btn":
                    case "Formation2_Next_Btn":
                    case "Formation3_Next_Btn":
                    case "Fantasy1_Next_Btn":
                    case "Matchmaking1_Next_Btn":
                    // MINIONS PLAYERS
                    //case "G_Btn":
                    //case "D_Btn":
                    //case "M_Btn":
                    //case "A_Btn":
                    case "Load_Player_Stats_To_Minion_Btn":
                    case "AssignDataToMinionField_Btn":
                    case "Filter_Btn":
                    case "SearchByName_Btn":
                    case "ValidateSearchByName":
                    case "OwnedPlayers_Btn":
                    case "Long_pass_Btn":
                    case "Short_pass_Btn":
                    case "Shot_Btn":
                    case "Dribble_Btn":
                    case "Pressing_Btn":
                    case "Counter_Btn":
                    case "Tackle_Btn":
                    case "Interception_pass_Btn":
                    case "Interception_shot_Btn":
                    case "Score_Btn":
                    case "Validate_Filter_Btn":
                    case "Confirm_Team_Btn":
                    case "AlertScorePopin_Continue_Btn":
                    case "Reset_Filter_Btn":
                    case "MinionsPlayersRemove_Btn":
                    case "AlertNotOwnedPopin_Continue_Btn":
                    case "CancelSearchByName":
                    // RANKING
                    //case "Trophy_Btn":
                    //case "ArenaCup_Btn":
                    //case "Home_ArenaCup_Grp":
                    //FANTASYPLAYERS
                    case "Fantasy_G_Btn":
                    case "Fantasy_D_Btn":
                    case "Fantasy_M_Btn":
                    case "Fantasy_A_Btn":
                    case "FantasyFilter_Btn":
                    case "FantasySearchByName_Btn":
                    case "FantasyValidate_Filter_Btn":
                    case "FantasyValidateSearchByName":
                    case "FantasyCancelSearchByName":
                    case "FantasyOwnedPlayers_Btn":
                    //// ARENA SEASON
                    //case "GetRewardArenaSeason_Btn":

                    // SHOP
                    case "com.sportfaction.soccerarena.starter_1":
                    case "com.sportfaction.soccerarena.hard_currency_1":
                    case "com.sportfaction.soccerarena.hard_currency_2":
                    case "com.sportfaction.soccerarena.hard_currency_3":
                    case "com.sportfaction.soccerarena.hard_currency_4":
                    case "com.sportfaction.soccerarena.player_box_medium":
                    case "com.sportfaction.soccerarena.player_box_big":


                        Audio.Instance.OnClickEnter();
                        DefaultUIAnimation.Instance.AnimateButtonEnter(eventData.pointerEnter.transform);
                        _currentTransform = eventData.pointerEnter.transform;
                 
                        break;
                   // LOOTBOX
                    case "Box_Animation":
                    case "G_LootBox_Grp":
                    case "D_LootBox_Grp":
                    case "M_LootBox_Grp":
                    case "A_LootBox_Grp":
                    case "Hard_LootBox_Grp":
                      
                        _currentTransform = eventData.pointerEnter.transform;
                        break;

                }
            }

        }

        /// <summary>
        /// Called by the EventSystem when the pointer
        /// enters the object associated with this EventTrigger.
        /// </summary>
        public void OnPointerExit(PointerEventData eventData)
        {
            if (null != _currentTransform)
            {
                DefaultUIAnimation.Instance.AnimateButtonUp(_currentTransform);
            }
        }

        /// <summary>
        /// Register button presses using the IPointerClickHandler
        /// </summary>
        public void OnPointerClick(PointerEventData eventData)
        {
            if ((null != eventData.pointerEnter && null != _pointerClickTransform && _pointerClickTransform.name != eventData.pointerEnter.transform.name) || (_pointerClickTransform == null && null != eventData.pointerEnter))
            {
                _pointerClickTransform = eventData.pointerEnter.transform;
                DefaultUIAnimation.Instance.AnimateButtonUp(_currentTransform);
                StartCoroutine(GoTo());
            }
        }

        public void DesactivateAllRaycast()
        {
            GameObject MenuHome_Grp = PrefabsIdxArray_Gob[Array.IndexOf(UI.Instance.PrefabsIdxArray_Str, "Home_Grp")];
            MenuHome_Grp.GetComponent<CanvasGroup>().blocksRaycasts = false;

            if (false == UserData.Instance.Profile.OnBoarding && false == UserData.Instance.Profile.OnBoardingHome)
            {
                GameObject OnBoarding_Grp = PrefabsIdxArray_Gob[Array.IndexOf(UI.Instance.PrefabsIdxArray_Str, "OnBoarding_Grp")];
                OnBoarding_Grp.GetComponent<CanvasGroup>().blocksRaycasts = false;
            }
            GameObject Matchmaking_Grp = PrefabsIdxArray_Gob[Array.IndexOf(UI.Instance.PrefabsIdxArray_Str, "Matchmaking_Grp")];
            Matchmaking_Grp.GetComponent<CanvasGroup>().blocksRaycasts = false;
            GameObject Ranking_Grp = PrefabsIdxArray_Gob[Array.IndexOf(UI.Instance.PrefabsIdxArray_Str, "Ranking_Grp")];
            Ranking_Grp.GetComponent<CanvasGroup>().blocksRaycasts = false;
            GameObject MinionsPlayers_Grp = PrefabsIdxArray_Gob[Array.IndexOf(UI.Instance.PrefabsIdxArray_Str, "MinionsPlayers_Grp")];
            MinionsPlayers_Grp.GetComponent<CanvasGroup>().blocksRaycasts = false;
            GameObject LootBox_Grp = PrefabsIdxArray_Gob[Array.IndexOf(UI.Instance.PrefabsIdxArray_Str, "LootBox_Grp")];
            LootBox_Grp.GetComponent<CanvasGroup>().blocksRaycasts = false;
            GameObject Fantasy_Grp = PrefabsIdxArray_Gob[Array.IndexOf(UI.Instance.PrefabsIdxArray_Str, "Fantasy_Grp")];
            Fantasy_Grp.GetComponent<CanvasGroup>().blocksRaycasts = false;
        }

        public IEnumerator GoTo()
        {
            if (null != _pointerClickTransform)
            {
                switch (_pointerClickTransform.name)
                {
                    #region GoTo Close
                    case "Close_Minions_Players_Window_Btn":
                    case "Close_Ranking_Window_Btn":
                    case "Close_Shop_Window_Btn":
                    case "Close_Fantasy_Window_Btn":
                        StartCoroutine(GoToHome());
                        break;
                    case "Close_Matchmaking_Window_Btn":
                        Matchmaking.Instance.Cancel();
                        PhotonMain.Instance.Cancel();
                        StartCoroutine(GoToHome());
                        break;
                    #endregion

                    #region GoTo Home
                    case "Open_Match_Window_Btn":
                        UserEventsManager.Instance.RecordEvent(new UserActionEvent("meta_match"));
                        StartCoroutine(GoToMatchmaking());
                        break;

                    case "PlayerStats_Grp":
                    case "Open_Ranking_Window_Btn":
                        UserEventsManager.Instance.RecordEvent(new UserActionEvent("meta_ranking"));
                        StartCoroutine(GoToRanking());
                        break;

                    case "LootBoxBlue_Grp":
                        UserEventsManager.Instance.RecordEvent(new UserActionEvent("lootbox_small"));
                        StartCoroutine(Home.Instance.OpenBlueBox());
                        break;

                    case "LootBoxPurple_Grp":
                        UserEventsManager.Instance.RecordEvent(new UserActionEvent("lootbox_medium"));
                        StartCoroutine(Home.Instance.OpenPurpleBox());
                        break;

                    case "Open_MinionsPlayers_Window_Btn":
                        UserEventsManager.Instance.RecordEvent(new UserActionEvent("meta_formation"));
                        StartCoroutine(GoToMinionsPlayers());
                        break;

                    case "Open_Shop_Window_Btn":
                    case "Hard_Currency_Grp":
                        UserEventsManager.Instance.RecordEvent(new UserActionEvent("meta_shop"));
                        StartCoroutine(GoToShop());
                        break;

                    case "Open_Fantasy_Window_Btn":
                        UserEventsManager.Instance.RecordEvent(new UserActionEvent("meta_fantasy"));
                        StartCoroutine(GoToFantasy());
                        break;



                    case "Open_ArenaSeason_Window_Btn":
                        UserEventsManager.Instance.RecordEvent(new UserActionEvent("meta_arenaseason"));
                        StartCoroutine(GoToArenaSeason());
                        break;

                    //case "ArenaCupPopinGetReward":
                    //    UserEventsManager.Instance.RecordEvent(new UserActionEvent("arenacup_getrewards"));
                    //    StartCoroutine(Home.Instance.GetRewardsArenaCup());
                    //    break;

                    case "WarningNotEnoughPlayers_Btn":
                        UserEventsManager.Instance.RecordEvent(new UserActionEvent("meta_formation"));
                        StartCoroutine(Home.Instance.CloseWarningNotEnoughPlayersPopin());
                        StartCoroutine(GoToMinionsPlayers());
                        break;

                    case "NeedToUpdateAppBlack_Bkg":
                        StartCoroutine(Home.Instance.CloseNeedToUpdateAppPopin());
                        break;

                    case "NeedToUpdate_btn":
                        Application.OpenURL(SystemInfo.operatingSystem.Contains("iOS") ? ConfigData.Instance.APPSTORE_URL : ConfigData.Instance.GOOGLEPLAY_URL);
                        Application.Quit();
                        StartCoroutine(Home.Instance.CloseNeedToUpdateAppPopin());
                        break;

                    case "AppUpdated_btn":
                        StartCoroutine(Home.Instance.CloseAppUpdatedPopin());
                        break;

                    #endregion

                    #region GoTo Onbaording

                    case "CreateManager1_Next_Btn":
                        UserEventsManager.Instance.RecordEvent(new UserActionEvent("onboarding_createmanager1"));
                        StartCoroutine(OnBoarding.Instance.SetPseudo());
                        break;
                    case "Home1_Next_Btn":
                        UserEventsManager.Instance.RecordEvent(new UserActionEvent("onboarding_home1"));
                        StartCoroutine(OnBoarding.Instance.ActiveButton("Open_MinionsPlayers_Window_Btn"));
                        break;
                    case "Formation1_Next_Btn":
                        UserEventsManager.Instance.RecordEvent(new UserActionEvent("onboarding_formation1"));
                        StartCoroutine(OnBoarding.Instance.OpenInitBox());
                        break;
                    case "Formation2_Next_Btn":
                        UserEventsManager.Instance.RecordEvent(new UserActionEvent("onboarding_formation2"));
                        StartCoroutine(OnBoarding.Instance.ActiveButton("Close_Minions_Players_Window_Btn"));
                        break;
                    case "Home2_Next_Btn":
                        UserEventsManager.Instance.RecordEvent(new UserActionEvent("onboarding_home2"));
                        StartCoroutine(OnBoarding.Instance.ActiveButton("Open_Match_Window_Btn"));
                        break;
                    case "OnBoarding_Open_MinionsPlayers_Window_Btn":
                        MetaData.Instance.PlayersTeamSelected = new List<Players.PlayersItem>();
                        GameObject MinionsPlayers_Grp = UI.Instance.PrefabsIdxArray_Gob[Array.IndexOf(UI.Instance.PrefabsIdxArray_Str, "MinionsPlayers_Grp")];
                        if (false == MinionsPlayers_Grp.activeSelf)
                        {
                            MinionsPlayers_Grp.SetActive(true);
                            MinionsPlayers_Grp.GetComponent<MinionsPlayers>().Init();
                            MinionsPlayers_Grp.SetActive(false);
                        }
                        else
                        {
                            MinionsPlayers_Grp.GetComponent<MinionsPlayers>().Init();
                        }
                        UserEventsManager.Instance.RecordEvent(new UserActionEvent("onboarding_click_openformation"));
                        StartCoroutine(OnBoarding.Instance.OnClickButton("Open_MinionsPlayers_Window_Btn", "Formation1"));
                        StartCoroutine(GoToMinionsPlayers());
                        break;
                    case "OnBoarding_Close_Minions_Players_Window_Btn":
                        UserEventsManager.Instance.RecordEvent(new UserActionEvent("onboarding_click_closeformation"));
                        StartCoroutine(OnBoarding.Instance.OnClickButton("Close_Minions_Players_Window_Btn", "" , "Open_Fantasy_Window_Btn"));
                        StartCoroutine(GoToHome());
                        break;
                    case "OnBoarding_Open_Fantasy_Window_Btn":
                        UserEventsManager.Instance.RecordEvent(new UserActionEvent("onboarding_click_openfantasy"));
                        StartCoroutine(OnBoarding.Instance.OnClickButton("Open_Fantasy_Window_Btn", "Fantasy1"));
                        StartCoroutine(GoToFantasy());
                        break;
                    case "Fantasy1_Next_Btn":
                        UserEventsManager.Instance.RecordEvent(new UserActionEvent("onboarding_fantasy1"));
                        StartCoroutine(OnBoarding.Instance.ActiveButton("Close_Fantasy_Window_Btn"));
                        break;
                    case "OnBoarding_Close_Fantasy_Window_Btn":
                        UserEventsManager.Instance.RecordEvent(new UserActionEvent("onboarding_click_closefantasy"));
                        StartCoroutine(OnBoarding.Instance.OnClickButton("Close_Fantasy_Window_Btn", "Home2"));
                        StartCoroutine(GoToHome());
                        break;
                    case "OnBoarding_Open_Match_Window_Btn":
                        UserEventsManager.Instance.RecordEvent(new UserActionEvent("onboarding_click_openmatch"));
                        StartCoroutine(OnBoarding.Instance.OnClickButton("Open_Match_Window_Btn", "Matchmaking1"));
                        StartCoroutine(GoToMatchmaking(false));
                        UserData.Instance.Profile.OnBoardingMeta = false;
                        UserData.Instance.PersistUserData();
                        break;
                    case "Matchmaking1_Next_Btn":
                        UserEventsManager.Instance.RecordEvent(new UserActionEvent("onboarding_matchmaking1"));
                        GameObject Matchmaking_Grp = PrefabsIdxArray_Gob[Array.IndexOf(PrefabsIdxArray_Str, "Matchmaking_Grp")];
                        StartCoroutine(OnBoarding.Instance.ClosePopin());
                        StartCoroutine(Matchmaking_Grp.GetComponent<Matchmaking>().SearchOpponent());
                        break;
                    case "Home3_Next_Btn":
                        UserEventsManager.Instance.RecordEvent(new UserActionEvent("onboarding_home3"));
                        StartCoroutine(OnBoarding.Instance.OpenPopin("Home4"));
                        break;
                    case "Home4_Next_Btn":
                 
                        
                        if (false == LocalTimersManager.Instance.IsLocalTimerExists("medium"))
                        {
                            UserEventsManager.Instance.RecordEvent(new UserActionEvent("onboarding_home4"));
                            GameObject Home_Grp = PrefabsIdxArray_Gob[Array.IndexOf(PrefabsIdxArray_Str, "Home_Grp")];
                            GameObject OnBoarding_Grp = PrefabsIdxArray_Gob[Array.IndexOf(PrefabsIdxArray_Str, "OnBoarding_Grp")];
                            OnBoarding_Grp.SetActive(false);
                            CurrentActive_Go = Home_Grp;
                            UserEventsManager.Instance.RecordEvent(new UserActionEvent("onboarding_lootbox_medium"));
                            StartCoroutine(Home.Instance.OpenPurpleBox());
                        }
                        else
                        {
                            GameObject OnBoarding_Grp = PrefabsIdxArray_Gob[Array.IndexOf(PrefabsIdxArray_Str, "OnBoarding_Grp")];
                            CurrentActive_Go = OnBoarding_Grp;
                            StartCoroutine(GoToHome());
                        }
                        break;

                    #endregion

                    #region GoTo MinionsPlayers
                    case "AssignDataToMinionField_Btn":

                        //if (Data.Instance.PlayersTeamSelected.Contains(Data.Instance.PlayerSelected))
                        if (MetaData.Instance.MinionScrollviewSelected != null)
                        {
                            StartCoroutine(MinionsPlayers.Instance.AssignDataToMinionField());
                        }

                        break;
                    case "MinionsPlayersGoBack_Btn":
                        StartCoroutine(MinionsPlayers.Instance.MinionsPlayersGoBack());
                        break;
                    case "Filter_Btn":
                        StartCoroutine(MinionsPlayers.Instance.OpenFilterPopin());
                        break;
                    case "Long_pass_Btn":
                    case "Short_pass_Btn":
                    case "Shot_Btn":
                    case "Dribble_Btn":
                    case "Pressing_Btn":
                    case "Counter_Btn":
                    case "Tackle_Btn":
                    case "Interception_pass_Btn":
                    case "Interception_shot_Btn":
                    case "Score_Btn":
                        StartCoroutine(MinionsPlayers.Instance.FilterBySkill(_pointerClickTransform.name));
                        break;
         
                    case "SearchByName_Btn":
                        StartCoroutine(MinionsPlayers.Instance.OpenSearchByNamePopin());
                        break;
                    case "ValidateSearchByName":
                        StartCoroutine(MinionsPlayers.Instance.FilterByName());
                        break;
                    case "CancelSearchByName":
                        StartCoroutine(MinionsPlayers.Instance.CloseSearchByNamePopin());
                        break;
                    case "OwnedPlayers_Btn":
                        StartCoroutine(MinionsPlayers.Instance.FilterByOwnedPlayers());
                        break;
                    case "Reset_Filter_Btn":
                        StartCoroutine(MinionsPlayers.Instance.ResetFilter());
                        break;
                    case "Validate_Filter_Btn":
                        StartCoroutine(MinionsPlayers.Instance.ValidateFilter());
                        break;
                    //case "Confirm_Team_Btn":
                    //    Data.Instance.SaveMinionsPlayers();
                    //    MinionsPlayers.Instance.ConfirmTeamBtn_Gob.SetActive(false);
                    //    break;
                    case "AlertScorePopin_Continue_Btn":
                        StartCoroutine(MinionsPlayers.Instance.CloseAlertScorePopin());
                        
                        break;
                    case "AlertNotOwnedPopin_Continue_Btn":
                        StartCoroutine(MinionsPlayers.Instance.CloseAlertOwnedPopin());
                        break;
                    case "MinionsPlayersRemove_Btn":
                        StartCoroutine(MinionsPlayers.Instance.RemoveMinionPlayer());
                        break;
                    case "G_Btn":
                    case "D_Btn":
                    case "M_Btn":
                    case "A_Btn":
                        StartCoroutine(MinionsPlayers.Instance.FilterByPosition(_pointerClickTransform.name));
                        break;
                    #endregion

                    #region GoTo Lootbox
                    case "Box_Animation":
                        StartCoroutine(LootBox.Instance.OpenLootBox());
                        break;
                    case "G_LootBox_Grp":
                        LootBox.Instance.ShowFullBackLootBox();
                        break;
                    case "D_LootBox_Grp":
                        LootBox.Instance.ShowMidfieldLootBox();
                        break;
                    case "M_LootBox_Grp":
                        LootBox.Instance.ShowForwardLootBox();
                        break;
                    case "A_LootBox_Grp":
                        LootBox.Instance.ShowHardLootBox();
                        break;
                    case "Hard_LootBox_Grp":
                        if(false == LootBox.Instance.ActiveTransition)
                        {
                            LootBox.Instance.ActiveTransition = true;
                            StartCoroutine(LootBox.Instance.CloseLootBox());
                        }
                        break;
                    #endregion

                    #region GoTo Ranking
                    //case "Trophy_Btn":
                    //    StartCoroutine(Ranking.Instance.ShowTrophy());
                    //    break;
                    //case "ArenaCup_Btn":
                    //    UserEventsManager.Instance.RecordEvent(new UserActionEvent("meta_arenacup"));
                    //    StartCoroutine(Ranking.Instance.ShowArenaCup());
                    //    break;
                    //case "Home_ArenaCup_Grp":
                    //    UserEventsManager.Instance.RecordEvent(new UserActionEvent("meta_arenacup"));
                    //    StartCoroutine(GoToRanking("arenacup"));
                    //    break;
                    #endregion

                    #region GoTo FantasyPlayers
                    case "Fantasy_G_Btn":
                    case "Fantasy_D_Btn":
                    case "Fantasy_M_Btn":
                    case "Fantasy_A_Btn":
                        StartCoroutine(FantasyPlayers.Instance.FilterByPosition(_pointerClickTransform.name));
                        break;

                    case "FantasyFilter_Btn":
                        StartCoroutine(FantasyPlayers.Instance.OpenFilterPopin());
                        break;
                    case "FantasySearchByName_Btn":
                        StartCoroutine(FantasyPlayers.Instance.OpenSearchByNamePopin());
                        break;
                    case "FantasyValidateSearchByName":
                        StartCoroutine(FantasyPlayers.Instance.FilterByName());
                        break;
                    case "FantasyCancelSearchByName":
                        StartCoroutine(FantasyPlayers.Instance.CloseSearchByNamePopin());
                        break;
                    case "FantasyValidate_Filter_Btn":
                        StartCoroutine(FantasyPlayers.Instance.ValidateFilter());
                        StartCoroutine(FantasyPlayers.Instance.CloseFilterPopin());
                        break;
                    case "FantasyOwnedPlayers_Btn":
                        StartCoroutine(FantasyPlayers.Instance.FilterByOwnedPlayers());
                        break;
                    #endregion

                    #region GoTo Shop
                    case "com.sportfaction.soccerarena.starter_1":
                        Shop.Instance.BuyProductWithRealMoney("com.sportfaction.soccerarena.starter_1");
                        break;
                    case "com.sportfaction.soccerarena.hard_currency_1":
                        Shop.Instance.BuyProductWithRealMoney("com.sportfaction.soccerarena.hard_currency_1");
                        break;
                    case "com.sportfaction.soccerarena.hard_currency_2":
                        Shop.Instance.BuyProductWithRealMoney("com.sportfaction.soccerarena.hard_currency_2");
                        break;
                    case "com.sportfaction.soccerarena.hard_currency_3":
                        Shop.Instance.BuyProductWithRealMoney("com.sportfaction.soccerarena.hard_currency_3");
                        break;
                    case "com.sportfaction.soccerarena.hard_currency_4":
                        Shop.Instance.BuyProductWithRealMoney("com.sportfaction.soccerarena.hard_currency_4");
                        break;
                    case "com.sportfaction.soccerarena.player_box_medium":
                        Shop.Instance.BuyProductWithHardCurrency("com.sportfaction.soccerarena.player_box_medium");
                        break;
                    case "com.sportfaction.soccerarena.player_box_big":
                        Shop.Instance.BuyProductWithHardCurrency("com.sportfaction.soccerarena.player_box_big");
                        break;
                    #endregion

                    //#region GoTo ArenaSeason
                    //case "GetRewardArenaSeason_Btn":
                    //    Data.Instance.LootMain.UnlockRewardArenaSeason();
                    //    break;
                    //#endregion

                }
            }

            yield return new WaitForSeconds(0.3f);

            _currentTransform = null;
            _pointerClickTransform = null;
        }


        public void StartLoader()
        {
            DefaultUIAnimation.Instance.FadeInCanvasGroup(Loader_Grp, 0.3f);
        }

        public void StopLoader()
        {
            DefaultUIAnimation.Instance.FadeOutCanvasGroup(Loader_Grp, 0.3f);
        }


        public void OnGetLootData_GE()
        {
            PreviousActive_Go = CurrentActive_Go;

            StartCoroutine(GoToLootBox());

            StopLoader();
                
            StartCoroutine(OpenLoots());
        }

        public IEnumerator OpenLoots()
        {
            LootBoxIsClosed = false;
            _endBigBoxOpened = false;
            _endMediumBoxOpened = false;
            _endSmallBoxOpened = false;

            LootData lData = MetaData.Instance.LootMain.LootData;


            if (false == MetaData.Instance.LootMain.LootData.IsConsumed)
            {
                if (lData.Type == "shop")
                {
                    GameObject Shop_Grp = UI.Instance.PrefabsIdxArray_Gob[Array.IndexOf(UI.Instance.PrefabsIdxArray_Str, "Shop_Grp")];
                    Shop.Instance.Loader_Gob.SetActive(false);
                    Shop.Instance.Loader_Gob.GetComponent<CanvasGroup>().alpha = 0;
                    Shop_Grp.SetActive(false);
                    Shop_Grp.GetComponent<CanvasGroup>().alpha = 0;
                }

                if (lData.Type == "app_update")
                {
                    UserData.Instance.Profile.VersionUpdated = false;
                    UserData.Instance.PersistUserData();
                }

                if (lData.Type == "medium" || lData.Type == "small" || lData.Type == "arenaseason" || lData.Type == "shop" || lData.Type == "app_update")
                {

                #if DEVELOPMENT_BUILD
                    Debug.Log(lData.Type + " | " + " BBOX (" + +lData.BigBox.Count + ") " + " MBOX (" + +lData.MediumBox.Count + ") " + " SBOX (" + +lData.SmallBox.Count + ") " + " F (" + lData.Fragment + ") " + " H (" + lData.Hard + ") ");
                #endif



                    if (lData.BigBox.Count > 0)
                    {
                        StartCoroutine(OpenBigBox(lData));
                    }
                    else
                    {
                        _endBigBoxOpened = true;
                    }

                    while (_endBigBoxOpened == false)
                    {
                        yield return new WaitForSeconds(0.3f);

                    }

                    if (lData.MediumBox.Count > 0)
                    {
                        StartCoroutine(OpenMediumBox(lData));
                    }
                    else
                    {
                        _endMediumBoxOpened = true;
                    }

                    while (_endMediumBoxOpened == false)
                    {
                        yield return new WaitForSeconds(0.3f);

                    }

                    if (lData.SmallBox.Count > 0)
                    {
                        StartCoroutine(OpenSmallBox(lData));
                    }
                    else
                    {
                        _endSmallBoxOpened = true;
                    }

                    while (_endSmallBoxOpened == false)
                    {
                        yield return new WaitForSeconds(0.3f);

                    }

                    if (lData.MediumBox.Count == 0 && lData.BigBox.Count == 0 && lData.SmallBox.Count == 0)
                    {
                        MetaData.Instance.LootMain.GetLootTimers();
                        GameObject LootBox_Grp = PrefabsIdxArray_Gob[Array.IndexOf(UI.Instance.PrefabsIdxArray_Str, "LootBox_Grp")];
                        LootBox_Grp.SetActive(true);
                        LootBox.Instance.Init(lData);
                    }
                }

                MetaData.Instance.UserMain.GetProfile();
                MetaData.Instance.PlayersMain.GetPlayersManager();
            }

        }

        public IEnumerator OpenBigBox(LootData pData)
        {
            for (int i = 0; i <= pData.BigBox.Count - 1; i++)
            {
                GameObject LootBox_Grp = PrefabsIdxArray_Gob[Array.IndexOf(UI.Instance.PrefabsIdxArray_Str, "LootBox_Grp")];
                LootBox_Grp.SetActive(true);

                if (pData.BigBox.Count - 1 == i && pData.MediumBox.Count == 0)
                {
                    LootBox.Instance.Init(pData.BigBox[i], true);
                }
                else
                {
                    LootBox.Instance.Init(pData.BigBox[i], false);
                }

                while (LootBoxIsClosed == false)
                {
                    yield return new WaitForSeconds(0.3f);

                }

                LootBoxIsClosed = false;

            }
            _endBigBoxOpened = true;



        }

        public IEnumerator OpenMediumBox(LootData pData)
        {
            GameObject LootBox_Grp = PrefabsIdxArray_Gob[Array.IndexOf(UI.Instance.PrefabsIdxArray_Str, "LootBox_Grp")];
            for (int i = 0; i <= pData.MediumBox.Count - 1; i++)
            {
                LootBox_Grp.SetActive(true);

                if (pData.MediumBox.Count - 1 == i)
                {
                    LootBox.Instance.Init(pData.MediumBox[i], true);
                }
                else
                {
                    LootBox.Instance.Init(pData.MediumBox[i], false);
                }

                while (LootBoxIsClosed == false)
                {
                    yield return new WaitForSeconds(0.3f);

                }
                LootBoxIsClosed = false;

            }

            _endMediumBoxOpened = true;
        }

        public IEnumerator OpenSmallBox(LootData pData)
        {
            GameObject LootBox_Grp = PrefabsIdxArray_Gob[Array.IndexOf(UI.Instance.PrefabsIdxArray_Str, "LootBox_Grp")];
            for (int i = 0; i <= pData.SmallBox.Count - 1; i++)
            {
                LootBox_Grp.SetActive(true);

                if (pData.SmallBox.Count - 1 == i)
                {
                    LootBox.Instance.Init(pData.SmallBox[i], true);
                }
                else
                {
                    LootBox.Instance.Init(pData.SmallBox[i], false);
                }

                while (LootBoxIsClosed == false)
                {
                    yield return new WaitForSeconds(0.3f);

                }
                LootBoxIsClosed = false;

            }

            _endSmallBoxOpened = true;
        }

        public void OnLootboxIsClosed_GE()
        {
            LootBoxIsClosed = true;

            if(false == UserData.Instance.Profile.NotifMinionsPlayers)
            {
                UserData.Instance.Profile.NotifMinionsPlayers = true;
                UserData.Instance.PersistUserData();

            }
        }

        #endregion

        #region Private Methods

        void Start()
        {
             DefaultUIAnimation.Instance.FadeInBackground(BG);
             DefaultUIAnimation.Instance.FadeInCanvasGroup(this.gameObject);

            if (true == UserData.Instance.Profile.OnBoarding && true == UserData.Instance.Profile.OnBoardingMeta)
            {
                UserData.Instance.Profile.OnBoardingHome = true;
                UserData.Instance.PersistUserData();

                GameObject OnBoarding_Grp = PrefabsIdxArray_Gob[Array.IndexOf(PrefabsIdxArray_Str, "OnBoarding_Grp")];
                DefaultUIAnimation.Instance.FadeInCanvasGroup(OnBoarding_Grp);
            }
            else if(false == UserData.Instance.Profile.OnBoarding && true == UserData.Instance.Profile.OnBoardingHome)
            {
                GameObject OnBoarding_Grp = PrefabsIdxArray_Gob[Array.IndexOf(PrefabsIdxArray_Str, "OnBoarding_Grp")];
                DefaultUIAnimation.Instance.FadeInCanvasGroup(OnBoarding_Grp);

                UserData.Instance.Profile.OnBoarding = false;
                UserData.Instance.Profile.OnBoardingHome = false;
                UserData.Instance.Profile.NotifShop = true;
                UserData.Instance.PersistUserData();
                Home.Instance.UpdateNotif();
                StartCoroutine(OnBoarding.Instance.OpenPopin("Home3"));

                MetaData.Instance.OnGetSquadPlayers_GE();

            }
            else
            {
                GameObject MenuHome_Grp = PrefabsIdxArray_Gob[Array.IndexOf(PrefabsIdxArray_Str, "Home_Grp")];
                DefaultUIAnimation.Instance.FadeInCanvasGroup(MenuHome_Grp);
                //TODO
                //PushNotifications.SetActive(true);

                MetaData.Instance.OnGetProductsList_GE();

                // Update Shop
                GameObject Shop_Grp = PrefabsIdxArray_Gob[Array.IndexOf(UI.Instance.PrefabsIdxArray_Str, "Shop_Grp")];
                Shop_Grp.SetActive(true);
                Shop.Instance.OnLocalTimersUpdated_GE();
                Shop_Grp.SetActive(false);



                MetaData.Instance.OnGetSquadPlayers_GE();





            }



            //StartCoroutine(DEVGoToMatchmaking());


            // StartCoroutine(GoToMinionsPlayers());

            Redirect();
        }

        public void Redirect()
        {
            if (null != UserData.Instance)
            {
                switch (UserData.Instance.Redirect)
                {
                    case "matchmaking_pvp":
                    case "matchmaking_bot":
                        StartCoroutine(Redirect(GoToMatchmaking()));
                        break;
                }
            }
        }

        void Awake()
        {
            Instance = this;

            //--------------------------------------------------------------------> Store GameObject Names For Array Index Of (Search By Name On A Specific Array)
            PrefabsIdxArray_Str = new string[PrefabsIdxArray_Gob.Length];

            for (int i = 0; i < PrefabsIdxArray_Str.Length; i++)
            { PrefabsIdxArray_Str[i] = PrefabsIdxArray_Gob[i].name; }

            DisableAllWindows();

            GameObject MenuHome_Grp = PrefabsIdxArray_Gob[Array.IndexOf(PrefabsIdxArray_Str, "Home_Grp")];

            if (true == UserData.Instance.Profile.OnBoarding && true == UserData.Instance.Profile.OnBoardingMeta)
            {
                GameObject OnBoarding_Grp = PrefabsIdxArray_Gob[Array.IndexOf(PrefabsIdxArray_Str, "OnBoarding_Grp")];

                OnBoarding_Grp.SetActive(true);

                OnBoarding_Grp.GetComponent<OnBoarding>().Init();

                CurrentActive_Go = OnBoarding_Grp;
            }
            else
            {
                MenuHome_Grp.SetActive(true);

                CurrentActive_Go = MenuHome_Grp;
            }



            //GameObject RewardHome = MenuHome_Grp.transform.Find("Home_ArenaCup_Grp/Reward_Grp").gameObject;
            //RewardHome.SetActive(false);

            this.transform.GetComponent<CanvasGroup>().alpha = 0;
            DefaultUIAnimation.Instance.FadeOutBackground(BG, 0f);
        }

        private IEnumerator Redirect(IEnumerator func)
        {
            yield return new WaitForSeconds(1f);

            StartCoroutine(func);
        }

        private void DisableAllWindows()
        {
            GameObject MenuHome_Grp = PrefabsIdxArray_Gob[Array.IndexOf(UI.Instance.PrefabsIdxArray_Str, "Home_Grp")];
            MenuHome_Grp.SetActive(false);
            MenuHome_Grp.GetComponent<CanvasGroup>().alpha = 0;

            GameObject OnBoarding_Grp = PrefabsIdxArray_Gob[Array.IndexOf(UI.Instance.PrefabsIdxArray_Str, "OnBoarding_Grp")];
            OnBoarding_Grp.SetActive(false);
            OnBoarding_Grp.GetComponent<CanvasGroup>().alpha = 0;

            GameObject Matchmaking_Grp = PrefabsIdxArray_Gob[Array.IndexOf(UI.Instance.PrefabsIdxArray_Str, "Matchmaking_Grp")];
            Matchmaking_Grp.SetActive(false);
            Matchmaking_Grp.GetComponent<CanvasGroup>().alpha = 0;

            GameObject Ranking_Grp = PrefabsIdxArray_Gob[Array.IndexOf(UI.Instance.PrefabsIdxArray_Str, "Ranking_Grp")];
            Ranking_Grp.SetActive(false);
            Ranking_Grp.GetComponent<CanvasGroup>().alpha = 0;

            GameObject MinionsPlayers_Grp = PrefabsIdxArray_Gob[Array.IndexOf(UI.Instance.PrefabsIdxArray_Str, "MinionsPlayers_Grp")];
            MinionsPlayers_Grp.SetActive(false);
            MinionsPlayers_Grp.GetComponent<CanvasGroup>().alpha = 0;

            GameObject LootBox_Grp = PrefabsIdxArray_Gob[Array.IndexOf(UI.Instance.PrefabsIdxArray_Str, "LootBox_Grp")];
            LootBox_Grp.SetActive(false);
            LootBox_Grp.GetComponent<CanvasGroup>().alpha = 0;

            GameObject Shop_Grp = PrefabsIdxArray_Gob[Array.IndexOf(UI.Instance.PrefabsIdxArray_Str, "Shop_Grp")];
            Shop_Grp.SetActive(false);
            Shop_Grp.GetComponent<CanvasGroup>().alpha = 0;

            GameObject Fantasy_Grp = PrefabsIdxArray_Gob[Array.IndexOf(UI.Instance.PrefabsIdxArray_Str, "Fantasy_Grp")];
            Fantasy_Grp.SetActive(false);
            Fantasy_Grp.GetComponent<CanvasGroup>().alpha = 0;
        }

    


#endregion
    }
}
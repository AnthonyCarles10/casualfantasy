﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using DG.Tweening;
using Newtonsoft.Json.Linq;
using Photon.Pun;
using Sportfaction.CasualFantasy.Events;
using Sportfaction.CasualFantasy.Leaderboard;
using Sportfaction.CasualFantasy.Manager.Profile;
using Sportfaction.CasualFantasy.Players;
using Sportfaction.CasualFantasy.PVP.Gameplay.Matchs;
using Sportfaction.CasualFantasy.PVP.Network;
using Sportfaction.CasualFantasy.Services;
using SportFaction.CasualFootballEngine.PhaseService.Enum;
using SportFaction.CasualFootballEngine.PlayerService.Enum;
using SportFaction.CasualFootballEngine.TeamService.Enum;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Sportfaction.CasualFantasy.Meta
{
    /// <summary>
    /// Description: Matchmaking
    /// Author: MAC & Antoine de Lachèze-Murel
    /// </summary>
    public sealed class MinionsPlayers : MonoBehaviour
    {
        public static MinionsPlayers Instance;

        #region Public Methods

        public string CurrentState_Str = "Idle";


        public GameObject SelectedMinion_Gob;
        public GameObject TeamScore_Gob;
        public GameObject MinionSelectedFieldGrp_Gob;
        public GameObject MinionSelectedListGrp_Gob;
        public GameObject MinionUnselectedGrp_Gob;
        public GameObject MinionsPlayersAssignDataBtn_Gob;
        public GameObject RPPopinBtn_Gob;
        //public GameObject BackBtn_Gob;
        public GameObject RemoveBtn_Gob;
        public GameObject FilterPopin_Gob;
        public GameObject AlertScorePopin_Gob;
        public GameObject RealPointsPlayerPopin_Gob;
        public GameObject MinionOutOfFieldGrp_Gob;
        public GameObject Field_Gob;
        public GameObject PlayersScrollGrp_Gob;
        public GameObject LineRenderer_Gob;
        public GameObject LineRendererScroll_Gob;
        public GameObject SearchPopin_Gob;
        public GameObject AlertNotOwnedPopin_Gob;
        public  GameObject  MinionSelectedHeader_Gob;

        public GameObject LoaderGrp_Gob;

        public Camera Camera;


        public List<string> SelectedTeamMinions_List = new List<string>();
        public Dictionary<string, GameObject> MinionChildGrp_Dic = new Dictionary<string, GameObject>();

        private string _previousFilterSkill = "";
        public string PreviousFilterPosition = "";
        private bool _ownedFilter = true;
        private bool _nameFilter = false;
        private string _nameFilterValue = "";

        private Transform _currentTransform;

        public int TeamScore = 0;

        public int MaxScore = 900;

        public int PrevisionScore = 0;

        private bool _showAlertScorePopin = false;

        private int _lastScore = 0;

        private Color _greenColor;

        private Color _redColor;

        public string LastSquadPositionFieldSelected = "";
    

        #endregion

        #region Public Methods

        void Start()
        {
            ColorUtility.TryParseHtmlString("#8CFF08", out _greenColor);

            ColorUtility.TryParseHtmlString("#E2322A", out _redColor);

            MaxScore = 900;

            AlertScorePopin_Gob.transform.Find("Popin_Grp/Team_Score_Grp/ScoreMax_Txt").GetComponent<TMPro.TMP_Text>().text = MaxScore.ToString();

            TeamScore_Gob.transform.Find("Max_Grp/ScoreMax_Txt").GetComponent<TMPro.TMP_Text>().text = MaxScore.ToString();
        }

        public void Init()
        {

                // INIT GameObject Position / Activation
                RectTransform lRectYellowBar = TeamScore_Gob.transform.Find("Bar_Grp/YellowBar_Img").GetComponent<RectTransform>();
                lRectYellowBar.sizeDelta = new Vector2(0, 23f);

                RectTransform lRectBlueBar = TeamScore_Gob.transform.Find("Bar_Grp/BlueBar_Img").GetComponent<RectTransform>();
                lRectBlueBar.sizeDelta = new Vector2(0, 23f);

                TeamScore_Gob.transform.Find("Max_Grp").gameObject.SetActive(false);

                TeamScore_Gob.transform.Find("Score_Txt").GetComponent<RectTransform>().localPosition = new Vector3(30f, TeamScore_Gob.transform.Find("Score_Txt").localPosition.y, TeamScore_Gob.transform.Find("Score_Txt").localPosition.z);

        

                //Field_Gob.GetComponent<RectTransform>().localPosition = new Vector3(0, 30, 0);

                MinionSelectedFieldGrp_Gob.SetActive(false);

                MinionSelectedListGrp_Gob.SetActive(false);

            //if (true == UserData.Instance.Profile.OnBoarding)
            //{
            //    MinionUnselectedGrp_Gob.SetActive(false);
            //}
            //else
            //{
            //    MinionUnselectedGrp_Gob.SetActive(true);

            //    MinionUnselectedGrp_Gob.GetComponent<CanvasGroup>().alpha = 1;
            //}

            MinionUnselectedGrp_Gob.SetActive(false);
         

                RPPopinBtn_Gob.SetActive(false);


                MinionsPlayersAssignDataBtn_Gob.SetActive(false);

                RemoveBtn_Gob.SetActive(false);

                SearchPopin_Gob.SetActive(false);

                LineRendererScroll_Gob.SetActive(false);
                //BackBtn_Gob.SetActive(false);

                //PlayersScrollGrp_Gob.GetComponent<RectTransform>().anchoredPosition = new Vector3(PlayersScrollGrp_Gob.GetComponent<RectTransform>().localPosition.x, -320f, PlayersScrollGrp_Gob.GetComponent<RectTransform>().localPosition.z);//0f

                MinionOutOfFieldGrp_Gob.GetComponent<CanvasGroup>().alpha = 0;

                LineRenderer_Gob.SetActive(true);

                MinionsPlayersAssignDataBtn_Gob.transform.Find("Lock_Img").gameObject.SetActive(false);
                MinionsPlayersAssignDataBtn_Gob.transform.GetComponent<Image>().color = Color.white;
                MinionsPlayersAssignDataBtn_Gob.transform.Find("Validate_Txt").GetComponent<TMPro.TMP_Text>().color = Color.white;


                // INIT SelectedTeamMinions_List with Data Squad Position
                SelectedTeamMinions_List = new List<string>();

                for (int i = 0; i < MetaData.Instance.PlayersTeamSelected.Count; i++)
                {
                    SelectedTeamMinions_List.Add(MetaData.Instance.PlayersTeamSelected[i].Profile.SquadPosition);
                }


                UpdateTeamPlayersField();
            MaxScore = 900;
            UpdateTeamScore(false, 0, false);

            _previousFilterSkill = "Score_Btn";

                //CurrentState_Str = "Idle";
       
        }

        public void OnGetPlayerLastGame_GE()
        {
            //RealPointsPlayerPopin_Gob.SetActive(true);
            //RealPointsPlayerPopin_Gob.transform.Find("Popin_Grp").transform.localPosition = new Vector3(0, 0, 0);
            //RealPointsPlayerPopin_Gob.transform.Find("Popin_Grp").GetComponent<Animator>().SetTrigger("Open");

            Debug.Log("Data.Instance.PlayerLastGame = " + MetaData.Instance.PlayersMain.PlayerLastGame);

            // DEFEND
            RealPointsPlayerPopin_Gob.transform.Find("Popin_Grp/RPTabLeft_Grp/#0/Name_Txt").GetComponent<TMPro.TMP_Text>().text = I2.Loc.LocalizationManager.GetTranslation("goalsConcOnfield_rp");
            RealPointsPlayerPopin_Gob.transform.Find("Popin_Grp/RPTabLeft_Grp/#0/RP_Txt").GetComponent<TMPro.TMP_Text>().text   = MetaData.Instance.PlayersMain.PlayerLastGame.Skills.GoalsConcOnfield.ToString();
            RealPointsPlayerPopin_Gob.transform.Find("Popin_Grp/RPTabLeft_Grp/#1/Name_Txt").GetComponent<TMPro.TMP_Text>().text = I2.Loc.LocalizationManager.GetTranslation("tackle_rp");
            RealPointsPlayerPopin_Gob.transform.Find("Popin_Grp/RPTabLeft_Grp/#1/RP_Txt").GetComponent<TMPro.TMP_Text>().text   = MetaData.Instance.PlayersMain.PlayerLastGame.Skills.Tackle.ToString();
            RealPointsPlayerPopin_Gob.transform.Find("Popin_Grp/RPTabLeft_Grp/#2/Name_Txt").GetComponent<TMPro.TMP_Text>().text = I2.Loc.LocalizationManager.GetTranslation("ballRecovery_rp");
            RealPointsPlayerPopin_Gob.transform.Find("Popin_Grp/RPTabLeft_Grp/#2/RP_Txt").GetComponent<TMPro.TMP_Text>().text   = MetaData.Instance.PlayersMain.PlayerLastGame.Skills.BallRecovery.ToString();
            RealPointsPlayerPopin_Gob.transform.Find("Popin_Grp/RPTabLeft_Grp/#3/Name_Txt").GetComponent<TMPro.TMP_Text>().text = I2.Loc.LocalizationManager.GetTranslation("interception_rp");
            RealPointsPlayerPopin_Gob.transform.Find("Popin_Grp/RPTabLeft_Grp/#3/RP_Txt").GetComponent<TMPro.TMP_Text>().text   = MetaData.Instance.PlayersMain.PlayerLastGame.Skills.Interception.ToString();
            RealPointsPlayerPopin_Gob.transform.Find("Popin_Grp/RPTabLeft_Grp/#4/Name_Txt").GetComponent<TMPro.TMP_Text>().text = I2.Loc.LocalizationManager.GetTranslation("dispossessed_rp");
            RealPointsPlayerPopin_Gob.transform.Find("Popin_Grp/RPTabLeft_Grp/#4/RP_Txt").GetComponent<TMPro.TMP_Text>().text   = MetaData.Instance.PlayersMain.PlayerLastGame.Skills.Dispossessed.ToString();
            RealPointsPlayerPopin_Gob.transform.Find("Popin_Grp/RPTabLeft_Grp/#5/Name_Txt").GetComponent<TMPro.TMP_Text>().text = I2.Loc.LocalizationManager.GetTranslation("goals_rp");
            RealPointsPlayerPopin_Gob.transform.Find("Popin_Grp/RPTabLeft_Grp/#5/RP_Txt").GetComponent<TMPro.TMP_Text>().text   = MetaData.Instance.PlayersMain.PlayerLastGame.Skills.Goals.ToString();
            RealPointsPlayerPopin_Gob.transform.Find("Popin_Grp/RPTabLeft_Grp/#6/Name_Txt").GetComponent<TMPro.TMP_Text>().text = I2.Loc.LocalizationManager.GetTranslation("keyPass_rp");
            RealPointsPlayerPopin_Gob.transform.Find("Popin_Grp/RPTabLeft_Grp/#6/RP_Txt").GetComponent<TMPro.TMP_Text>().text   = MetaData.Instance.PlayersMain.PlayerLastGame.Skills.KeyPass.ToString();
            RealPointsPlayerPopin_Gob.transform.Find("Popin_Grp/RPTabLeft_Grp/#7/Name_Txt").GetComponent<TMPro.TMP_Text>().text = I2.Loc.LocalizationManager.GetTranslation("goalAssist_rp");
            RealPointsPlayerPopin_Gob.transform.Find("Popin_Grp/RPTabLeft_Grp/#7/RP_Txt").GetComponent<TMPro.TMP_Text>().text   = MetaData.Instance.PlayersMain.PlayerLastGame.Skills.GoalAssist.ToString();
            RealPointsPlayerPopin_Gob.transform.Find("Popin_Grp/RPTabLeft_Grp/#8/Name_Txt").GetComponent<TMPro.TMP_Text>().text = I2.Loc.LocalizationManager.GetTranslation("accuratePassTr3_rp");
            RealPointsPlayerPopin_Gob.transform.Find("Popin_Grp/RPTabLeft_Grp/#8/RP_Txt").GetComponent<TMPro.TMP_Text>().text  = MetaData.Instance.PlayersMain.PlayerLastGame.Skills.AccuratePassTr3.ToString();
            RealPointsPlayerPopin_Gob.transform.Find("Popin_Grp/RPTabLeft_Grp/#9/Name_Txt").GetComponent<TMPro.TMP_Text>().text = I2.Loc.LocalizationManager.GetTranslation("wonContest_rp");
            RealPointsPlayerPopin_Gob.transform.Find("Popin_Grp/RPTabLeft_Grp/#9/RP_Txt").GetComponent<TMPro.TMP_Text>().text = MetaData.Instance.PlayersMain.PlayerLastGame.Skills.WonContest.ToString();

            RealPointsPlayerPopin_Gob.transform.Find("Popin_Grp/RPTabRight_Grp/#0/Name_Txt").GetComponent<TMPro.TMP_Text>().text = I2.Loc.LocalizationManager.GetTranslation("accurateCrossNocorner_rp");
            RealPointsPlayerPopin_Gob.transform.Find("Popin_Grp/RPTabRight_Grp/#0/RP_Txt").GetComponent<TMPro.TMP_Text>().text   = MetaData.Instance.PlayersMain.PlayerLastGame.Skills.AccurateCrossNocorner.ToString();
            RealPointsPlayerPopin_Gob.transform.Find("Popin_Grp/RPTabRight_Grp/#1/Name_Txt").GetComponent<TMPro.TMP_Text>().text = I2.Loc.LocalizationManager.GetTranslation("shotOffTarget_rp");
            RealPointsPlayerPopin_Gob.transform.Find("Popin_Grp/RPTabRight_Grp/#1/RP_Txt").GetComponent<TMPro.TMP_Text>().text   = MetaData.Instance.PlayersMain.PlayerLastGame.Skills.ShotOffTarget.ToString();
            RealPointsPlayerPopin_Gob.transform.Find("Popin_Grp/RPTabRight_Grp/#2/Name_Txt").GetComponent<TMPro.TMP_Text>().text = I2.Loc.LocalizationManager.GetTranslation("bigChanceMissed_rp");
            RealPointsPlayerPopin_Gob.transform.Find("Popin_Grp/RPTabRight_Grp/#2/RP_Txt").GetComponent<TMPro.TMP_Text>().text   = MetaData.Instance.PlayersMain.PlayerLastGame.Skills.BigChanceMissed.ToString();
            RealPointsPlayerPopin_Gob.transform.Find("Popin_Grp/RPTabRight_Grp/#3/Name_Txt").GetComponent<TMPro.TMP_Text>().text = I2.Loc.LocalizationManager.GetTranslation("redCard_rp");
            RealPointsPlayerPopin_Gob.transform.Find("Popin_Grp/RPTabRight_Grp/#3/RP_Txt").GetComponent<TMPro.TMP_Text>().text   = MetaData.Instance.PlayersMain.PlayerLastGame.Skills.RedCard.ToString();
            RealPointsPlayerPopin_Gob.transform.Find("Popin_Grp/RPTabRight_Grp/#4/Name_Txt").GetComponent<TMPro.TMP_Text>().text = I2.Loc.LocalizationManager.GetTranslation("yellowCard_rp");
            RealPointsPlayerPopin_Gob.transform.Find("Popin_Grp/RPTabRight_Grp/#4/RP_Txt").GetComponent<TMPro.TMP_Text>().text   = MetaData.Instance.PlayersMain.PlayerLastGame.Skills.YellowCard.ToString();
            if(MetaData.Instance.PlayersMain.PlayerLastGame.Skills.MinsPlayed1 == 0)
            {
                RealPointsPlayerPopin_Gob.transform.Find("Popin_Grp/RPTabRight_Grp/#5/Name_Txt").GetComponent<TMPro.TMP_Text>().text = I2.Loc.LocalizationManager.GetTranslation("minsPlayed1_rp");
                RealPointsPlayerPopin_Gob.transform.Find("Popin_Grp/RPTabRight_Grp/#5/RP_Txt").GetComponent<TMPro.TMP_Text>().text = MetaData.Instance.PlayersMain.PlayerLastGame.Skills.MinsPlayed1.ToString();
            }
            else
            {
                RealPointsPlayerPopin_Gob.transform.Find("Popin_Grp/RPTabRight_Grp/#5/Name_Txt").GetComponent<TMPro.TMP_Text>().text = I2.Loc.LocalizationManager.GetTranslation("noMinsPlayed_rp");
                RealPointsPlayerPopin_Gob.transform.Find("Popin_Grp/RPTabRight_Grp/#5/RP_Txt").GetComponent<TMPro.TMP_Text>().text = MetaData.Instance.PlayersMain.PlayerLastGame.Skills.NoMinsPlayed.ToString();
            }
            RealPointsPlayerPopin_Gob.transform.Find("Popin_Grp/RPTabRight_Grp/#6/Name_Txt").GetComponent<TMPro.TMP_Text>().text = I2.Loc.LocalizationManager.GetTranslation("totalOffside_rp");
            RealPointsPlayerPopin_Gob.transform.Find("Popin_Grp/RPTabRight_Grp/#6/RP_Txt").GetComponent<TMPro.TMP_Text>().text   = MetaData.Instance.PlayersMain.PlayerLastGame.Skills.TotalOffside.ToString();
            RealPointsPlayerPopin_Gob.transform.Find("Popin_Grp/RPTabRight_Grp/#7/Name_Txt").GetComponent<TMPro.TMP_Text>().text = I2.Loc.LocalizationManager.GetTranslation("fouls_rp");
            RealPointsPlayerPopin_Gob.transform.Find("Popin_Grp/RPTabRight_Grp/#7/RP_Txt").GetComponent<TMPro.TMP_Text>().text   = MetaData.Instance.PlayersMain.PlayerLastGame.Skills.Fouls.ToString();
            RealPointsPlayerPopin_Gob.transform.Find("Popin_Grp/RPTabRight_Grp/#8/Name_Txt").GetComponent<TMPro.TMP_Text>().text = I2.Loc.LocalizationManager.GetTranslation("saves_rp");
            RealPointsPlayerPopin_Gob.transform.Find("Popin_Grp/RPTabRight_Grp/#8/RP_Txt").GetComponent<TMPro.TMP_Text>().text   = MetaData.Instance.PlayersMain.PlayerLastGame.Skills.Saves.ToString();
            RealPointsPlayerPopin_Gob.transform.Find("Popin_Grp/RPTabRight_Grp/#9/Name_Txt").GetComponent<TMPro.TMP_Text>().text = I2.Loc.LocalizationManager.GetTranslation("saves_rp");
            RealPointsPlayerPopin_Gob.transform.Find("Popin_Grp/RPTabRight_Grp/#9/RP_Txt").GetComponent<TMPro.TMP_Text>().text   = MetaData.Instance.PlayersMain.PlayerLastGame.Skills.Saves.ToString();


            RealPointsPlayerPopin_Gob.transform.Find("Popin_Grp/RPWonOverall_Txt").GetComponent<TMPro.TMP_Text>().text = I2.Loc.LocalizationManager.GetTranslation("rp_won_overall") + " " + MetaData.Instance.PlayersMain.PlayerLastGame.RealPointGlobal.ToString();
            RealPointsPlayerPopin_Gob.transform.Find("Popin_Grp/Total_Grp/Score_Txt").GetComponent<TMPro.TMP_Text>().text = MetaData.Instance.PlayersMain.PlayerLastGame.RealPoint.ToString();

            RealPointsPlayerPopin_Gob.transform.Find("Popin_Grp/Versus_Grp/NameTeamA_Txt").GetComponent<TMPro.TMP_Text>().text = MetaData.Instance.PlayersMain.PlayerLastGame.GameData.TeamAName;
            RealPointsPlayerPopin_Gob.transform.Find("Popin_Grp/Versus_Grp/ScoreTeamA_Txt").GetComponent<TMPro.TMP_Text>().text = MetaData.Instance.PlayersMain.PlayerLastGame.GameData.TeamAScore.ToString();
            RealPointsPlayerPopin_Gob.transform.Find("Popin_Grp/Versus_Grp/NameTeamB_Txt").GetComponent<TMPro.TMP_Text>().text = MetaData.Instance.PlayersMain.PlayerLastGame.GameData.TeamBName;
            RealPointsPlayerPopin_Gob.transform.Find("Popin_Grp/Versus_Grp/ScoreTeamB_Txt").GetComponent<TMPro.TMP_Text>().text = MetaData.Instance.PlayersMain.PlayerLastGame.GameData.TeamBScore.ToString();

            RealPointsPlayerPopin_Gob.transform.Find("Popin_Grp/Header_Grp/Header_Txt").GetComponent<TMPro.TMP_Text>().text = MetaData.Instance.MinionScrollviewSelected.Profile.Name;

            RealPointsPlayerPopin_Gob.transform.Find("Popin_Grp/MatchName_Grp/Name_Txt").GetComponent<TMPro.TMP_Text>().text = MetaData.Instance.PlayersMain.PlayerLastGame.GameData.ChampionshipName;

            RealPointsPlayerPopin_Gob.transform.Find("Popin_Grp/FormLevel_Grp/Score_Txt").GetComponent<TMPro.TMP_Text>().text = MetaData.Instance.PlayersMain.PlayerLastGame.GameData.Form.ToString();

            int lForm = MetaData.Instance.PlayersMain.PlayerLastGame.GameData.Form;

            RectTransform lRectransform = RealPointsPlayerPopin_Gob.transform.Find("Popin_Grp/FormLevel_Grp/FormLevelBar_Grp/Arrow_Img").GetComponent<RectTransform>();

            if (lForm <= 0)
            {
                lRectransform.DOAnchorPosX(-18f, 2f);
            }
            else if(lForm > 0 && lForm <= 20)
            {
                lRectransform.DOAnchorPosX((192 * lForm) / 20, 2f);
            }
            else if (lForm > 20 && lForm <= 70)
            {
                lRectransform.DOAnchorPosX((400 * lForm) / 70, 2f);
            }
            else if (lForm > 70 && lForm <= 133)
            {
                lRectransform.DOAnchorPosX((605 * lForm) / 133, 2f);
            }
            else if (lForm > 133 && lForm <= 170)
            {
                lRectransform.DOAnchorPosX((820 * lForm) / 170, 2f);
            }
            else
            {
                lRectransform.DOAnchorPosX(820, 2f);
            }
        }

        public IEnumerator MinionsPlayersGoBack()
        {
            UpdateTeamPlayersField();

            yield return new WaitForSeconds(0.1f); // wait button animation

            MinionsPlayersAssignDataBtn_Gob.SetActive(false);
            //BackBtn_Gob.SetActive(false);

            Sequence lSequence = DOTween.Sequence();
            //lSequence.Insert(0, Field_Gob.GetComponent<RectTransform>().DOAnchorPosX(-465.9f, 0.3f));//-1600f
            //lSequence.Insert(0, MinionOutOfFieldGrp_Gob.GetComponent<RectTransform>().DOAnchorPosX(MinionOutOfFieldGrp_Gob.GetComponent<RectTransform>().localPosition.x + 860f, 0.3f));
            //lSequence.Insert(0, PlayersScrollGrp_Gob.GetComponent<RectTransform>().DOAnchorPosY(-320f, 0.3f));//0f
            //lSequence.Insert(0, MinionSelectedFieldGrp_Gob.GetComponent<RectTransform>().DOAnchorPosX(0f, 0.3f));//-960f
            //lSequence.Insert(0, MinionSelectedListGrp_Gob.GetComponent<RectTransform>().DOAnchorPosX(960f, 0.3f));//0f
            lSequence.Insert(0.2f, MinionSelectedFieldGrp_Gob.GetComponent<CanvasGroup>().DOFade(0, 0.3f));//0f
            lSequence.Insert(0.2f, MinionSelectedListGrp_Gob.GetComponent<CanvasGroup>().DOFade(0, 0.3f));//0f
            lSequence.Insert(0.2f, MinionOutOfFieldGrp_Gob.GetComponent<CanvasGroup>().DOFade(0, 0.3f));//0f

            yield return new WaitForSeconds(0.3f); // wait button animation

            MinionSelectedFieldGrp_Gob.SetActive(false);
            MinionSelectedListGrp_Gob.SetActive(false);
            MinionUnselectedGrp_Gob.SetActive(true);
          
        }

        public IEnumerator RemoveMinionPlayer()
        {
            yield return new WaitForSeconds(0.1f); // wait button animation

            PlayersItem lPlayer = MetaData.Instance.PlayersTeamSelected.Find(x => x.Profile.SquadPosition == MetaData.Instance.MinionFieldSquadPositionSelected);

            if(lPlayer != null)
            {
                MetaData.Instance.PlayersTeamSelected.Remove(lPlayer);
                MetaData.Instance.SaveMinionsPlayers();
            }

            LoadData(MetaData.Instance.MinionFieldSquadPositionSelected);
            //Data.Instance.PositionFieldSelected = null;
            //Data.Instance.MinionFieldSquadPositionSelected = null;
            MetaData.Instance.MinionScrollviewSelected = null;
            MetaData.Instance.MinionScrollviewPlayerIdSelected = null;
            MinionsPlayersScrollview.Instance.SelectedMinion = null;
            MinionsPlayersScrollview.Instance.SelectedMinionPlayerId = null;

            MinionsPlayersScrollview.Instance.UpdateData();
            UpdateTeamPlayersField();
            UpdateTeamScore();

            MinionsPlayersAssignDataBtn_Gob.SetActive(false);
            RemoveBtn_Gob.SetActive(false);
            LineRenderer_Gob.SetActive(false);
            LineRendererScroll_Gob.SetActive(false);

            Sequence lSequence = DOTween.Sequence();
            lSequence.Insert(0f, MinionSelectedListGrp_Gob.GetComponent<CanvasGroup>().DOFade(0, 0.3f));//0f

 
            yield return new WaitForSeconds(0.5f); // wait button animation
        }


        public IEnumerator OpenAlertOwnedPopin()
        {
            yield return new WaitForSeconds(0.1f); // wait button animation

            AlertNotOwnedPopin_Gob.SetActive(true);
            AlertNotOwnedPopin_Gob.transform.Find("Popin_Grp").transform.localPosition = new Vector3(0, 0, 0);
            AlertNotOwnedPopin_Gob.transform.Find("Popin_Grp").GetComponent<Animator>().SetTrigger("Open");

            LineRenderer_Gob.SetActive(false);
        }

        public IEnumerator CloseAlertOwnedPopin()
        {
            yield return new WaitForSeconds(0.1f); // wait button animation

            AlertNotOwnedPopin_Gob.SetActive(true);
            AlertNotOwnedPopin_Gob.transform.Find("Popin_Grp").GetComponent<Animator>().SetTrigger("Close");

            yield return new WaitForSeconds(0.15f); // wait popin close animation

            AlertNotOwnedPopin_Gob.SetActive(false);
            //TODO
            LineRenderer_Gob.SetActive(true);
        }

        public IEnumerator AssignDataToMinionField()
        {

            yield return new WaitForSeconds(0.1f); // wait button animation


            if (false == MetaData.Instance.MinionScrollviewSelected.Profile.IsOwned)
            {

                StartCoroutine(OpenAlertOwnedPopin());
            }
            else if(TeamScore > MaxScore || PrevisionScore > MaxScore)
            {
                AlertScorePopin_Gob.SetActive(true);
                AlertScorePopin_Gob.transform.Find("Popin_Grp").transform.localPosition = new Vector3(0, 0, 0);
                AlertScorePopin_Gob.transform.Find("Popin_Grp").GetComponent<Animator>().SetTrigger("Open");
                LineRenderer_Gob.SetActive(false);
            }
            else
            {
                MetaData.Instance.SetPlayerToMinion();
                MetaData.Instance.SaveMinionsPlayers();

                MinionsPlayersAssignDataBtn_Gob.SetActive(false);
                RemoveBtn_Gob.SetActive(true);

                //BackBtn_Gob.SetActive(false);

                LoadData(MetaData.Instance.MinionFieldSquadPositionSelected);
                MetaData.Instance.MinionScrollviewSelected = null;
                MetaData.Instance.MinionScrollviewPlayerIdSelected = null;
                MinionsPlayersScrollview.Instance.SelectedMinion = null;
                MinionsPlayersScrollview.Instance.SelectedMinionPlayerId = null;

                LineRendererScroll_Gob.SetActive(false);
                MinionsPlayersScrollview.Instance.UpdateData();


                Sequence lSequence = DOTween.Sequence();
                lSequence.Insert(0.2f, MinionSelectedListGrp_Gob.GetComponent<CanvasGroup>().DOFade(0, 0.3f));//0f


                UpdateTeamScore();


              
            }

        }

        public IEnumerator CloseRealPointsPlayerPopin()
        {
            yield return new WaitForSeconds(0.1f); // wait button animation

            RealPointsPlayerPopin_Gob.SetActive(true);
            RealPointsPlayerPopin_Gob.transform.Find("Popin_Grp").GetComponent<Animator>().SetTrigger("Close");

            yield return new WaitForSeconds(0.15f); // wait popin close animation

            RealPointsPlayerPopin_Gob.SetActive(false);
        }

        public IEnumerator OpenSearchByNamePopin()
        {
            yield return new WaitForSeconds(0.1f); // wait button animation

            if (true == _nameFilter)
            {
                StartCoroutine(FilterByName());
            }
            else
            {
                SearchPopin_Gob.SetActive(true);
                SearchPopin_Gob.transform.Find("Popin_Grp").transform.localPosition = new Vector3(0, 0, 0);
                SearchPopin_Gob.transform.Find("Popin_Grp").GetComponent<Animator>().SetTrigger("Open");
            }

            LineRenderer_Gob.SetActive(false);

        }

        public IEnumerator CloseSearchByNamePopin()
        {
            yield return new WaitForSeconds(0.1f); // wait button animation

            SearchPopin_Gob.SetActive(true);
            SearchPopin_Gob.transform.Find("Popin_Grp").GetComponent<Animator>().SetTrigger("Close");

            yield return new WaitForSeconds(0.15f); // wait popin close animation

            SearchPopin_Gob.SetActive(false);

            LineRenderer_Gob.SetActive(true);
        }

        public IEnumerator OpenFilterPopin()
        {
            yield return new WaitForSeconds(0.1f); // wait button animation

            FilterPopin_Gob.SetActive(true);
            FilterPopin_Gob.transform.Find("Popin_Grp").transform.localPosition = new Vector3(0,0,0);
            FilterPopin_Gob.transform.Find("Popin_Grp").GetComponent<Animator>().SetTrigger("Open");

            LineRenderer_Gob.SetActive(false);
        }

        public IEnumerator CloseFilterPopin()
        {
            yield return new WaitForSeconds(0.1f); // wait button animation

            FilterPopin_Gob.SetActive(true);
            FilterPopin_Gob.transform.Find("Popin_Grp").GetComponent<Animator>().SetTrigger("Close");

            yield return new WaitForSeconds(0.15f); // wait popin close animation

            FilterPopin_Gob.SetActive(false);
            //TODO
            LineRenderer_Gob.SetActive(true);
        }

        public IEnumerator CloseAlertScorePopin()
        {
            yield return new WaitForSeconds(0.1f); // wait button animation

            AlertScorePopin_Gob.SetActive(true);
            AlertScorePopin_Gob.transform.Find("Popin_Grp").GetComponent<Animator>().SetTrigger("Close");

            yield return new WaitForSeconds(0.15f); // wait popin close animation

            AlertScorePopin_Gob.SetActive(false);
            //TODO
            LineRenderer_Gob.SetActive(true);
        }

        public IEnumerator ResetFilter()
        {
            yield return new WaitForSeconds(0f); // wait button animation

            Sprite[] lSpritesMenu = Resources.LoadAll<Sprite>("Sprites/Meta/Menu_2_SS");

            MetaData.Instance.QueryParams["order_by"] = "score";
            MetaData.Instance.QueryParams["position"] = "";

            if (false == String.IsNullOrEmpty(_previousFilterSkill))
            {
                if (_previousFilterSkill == "Score_Btn")
                {
                    this.transform.Find("FilterPopin_Grp/Popin_Grp/" + _previousFilterSkill).GetComponent<Image>().sprite = UtilityMethods.GetSprite(lSpritesMenu, "Button_Blue_2_Background");
                }
                else
                {
                    this.transform.Find("FilterPopin_Grp/Popin_Grp/" + _previousFilterSkill).GetComponent<Image>().sprite = UtilityMethods.GetSprite(lSpritesMenu, "Button_Blue_Background");
                }

                this.transform.Find("FilterPopin_Grp/Popin_Grp/Score_Btn").GetComponent<Image>().sprite = UtilityMethods.GetSprite(lSpritesMenu, "Button_Yellow_2_Background");

                _previousFilterSkill = "";
            }

            if (false == String.IsNullOrEmpty(PreviousFilterPosition))
            {
                if (false == String.IsNullOrEmpty(PreviousFilterPosition))
                {
                    this.transform.Find("FilterPopin_Grp/Popin_Grp/" + PreviousFilterPosition).GetComponent<Image>().sprite = UtilityMethods.GetSprite(lSpritesMenu, "Button_Blue_Background");
                }
                PreviousFilterPosition = "";
            }

            MetaData.Instance.Filter = true;
            MetaData.Instance.GetPlayersList();

            StartCoroutine(CloseFilterPopin());
        }

        public IEnumerator ValidateFilter()
        {
            yield return new WaitForSeconds(0f); // wait button animation

            if (false == String.IsNullOrEmpty(_previousFilterSkill))
            {
                string lFilter = _previousFilterSkill.Substring(0, _previousFilterSkill.Length - 4);
                
                if("Score" == lFilter)
                {
                    MetaData.Instance.QueryParams["order_by"] = "score";
                }
                else
                {
                    MetaData.Instance.QueryParams["order_by"] = lFilter.ToLower() + "_skill";
                }
           
            }
            else
            {
                MetaData.Instance.QueryParams["order_by"] = "score";
            }

            if (false == String.IsNullOrEmpty(PreviousFilterPosition))
            {
                string lFilter = "";
                if (PreviousFilterPosition.Length == 1)
                {
                    lFilter = PreviousFilterPosition;
                }
                else
                {
                    lFilter = PreviousFilterPosition.Substring(0, PreviousFilterPosition.Length - 4);
                }

                MetaData.Instance.QueryParams["position"] = lFilter;
            }
            else
            {
                MetaData.Instance.QueryParams["position"] = "";
            }

            if(true == _ownedFilter)
            {
                MetaData.Instance.QueryParams["is_owned"] = "1";
            }
            else
            {
                MetaData.Instance.QueryParams["is_owned"] = "-1";
            }

            if(true == _nameFilter)
            {
                _nameFilterValue = Regex.Replace(_nameFilterValue, "\u200B", "");
                MetaData.Instance.QueryParams["keyword"] = _nameFilterValue;
            }
            else
            {
                MetaData.Instance.QueryParams["keyword"] = "";
            }

            MetaData.Instance.QueryParams["offset"] = "0";


            FilterApiAndAnimation();
            //StartCoroutine(CloseFilterPopin());


        }

        public void FilterApiAndAnimation()
        {
            MetaData.Instance.Filter = true;
            MetaData.Instance.GetPlayersList();

            MetaData.Instance.MinionScrollviewPlayerIdSelected = null;
            LineRendererScroll_Gob.SetActive(false);

            Sequence lSequence = DOTween.Sequence();
            lSequence.Insert(0, PlayersScrollGrp_Gob.GetComponent<RectTransform>().DOAnchorPosY(-320f, 0.3f));//0f
            lSequence.Insert(0.5f, PlayersScrollGrp_Gob.GetComponent<RectTransform>().DOAnchorPosY(0, 0.5f));//0f
        }

        public IEnumerator FilterBySkill(string button_Str)
        {
            yield return new WaitForSeconds(0.1f); // wait button animation


            Sprite[] lSpritesMenu = Resources.LoadAll<Sprite>("Sprites/Meta/Menu_2_SS");
            if(button_Str == "Score_Btn")
            {
                this.transform.Find("FilterPopin_Grp/Popin_Grp/" + button_Str).GetComponent<Image>().sprite = UtilityMethods.GetSprite(lSpritesMenu, "Button_Yellow_2_Background");
            }
            else
            {
                this.transform.Find("FilterPopin_Grp/Popin_Grp/" + button_Str).GetComponent<Image>().sprite = UtilityMethods.GetSprite(lSpritesMenu, "Button_Yellow_Background");
            }
  

            if(false == String.IsNullOrEmpty(_previousFilterSkill))
            {
                if (_previousFilterSkill == "Score_Btn")
                {
                    this.transform.Find("FilterPopin_Grp/Popin_Grp/" + _previousFilterSkill).GetComponent<Image>().sprite = UtilityMethods.GetSprite(lSpritesMenu, "Button_Blue_2_Background");
                }
                else
                {
                    this.transform.Find("FilterPopin_Grp/Popin_Grp/" + _previousFilterSkill).GetComponent<Image>().sprite = UtilityMethods.GetSprite(lSpritesMenu, "Button_Blue_Background");
                }
               
            }

            if(_previousFilterSkill == button_Str)
            {
                _previousFilterSkill = "";
            }
            else
            {
                _previousFilterSkill = button_Str;
            }
        }

        public IEnumerator FilterByPosition(string button_Str)
        {
            yield return new WaitForSeconds(0.1f); // wait button animation

            Sprite[] lSpritesMenu = Resources.LoadAll<Sprite>("Sprites/Meta/Menu_2_SS");

            PlayersScrollGrp_Gob.transform.Find("FilterPopin_Grp/Popin_Grp/" + button_Str).GetComponent<Image>().sprite = UtilityMethods.GetSprite(lSpritesMenu, "Button_Filter_On");

            Debug.Log("Previous filter = " + PreviousFilterPosition);

            if (false == String.IsNullOrEmpty(PreviousFilterPosition))
            {
                PlayersScrollGrp_Gob.transform.Find("FilterPopin_Grp/Popin_Grp/" + PreviousFilterPosition).GetComponent<Image>().sprite = UtilityMethods.GetSprite(lSpritesMenu, "Button_Filter_Off");
            }

            if (PreviousFilterPosition == button_Str)
            {
                PreviousFilterPosition = "";
            }
            else
            {
                PreviousFilterPosition = button_Str;
            }

            switch (button_Str)
            {
                case "G_Btn":
                    LastSquadPositionFieldSelected = "G";
                    break;
                case "M_Btn":
                    LastSquadPositionFieldSelected = "MD";
                    break;
                case "D_Btn":
                    LastSquadPositionFieldSelected = "DD";
                    break;
                case "A_Btn":
                    LastSquadPositionFieldSelected = "ACD";
                    break;
            }
            DefaultPositionSelected();
        

            StartCoroutine(ValidateFilter());
        }

        public IEnumerator FilterByOwnedPlayers()
        {
            yield return new WaitForSeconds(0.1f); // wait button animation
            Sprite[] lSpritesMenu = Resources.LoadAll<Sprite>("Sprites/Meta/Menu_2_SS");

            if (false == _ownedFilter)
            {
                PlayersScrollGrp_Gob.transform.Find("FilterPopin_Grp/Popin_Grp/OwnedPlayers_Btn").GetComponent<Image>().sprite = UtilityMethods.GetSprite(lSpritesMenu, "Button_Filter_On");
                _ownedFilter = true;
            }
            else
            {
                PlayersScrollGrp_Gob.transform.Find("FilterPopin_Grp/Popin_Grp/OwnedPlayers_Btn").GetComponent<Image>().sprite = UtilityMethods.GetSprite(lSpritesMenu, "Button_Filter_Off");
                _ownedFilter = false;
            }
            StartCoroutine(ValidateFilter());
        }

        public IEnumerator FilterByName()
        {
            yield return new WaitForSeconds(0.1f); // wait button animation
            Sprite[] lSpritesMenu = Resources.LoadAll<Sprite>("Sprites/Meta/Menu_2_SS");

            if (false == _nameFilter)
            {
                _nameFilter = true;
                PlayersScrollGrp_Gob.transform.Find("FilterPopin_Grp/Popin_Grp/SearchByName_Btn").GetComponent<Image>().sprite = UtilityMethods.GetSprite(lSpritesMenu, "Button_Filter_On");
                _nameFilterValue = SearchPopin_Gob.transform.Find("Popin_Grp/Search_Input_Grp/Search_Input_Mask/Value_Txt").GetComponent<TMPro.TMP_Text>().text;
                StartCoroutine(CloseSearchByNamePopin());
            }
            else
            {
                _nameFilter = false;
                PlayersScrollGrp_Gob.transform.Find("FilterPopin_Grp/Popin_Grp/SearchByName_Btn").GetComponent<Image>().sprite = UtilityMethods.GetSprite(lSpritesMenu, "Button_Filter_Off");
                SearchPopin_Gob.transform.Find("Popin_Grp/Search_Input_Grp/Search_Input_Mask/Value_Txt").GetComponent<TMPro.TMP_Text>().text = "";
                _nameFilterValue = "";
            }

           
            StartCoroutine(ValidateFilter());
        }

        public void LoadData(string pPosition)
        {

            //Sprite[] lSpritesMenu = Resources.LoadAll<Sprite>("Sprites/Meta/Menu_SS");

            if (null != GetPlayersItem(pPosition))
            {

                PlayersItem lPlayersItem = GetPlayersItem(pPosition);

                if ((null != lPlayersItem && lPlayersItem.Profile.GlobalPosition.ToString() == "G") || (null != lPlayersItem && lPlayersItem.Profile.GlobalPosition.ToString() == "D"))
                {
                    MinionSelectedFieldGrp_Gob.transform.Find("Skills_VLG/Defensive_header_Grp").transform.SetSiblingIndex(0);
                    MinionSelectedFieldGrp_Gob.transform.Find("Skills_VLG/Defensive_Grp").transform.SetSiblingIndex(1);
                    MinionSelectedFieldGrp_Gob.transform.Find("Skills_VLG/Offensive_header_Grp").transform.SetSiblingIndex(2);
                    MinionSelectedFieldGrp_Gob.transform.Find("Skills_VLG/Offensive_Grp").transform.SetSiblingIndex(3);
                }
                else
                {
                    MinionSelectedFieldGrp_Gob.transform.Find("Skills_VLG/Defensive_header_Grp").transform.SetSiblingIndex(2);
                    MinionSelectedFieldGrp_Gob.transform.Find("Skills_VLG/Defensive_Grp").transform.SetSiblingIndex(3);
                    MinionSelectedFieldGrp_Gob.transform.Find("Skills_VLG/Offensive_header_Grp").transform.SetSiblingIndex(0);
                    MinionSelectedFieldGrp_Gob.transform.Find("Skills_VLG/Offensive_Grp").transform.SetSiblingIndex(1);
                }
               

                MinionSelectedFieldGrp_Gob.transform.Find("Header_Player_Grp/Selected_Minion_Score_Txt").GetComponent<TMPro.TMP_Text>().text = lPlayersItem.Profile.Score.ToString();

 
                MinionSelectedFieldGrp_Gob.transform.Find("Header_Player_Grp/Selected_Minion_Name_Txt").GetComponent<TMPro.TMP_Text>().text = lPlayersItem.Profile.NameField;

                MinionSelectedFieldGrp_Gob.transform.Find("Header_Player_Grp/Selected_Minion_Favorit_Position_Txt").GetComponent<TMPro.TMP_Text>().text =  I2.Loc.LocalizationManager.GetTranslation(lPlayersItem.Profile.GlobalPosition + "short");

                MinionSelectedFieldGrp_Gob.transform.Find("Skills_VLG/Defensive_header_Grp/Score_Txt").GetComponent<TMPro.TMP_Text>().text = lPlayersItem.Profile.Defense.ToString();

                MinionSelectedFieldGrp_Gob.transform.Find("Skills_VLG/Offensive_header_Grp/Score_Txt").GetComponent<TMPro.TMP_Text>().text = lPlayersItem.Profile.Attack.ToString();

                TMPro.TMP_Text lTMP_Text = MinionSelectedFieldGrp_Gob.transform.Find("Skills_VLG/Defensive_Grp/Pressing_skill/Label_Txt").GetComponent<TMPro.TMP_Text>();
                lTMP_Text.text += " - - - - - - - - - - - - - - - - - - - - - - - - ";

                MinionSelectedFieldGrp_Gob.transform.Find("Skills_VLG/Defensive_Grp/Pressing_skill/Value_Txt").GetComponent<TMPro.TMP_Text>().text = lPlayersItem.Profile.GetSkill(eSkill.PRESSING).ToString();

                lTMP_Text = MinionSelectedFieldGrp_Gob.transform.Find("Skills_VLG/Defensive_Grp/Counter_skill/Label_Txt").GetComponent<TMPro.TMP_Text>();
                lTMP_Text.text += " - - - - - - - - - - - - - - - - - - - - - - - - ";

                MinionSelectedFieldGrp_Gob.transform.Find("Skills_VLG/Defensive_Grp/Counter_skill/Value_Txt").GetComponent<TMPro.TMP_Text>().text = lPlayersItem.Profile.GetSkill(eSkill.COUNTER).ToString();

                lTMP_Text = MinionSelectedFieldGrp_Gob.transform.Find("Skills_VLG/Defensive_Grp/Tackle_skill/Label_Txt").GetComponent<TMPro.TMP_Text>();
                lTMP_Text.text += " - - - - - - - - - - - - - - - - - - - - - - - - ";

                MinionSelectedFieldGrp_Gob.transform.Find("Skills_VLG/Defensive_Grp/Tackle_skill/Value_Txt").GetComponent<TMPro.TMP_Text>().text = lPlayersItem.Profile.GetSkill(eSkill.TACKLE).ToString();

                lTMP_Text = MinionSelectedFieldGrp_Gob.transform.Find("Skills_VLG/Defensive_Grp/Interception_pass_skill/Label_Txt").GetComponent<TMPro.TMP_Text>();
                lTMP_Text.text += " - - - - - - - - - - - - - - - - - - - - - - - - ";

                MinionSelectedFieldGrp_Gob.transform.Find("Skills_VLG/Defensive_Grp/Interception_pass_skill/Value_Txt").GetComponent<TMPro.TMP_Text>().text = lPlayersItem.Profile.GetSkill(eSkill.INTERCEPTION_PASS).ToString();

                lTMP_Text = MinionSelectedFieldGrp_Gob.transform.Find("Skills_VLG/Defensive_Grp/Insterception_shot_skill/Label_Txt").GetComponent<TMPro.TMP_Text>();
                lTMP_Text.text += " - - - - - - - - - - - - - - - - - - - - - - - - ";

                MinionSelectedFieldGrp_Gob.transform.Find("Skills_VLG/Defensive_Grp/Insterception_shot_skill/Value_Txt").GetComponent<TMPro.TMP_Text>().text = lPlayersItem.Profile.GetSkill(eSkill.INTERCEPTION_LONG_KICK).ToString();

                lTMP_Text = MinionSelectedFieldGrp_Gob.transform.Find("Skills_VLG/Defensive_Grp/Defensive_duel_skill/Label_Txt").GetComponent<TMPro.TMP_Text>();
                lTMP_Text.text += " - - - - - - - - - - - - - - - - - - - - - - - - ";

                MinionSelectedFieldGrp_Gob.transform.Find("Skills_VLG/Defensive_Grp/Defensive_duel_skill/Value_Txt").GetComponent<TMPro.TMP_Text>().text = lPlayersItem.Profile.GetSkill(eSkill.DEFENSIVE_DUEL).ToString();

                lTMP_Text = MinionSelectedFieldGrp_Gob.transform.Find("Skills_VLG/Defensive_Grp/Pressure_player_ball/Label_Txt").GetComponent<TMPro.TMP_Text>();
                lTMP_Text.text += " - - - - - - - - - - - - - - - - - - - - - - - - ";

                MinionSelectedFieldGrp_Gob.transform.Find("Skills_VLG/Defensive_Grp/Pressure_player_ball/Value_Txt").GetComponent<TMPro.TMP_Text>().text = MatchEngine.Instance.Engine.ActionMain.GetPressionValue(lPlayersItem.Profile.GetSkill(eSkill.DEFENSIVE_DUEL)).ToString();

                lTMP_Text = MinionSelectedFieldGrp_Gob.transform.Find("Skills_VLG/Offensive_Grp/Long_pass_skill/Label_Txt").GetComponent<TMPro.TMP_Text>();
                lTMP_Text.text += " - - - - - - - - - - - - - - - - - - - - - - - - ";

                MinionSelectedFieldGrp_Gob.transform.Find("Skills_VLG/Offensive_Grp/Long_pass_skill/Value_Txt").GetComponent<TMPro.TMP_Text>().text = lPlayersItem.Profile.GetSkill(eSkill.LONG_PASS).ToString();

                lTMP_Text = MinionSelectedFieldGrp_Gob.transform.Find("Skills_VLG/Offensive_Grp/Short_pass_skill/Label_Txt").GetComponent<TMPro.TMP_Text>();
                lTMP_Text.text += " - - - - - - - - - - - - - - - - - - - - - - - - ";

                MinionSelectedFieldGrp_Gob.transform.Find("Skills_VLG/Offensive_Grp/Short_pass_skill/Value_Txt").GetComponent<TMPro.TMP_Text>().text = lPlayersItem.Profile.GetSkill(eSkill.SHORT_PASS).ToString();

                lTMP_Text = MinionSelectedFieldGrp_Gob.transform.Find("Skills_VLG/Offensive_Grp/Shot_skill/Label_Txt").GetComponent<TMPro.TMP_Text>();
                lTMP_Text.text += " - - - - - - - - - - - - - - - - - - - - - - - - ";

                MinionSelectedFieldGrp_Gob.transform.Find("Skills_VLG/Offensive_Grp/Shot_skill/Value_Txt").GetComponent<TMPro.TMP_Text>().text = lPlayersItem.Profile.GetSkill(eSkill.SHOT).ToString();

                lTMP_Text = MinionSelectedFieldGrp_Gob.transform.Find("Skills_VLG/Offensive_Grp/Dribble_skill/Label_Txt").GetComponent<TMPro.TMP_Text>();
                lTMP_Text.text += " - - - - - - - - - - - - - - - - - - - - - - - - ";

                MinionSelectedFieldGrp_Gob.transform.Find("Skills_VLG/Offensive_Grp/Dribble_skill/Value_Txt").GetComponent<TMPro.TMP_Text>().text = lPlayersItem.Profile.GetSkill(eSkill.DRIBBLE).ToString();

                lTMP_Text = MinionSelectedFieldGrp_Gob.transform.Find("Skills_VLG/Offensive_Grp/Offensive_duel_skill/Label_Txt").GetComponent<TMPro.TMP_Text>();
                lTMP_Text.text += " - - - - - - - - - - - - - - - - - - - - - - - - ";

                MinionSelectedFieldGrp_Gob.transform.Find("Skills_VLG/Offensive_Grp/Offensive_duel_skill/Value_Txt").GetComponent<TMPro.TMP_Text>().text = lPlayersItem.Profile.GetSkill(eSkill.OFFENSIVE_DUEL).ToString();

                Sprite[] lSpritesMenu = Resources.LoadAll<Sprite>("Sprites/Meta/Menu_2_SS");
                // Sprite[] lSpritesGameplay = Resources.LoadAll<Sprite>("Sprites/Gameplay/gameplay");
                switch (MetaData.Instance.FormLevelLetter[lPlayersItem.Profile.FormLevel])
                {
                    case "A":
                        MinionSelectedFieldGrp_Gob.transform.Find("Precision_Points_Grp/Bubble_Grp/Form_Img").GetComponent<Image>().sprite = UtilityMethods.GetSprite(lSpritesMenu, "Form_Flame_Player_A");
                        break;
                    case "B":
                        MinionSelectedFieldGrp_Gob.transform.Find("Precision_Points_Grp/Bubble_Grp/Form_Img").GetComponent<Image>().sprite = UtilityMethods.GetSprite(lSpritesMenu, "Form_Flame_Player_B");
                        break;
                    case "C":
                        MinionSelectedFieldGrp_Gob.transform.Find("Precision_Points_Grp/Bubble_Grp/Form_Img").GetComponent<Image>().sprite = UtilityMethods.GetSprite(lSpritesMenu, "Form_Flame_Player_C");
                        break;
                    case "D":
                        MinionSelectedFieldGrp_Gob.transform.Find("Precision_Points_Grp/Bubble_Grp/Form_Img").GetComponent<Image>().sprite = UtilityMethods.GetSprite(lSpritesMenu, "Form_Flame_Player_D");
                        break;
                }


                MinionSelectedFieldGrp_Gob.transform.Find("Precision_Points_Grp/Bubble_Grp/Value_Txt").GetComponent<TMPro.TMP_Text>().text = "+" + MetaData.Instance.FormLevelPrecisionPoints[lPlayersItem.Profile.FormLevel] + " pp"; ;

                MinionSelectedFieldGrp_Gob.transform.Find("Footer_Grp/Teamname_Txt").GetComponent<TMPro.TMP_Text>().text = lPlayersItem.Profile.TeamName;

                MinionSelectedFieldGrp_Gob.transform.Find("Footer_Grp/Championship_Txt").GetComponent<TMPro.TMP_Text>().text = lPlayersItem.Profile.ChampionshipName;

                Sprite[] lSpritesMenuFlag = Resources.LoadAll<Sprite>("Sprites/Meta/Flag");
             
                string country = lPlayersItem.Profile.Country.Replace(" ", "-").ToLower();

                Image lImg = MinionSelectedFieldGrp_Gob.transform.Find("Footer_Grp/Flag_Img").GetComponent<Image>();
                lImg.sprite = UtilityMethods.GetSprite(lSpritesMenuFlag, country);
                Color lColor = lImg.color;

                if (lImg.sprite == null)
                {
                    lColor.a = 0;
                }
                else
                {
                    lColor.a = 1f;
                }


                lImg.color = lColor;

                if (lPlayersItem.Profile.SquadPosition == "G")
                {

                    MinionSelectedFieldGrp_Gob.transform.Find("Skills_VLG/Defensive_Grp/Pressing_skill/Label_Txt").GetComponent<TMPro.TMP_Text>().text = I2.Loc.LocalizationManager.GetTranslation("stopTitle");
                    lTMP_Text = MinionSelectedFieldGrp_Gob.transform.Find("Skills_VLG/Defensive_Grp/Pressing_skill/Label_Txt").GetComponent<TMPro.TMP_Text>();
                    lTMP_Text.text += " - - - - - - - - - - - - - - - - - - - - - - - - ";

                    MinionSelectedFieldGrp_Gob.transform.Find("Skills_VLG/Defensive_Grp/Pressing_skill/Value_Txt").GetComponent<TMPro.TMP_Text>().text = lPlayersItem.Profile.GetSkill(eSkill.SAVE).ToString();

                    MinionSelectedFieldGrp_Gob.transform.Find("Skills_VLG/Defensive_Grp/Counter_skill/Value_Txt").gameObject.SetActive(false);
                    MinionSelectedFieldGrp_Gob.transform.Find("Skills_VLG/Defensive_Grp/Counter_skill/Label_Txt").gameObject.SetActive(false);

                    MinionSelectedFieldGrp_Gob.transform.Find("Skills_VLG/Defensive_Grp/Tackle_skill/Value_Txt").gameObject.SetActive(false);
                    MinionSelectedFieldGrp_Gob.transform.Find("Skills_VLG/Defensive_Grp/Tackle_skill/Label_Txt").gameObject.SetActive(false);

                    MinionSelectedFieldGrp_Gob.transform.Find("Skills_VLG/Defensive_Grp/Interception_pass_skill/Value_Txt").gameObject.SetActive(false);
                    MinionSelectedFieldGrp_Gob.transform.Find("Skills_VLG/Defensive_Grp/Interception_pass_skill/Label_Txt").gameObject.SetActive(false);

                    MinionSelectedFieldGrp_Gob.transform.Find("Skills_VLG/Defensive_Grp/Insterception_shot_skill/Value_Txt").gameObject.SetActive(false);
                    MinionSelectedFieldGrp_Gob.transform.Find("Skills_VLG/Defensive_Grp/Insterception_shot_skill/Label_Txt").gameObject.SetActive(false);

                    MinionSelectedFieldGrp_Gob.transform.Find("Skills_VLG/Defensive_Grp/Defensive_duel_skill/Value_Txt").gameObject.SetActive(false);
                    MinionSelectedFieldGrp_Gob.transform.Find("Skills_VLG/Defensive_Grp/Defensive_duel_skill/Label_Txt").gameObject.SetActive(false);

                    MinionSelectedFieldGrp_Gob.transform.Find("Skills_VLG/Defensive_Grp/Pressure_player_ball/Value_Txt").gameObject.SetActive(false);
                    MinionSelectedFieldGrp_Gob.transform.Find("Skills_VLG/Defensive_Grp/Pressure_player_ball/Label_Txt").gameObject.SetActive(false);

                    MinionSelectedFieldGrp_Gob.transform.Find("Skills_VLG/Offensive_Grp/Shot_skill/Value_Txt").gameObject.SetActive(false);
                    MinionSelectedFieldGrp_Gob.transform.Find("Skills_VLG/Offensive_Grp/Shot_skill/Label_Txt").gameObject.SetActive(false);

                    MinionSelectedFieldGrp_Gob.transform.Find("Skills_VLG/Offensive_Grp/Dribble_skill/Value_Txt").gameObject.SetActive(false);
                    MinionSelectedFieldGrp_Gob.transform.Find("Skills_VLG/Offensive_Grp/Dribble_skill/Label_Txt").gameObject.SetActive(false);

                    MinionSelectedFieldGrp_Gob.transform.Find("Skills_VLG/Offensive_Grp/Offensive_duel_skill/Value_Txt").gameObject.SetActive(false);
                    MinionSelectedFieldGrp_Gob.transform.Find("Skills_VLG/Offensive_Grp/Offensive_duel_skill/Label_Txt").gameObject.SetActive(false);
                }
                else
                {
                    MinionSelectedFieldGrp_Gob.transform.Find("Skills_VLG/Defensive_Grp/Pressing_skill/Label_Txt").GetComponent<TMPro.TMP_Text>().text = I2.Loc.LocalizationManager.GetTranslation("pressing_title");
                    lTMP_Text = MinionSelectedFieldGrp_Gob.transform.Find("Skills_VLG/Defensive_Grp/Pressing_skill/Label_Txt").GetComponent<TMPro.TMP_Text>();
                    lTMP_Text.text += " - - - - - - - - - - - - - - - - - - - - - - - - ";


                    MinionSelectedFieldGrp_Gob.transform.Find("Skills_VLG/Defensive_Grp/Counter_skill/Value_Txt").gameObject.SetActive(true);
                    MinionSelectedFieldGrp_Gob.transform.Find("Skills_VLG/Defensive_Grp/Counter_skill/Label_Txt").gameObject.SetActive(true);

                    MinionSelectedFieldGrp_Gob.transform.Find("Skills_VLG/Defensive_Grp/Tackle_skill/Value_Txt").gameObject.SetActive(true);
                    MinionSelectedFieldGrp_Gob.transform.Find("Skills_VLG/Defensive_Grp/Tackle_skill/Label_Txt").gameObject.SetActive(true);

                    MinionSelectedFieldGrp_Gob.transform.Find("Skills_VLG/Defensive_Grp/Interception_pass_skill/Value_Txt").gameObject.SetActive(true);
                    MinionSelectedFieldGrp_Gob.transform.Find("Skills_VLG/Defensive_Grp/Interception_pass_skill/Label_Txt").gameObject.SetActive(true);

                    MinionSelectedFieldGrp_Gob.transform.Find("Skills_VLG/Defensive_Grp/Insterception_shot_skill/Value_Txt").gameObject.SetActive(true);
                    MinionSelectedFieldGrp_Gob.transform.Find("Skills_VLG/Defensive_Grp/Insterception_shot_skill/Label_Txt").gameObject.SetActive(true);

                    MinionSelectedFieldGrp_Gob.transform.Find("Skills_VLG/Defensive_Grp/Defensive_duel_skill/Value_Txt").gameObject.SetActive(true);
                    MinionSelectedFieldGrp_Gob.transform.Find("Skills_VLG/Defensive_Grp/Defensive_duel_skill/Label_Txt").gameObject.SetActive(true);

                    MinionSelectedFieldGrp_Gob.transform.Find("Skills_VLG/Defensive_Grp/Pressure_player_ball/Value_Txt").gameObject.SetActive(true);
                    MinionSelectedFieldGrp_Gob.transform.Find("Skills_VLG/Defensive_Grp/Pressure_player_ball/Label_Txt").gameObject.SetActive(true);

                    MinionSelectedFieldGrp_Gob.transform.Find("Skills_VLG/Offensive_Grp/Shot_skill/Value_Txt").gameObject.SetActive(true);
                    MinionSelectedFieldGrp_Gob.transform.Find("Skills_VLG/Offensive_Grp/Shot_skill/Label_Txt").gameObject.SetActive(true);

                    MinionSelectedFieldGrp_Gob.transform.Find("Skills_VLG/Offensive_Grp/Dribble_skill/Value_Txt").gameObject.SetActive(true);
                    MinionSelectedFieldGrp_Gob.transform.Find("Skills_VLG/Offensive_Grp/Dribble_skill/Label_Txt").gameObject.SetActive(true);

                    MinionSelectedFieldGrp_Gob.transform.Find("Skills_VLG/Offensive_Grp/Offensive_duel_skill/Value_Txt").gameObject.SetActive(true);
                    MinionSelectedFieldGrp_Gob.transform.Find("Skills_VLG/Offensive_Grp/Offensive_duel_skill/Label_Txt").gameObject.SetActive(true);

                }

            }
            else
            {

                MinionSelectedFieldGrp_Gob.transform.Find("Header_Player_Grp/Selected_Minion_Score_Txt").GetComponent<TMPro.TMP_Text>().text = "";

                MinionSelectedFieldGrp_Gob.transform.Find("Header_Player_Grp/Selected_Minion_Name_Txt").GetComponent<TMPro.TMP_Text>().text = "";

                MinionSelectedFieldGrp_Gob.transform.Find("Header_Player_Grp/Selected_Minion_Favorit_Position_Txt").GetComponent<TMPro.TMP_Text>().text = "";

                MinionSelectedFieldGrp_Gob.transform.Find("Skills_VLG/Defensive_header_Grp/Score_Txt").GetComponent<TMPro.TMP_Text>().text ="";

                MinionSelectedFieldGrp_Gob.transform.Find("Skills_VLG/Offensive_header_Grp/Score_Txt").GetComponent<TMPro.TMP_Text>().text = "";

                MinionSelectedFieldGrp_Gob.transform.Find("Skills_VLG/Defensive_Grp/Pressing_skill/Value_Txt").GetComponent<TMPro.TMP_Text>().text = "-";

                MinionSelectedFieldGrp_Gob.transform.Find("Skills_VLG/Defensive_Grp/Counter_skill/Value_Txt").GetComponent<TMPro.TMP_Text>().text = "-";

                MinionSelectedFieldGrp_Gob.transform.Find("Skills_VLG/Defensive_Grp/Tackle_skill/Value_Txt").GetComponent<TMPro.TMP_Text>().text = "-";

                MinionSelectedFieldGrp_Gob.transform.Find("Skills_VLG/Defensive_Grp/Interception_pass_skill/Value_Txt").GetComponent<TMPro.TMP_Text>().text = "-";

                MinionSelectedFieldGrp_Gob.transform.Find("Skills_VLG/Defensive_Grp/Insterception_shot_skill/Value_Txt").GetComponent<TMPro.TMP_Text>().text = "-";

                MinionSelectedFieldGrp_Gob.transform.Find("Skills_VLG/Defensive_Grp/Defensive_duel_skill/Value_Txt").GetComponent<TMPro.TMP_Text>().text = "-";

                MinionSelectedFieldGrp_Gob.transform.Find("Skills_VLG/Defensive_Grp/Pressure_player_ball/Value_Txt").GetComponent<TMPro.TMP_Text>().text = "-";

                MinionSelectedFieldGrp_Gob.transform.Find("Skills_VLG/Offensive_Grp/Long_pass_skill/Value_Txt").GetComponent<TMPro.TMP_Text>().text = "-";

                MinionSelectedFieldGrp_Gob.transform.Find("Skills_VLG/Offensive_Grp/Short_pass_skill/Value_Txt").GetComponent<TMPro.TMP_Text>().text = "-";

                MinionSelectedFieldGrp_Gob.transform.Find("Skills_VLG/Offensive_Grp/Shot_skill/Value_Txt").GetComponent<TMPro.TMP_Text>().text = "-";

                MinionSelectedFieldGrp_Gob.transform.Find("Skills_VLG/Offensive_Grp/Dribble_skill/Value_Txt").GetComponent<TMPro.TMP_Text>().text = "-";

                MinionSelectedFieldGrp_Gob.transform.Find("Skills_VLG/Offensive_Grp/Offensive_duel_skill/Value_Txt").GetComponent<TMPro.TMP_Text>().text = "-";

                //MinionSelectedFieldGrp_Gob.transform.Find("Precision_Points_Grp/Bubble_Grp/Form_Img/Form_Txt").GetComponent<TMPro.TMP_Text>().text = "-";

                MinionSelectedFieldGrp_Gob.transform.Find("Precision_Points_Grp/Bubble_Grp/Value_Txt").GetComponent<TMPro.TMP_Text>().text = "+ 0";

                MinionSelectedFieldGrp_Gob.transform.Find("Footer_Grp/Teamname_Txt").GetComponent<TMPro.TMP_Text>().text = "";

                MinionSelectedFieldGrp_Gob.transform.Find("Footer_Grp/Championship_Txt").GetComponent<TMPro.TMP_Text>().text = "";

                Image lImg = MinionSelectedFieldGrp_Gob.transform.Find("Footer_Grp/Flag_Img").GetComponent<Image>();
                lImg.sprite = null;
                Color lColor = lImg.color;
                lColor.a = 0f;
                lImg.color = lColor;

            }

            if (null != MetaData.Instance.MinionScrollviewSelected)
            {
                PlayersItem lPlayersItem = GetPlayersItem(MetaData.Instance.MinionFieldSquadPositionSelected);


                if ((null != lPlayersItem &&  lPlayersItem.Profile.GlobalPosition.ToString() == "G") || (null != lPlayersItem && lPlayersItem.Profile.GlobalPosition.ToString() == "D"))
                {
                    MinionSelectedListGrp_Gob.transform.Find("Skills_VLG/Defensive_header_Grp").transform.SetSiblingIndex(0);
                    MinionSelectedListGrp_Gob.transform.Find("Skills_VLG/Defensive_Grp").transform.SetSiblingIndex(1);
                    MinionSelectedListGrp_Gob.transform.Find("Skills_VLG/Offensive_header_Grp").transform.SetSiblingIndex(2);
                    MinionSelectedListGrp_Gob.transform.Find("Skills_VLG/Offensive_Grp").transform.SetSiblingIndex(3);
                }
                else
                {
                    MinionSelectedListGrp_Gob.transform.Find("Skills_VLG/Defensive_header_Grp").transform.SetSiblingIndex(2);
                    MinionSelectedListGrp_Gob.transform.Find("Skills_VLG/Defensive_Grp").transform.SetSiblingIndex(3);
                    MinionSelectedListGrp_Gob.transform.Find("Skills_VLG/Offensive_header_Grp").transform.SetSiblingIndex(0);
                    MinionSelectedListGrp_Gob.transform.Find("Skills_VLG/Offensive_Grp").transform.SetSiblingIndex(1);
                }

                TMPro.TMP_Text lText = MinionSelectedListGrp_Gob.transform.Find("Header_Player_Grp/Selected_Minion_Score_Txt").GetComponent<TMPro.TMP_Text>();
                lText.text = MetaData.Instance.MinionScrollviewSelected.Profile.Score.ToString();
                lText.color = Color.white;
                if(lPlayersItem == null || MetaData.Instance.MinionScrollviewSelected.Profile.Score > lPlayersItem.Profile.Score)
                {
                    lText.color = _greenColor;
                }
                else if(MetaData.Instance.MinionScrollviewSelected.Profile.Score > lPlayersItem.Profile.Score)
                {
                    lText.color = _redColor;
                }


                lText = MinionSelectedListGrp_Gob.transform.Find("Header_Player_Grp/Selected_Minion_Score_Txt").GetComponent<TMPro.TMP_Text>();

                lText.text = MetaData.Instance.MinionScrollviewSelected.Profile.Score.ToString();
                lText.color = Color.white;
                if (lPlayersItem == null || MetaData.Instance.MinionScrollviewSelected.Profile.Score > lPlayersItem.Profile.Score)
                {
                    lText.color = _greenColor;
                }
                else if (MetaData.Instance.MinionScrollviewSelected.Profile.Score < lPlayersItem.Profile.Score)
                {
                    lText.color = _redColor;
                }


                lText = MinionSelectedListGrp_Gob.transform.Find("Skills_VLG/Defensive_header_Grp/Score_Txt").GetComponent<TMPro.TMP_Text>();

                lText.text = MetaData.Instance.MinionScrollviewSelected.Profile.Defense.ToString();
                lText.color = Color.white;
                if (lPlayersItem == null || MetaData.Instance.MinionScrollviewSelected.Profile.Defense > lPlayersItem.Profile.Defense)
                {
                    lText.color = _greenColor;
                }
                else if (MetaData.Instance.MinionScrollviewSelected.Profile.Defense < lPlayersItem.Profile.Defense)
                {
                    lText.color = _redColor;
                }

                lText = MinionSelectedListGrp_Gob.transform.Find("Skills_VLG/Offensive_header_Grp/Score_Txt").GetComponent<TMPro.TMP_Text>();

                lText.text = MetaData.Instance.MinionScrollviewSelected.Profile.Attack.ToString();
                lText.color = Color.white;
                if (lPlayersItem == null || MetaData.Instance.MinionScrollviewSelected.Profile.Attack > lPlayersItem.Profile.Attack)
                {
                    lText.color = _greenColor;
                }
                else if (MetaData.Instance.MinionScrollviewSelected.Profile.Attack < lPlayersItem.Profile.Attack)
                {
                    lText.color = _redColor;
                }

                lText = MinionSelectedListGrp_Gob.transform.Find("Skills_VLG/Defensive_Grp/Pressing_skill/Value_Txt").GetComponent<TMPro.TMP_Text>();

                lText.text = MetaData.Instance.MinionScrollviewSelected.Profile.GetSkill(eSkill.PRESSING).ToString();
                lText.color = Color.white;
                if (lPlayersItem == null || MetaData.Instance.MinionScrollviewSelected.Profile.GetSkill(eSkill.PRESSING) > lPlayersItem.Profile.GetSkill(eSkill.PRESSING))
                {
                    lText.color = _greenColor;
                }
                else if (MetaData.Instance.MinionScrollviewSelected.Profile.GetSkill(eSkill.PRESSING) < lPlayersItem.Profile.GetSkill(eSkill.PRESSING))
                {
                    lText.color = _redColor;
                }

                lText = MinionSelectedListGrp_Gob.transform.Find("Skills_VLG/Defensive_Grp/Counter_skill/Value_Txt").GetComponent<TMPro.TMP_Text>();

                lText.text = MetaData.Instance.MinionScrollviewSelected.Profile.GetSkill(eSkill.COUNTER).ToString();
                lText.color = Color.white;
                if (lPlayersItem == null || MetaData.Instance.MinionScrollviewSelected.Profile.GetSkill(eSkill.COUNTER) > lPlayersItem.Profile.GetSkill(eSkill.COUNTER))
                {
                    lText.color = _greenColor;
                }
                else if (MetaData.Instance.MinionScrollviewSelected.Profile.GetSkill(eSkill.COUNTER) < lPlayersItem.Profile.GetSkill(eSkill.COUNTER))
                {
                    lText.color = _redColor;
                }


                lText = MinionSelectedListGrp_Gob.transform.Find("Skills_VLG/Defensive_Grp/Tackle_skill/Value_Txt").GetComponent<TMPro.TMP_Text>();

                lText.text = MetaData.Instance.MinionScrollviewSelected.Profile.GetSkill(eSkill.TACKLE).ToString();
                lText.color = Color.white;
                if (lPlayersItem == null || MetaData.Instance.MinionScrollviewSelected.Profile.GetSkill(eSkill.TACKLE) > lPlayersItem.Profile.GetSkill(eSkill.TACKLE))
                {
                    lText.color = _greenColor;
                }
                else if (MetaData.Instance.MinionScrollviewSelected.Profile.GetSkill(eSkill.TACKLE) < lPlayersItem.Profile.GetSkill(eSkill.TACKLE))
                {
                    lText.color = _redColor;
                }

                lText = MinionSelectedListGrp_Gob.transform.Find("Skills_VLG/Defensive_Grp/Interception_pass_skill/Value_Txt").GetComponent<TMPro.TMP_Text>();

                lText.text = MetaData.Instance.MinionScrollviewSelected.Profile.GetSkill(eSkill.INTERCEPTION_PASS).ToString();
                lText.color = Color.white;
                if (lPlayersItem == null || MetaData.Instance.MinionScrollviewSelected.Profile.GetSkill(eSkill.INTERCEPTION_PASS) > lPlayersItem.Profile.GetSkill(eSkill.INTERCEPTION_PASS))
                {
                    lText.color = _greenColor;
                }
                else if (MetaData.Instance.MinionScrollviewSelected.Profile.GetSkill(eSkill.INTERCEPTION_PASS) < lPlayersItem.Profile.GetSkill(eSkill.INTERCEPTION_PASS))
                {
                    lText.color = _redColor;
                }

                lText = MinionSelectedListGrp_Gob.transform.Find("Skills_VLG/Defensive_Grp/Insterception_shot_skill/Value_Txt").GetComponent<TMPro.TMP_Text>();

                lText.text = MetaData.Instance.MinionScrollviewSelected.Profile.GetSkill(eSkill.INTERCEPTION_LONG_KICK).ToString();
                lText.color = Color.white;
                if (lPlayersItem == null || MetaData.Instance.MinionScrollviewSelected.Profile.GetSkill(eSkill.INTERCEPTION_LONG_KICK) > lPlayersItem.Profile.GetSkill(eSkill.INTERCEPTION_LONG_KICK))
                {
                    lText.color = _greenColor;
                }
                else if (MetaData.Instance.MinionScrollviewSelected.Profile.GetSkill(eSkill.INTERCEPTION_LONG_KICK) < lPlayersItem.Profile.GetSkill(eSkill.INTERCEPTION_LONG_KICK))
                {
                    lText.color = _redColor;
                }


                lText = MinionSelectedListGrp_Gob.transform.Find("Skills_VLG/Defensive_Grp/Defensive_duel_skill/Value_Txt").GetComponent<TMPro.TMP_Text>();

                lText.text = MetaData.Instance.MinionScrollviewSelected.Profile.GetSkill(eSkill.DEFENSIVE_DUEL).ToString();
                lText.color = Color.white;
                if (lPlayersItem == null || MetaData.Instance.MinionScrollviewSelected.Profile.GetSkill(eSkill.DEFENSIVE_DUEL) > lPlayersItem.Profile.GetSkill(eSkill.DEFENSIVE_DUEL))
                {
                    lText.color = _greenColor;
                }
                else if (MetaData.Instance.MinionScrollviewSelected.Profile.GetSkill(eSkill.DEFENSIVE_DUEL) < lPlayersItem.Profile.GetSkill(eSkill.DEFENSIVE_DUEL))
                {
                    lText.color = _redColor;
                }

                lText = MinionSelectedListGrp_Gob.transform.Find("Skills_VLG/Defensive_Grp/Pressure_player_ball/Value_Txt").GetComponent<TMPro.TMP_Text>();


                if(null != MatchEngine.Instance && null != MatchEngine.Instance.Engine)
                {
                    lText.text = MatchEngine.Instance.Engine.ActionMain.GetPressionValue(MetaData.Instance.MinionScrollviewSelected.Profile.GetSkill(eSkill.DEFENSIVE_DUEL)).ToString();
                    lText.color = Color.white;

                    if (lPlayersItem == null || MatchEngine.Instance.Engine.ActionMain.GetPressionValue(MetaData.Instance.MinionScrollviewSelected.Profile.GetSkill(eSkill.DEFENSIVE_DUEL)) > MatchEngine.Instance.Engine.ActionMain.GetPressionValue(lPlayersItem.Profile.GetSkill(eSkill.DEFENSIVE_DUEL)))
                    {
                        lText.color = _greenColor;
                    }
                    else if (MatchEngine.Instance.Engine.ActionMain.GetPressionValue(MetaData.Instance.MinionScrollviewSelected.Profile.GetSkill(eSkill.DEFENSIVE_DUEL)) < MatchEngine.Instance.Engine.ActionMain.GetPressionValue(lPlayersItem.Profile.GetSkill(eSkill.DEFENSIVE_DUEL)))
                    {
                        lText.color = _redColor;
                    }
                }
            

               
                //if (Data.Instance.MinionScrollviewSelected.Profile.Score > lPlayersItem.Profile.Score)
                //{
                //    lText.color = _greenColor;
                //}
                //else if (Data.Instance.MinionScrollviewSelected.Profile.Score > lPlayersItem.Profile.Score)
                //{
                //    lText.color = _redColor;
                //}

                lText = MinionSelectedListGrp_Gob.transform.Find("Skills_VLG/Offensive_Grp/Long_pass_skill/Value_Txt").GetComponent<TMPro.TMP_Text>();

                lText.text = MetaData.Instance.MinionScrollviewSelected.Profile.GetSkill(eSkill.LONG_PASS).ToString();
                lText.color = Color.white;
                if (lPlayersItem == null || MetaData.Instance.MinionScrollviewSelected.Profile.GetSkill(eSkill.LONG_PASS) > lPlayersItem.Profile.GetSkill(eSkill.LONG_PASS))
                {
                    lText.color = _greenColor;
                }
                else if (MetaData.Instance.MinionScrollviewSelected.Profile.GetSkill(eSkill.LONG_PASS) < lPlayersItem.Profile.GetSkill(eSkill.LONG_PASS))
                {
                    lText.color = _redColor;
                }

                lText = MinionSelectedListGrp_Gob.transform.Find("Skills_VLG/Offensive_Grp/Short_pass_skill/Value_Txt").GetComponent<TMPro.TMP_Text>();

                lText.text = MetaData.Instance.MinionScrollviewSelected.Profile.GetSkill(eSkill.SHORT_PASS).ToString();
                lText.color = Color.white;
                if (lPlayersItem == null || MetaData.Instance.MinionScrollviewSelected.Profile.GetSkill(eSkill.SHORT_PASS) > lPlayersItem.Profile.GetSkill(eSkill.SHORT_PASS))
                {
                    lText.color = _greenColor;
                }
                else if (MetaData.Instance.MinionScrollviewSelected.Profile.GetSkill(eSkill.SHORT_PASS) < lPlayersItem.Profile.GetSkill(eSkill.SHORT_PASS))
                {
                    lText.color = _redColor;
                }

                lText = MinionSelectedListGrp_Gob.transform.Find("Skills_VLG/Offensive_Grp/Shot_skill/Value_Txt").GetComponent<TMPro.TMP_Text>();

                lText.text = MetaData.Instance.MinionScrollviewSelected.Profile.GetSkill(eSkill.SHOT).ToString();
                lText.color = Color.white;
                if (lPlayersItem == null || MetaData.Instance.MinionScrollviewSelected.Profile.GetSkill(eSkill.SHOT) > lPlayersItem.Profile.GetSkill(eSkill.SHOT))
                {
                    lText.color = _greenColor;
                }
                else if (MetaData.Instance.MinionScrollviewSelected.Profile.GetSkill(eSkill.SHOT) < lPlayersItem.Profile.GetSkill(eSkill.SHOT))
                {
                    lText.color = _redColor;
                }

                lText = MinionSelectedListGrp_Gob.transform.Find("Skills_VLG/Offensive_Grp/Dribble_skill/Value_Txt").GetComponent<TMPro.TMP_Text>();

                lText.text = MetaData.Instance.MinionScrollviewSelected.Profile.GetSkill(eSkill.DRIBBLE).ToString();
                lText.color = Color.white;
                if (lPlayersItem == null || MetaData.Instance.MinionScrollviewSelected.Profile.GetSkill(eSkill.DRIBBLE) > lPlayersItem.Profile.GetSkill(eSkill.DRIBBLE))
                {
                    lText.color = _greenColor;
                }
                else if (MetaData.Instance.MinionScrollviewSelected.Profile.GetSkill(eSkill.DRIBBLE) < lPlayersItem.Profile.GetSkill(eSkill.DRIBBLE))
                {
                    lText.color = _redColor;
                }

                lText = MinionSelectedListGrp_Gob.transform.Find("Skills_VLG/Offensive_Grp/Offensive_duel_skill/Value_Txt").GetComponent<TMPro.TMP_Text>();

                lText.text = MetaData.Instance.MinionScrollviewSelected.Profile.GetSkill(eSkill.OFFENSIVE_DUEL).ToString();
                lText.color = Color.white;
                if (lPlayersItem == null || MetaData.Instance.MinionScrollviewSelected.Profile.GetSkill(eSkill.OFFENSIVE_DUEL) > lPlayersItem.Profile.GetSkill(eSkill.OFFENSIVE_DUEL))
                {
                    lText.color = _greenColor;
                }
                else if (MetaData.Instance.MinionScrollviewSelected.Profile.GetSkill(eSkill.OFFENSIVE_DUEL) < lPlayersItem.Profile.GetSkill(eSkill.OFFENSIVE_DUEL))
                {
                    lText.color = _redColor;
                }

                //lText = MinionSelectedListGrp_Gob.transform.Find("Precision_Points_Grp/Bubble_Grp/Form_Img/Form_Txt").GetComponent<TMPro.TMP_Text>();

                //lText.text = Data.Instance.FormLevelLetter[Data.Instance.MinionScrollviewSelected.Profile.FormLevel];
                //lText.color = Color.white;
                //if (lPlayersItem == null || Data.Instance.MinionScrollviewSelected.Profile.FormLevel > lPlayersItem.Profile.FormLevel)
                //{
                //    lText.color = _greenColor;
                //}
                //else if (Data.Instance.MinionScrollviewSelected.Profile.FormLevel < lPlayersItem.Profile.FormLevel)
                //{
                //    lText.color = _redColor;
                //}

                Sprite[] lSpritesMenu = Resources.LoadAll<Sprite>("Sprites/Meta/Menu_2_SS");
                // Sprite[] lSpritesGameplay = Resources.LoadAll<Sprite>("Sprites/Gameplay/gameplay");
                switch (MetaData.Instance.FormLevelLetter[MetaData.Instance.MinionScrollviewSelected.Profile.FormLevel])
                {
                    case "A":
                        MinionSelectedListGrp_Gob.transform.Find("Precision_Points_Grp/Bubble_Grp/Form_Img").GetComponent<Image>().sprite = UtilityMethods.GetSprite(lSpritesMenu, "Form_Flame_Player_A");
                        break;
                    case "B":
                        MinionSelectedListGrp_Gob.transform.Find("Precision_Points_Grp/Bubble_Grp/Form_Img").GetComponent<Image>().sprite = UtilityMethods.GetSprite(lSpritesMenu, "Form_Flame_Player_B");
                        break;
                    case "C":
                        MinionSelectedListGrp_Gob.transform.Find("Precision_Points_Grp/Bubble_Grp/Form_Img").GetComponent<Image>().sprite = UtilityMethods.GetSprite(lSpritesMenu, "Form_Flame_Player_C");
                        break;
                    case "D":
                        MinionSelectedListGrp_Gob.transform.Find("Precision_Points_Grp/Bubble_Grp/Form_Img").GetComponent<Image>().sprite = UtilityMethods.GetSprite(lSpritesMenu, "Form_Flame_Player_D");
                        break;
                }

                lText = MinionSelectedListGrp_Gob.transform.Find("Precision_Points_Grp/Bubble_Grp/Value_Txt").GetComponent<TMPro.TMP_Text>();

                lText.text = "+" + MetaData.Instance.FormLevelPrecisionPoints[MetaData.Instance.MinionScrollviewSelected.Profile.FormLevel] + " pp" ;
                //lText.color = Color.white;
                //if (lPlayersItem == null || int.Parse(Data.Instance.FormLevelPrecisionPoints[Data.Instance.MinionScrollviewSelected.Profile.FormLevel]) > int.Parse(Data.Instance.FormLevelPrecisionPoints[lPlayersItem.Profile.FormLevel]))
                //{
                //    lText.color = _greenColor;
                //}
                //else if (int.Parse(Data.Instance.FormLevelPrecisionPoints[Data.Instance.MinionScrollviewSelected.Profile.FormLevel]) < int.Parse(Data.Instance.FormLevelPrecisionPoints[lPlayersItem.Profile.FormLevel]))
                //{
                //    lText.color = _redColor;
                //}

                if(MetaData.Instance.MinionScrollviewSelected.Profile.SquadPosition == "G")
                {

                    lText = MinionSelectedListGrp_Gob.transform.Find("Skills_VLG/Defensive_Grp/Pressing_skill/Value_Txt").GetComponent<TMPro.TMP_Text>();

                    lText.text = MetaData.Instance.MinionScrollviewSelected.Profile.GetSkill(eSkill.SAVE).ToString();
                    lText.color = Color.white;
                    if (lPlayersItem == null || MetaData.Instance.MinionScrollviewSelected.Profile.GetSkill(eSkill.SAVE) > lPlayersItem.Profile.GetSkill(eSkill.SAVE))
                    {
                        lText.color = _greenColor;
                    }
                    else if (MetaData.Instance.MinionScrollviewSelected.Profile.GetSkill(eSkill.SAVE) < lPlayersItem.Profile.GetSkill(eSkill.SAVE))
                    {
                        lText.color = _redColor;
                    }


                    MinionSelectedListGrp_Gob.transform.Find("Skills_VLG/Defensive_Grp/Counter_skill/Value_Txt").GetComponent<TMPro.TMP_Text>().text = "";

                    MinionSelectedListGrp_Gob.transform.Find("Skills_VLG/Defensive_Grp/Tackle_skill/Value_Txt").GetComponent<TMPro.TMP_Text>().text = "";

                    MinionSelectedListGrp_Gob.transform.Find("Skills_VLG/Defensive_Grp/Interception_pass_skill/Value_Txt").GetComponent<TMPro.TMP_Text>().text = "";

                    MinionSelectedListGrp_Gob.transform.Find("Skills_VLG/Defensive_Grp/Insterception_shot_skill/Value_Txt").GetComponent<TMPro.TMP_Text>().text = "";

                    MinionSelectedListGrp_Gob.transform.Find("Skills_VLG/Defensive_Grp/Defensive_duel_skill/Value_Txt").GetComponent<TMPro.TMP_Text>().text = "";

                    MinionSelectedListGrp_Gob.transform.Find("Skills_VLG/Defensive_Grp/Pressure_player_ball/Value_Txt").GetComponent<TMPro.TMP_Text>().text = "";

                    MinionSelectedListGrp_Gob.transform.Find("Skills_VLG/Offensive_Grp/Shot_skill/Value_Txt").GetComponent<TMPro.TMP_Text>().text = "";

                    MinionSelectedListGrp_Gob.transform.Find("Skills_VLG/Offensive_Grp/Dribble_skill/Value_Txt").GetComponent<TMPro.TMP_Text>().text = "";

                    MinionSelectedListGrp_Gob.transform.Find("Skills_VLG/Offensive_Grp/Offensive_duel_skill/Value_Txt").GetComponent<TMPro.TMP_Text>().text = "";
                }
            }
            else
            {

                MinionSelectedListGrp_Gob.transform.Find("Header_Player_Grp/Selected_Minion_Score_Txt").GetComponent<TMPro.TMP_Text>().text = "-";

                MinionSelectedListGrp_Gob.transform.Find("Skills_VLG/Defensive_Grp/Pressing_skill/Value_Txt").GetComponent<TMPro.TMP_Text>().text = "-";

                MinionSelectedListGrp_Gob.transform.Find("Skills_VLG/Defensive_Grp/Counter_skill/Value_Txt").GetComponent<TMPro.TMP_Text>().text = "-";

                MinionSelectedListGrp_Gob.transform.Find("Skills_VLG/Defensive_Grp/Tackle_skill/Value_Txt").GetComponent<TMPro.TMP_Text>().text = "-";

                MinionSelectedListGrp_Gob.transform.Find("Skills_VLG/Defensive_Grp/Interception_pass_skill/Value_Txt").GetComponent<TMPro.TMP_Text>().text = "-";

                MinionSelectedListGrp_Gob.transform.Find("Skills_VLG/Defensive_Grp/Insterception_shot_skill/Value_Txt").GetComponent<TMPro.TMP_Text>().text = "-";

                MinionSelectedListGrp_Gob.transform.Find("Skills_VLG/Defensive_Grp/Defensive_duel_skill/Value_Txt").GetComponent<TMPro.TMP_Text>().text = "-";

                MinionSelectedListGrp_Gob.transform.Find("Skills_VLG/Defensive_Grp/Pressure_player_ball/Value_Txt").GetComponent<TMPro.TMP_Text>().text = "-";

                MinionSelectedListGrp_Gob.transform.Find("Skills_VLG/Offensive_Grp/Long_pass_skill/Value_Txt").GetComponent<TMPro.TMP_Text>().text = "-";

                MinionSelectedListGrp_Gob.transform.Find("Skills_VLG/Offensive_Grp/Short_pass_skill/Value_Txt").GetComponent<TMPro.TMP_Text>().text = "-";

                MinionSelectedListGrp_Gob.transform.Find("Skills_VLG/Offensive_Grp/Shot_skill/Value_Txt").GetComponent<TMPro.TMP_Text>().text = "-";

                MinionSelectedListGrp_Gob.transform.Find("Skills_VLG/Offensive_Grp/Dribble_skill/Value_Txt").GetComponent<TMPro.TMP_Text>().text = "-";

                MinionSelectedListGrp_Gob.transform.Find("Skills_VLG/Offensive_Grp/Offensive_duel_skill/Value_Txt").GetComponent<TMPro.TMP_Text>().text = "-";

                //MinionSelectedListGrp_Gob.transform.Find("Precision_Points_Grp/Bubble_Grp/Form_Img/Form_Txt").GetComponent<TMPro.TMP_Text>().text = "-";

                MinionSelectedListGrp_Gob.transform.Find("Precision_Points_Grp/Bubble_Grp/Value_Txt").GetComponent<TMPro.TMP_Text>().text = "+ 0";
            }
        }

        public void UpdateTeamScore(bool pFuture = false, int pFutureScore = 0, bool pAnimate = true)
        {
            if(true == pFuture)
            {
                PrevisionScore = 0;

                for (int i = 0; i < MetaData.Instance.PlayersTeamSelected.Count; i++)
                {
                    PrevisionScore += MetaData.Instance.PlayersTeamSelected[i].Profile.Score;
                }

                PrevisionScore += pFutureScore;

                StartCoroutine(UpdateTeamScoreAnimation(PrevisionScore, true, pAnimate));
            }
            else
            {
                TeamScore = 0;

                for (int i = 0; i < MetaData.Instance.PlayersTeamSelected.Count; i++)
                {
                    TeamScore += MetaData.Instance.PlayersTeamSelected[i].Profile.Score;
               
                }
                StartCoroutine(UpdateTeamScoreAnimation(TeamScore, false, pAnimate)) ;
            }

        }

        private IEnumerator UpdateTeamScoreAnimation(int pScore, bool pFuture = false, bool pAnimate = true)
        {

            int lDiffMax = pScore - MaxScore; 


            RectTransform lRectYellowBar = TeamScore_Gob.transform.Find("Bar_Grp/YellowBar_Img").GetComponent<RectTransform>();
            RectTransform lRectBlueBar = TeamScore_Gob.transform.Find("Bar_Grp/BlueBar_Img").GetComponent<RectTransform>();
            RectTransform lRectBlueScore = TeamScore_Gob.transform.Find("Score_Txt").GetComponent<RectTransform>();
            if (true == pFuture)
            {
                this.transform.Find("AlertScorePopin_Grp/Popin_Grp/Team_Score_Grp/Score_Txt").GetComponent<TMPro.TMP_Text>().text = pScore.ToString();

                Sequence lSequenceYellowBar = DOTween.Sequence();
                float x = (float)((pScore * 355) / MaxScore);

                if (x > 355)
                {
                    x = 355;
                }

                lSequenceYellowBar.Insert(0, lRectYellowBar.DOSizeDelta(new Vector2(x, 23f), 0.3f));

                if (pScore > MaxScore)
                {
                    Color lGrayColor;
                    ColorUtility.TryParseHtmlString("#8B8B8B", out lGrayColor);
                    TeamScore_Gob.transform.Find("Max_Grp").gameObject.SetActive(true);
                    MinionsPlayersAssignDataBtn_Gob.transform.Find("Lock_Img").gameObject.SetActive(true);
                    MinionsPlayersAssignDataBtn_Gob.transform.GetComponent<Image>().color = lGrayColor;
                    MinionsPlayersAssignDataBtn_Gob.transform.Find("Validate_Txt").GetComponent<TMPro.TMP_Text>().color = lGrayColor;
                    AlertScorePopin_Gob.transform.Find("Popin_Grp/Team_Score_Grp/Score_Txt").GetComponent<TMPro.TMP_Text>().text = pScore.ToString();
                }
                else
                {

                    MinionsPlayersAssignDataBtn_Gob.transform.Find("Lock_Img").gameObject.SetActive(false);
                    MinionsPlayersAssignDataBtn_Gob.transform.GetComponent<Image>().color = Color.white;
                    MinionsPlayersAssignDataBtn_Gob.transform.Find("Validate_Txt").GetComponent<TMPro.TMP_Text>().color = Color.white;
                    
                }
            }
            else
            {

                if (Math.Abs(pScore - _lastScore) == 0)
                {
                    yield break;
                }


                Sequence lSequenceBlueBar = DOTween.Sequence();
                float x = (float)((pScore * 355) / MaxScore);

                if (x > 355)
                {
                    x = 355;
                    pScore = MaxScore;
                }


          
                if(true == pAnimate)
                {
                    lSequenceBlueBar.Insert(0, lRectBlueBar.DOSizeDelta(new Vector2(x, 23f), 2f));
                    lSequenceBlueBar.Insert(0, lRectBlueScore.DOAnchorPosX(x + 20f, 2f));
                    lSequenceBlueBar.Insert(0, lRectYellowBar.DOSizeDelta(new Vector2(x, 23f), 2f));


                    if (pScore - _lastScore > 100)
                    {
                        _lastScore = pScore + 100;
                    }
                    else if (pScore - _lastScore < 100)
                    {
                        _lastScore = pScore - 100;
                    }
                    float lTimer = 1 / Math.Abs(pScore - _lastScore);
                    while (true)
                    {

                        if (_lastScore < pScore)
                        {
                            _lastScore++;
                        }
                        else
                        {
                            _lastScore--;
                        }

                        TeamScore_Gob.transform.Find("Score_Txt").GetComponent<TMPro.TMP_Text>().text = _lastScore.ToString();

                        yield return new WaitForSeconds(lTimer);

                        if (_lastScore == pScore)
                        {
                            break;
                        }
                    }
                }
                else
                {
                    lRectBlueBar.sizeDelta = new Vector2(x, 23f);
                    lRectBlueScore.anchoredPosition = new Vector2(x + 20f, lRectBlueScore.anchoredPosition.y);
                    lRectYellowBar.sizeDelta = new Vector2(x, 23f);

                    TeamScore_Gob.transform.Find("Score_Txt").GetComponent<TMPro.TMP_Text>().text = pScore.ToString();

                }

               

            }


            if (lDiffMax > 0)
            {
                TeamScore_Gob.transform.Find("Max_Grp").gameObject.SetActive(true);
                TeamScore_Gob.transform.Find("Max_Grp/ScoreMaxValue_Txt").GetComponent<TMPro.TMP_Text>().text = "+ " + lDiffMax;
            }
            else
            {
                TeamScore_Gob.transform.Find("Max_Grp").gameObject.SetActive(false);
            }




        
   
        }

        public void UpdateTeamPlayersField()
        {
            // this.transform.Find("Field/Selected_Minion_In_The_Field_Pos").GetComponent<CanvasGroup>().alpha = 0;
            GameObject[] allMinionsOnFieldArray_Gob = GameObject.FindGameObjectsWithTag("Player_Team_Minions");

            if(null != allMinionsOnFieldArray_Gob)
            {
                foreach (GameObject catch_Var in allMinionsOnFieldArray_Gob)
                {
                    if(null != catch_Var.GetComponent<MinionsPlayersItem>())
                    {
                        catch_Var.GetComponent<MinionsPlayersItem>().Init();
                    }
                }

                DefaultPositionSelected();
            }


        }

        public void DefaultPositionSelected()
        {
            GameObject[] allMinionsOnFieldArray_Gob = GameObject.FindGameObjectsWithTag("Player_Team_Minions");

            int i = 0;
            int lIndex = 0;
            foreach (GameObject catch_Var in allMinionsOnFieldArray_Gob)
            {
                if (catch_Var.transform.name == LastSquadPositionFieldSelected)
                {
                    lIndex = i;
                }
                i++;
            }

            allMinionsOnFieldArray_Gob[lIndex].GetComponent<MinionsPlayersItem>().OnPointerClick(null);
        }

        public void UpdateTeamPlayersScrollView()
        {
            MetaData.Instance.MinionScrollviewPlayerIdSelected = null;
            LoadData(MetaData.Instance.MinionFieldSquadPositionSelected);
            foreach (MinionsPlayersScrollviewItem catch_Var in PlayersScrollGrp_Gob.GetComponentsInChildren<MinionsPlayersScrollviewItem>())
            {
                catch_Var.GetComponent<MinionsPlayersScrollviewItem>().UpdateUI();
            }
        }

        private PlayersItem GetPlayersItem(string pPosition)
        {
            foreach (PlayersItem item in MetaData.Instance.PlayersTeamSelected)
            {
                if (item.Profile.SquadPosition == pPosition)
                {
                    return item;
                }
            }

            return null;
        }

        #endregion


        void Awake()
        {
            Instance = this;
        }


    }
}
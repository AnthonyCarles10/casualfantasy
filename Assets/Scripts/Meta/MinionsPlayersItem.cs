﻿using System.Collections;
using DG.Tweening;
using Sportfaction.CasualFantasy.Players;
using Sportfaction.CasualFantasy.Services;


using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Sportfaction.CasualFantasy.Meta
{
    /// <summary>
    /// Description: Minions Players Scrollview Item
    /// Author: Antoine de Lachèze-Murel
    /// </summary>
    public class MinionsPlayersItem : MonoBehaviour, IPointerClickHandler
    {
        public PlayersItem Player;

        public Vector3 InitPosition;

        public Vector3 InitPositionOutPlayer;

        public GameObject Empty_Gob;
        
        public GameObject Player_Gob;

        #region Public Methods

        /// <summary>
        /// Init to set data
        /// </summary>
        public int Init()
        {
            if (this.transform.parent.parent.transform.name != "Field")
            {
                return 0; // Player Out of Field
            }

            this.transform.GetComponent<CanvasGroup>().alpha = 1;

            this.transform.GetComponent<Image>().raycastTarget = true;


            Player = GetPlayersItem();

 

            if (null != Player)
            {
                Sprite[] lSpritesMenu = Resources.LoadAll<Sprite>("Sprites/Meta/Menu_2_SS");
                // Sprite[] lSpritesGameplay = Resources.LoadAll<Sprite>("Sprites/Gameplay/gameplay");
                switch (MetaData.Instance.FormLevelLetter[Player.Profile.FormLevel])
                {
                    case "A":
                        Player_Gob.transform.Find("PlayerForm/ScoreForm_Img").GetComponent<Image>().sprite = UtilityMethods.GetSprite(lSpritesMenu, "Form_Flame_Player_A");
                        //Player_Gob.transform.Find("PlayerForm/ScoreForm_Img").GetComponent<Image>().SetNativeSize();
                        //RectTransform lRect = Player_Gob.transform.Find("PlayerForm/Form_Txt").GetComponent<RectTransform>();
                        //lRect.anchoredPosition = new Vector3(0, -3.5f, 0);
                        //Player_Gob.transform.Find("PlayerScore/Score_Txt").GetComponent<TMPro.TMP_Text>().color = Color.white;
                        //Color lColor; ColorUtility.TryParseHtmlString("#838383", out lColor);
                        //Player_Gob.transform.Find("PlayerForm/Form_Txt").GetComponent<TMPro.TMP_Text>().color = lColor;
                        //Player_Gob.transform.Find("PlayerForm/Form_Txt").GetComponent<TMPro.TMP_Text>().fontSize = 29;
                        break;
                    case "B":
                        Player_Gob.transform.Find("PlayerForm/ScoreForm_Img").GetComponent<Image>().sprite = UtilityMethods.GetSprite(lSpritesMenu, "Form_Flame_Player_B");
                        break;
                    case "C":
                        Player_Gob.transform.Find("PlayerForm/ScoreForm_Img").GetComponent<Image>().sprite = UtilityMethods.GetSprite(lSpritesMenu, "Form_Flame_Player_C");
                        break;
                    case "D":
                        Player_Gob.transform.Find("PlayerForm/ScoreForm_Img").GetComponent<Image>().sprite = UtilityMethods.GetSprite(lSpritesMenu, "Form_Flame_Player_D");
                        break;
                }

                Player_Gob.transform.Find("Shirt").GetComponent<Image>().sprite = UtilityMethods.GetSprite(lSpritesMenu, "Shirt_Rarity_" + (Player.Profile.RareLevel + 1) + "_Bck");

                Player_Gob.SetActive(true);

                Empty_Gob.SetActive(false);

               // Player_Gob.transform.Find("PlayerForm/Form_Txt").GetComponent<TMPro.TMP_Text>().text = Data.Instance.FormLevelLetter[Player.Profile.FormLevel];

                Player_Gob.transform.Find("PlayerName/Name_Txt").GetComponent<TMPro.TMP_Text>().text = Player.Profile.NameField;

                Player_Gob.transform.Find("PlayerScore/Score_Txt").GetComponent<TMPro.TMP_Text>().text = Player.Profile.Score.ToString();

                // Player_Gob.transform.GetComponent<Image>().sprite = UtilityMethods.GetSprite(lSpritesMenu, "TeamPlayer");

                return Player.Profile.Score;
            }
            else
            {
                Player_Gob.SetActive(false);

                Empty_Gob.SetActive(true);

                Empty_Gob.transform.Find("SquadPosition_Txt").GetComponent<TMPro.TMP_Text>().text = this.transform.name;
                
                //this.transform.GetComponent<Image>().sprite = UtilityMethods.GetSprite(lSpritesMenu, "Player_Inactived");

                return 0;
            }
        }

        public void SetDataWithSelectedPlayer(PlayersItem pPlayer)
        {
            // Sprite[] lSpritesMenu = Resources.LoadAll<Sprite>("Sprites/Meta/Menu_SS");

            if (null != pPlayer)
            {

                Player_Gob.SetActive(true);

                Empty_Gob.SetActive(false);

                Empty_Gob.transform.Find("SquadPosition_Txt").GetComponent<TMPro.TMP_Text>().text = pPlayer.Profile.SquadPosition;

                Player_Gob.transform.Find("PlayerName/Name_Txt").GetComponent<TMPro.TMP_Text>().text = pPlayer.Profile.NameField;

                Player_Gob.transform.Find("PlayerScore/Score_Txt").GetComponent<TMPro.TMP_Text>().text = pPlayer.Profile.Score.ToString();

                // this.transform.GetComponent<Image>().sprite = UtilityMethods.GetSprite(lSpritesMenu, "TeamPlayer");
            }
            else
            {
                Player_Gob.SetActive(true);

                Empty_Gob.SetActive(true);

                //this.transform.GetComponent<Image>().sprite = UtilityMethods.GetSprite(lSpritesMenu, "Player_Inactived");

                Empty_Gob.transform.Find("SquadPosition_Txt").GetComponent<TMPro.TMP_Text>().text = MetaData.Instance.MinionFieldSquadPositionSelected;
            }

            Player = pPlayer;
        }

        public void OnPointerClick(PointerEventData pointerEventData)
        {
            MinionsPlayers.Instance.PrevisionScore = 0;
            //LineRenderer lLine = MinionsPlayers.Instance.LineRenderer_Gob.GetComponent<LineRenderer>();


            //Vector2 adjustedPosition = MinionsPlayers.Instance.Camera.ScreenToWorldPoint(transform.position);
            //float lCropLeftReso1920x1080 = 0;
            //RectTransform CanvasRect = UI.Instance.GetComponent<RectTransform>();

            //Vector2 adjustedPosition = MinionsPlayers.Instance.Camera.WorldToViewportPoint(transform.GetComponent<RectTransform>().position);

            //adjustedPosition = new Vector2(adjustedPosition.x / UI.Instance.GetComponent<CanvasScaler>().scaleFactor, adjustedPosition.y / UI.Instance.GetComponent<CanvasScaler>().scaleFactor);


            //RectTransform LineRenderRectTransform = MinionsPlayers.Instance.LineRenderer_Gob.GetComponent<RectTransform>();
            //MinionsPlayers.Instance.LineRenderer_Gob.GetComponent<RectTransform>().localPosition = new Vector3(lCropLeftReso1920x1080, LineRenderRectTransform.localPosition.y, LineRenderRectTransform.localPosition.z);


            //Debug.Log(Screen.currentResolution.width / Screen.currentResolution.height);
            //Debug.Log(1920 / 1080);
            //Debug.Log(lCropLeftReso1920x1080);
            //adjustedPosition.x += lCropLeftReso1920x1080;


            //adjustedPosition.x *= UI.Instance.GetComponent<CanvasScaler>().referenceResolution.x / (float)MinionsPlayers.Instance.Camera.pixelWidth;

            //adjustedPosition.y *= UI.Instance.GetComponent<CanvasScaler>().referenceResolution.y / (float)MinionsPlayers.Instance.Camera.pixelHeight;

            if (MinionsPlayers.Instance.CurrentState_Str == "Idle")
            {
                //adjustedPosition.x -= 510f;
                //adjustedPosition.y += 56f;

                MinionsPlayers.Instance.CurrentState_Str = "MinionSelected";

                StartCoroutine(AnimateLineRenderer());
            }
            else
            {
                PlaceLR();
            }

            switch (this.transform.name)
            {
                case "ACD":
                case "ACG":
                    MetaData.Instance.MinionFieldPositionSelected = "A";
                    //adjustedPosition.x += 102f;
                    break;
                case "MCG":
                case "MCD":
                case "MD":
                case "MG":
                    MetaData.Instance.MinionFieldPositionSelected = "M";
                    //adjustedPosition.x += 110f;
                    break;
                case "DCG":
                case "DCD":
                case "DD":
                case "DG":
                    MetaData.Instance.MinionFieldPositionSelected = "D";
                    //adjustedPosition.x += 120f;
                    break;
                case "G":
                    MetaData.Instance.MinionFieldPositionSelected = "G";
                    //adjustedPosition.x += 185f;
                    break;
            }

            //lLine.SetPosition(0, adjustedPosition);

            if (null != Player)
            {
                MetaData.Instance.MinionFieldPositionSelected = Player.Profile.GlobalPosition.ToString();
                MinionsPlayers.Instance.RemoveBtn_Gob.SetActive(true);
                MetaData.Instance.MinionFieldSelected = Player;
            }
            else
            {
                MetaData.Instance.MinionFieldSelected = null;
                MinionsPlayers.Instance.RemoveBtn_Gob.SetActive(false);
            }

            if (this.transform.name != MetaData.Instance.MinionFieldSquadPositionSelected)
            {
                MetaData.Instance.QueryParams["position"] = MetaData.Instance.MinionFieldPositionSelected;
                MetaData.Instance.QueryParams["offset"] = "0";
                MinionsPlayers.Instance.FilterApiAndAnimation();
                MinionsPlayers.Instance.UpdateTeamScore(true, 0);
                MinionsPlayers.Instance.PreviousFilterPosition = MetaData.Instance.MinionFieldPositionSelected + "_Btn";
                Sprite[] lSpritesMenu = Resources.LoadAll<Sprite>("Sprites/Meta/Menu_2_SS");
                
                MinionsPlayers.Instance.PlayersScrollGrp_Gob.transform.Find("FilterPopin_Grp/Popin_Grp/G_Btn").GetComponent<Image>().sprite = UtilityMethods.GetSprite(lSpritesMenu, "Button_Filter_Off");
                MinionsPlayers.Instance.PlayersScrollGrp_Gob.transform.Find("FilterPopin_Grp/Popin_Grp/D_Btn").GetComponent<Image>().sprite = UtilityMethods.GetSprite(lSpritesMenu, "Button_Filter_Off");
                MinionsPlayers.Instance.PlayersScrollGrp_Gob.transform.Find("FilterPopin_Grp/Popin_Grp/M_Btn").GetComponent<Image>().sprite = UtilityMethods.GetSprite(lSpritesMenu, "Button_Filter_Off");
                MinionsPlayers.Instance.PlayersScrollGrp_Gob.transform.Find("FilterPopin_Grp/Popin_Grp/A_Btn").GetComponent<Image>().sprite = UtilityMethods.GetSprite(lSpritesMenu, "Button_Filter_Off");
                MinionsPlayers.Instance.PlayersScrollGrp_Gob.transform.Find("FilterPopin_Grp/Popin_Grp/" + MetaData.Instance.MinionFieldPositionSelected + "_Btn").GetComponent<Image>().sprite = UtilityMethods.GetSprite(lSpritesMenu, "Button_Filter_On");
            }

            MetaData.Instance.MinionFieldSquadPositionSelected = this.transform.name;
            MinionsPlayers.Instance.LastSquadPositionFieldSelected = this.transform.name;

            MinionsPlayers.Instance.LoadData(this.transform.name);
            MinionsPlayers.Instance.MinionSelectedFieldGrp_Gob.SetActive(true);
            MinionsPlayers.Instance.MinionSelectedFieldGrp_Gob.GetComponent<CanvasGroup>().alpha = 1;
            MinionsPlayers.Instance.MinionSelectedListGrp_Gob.SetActive(false);
            MinionsPlayers.Instance.MinionUnselectedGrp_Gob.SetActive(false);
            MinionsPlayers.Instance.MinionsPlayersAssignDataBtn_Gob.SetActive(false);
            MinionsPlayers.Instance.LineRendererScroll_Gob.SetActive(false);


            MetaData.Instance.MinionScrollviewSelected = null;
            MetaData.Instance.MinionScrollviewPlayerIdSelected = null;
            MinionsPlayersScrollview.Instance.SelectedMinion = null;
            MinionsPlayersScrollview.Instance.SelectedMinionPlayerId = null;

            GameObject[] allMinionsOnFieldArray_Gob = GameObject.FindGameObjectsWithTag("Player_Team_Minions");

            foreach (GameObject catch_Var in allMinionsOnFieldArray_Gob)
            {
                //catch_Var.GetComponent<Image>().raycastTarget = false;
                catch_Var.transform.Find("Selected_Bkg").gameObject.SetActive(false);
            }

            this.transform.Find("Selected_Bkg").gameObject.SetActive(true);

        

            Sequence lSequence = DOTween.Sequence();
            lSequence.Insert(0, MinionsPlayers.Instance.Field_Gob.GetComponent<RectTransform>().DOAnchorPos(new Vector2(-510f, 86f), 0.5f));
        }

        private PlayersItem GetPlayersItem()
        {
            foreach (PlayersItem item in MetaData.Instance.PlayersTeamSelected)
            {
                if(item.Profile.SquadPosition == this.transform.name)
                {
                    return item;
                }
            }

            return null;
        }

        private IEnumerator AnimateLineRenderer()
        {
            yield return new WaitForSeconds(0.5f); // wait animation

            PlaceLR();
            MinionsPlayers.Instance.LineRenderer_Gob.SetActive(true);

        }

        private void Start()
        {
            InitPosition = this.transform.position;
            InitPositionOutPlayer = this.transform.position;
        }

        #endregion

        #region Private Methods

        private void    PlaceLR()
        {
            LineRenderer    lLine       =   MinionsPlayers.Instance.LineRenderer_Gob.GetComponent<LineRenderer>();
            Vector3 adjustedPosition    =   transform.position;

            switch (this.transform.name)
            {
                case "ACD":
                case "ACG":
                    adjustedPosition.x += 0.05f;
                    break;
                case "MCG":
                case "MCD":
                case "MD":
                case "MG":
                    adjustedPosition.x += 0.03f;
                    break;
                case "DCG":
                case "DCD":
                case "DD":
                case "DG":
                    adjustedPosition.x += 0.03f;
                    break;
                case "G":
                    adjustedPosition.x += 0.075f;
                    break;
            }

            lLine.SetPosition(0, adjustedPosition);
            lLine.SetPosition(1, MinionsPlayers.Instance.MinionSelectedHeader_Gob.transform.position + new Vector3(-0.035f, 0f, 0f));
            lLine.transform.position    =   adjustedPosition;
        }

        #endregion
    }
}
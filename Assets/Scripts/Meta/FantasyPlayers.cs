﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using DG.Tweening;
using Newtonsoft.Json.Linq;
using Photon.Pun;
using Sportfaction.CasualFantasy.Events;
using Sportfaction.CasualFantasy.Leaderboard;
using Sportfaction.CasualFantasy.Manager.Profile;
using Sportfaction.CasualFantasy.Players;
using Sportfaction.CasualFantasy.PVP.Gameplay.Matchs;
using Sportfaction.CasualFantasy.PVP.Network;
using Sportfaction.CasualFantasy.Services;
using SportFaction.CasualFootballEngine.PhaseService.Enum;
using SportFaction.CasualFootballEngine.PlayerService.Enum;
using SportFaction.CasualFootballEngine.TeamService.Enum;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Sportfaction.CasualFantasy.Meta
{
    /// <summary>
    /// Description: FantasyPlayers
    /// Author: Antoine de Lachèze-Murel
    /// </summary>
    public sealed class FantasyPlayers : MonoBehaviour
    {
        public static FantasyPlayers Instance;

        #region Public Methods

        public GameObject SearchPopin_Gob;
        public GameObject FilterPopin_Gob;
        public GameObject PlayersScrollGrp_Gob;
        public GameObject StatsPlayerVLG_Gob;
        public GameObject StatHLG_Prefab;

        public GameObject TeamAGoalsVLG_Gob;
        public GameObject TeamBGoalsVLG_Gob;
        public GameObject PlayerGoalHLG_Prefab;

        public string CurrentState_Str = "Idle";

        public Material RobotoBold;

        public Material RobotoBoldWithoutStroke;

        public GameObject LoaderGrp_Gob;



        private string _previousFilterSkill = "";
        private string _previousFilterPosition = "";
        private bool _ownedFilter = true;
        private bool _nameFilter = false;
        private string _nameFilterValue = "";

        private Transform _currentTransform;



        private bool _showAlertScorePopin = false;

        private int _lastScore = 0;

        private Color _greenColor;
        private Color _redColor;

        private int _nbStats = 0;
        private int _totalSkills = 0;

        private int _totalGoalsTeamA = 0;
        private int _totalGoalsTeamB = 0;




        #endregion

        #region Public Methods

        void Start()
        {
            ColorUtility.TryParseHtmlString("#8CFF08", out _greenColor);

            ColorUtility.TryParseHtmlString("#E2322A", out _redColor);
        }

        //public void OnGetPlayerFantasyList()
        //{
        //    PlayersItem lPlayer = Data.Instance._playersMain.PlayersFantasyList.Items[0];
        //    lPl
        //}

        public void GetPlayerLastGame()
        {

            DefaultUIAnimation.Instance.DeleteAllChilds(StatsPlayerVLG_Gob);

            DefaultUIAnimation.Instance.DeleteAllChilds(TeamAGoalsVLG_Gob);

            DefaultUIAnimation.Instance.DeleteAllChilds(TeamBGoalsVLG_Gob);

            _nbStats = 0;

            _totalSkills = 0;

            _totalGoalsTeamA = 0;

            _totalGoalsTeamB = 0;

            this.transform.Find("PlayerInfos_Grp/Header_Grp/Position_Txt").GetComponent<TMPro.TMP_Text>().text = I2.Loc.LocalizationManager.GetTranslation(MetaData.Instance.FantasyPlayerScrollviewSelected.Profile.GlobalPosition.ToString() + "long");
            this.transform.Find("PlayerInfos_Grp/Header_Grp/Name_Txt").GetComponent<TMPro.TMP_Text>().text = MetaData.Instance.FantasyPlayerScrollviewSelected.Profile.Name;
            this.transform.Find("PlayerInfos_Grp/Header_Grp/Club_Txt").GetComponent<TMPro.TMP_Text>().text = MetaData.Instance.FantasyPlayerScrollviewSelected.Profile.TeamName;
            this.transform.Find("PlayerInfos_Grp/Header_Grp/Score_Txt").GetComponent<TMPro.TMP_Text>().text = MetaData.Instance.FantasyPlayerScrollviewSelected.Profile.Score.ToString();
     


            this.transform.Find("PlayerInfos_Grp/LastGame_Grp/MatchDate_Txt").GetComponent<TMPro.TMP_Text>().text = DateExtension.ConvertDateTimeToDate(MetaData.Instance.PlayersMain.PlayerLastGame.GameData.Date);
            this.transform.Find("PlayerInfos_Grp/LastGame_Grp/NextMatchDate_Txt").GetComponent<TMPro.TMP_Text>().text = I2.Loc.LocalizationManager.GetTranslation("next_real_match_label") + DateExtension.ConvertDateTimeToDate(MetaData.Instance.PlayersMain.PlayerLastGame.NextGameData.Date);


            // GOALS LAST GAME
            foreach (PlayerLastGameDataGoal item in MetaData.Instance.PlayersMain.PlayerLastGame.GameData.Goals)
            {
                if(item.OfficialTeamId == MetaData.Instance.PlayersMain.PlayerLastGame.GameData.TeamAId)
                {
                    InitGoalPrefab(TeamAGoalsVLG_Gob, item);
                    _totalGoalsTeamA++;
                }
                else
                {
                    InitGoalPrefab(TeamBGoalsVLG_Gob, item);
                    _totalGoalsTeamB++;
                }
           
            }

            TeamAGoalsVLG_Gob.GetComponent<RectTransform>().sizeDelta = new Vector2(TeamAGoalsVLG_Gob.GetComponent<RectTransform>().sizeDelta.x, _totalGoalsTeamA * PlayerGoalHLG_Prefab.GetComponent<RectTransform>().sizeDelta.y);
            TeamBGoalsVLG_Gob.GetComponent<RectTransform>().sizeDelta = new Vector2(TeamBGoalsVLG_Gob.GetComponent<RectTransform>().sizeDelta.x, _totalGoalsTeamB * PlayerGoalHLG_Prefab.GetComponent<RectTransform>().sizeDelta.y);


            // DEFEND
            InitStatPrefab("goalsConcOnfield_rp", MetaData.Instance.PlayersMain.PlayerLastGame.Skills.GoalsConcOnfield);
            InitStatPrefab("tackle_rp", MetaData.Instance.PlayersMain.PlayerLastGame.Skills.Tackle);
            InitStatPrefab("ballRecovery_rp", MetaData.Instance.PlayersMain.PlayerLastGame.Skills.BallRecovery);
            InitStatPrefab("dispossessed_rp", MetaData.Instance.PlayersMain.PlayerLastGame.Skills.Dispossessed);
            InitStatPrefab("goals_rp", MetaData.Instance.PlayersMain.PlayerLastGame.Skills.Goals);
            InitStatPrefab("keyPass_rp", MetaData.Instance.PlayersMain.PlayerLastGame.Skills.KeyPass);
            InitStatPrefab("goalAssist_rp", MetaData.Instance.PlayersMain.PlayerLastGame.Skills.GoalAssist);
            InitStatPrefab("accuratePassTr3_rp", MetaData.Instance.PlayersMain.PlayerLastGame.Skills.AccuratePassTr3);
            InitStatPrefab("wonContest_rp", MetaData.Instance.PlayersMain.PlayerLastGame.Skills.WonContest);

            InitStatPrefab("accurateCrossNocorner_rp", MetaData.Instance.PlayersMain.PlayerLastGame.Skills.AccurateCrossNocorner);
            InitStatPrefab("shotOffTarget_rp", MetaData.Instance.PlayersMain.PlayerLastGame.Skills.ShotOffTarget);
            InitStatPrefab("bigChanceMissed_rp", MetaData.Instance.PlayersMain.PlayerLastGame.Skills.BigChanceMissed);
            InitStatPrefab("redCard_rp", MetaData.Instance.PlayersMain.PlayerLastGame.Skills.RedCard);
            InitStatPrefab("yellowCard_rp", MetaData.Instance.PlayersMain.PlayerLastGame.Skills.YellowCard);

            if (MetaData.Instance.PlayersMain.PlayerLastGame.Skills.MinsPlayed1 == 0)
            {
                InitStatPrefab("minsPlayed1_rp", MetaData.Instance.PlayersMain.PlayerLastGame.Skills.MinsPlayed1);
            }
            else
            {
                InitStatPrefab("noMinsPlayed_rp", MetaData.Instance.PlayersMain.PlayerLastGame.Skills.NoMinsPlayed);
            }
            InitStatPrefab("totalOffside_rp", MetaData.Instance.PlayersMain.PlayerLastGame.Skills.TotalOffside);
            InitStatPrefab("fouls_rp", MetaData.Instance.PlayersMain.PlayerLastGame.Skills.Fouls);
            InitStatPrefab("saves_rp", MetaData.Instance.PlayersMain.PlayerLastGame.Skills.Saves);


            StatsPlayerVLG_Gob.GetComponent<RectTransform>().sizeDelta = new Vector2(StatsPlayerVLG_Gob.GetComponent<RectTransform>().sizeDelta.x, _nbStats * StatHLG_Prefab.GetComponent<RectTransform>().sizeDelta.y);


            this.transform.Find("PlayerInfos_Grp/LastGame_Grp/MatchName_Txt").GetComponent<TMPro.TMP_Text>().text = I2.Loc.LocalizationManager.GetTranslation(MetaData.Instance.PlayersMain.PlayerLastGame.GameData.ChampionshipName);
            this.transform.Find("PlayerInfos_Grp/LastGame_Grp/MatchDate_Txt").GetComponent<TMPro.TMP_Text>().text = MetaData.Instance.PlayersMain.PlayerLastGame.GameData.Date;
            this.transform.Find("PlayerInfos_Grp/LastGame_Grp/TeamAName_Txt").GetComponent<TMPro.TMP_Text>().text = MetaData.Instance.PlayersMain.PlayerLastGame.GameData.TeamAName;
            this.transform.Find("PlayerInfos_Grp/LastGame_Grp/MatchScore_Txt").GetComponent<TMPro.TMP_Text>().text = MetaData.Instance.PlayersMain.PlayerLastGame.GameData.TeamAScore.ToString() + " - " + MetaData.Instance.PlayersMain.PlayerLastGame.GameData.TeamBScore.ToString();
            this.transform.Find("PlayerInfos_Grp/LastGame_Grp/TeamBName_Txt").GetComponent<TMPro.TMP_Text>().text = MetaData.Instance.PlayersMain.PlayerLastGame.GameData.TeamBName;


            //this.transform.Find("PlayerInfos_Grp/Form_Grp/Fire_Img/Title_Txt").GetComponent<TMPro.TMP_Text>().text = "-";
            //this.transform.Find("PlayerInfos_Grp/Form_Grp/CurrentValue_Grp/Title_Txt").GetComponent<TMPro.TMP_Text>().text = Data.Instance.PlayersMain.PlayerLastGame.GameData.Form.ToString();


            this.transform.Find("PlayerInfos_Grp/Form_Grp/Form_Bar_Grp/lv4_Img").gameObject.SetActive(false);
            this.transform.Find("PlayerInfos_Grp/Form_Grp/Form_Bar_Grp/lv4_Img").transform.GetComponent<RectTransform>().sizeDelta = new Vector2(191f, 74f);
            this.transform.Find("PlayerInfos_Grp/Form_Grp/Form_Bar_Grp/lv3_Img").gameObject.SetActive(false);
            this.transform.Find("PlayerInfos_Grp/Form_Grp/Form_Bar_Grp/lv3_Img").transform.GetComponent<RectTransform>().sizeDelta = new Vector2(191f, 74f);
            this.transform.Find("PlayerInfos_Grp/Form_Grp/Form_Bar_Grp/lv2_Img").gameObject.SetActive(false);
            this.transform.Find("PlayerInfos_Grp/Form_Grp/Form_Bar_Grp/lv2_Img").transform.GetComponent<RectTransform>().sizeDelta = new Vector2(191f, 74f);
            this.transform.Find("PlayerInfos_Grp/Form_Grp/Form_Bar_Grp/lv1_Img").gameObject.SetActive(false);
            this.transform.Find("PlayerInfos_Grp/Form_Grp/Form_Bar_Grp/lv1_Img").transform.GetComponent<RectTransform>().sizeDelta = new Vector2(191f, 74f);

            int lForm = MetaData.Instance.FantasyPlayerScrollviewSelected.Profile.Form;

            this.transform.Find("PlayerInfos_Grp/Form_Grp/CurrentValue_Grp/Title_Txt").GetComponent<TMPro.TMP_Text>().text = lForm.ToString();
            //this.transform.Find("PlayerInfos_Grp/Form_Grp/Fire_Img/Title_Txt").GetComponent<TMPro.TMP_Text>().text = Data.Instance.FormLevelLetter[Data.Instance.FantasyPlayerScrollviewSelected.Profile.FormLevel];


            Sprite[] lSpritesMenu = Resources.LoadAll<Sprite>("Sprites/Meta/Menu_2_SS");
            // Sprite[] lSpritesGameplay = Resources.LoadAll<Sprite>("Sprites/Gameplay/gameplay");
            switch (MetaData.Instance.FormLevelLetter[MetaData.Instance.FantasyPlayerScrollviewSelected.Profile.FormLevel])
            {
                case "A":
                    this.transform.Find("PlayerInfos_Grp/Form_Grp/Form_Img").GetComponent<Image>().sprite = UtilityMethods.GetSprite(lSpritesMenu, "Form_Flame_Player_A");
                    break;
                case "B":
                    this.transform.Find("PlayerInfos_Grp/Form_Grp/Form_Img").GetComponent<Image>().sprite = UtilityMethods.GetSprite(lSpritesMenu, "Form_Flame_Player_B");
                    break;
                case "C":
                    this.transform.Find("PlayerInfos_Grp/Form_Grp/Form_Img").GetComponent<Image>().sprite = UtilityMethods.GetSprite(lSpritesMenu, "Form_Flame_Player_C");
                    break;
                case "D":
                    this.transform.Find("PlayerInfos_Grp/Form_Grp/Form_Img").GetComponent<Image>().sprite = UtilityMethods.GetSprite(lSpritesMenu, "Form_Flame_Player_D");
                    break;
            }

            if (lForm < 22)
            {
                int lSizePercent = (int)System.Math.Ceiling((double)((lForm) * 100 / (22)));

                //Debug.Log("FORM = " + lForm + " / PERCENT = " + lSizePercent + " / (191f * lSizePercent) = " + (191f * lSizePercent *0.01f));
                Vector2 lVector2 = new Vector2(191f * lSizePercent * 0.01f, 74f);
                this.transform.Find("PlayerInfos_Grp/Form_Grp/Form_Bar_Grp/lv1_Img").gameObject.SetActive(true);
                this.transform.Find("PlayerInfos_Grp/Form_Grp/Form_Bar_Grp/lv1_Img").transform.GetComponent<RectTransform>().sizeDelta = lVector2;
               
                Vector3 lVector3 = new Vector3(-35f + ((144f + 35f) * lSizePercent * 0.01f), 9f, 0);
                this.transform.Find("PlayerInfos_Grp/Form_Grp/CurrentValue_Grp").transform.GetComponent<RectTransform>().anchoredPosition = lVector3;
            }
            if (lForm >= 22)
            {
                this.transform.Find("PlayerInfos_Grp/Form_Grp/Form_Bar_Grp/lv1_Img").gameObject.SetActive(true);
                if(lForm < 78)
                {
                    int lSizePercent = (int)System.Math.Ceiling((double)((lForm - 22) * 100 / (78 - 22)));
                    //Debug.Log("FORM = " + lForm + " / PERCENT = " + lSizePercent + " / (191f * lSizePercent) = " + (191f * lSizePercent * 0.01f));
                    Vector2 lVector2 = new Vector2(191f * lSizePercent * 0.01f, 74f);
                    this.transform.Find("PlayerInfos_Grp/Form_Grp/Form_Bar_Grp/lv2_Img").gameObject.SetActive(true);
                    this.transform.Find("PlayerInfos_Grp/Form_Grp/Form_Bar_Grp/lv2_Img").transform.GetComponent<RectTransform>().sizeDelta = lVector2;

                    //Debug.Log("144f + " + ((289f - 144f) * lSizePercent * 0.01f));
                    Vector3 lVector3 = new Vector3(144f + ((289f - 144f) * lSizePercent * 0.01f), 9f, 0);
                    this.transform.Find("PlayerInfos_Grp/Form_Grp/CurrentValue_Grp").transform.GetComponent<RectTransform>().anchoredPosition = lVector3;
                }
            }

            if (lForm >= 77)
            {
                this.transform.Find("PlayerInfos_Grp/Form_Grp/Form_Bar_Grp/lv2_Img").gameObject.SetActive(true);
                if (lForm < 145)
                {
                    int lSizePercent = (int)System.Math.Ceiling((double)((lForm - 77) * 100 / (145 - 77)));
                    //Debug.Log("FORM = " + lForm + " / PERCENT = " + lSizePercent + " / (191f * lSizePercent) = " + (191f * lSizePercent * 0.01f));
                    Vector2 lVector2 = new Vector2(191f * lSizePercent * 0.01f, 74f);
                    this.transform.Find("PlayerInfos_Grp/Form_Grp/Form_Bar_Grp/lv3_Img").gameObject.SetActive(true);
                    this.transform.Find("PlayerInfos_Grp/Form_Grp/Form_Bar_Grp/lv3_Img").transform.GetComponent<RectTransform>().sizeDelta = lVector2;

                    //Debug.Log("289f + " + ((435f - 289f) * lSizePercent * 0.01f));
                    Vector3 lVector3 = new Vector3(289f + ((435f - 289f) * lSizePercent * 0.01f), 9f, 0);
                    this.transform.Find("PlayerInfos_Grp/Form_Grp/CurrentValue_Grp").transform.GetComponent<RectTransform>().anchoredPosition = lVector3;
                }
            }

            if (lForm >= 144)
            {
                this.transform.Find("PlayerInfos_Grp/Form_Grp/Form_Bar_Grp/lv3_Img").gameObject.SetActive(true);

                if (lForm <= 170)
                {
                    int lSizePercent = (int)System.Math.Ceiling((double)((lForm - 144) * 100 / (170 - 144)));
                    //Debug.Log("FORM = " + lForm + " / PERCENT = " + lSizePercent + " / (191f * lSizePercent) = " + (191f * lSizePercent * 0.01f));
                    Vector2 lVector2 = new Vector2(191f * lSizePercent * 0.01f, 74f);
                    this.transform.Find("PlayerInfos_Grp/Form_Grp/Form_Bar_Grp/lv4_Img").gameObject.SetActive(true);
                    this.transform.Find("PlayerInfos_Grp/Form_Grp/Form_Bar_Grp/lv4_Img").transform.GetComponent<RectTransform>().sizeDelta = lVector2;

                    //Debug.Log("435f + " + ((573f - 435f) * lSizePercent * 0.01f));
                    Vector3 lVector3 = new Vector3(435f + ((573f - 435f) * lSizePercent * 0.01f), 9f, 0);
                    this.transform.Find("PlayerInfos_Grp/Form_Grp/CurrentValue_Grp").transform.GetComponent<RectTransform>().anchoredPosition = lVector3;

                }
            }

            if (lForm > 170)
            {
                Vector3 lVector3 = new Vector3(573f, 9f, 0);
                this.transform.Find("PlayerInfos_Grp/Form_Grp/CurrentValue_Grp").transform.GetComponent<RectTransform>().anchoredPosition = lVector3;
                this.transform.Find("PlayerInfos_Grp/Form_Grp/Form_Bar_Grp/lv4_Img").gameObject.SetActive(true);
            }


                this.transform.Find("PlayerInfos_Grp/FantasyPoints_Grp/Total_Txt").transform.GetComponent<TMPro.TMP_Text>().text = _totalSkills.ToString();
            this.transform.Find("PlayerInfos_Grp/FantasyPoints_Grp/GlobalRp_Txt").transform.GetComponent<TMPro.TMP_Text>().text = I2.Loc.LocalizationManager.GetTranslation("global_real_points_label") + MetaData.Instance.PlayersMain.PlayerLastGame.RealPointGlobal.ToString();
            this.transform.Find("PlayerInfos_Grp/Form_Grp/Label_Txt").GetComponent<TMPro.TMP_Text>().text = String.Format(I2.Loc.LocalizationManager.GetTranslation("fantasy_form_max_score_label"), MetaData.Instance.FormLevelPrecisionPoints[MetaData.Instance.FantasyPlayerScrollviewSelected.Profile.FormLevel]);


        }

        private void InitStatPrefab(string pLabel, int pValue, bool pIsGreen = true)
        {
            if(pValue != 0)
            {
                GameObject lPrefab = Instantiate(StatHLG_Prefab);
                lPrefab.name = pLabel;
                lPrefab.transform.Find("Value_Txt").GetComponent<TMPro.TMP_Text>().text = I2.Loc.LocalizationManager.GetTranslation(pLabel) + " = " + pValue.ToString();
                lPrefab.transform.Find("Dot_Img").GetComponent<Image>().color = pValue > 0 ? _greenColor : _redColor;
                lPrefab.transform.SetParent(StatsPlayerVLG_Gob.transform, false);

                _totalSkills += pValue;

                _nbStats++;
            }
           
        }

        private void InitGoalPrefab(GameObject pParent, PlayerLastGameDataGoal pGoal)
        {
            GameObject lPrefab = Instantiate(PlayerGoalHLG_Prefab);
            lPrefab.transform.Find("Name_Txt").GetComponent<TMPro.TMP_Text>().text = pGoal.NameField;
            if (pGoal.Score > 1)
            {
                lPrefab.transform.Find("Time_Txt").GetComponent<TMPro.TMP_Text>().text = pGoal.Score.ToString();
            }
            else
            {
                lPrefab.transform.Find("Time_Txt").GetComponent<TMPro.TMP_Text>().text = "";
            }
       
            lPrefab.transform.SetParent(pParent.transform, false);
        }

     


        public IEnumerator OpenSearchByNamePopin()
        {
            yield return new WaitForSeconds(0.1f); // wait button animation

            if (true == _nameFilter)
            {
                StartCoroutine(FilterByName());
            }
            else
            {
                SearchPopin_Gob.SetActive(true);
                SearchPopin_Gob.transform.Find("Popin_Grp").transform.localPosition = new Vector3(0, 0, 0);
                SearchPopin_Gob.transform.Find("Popin_Grp").GetComponent<Animator>().SetTrigger("Open");
            }
          
        }

        public IEnumerator CloseSearchByNamePopin()
        {
            yield return new WaitForSeconds(0.1f); // wait button animation

            SearchPopin_Gob.SetActive(true);
            SearchPopin_Gob.transform.Find("Popin_Grp").GetComponent<Animator>().SetTrigger("Close");

            yield return new WaitForSeconds(0.15f); // wait popin close animation

            SearchPopin_Gob.SetActive(false);
        }

        public IEnumerator OpenFilterPopin()
        {
            yield return new WaitForSeconds(0.1f); // wait button animation

            FilterPopin_Gob.SetActive(true);
            FilterPopin_Gob.transform.Find("Popin_Grp").transform.localPosition = new Vector3(0,0,0);
            FilterPopin_Gob.transform.Find("Popin_Grp").GetComponent<Animator>().SetTrigger("Open");
        }

        public IEnumerator CloseFilterPopin()
        {
            yield return new WaitForSeconds(0.1f); // wait button animation

            FilterPopin_Gob.SetActive(true);
            FilterPopin_Gob.transform.Find("Popin_Grp").GetComponent<Animator>().SetTrigger("Close");

            yield return new WaitForSeconds(0.15f); // wait popin close animation

            FilterPopin_Gob.SetActive(false);
        }

      
        public IEnumerator ResetFilter()
        {
            yield return new WaitForSeconds(0f); // wait button animation

            Sprite[] lSpritesMenu = Resources.LoadAll<Sprite>("Sprites/Meta/Menu_2_SS");

            MetaData.Instance.QueryParamsFantasy["order_by"] = "score";
            MetaData.Instance.QueryParamsFantasy["position"] = "";

            if (false == String.IsNullOrEmpty(_previousFilterSkill))
            {
                if (_previousFilterSkill == "Score_Btn")
                {
                    FilterPopin_Gob.transform.Find("Popin_Grp/" + _previousFilterSkill).GetComponent<Image>().sprite = UtilityMethods.GetSprite(lSpritesMenu, "Button_Filter_Off");
                }
                else
                {
                    this.transform.Find("FilterPopin_Grp/Popin_Grp/" + _previousFilterSkill).GetComponent<Image>().sprite = UtilityMethods.GetSprite(lSpritesMenu, "Button_Filter_Off");
                }

                FilterPopin_Gob.transform.Find("Popin_Grp/Score_Btn").GetComponent<Image>().sprite = UtilityMethods.GetSprite(lSpritesMenu, "Button_Filter_On");

                _previousFilterSkill = "";
            }

            if (false == String.IsNullOrEmpty(_previousFilterPosition))
            {
                if (false == String.IsNullOrEmpty(_previousFilterPosition))
                {
                    FilterPopin_Gob.transform.Find("Popin_Grp/" + _previousFilterPosition).GetComponent<Image>().sprite = UtilityMethods.GetSprite(lSpritesMenu, "Button_Filter_Off");
                }
                _previousFilterPosition = "";
            }

            MetaData.Instance.Filter = true;
            MetaData.Instance.GetPlayersList();

            StartCoroutine(CloseFilterPopin());
        }

        public IEnumerator ValidateFilter()
        {
            yield return new WaitForSeconds(0f); // wait button animation

            if (false == String.IsNullOrEmpty(_previousFilterSkill))
            {
                string lFilter = _previousFilterSkill.Substring(0, _previousFilterSkill.Length - 4);

                if("Score" == lFilter)
                {
                    MetaData.Instance.QueryParamsFantasy["order_by"] = "score";
                }
                else
                {
                    MetaData.Instance.QueryParamsFantasy["order_by"] = lFilter.ToLower() + "_skill";
                }
           
            }
            else
            {
                MetaData.Instance.QueryParamsFantasy["order_by"] = "score";
            }

            if (false == String.IsNullOrEmpty(_previousFilterPosition))
            {
                string lFilter = _previousFilterPosition.Substring(8, 1);

                MetaData.Instance.QueryParamsFantasy["position"] = lFilter;

            }
            else
            {
                MetaData.Instance.QueryParamsFantasy["position"] = "";
            }

            if(true == _ownedFilter)
            {
                MetaData.Instance.QueryParamsFantasy["is_owned"] = "1";
            }
            else
            {
                MetaData.Instance.QueryParamsFantasy["is_owned"] = "-1";
            }

            if(true == _nameFilter)
            {
                _nameFilterValue = Regex.Replace(_nameFilterValue, "\u200B", "");
                MetaData.Instance.QueryParamsFantasy["keyword"] = _nameFilterValue;
            }
            else
            {
                MetaData.Instance.QueryParamsFantasy["keyword"] = "";
            }

            MetaData.Instance.QueryParamsFantasy["offset"] = "0";


            FilterApiAndAnimation();

        }

        public void FilterApiAndAnimation()
        {
            MetaData.Instance.FilterFantasy = true;
            MetaData.Instance.GetPlayersFantasyList();

            MetaData.Instance.FantasyPlayerScrollviewSelected = null;

            Sequence lSequence = DOTween.Sequence();
            lSequence.Insert(0, PlayersScrollGrp_Gob.GetComponent<RectTransform>().DOAnchorPosY(-320f, 0.3f));//0f
            lSequence.Insert(0.5f, PlayersScrollGrp_Gob.GetComponent<RectTransform>().DOAnchorPosY(0, 0.5f));//0f
        }

        public IEnumerator FilterBySkill(string button_Str)
        {
            yield return new WaitForSeconds(0.1f); // wait button animation


            Sprite[] lSpritesMenu = Resources.LoadAll<Sprite>("Sprites/Meta/Menu_2_SS");
            if(button_Str == "Score_Btn")
            {
                FilterPopin_Gob.transform.Find("Popin_Grp/" + button_Str).GetComponent<Image>().sprite = UtilityMethods.GetSprite(lSpritesMenu, "Bouton_Filter_On");
            }
            else
            {
                FilterPopin_Gob.transform.Find("Popin_Grp/" + button_Str).GetComponent<Image>().sprite = UtilityMethods.GetSprite(lSpritesMenu, "Bouton_Filter_On");
            }
  

            if(false == String.IsNullOrEmpty(_previousFilterSkill))
            {
                if (_previousFilterSkill == "Score_Btn")
                {
                    FilterPopin_Gob.transform.Find("Popin_Grp/" + _previousFilterSkill).GetComponent<Image>().sprite = UtilityMethods.GetSprite(lSpritesMenu, "Bouton_Filter_Off");
                }
                else
                {
                    FilterPopin_Gob.transform.Find("Popin_Grp/" + _previousFilterSkill).GetComponent<Image>().sprite = UtilityMethods.GetSprite(lSpritesMenu, "Bouton_Filter_Off");
                }
               
            }

            if(_previousFilterSkill == button_Str)
            {
                _previousFilterSkill = "";
            }
            else
            {
                _previousFilterSkill = button_Str;
            }
        }

        public IEnumerator FilterByPosition(string button_Str)
        {
            yield return new WaitForSeconds(0.1f); // wait button animation

            Sprite[] lSpritesMenu = Resources.LoadAll<Sprite>("Sprites/Meta/Menu_2_SS");


            if (false == String.IsNullOrEmpty(_previousFilterPosition) && _previousFilterPosition == button_Str)
            {
                FilterPopin_Gob.transform.Find("Popin_Grp/" + _previousFilterPosition).GetComponent<Image>().sprite = UtilityMethods.GetSprite(lSpritesMenu, "Bouton_Filter_Off");
            }
            else
            {
                FilterPopin_Gob.transform.Find("Popin_Grp/" + button_Str).GetComponent<Image>().sprite = UtilityMethods.GetSprite(lSpritesMenu, "Bouton_Filter_On");
            }
    
            if (false == String.IsNullOrEmpty(_previousFilterPosition))
            {
                FilterPopin_Gob.transform.Find("Popin_Grp/" + _previousFilterPosition).GetComponent<Image>().sprite = UtilityMethods.GetSprite(lSpritesMenu, "Bouton_Filter_Off");
            }

            if (_previousFilterPosition == button_Str)
            {
                _previousFilterPosition = "";
            }
            else
            {
                _previousFilterPosition = button_Str;
            }

            //StartCoroutine(CloseFilterPopin());
        }

        public IEnumerator FilterByOwnedPlayers()
        {
            yield return new WaitForSeconds(0.1f); // wait button animation
            Sprite[] lSpritesMenu = Resources.LoadAll<Sprite>("Sprites/Meta/Menu_2_SS");

            if (false == _ownedFilter)
            {
                PlayersScrollGrp_Gob.transform.Find("Filter_Grp/Popin_Grp/FantasyOwnedPlayers_Btn").GetComponent<Image>().sprite = UtilityMethods.GetSprite(lSpritesMenu, "Button_Filter_On");
                _ownedFilter = true;
            }
            else
            {
                PlayersScrollGrp_Gob.transform.Find("Filter_Grp/Popin_Grp/FantasyOwnedPlayers_Btn").GetComponent<Image>().sprite = UtilityMethods.GetSprite(lSpritesMenu, "Button_Filter_Off");
                _ownedFilter = false;
            }
            StartCoroutine(ValidateFilter());
        }

        public IEnumerator FilterByName()
        {
            yield return new WaitForSeconds(0.1f); // wait button animation
            Sprite[] lSpritesMenu = Resources.LoadAll<Sprite>("Sprites/Meta/Menu_2_SS");

            if (false == _nameFilter)
            {
                _nameFilter = true;
                PlayersScrollGrp_Gob.transform.Find("Filter_Grp/Popin_Grp/FantasySearchByName_Btn").GetComponent<Image>().sprite = UtilityMethods.GetSprite(lSpritesMenu, "Bouton_Filter_On");
                _nameFilterValue = SearchPopin_Gob.transform.Find("Popin_Grp/Search_Input_Grp/Search_Input_Mask/Value_Txt").GetComponent<TMPro.TMP_Text>().text;
                StartCoroutine(CloseSearchByNamePopin());
            }
            else
            {
                _nameFilter = false;
                PlayersScrollGrp_Gob.transform.Find("Filter_Grp/Popin_Grp/FantasySearchByName_Btn").GetComponent<Image>().sprite = UtilityMethods.GetSprite(lSpritesMenu, "Bouton_Filter_Off");
                SearchPopin_Gob.transform.Find("Popin_Grp/Search_Input_Grp/Search_Input_Mask/Value_Txt").GetComponent<TMPro.TMP_Text>().text = "";
                _nameFilterValue = "";
            }

           
            StartCoroutine(ValidateFilter());
        }
        


        public void UpdateTeamPlayersScrollView()
        {
            MetaData.Instance.MinionScrollviewPlayerIdSelected = null;

            foreach (MinionsPlayersScrollviewItem catch_Var in PlayersScrollGrp_Gob.GetComponentsInChildren<MinionsPlayersScrollviewItem>())
            {
                catch_Var.GetComponent<MinionsPlayersScrollviewItem>().UpdateUI();
            }
        }

        private PlayersItem GetPlayersItem(string pPosition)
        {
            foreach (PlayersItem item in MetaData.Instance.PlayersTeamSelected)
            {
                if (item.Profile.SquadPosition == pPosition)
                {
                    return item;
                }
            }

            return null;
        }

        #endregion


        void Awake()
        {
            Instance = this;
        }


    }
}
﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;

namespace Sportfaction.CasualFantasy.Menu
{
	/// <summary>
	/// Description: Class handling pvp loading background
	/// Author: Rémi Carreira
	/// </summary>
	[RequireComponent(typeof(Graphic))]
	public class PVPLoadingBackground : MonoBehaviour
	{
		#region Fields


		[SerializeField, Tooltip("Animate pvp loading background on start")]
		private	bool			_onStart				=	true;
		[SerializeField, MinValue(0f), Tooltip("Duration of the animation")]
		private	float			_duration				=	1f;
		[SerializeField, Tooltip("End value of the anchor position")]
		private	Vector2			_endAnchorPosition		=	Vector2.zero;
		[SerializeField, Tooltip("Transform of the background")]
		private	RectTransform	_backgroundTransform	=	null;
		#endregion

		#region Public Methods

		/// <summary>
		/// Method used to animate background
		/// </summary>
		public	void	AnimateBackground()
		{
			Assert.IsNotNull(_backgroundTransform, "[PVPLoadingBackground] AnimateBackground(), _backgroundTransform is null.");

			_backgroundTransform.DOAnchorPos(_endAnchorPosition, _duration);
		}

		#endregion

		#region Private Methods

		/// <summary>
		/// Monoebehaviour method called once when this object is enable
		/// </summary>
		private	void	Start ()
		{
			if (true == _onStart)
			{
				AnimateBackground();
			}
		}

		#endregion
	}
}
﻿using Sportfaction.CasualFantasy.PVP.Gameplay.Players;

using UnityEngine;

namespace Sportfaction.CasualFantasy.References.CustomClass
{
    /// <summary>
    /// Description: Reference to a PVP player
    /// Author: Madeleine Tranier
    /// </summary>
    [CreateAssetMenu(fileName = "New PVP Player Reference", menuName = "CasualFantasy/References/CustomClass/PlayerPVP")]
	public sealed class PlayerPVPReference : AReference<PlayerPVP>
	{
	}
}
﻿using UnityEngine;

namespace Sportfaction.CasualFantasy.References.CustomClass
{
	[System.Serializable]
	public	struct	ActionPoints
	{
		[SerializeField, ReadOnly, Tooltip("Current point of the specific team")]
		public	ushort	Points;
		[SerializeField, ReadOnly, Tooltip("Amount of filling")]
		public	float	FillAmount;
	}
	
	/// <summary>
	/// Description: Reference to manager action points data
	/// Author: Madeleine Tranier
	/// </summary>
	[CreateAssetMenu(fileName = "New Action Points Reference", menuName = "CasualFantasy/References/CustomClass/ActionPoints")]
	public	sealed	class	ActionPointsReference : AReference<ActionPoints>
	{
	}
}
﻿using UnityEngine;

namespace Sportfaction.CasualFantasy.References.CustomClass
{
	[System.Serializable]
	public	struct	ExperienceReward
	{
		[SerializeField, ReadOnly, Tooltip("Is manager vip ?")]
		public	bool	IsVIP;
		[SerializeField, ReadOnly, Tooltip("Experience earned with this match.")]
		public	int		ExperienceEarned;
		[SerializeField, ReadOnly, Tooltip("Bonus of experiences won")]
		public	int		ExperienceBonus;
	}
	
	/// <summary>
	/// Description: Reference to manager experience reward data
	/// Author: Madeleine Tranier
	/// </summary>
	[CreateAssetMenu(fileName = "New Experience Reward Reference", menuName = "CasualFantasy/References/CustomClass/ExperienceReward")]
	public	sealed	class	ExperienceRewardReference : AReference<ExperienceReward>
	{
	}
}
﻿using SportFaction.CasualFootballEngine.PlayerService.Enum;

using System.Collections.Generic;
using UnityEngine;

namespace Sportfaction.CasualFantasy.References.CustomClass
{
	[System.Serializable]
	public	struct	SoftCurrencyReward
	{
		[SerializeField, ReadOnly, Tooltip("Is handling manager A reward?")]
		public	bool		ManagerAReward;
		[SerializeField, ReadOnly, Tooltip("Total soft currency earned ")]
		public	int			TotalSoftCurrency;
		[SerializeField, ReadOnly, Tooltip("Soft currency earned by this manager")]
		public	int			SoftCurrencyEarned;
		[SerializeField, ReadOnly, Tooltip("Bonus of soft currency according to goals")]
		public	int			GoalsSoftCurrency;
		[SerializeField, ReadOnly, Tooltip("Bonus of soft currency according to vip status")]
		public	int			VIPSoftCurrency;
		[SerializeField, ReadOnly, Tooltip("Reward amount for each player")]
		public	KeyValuePair<ePosition, int>[]	PlayersReward;
	}
	
	/// <summary>
	/// Description: Reference to manager soft currency reward data
	/// Author: Madeleine Tranier
	/// </summary>
	[CreateAssetMenu(fileName = "New Soft Currency Reward Reference", menuName = "CasualFantasy/References/CustomClass/SoftCurrencyReward")]
	public	sealed	class	SoftCurrencyRewardReference : AReference<SoftCurrencyReward>
	{
	}
}
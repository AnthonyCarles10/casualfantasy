﻿using Sportfaction.CasualFantasy.PVP.Gameplay.History;

using System.Collections.Generic;
using UnityEngine;

namespace Sportfaction.CasualFantasy.References.CustomClass
{
	/// <summary>
	/// Description: Reference an array of histor event
	/// Author: Rémi Carreira
	/// </summary>
	[CreateAssetMenu(fileName = "New History Events Ref", menuName = "CasualFantasy/References/CustomClass/PVPHistoryEvents")]
	public sealed class PVPHistoryEventsReference : AReference<List<HistoryEvent>>
	{
	}
}
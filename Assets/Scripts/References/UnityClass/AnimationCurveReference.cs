﻿using UnityEngine;

namespace Sportfaction.CasualFantasy.References.UnityClass
{
	/// <summary>
	/// Description: Reference to an animation curve
	/// Author: Rémi Carreira
	/// </summary>
	[CreateAssetMenu(fileName = "New Animation Curve Ref", menuName = "CasualFantasy/References/UnityClass/AnimationCurve")]
	public sealed class AnimationCurveReference : AReference<AnimationCurve>
	{
	}
}
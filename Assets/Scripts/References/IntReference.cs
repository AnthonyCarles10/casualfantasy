﻿using UnityEngine;

namespace Sportfaction.CasualFantasy.References
{
	/// <summary>
	/// Description: Reference used to contain an integer
	/// Author: Rémi Carreira
	/// </summary>
	[CreateAssetMenu(fileName = "New Int Reference", menuName = "CasualFantasy/References/Int")]
	public sealed class IntReference : AReference<int>
	{
	}
}
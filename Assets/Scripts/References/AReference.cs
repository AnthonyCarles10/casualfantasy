﻿using UnityEngine;

namespace Sportfaction.CasualFantasy.References
{
	/// <summary>
	/// Description: Scriptable object used to contain any type of value
	/// Author: Rémi Carreira
	/// </summary>
	public abstract class AReference<T> : ScriptableObject
	{
		#region Properties

		public	T	DefaultValue	{ get { return _defaultValue; } }	// Property used to get the default value

		#endregion

		#region Fields

		[SerializeField, Tooltip("Current value of type T")]
		public	T	CurrentValue	=	default(T);
		[SerializeField, Tooltip("Default value of type T")]
		private	T	_defaultValue	=	default(T);

		#endregion

		#region Private Methods

		/// <summary>
		/// Scriptable object method used to initialize current value
		/// </summary>
		private	void	OnEnable()
		{
			CurrentValue	=	_defaultValue;
		}

		#endregion
	}
}
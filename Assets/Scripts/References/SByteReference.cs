﻿using UnityEngine;

namespace Sportfaction.CasualFantasy.References
{
	/// <summary>
	/// Description: Reference used to contain a sbyte
	/// Author: Rémi Carreira
	/// </summary>
	[CreateAssetMenu(fileName = "New SByte Reference", menuName = "CasualFantasy/References/SByte")]
	public sealed class SByteReference : AReference<sbyte>
	{
	}
}
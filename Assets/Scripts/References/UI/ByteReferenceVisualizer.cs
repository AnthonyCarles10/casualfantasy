﻿namespace Sportfaction.CasualFantasy.References.UI
{
	/// <summary>
	/// Description: Show a byte reference value
	/// Author: Rémi Carreira
	/// </summary>
	public sealed class ByteReferenceVisualizer : AReferenceVisualizer<ByteReference, byte>
	{
	}
}
﻿using UnityEngine;
using UnityEngine.Assertions;

namespace Sportfaction.CasualFantasy.References.UI
{
	/// <summary>
	/// Description: Show an int reference value
	/// Author: Rémi Carreira
	/// </summary>
	public sealed class IntReferenceVisualizer : AReferenceVisualizer<IntReference, int>
	{
		#region Fields

		[SerializeField, Tooltip("Text used to display the bool reference's value")]
		private	bool	_convert	=	false;	// Whether to convert value or not

		#endregion

		#region Public Methods

		/// <summary>
		/// Update the text used to visualize a specific reference
		/// </summary>
		public override	void	UpdateText()
		{
			Assert.IsNotNull(_uiText, "IntReferenceVisualizer: UpdateText() failed, _");

			Assert.IsNotNull(_ref, "IntReferenceVisualizer: UpdateText() failed, _ref is null.");
			
			if (_convert)
			{
				_uiText.text	=	(_ref.CurrentValue / 1000 + 1).ToString();
			}
			else
			{
				_uiText.text	=	_ref.CurrentValue.ToString();
			}
		}

		#endregion
	}
}
﻿namespace Sportfaction.CasualFantasy.References.UI
{
	/// <summary>
	/// Description: Show an uint reference value
	/// Author: Rémi Carreira
	/// </summary>
	public sealed class UIntReferenceVisualizer : AReferenceVisualizer<UIntReference, uint>
	{
	}
}
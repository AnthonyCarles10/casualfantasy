﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Assertions;

namespace Sportfaction.CasualFantasy.References.UI
{
	/// <summary>
	/// Description: Show a specific reference value
	/// Author: Rémi Carreira
	/// </summary>
	[RequireComponent(typeof(Text))]
	public abstract class AReferenceVisualizer<T, U> : MonoBehaviour where T : AReference<U>
	{
		#region Properties
		
		public	T	Ref	{ set { _ref = value; } }
		
		#endregion
		
		#region Fields

		[SerializeField, Tooltip("Text used to display the bool reference's value")]
		protected	Text	_uiText	=	null;	// Text used to display the bool reference's value
		[SerializeField, Tooltip("Reference to a boolean")]
		protected	T		_ref	=	null;	// Reference to a value (boolean, byte, float, etc)
		[SerializeField, Tooltip("Define if text is updated when this gameObject is OnEnable")]
		protected	bool	_updateTextOnEnable		=	false;	// Define if text is updated when this gameObject is OnEnable
		[SerializeField, Tooltip("Define if text is updated each frame")]
		protected	bool	_updateTextEachFrame	=	false;	// Define if text is updated each frame

		#endregion

		#region Public Methods

		/// <summary>
		/// Update the text used to visualize a specific reference
		/// </summary>
		public	virtual	void	UpdateText()
		{
			Assert.IsNotNull(_uiText, "AReferenceVisualizer: UpdateText() failed, _");

			Assert.IsNotNull(_ref, "AReferenceVisualizer: UpdateText() failed, _ref is null.");

			_uiText.text	=	_ref.CurrentValue.ToString();
		}

		#endregion

		#region Protected Methods

		/// <summary>
		/// MonoBehaviour method used if the text need to be updated
		/// </summary>
		protected	virtual	void	OnEnable()
		{
			if (true == _updateTextOnEnable)
			{
				UpdateText();
			}
		}

		/// <summary>
		/// MonoBehaviour method used if the text have to be updated text each frame
		/// </summary>
		protected	virtual	void	Update()
		{
			if (true == _updateTextEachFrame)
			{
				UpdateText();
			}
		}

		#endregion
	}
}
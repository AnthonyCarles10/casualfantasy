﻿using UnityEngine;
using UnityEngine.Assertions;

namespace Sportfaction.CasualFantasy.References.UI
{
	/// <summary>
	/// Description: Show a float reference value
	/// Author: Rémi Carreira
	/// </summary>
	public sealed class FloatReferenceVisualizer : AReferenceVisualizer<FloatReference, float>
	{
		[SerializeField, Tooltip("Formatting the floating value")]
		private	string	_stringFormat	=	"0.00"; //Formatting the floating value

		[SerializeField, Tooltip("Convert value before displaying")]
		private	bool	_convert		=	false;

		#region Public Methods

		public override void UpdateText()
		{
			Assert.IsNotNull(_uiText, "FloatReferenceVisualizer: UpdateText() failed, _");

			Assert.IsNotNull(_ref, "FloatReferenceVisualizer: UpdateText() failed, _ref is null.");

			if (_convert)
			{
				ConvertAndUpdateText();
			}
			else
			{
				if ("0" == _stringFormat)
				{
					_uiText.text	=	Mathf.Floor(_ref.CurrentValue).ToString(_stringFormat);
				}
				else
				{
					_uiText.text	=	_ref.CurrentValue.ToString(_stringFormat);
				}
			}
		}
		
		public void ConvertAndUpdateText()
		{
			Assert.IsNotNull(_uiText, "FloatReferenceVisualizer: UpdateText() failed, _");

			Assert.IsNotNull(_ref, "FloatReferenceVisualizer: UpdateText() failed, _ref is null.");

			if ("0" == _stringFormat)
			{
				_uiText.text	=	Mathf.Floor(_ref.CurrentValue / 1000f).ToString(_stringFormat);
			}
			else
			{
				_uiText.text	=	(_ref.CurrentValue / 1000f).ToString(_stringFormat);
			}
		}

		#endregion
	}
}
﻿namespace Sportfaction.CasualFantasy.References.UI
{
	/// <summary>
	/// Description: Show a boolean reference value
	/// Author: Rémi Carreira
	/// </summary>
	public sealed class BoolReferenceVisualizer : AReferenceVisualizer<BoolReference, bool>
	{
	}
}
﻿using UnityEngine.Assertions;

namespace Sportfaction.CasualFantasy.References.UI
{
	/// <summary>
	/// Description: Show string reference value
	/// Author: Rémi Carreira
	/// </summary>
	public sealed class StringReferenceVisualizer : AReferenceVisualizer<StringReference, string>
	{
		#region Public Methods

		public override void UpdateText()
		{
			Assert.IsNotNull(_uiText, "StringReferenceVisualize: UpdateText() failed, _uiText is null.");

			_uiText.text	=	_ref.CurrentValue;
		}

		#endregion
	}
}
﻿namespace Sportfaction.CasualFantasy.References.UI
{
	/// <summary>
	/// Description: Show sbyte reference value
	/// Author: Rémi Carreira
	/// </summary>
	public sealed class SByteReferenceVisualizer : AReferenceVisualizer<SByteReference, sbyte>
	{
	}
}
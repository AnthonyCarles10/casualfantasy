﻿using SportFaction.CasualFootballEngine.TeamService.Model;

using UnityEngine;

namespace Sportfaction.CasualFantasy.References.Engine
{
	/// <summary>
	/// Description: Reference a team in casual football engine
	/// Author: Rémi Carreira
	/// </summary>
	[CreateAssetMenu(fileName = "New Team Reference", menuName = "CasualFantasy/References/Engine/Team")]
	public sealed class TeamReference : AReference<Team>
	{
	}
}

﻿using SportFaction.CasualFootballEngine.TeamService.Enum;

using UnityEngine;

namespace Sportfaction.CasualFantasy.References.Engine
{
	/// <summary>
	/// Description: Reference a team possessing the ball in a soccer match
	/// Author: Rémi Carreira
	/// </summary>
	[CreateAssetMenu(fileName = "New Team Possession Reference", menuName = "CasualFantasy/References/Engine/Possession/Team")]
	public sealed class TeamPossessionReference : AReference<eTeam>
	{
	}
}

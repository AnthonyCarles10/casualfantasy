﻿using UnityEngine;

namespace Sportfaction.CasualFantasy.References
{
	/// <summary>
	/// Description: Reference used to contain a boolean
	/// Author: Rémi Carreira
	/// </summary>
	[CreateAssetMenu(fileName = "New Bool Reference", menuName = "CasualFantasy/References/Bool")]
	public sealed class BoolReference : AReference<bool>
	{
	}
}
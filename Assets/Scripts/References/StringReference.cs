﻿using UnityEngine;

namespace Sportfaction.CasualFantasy.References
{
	/// <summary>
	/// Description: Reference used to contain a string
	/// Author: Rémi Carreira
	/// </summary>
	[CreateAssetMenu(fileName = "New string reference", menuName = "CasualFantasy/References/String")]
	public sealed class StringReference : AReference<string>
	{
	}
}
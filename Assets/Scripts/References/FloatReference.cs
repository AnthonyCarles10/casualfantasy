﻿using UnityEngine;

namespace Sportfaction.CasualFantasy.References
{
	/// <summary>
	/// Description: Reference used to contain a float
	/// Author: Rémi Carreira
	/// </summary>
	[CreateAssetMenu(fileName = "New Float Reference", menuName = "CasualFantasy/References/Float")]
	public sealed class FloatReference : AReference<float>
	{
	}
}
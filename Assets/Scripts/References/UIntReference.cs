﻿using UnityEngine;

namespace Sportfaction.CasualFantasy.References
{
	/// <summary>
	/// Description: Reference used to contain an unsigned int
	/// Author: Rémi Carreira
	/// </summary>
	[CreateAssetMenu(fileName = "New UInt Reference", menuName = "CasualFantasy/References/UInt")]
	public sealed class UIntReference : AReference<uint>
	{
	}
}
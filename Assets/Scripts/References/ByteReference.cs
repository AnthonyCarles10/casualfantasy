﻿using UnityEngine;

namespace Sportfaction.CasualFantasy.References
{
	/// <summary>
	/// Description: Reference used to contain a byte
	/// Author: Rémi Carreira
	/// </summary>
	[CreateAssetMenu(fileName = "New Byte Reference", menuName = "CasualFantasy/References/Byte")]
	public sealed class ByteReference : AReference<byte>
	{
	}
}
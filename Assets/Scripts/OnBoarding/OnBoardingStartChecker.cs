﻿using Sportfaction.CasualFantasy.Config;
using Sportfaction.CasualFantasy.Manager.Profile;

using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Events;

namespace Sportfaction.CasualFantasy.OnBoarding
{
	/// <summary>
	/// Description: Class check onBoardingStart
	/// Author: Rémi Carreira
	/// </summary>
	public class OnBoardingStartChecker : MonoBehaviour
	{
		#region Fields

		[SerializeField, Tooltip("Response to invoke if onBoardingStart")]
		private	UnityEvent	_onBoardingStartResponse		=	new UnityEvent();
		[SerializeField, Tooltip("Response to invoke if onBoardingDontStart")]
		private	UnityEvent	_onBoardingDontStartResponse	=	new UnityEvent();

		#endregion

		#region Private Methods

		/// <summary>
		/// Monobehaviour method used to check onBoardingStart
		/// </summary>
		private void	Start()
		{
            if (null != UserData.Instance)
			{
                if (null == ConfigData.Instance || true == UserData.Instance.Profile.OnBoarding)
				{
					Assert.IsNotNull("[OnBoardingStartChecker] Start(), _onBoardingStartResponse is null.");

					_onBoardingStartResponse.Invoke();
				}
				else
				{
					Assert.IsNotNull("[OnBoardingStartChecker] Start(), _onBoardingDontStartResponse is null.");

					_onBoardingDontStartResponse.Invoke();
				}
			}
		}

		#endregion
	}
}

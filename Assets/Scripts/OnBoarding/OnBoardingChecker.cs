﻿using Sportfaction.CasualFantasy.Events;

using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.SceneManagement;

namespace Sportfaction.CasualFantasy
{
    /// <summary>
    /// Description: Class check if all data was received from API
    /// Author: Madeleine Tranier
    /// </summary>
    public sealed class OnBoardingChecker : MonoBehaviour
    {
        #region Fields

		[Header("Informations")]
		[SerializeField, ReadOnly, Tooltip("Boolean used to know if match structure was received")]
        private bool	_matchStructureReceived		= false;
		[SerializeField, ReadOnly, Tooltip("Boolean used to know if all players of team A was received")]
        private bool	_teamAPlayersReceived		= false;
		[SerializeField, ReadOnly, Tooltip("Boolean used to know if composition of team A was received")]
        private bool	_teamACompositionReceived	= false;
		[SerializeField, ReadOnly, Tooltip("Boolean used to know if all players of team B was received")]
        private bool	_teamBPlayersReceived		= false;
		[SerializeField, ReadOnly, Tooltip("Boolean used to know if composition of team B was received")]
        private bool	_teamBCompositionReceived	= false;

        #endregion

        #region Public Methods

        /// <summary>
        /// Callback when match structure was received
        /// </summary>
        public void OnMatchStructureReceived()
        {
            _matchStructureReceived = true;
            
			CheckCurrentManagerReady();
        }

        /// <summary>
        /// Callback when team A players was received
        /// </summary>
        public void OnTeamAPlayersReceived()
        {
            _teamAPlayersReceived = true;

			CheckCurrentManagerReady();
        }

        /// <summary>
        /// Callback when team A composition was received
        /// </summary>
        public void OnTeamACompositionReceived()
        {
            _teamACompositionReceived = true;

			CheckCurrentManagerReady();
        }

        /// <summary>
        /// Callback when team B players was received
        /// </summary>
        public void OnTeamBPlayersReceived()
        {
            _teamBPlayersReceived = true;

			CheckCurrentManagerReady();
        }

        /// <summary>
        /// Callback when team B composition was received
        /// </summary>
        public void OnTeamBCompositionReceived()
        {
            _teamBCompositionReceived = true;

			CheckCurrentManagerReady();
        }

		#endregion

		#region Private Methods

		/// <summary>
		/// Check if current manager is ready
		/// </summary>
		private	void	CheckCurrentManagerReady()
		{
			if (true == IsAllDataReceived())
			{
				SceneManager.LoadScene("OnBoardingGameplay");
			}
		}

		/// <summary>
		/// Invoke a specific game event
		/// </summary>
		/// <param name="pEvent">Event to invoke</param>
		private	void	_invokeGameEvent(GameEvent pEvent)
		{
			Assert.IsNotNull(pEvent, "[PreMatchChecker] InvokeEvent(), pEvent is null.");

			pEvent.Invoke();
		}

		/// <summary>
		/// Check if all data was received
		/// </summary>
		/// <returns>True if all data was received</returns>
		private bool IsAllDataReceived()
		{
			return (true == _matchStructureReceived &&
					true == _teamAPlayersReceived &&
					true == _teamACompositionReceived &&
					true == _teamBPlayersReceived &&
					true == _teamBCompositionReceived);
		}

		#endregion

	}
}
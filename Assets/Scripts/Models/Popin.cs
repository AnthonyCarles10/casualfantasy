﻿using UnityEngine;
using UnityEngine.UI;

namespace Sportfaction.CasualFantasy
{
    /// <summary>
    /// Class handling popin
    /// </summary>
    /// @author Madeleine Tranier
    public class Popin : MonoBehaviour
	{
		#region Variables

		public delegate void OnPopinClose();
		public event OnPopinClose OnClose;

		public GameObject PopinContainer;    // Popin
		public Text PopinMessage;            // Message to display in popin
		public Button ButtonClose;

		#endregion

		#region Public Methods

		public void SetMessage(string pMessage)
		{
			PopinMessage.text = pMessage;
		}

		public void OnClosePopin()
		{
			PopinContainer.SetActive(false);
			Destroy(PopinContainer);
			if (OnClose != null)
			{
				OnClose();
			}
		}

		#endregion
	}
}
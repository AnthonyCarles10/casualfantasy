using System;

namespace Sportfaction.CasualFantasy
{	
	/// <summary>
	/// Description: structure of maintenance object
	/// author: madeleine.tranier
	/// </summary>
	[Serializable]
	public class Maintenance
	{
		public bool result;
		public string message;
	}
}

﻿using System;

namespace Sportfaction.CasualFantasy
{
    /// <summary>
    /// Description: Structure of a user profile object
    /// author: Antoine de Lachèze-Murel
    /// </summary>
    [Serializable]
    public class AskAssociativeCode
    {
        public string code;
        public int expireAt;
    }

}

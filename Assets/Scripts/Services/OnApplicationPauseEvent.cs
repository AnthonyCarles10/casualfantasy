using UnityEngine;
using Facebook.Unity;
using Sportfaction.CasualFantasy.Meta;

namespace Sportfaction.CasualFantasy.Services
{
    /// <summary>
    /// Description: ApplicationPause
    /// Author: Antoine de Lachèze-Murel
    /// </summary>
    public class OnApplicationPauseEvent : MonoBehaviour
    {
        // Unity will call OnApplicationPause(false) when an app is resumed
        // from the background
        void OnApplicationPause(bool pauseStatus)
        {
            // Check the pauseStatus to see if we are in the foreground
            // or background
            if (!pauseStatus)
            {
                //if (null != GameObject.Find("_Data"))
                //{
                //    MetaData.Instance.ArenaCupMain.GetCurrentEndTimestampArenaCup();
                //    MetaData.Instance.LeaderboardMain.GetCurrentNumberArenaCup();
                //}

                //app resume
                if (FB.IsInitialized)
                {
                    FB.ActivateApp();
                }
                else
                {
                    //Handle FB.Init
                    FB.Init(() => {
                        FB.ActivateApp();
                    });
                }
            }
        }
    }
}


﻿using System;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using UnityEngine;

namespace Sportfaction.CasualFantasy.Services
{
    public delegate void FuncDelegate();

    /// <summary>
    /// Author: Antoine
    /// </summary>
    public static class RetryHelper
    {

        public static void RetryOnException(int times, TimeSpan delay, FuncDelegate operation)
        {
            var attempts = 0;
            do
            {
                try
                {
                    attempts++;
                    operation();
                    break; // Sucess! Lets exit the loop!
                }
                catch (Exception ex)
                {
                    if (attempts == times)
                        throw;

                    Debug.Log(String.Format($"Exception caught on attempt {attempts} - will retry after delay {delay}", ex));

                    Task.Delay(delay).Wait();
                }
            } while (true);
        }
    }


}
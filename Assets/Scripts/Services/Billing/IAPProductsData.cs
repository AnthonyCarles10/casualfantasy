﻿using System.Collections.Generic;
using UnityEngine.Purchasing;
using UnityEngine;

namespace Sportfaction.CasualFantasy.Services.Billing
{
	/// <summary>
	/// Description: Class contains data about IAP products
	/// Author: Rémi Carreira
	/// </summary>
	[CreateAssetMenu(fileName = "Store-IAPProductsData", menuName = "CasualFantasy/Data/Store/IAPProducts")]
	public sealed class IAPProductsData : ScriptableObject
	{
		#region Properties

		// Property used to get the list of products
		public	List<Product>	Products	{ get { return _products; } }

		#endregion

		#region Variables

		[SerializeField, Tooltip("List contains all products")]
		private List<Product>	_products	= null;

		#endregion

		#region Public Methods

		/// <summary>
		/// Initialize IAP Products data
		/// </summary>
		public	void	Init()
		{
			if (null != IAPStoreManager.Instance)
			{
				_products	=	new List<Product>();
				if (null != _products && null != IAPStoreManager.Instance.Controller)
				{
					_products.AddRange(IAPStoreManager.Instance.Controller.products.all);
				}
			}
		}

		#endregion
	}
}

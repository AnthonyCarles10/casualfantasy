﻿using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Purchasing;

namespace Sportfaction.CasualFantasy.Services.Billing
{
	/// <summary>
	/// Description: Abstract class listening when a product was purchased
	/// Author: Rémi Carreira
	/// </summary>
	public abstract class PurchaseListener : MonoBehaviour
	{
		#region Protected Methods

		/// <summary>
		/// MonoBehaviour method used to bind OnProductPurchased function
		/// </summary>
		protected virtual void	OnEnable()
		{
			Assert.IsNotNull(IAPStoreManager.Instance, "PurchaseListener: OnEnable() failed, IAPStoreManager.Instance is null.");

			IAPStoreManager.Instance.AddProductPurchasedListener(OnProductPurchased);
		}

		/// <summary>
		/// MonoBehaviour method used to unbind OnProductPurchased function
		/// </summary>
		protected virtual void	OnDisable()
		{
			Assert.IsNotNull(IAPStoreManager.Instance, "PurchaseListener: OnDisable() failed, IAPStoreManager.Instance is null.");

			IAPStoreManager.Instance.RemoveProductPurchasedListener(OnProductPurchased);
		}

		/// <summary>
		/// Abstract function called when a product was purchased
		/// </summary>
		/// <param name="pProduct">Informations about the product purchase</param>
		protected abstract void	OnProductPurchased(Product pProduct);

		#endregion
	}
}
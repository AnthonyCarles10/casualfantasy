﻿using Sportfaction.CasualFantasy.Events;
using Sportfaction.CasualFantasy.Services.Userstracking;

using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Purchasing;

namespace Sportfaction.CasualFantasy.Services.Billing
{
    /// <summary>
    /// Description: Manage in app billing store
    /// Author: Rémi Carreira
    /// </summary>
    public sealed class IAPStoreManager : Singleton<IAPStoreManager>, IStoreListener
	{
		#region Properties

		// Propery used to get the catalog
		public	ProductCatalog		Catalog		{ get { return _catalog; } }
		// Property used to get the store controlelr
		public	IStoreController	Controller	{ get { return _controller; } }
		// Property used to get the store-specific extended functionality
		public	IExtensionProvider	Extensions	{ get { return _extensions; } }

		#endregion

		#region Fields

		[Header("Data")]
		[SerializeField, Tooltip("Scriptable object contains data of IAP products")]
		private	IAPProductsData		_IAPProductsData	=	null;

		// Current catalog of products
		private	ProductCatalog			_catalog			=	null;
		// Store controlling unity IAP
		private	IStoreController		_controller			=	null;
		// Provide access to store-specific extended functionality
		private	IExtensionProvider		_extensions			=	null;

		#endregion

		#region Events

		[Header("Events")]
        [SerializeField]
        private GameEvent _onGetProductsIAPStore = null;
        [SerializeField]
        private GameEvent _onPurchaseFailed = null;

		// Callback invoke when a product was purchased
		public	System.Action<Product>	ProductPurchased	=	null;

        // public  delegate void           OnReceivedProducts(List<Product> products);
        // public  event                   OnReceivedProducts OnReceivedProductsEvent;

        public  delegate void           OnProcessPurchase(Product product);
        public  event                   OnProcessPurchase OnPurchase;

		#endregion

		#region Public Methods

		/// <summary>
		/// Add listener to product purchased function
		/// </summary>
		/// <param name="pListener">Listener to add</param>
		public	void	AddProductPurchasedListener(System.Action<Product> pListener)
		{
            ProductPurchased +=	pListener;
		}

		/// <summary>
		/// Remove listener from product purchased function
		/// </summary>
		/// <param name="pListener">Listener to remove</param>
		public	void	RemoveProductPurchasedListener(System.Action<Product> pListener)
		{
            ProductPurchased -=	pListener;
		}

		/// <summary>
		/// Initiate a purchase 
		/// </summary>
		/// <param name="pProductId">Id of the product to purchase</param>
		public	void	InitiatePurchase(string pProductId)
		{
			Assert.IsFalse(string.IsNullOrEmpty(pProductId), "IAPStoreManager: InitiatePurchase() failed, pProductId is null or empty");

			Assert.IsNotNull(_controller, "IAPStoreManager: InitiatePurchase() failed, _controller is null.");

			_controller.InitiatePurchase(pProductId);
		}

		/// <summary>
		/// Restore purchases initiated
		/// </summary>
		public	void	RestoreTransactions()
		{
			Assert.IsNotNull(_extensions, "IAPStoreManager: RestoreTransactions() failed, _extensions is null.");

			#if UNITY_WSA

			IMicrosoftExtensions	lMicrosoftExtension	=	_extensions.GetExtension<IMicrosoftExtensions>();

			Assert.IsNotNull(lMicrosoftExtension, "IAPStoreManager: RestoreTransactions() failed, lMicrosoftExtnesion is null.");

			lMicrosoftExtension.RestoreTransactions();

			#elif UNITY_ANDROID

				StandardPurchasingModule	lModule	=	StandardPurchasingModule.Instance();

				Assert.IsNotNull(lModule, "IAPStoreManager: RestoreTransactions() failed, lModule is null.");

				if (lModule.appStore == AppStore.SamsungApps)
				{
					ISamsungAppsExtensions	lSamsungExtension	=	_extensions.GetExtension<ISamsungAppsExtensions>();

					Assert.IsNotNull(lSamsungExtension, "IAPStoreManager: RestoreTransactions() failed, lSamsungExtension is null.");

					lSamsungExtension.RestoreTransactions(OnTransactionsRestored);
				}
				else if (lModule.appStore == AppStore.CloudMoolah)
				{
					IMoolahExtension		lMoolahExtension	=	_extensions.GetExtension<IMoolahExtension>();

					Assert.IsNotNull(lMoolahExtension, "IAPStoreManager: RestoreTransactions() failed, lMoolahExtension is null.");

					lMoolahExtension.RestoreTransactionID(OnTransactionsRestored);
				}

			#elif UNITY_IOS || UNITY_TVOS || UNITY_EDITOR_OSX || UNITY_STANDALONE_OSX

				IAppleExtensions lAppleExtension = _extensions.GetExtension<IAppleExtensions>();

				Assert.IsNotNull(lAppleExtension, "IAPStoreManager: RestoreTransactions() failed, lAppleExtension is null.");

				lAppleExtension.RestoreTransactions(OnTransactionsRestored);

			#endif
		}

		/// <summary>
		/// IStoreListener method called when this store is initialized
		/// </summary>
		/// <param name="pController">Store controlling unity IAP</param>
		/// <param name="pExtensions">Provide access to store-specific extended functionality</param>
		public void OnInitialized(IStoreController pController, IExtensionProvider pExtensions)
		{
            _controller	=	pController;
			_extensions	=	pExtensions;

			Assert.IsNotNull(_IAPProductsData, "[IAPStoreManager] OnInitialized(), _IAPProductsData is null.");

			_IAPProductsData.Init();

			if (null != _onGetProductsIAPStore)
			{
				_onGetProductsIAPStore.Invoke();
			}

            // OnReceivedProductsEvent(Products);

            //TODO TO DECOMMENT
            // RestoreTransactions();
		}

		/// <summary>
		/// IStoreListener method called if store failed to initialize
		/// </summary>
		/// <param name="pError">Error's information</param>
		public void OnInitializeFailed(InitializationFailureReason pError)
		{
			Debug.Log("IAPStoreManager: OnInitializeFailed() called with error: " + pError.ToString());
		}

		/// <summary>
		/// IStoreListener method called if purchase a product failed
		/// </summary>
		/// <param name="pProduct">Product which purchase failed</param>
		/// <param name="pReason">Reason of failure</param>
		public void OnPurchaseFailed(Product pProduct, PurchaseFailureReason pReason)
		{
            _onPurchaseFailed.Invoke();
            UserEventsManager.Instance.RecordEvent(new UserActionEvent("purchase_failed_" + pReason.ToString()));
            Debug.Log("IAPStoreManager: OnPurchaseFailed() called for product: " + pProduct.definition.id + " with reason: " + pReason.ToString());
		}

		/// <summary>
		/// IStoreListener method called if process purchase
		/// </summary>
		/// <param name="pEvent">Event containing the product purchase</param>
		/// <returns>The result of the purchase</returns>
		public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs pEvent)
		{
            Assert.IsNotNull(pEvent, "IAPStoreManager: ProcessPurchase() failed, pEvent is null.");
            OnPurchase(pEvent.purchasedProduct);



            //			bool	lValidPurchase	=	true;	//	Valid or not the purchase

            //			#if UNITY_ANDROID || UNITY_IPHONE || UNITY_STANDALONE_OSX || UNITY_TVOS

            //				CrossPlatformValidator lValidator = new CrossPlatformValidator(GooglePlayTangle.Data(), AppleTangle.Data(), Application.identifier);

            //				Assert.IsNotNull(lValidator, "IAPStoreManager: ProcessPurchase() failed, lValidator is null.");

            //				try
            //				{
            //					Assert.IsNotNull(pEvent.purchasedProduct, "IAPStoreManager: ProcessPurchase() failed, pEvent.purchaseProduct is null.");

            //					lValidator.Validate(pEvent.purchasedProduct.receipt);
            //				}
            //				catch (IAPSecurityException)
            //				{
            //					Debug.LogError("IAPStoreManager: ProcessPurchase() failed, IAPSecurityException throw.");

            //					lValidPurchase	=	false;
            //				}

            //			#endif

            //			if (true == lValidPurchase)
            //			{
            //#if DEVELOPMENT_BUILD
            //					Debug.Log("IAPStoreManager: ProcessPurchase() complete, product with the receipt '" + pEvent.purchasedProduct.receipt + "' was purchase.");
            //#endif

            //	if (null != ProductPurchased)
            //	{
            //		ProductPurchased(pEvent.purchasedProduct);
            //	}
            //}
            return PurchaseProcessingResult.Complete;
		}

		/// <summary>
		/// Check if product exists
		/// </summary>
		/// <param name="pProductId">Product Id to find</param>
		/// <returns>True if product exists</returns>
		public	bool	HasProduct(string pProductId)
		{
			Assert.IsFalse(string.IsNullOrEmpty(pProductId), "IAPStoreManager: HasProduct() failed, pProductId is null or empty.");

			Assert.IsNotNull(_catalog, "IAPStoreManager: HasProduct() failed, _catalog is null.");

			if (null != _catalog.allProducts)
			{
				foreach (ProductCatalogItem lProduct in _catalog.allProducts)
				{
					if (pProductId == lProduct.id)
					{
						return true;
					}
				}
			}
			return false;
		}

		/// <summary>
		/// Get a specific product
		/// </summary>
		/// <param name="pProductId">Product Id to get</param>
		/// <returns>Product with the specific id</returns>
		public	Product	GetProduct(string pProductId)
		{
			Assert.IsFalse(string.IsNullOrEmpty(pProductId), "IAPStoreManager: GetProduct() failed, pProductId is null or empty.");

			Assert.IsNotNull(_controller, "IAPStoreManager: GetProduct() failed, _controller is null.");

			if (null != _controller.products)
			{
				return _controller.products.WithID(pProductId);
			}
			return null;
		}

		#endregion

		#region Protected Methods

		/// <summary>
		/// MonoBehaviour method used to initialize in app product
		/// </summary>
		protected override void	Awake()
		{
            base.Awake();
		}

        private void Start()
        {
            InitializeIAP();
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Initialize in app products
        /// </summary>
        private	void	InitializeIAP()
		{
			if (false == IsInitialized())
			{
				_catalog = ProductCatalog.LoadDefaultCatalog();

				Assert.IsNotNull(_catalog, "PurchaseButton: Start() failed, _catalog is null.");

				StandardPurchasingModule	lModule	=	StandardPurchasingModule.Instance();
				Assert.IsNotNull(lModule, "PurchaseButton: Start() failed, lModule is null.");

                // lModule.useFakeStoreUIMode	=	FakeStoreUIMode.DeveloperUser;

				ConfigurationBuilder	lBuilder	=	ConfigurationBuilder.Instance(lModule);
				Assert.IsNotNull(lBuilder, "PurchaseButton: Start() failed, lBuilder is null.");

				IAPConfigurationHelper.PopulateConfigurationBuilder(ref lBuilder, _catalog);
				UnityPurchasing.Initialize(this, lBuilder);
			}
		}

		/// <summary>
		/// Check if in app products store is already initialized
		/// </summary>
		/// <returns>True if in app products store is initialized</returns>
		private	bool	IsInitialized()
		{
			return (null != _catalog && null != _controller &&  null != _extensions);
		}

		/// <summary>
		/// Callback called when transaction restored was called
		/// </summary>
		/// <param name="pSuccess">True if transaction restored successfully</param>
		private	void	OnTransactionsRestored(bool pSuccess)
		{
			Debug.Log("Transactions restored: " + pSuccess);
		}

		/// <summary>
		/// Callback called when transaction restored was called for cloud moolah app
		/// </summary>
		/// <param name="pState">State of restore transaction</param>
		private	void	OnTransactionsRestored(RestoreTransactionIDState pState)
		{
			OnTransactionsRestored(pState != RestoreTransactionIDState.RestoreFailed && pState != RestoreTransactionIDState.NotKnown);
		}

		#endregion
	}
}
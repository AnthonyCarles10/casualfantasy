﻿using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Purchasing;

namespace Sportfaction.CasualFantasy.Services.Billing
{
	/// <summary>
	/// Description: Button used to purchase a specific product
	/// Author: Rémi Carreira
	/// </summary>
	public sealed class PurchaseButton : MonoBehaviour
	{
		#region Fields

		[SerializeField, Tooltip("Reference of the product to buy")]
        private	string	_ID	=	"";	// Id of the product to buy

		#endregion

		#region Public Methods

		/// <summary>
		/// Purchase a product
		/// </summary>
		public	void	PurchaseProduct()
		{
			Assert.IsNotNull(IAPStoreManager.Instance, "PurchaseButton: PurchaseProduct() failed, IAPStoreManager.Instance is null.");

            IAPStoreManager.Instance.InitiatePurchase(_ID);
		}

		#endregion
	}
}
﻿using UnityEngine;

namespace Sportfaction.CasualFantasy.Services
{
	/// <summary>
	/// Description: Class contains and apply all mobile settings
	/// Author: Rémi Carreira
	/// </summary>
	public sealed class FontFix : MonoBehaviour
	{
        public Font[] fonts;

		#region Private Methods

        void Start(){
            for (int i = 0; i < fonts.Length; i++){
                fonts[i].material.mainTexture.filterMode = FilterMode.Point;
            }
        }
		#endregion
	}
}
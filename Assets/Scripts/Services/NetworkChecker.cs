﻿using UnityEngine;

namespace Sportfaction.CasualFantasy.Services
{
	/// <summary>
	/// Description: Handle network connectivity plugin
	/// Author: Rémi Carreira
	/// </summary>
	public sealed class NetworkChecker : MonoBehaviour
	{
		#region Properties

		/// <summary>
		///	Return true if network connectivity is connected
		/// </summary>
		public	static	bool	IsConnected		{ get { return NetworkConnectivity.Instance.IsConnected; } }

		#endregion

		#region Public Methods

		/// <summary>
		/// Bind function to NetworkConnectivityChangedEvent
		/// </summary>
		/// <param name="pFunction">Function to bind</param>
		public	static	void	StartListeningNetworkConnectivity(NetworkConnectivity.NetworkConnectivityChanged pFunction)
		{
			NetworkConnectivity.NetworkConnectivityChangedEvent	+=	pFunction;
		}

		/// <summary>
		/// Unbind function from NetworkConnectivityChangedEvent
		/// </summary>
		/// <param name="pFunction">Function to unbind</param>
		public	static	void	StopListeningNetworkConnectivity(NetworkConnectivity.NetworkConnectivityChanged pFunction)
		{
			NetworkConnectivity.NetworkConnectivityChangedEvent	-=	pFunction;
		}

		#endregion

		#region Private Methods

		/// <summary>
		/// Unity Method | Initiliaze network connectivity
		/// </summary>
		private	void	Start()
		{
            if (null != NetworkConnectivity.Instance)
			    NetworkConnectivity.Instance.Initialise();
		}

		#endregion
	}
}
using System;
using System.Linq;
using BestHTTP.Statistics;
using UnityEngine;

namespace Sportfaction.CasualFantasy.Services
{
    /// <summary>
    /// Description: Class contains extension for string class
    /// Author: Rémi Carreira
    /// </summary>
    public static class StringExtensions
    {
        /// <summary>
        /// Sets first letter of provided string to uppercase
        /// </summary>
        /// <returns>String with first letter in uppercase</returns>
        /// <param name="pInput">string to modify</param>
        public static string FirstCharToUpper(string pInput)
        {
            pInput  =   pInput.ToLower();
            
            if (String.IsNullOrEmpty(pInput))
                throw new ArgumentException("FirstCharToUpper => string is null !");

            return pInput.First().ToString().ToUpper() + pInput.Substring(1);
        }

        /// <summary>
        /// Sets first letter of provided string to uppercase
        /// </summary>
        /// <returns>String with first letter in uppercase</returns>
        /// <param name="pInput">string to modify</param>
        public static string Capitalize(string pInput)
        {
            pInput  =   pInput.ToLower().Trim();
            
            if (String.IsNullOrEmpty(pInput))
                throw new ArgumentException("FirstCharToUpper => string is null !");

            string[]    lParts  =   pInput.Split(' ');
            string      lReturn =   "";

            if (0 >= lParts.Length)
                return pInput.First().ToString().ToUpper() + pInput.Substring(1);

            for (int li = 0; li < lParts.Length; li++)
            {
                lReturn += " " + lParts[li].First().ToString().ToUpper() + lParts[li].Substring(1);
            }

            return lReturn.TrimStart();
        }

        public  static  string  Truncate(string pInput, int pMaxLength = 9)
        {
            if (pInput.Length <= pMaxLength)
                return pInput;

            return pInput.Substring(0, pMaxLength - 1) + ".";
        }
    }
}
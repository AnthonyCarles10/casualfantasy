using Sportfaction.CasualFantasy.Services.Userstracking;

//using Facebook.Unity;
using UnityEngine;

namespace Sportfaction.CasualFantasy.Services
{
    /// <summary>
    /// Description: ApplicationQuit
    /// Author: Antoine de Lachèze-Murel
    /// </summary>
    public class OnApplicationQuitEvent : MonoBehaviour
    {
        void OnApplicationQuit()
        {
            Debug.Log("Session Time = " + Time.realtimeSinceStartup.ToString());

            UserEventsManager.Instance.RecordEvent(new UserActionEvent("user_session"));

            UserEventsManager.Instance.ForceUploadEvents();
        }
    }
}


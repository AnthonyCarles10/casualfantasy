﻿namespace Sportfaction.CasualFantasy.Services
{
	/// <summary>
	/// Description: Static class contains all constants used with Api Service
	/// Author: Rémi Carreira
	/// </summary>
	public static class ApiConstants
	{
		#region Properties

            #region AppParameters

            public  static  string  GetAppParams           { get { return "app_params"; } }     // Api Url used to get App Parameters
            public  static  string  AppParamsQueryKeyword  { get { return "keyword"; } }   // Key used to GetAppParams

            #endregion

            #region Action

            public static string GetListActions { get { return "actions"; } }
            public static string GetActionsBalancing { get { return "pvpoppositionbalancings"; } }
            public static string GetListPositionsActions { get { return "minions"; } }
            public static string GetListActionsLimitKey { get { return "limit"; } }
            public static string AddOrUpdateActionsDeck { get { return "actions/add_or_update_cards_deck"; } }
            public static string GetTacticalAvailableActions { get { return "actions/member"; } }
            public static string GetTacticalMemberActions { get { return "actions/params/member"; } }
            public static string SaveTacticalActions { get { return "actions/params/save"; } }

        #endregion

        #region ArenaCup
            public static string GetRewardsArenaCup { get { return "cycleleaderboards/rewards/arenacup"; } }
            public static string GetCurrentEndTimestampArenaCup { get { return "cycleleaderboards/current_end_timestamp/arenacup"; } }

        #endregion

        #region ArenaSeason
        public static string GetRewardsArenaSeason { get { return "arenaseasons/rewards"; } }
        public static string UnlockRewardArenaSeason { get { return "arenaseasons/unlocked_rewards"; } }

        #endregion

        

        #region Championship

        public static string GetOfficialTeamsByCountry { get { return "championships/official_teams_by_country"; } } // Check if a new application version is available.

            #endregion

            #region Core

            public static string IsUpdateAvailable              { get { return "core/is_update_available"; } } // Check if a new application version is available.
            public static string IsUpdateAvailableSupportKey    { get { return "support"; } } // support android or ios
            public static string IsUpdateAvailableVersionKey    { get { return "version"; } } // application version

            #endregion

            #region EditorID

            public	static	string	editorGamePlayerId	{ get { return "unity153_lold4"; } }		// Specific game player id when connect to api with unity editor
    		public	static	string	editorDeviceId		{ get { return "unity153_lold4"; } }	// Specific device id when connect to api with unity editor

    		#endregion
            	
    		#region FANTASY

    		public	static	string	GetSquadPlayersURL	{ get { return "teams/squads"; } }  // Api Url used to get players squad
            public static string    GetSquadBot         { get { return "teams/bot/squads"; } } // Api URL used to get a squad collection
            public static string    GetBuffsList        { get { return "buffs"; } } 
            #endregion

            #region Feedback

            public  static  string  SendFeedback    { get { return "feedbacks"; } } // Api URL used to get a overall ranking

            #endregion

            #region Field

            public static string GetFieldZones { get { return "pvpzones"; } }

            #endregion

            #region FieldSetup

            public static string GetListFieldSetup { get { return "pvpfieldpositions?composition_a=442f&composition_b=442f"; } }
            public static string GetListFieldSetupAction { get { return "pvpactiontriggerings"; } }

            #endregion

            #region Ladder

            public static  string GetOverallRankingByTrophy { get { return "users/leaderboard/score"; } }
            public static  string GetOverallRankingByRP { get { return "users/leaderboard/real_point"; } } 
            public static  string GetOverallRankingByArenaCup { get { return "users/leaderboard/arenacup"; } } 
            public static  string GetCurrentNumberArenaCup { get { return "cycleleaderboards/current_number/arenacup"; } } 
            public  static  string  GetOverallRankingLimitKey   { get { return "limit"; } } //  Key limit used to get a overall ranking
            public  static  string  GetOverallRankingOffsetKey  { get { return "offset"; } } //  Key offset used to get a overall ranking

            #endregion

            #region Loot
            public static string AskManagerLoot { get { return "userloots/ask_loot"; } }
            public static string GetLootTimers { get { return "userloots/loot_timers"; } }
            public static string PreloadLoot { get { return "userlootpreloads/preload_loot"; } }
            #endregion

            #region Shop

            public static   string  BuyProductWithRealMoney     { get { return "payments/real_money_order"; } }
            public static   string  BuyProduct                  { get { return "products/buy"; } }
            public static   string  GetProduct                  { get { return "products"; } }

        #endregion

            #region Userminions
        public static string GetPositionsManager { get { return "userminions"; } }
                public static string UpdatePositionManager { get { return "/userminions/upgrade_user_minion"; } }
            #endregion

            #region PVP

            public static	string	GetActionsLinksURL		{ get { return "pvpactionrelations"; } }	// Api URL used to get all actions links

                public static	string	GetTeamURL				{ get { return "teams"; } }			// Api URL used to get a team (DEPRECATED)
    		    public static	string	GetChallengesURL		{ get { return "pvpchallenges"; } }	// Api URL used to get all challenges

    		    public	static	string	GetAllPositionsURL					{ get { return "pvpfieldpositions"; } }	// Api URL used to get all possible positions during a match
    		    public	static	string	GetAllPositionsFirstParameterKey	{ get { return "composition_a"; } }		// Key used to set the first parameter of the GetAllPositionsURL
    		    public	static	string	GetAllPositionsSecondParameterKey	{ get { return "composition_b"; } }		// Key used to set the second parameter of the GetAllPositionsURL

    		    public	static	string	GetOppositionsURL				{ get { return "pvpoppositions"; } }			// Api Url used to get oppositions between two teams
    		    public	static	string	GetOppositionFirstParameterKey	{ get { return "composition_name_team_a"; } }	// Key used for the first parameter of the get oppostion 
    		    public	static	string	GetOppositionSecondParameterKey	{ get { return "composition_name_team_b"; } }	// Key used for the second parameter of the get opposition

    		    public	static	string	GetCompositionURL						{ get { return "pvpcompositions"; } }			// Api Url used to get composition of a team
    		    public	static	string	GetAllCompositionURL					{ get { return "pvpcompositions"; } }			// Api Url used to get all compositions
    		    public	static	string	GetMemberCompositionURL					{ get { return "pvpcompositions/member"; } }	// Api Url used to get member composition
    		    public	static	string	GetMemberCompositionFirstParameterKey	{ get { return "user_id"; } }					// Key used for the first parameter of the get member composition
                public  static  string  GetMemberCompositionSecondParameterKey  { get { return "device_id"; } }                 // Key used for the first parameter of the get member composition

                public	static	string	PostPvpgames                            { get { return "pvpgames"; } }					// Send Score End Match
                public  static  string  StartPvpgames                           { get { return "pvpgames/start"; } }            // Send SF bet

                public  static  string  GetDraftPlayers                         { get { return "pvpgames/draft_players"; } }            // Get Draft Players

                #endregion

            #region Manager

            public  static  string  LoginDefault        { get { return "users/login-default"; } } // Api URL used to get a profile user
            public  static  string  GetProfile          { get { return "users"; } } // Api URL used to get a profile user
            public  static  string  PatchProfile        { get { return "users"; } } // Api URL used to patch a profile user
            public  static  string  AskAssociativeCode  { get { return "users/ask_associative_code"; } } // Get code to associate a new device
            public  static  string  GetStatsProfile     { get { return "users/stats"; } } // Get code to associate a new device
            public  static  string  UpdateTeamCountry   { get { return "users/update_team_country"; } }

            #endregion

            #region Players

            public static string GetPlayers { get { return "players"; } } 
            public static string GetPlayerLastGame { get { return "players/last_game"; } } 
            public static string GetPlayersManager { get { return "teamplayers/ids"; } } 

            #endregion

            #region Squad

            public  static  string  GetPVPCategories    { get { return "pvpcategories"; } } // Api URL used to get pvp categories
            public  static  string  GetTeam             { get { return "teams/by_manager"; } }
            public  static  string  CreateOrUpdateLineUp { get { return "teams/lineup/create_or_update"; } }

            #endregion

            #region User

            public static string UpdateDeviceTokenURL { get { return "users/update_device_token"; } } // Api Url used to update device token
            public static string GetNextRealGame { get { return "users/next_real_game"; } } // Api Url used to update device token
            public static string GetLastRealGame { get { return "users/last_real_game"; } } // Api Url used to update device token
            public static string ResetTeam { get { return "users/reinit_team"; } }
            public static string ResetLoots { get { return "users/reinit_user_loots"; } }

            #endregion

        #endregion
    }
}
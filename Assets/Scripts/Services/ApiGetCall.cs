﻿using Sportfaction.CasualFantasy.Events;
using Sportfaction.CasualFantasy.References;

using BestHTTP;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

namespace Sportfaction.CasualFantasy.Services
{
	/// <summary>
	/// Description: Class handles a get call to the API
	/// Author: Rémi Carreira
	/// </summary>
	public class ApiGetCall : MonoBehaviour
	{
		#region Struct

		/// <summary>
		/// Custom key value pair
		/// </summary>
		[System.Serializable]
		public struct Param
		{
			#region Variables

			// Key of the parameter
			public	string		Key;
			// Value of the parameter
			public	string		Value;

			#endregion

			#region Constructor

			/// <summary>
			/// Param's Constructor
			/// </summary>
			/// <param name="pKey">Current key</param>
			/// <param name="pValue">Current Value</param>
			public	Param(string pKey, string pValue)
			{
				Key		= pKey;
				Value	=	pValue;
			}

			#endregion
		}

		#endregion

		#region Properties

		// Property used to set / get json reference
		public	StringReference		JsonReference		{ set { _jsonReference = value; } get { return _jsonReference; } }
		// Property used to set / get the authentification
		public	bool				Authentification	{ set { _authentification = value; } get { return _authentification; } }
		// Property used to set / get the url
		public	string				URL					{ set { _url = value; } get { return _url; } }
		// Property used to get the parameters
		public	List<Param>			Parameters			{ get { return _parameters; } }
		// Property used to set / get the game event when request finish successfully
		public	GameEvent			OnRequestRetrieve	{ set { _onRequestRetrieve = value; } get { return _onRequestRetrieve; } }

		#endregion

		#region Variables

		[Header("Variables")]
		[SerializeField, Tooltip("Reference to a string for json")]
		private	StringReference		_jsonReference		=	null;
		[SerializeField, Tooltip("Authentification needed?")]
		private	bool				_authentification	=	true;

		[SerializeField, Tooltip("URL used to get informations")]
		private	string				_url				=	"";
		[SerializeField, Tooltip("All query params sent")]
		private	List<Param>			_parameters			=	new List<Param>();

		#endregion

		#region Events

		[Header("Events")]
		[SerializeField, Tooltip("Game event invoke when request was retrieve")]
		private	GameEvent			_onRequestRetrieve	=	null;

		#endregion

		#region Public Methods

		/// <summary>
		/// Call api to retrieve some informations
		/// </summary>
		public	void	CallAPI()
		{
			if (null != Api.Instance)
			{
				Dictionary<string, string>	lQueryParams	=	new Dictionary<string, string>();
				if (null != lQueryParams)
				{
					Assert.IsNotNull(_parameters, "[ApiGetCall] CallAPI(), _parameters is null.");

					foreach (Param lParam in _parameters)
					{
						lQueryParams.Add(lParam.Key, lParam.Value);
					}

					string	lGeneratedURL	=	Api.Instance.GenerateUrl(_url, lQueryParams);

					Assert.IsFalse(string.IsNullOrEmpty(lGeneratedURL), "[ApiGetCall] CallAPI(), lGeneratedURL is invalid.");

					HTTPRequest	lRequest	=	Api.Instance.SetUpRequest(lGeneratedURL, Api.Methods.GET, _authentification);

					Assert.IsNotNull(lRequest, "[ApiGetCall] CallAPI(), lRequest is null.");

					StartCoroutine(Api.Instance.SendRequest(lRequest));
				}
			}
			else
			{
				Debug.LogError("[ApiGetCall] CallAPI(), API.Instance is null.");
			}
		}

		#endregion

		#region Private Methods

		/// <summary>
		/// MonoBehaviour method used to link callback
		/// </summary>
		private	void	OnEnable()
		{
			if (null != Api.Instance)
			{
				Api.Instance.OnSuccess	+=	OnRequestSuccess;
			}
		}

		/// <summary>
		/// MonoBehaviour method used to unlink callback
		/// </summary>
		private	void	OnDisable()
		{
			if (null != Api.Instance)
			{
				Api.Instance.OnSuccess	-=	OnRequestSuccess;
			}
		}

		/// <summary>
		/// Method invoke when a request finish successfully
		/// </summary>
		/// <param name="pCurrentUrl">Url of the request</param>
		/// <param name="pMethod">Http method</param>
		/// <param name="pData">Data sent by this request</param>
        private void OnRequestSuccess(string pCurrentUrl, HTTPMethods pMethod, string pData)
		{
			if (false == string.IsNullOrEmpty(_url) && true == pCurrentUrl.Contains(_url))
			{
				Assert.IsNotNull(_jsonReference, "[ApiGetCall] OnRequestSuccess(), _jsonReference is null.");

				_jsonReference.CurrentValue		=	pData;

				if (null != _onRequestRetrieve)
				{
					_onRequestRetrieve.Invoke();
				}
			}
		}

		#endregion
	}
}

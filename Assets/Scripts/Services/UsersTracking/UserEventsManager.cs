﻿using Sportfaction.CasualFantasy.Config;

//using Amazon;
//using Amazon.CognitoIdentity;
//using Amazon.Kinesis;
//using Amazon.Runtime;
using Facebook.Unity;
using Firebase;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Assertions;
using SportFaction.CasualFootballEngine;

namespace Sportfaction.CasualFantasy.Services.Userstracking
{
    /// <summary>
    /// Description: Manage all user events
    /// Author: Rémi Carreira & Antoine de Lachèze-Murel
    /// </summary>
    public class UserEventsManager : Singleton<UserEventsManager>
	{
		#region Properties

		//private	static	string			_partitionKey			=	"partitionKey";										// Define which shard in the stream that record is assigned to
		//private	static	string			_streamName				=	"sf-stream-app-events";								// Name of the stream to put the data record into


        //private	static	string			_identityPoolId			=	"eu-west-1:e41554cc-20b1-40f8-b68e-f2a87967e866";	// Cognito AWS Identity Pool ID
		//private	static	RegionEndpoint	_kinesisRegion			=	RegionEndpoint.EUWest1;								// Kinesis Region
		//private	static	RegionEndpoint	_cognitoIdentityRegion	=	RegionEndpoint.EUWest1;								// Cognito Region

		private	bool	_started	=	false;	// Use to know if tracking started
		private	byte	_maxEvents	=	4;		// The maximum of events to record before sending
		private	float	_delay		=	5f;	// Delay before automatically sending events 

        //private IAmazonKinesis		_amazonClient	=	null;					// Amazon Kinesis Client to put Records
        private	List<UserActionEvent>	_events			=	new List<UserActionEvent>();	// The list of all events recorded

        private FirebaseApp _firebaseInstance = null;

        // private static MemoryStream memoryStream = new MemoryStream(); // Creates a stream whose backing store is memory.
        // private StreamWriter streamWriter = new StreamWriter(memoryStream); // Implements a TextWriter for writing characters to a stream in a particular encoding.

        #endregion

        #region Public Methods

        /// <summary>
        /// Start tracking, connect to API and start automatic upload
        /// </summary>
        public	void	StartTracking()
		{
			if (false == _started)
			{
				_started	=	true;

				if (false == IsInvoking("UploadEvents"))
				{
					InvokeRepeating("UploadEvents", _delay, _delay);
				}
			}
		}

		/// <summary>
		/// Stop tracking, stop automatic upload
		/// </summary>
		public	void	StopTracking()
		{
			if (true == _started)
			{
				_started	=	false;

				UploadEvents();

				if (true == IsInvoking("UploadEvents"))
				{
					CancelInvoke("UploadEvents");
				}
			}
		}

		/// <summary>
		/// Stock event and call upload if limit of events reached
		/// </summary>
		/// <param name="pEvent">Event data</param>
        public	void	RecordEvent(UserActionEvent pEvent)
		{
			if (true == _started)
			{
				Assert.IsNotNull(pEvent, "UserEventsManager: RecordEvent() failed, user event can't be negative.");

				_events.Add(pEvent);

				if (_maxEvents <= _events.Count)
				{
					UploadEvents();
				}
			}
		}

		/// <summary>
		/// Put all events stored in the kinesis stream 
		/// </summary>
		public	void	UploadEvents()
		{
			if ((true == _started && 0 < _events.Count))
			{
                //UploadToAWS();
                //if ("PROD" == ConfigData.Instance.ENV)
                //{
                //	UploadToFacebook();
                //}

                // UploadToAWS();

                for (int i = 0; i < _events.Count; i++)
                {
                    if (true == FB.IsInitialized)
                    {
                        UploadToFacebook(_events[i]);
                    }
                    else
                    {
                        Debug.Log(FB.IsInitialized + " -> FB is not initiated !");
                    }
                    if (_firebaseInstance != null)
                    {
                        UploadToFirebase(_events[i]);
                    }

                    _events.Remove(_events[i]);
                }

                //_events.Clear();
			}
		}

		/// <summary>
		/// Put all events stored in the kinesis stream 
		/// </summary>
		public	void	ForceUploadEvents()
		{
            for (int i = 0; i < _events.Count; i++)
            {
                if (true == FB.IsInitialized)
                {
                    UploadToFacebook(_events[i]);
                }
                else
                {
                    Debug.Log(FB.IsInitialized + " -> FB is not initiated !");
                }
                if (_firebaseInstance != null)
                {
                    UploadToFirebase(_events[i]);
                }

                _events.Remove(_events[i]);
            }
        }

		//public void UploadToAWS()
		//{
 	//		Assert.IsNotNull(_amazonClient, "UserEventsManager: UploadEvents() failed, _amazonClient is null.");

		//	using (var memoryStream = new MemoryStream())
		//	using (var streamWriter = new StreamWriter(memoryStream))
		//	{
  //              string ljson = "{[";
  //              foreach (UserActionEvent uEvent in _events)
  //              {
  //                  ljson += uEvent.SerializeObjectToJson() + ',';
  //              }

  //              ljson = ljson.Remove(ljson.Length - 1);
  //              ljson += "]}";

  //              // Debug.Log(ljson);
		//			// streamWriter.Write(JsonConvert.SerializeObject(_events.ToArray()));

		//			// _amazonClient.PutRecordAsync(new PutRecordRequest
		//			// {
		//			// 		Data = memoryStream,
		//			// 		PartitionKey = _partitionKey,
		//			// 		StreamName = _streamName
		//			// },
		//			// (responseObject) =>
		//			// {
		//			// 		if (responseObject.Exception == null)
		//			// 		{
		//			// 			// Debug.Log(string.Format("Successfully put record with sequence number '{0}'.", responseObject.Response.SequenceNumber));
		//			// 		}
		//			// 		else
		//			// 		{
		//			// 			Debug.LogError(responseObject.Exception);
		//			// 		}
		//			// }
		//			// );
		//	}
		//}

		public void UploadToFacebook(UserActionEvent uEvent)
		{
            #if !DEVELOPMENT_BUILD && !UNITY_EDITOR

            if (ConfigData.Instance.ENV == eConfig.PROD)
            {

                switch (uEvent.ActionName)
                {
                    case "first_connect":
                        FB.LogAppEvent(AppEventName.CompletedRegistration, 1, uEvent.FacebookParams);
                        break;
                    //case "manager_match_ended":
                    //    FB.LogAppEvent(AppEventName.UnlockedAchievement, 1, uEvent.FacebookParams);
                    //    break;
                    default:
                        FB.LogAppEvent(uEvent.ActionName, 1, uEvent.FacebookParams);
                        break;
                }
            }
               
            #endif

        }

        public void UploadToFirebase(UserActionEvent uEvent)
        {
            #if !DEVELOPMENT_BUILD && !UNITY_EDITOR
            if(ConfigData.Instance.ENV == eConfig.PROD)
            {
                Firebase.Analytics.FirebaseAnalytics.LogEvent(
                    uEvent.ActionName,
                    uEvent.FirebasePrams
                );
            }
              
            #endif
        }

        public void SetUserPropertiesFirebase()
        {
            foreach (var pUserProperty in ConfigData.Instance.UserProperties)
            {
                Firebase.Analytics.FirebaseAnalytics.SetUserProperty(pUserProperty.Key, pUserProperty.Value);
            }
        }

        #endregion

        #region Protected Methods

        /// <summary>
        /// MonoBehaviour method used to initialize UserEventsManager
        /// </summary>
        protected override void	Awake()
		{
			base.Awake();
            //UnityInitializer.AttachToGameObject(this.gameObject);

            #if !UNITY_EDITOR
                InitFacebook();
                InitFirebase();
            #endif
        }



        #endregion

        #region Private Methods

        //private void InitAWS()
        //{
        //    AWSConfigs.HttpClient = AWSConfigs.HttpClientOption.UnityWebRequest;

        //    AWSCredentials lCredentials = new CognitoAWSCredentials(_identityPoolId, _cognitoIdentityRegion);

        //    Assert.IsNotNull(lCredentials, "UserEventsManager: OnAwake() failed, lCredentials is null.");

        //    _amazonClient = new AmazonKinesisClient(lCredentials, _kinesisRegion);

        //    Assert.IsNotNull(_amazonClient, "UserEventsManager: OnAwake() failed, _amazonClient is null.");
        //}

        private void InitFacebook()
        {
            if (!FB.IsInitialized)
            {
                // Initialize the Facebook SDK
                Debug.Log("FB Begin Init");
                FB.Init(InitCallback, OnHideUnity);

            }
            else
            {
                // Already initialized, signal an app activation App Event
                FB.ActivateApp();
            }
        }

        private void InitFirebase()
        {
            FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task => {
                var dependencyStatus = task.Result;
                if (dependencyStatus == DependencyStatus.Available)
                {
                    _firebaseInstance = FirebaseApp.DefaultInstance;

                    Debug.Log("Firebase is Ready");

                }
                else
                {
                    Debug.LogError(System.String.Format(
                       "Could not resolve all Firebase dependencies: {0}", dependencyStatus));
                    // Firebase Unity SDK is not safe to use here.
                }
            });
        }

        /// <summary>
        /// Facebook CallBack on Init
        /// </summary>
        private void InitCallback()
        {
            Debug.Log("FB InitCallback");
            if (FB.IsInitialized)
            {
                // Signal an app activation App Event
                FB.ActivateApp();
                Debug.Log("FB Init succeed");
            }
            else
            {
                Debug.Log("Failed to Initialize the FB SDK");
            }
        }

        private void OnHideUnity(bool isGameShown)
        {
            if (!isGameShown)
            {
                // Pause the game - we will need to hide
                Time.timeScale = 0;
            }
            else
            {
                // Resume the game - we're getting focus again
                Time.timeScale = 1;
            }
        }

        #endregion
    }
}
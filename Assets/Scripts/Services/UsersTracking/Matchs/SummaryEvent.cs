﻿//using Newtonsoft.Json;
//using UnityEngine.Assertions;
//using System.Collections.Generic;
//using Sportfaction.CasualFantasy.PVP.Gameplay.Matchs;
//using Sportfaction.CasualFantasy.PVP.Gameplay.Players;
//using Sportfaction.CasualFantasy.PVP.Gameplay.Matchs.Challenges;
//using Sportfaction.CasualFantasy.PVP.Gameplay.Matchs.Oppositions;

//namespace Sportfaction.CasualFantasy.Services.Userstracking.Matchs
//{
//	/// <summary>
//	/// Description: Class contains a summary of the current match
//	/// Author: Rémi Carreira
//	/// </summary>
//	public sealed class SummaryEvent : AUserEvent
//	{
//		#region Class

//		/// <summary>
//		/// Class contains opposition data
//		/// </summary>
//		public class Opposition
//		{
//			#region Fields

//			[JsonProperty(PropertyName = "phase_id", Order = 0)]
//			public	int		PhaseID				=	1;
//			[JsonProperty(PropertyName = "opposition_id", Order = 1)]
//			public	int		OppositionID		=	1;

//			[JsonProperty(PropertyName = "player_a_phase", Order = 2)]
//			public	string	PlayerAPhase		=	"";
//			[JsonProperty(PropertyName = "player_a_position", Order = 3)]
//			public	string	PlayerAPosition		=	"";
//			[JsonProperty(PropertyName = "player_a_challenge", Order = 4)]
//			public	string	PlayerAChallenge	=	"";
//			[JsonProperty(PropertyName = "player_a_score", Order = 5)]
//			public	float	PlayerAScore		=	0f;

//			[JsonProperty(PropertyName = "player_b_phase", Order = 6)]
//			public	string	PlayerBPhase		=	"";
//			[JsonProperty(PropertyName = "player_b_position", Order = 7)]
//			public	string	PlayerBPosition		=	"";
//			[JsonProperty(PropertyName = "player_b_challenge", Order = 8)]
//			public	string	PlayerBChallenge	=	"";
//			[JsonProperty(PropertyName = "player_b_score", Order = 9)]
//			public	float	PlayerBScore		=	0f;

//			#endregion

//			#region Public Methods

//			/// <summary>
//			/// Specific constructor
//			/// </summary>
//			/// <param name="pPlayerAPhase">Cur match phase of the player A</param>
//			/// <param name="pPlayerBPhase">Cur match phase of the player B</param>
//			/// <param name="pOpp">Data of the opposition between player A and player B</param>
//			public	Opposition(int pPhaseId, int pOppositionId, eMatchPhase pPlayerAPhase, eMatchPhase pPlayerBPhase, PlayersChallengeOpposition pOpp)
//			{
//				PhaseID			=	pPhaseId;
//				OppositionID	=	pOppositionId;
//				PlayerAPhase	=	pPlayerAPhase.ToString();
//				PlayerBPhase	=	pPlayerBPhase.ToString();

//				if (null != pOpp)
//				{
//					InitializePlayerAData(pOpp.PlayerAChallenge);

//					InitializePlayerBData(pOpp.PlayerBChallenge);
//				}
//			}

//			#endregion

//			#region Private Methods

//			/// <summary>
//			/// Initialize player A data
//			/// </summary>
//			/// <param name="pPlayerChallenge">Use infos in player challenge</param>
//			private	void	InitializePlayerAData(PlayerChallenge pPlayerChallenge)
//			{
//				Assert.IsNotNull(pPlayerChallenge, "[SummaryEvent.Opposition] InitializePlayerAData(), pPlayerChallenge is null");

//				PlayerAScore		=	pPlayerChallenge.CurrentScore;

//				Assert.IsNotNull(pPlayerChallenge.Challenge, "[SummaryEvent.Opposition] InitializePlayerAData(), pPlayerChallenge.Challenge is null.");

//				PlayerAChallenge	=	pPlayerChallenge.Challenge.Name;

//				Assert.IsNotNull(pPlayerChallenge.PlayerData, "[SummaryEvent.Opposition] InitializePlayerAData(), pPlayerChallenge.PlayerData is null.");

//				PlayerAPosition		=	pPlayerChallenge.PlayerData.PVPData.Position.ToString();
//			}

//			/// <summary>
//			/// Initialize player B data
//			/// </summary>
//			/// <param name="pPlayerChallenge">Use infos in player challenge</param>
//			private	void	InitializePlayerBData(PlayerChallenge pPlayerChallenge)
//			{
//				Assert.IsNotNull(pPlayerChallenge, "[SummaryEvent.Opposition] InitializePlayerBData(), pPlayerChallenge is null");

//				PlayerBScore		=	pPlayerChallenge.CurrentScore;

//				Assert.IsNotNull(pPlayerChallenge.Challenge, "[SummaryEvent.Opposition] InitializePlayerBData(), pPlayerChallenge.Challenge is null.");

//				PlayerBChallenge	=	pPlayerChallenge.Challenge.Name;

//				Assert.IsNotNull(pPlayerChallenge.PlayerData, "[SummaryEvent.Opposition] InitializePlayerBData(), pPlayerChallenge.PlayerData is null.");

//				//PlayerBPosition		=	pPlayerChallenge.PlayerData.PVPData.Position.ToString();
//				PlayerBPosition		=	pPlayerChallenge.PlayerData.
//			}

//			#endregion
//		}

//		#endregion

//		#region Fields

//		[JsonProperty(PropertyName = "oppositions", Order = 1)]
//		private	List<Opposition>	_oppositions	=	new List<Opposition>();

//		[JsonIgnore]
//		private	int	_phaseId		=	0;
//		[JsonIgnore]
//		private	int	_oppositionId	=	1;	

//		#endregion

//		#region Public Methods

//		/// <summary>
//		/// Default constructor
//		/// </summary>
//		public SummaryEvent()
//		: base(UserEventType.MatchSummary)
//		{
//		} 

//		/// <summary>
//		/// Register new phases of team A and team B
//		/// </summary>
//		/// <param name="pTeamAPhase">New phase of team A</param>
//		/// <param name="pTeamBPhase">New phase of team B</param>
//		public	void	RegisterNewPhases(eMatchPhase pTeamAPhase, eMatchPhase pTeamBPhase)
//		{
//			++_phaseId;

//			Opposition	lNewOpposition	=	new Opposition(_phaseId, -1, pTeamAPhase, pTeamBPhase, null);

//			Assert.IsNotNull(lNewOpposition, "[SummaryEvent] RegisterNewPhases(), lNewOpposition is null.");

//			_oppositions.Add(lNewOpposition);
//		}

//		/// <summary>
//		/// Register all oppositions data
//		/// </summary>
//		/// <param name="pTeamAPhase">Cur match phase of team A</param>
//		/// <param name="pTeamBPhase">Cur match phase of team B</param>
//		/// <param name="pPlayersOppositions">Cur oppositions between players of team A and team B</param>
//		public	void	RegisterOppositions(eMatchPhase pTeamAPhase, eMatchPhase pTeamBPhase, PlayersOpposition[] pPlayersOppositions)
//		{
//			Assert.IsNotNull(pPlayersOppositions, "[SummayEvent] RegisterOppositions(), pPlayersOppositions is null.");

//			Assert.IsNotNull(_oppositions, "[SummaryEvent] RegisterOppositions(), _oppositions is null.");

//			int	lFirstLength	=	pPlayersOppositions.Length;
//			int	lSecondLength	=	0;

//			for (int lFirstIndex = 0; lFirstIndex < lFirstLength; ++lFirstIndex)
//			{
//				Assert.IsNotNull(pPlayersOppositions[lFirstIndex], "[SummaryEvent] RegisterOppositions(), pPlayersOppositions[" + lFirstIndex + "] is null.");

//				Assert.IsNotNull(pPlayersOppositions[lFirstIndex].Oppositions, "[SummaryEvent] RegisterOppositions(), pPlayersOppositions[" + lFirstIndex + "].Oppositions is null.");

//				lSecondLength	=	pPlayersOppositions[lFirstIndex].Oppositions.Length;

//				for (int lSecondIndex = 0; lSecondIndex < lSecondLength; ++lSecondIndex)
//				{
//					Assert.IsNotNull(pPlayersOppositions[lFirstIndex].Oppositions[lSecondIndex], "[SummaryEvent] RegisterOppositions(), pPlayersOppositions[" + lFirstIndex + "].Oppositions[" + lSecondIndex + "] is null.");

//					Opposition	lNewOpposition	=	new Opposition(_phaseId, _oppositionId, pTeamAPhase, pTeamBPhase, pPlayersOppositions[lFirstIndex].Oppositions[lSecondIndex]);

//					Assert.IsNotNull(lNewOpposition, "[SummaryEvent] RegisterOppositions(), lNewOpposition is null.");

//					_oppositions.Add(lNewOpposition);
//				}
//			}

//			++_oppositionId;
//		}

//		#endregion
//	}
//}
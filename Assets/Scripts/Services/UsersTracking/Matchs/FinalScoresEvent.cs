﻿//using Newtonsoft.Json;

//namespace Sportfaction.CasualFantasy.Services.Userstracking
//{
//	/// <summary>
//	/// Description: Specific user event data contains teams' score
//	/// Author: Rémi Carreira
//	/// </summary>
//	public sealed class FinalScoresEvent : AUserEvent
//	{
//		#region Properties

//		// Property used to get the score of the team A
//		[JsonIgnore]
//		public	string	TeamAScore { get { return _teamAScore; } }
//		// Property used to get the score of the team B
//		[JsonIgnore]
//		public	string	TeamBScore { get { return _teamBScore; } }

//		#endregion

//		#region Fields

//		[JsonProperty(PropertyName = "teamAScore", Order = 1)]
//		private	string	_teamAScore	=	"";	// Score of team A
//		[JsonProperty(PropertyName = "teamBScore", Order = 2)]
//		private	string	_teamBScore	=	"";	// Score of team B

//		#endregion

//		#region Public Methods

//		/// <summary>
//		/// Specific constructor
//		/// </summary>
//		/// <param name="pTeamAScore">Score of team A</param>
//		/// <param name="pTeamBScore">Score of team B</param>
//		public FinalScoresEvent(string pTeamAScore, string pTeamBScore)
//		: base(UserEventType.MatchScores)
//		{
//			_teamAScore	=	pTeamAScore;
//			_teamBScore	=	pTeamBScore;
//		}

//		#endregion
//	}
//}
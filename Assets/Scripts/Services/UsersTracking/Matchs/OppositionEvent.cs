﻿//using UnityEngine;
//using Newtonsoft.Json;
//using UnityEngine.Assertions;
//using System.Collections.Generic;
////using Sportfaction.CasualFantasy.PVP.Gameplay.Matchs.Oppositions;
////using Sportfaction.CasualFantasy.PVP.Gameplay.Matchs.Challenges;
//using Sportfaction.CasualFantasy.PVP.Gameplay.Players;

//namespace Sportfaction.CasualFantasy.Services.Userstracking
//{
//	/// <summary>
//	/// Description: 
//	/// Author: Rémi Carreira
//	/// </summary>
//	public sealed class OppositionEvent : AUserEvent
//	{
//		#region Public Struct

//		[System.Serializable]
//		private	sealed class ChallengeData
//		{
//			#region Fields

//			[JsonProperty(PropertyName = "name", Order = 1)]
//			public	string		ChallengeName	=	"";
//			[JsonProperty(PropertyName = "score", Order = 2)]
//			public	float		ChallengeScore	=	0f;

//			#endregion

//			#region Public Methods

//			/// <summary>
//			/// Specific constructor
//			/// </summary>
//			/// <param name="pChallenge">Player challenge</param>
//			public	ChallengeData(PlayerChallenge pChallenge)
//			{
//				Assert.IsNotNull(pChallenge, "ChallengeData: Specific Constructor failed, pChallenge is null.");

//				ChallengeScore	=	pChallenge.CurrentScore;

//				Assert.IsNotNull(pChallenge.Challenge, "ChallengeData: Specific Constructor failed, pChallenge.Challenge is null.");

//				ChallengeName	=	pChallenge.Challenge.Name;
//			}

//			#endregion
//		}

//		[System.Serializable]
//		private sealed class PlayersChallengeOppositionData
//		{
//			#region Fields

//			[JsonProperty(PropertyName = "player_a_challenge", Order = 1)]
//			public	ChallengeData	PlayerAChallengeData	=	null;
//			[JsonProperty(PropertyName = "player_b_challenge", Order = 1)]
//			public	ChallengeData	PlayerBChallengeData	=	null;

//			#endregion

//			#region Public Methods

//			/// <summary>
//			/// Specific constructor
//			/// </summary>
//			/// <param name="pOpp"></param>
//			public	PlayersChallengeOppositionData(PlayersChallengeOpposition pOpp)
//			{
//				Assert.IsNotNull(pOpp, "ChallengeData: Specific Constructor failed, pOpp is null.");

//				Assert.IsNotNull(pOpp.PlayerAChallenge, "ChallengeData: Specific Constructor failed, pOpp.PlayerAChallenge is null.");

//				PlayerAChallengeData = new ChallengeData(pOpp.PlayerAChallenge);

//				Assert.IsNotNull(pOpp.PlayerBChallenge, "ChallengeData: Specific Constructor failed, pOpp.PlayerBChallenge is null.");

//				PlayerBChallengeData = new ChallengeData(pOpp.PlayerBChallenge);
//			}

//			#endregion
//		}

//		[System.Serializable]
//		private sealed class PlayersOppositionData
//		{
//			#region Fields

//			[JsonProperty(PropertyName = "player_a_position", Order = 1)]
//			public	string	PlayerAPosition	=	"";
//			[JsonProperty(PropertyName = "player_b_position", Order = 2)]
//			public	string	PlayerBPosition	=	"";
//			[JsonProperty(PropertyName = "player_a_score", Order = 3)]
//			public	float	PlayerAScore	=	0f;
//			[JsonProperty(PropertyName = "player_b_score", Order = 4)]
//			public	float	PlayerBScore	=	0f;
//			[JsonProperty(PropertyName = "players_opposition_difference", Order = 5)]
//			public	float	Difference		=	0f;
//			[JsonProperty(PropertyName = "all_players_challenge_oppositions", Order = 5)]
//			public	List<PlayersChallengeOppositionData>	AllPlayersChallengeOppositions	=	new List<PlayersChallengeOppositionData>();

//			#endregion

//			#region Public Methods

//			/// <summary>
//			/// 
//			/// </summary>
//			/// <param name="pOpp"></param>
//			public	PlayersOppositionData(PlayersOpposition pOpp)
//			{
//				//Assert.IsNotNull(pOpp, "PlayersOppositionData: Specific constructor failed, pOpp is null.");

//				//PlayerAScore	=	pOpp.PlayerAFrameScore;
//				//PlayerBScore	=	pOpp.PlayerBFrameScore;
//				//Difference		=	PlayerAScore - PlayerBScore;

//				//Assert.IsNotNull(pOpp.PlayerAData, "PlayersOppositionData: Specific constructor failed, pOpp.PlayerAData is null.");

//				//PlayerAPosition = ePositionMethods.ConvertEPlayerPositionToString(pOpp.PlayerAData.PVPData.Position);

//				//Assert.IsNotNull(pOpp.PlayerBData, "PlayersOppositionData: Specific constructor failed, pOpp.PlayerBData is null.");

//				//PlayerBPosition = ePositionMethods.ConvertEPlayerPositionToString(pOpp.PlayerBData.PVPData.Position);

//				//Assert.IsNotNull(pOpp.Oppositions, "PlayersOppositionData: Specific constructor failed, pOpp.Oppositions is null.");

//				//Assert.IsNotNull(AllPlayersChallengeOppositions, "PlayersOppositionData: Specific constructor failed, AllPlayersChallengeOppositions is null.");

//				//if (0 < pOpp.Oppositions.Length)
//				//{
//				//	int	lLength = pOpp.Oppositions.Length;

//				//	for (int lIndex = 0; lIndex < lLength; ++lIndex)
//				//	{
//				//		PlayersChallengeOppositionData lOppData = new PlayersChallengeOppositionData(pOpp.Oppositions[lIndex]);

//				//		Assert.IsNotNull(lOppData, "PlayersOppositionData: Specific constructor failed, lOppData is null.");

//				//		AllPlayersChallengeOppositions.Add(lOppData);
//				//	}
//				//}
//			}

//			#endregion
//		}

//		#endregion

//		#region Fields

//		[JsonProperty(PropertyName = "opposition_difference", Order = 1)]
//		private float	_difference	=	0f;
//		[JsonProperty(PropertyName = "team_a_score", Order = 2)]
//		private	float	_teamAScore	=	0f;
//		[JsonProperty(PropertyName = "team_b_score", Order = 3)]
//		private	float	_teamBScore	=	0f;
//		[JsonProperty(PropertyName = "all_players_oppositions", Order = 4)]
//		private	List<PlayersOppositionData>	_allPlayersOppositions	=	new List<PlayersOppositionData>();

//		#endregion

//		#region Public Methods

//		/// <summary>
//		/// 
//		/// </summary>
//		public OppositionEvent(float pTeamAScore, float pTeamBScore, PlayersOpposition[] pPlayersOpposition)
//		: base(UserEventType.MatchOpposition)
//		{
//			_teamAScore	=	pTeamAScore;
//			_teamBScore	=	pTeamBScore;
//			_difference	=	_teamAScore - _teamBScore;

//			Assert.IsNotNull(pPlayersOpposition, "OppositionEvent: Specific Constructor failed, pPlayersOpposition is null.");

//			Assert.IsNotNull(_allPlayersOppositions, "OppositionEvent: Specific Constructor failed, _allPlayersOppositions is null.");

//			if (0 < pPlayersOpposition.Length)
//			{
//				int	lLength	=	pPlayersOpposition.Length;

//				for (int lIndex = 0; lIndex < lLength; ++lIndex)
//				{
//					PlayersOppositionData lOppData = new PlayersOppositionData(pPlayersOpposition[lIndex]);

//					Assert.IsNotNull(lOppData, "OppositionEvent: Specific constructor failed, lOppData is null.");

//					_allPlayersOppositions.Add(lOppData);
//				}
//			}
//		}

//		#endregion
//	}
//}
﻿namespace Sportfaction.CasualFantasy.Services.Userstracking
{
	/// <summary>
	/// Description: Type of user event
	/// Author: Rémi Carreira
	/// </summary>
	public static class UserEventType
	{
		public	static	string	Action			{ get { return "action"; } }
		public	static	string	Connexion		{ get { return "connexion"; } }
		public	static	string	PageView		{ get { return "pageview"; } }
		public	static	string	MatchScores		{ get { return "match_scores"; } }
		public	static	string	MatchOpposition { get { return "match_opposition"; } }
		public	static	string	MatchSummary	{ get { return "match_summary"; } }
	}
}
﻿using Sportfaction.CasualFantasy.Config;

using Newtonsoft.Json;
using System.Collections.Generic;
using UnityEngine;

namespace Sportfaction.CasualFantasy.Services.Userstracking
{
    /// <summary>
    /// Description: Specific user event data created when the user do an action
    /// Author: Rémi Carreira
    /// </summary>
    public class UserActionEvent
    {
        #region Properties

        [JsonIgnore]
        public Dictionary<string, object> FacebookParams { get { return _facebookParams; } }

        [JsonIgnore]
        public Firebase.Analytics.Parameter[] FirebasePrams { get { return _firebaseParams; } }

        [JsonIgnore]
        public string ActionName { get { return _actionName; } }

        [JsonProperty("actionName", Order = 1)]
        private string _actionName = ""; // Name of the action occured by the user

        // Dictionary contains parameters
        private Dictionary<string, object> _facebookParams = new Dictionary<string, object>();

        private Firebase.Analytics.Parameter[] _firebaseParams = new Firebase.Analytics.Parameter[]{};

        #endregion

        #region Public Methods

        public	UserActionEvent(string pActionName)
		{
			_actionName = pActionName;

           SetFacebookParams(pActionName);
           SetFirebaseParams(pActionName);
        }

		 public string SerializeObjectToJson()
		 {
		 	Dictionary<string, object> _tmp = new Dictionary<string, object>();
		 	_tmp = _facebookParams;

            if (_tmp.ContainsKey("actionName") == true)
            {
                _tmp["actionName"] = _actionName;
            }
            else
            {
                _tmp.Add("actionName", _actionName);
            }


		 	return JsonConvert.SerializeObject(_tmp);
		 }


        public void SetFacebookParams(string pActionName)
        {
            _facebookParams = new Dictionary<string, object>();

            if (null != ConfigData.Instance)
            {
                Dictionary<string, object> lDict = new Dictionary<string, object>();
                foreach (KeyValuePair<string, string> keyValuePair in ConfigData.Instance.TrackingParams)
                {
                    lDict.Add(keyValuePair.Key, keyValuePair.Value == null ? "" : keyValuePair.Value.ToString());
                }

                lDict = new Dictionary<string, object>();
                foreach (KeyValuePair<string, string> keyValuePair in ConfigData.Instance.UserProperties)
                {
                    lDict.Add(keyValuePair.Key, keyValuePair.Value == null ? "" : keyValuePair.Value.ToString());
                }
                _facebookParams = lDict;
            }

            if ("user_session" == pActionName)
            {
                _facebookParams.Add("session_time", Time.realtimeSinceStartup.ToString());
            }
        }

        public void SetFirebaseParams(string pActionName)
        {
            int _nbParams = ConfigData.Instance.TrackingParams.Count;
            if ("user_session" == pActionName)
            {
                _nbParams++;
            }
 
             _firebaseParams = new Firebase.Analytics.Parameter[_nbParams];

            int i = 0;

            if (null != ConfigData.Instance)
            {
                        
                foreach (var pParam in ConfigData.Instance.TrackingParams)
                {
                    _firebaseParams[i] = new Firebase.Analytics.Parameter(pParam.Key, pParam.Value);
                    i++;
                }
            }

            if ("user_session" == pActionName)
            {
                _firebaseParams[i] = new Firebase.Analytics.Parameter("session_time", Time.realtimeSinceStartup.ToString());
            }

          
        }

        #endregion
    }
}
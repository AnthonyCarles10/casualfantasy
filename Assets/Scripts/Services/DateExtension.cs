﻿using System;
using System.Globalization;
using System.Linq;
using UnityEngine;

namespace Sportfaction.CasualFantasy.Services
{
    /// <summary>
    /// Author: Antoine
    /// </summary>
    public sealed class DateExtension : MonoBehaviour
    {

        #region Private Methods

        public static string ConvertDateTimeToDate(string dateTimeString, string pFormat = "ddd d MMM yyyy ,", string pParseFormat = "yyyy-MM-dd hh:mm:ss")
        {
            if (String.IsNullOrEmpty(dateTimeString))
            {
                return dateTimeString;
            }

            DateTime dt = DateTime.MinValue;

            if (DateTime.TryParse(dateTimeString, out dt))
            {
                return dt.ToString(pFormat, GetCurrentCultureInfo());
            }
            else if(DateTime.TryParseExact(dateTimeString, pParseFormat,CultureInfo.InvariantCulture,DateTimeStyles.None,out dt))
            {
                return dt.ToString(pFormat, CultureInfo.CreateSpecificCulture(GetCurrentCultureInfo().ToString()));
            }

            return dateTimeString;
        }

        public static CultureInfo GetCurrentCultureInfo()
        {
            SystemLanguage currentLanguage = Application.systemLanguage;
            CultureInfo correspondingCultureInfo = CultureInfo.GetCultures(CultureTypes.AllCultures).FirstOrDefault(x => x.EnglishName.Equals(currentLanguage.ToString()));
            return CultureInfo.CreateSpecificCulture(correspondingCultureInfo.TwoLetterISOLanguageName);
        }
        #endregion
    }


}
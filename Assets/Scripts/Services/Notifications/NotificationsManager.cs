﻿using UnityEngine;
using UnityEngine.Assertions;
using System.Collections.Generic;
using Firebase.Messaging;

namespace Sportfaction.CasualFantasy.Services.Notifications
{
	/// <summary>
	/// Description: Handle local notifications and firebase cloud messaging
	/// Author: Rémi Carreira
	/// </summary>
	public sealed class NotificationsManager : Singleton<NotificationsManager>
	{
        #region Properties

        public LocalNotificationDatabase Database { get { return _database; } } 

        #endregion

        #region Fields

        [SerializeField, Tooltip("The database containing all local notifications")]
		private	LocalNotificationDatabase	_database	=	null;   // The database containing all local notifications

        #endregion

        #region Public Methods


        /// <summary>
        /// Send a specific local notification
        /// </summary>
        /// <param name="pNotificationName">The name of the notification to send</param>
        public	void	SendLocalNotification(string pNotificationName)
		{
			Assert.IsFalse(string.IsNullOrEmpty(pNotificationName), "NotificationsManager: SendLocalNotification() failed, pNotificationName is null or empty.");

			SendLocalNotification(pNotificationName.GetHashCode());
		}

		/// <summary>
		/// Send a specific local notification
		/// </summary>
		/// <param name="pNotificationId">The id of the notification to send</param>
		public	void	SendLocalNotification(int pNotificationId)
		{
			Assert.IsNotNull(_database, "NotificationsManager: SendLocalNotification() failed, _database is null.");

			LocalNotificationData	lNotif	=	_database.GetLocalNotification(pNotificationId);

			Assert.IsNotNull(lNotif, "NotificationsManager: SendLocalNotifications() failed, lNotif is null.");

			lNotif.SendNotification();
		}

		/// <summary>
		/// Cancel a specific local notification
		/// </summary>
		/// <param name="pNotificationName">The name of the notification to cancel</param>
		public	void	CancelLocalNotification(string pNotificationName)
		{
			Assert.IsFalse(string.IsNullOrEmpty(pNotificationName), "NotificationsManager: CancelNotification() failed, pNotificationName is null or empty.");

			CancelLocalNotification(pNotificationName.GetHashCode());
		}

		/// <summary>
		/// Cancel a specific local notification
		/// </summary>
		/// <param name="pNotificationId">The id of the notification to cancel</param>
		public	void	CancelLocalNotification(int pNotificationId)
		{
			LocalNotification.CancelNotification(pNotificationId);
		}

		/// <summary>
		/// Clear all local notifications
		/// </summary>
		public	void	ClearAllLocalNotifications()
		{
			LocalNotification.ClearNotifications();
		}

		#endregion

		#region Private Methods

        //private void Awake()
        //{
        //    UnityEngine.iOS.NotificationServices.RegisterForNotifications(
        //        NotificationType.Alert |
        //        NotificationType.Badge |
        //        NotificationType.Sound);
        //}

		/// <summary>
		/// MonoBehaviour Method | Bind FCM functions
		/// </summary>
		private	void	OnEnable()
		{
			FirebaseMessaging.TokenReceived		+= OnTokenReceived;
			FirebaseMessaging.MessageReceived	+= OnMessageReceived;
		}

		/// <summary>
		/// MonoBehaviour Method | UnBind FCM functions
		/// </summary>
		private	void	OnDisable()
		{
			FirebaseMessaging.TokenReceived		-= OnTokenReceived;
			FirebaseMessaging.MessageReceived	-= OnMessageReceived;
		}

		/// <summary>
		/// Callback used when token was received from Firebase Cloud Messaging
		/// </summary>
		/// <param name="pSender">Token sender</param>
		/// <param name="pToken">User token</param>
		private	void	OnTokenReceived(object pSender, TokenReceivedEventArgs pToken)
		{
			#if DEVELOPMENT_BUILD
				Debug.Log("NotificationsManager: OnTokenReceived() occured, received registration token: " + pToken.Token);
			#endif

			HandleToken(pToken.Token);
		}

		/// <summary>
		/// Callback used when message was received from Firebase Cloud Messaging
		/// </summary>
		/// <param name="pSender">Message sender</param>
		/// <param name="pMsg">Message informations</param>
		private	void	OnMessageReceived(object pSender, MessageReceivedEventArgs pMsg)
		{
			#if DEVELOPMENT_BUILD
				Debug.Log("NotificationsManager: OnMessageReceived() occured, received a message from: " + pMsg.Message.From);
			#endif

			HandleFirebaseMessage(pMsg.Message.Data);
		}

		/// <summary>
		/// Handle FCM token and update API
		/// </summary>
		/// <param name="pToken">FCM token data</param>
		private	void	HandleToken(string pToken)
		{
			Assert.IsFalse(string.IsNullOrEmpty(pToken), "NotificationsManager: HandleToken() failed, pToken is null or empty.");

			NotificationToken	lToken	=	new NotificationToken();

			lToken.Token	=	pToken;
            lToken.Support  =   Application.platform == RuntimePlatform.Android ? "android" : "ios";

			BestHTTP.HTTPRequest lRequest = Api.Instance.SetUpRequest(Api.Instance.GenerateUrl(ApiConstants.UpdateDeviceTokenURL, true), true, lToken);

			StartCoroutine(Api.Instance.SendRequest(lRequest));

			#if DEVELOPMENT_BUILD
				Debug.Log("NotificationsManager: HandleToken(), Token is : " + lToken.Token);
				Debug.Log("NotificationsManager: HandleToken(), Support is : " + lToken.Support);
				Debug.LogWarning("REQUEST: " + lRequest.Uri);
			#endif

		}

		/// <summary>
		/// Handle message pushed by FCM
		/// </summary>
		/// <param name="lData">Message data with string keys and values</param>
		private	void	HandleFirebaseMessage(IDictionary<string, string> lData)
		{


            // Assert.IsFalse(null == lData || 0 == lData.Count, "NotificationsManager: HandleFirebaseMessage() failed, lData is null or empty.");

            #if DEVELOPMENT_BUILD
                			Debug.Log("NotificationsManager: HandleFirebaseMessage().");

                			foreach (KeyValuePair<string, string> lKVP in lData)
                			{
                				Debug.Log("NotificationsManager: HandleFirebaseMessage(), KVP is " + lKVP.Key + " | " + lKVP.Value);
                			}
            #endif

            Assert.IsTrue(lData.ContainsKey(FirebaseConstants.NotificationTitleKey), "NotificationsManager: HandleFirebaseMessage() failed, lData don't have notification title.");

            Assert.IsTrue(lData.ContainsKey(FirebaseConstants.NotificationBodyKey), "NotificationsManager: HandleFirebaseMessage() failed, lData don't have notification body.");


            LocalNotification.SendNotification(FirebaseConstants.NotificationId, FirebaseConstants.NotificationDelay, lData[FirebaseConstants.NotificationTitleKey], lData[FirebaseConstants.NotificationBodyKey],
                                                       FirebaseConstants.NotificationColor, FirebaseConstants.NotificationPlaySound, FirebaseConstants.NotificationAllowVibration, FirebaseConstants.NotificationAllowLights,
                                                       FirebaseConstants.NotificationBigIcon, FirebaseConstants.NotificationSoundName, FirebaseConstants.NotificationChannel);

                //if(true == lData.ContainsKey(FirebaseConstants.NotificationTitleKey)) {
                //    if (Application.platform == RuntimePlatform.Android)
                //    {
                //        Debug.Log("OK Notif Android > " + lData[FirebaseConstants.NotificationTitleKey] + " >> " + lData[FirebaseConstants.NotificationBodyKey]);

                //        LocalNotification.SendNotification(FirebaseConstants.NotificationId, FirebaseConstants.NotificationDelay, lData[FirebaseConstants.NotificationTitleKey], lData[FirebaseConstants.NotificationBodyKey],
                //                                       FirebaseConstants.NotificationColor, FirebaseConstants.NotificationPlaySound, FirebaseConstants.NotificationAllowVibration, FirebaseConstants.NotificationAllowLights,
                //                                       FirebaseConstants.NotificationBigIcon, FirebaseConstants.NotificationSoundName, FirebaseConstants.NotificationChannel);
                //    }
                //    else
                //    {
                //        Debug.Log("OK Notif IOS > " + lData[FirebaseConstants.NotificationTitleKey] + " >> " + lData[FirebaseConstants.NotificationBodyKey]);

                //        UnityEngine.iOS.LocalNotification liOSNotif = new UnityEngine.iOS.LocalNotification();
                //        liOSNotif.alertAction = lData[FirebaseConstants.NotificationTitleKey];
                //        liOSNotif.alertBody = lData[FirebaseConstants.NotificationBodyKey];
                //        liOSNotif.fireDate = DateTime.Now.AddSeconds(2);
                //        UnityEngine.iOS.NotificationServices.ScheduleLocalNotification(liOSNotif);
                //    }  
                //}

			/*	Custom Notifications

			Assert.IsTrue(lData.ContainsKey(FirebaseConstants.CustomNotificationKey), "NotificationsManager: HandleFirebaseMessage() failed, firebase message don't have custom notification data.");

			FirebaseCustomNotification	lCustomNotification = JsonConvert.DeserializeObject<FirebaseCustomNotification>(lData[FirebaseConstants.CustomNotificationKey]);

			LocalNotification.SendNotification(FirebaseConstants.NotificationId, FirebaseConstants.NotificationDelay, lCustomNotification.Title, lCustomNotification.Body, lCustomNotification.GetColor(),
											   FirebaseConstants.NotificationPlaySound, FirebaseConstants.NotificationAllowVibration, FirebaseConstants.NotificationAllowLights, FirebaseConstants.NotificationBigIcon,
											   FirebaseConstants.NotificationSoundName, FirebaseConstants.NotificationChannel);

			*/
		}

		#endregion
	}
}
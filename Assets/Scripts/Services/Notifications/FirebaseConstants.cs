﻿using UnityEngine;

namespace Sportfaction.CasualFantasy.Services.Notifications
{
	/// <summary>
	/// Description: Static class contains firebase constants
	/// Author: Rémi Carreira
	/// </summary>
	public static class FirebaseConstants
	{
		#region Fields

		public	static	string	NotificationTitleKey	=	"title";				// Key used to get the title of the notification push by FCM
		public	static	string	NotificationBodyKey		=	"body";					// Key used to get the body of the notification push by FCM
		public	static	string	CustomNotificationKey	=	"custom_notification";	// Key used to get the custom notification push by FCM

		public	static	int		NotificationId		=	-1;				// Id of the notification push by FCM
		public	static	long	NotificationDelay	=	1000;			// Delay before send notification push by FCM
		public	static	Color	NotificationColor	=	Color.clear;	// Color blended to icon of the notification push by FCM

		public	static	bool	NotificationPlaySound		=	true;	// Play sound when a notification was push by FCM ?
		public	static	bool	NotificationAllowVibration	=	true;	// Allow vibration when a notification was push by FCM ?
		public	static	bool	NotificationAllowLights		=	true;	// Allow lights when a notification was push by FCM ?

		public	static	string	NotificationBigIcon			=	"";			// Name of the big icon used for notification push by FCM
		public	static	string	NotificationSoundName		=	"";			// Name of the sound used for notification push by FCM
		public	static	string	NotificationChannel			=	"default";	// Name of the channel used for notification push by FCM

		#endregion
	}
}
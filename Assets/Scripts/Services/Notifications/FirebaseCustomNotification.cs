﻿using UnityEngine;
using Newtonsoft.Json;

namespace Sportfaction.CasualFantasy.Services.Notifications
{
	/// <summary>
	/// Description: 
	/// Author: Rémi Carreira
	/// </summary>
	[System.Serializable]
	public struct FirebaseCustomNotification
	{
		#region Fields

		[JsonProperty(PropertyName = "show_in_foreground", Order = 1)]
		public	bool	ShowInForeground;
		[JsonProperty(PropertyName = "title", Order = 7)]
		public	string	Title;
		[JsonProperty(PropertyName = "body", Order = 8)]
		public	string	Body;
		[JsonProperty(PropertyName = "big_text", Order = 4)]
		public	string	BigText;
		[JsonProperty(PropertyName = "tag", Order = 6)]
		public	string	Tag;
		[JsonProperty(PropertyName = "priority", Order = 9)]
		public	string	Priority;
		[JsonProperty(PropertyName = "sound", Order = 2)]
		public	string	SoundName;
		[JsonProperty(PropertyName = "icon", Order = 3)]
		public	string	NormalIconName;
		[JsonProperty(PropertyName = "large_icon", Order = 5)]
		public	string	LargeIconName;
		[JsonProperty(PropertyName = "color", Order = 0)]
		public	string	HexColor;

		#endregion

		#region Public Fields

		/// <summary>
		/// Get the custom notification color
		/// </summary>
		/// <returns>A color struct</returns>
		public	Color	GetColor()
		{
			Color	lColor	=	new Color();

			if (true == ColorUtility.TryParseHtmlString(HexColor, out lColor))
			{
				return lColor;
			}
			return Color.clear;
		}

		#endregion
	}
}
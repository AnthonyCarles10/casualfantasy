﻿using UnityEngine;
using UnityEngine.Assertions;

namespace Sportfaction.CasualFantasy.Services.Notifications
{
	/// <summary>
	/// Description: Databse containing all local notifications used at runtime
	/// Author: Rémi Carreira
	/// </summary>
	[CreateAssetMenu(fileName = "New Local Notifications Database", menuName = "CasualFantasy/Notifications/Local Notifs Database")]
	public sealed class LocalNotificationDatabase : ScriptableObject
	{
		#region Fields

		[SerializeField, Tooltip("All local notifications used at runtime")]
		private	LocalNotificationData[]		_localNotifications;		// All local notifications used at runtime

		#endregion

		#region Public Methods

		/// <summary>
		/// Get the local notification corresponding to the given notification name
		/// </summary>
		/// <param name="pNotificationName">The name of the notification to get</param>
		/// <returns>A LocalNotificationData</returns>
		public	LocalNotificationData	GetLocalNotification(string pNotificationName)
		{
			return GetLocalNotification(pNotificationName.GetHashCode());
		}

		/// <summary>
		/// Get the local notification corresponding to the given notification id
		/// </summary>
		/// <param name="pNotificationId">The id of the notification to get</param>
		/// <returns>A LocalNotificationData</returns>
		public	LocalNotificationData	GetLocalNotification(int pNotificationId)
		{
			Assert.IsNotNull(_localNotifications, "LocalNotificationDatabase: GetLocalNotification() failed, _localNotifications is null.");

			for (int lIndex = 0; lIndex < _localNotifications.Length; ++lIndex)
			{
				Assert.IsNotNull(_localNotifications[lIndex], "LocalNotificationsDatabse: GetLocalNotification() failed, _localNotifications at index " + lIndex + " is null.");

				if (pNotificationId == _localNotifications[lIndex].NotificationId)
				{
					return _localNotifications[lIndex];
				}
			}
			return null;
		}

		#endregion
	}
}
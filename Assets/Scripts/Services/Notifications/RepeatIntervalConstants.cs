﻿namespace Sportfaction.CasualFantasy.Services.Notifications
{
	/// <summary>
	/// Description: Static class contains repeat interval constants 
	/// Author: Rémi Carreira
	/// </summary>
	public static class RepeatIntervalConstants
	{
		#region Fields

		public	static	long	Minute	=	60000;			// Constant value for a minute
		public	static	long	Hour	=	3600000;		// Constant value for an hour
		public	static	long	Day		=	86400000;		// Constant value for a day
		public	static	long	Week	=	604800000;		// Constant value for a week
		public	static	long	Month	=	2419200000;		// Constant value for a month
		public	static	long	Year	=	31536000000;    // Constant value for a year

		#endregion

		#region Public Methods

		/// <summary>
		/// Convert eRepeatInterval to long constants
		/// </summary>
		/// <param name="pRepeatInteval">Type of repeat interval</param>
		/// <returns>A long corresponding to the interval required</returns>
		public	static	long	ConvertERepeatInterval(eRepeatInterval pRepeatInteval)
		{
			switch (pRepeatInteval)
			{
				case eRepeatInterval.MINUTE:
					return Minute;
				case eRepeatInterval.HOUR:
					return Hour;
				case eRepeatInterval.DAY:
					return Day;
				case eRepeatInterval.WEEK:
					return Week;
				case eRepeatInterval.MONTH:
					return Month;
				case eRepeatInterval.YEAR:
					return Year;
				default:
					return 0;
			}
		}

		#endregion
	}
}
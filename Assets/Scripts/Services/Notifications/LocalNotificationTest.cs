﻿using UnityEngine;

namespace Sportfaction.CasualFantasy.Services.Notifications
{
	/// <summary>
	/// Description: Test local notifications functions
	/// Author: Rémi Carreira
	/// </summary>
	public sealed class LocalNotificationTest : MonoBehaviour
	{
		#region Fields

		[SerializeField, Tooltip("Schedule local notification")]
		private	bool	_scheduleNotification	=	true;	// Schedule local notification ?
		[SerializeField, Tooltip("Cancel schedule for local notification test")]
		private	bool	_cancelNotification		=	true;	// Cancel schedule for local notification test
		[SerializeField, Tooltip("Discard all local notifications")]
		private	bool	_discardNotifications	=	true;	// Discard all local notifications
		[SerializeField, Tooltip("Local notification to use for test.")]
		private	string	_localNotificationName	=	"";		// Local notification name

		#endregion

		#region Private Methods

		/// <summary>
		/// MonoBehaviour Method | Schedule or cancel the local notification test
		/// </summary>
		private	void	Start()
		{
			if (true == _scheduleNotification)
			{
				NotificationsManager.Instance.SendLocalNotification(_localNotificationName);
			}
			if (true == _cancelNotification)
			{
				NotificationsManager.Instance.CancelLocalNotification(_localNotificationName);
			}
			if (true == _discardNotifications)
			{
				NotificationsManager.Instance.ClearAllLocalNotifications();
			}
		}

		#endregion
	}
}
﻿namespace Sportfaction.CasualFantasy.Services.Notifications
{
	/// <summary>
	/// Description: Specify the repeat interval between notifications
	/// Author: Rémi Carreira
	/// </summary>
	public enum eRepeatInterval
	{
		MINUTE,
		HOUR,
		DAY,
		WEEK,
		MONTH,
		YEAR,
		OTHER,
	}
}
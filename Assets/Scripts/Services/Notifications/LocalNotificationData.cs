﻿using UnityEngine;

namespace Sportfaction.CasualFantasy.Services.Notifications
{
	/// <summary>
	/// Description: Data used to display a local notification
	/// Author: Rémi Carreira
	/// </summary>
	[CreateAssetMenu(fileName = "New Local Notification", menuName = "CasualFantasy/Notifications/Local Notif Data")]
	public sealed class LocalNotificationData : ScriptableObject
	{
		#region Properties

		public	int		NotificationId { get { return _notificationId; } }  // Property used to get notification id

        public long Delay { get { return _delay; } set { _delay = value; } }

        public eRepeatInterval Interval { get { return _interval; } set { _interval = value; } }

		public	long    OtherInteval { get { return _otherInteval; } set { _otherInteval = value; } }	

		#endregion

		#region Fields

		[Header("Global")]
		[SerializeField, ReadOnly, Tooltip("Notification id generated at runtime")]
		private	int		_notificationId		=	0;
		[SerializeField, Tooltip("Name of the notification")]
		private	string	_notificationName	=	"";
		[SerializeField, Tooltip("Play sound ?")]
		private	bool	_sound				=	true;
		[SerializeField, Tooltip("Vibrate phone ?")]
		private	bool	_vibrate			=	true;
		[SerializeField, Tooltip("Enable lights ?")]
		private	bool	_lights				=	true;
		[SerializeField, Tooltip("Key of the title to localize and display")]
		private	string	_titleKey			=	"";
		[SerializeField, Tooltip("Key of the message to localize and display")]
		private	string	_messageKey			=	"";
		[SerializeField, Tooltip("Delay in milliseconds before firing the notification")]
		private	long	_delay				=	0;
		[SerializeField, Tooltip("Color blended to icon")]
		private	Color	_color				=	Color.clear;

		[Header("Repeating")]
		[SerializeField, Tooltip("Repeat notification ?")]
		private	bool			_repeat			=	false;
		[SerializeField, Tooltip("Interval between notifications")]
		private	eRepeatInterval	_interval		=	eRepeatInterval.MINUTE;
		[SerializeField, Tooltip("Interval in milliseconds to take into account if interval is set to OTHER")]
		private	long			_otherInteval	=	60000;

		[Header("Others")]
		[SerializeField, Tooltip("Custom big icon name")]
		private	string	_bigIconName	=	"";
		[SerializeField, Tooltip("Custom sound name")]
		private	string	_soundName		=	"";
		[SerializeField, Tooltip("Channel to send this notification")]
		private	string	_channel		=	"default";

		#endregion

		#region Public Methods

		/// <summary>
		/// Send this notification
		/// </summary>
		public	void	SendNotification()
		{
			string	lTitle		=	I2.Loc.LocalizationManager.GetTranslation(_titleKey);
			string	lMessage	=	I2.Loc.LocalizationManager.GetTranslation(_messageKey);

            //Debug.Log("ADLM SEND NOTIF = " + lTitle + " / " + lMessage + " / delay =  " + _delay  + " / _interval = " + _interval);
			if (true == _repeat)
			{
				if (eRepeatInterval.OTHER == _interval)
				{
					LocalNotification.SendRepeatingNotification(_notificationId, _delay, _otherInteval, lTitle, lMessage, _color, _sound, _vibrate,
																_lights, _bigIconName, _soundName, _channel);
				}
				else
				{
					LocalNotification.SendRepeatingNotification(_notificationId, _delay, RepeatIntervalConstants.ConvertERepeatInterval(_interval),
																lTitle, lMessage, _color, _sound, _vibrate, _lights, _bigIconName, _soundName, _channel);
				}
			}
			else
			{
				LocalNotification.SendNotification(_notificationId, _delay, lTitle, lMessage, _color, _sound, _vibrate, _lights, _bigIconName, _soundName, _channel);
			}
		}

		/// <summary>
		/// Cancel this notification
		/// </summary>
		public	void	CancelNotification()
		{
			LocalNotification.CancelNotification(_notificationId);
		}

		#endregion

		#region Private Methods

		/// <summary>
		/// Scriptable object method | Generate notification id
		/// </summary>
		private	void	OnEnable()
		{
			_notificationId	=	_notificationName.GetHashCode();
		}

		#endregion
	}
}
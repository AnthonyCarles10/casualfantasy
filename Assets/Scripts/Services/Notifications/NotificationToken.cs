﻿using Newtonsoft.Json;

namespace Sportfaction.CasualFantasy.Services.Notifications
{
	/// <summary>
	/// Description: Struct used to send notification token to api
	/// Author: Rémi Carreira
	/// </summary>
	[System.Serializable]
	public struct NotificationToken
	{
		#region Fields

		[JsonProperty(PropertyName = "token", Order = 0)]
		public	string	Token;
		[JsonProperty(PropertyName = "support", Order = 1)]
		public	string	Support;

		#endregion
	}
}
using Newtonsoft.Json.Linq;

namespace Sportfaction.CasualFantasy.Services
{
    /// <summary>
    /// Description: Class contains extension for Newtonsoft class
    /// Author: Rémi Carreira
    /// </summary>
    public static class JsonExtensions
    {
        /// <summary>
        /// Parses provided token into specific class & returns safe value if token not parsable
        /// </summary>
        /// <returns>Value parsed of default safe value</returns>
        /// <param name="pToken">Token to parse</param>
        /// <param name="pKey">Key to search for in token</param>
        /// <param name="pDefault">Default value to return if token not parsable</param>
        /// <typeparam name="T">Value type to return</typeparam>
        public static T SafeValue<T>(this JToken pToken, string pKey, T pDefault = default(T))
        {
            JToken lToken = pToken[pKey];

            if (null != lToken && lToken.Type != JTokenType.Null)
            {
                return Newtonsoft.Json.Linq.Extensions.Value<JToken, T>(lToken);
            }
            
            return pDefault;
        }
    }
}
﻿using Sportfaction.CasualFantasy.Events;

using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

namespace Sportfaction.CasualFantasy.Services.Timers
{
	/// <summary>
	/// Description: Class handles multiple local timers
	/// Author: Rémi Carreira
	/// </summary>
	public sealed class LocalTimersManager : Singleton<LocalTimersManager>
	{

        #region Variables

        // Key used to load/save local timers infos in player prefs
        private static	string			_playerPrefKey		=	"LocalTimers";
		// Dictionary contains data of local timers
		private	List<LocalTimerData>	_localTimersData	=	new List<LocalTimerData>();
		// Coroutine used to update local timers
		private	Coroutine				_updateCoroutine	=	null;

		#endregion

		#region Events

		[Header("Events")]
		[SerializeField, Tooltip("Game event to invoke when local timers are updated")]
		private	GameEvent		_onLocalTimersUpdated	=	null;
		[SerializeField, Tooltip("Game event to invoke when a local timer complete")]
		private	GameEvent		_onLocalTimerComplete	=	null;

		#endregion

		#region Public Methods

		/// <summary>
		/// Method used to test timer
		/// </summary>
		[ContextMenu("Test")]
		public	void	Test()
		{
			//StartTimer("Test-0-0-60", 0, 0, 60);
		}

        /// <summary>
        /// Start a new timer
        /// </summary>
        /// <param name="pName">Name of the timer to start</param>
        /// <param name="pEndTimestamp">string</param>
        /// <returns>True if a new timer was created</returns>
        public LocalTimerData StartTimer(string pName, string pEndTimestamp, string pCurrentTimestamp = null, GameEvent pGameEvent = null)
		{
			Assert.IsNotNull(_localTimersData, "[LocalTimersManager] StartTimer(), _localTimers is null.");

            int lIndex = -1;
            for (int i = 0; i <= _localTimersData.Count - 1; i++)
            {
                if (string.Equals(_localTimersData[i].Name, pName))
                {
                    lIndex = i;
                    break;
                }
            }


            LocalTimerData	lNewLocalTimerData	=	CreateLocalTimer(pName, pEndTimestamp, pCurrentTimestamp, pGameEvent);

			if (null != lNewLocalTimerData)
			{
                if(lIndex != - 1)
                {
                    _localTimersData[lIndex] = lNewLocalTimerData;
                }
                else
                {
                    _localTimersData.Add(lNewLocalTimerData);
                }

                StartUpdateCoroutine();

				SaveLocalTimers();
			}
			return lNewLocalTimerData;
		}


        /// <summary>
		/// Get time left
		/// </summary>
		/// <returns>Timespan</returns>
		public bool IsLocalTimerExists(string pName)
        {
            Assert.IsNotNull(_localTimersData, "[LocalTimersManager] GetTimeLeft(), _localTimers is null.");

            foreach (LocalTimerData lLocalTimerData in _localTimersData)
            {
                if (string.Equals(lLocalTimerData.Name, pName))
                {
                    return true;

                }
            }
            return false;
        }

        /// <summary>
        /// Get time left
        /// </summary>
        /// <returns>Timespan</returns>
        public	TimeSpan	GetTimeLeft(string pName)
		{
			Assert.IsNotNull(_localTimersData, "[LocalTimersManager] GetTimeLeft(), _localTimers is null.");

			foreach (LocalTimerData lLocalTimerData in _localTimersData)
			{
				if (string.Equals(lLocalTimerData.Name, pName))
				{
					return lLocalTimerData.TimeLeft;

				}
			}
			return TimeSpan.Zero;
		}

		/// <summary>
		/// Check if a timer complete
		/// </summary>
		/// <returns>True if timer complete or not found</returns>
		public	bool	IsTimerComplete(string pName)
		{
			Assert.IsNotNull(_localTimersData, "[LocalTimersManager] IsTimerComplete(), _localTimers is null.");

			foreach (LocalTimerData lLocalTimerData in _localTimersData)
			{
				if (string.Equals(lLocalTimerData.Name, pName))
				{
					return false;
				}
			}
			return true;
		}

        /// <summary>
        /// Load local timers on awake home script
        /// </summary>
        public void LoadLocalTimers()
        {
            if (PlayerPrefs.HasKey(_playerPrefKey))
            {
                string lLocalTimersTxt = PlayerPrefs.GetString(_playerPrefKey);

                _localTimersData = JsonConvert.DeserializeObject<List<LocalTimerData>>(lLocalTimersTxt);

                Assert.IsNotNull(_localTimersData, "[LocalTimersManager] LoadLocalTimers(), _localTimersData is null.");

                List<LocalTimerData> lGarbage = new List<LocalTimerData>();

                Assert.IsNotNull(lGarbage, "[LocalTimersManager] LoadLocalTimers(), lGarbage is null.");

                foreach (LocalTimerData lLocalTimerData in _localTimersData)
                {
                    lLocalTimerData.TimeLeft = lLocalTimerData.EndDate - lLocalTimerData.StartDate;

                    if (lLocalTimerData.TimeLeft <= TimeSpan.Zero)
                    {
                        lGarbage.Add(lLocalTimerData);
                    }
                }

                foreach (LocalTimerData lLocalTimerData in lGarbage)
                {
                    if (lLocalTimerData.Event != null)
                    {
                        lLocalTimerData.Event.Invoke();
                    }

                    _localTimersData.Remove(lLocalTimerData);

                }

                lGarbage.Clear();

                SaveLocalTimers();

                StartUpdateCoroutine();
            }
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// MonoBehaviour method called when this component is disable
        /// </summary>
        private	void	OnDisable()
		{
			StopUpdateCoroutine();
		}


		/// <summary>
		/// Save local timers
		/// </summary>
		private	void	SaveLocalTimers()
		{
            PlayerPrefs.SetString(_playerPrefKey, JsonConvert.SerializeObject(_localTimersData));
            PlayerPrefs.Save();
        }

		/// <summary>
		/// Start update coroutine
		/// </summary>
		private	void	StartUpdateCoroutine()
		{
			if (null == _updateCoroutine)
			{
				_updateCoroutine	=	StartCoroutine(UpdateLocalTimers());
			}
		}

		/// <summary>
		/// 
		/// </summary>
		private	void	StopUpdateCoroutine()
		{
			if (null != _updateCoroutine)
			{
				StopCoroutine(_updateCoroutine);

				_updateCoroutine	=	null;
			}
		}

		/// <summary>
		/// Create a local timer data
		/// </summary>
		/// <returns>A local timer data</returns>
		private	LocalTimerData	CreateLocalTimer(string pName, string pEndTimestamp, string pCurrentTimestamp, GameEvent pEvent)
		{
            LocalTimerData lNewTimerData = new LocalTimerData();
            lNewTimerData.Name = pName;
            lNewTimerData.Event = pEvent;

            DateTime epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

            if (null != pCurrentTimestamp)
            {
       
                TimeSpan timeSpanCurrent = TimeSpan.FromSeconds(double.Parse(pCurrentTimestamp));
                DateTime StartDateTime = epoch.Add(timeSpanCurrent).ToLocalTime();

                TimeSpan timeSpanEnd = TimeSpan.FromSeconds(double.Parse(pEndTimestamp));
                DateTime EndDateTime = epoch.Add(timeSpanEnd).ToLocalTime();

            
                lNewTimerData.StartDate = StartDateTime;
                lNewTimerData.EndDate = EndDateTime;
                lNewTimerData.TimeLeft = lNewTimerData.EndDate - lNewTimerData.StartDate;
                lNewTimerData.TimeLeft = lNewTimerData.TimeLeft.Add(TimeSpan.FromMinutes(1));
                //Debug.Log(pName + " BEGIN > " + StartDateTime.ToLongTimeString());
                //Debug.Log(pName + " END > " + EndDateTime.ToLongTimeString());
                //Debug.Log(pName + " LEFT > " + lNewTimerData.TimeLeft.ToString());

            }
            else
            {

                TimeSpan timeSpan = TimeSpan.FromSeconds(double.Parse(pEndTimestamp));
                DateTime localDateTime = epoch.Add(timeSpan).ToLocalTime();

                lNewTimerData.StartDate = DateTime.UtcNow;
                lNewTimerData.EndDate = localDateTime;
                lNewTimerData.TimeLeft = lNewTimerData.EndDate - lNewTimerData.StartDate;
            }
	
		


              
			
			return lNewTimerData;
		}

		/// <summary>
		/// Coroutine used to update local timers
		/// </summary>
		private	IEnumerator		UpdateLocalTimers()
		{
			List<LocalTimerData>	lGarbage		=	new List<LocalTimerData>();

			Assert.IsNotNull(lGarbage, "[LocalTimersManager] UpdateLocalTimers(), lGarbage is null.");

			Assert.IsNotNull(_localTimersData, "[LocalTimersManager] UpdateLocalTimers(), _localTimersData is null.");

			while (0 < _localTimersData.Count)
			{
                yield return new WaitForSeconds(1f);

				foreach (LocalTimerData lLocalTimerData in _localTimersData)
				{
					lLocalTimerData.TimeLeft	= lLocalTimerData.TimeLeft.Subtract(TimeSpan.FromSeconds(1));

                    if (lLocalTimerData.TimeLeft <= TimeSpan.Zero)
					{
						lGarbage.Add(lLocalTimerData);
					}
				}

				if (null != _onLocalTimersUpdated)
				{
					_onLocalTimersUpdated.Invoke();
				}
				if (0 < lGarbage.Count)
				{
					foreach (LocalTimerData lLocalTimerData in lGarbage)
					{
                        if (lLocalTimerData.Event != null)
                        {
                            lLocalTimerData.Event.Invoke();
                        }

                        _localTimersData.Remove(lLocalTimerData);
					}

					lGarbage.Clear();

					SaveLocalTimers();

					if (null != _onLocalTimerComplete)
					{
						_onLocalTimerComplete.Invoke();
					}
				}
			}

			_updateCoroutine	=	null;
		}

		#endregion
	}
}
﻿using Sportfaction.CasualFantasy.Events;
using System;

namespace Sportfaction.CasualFantasy.Services.Timers
{
	/// <summary>
	/// Description: Structure contains data of a local timer
	/// Author: Rémi Carreira
	/// </summary>
	[Serializable]
	public sealed class LocalTimerData
	{
		#region Variables

		// Name of the local timer
		public	string		Name		=	"";
		// Date time when local timer has been started
		public	DateTime	StartDate	=	new DateTime();
		// Date time of the local timer when has to finish
		public	DateTime	EndDate		=	new DateTime();
		// Date time contains the time left
		public	TimeSpan	TimeLeft	=	new TimeSpan();
        // Game Event is invoke when timer is finished
        public GameEvent Event          =   null;

        #endregion
    }
}

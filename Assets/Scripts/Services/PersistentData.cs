﻿using Sportfaction.CasualFantasy.Manager.Profile;

using System;
using System.IO;
using UnityEngine;

public static class PersistentData
{
    public static void SaveFile(string filename, UserData obj)
    {
        try
        {
            // Debug.Log("PersistentData = " + obj.Profile.AccessToken);
            //Write File
            string saveStatePath = Path.Combine(Application.persistentDataPath, filename + ".json");
            File.WriteAllText(saveStatePath, JsonUtility.ToJson(obj, true));

        }
        catch (Exception e)
        {
            Debug.LogWarning("PersistentData.SaveFile(): Failed to serialize object to a file " + filename + " (Reason: " + e.Message + ")");
        }
    }

    public static string LoadFile(string filename)
    {
        try
        {
            return File.ReadAllText(Path.Combine(Application.persistentDataPath, filename + ".json"));
        }
        catch (Exception e)
        {
            Debug.LogWarning("PersistentData.LoadFile(): Failed to deserialize a file " + filename + " (Reason: " + e.Message + ")");
            return null;
        }
    }
}

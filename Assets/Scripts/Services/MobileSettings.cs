﻿using UnityEngine;

namespace Sportfaction.CasualFantasy.Services
{
	/// <summary>
	/// Description: Class contains and apply all mobile settings
	/// Author: Rémi Carreira
	/// </summary>
	public sealed class MobileSettings : MonoBehaviour
	{
		#region Fields

		[SerializeField, Tooltip("Time before sleep application, -1 == Never")]
		private	int		_sleepTimeout		=	SleepTimeout.NeverSleep;
		[SerializeField, Range(0, 60), Tooltip("Target frame rate")]
		private	int		_targetFrameRate	=	30;

		#endregion

		#region Private Methods

		/// <summary>
		/// Monobehaviour method used to apply settings
		/// </summary>
		private	void	Awake()
		{
			Screen.sleepTimeout			=	_sleepTimeout;

			Application.targetFrameRate	=	_targetFrameRate;
		}

		#endregion
	}
}
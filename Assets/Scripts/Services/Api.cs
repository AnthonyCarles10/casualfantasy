﻿﻿using Sportfaction.CasualFantasy.Config;
using Sportfaction.CasualFantasy.Manager.Profile;
using Sportfaction.CasualFantasy.Services;

using SportFaction.CasualFootballEngine;

using BestHTTP;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Assertions;
using PaperPlaneTools;

namespace Sportfaction.CasualFantasy
{	
	/// <summary>
	/// Description: Singleton that manages REST every call to server
	/// author: antoine.demurel
	/// </summary>
    public class Api : Singleton<Api>
    {
        protected Api() { } // guarantee this will be always a singleton only - can't use the constructor!

        #region Variable

		public enum Methods { GET, HEAD, PATCH, POST, PUT }

		public delegate void OnRequestSuccess(string pCurrentUri, HTTPMethods pMethod, string pData);
        public event OnRequestSuccess OnSuccess;

		public delegate void OnRequestFailed(string pCurrentUri, string pStatus);
		public event OnRequestFailed OnFailure;

        public delegate void OnBadRequestEvent(string pCurrentUri, string pData);
        public event OnBadRequestEvent OnBadRequest;

        public int MaxRequest = 5;                                                // Number of times a request can be sent

        
        public string BaseUrl = "";    // URL prefix for API calls
		//private string _maintenanceUrl = "";                                      // Full URL for maintenance API calls
        public string _status = "";                                              // Request status info
        private HTTPRequest _refreshRequest = null;                               // Refresh token request
		private HTTPRequest _updateDeviceRequest = null;                          // Update Device request
        private int _pingTry = 0;
		private int _refreshTry = 0;
		private string _body = null;
        private int _retryFailedRequest = 0;

        public Queue<SFHTTPRequest> RequestsQueue = new Queue<SFHTTPRequest>();

        // Coroutine used for reconnect max delay
        private IEnumerator _performRequest = null;

        #endregion

        #region Public Methods

        /// <summary>
        /// Generates full API URL to call (simple version with no parameters)
        /// </summary>
        /// <returns>The full URL</returns>
        /// <param name="pUrl">URL</param>
        /// <param name="pWithDeviceId">Add or not the device id to the url</param>
        public string GenerateUrl(string pUrl, bool pWithDeviceId = false)
        {

            if(String.IsNullOrEmpty(BaseUrl))
            {
                BaseUrl = "https://dev.casualfantasyfootball.com/api/";
            }
			if (true == pWithDeviceId)
			{
				#if UNITY_EDITOR
					return BaseUrl + pUrl + "?device_id=" + ApiConstants.editorDeviceId;
				#else
					return BaseUrl + pUrl + "?device_id=" + SystemInfo.deviceUniqueIdentifier;
				#endif
			}
			else
			{
				return BaseUrl + pUrl;
			}
        }

		/// <summary>
		/// Generates full API URL to call (version with query parameters to add)
		/// </summary>
		/// <returns>The full URL</returns>
		/// <param name="pUrl">URL</param>
		/// <param name="pQueryParams">URL query parameters</param>
        public string GenerateUrl(string pUrl, Dictionary<string, string> pQueryParams)
        {
			#if UNITY_EDITOR
				string lFormatedUrl = BaseUrl + pUrl + "?device_id=" + ApiConstants.editorDeviceId;
			#else
				string lFormatedUrl = BaseUrl + pUrl + "?device_id=" + SystemInfo.deviceUniqueIdentifier;
			#endif

            if (pQueryParams != null && pQueryParams.Count > 0)
            {
                foreach (KeyValuePair<string, string> q in pQueryParams)
                {
                    lFormatedUrl += "&" + q.Key + "=" + q.Value;

					//if (lFormatedUrl == BaseUrl + pUrl)
                    //    lFormatedUrl += "?" + q.Key + "=" + q.Value;
                    //else
                        //lFormatedUrl += "&=" + q.Key + "=" + q.Value;
                }
            }

            return lFormatedUrl;
        }

		/// <summary>
		/// Retrieves token from saved profile (if it exists) and adds it to the request header
		/// If token or profile missing => load landing scene
		/// </summary>
		public void Authenticate(HTTPRequest pRequest)
		{
			// load Profile
            //string lData = PersistentData.LoadFile("user");
            
            //if (lData == null)
            //{
            //    SceneManager.LoadScene("Menu");
            //}
            //else
            //{
            //    _user = JsonUtility.FromJson<UserData>(lData);
                
            //    #if DEVELOPMENT_BUILD
            //        Debug.Log("Bearer " + _user.Profile.AccessToken);
            //    #endif
                
            //    pRequest.SetHeader("Authorization", "Bearer " + _user.Profile.AccessToken);
            //}

			Assert.IsNotNull(UserData.Instance, "[API] Authenticate(), UserData.Instance is null.");

            pRequest.SetHeader("Authorization", "Bearer " + UserData.Instance.AccessToken);
		}

		/// <summary>
		/// Sets up the API request
		/// </summary>
		/// <returns>The request</returns>
		/// <param name="pUrl">Full URL to call</param>
		/// <param name="pMethod">Method type</param>
		/// <param name="pAuthenticationRequired">Whether authentication token is needed or not</param>
		public HTTPRequest SetUpRequest(String pUrl, Methods pMethod, bool pAuthenticationRequired)
		{
			HTTPRequest lRequest = null;
            _body = null;

            switch (pMethod)
			{
				case Methods.GET:
					lRequest = new HTTPRequest(new Uri(pUrl), HTTPMethods.Get);
					break;
				case Methods.HEAD:
					lRequest = new HTTPRequest(new Uri(pUrl), HTTPMethods.Head);
					break;
				case Methods.PATCH:
					lRequest = new HTTPRequest(new Uri(pUrl), HTTPMethods.Patch);
					break;
				case Methods.PUT:
					lRequest = new HTTPRequest(new Uri(pUrl), HTTPMethods.Put);
					break;
			}

			lRequest.SetHeader("Content-Type", "application/json; charset = UTF-8");

			if (pAuthenticationRequired)
			{
				Authenticate(lRequest);
			}

			return lRequest;
		}

		/// <summary>
		/// Sets up the POST API request
		/// </summary>
		/// <returns>The request</returns>
		/// <param name="pUrl">Full URL to call</param>
		/// <param name="pAuthenticationRequired">Whether authentication token is needed or not</param>
        /// <param name="pBody">Data to post</param>
		/// <param name="pMethodType">HTTP call method</param>
		public HTTPRequest SetUpRequest(String pUrl, bool pAuthenticationRequired, object pBody, HTTPMethods pMethodType = HTTPMethods.Post)
		{
            
            _body = null;
            HTTPRequest lRequest = new HTTPRequest(new Uri(pUrl), pMethodType);
            _body = JsonConvert.SerializeObject(pBody);

            //Debug.Log(_body);
            lRequest.RawData = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(pBody));
			lRequest.SetHeader("Content-Type", "application/json; charset = UTF-8");

			if (pAuthenticationRequired)
			{
				Authenticate(lRequest);
			}

			return lRequest;
		}

        /// <summary>
        /// Sends the request and handles the server's response
        /// </summary>
        /// <returns>The request</returns>
        /// <param name="pRequest">Request to send</param>
        /// <param name="pTry">Current number of request call tries</param>
        public IEnumerator SendRequest(HTTPRequest pRequest, bool pNoWaitResponse = false)
        {

            SFHTTPRequest lRequest = new SFHTTPRequest(pRequest, _body, pNoWaitResponse);
            _body = "";

        
            RequestsQueue.Enqueue(lRequest);

            if(null == _performRequest)
            {
                _performRequest = PerformRequest();
                StartCoroutine(_performRequest);
            }

            yield return new WaitForSeconds(0f);

        }

        public IEnumerator PerformRequest()
        {

            while (RequestsQueue.Count > 0)
            {
                Debug.Log("Request Queue = " + RequestsQueue.Count);
                SFHTTPRequest lSFHTTPRequest = RequestsQueue.Peek();

                #if DEVELOPMENT_BUILD || UNITY_EDITOR
                    if (lSFHTTPRequest.Request.MethodType == HTTPMethods.Post ||
                        lSFHTTPRequest.Request.MethodType == HTTPMethods.Patch ||
                        lSFHTTPRequest.Request.MethodType == HTTPMethods.Put)
                    {
                        Debug.LogFormat("<color=#D3D3D3>API[{0}]</color> : \n{1}\nBody : {2}",
                            lSFHTTPRequest.Request.MethodType,
                            lSFHTTPRequest.Request.CurrentUri,
                            lSFHTTPRequest.Body
                        );
                    }
                    else
                    {
                        Debug.LogFormat("<color=#D3D3D3>API[{0}]</color> : \n{1}",
                        lSFHTTPRequest.Request.MethodType,
                        lSFHTTPRequest.Request.CurrentUri
                        );
                    }
                #endif

                //RequestsQueue[i].DisableCache = true;
                lSFHTTPRequest.Request.Send();

                if(true == lSFHTTPRequest.NoWaitResponse)
                {

                    #if DEVELOPMENT_BUILD || UNITY_EDITOR
                        Debug.Log("<color=#9ACD32>API-Success !</color>");
                    #endif
                }
                else
                {
                    _retryFailedRequest++;

                    while (lSFHTTPRequest.Request.State < HTTPRequestStates.Finished)
                    {
                        yield return new WaitForSeconds(0.1f);

                        #if DEVELOPMENT_BUILD || UNITY_EDITOR
                            Debug.LogFormat("<color=#D3D3D3>API-Wait</color> ");
                        #endif
                    }

                    // Check the outcome of our request.
                    switch (lSFHTTPRequest.Request.State)
                    {
                        // The request finished without any problem.
                        case HTTPRequestStates.Finished:

                            if (lSFHTTPRequest.Request.Response.IsSuccess)
                            {

                                #if DEVELOPMENT_BUILD || UNITY_EDITOR
                                    Debug.Log("<color=#9ACD32>API-Success !</color>");
                                #endif
                                if (OnSuccess != null)
                                {
                                    _retryFailedRequest = 0;

                                    OnSuccess(lSFHTTPRequest.Request.CurrentUri.AbsoluteUri, lSFHTTPRequest.Request.MethodType, lSFHTTPRequest.Request.Response.DataAsText);

                                }
                            }
                            else
                            {
                                switch (lSFHTTPRequest.Request.Response.StatusCode)
                                {
                                    case 401:
                                        #if DEVELOPMENT_BUILD || UNITY_EDITOR
                                            Debug.Log("<color=#9ACD32>[401] API-Failed !</color>");
                                        #endif
                                        if (_refreshTry == 0) //Refresh token : 1 attempt
                                        {
                                            if (GenerateUrl(ApiConstants.LoginDefault) == lSFHTTPRequest.Request.CurrentUri.AbsoluteUri)
                                            {
                                                OnBadRequest(lSFHTTPRequest.Request.CurrentUri.AbsoluteUri, lSFHTTPRequest.Request.Response.DataAsText);
                                            }
                                            else
                                            {
                                                yield return RefreshToken(UserData.Instance.RefreshToken, lSFHTTPRequest.Request);
                                                _refreshTry++;
                                            }
                                        }
                                        break;
                                    case 406:
                                        //string[] m_buttonsArray = new string[] { "oui", "non" };
                                        //AlertDialogs.Instance.ShowAlert(
                                        //    "Nouvel Appareil?",
                                        //    "Vous êtes déjà connecté sur un autre appareil, Voulez-vous vous connecter avec celui-ci?",
                                        //    m_buttonsArray, (_buttonPressed) =>
                                        //    {
                                        //        if(_buttonPressed == "oui") {
                                        //            StartCoroutine(UpdateCurrentDevice(pRequest));
                                        //        } else {
                                        //            // Application.Quit();
                                        //            // TODO Load Scene 
                                        //        }

                                        //    }
                                        //);

                                        break;
                                    case 500:
                                        #if DEVELOPMENT_BUILD || UNITY_EDITOR
                                            Debug.Log("<color=#9ACD32>[500] API-Failed !</color>");
                                        #endif
                                        if (_retryFailedRequest == 1)
                                        {
                                            yield return Ping(lSFHTTPRequest.Request, _retryFailedRequest);
                                        }
                                        else if (_retryFailedRequest < MaxRequest)
                                        {
                                            yield return SendRequest(lSFHTTPRequest.Request);
                                        }
                                        else
                                        {
                                            DisplayMaintenance();
                                            if (OnFailure != null)
                                            {
                                                OnFailure(lSFHTTPRequest.Request.CurrentUri.AbsoluteUri, _status);
                                            }
                                        }
                                        break;
                                    case 503:
                                        #if DEVELOPMENT_BUILD || UNITY_EDITOR
                                            Debug.Log("<color=#9ACD32>[503] API-Failed !</color>");
                                        #endif
                                        DisplayMaintenance();
                                        break;

                                    case 400:

                                        #if DEVELOPMENT_BUILD || UNITY_EDITOR
                                        Debug.Log("<color=#9ACD32>[400] API-Failed ! /n" + lSFHTTPRequest.Request.Response.DataAsText + "</color>");
                                        #endif

                                        OnBadRequest(lSFHTTPRequest.Request.CurrentUri.AbsoluteUri, lSFHTTPRequest.Request.Response.DataAsText);
                                        break;

                                    default:
                                        Debug.LogError(_status);
                                        if (OnFailure != null)
                                        {
                                            OnFailure(lSFHTTPRequest.Request.CurrentUri.AbsoluteUri, _status);
                                        }
                                        break;
                                }

                            }


                            break;

                        // The request finished with an unexpected error. The request's Exception property may contain more info about the error.
                        case HTTPRequestStates.Error:
                            _status = "Request Finished with Error!\n" + (lSFHTTPRequest.Request.Exception != null ? (lSFHTTPRequest.Request.Exception.Message + "\n" + lSFHTTPRequest.Request.Exception.StackTrace) : "No Exception");
                            Debug.LogError(_status);
                            ErrorNetwork();
                            if (OnFailure != null)
                            {
                                OnFailure(lSFHTTPRequest.Request.CurrentUri.AbsoluteUri, _status);
                            }
                            break;

                        // The request aborted, initiated by the user.
                        case HTTPRequestStates.Aborted:
                            _status = "Request Aborted!";
                            Debug.LogError(_status);
                            ErrorNetwork();
                            if (OnFailure != null)
                            {
                                OnFailure(lSFHTTPRequest.Request.CurrentUri.AbsoluteUri, _status);
                            }
                            break;

                        // Connecting to the server is timed out.
                        case HTTPRequestStates.ConnectionTimedOut:
                            _status = "Connection Timed Out!";
                            Debug.LogError(_status);
                            ErrorNetwork();
                            if (OnFailure != null)
                            {
                                OnFailure(lSFHTTPRequest.Request.CurrentUri.AbsoluteUri, _status);
                            }
                            break;

                        // The request didn't finished in the given time.
                        case HTTPRequestStates.TimedOut:
                            _status = "Processing the request Timed Out! (" + lSFHTTPRequest.Request.CurrentUri + ")";
                            Debug.LogError(_status);
                            ErrorNetwork();
                            if (OnFailure != null)
                            {
                                OnFailure(lSFHTTPRequest.Request.CurrentUri.AbsoluteUri, _status);
                            }
                            break;

                    }

                }


                RequestsQueue.Dequeue();
            }
  
            StopCoroutine(_performRequest);
            _performRequest = null;

        }



		/// <summary>
		/// Pings the server to check if APIs are reachable
		/// </summary>
		/// <returns>The ping.</returns>
		/// <param name="pRequest">Request to relaunch if APIs reachable</param>
		/// <param name="pTry">Current number of request call tries</param>
		public IEnumerator Ping(HTTPRequest pRequest, int pTry)
		{
			HTTPRequest pingRequest = new HTTPRequest(new Uri(GenerateUrl("core/ping")), HTTPMethods.Get);
			pingRequest.SetHeader("Content-Type", "application/json");

			pingRequest.Send();
			_pingTry++;

			while (pingRequest.State < HTTPRequestStates.Finished)
			{
				yield return new WaitForSeconds(0.1f);

				_status = "Loading...";
			}
			Debug.Log("Pinging");

			// Check the outcome of our request.
			switch (pingRequest.State)
			{
				// The request finished without any problem.
				case HTTPRequestStates.Finished:
					if (pingRequest.Response.IsSuccess)
					{
						// Resend previously failed request
						_pingTry = 0;
						yield return SendRequest(pRequest);
					}
					else if (_pingTry < MaxRequest)
					{
						yield return Ping(pRequest, pTry);
					}
					else
					{
						_pingTry = 0;
						_status = string.Format(
							"Ping finished successfully, but the server sent an error. Status Code: {0}-{1} Message: {2}",
							pingRequest.Response.StatusCode,
							pingRequest.Response.Message,
							pingRequest.Response.DataAsText
						);
						Debug.LogError(_status);

						if (OnFailure != null)
						{
							OnFailure(pingRequest.CurrentUri.AbsoluteUri, _status);
						}

						yield return null;
					}

					break;

				// The request finished with an unexpected error.
				default:
					if (_pingTry < MaxRequest)
					{
						yield return Ping(pRequest, pTry);
					}
					else
					{
						_status = "Ping finished with Error! " + (pingRequest.Exception != null ? (pingRequest.Exception.Message + "\n" + pingRequest.Exception.StackTrace) : "No Exception");
						Debug.LogError(_status);
						if (OnFailure != null)
						{
							OnFailure(pingRequest.CurrentUri.AbsoluteUri, _status);
						}
					}
					break;
			}
		}

		/// <summary>
		/// Checks whether the server's under maintenance or not
		/// </summary>
		//public void CheckMaintenance()
		//{
  //          _maintenanceUrl = Api.Instance.GenerateUrl("core/maintenance/is_activated");
		//	HTTPRequest lMaintenance = SetUpRequest(_maintenanceUrl, Api.Methods.GET, false);
		//	StartCoroutine(SendRequest(lMaintenance, 0));
		//	Api.Instance.OnSuccess += DisplayMaintenance;
		//}

        /// <summary>
        /// Popin Miantenance
        /// </summary>
        private void DisplayMaintenance()
        {

            new Alert(I2.Loc.LocalizationManager.GetTranslation("MaintenanceTitle"),
                I2.Loc.LocalizationManager.GetTranslation("MaintenanceMessage"))
                .SetPositiveButton(I2.Loc.LocalizationManager.GetTranslation("MaintenanceButton"), () => {
                    SceneManager.LoadScene("Splashscreen");
                })
                .Show();
        }

        /// <summary>
        /// Popin Error Network
        /// </summary>
        private void ErrorNetwork()
        {
            new Alert(I2.Loc.LocalizationManager.GetTranslation("NetworkConnectivityAlertTitle"),
                I2.Loc.LocalizationManager.GetTranslation("NetworkConnectivityAlertMessage"))
                .SetPositiveButton(I2.Loc.LocalizationManager.GetTranslation("NetworkConnectivityAlertButton"), () => {
                    SceneManager.LoadScene("Splashscreen");
                })
                .Show();
        }

        /// <summary>
        /// Retrieves new token for current connected user
        /// </summary>
        /// <param name="pRefreshToken">Refresh token value</param>
        /// <param name="pRequest">Request call data</param>
        public IEnumerator RefreshToken(string pRefreshToken, HTTPRequest pRequest)
        {
			object body = new { refresh_token = pRefreshToken };
				
			_refreshRequest = new HTTPRequest(new Uri(GenerateUrl("users/token/refresh")), HTTPMethods.Post);
			_refreshRequest.RawData = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(body));
			_refreshRequest.SetHeader("Content-Type", "application/json");
			_refreshRequest.Send();

			while (_refreshRequest.State < HTTPRequestStates.Finished)
			{
				yield return new WaitForSeconds(0.1f);

				_status = "Loading...";
			}
			// Check the outcome of our request.
			switch (_refreshRequest.State)
			{
				// The request finished without any problem.
				case HTTPRequestStates.Finished:
					if (_refreshRequest.Response.IsSuccess)
					{
						#if DEVELOPMENT_BUILD && UNITY_EDITOR
                            Debug.Log("REFRESH TOKEN");
						#endif

						Assert.IsNotNull(UserData.Instance, "[Api] RefreshToken(), UserData.Instance is null.");

                        JToken lJToken = JToken.Parse(_refreshRequest.Response.DataAsText);

                        UserData.Instance.AccessToken	=	lJToken.Value<string>("token");
                        UserData.Instance.RefreshToken	=	lJToken.Value<string>("refresh_token");

                        _refreshTry = 0;

                        pRequest.SetHeader("Authorization", "Bearer " + UserData.Instance.AccessToken);

                        yield return SendRequest(pRequest);
					}
					else
					{
						_status = string.Format(
							"Refresh token finished successfully, but the server sent an error. Status Code: {0}-{1} Message: {2}",
							_refreshRequest.Response.StatusCode,
							_refreshRequest.Response.Message,
							_refreshRequest.Response.DataAsText
						);
						Debug.LogError(_status);

						if (OnFailure != null)
						{
							OnFailure(_refreshRequest.CurrentUri.AbsoluteUri, _status);
						}

						yield return null;
					}

					break;

				// The request finished with an unexpected error.
				default:
					_status = "Refresh token finished with Error! " + (_refreshRequest.Exception != null ? (_refreshRequest.Exception.Message + "\n" + _refreshRequest.Exception.StackTrace) : "No Exception");
					Debug.LogError(_status);
					if (OnFailure != null)
					{
						OnFailure(_refreshRequest.CurrentUri.AbsoluteUri, _status);
					}
					break;
			}
        }


        /// <summary>
        /// Update Current Device to use App
        /// </summary>
        /// <param name="pRequest">last request in memory</param>
        public IEnumerator UpdateCurrentDevice(HTTPRequest pRequest)
        {
			Assert.IsNotNull(UserData.Instance, "[Api] UpdateCurrentDevice(), UserData.Instance is null.");

            object body = new { current_device_id = SystemInfo.deviceUniqueIdentifier };

            _updateDeviceRequest = new HTTPRequest(new Uri(GenerateUrl("users/update_current_device")), HTTPMethods.Post);
            _updateDeviceRequest.RawData = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(body));
            _updateDeviceRequest.SetHeader("Content-Type", "application/json");
            _updateDeviceRequest.SetHeader("Authorization", "Bearer " + UserData.Instance.AccessToken);
            _updateDeviceRequest.Send();

            while (_updateDeviceRequest.State < HTTPRequestStates.Finished)
            {
                yield return new WaitForSeconds(0.1f);

                _status = "Loading...";
            }
            // Check the outcome of our request.
            switch (_updateDeviceRequest.State)
            {
                // The request finished without any problem.
                case HTTPRequestStates.Finished:
                    if (_updateDeviceRequest.Response.IsSuccess)
                    {
                        SendRequest(pRequest);

                    }
                    else
                    {
                        _status = string.Format(
                            "users/update_current_device finished successfully, but the server sent an error. Status Code: {0}-{1} Message: {2}",
                            _updateDeviceRequest.Response.StatusCode,
                            _updateDeviceRequest.Response.Message,
                            _updateDeviceRequest.Response.DataAsText
                        );
                        Debug.LogError(_status);

                        if (OnFailure != null)
                        {
                            OnFailure(_updateDeviceRequest.CurrentUri.AbsoluteUri, _status);
                        }

                        yield return null;
                    }

                    break;

                // The request finished with an unexpected error.
                default:
                    _status = "users/update_current_device finished with Error! " + (_updateDeviceRequest.Exception != null ? (_updateDeviceRequest.Exception.Message + "\n" + _updateDeviceRequest.Exception.StackTrace) : "No Exception");
                    Debug.LogError(_status);
                    if (OnFailure != null)
                    {
                        OnFailure(_updateDeviceRequest.CurrentUri.AbsoluteUri, _status);
                    }
                    break;
            }
        }

        private void Start()
        {
            BaseUrl = (null == ConfigData.Instance || ConfigData.Instance.ENV == eConfig.DEV) ? ConfigData.Instance.API_URL_DEV : ConfigData.Instance.API_URL_PROD;
        }
        #endregion
    }

    
}

public class SFHTTPRequest
{
    public HTTPRequest Request;
    public string Body;
    public bool NoWaitResponse;

    public SFHTTPRequest(HTTPRequest pRequest, string pBody, bool pNoWaitResponse)
    {
        Request = pRequest;
        Body = pBody;
        NoWaitResponse = pNoWaitResponse;
    }
}


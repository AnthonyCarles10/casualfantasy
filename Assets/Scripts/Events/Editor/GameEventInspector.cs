﻿using UnityEngine;
using UnityEngine.Assertions;

namespace Sportfaction.CasualFantasy.Events.Editor
{
	/// <summary>
	/// Description: Class used to override the GameEvent Inspector
	/// Author: Rémi Carreira
	/// </summary>
	[UnityEditor.CustomEditor(typeof(GameEvent))]
	public sealed class GameEventInspector : UnityEditor.Editor
	{
		#region Public Methods

		public override void OnInspectorGUI()
		{
			base.OnInspectorGUI();

			if (GUILayout.Button("Invoke"))
			{
				GameEvent	lEvent	=	target as GameEvent;

				Assert.IsNotNull(lEvent, "GameEventInspector: OnInspectorGUI() failed, lEvent is null.");

				lEvent.Invoke();
			}
		}

		#endregion
	}
}
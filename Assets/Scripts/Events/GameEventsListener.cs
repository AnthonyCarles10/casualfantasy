﻿using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Events;

namespace Sportfaction.CasualFantasy.Events
{
	/// <summary>
	/// Description: MonoBehaviour class used to start & stop listening specific game events
	/// Author: Rémi Carreira
	/// </summary>
	public sealed class GameEventsListener : MonoBehaviour
	{
		#region Fields

		[SerializeField, Tooltip("All game events to listen")]
		private	GameEvent[]		_gameEvents	=	new GameEvent[0];
		[SerializeField, Tooltip("")]
		private	UnityEvent		_response	=	new UnityEvent();

		#endregion

		#region Private Methods

		/// <summary>
		/// MonoBehaviour method used to start listening all game events
		/// </summary>
		private	void	OnEnable()
		{
			Assert.IsNotNull(_gameEvents, "[GameEventsListener] OnEnable(), _gameEvents is null.");

			Assert.IsNotNull(_response, "[GameEventsListener] OnEnable(), _response is null.");

			for (int lIndex = 0; lIndex < _gameEvents.Length; ++lIndex)
			{
				Assert.IsNotNull(_gameEvents[lIndex], "[GameEventsListener] OnEnable(), _gameEvents[" + lIndex + "] is null.");

				_gameEvents[lIndex].StartListening(_response.Invoke);
			}
		}

		/// <summary>
		/// MonoBehaviour method used to stop listening all game events
		/// </summary>
		private	void	OnDisable()
		{
			Assert.IsNotNull(_gameEvents, "[GameEventsListener] OnDisable(), _gameEvents is null.");

			Assert.IsNotNull(_response, "[GameEventsListener] OnDisable(), _response is null.");

			for (int lIndex = 0; lIndex < _gameEvents.Length; ++lIndex)
			{
				Assert.IsNotNull(_gameEvents[lIndex], "[GameEventsListener] OnDisable(), _gameEvents[" + lIndex + "] is null.");

				_gameEvents[lIndex].StopListening(_response.Invoke);
			}
		}

		#endregion
	}
}
﻿using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Events;

namespace Sportfaction.CasualFantasy.Events
{
	/// <summary>
	/// Description: Scriptable object used to contain actions for a specific game event
	/// Author: Rémi Carreira
	/// </summary>
	[CreateAssetMenu(fileName = "New Game Event", menuName = "CasualFantasy/Game Event")]
	public sealed class GameEvent : ScriptableObject
	{
		#region Fields

		// If true, this game event is log in development build
		public	bool		LogInDevelopmentBuild	=	true;

		// Event contains some actions
		private	UnityEvent	_event	=	new UnityEvent();

		#endregion

		#region Public Methods

		/// <summary>
		/// Invoke game event
		/// </summary>
		public	void	Invoke()
		{
			#if UNITY_EDITOR && DEVELOPMENT_BUILD
				if (true == LogInDevelopmentBuild)
				{
					Debug.LogFormat("<color=magenta>[GameEvent]</color> Invoke(): {0}", this.name);
				}
			#endif

			Assert.IsNotNull(_event, "GameEvent: Invoke() failed, _event is null.");

			_event.Invoke();
		}

		/// <summary>
		/// Add action to game event
		/// </summary>
		/// <param name="pAction">Action to add</param>
		public	void	StartListening(UnityAction pAction)
		{
			Assert.IsNotNull(pAction, "GameEvent: StartListening() failed, pAction is null.");

			Assert.IsNotNull(_event, "GameEvent: StartListening() failed, _event is null.");

			_event.AddListener(pAction);
		}

		/// <summary>
		/// Remove action from game event
		/// </summary>
		/// <param name="pAction">Action to remove</param>
		public	void	StopListening(UnityAction pAction)
		{
			Assert.IsNotNull(pAction, "GameEvent: StopListening() failed, pAction is null.");

			Assert.IsNotNull(_event, "GameEvent: StopListening() failed, _event is null.");

			_event.RemoveListener(pAction);
		}

		#endregion
	}
}
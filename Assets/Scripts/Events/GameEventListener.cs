﻿using UnityEngine;
using UnityEngine.Events;

namespace Sportfaction.CasualFantasy.Events
{
    /// <summary>
    /// Description: MonoBehaviour class used to start & stop listening a specific game event
    /// Author: Rémi Carreira
    /// </summary>
    public sealed class GameEventListener : MonoBehaviour
	{
		#region Fields

		[SerializeField, Tooltip("Game event to listen")]
		private	GameEvent	_gameEvent	=	null;				// Game event to listen
		[SerializeField, Tooltip("Response called when game event is invoke")]
		private	UnityEvent	_response	=	new UnityEvent();	// Response called when game event is invoke

		#endregion

		#region Private Methods

		/// <summary>
		/// MonoBehaviour method used to start listening game event
		/// </summary>
		private	void	OnEnable()
		{
			Debug.Assert(null != _gameEvent, "GameEventListener: OnEnable() failed, _gameEvent is null on object " + gameObject.name, gameObject);

			Debug.Assert(null != _response, "GameEventListener: OnEnable() failed, _response is null on object " + gameObject.name, gameObject);

			_gameEvent.StartListening(_response.Invoke);
		}

		/// <summary>
		/// MonoBehaviour method used to stop listening game event
		/// </summary>
		private	void	OnDisable()
		{
			Debug.Assert(null != _gameEvent, "GameEventListener: OnDisable() failed, _gameEvent is null on object " + gameObject.name, gameObject);

			Debug.Assert(null != _response, "GameEventListener: OnDisable() failed, _response is null on object " + gameObject.name, gameObject);

			_gameEvent.StopListening(_response.Invoke);
		}

		#endregion
	}
}
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeColorOnRuntime : MonoBehaviour
{
	#region Fields
	
	[SerializeField]
	Color	ColorToApply_AtRuntime	=	Color.white;
	[SerializeField, Tooltip("Sprite renderer to set")]
	private	SpriteRenderer	_sprite	=	null;

	private	ParticleSystem[]	particlesList;

	#endregion

	#region Public Methods
	
	public	void	ChangeColor(Color pColorToApply)
	{
		ColorToApply_AtRuntime	=	pColorToApply;
		_setParticlesColor();
	}

	#endregion

	#region Private Methods

	private	void	Awake()
	{
		particlesList = GetComponentsInChildren<ParticleSystem>();
	}

	private	void	Start()
	{
		_setParticlesColor();
	}
	
	private	void	_setParticlesColor()
	{
		if (null != _sprite)
			_sprite.color	=	ColorToApply_AtRuntime;
		
		if (null == particlesList) return;
		
		for (int i = 0; i < particlesList.Length; i++)
		{
			if (particlesList[i] != null)
			{
				var main = particlesList[i].main;
				main.startColor = ColorToApply_AtRuntime;
			}
		}
	}

	#endregion
}

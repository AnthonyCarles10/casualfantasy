namespace BugseePlugin
{
	public static class BugseePluginVersion 
	{
		public const string BUGSEE_PLUGIN_VERSION = "1.6.2";
		public const string BUGSEE_PLUGIN_BUILD = "4e746d1-";
	}
}

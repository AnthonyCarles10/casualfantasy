﻿namespace UnityEngine
{
	/// <summary>
	/// Description: MonoBehaviour class used to test custom attributes
	/// Author: Rémi Carreira
	/// </summary>
	public sealed class AttributesTest : MonoBehaviour
	{
		#region Fields

		#pragma warning disable 414

		[SerializeField, MinValue(0), Tooltip("Min Integer Value is 0")]
		private	int		_minIntegerValue	=	5;
		[SerializeField, MaxValue(20), Tooltip("Max Integer Value is 20")]
		private	int		_maxIntegerValue	=	10;
		[SerializeField, MinValue(0f), Tooltip("Min Float Value is 0f")]
		private	float	_minFloatValue		=	5;
		[SerializeField, MaxValue(20f), Tooltip("Max Float Value is 20f")]
		private	float	_maxFloatValue		=	10;
		[SerializeField, ReadOnly, Tooltip("Read Only field")]
		private	string		_readOnly	=	"ReadOnly";

		#pragma warning restore 414

		#endregion

		#region Public Methods

		[EditorButton]
		private	void	Test()
		{
			Debug.Log("AttributesTest: Test() occured.");
		}

		#endregion
	}
}
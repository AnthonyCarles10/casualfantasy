﻿using UnityEngine;

namespace UnityEditor
{
	/// <summary>
	/// Description: Drawer used to display min value attribute
	/// Author: Rémi Carreira
	/// </summary>
	[CustomPropertyDrawer(typeof(MinValueAttribute))]
	public sealed class MinValueDrawer : PropertyDrawer
	{
		#region Public Methods

		/// <summary>
		/// Inherited method | Specify the gui's size in inspector.
		/// </summary>
		/// <param name="property">The SerializedProperty to make the custom GUI for.</param>
		/// <param name="label">The label of this property.</param>
		/// <returns>The height in pixels.</returns>
		public	override	float	GetPropertyHeight(SerializedProperty property, GUIContent label)
		{
			return EditorGUI.GetPropertyHeight(property, label, true);
		}

		/// <summary>
		/// Inherited method | Make my own gui
		/// </summary>
		/// <param name="position">Rectangle on the screen to use for the property GUI.</param>
		/// <param name="property">The SerializedProperty to make the custom GUI for.</param>
		/// <param name="label">The label of this property.</param>
		public	override	void	OnGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			MinValueAttribute	lAttribute	=	(MinValueAttribute)base.attribute;

			if (property.propertyType == SerializedPropertyType.Integer)
			{
				if (property.intValue < lAttribute.IntValue)
				{
					property.intValue	=	lAttribute.IntValue;
				}
			}
			else if (property.propertyType == SerializedPropertyType.Float)
			{
				if (property.floatValue < lAttribute.FloatValue)
				{
					property.floatValue	=	lAttribute.FloatValue;
				}
			}

			EditorGUI.PropertyField(position, property, label, true);
		}

		#endregion
	}
}
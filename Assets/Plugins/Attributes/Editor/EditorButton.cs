﻿using UnityEngine;
using System;
using System.Linq;
using System.Reflection;
using System.Collections.Generic;

namespace UnityEditor
{
	/// <summary>
	/// Description: Override Monobehaviour Editor to add button(s)
	/// Author: Rémi Carreira
	/// </summary>
	[CustomEditor(typeof(MonoBehaviour), true)]
	public sealed class EditorButton : Editor
	{
		#region Public Methods

		/// <summary>
		/// Inherited function | Customize inspector gui
		/// </summary>
		public override void OnInspectorGUI()
		{
			base.OnInspectorGUI();

			MonoBehaviour	lMono	=	target	as MonoBehaviour;

			IEnumerable<MemberInfo> lMethods = lMono.GetType()
					.GetMembers(BindingFlags.Instance | BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic)
					.Where(o => Attribute.IsDefined(o, typeof (EditorButtonAttribute)));

			foreach (MemberInfo lMemberInfo in lMethods)
			{
				if (GUILayout.Button(lMemberInfo.Name))
				{
					MethodInfo lMethod = lMemberInfo as MethodInfo;

					lMethod.Invoke(lMono, null);
				}
			}
		}

		#endregion
	}
}
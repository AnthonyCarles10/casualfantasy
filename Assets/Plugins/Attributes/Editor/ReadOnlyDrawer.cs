﻿using UnityEngine;

namespace UnityEditor
{
	/// <summary>
	/// Description: Drawer used to display unalterable variable
	/// Author: Rémi Carreira
	/// </summary>
	[CustomPropertyDrawer(typeof(ReadOnlyAttribute))]
	public sealed class ReadOnlyDrawer : PropertyDrawer
	{
		#region Public Methods

		/// <summary>
		/// Inherited method | Specify the gui's size in inspector.
		/// </summary>
		/// <param name="property">The SerializedProperty to make the custom GUI for.</param>
		/// <param name="label">The label of this property.</param>
		/// <returns>The height in pixels.</returns>
		public	override	float	GetPropertyHeight(SerializedProperty property, GUIContent label)
		{
			return EditorGUI.GetPropertyHeight(property, label, true);
		}

		/// <summary>
		/// Inherited method | Make my own gui
		/// </summary>
		/// <param name="position">Rectangle on the screen to use for the property GUI.</param>
		/// <param name="property">The SerializedProperty to make the custom GUI for.</param>
		/// <param name="label">The label of this property.</param>
		public	override	void	OnGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			GUI.enabled	=	false;

			EditorGUI.PropertyField(position, property, label, true);

			GUI.enabled	=	true;
		}

		#endregion
	}
}
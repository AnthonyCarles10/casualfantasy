﻿using UnityEngine;

namespace UnityEngine
{
	/// <summary>
	/// Description: Attribute used to define the maximum value
	/// Author: Rémi Carreira
	/// </summary>
	[System.AttributeUsage(System.AttributeTargets.Field, AllowMultiple = true)]
	public sealed class MaxValueAttribute : PropertyAttribute
	{
		#region Properties

		public	readonly	int		IntValue	=	0;	// Min integer value
		public	readonly	float	FloatValue	=	0f;	// Min float value

		#endregion

		#region Public Methods

		/// <summary>
		/// Custom Constructor
		/// </summary>
		/// <param name="pMinValue">Take integer value</param>
		public	MaxValueAttribute(int pMaxValue)
		{
			IntValue	=	pMaxValue;
		}

		/// <summary>
		/// Custom Constructor
		/// </summary>
		/// <param name="pMinValue">Take float value</param>
		public	MaxValueAttribute(float pMaxValue)
		{
			FloatValue	=	pMaxValue;
		}

		#endregion

		#region Private Methods

		/// <summary>
		/// Default Constructor
		/// </summary>
		private	MaxValueAttribute()
		{
		}

		#endregion
	}
}
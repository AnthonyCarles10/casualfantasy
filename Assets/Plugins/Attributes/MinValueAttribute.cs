﻿namespace UnityEngine
{
	/// <summary>
	/// Description: Attribute used to define a minimum value
	/// Author: Rémi Carreira
	/// </summary>
	[System.AttributeUsage(System.AttributeTargets.Field, AllowMultiple = true)]
	public sealed class MinValueAttribute : PropertyAttribute
	{
		#region Properties

		public	readonly	int		IntValue	=	0;	// Min integer value
		public	readonly	float	FloatValue	=	0f;	// Min float value

		#endregion

		#region Public Methods

		/// <summary>
		/// Custom Constructor
		/// </summary>
		/// <param name="pMinValue">Take integer value</param>
		public	MinValueAttribute(int pMinValue)
		{
			IntValue	=	pMinValue;
			FloatValue	=	(float)pMinValue;
		}

		/// <summary>
		/// Custom Constructor
		/// </summary>
		/// <param name="pMinValue">Take float value</param>
		public	MinValueAttribute(float pMinValue)
		{
			IntValue	=	(int)pMinValue;
			FloatValue	=	pMinValue;
		}

		#endregion

		#region Private Methods

		/// <summary>
		/// Default Constructor
		/// </summary>
		private	MinValueAttribute()
		{
		}

		#endregion
	}
}
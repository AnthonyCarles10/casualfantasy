﻿namespace UnityEngine
{
	/// <summary>
	/// Description: Attribute used to add a button for a specific function
	/// Author: Rémi Carreira
	/// </summary>
	[System.AttributeUsage(System.AttributeTargets.Method, AllowMultiple = true)]
	public sealed class EditorButtonAttribute : PropertyAttribute
	{
	}
}
﻿namespace UnityEngine
{
	/// <summary>
	/// Description: Attribute used to disable modification possibility
	/// Author: Rémi Carreira
	/// </summary>
	[System.AttributeUsage(System.AttributeTargets.Field, AllowMultiple = true)]
	public sealed class ReadOnlyAttribute : PropertyAttribute
	{
	}
}
#if UNITY_ANDROID || UNITY_IPHONE || UNITY_STANDALONE_OSX || UNITY_TVOS
// WARNING: Do not modify! Generated file.

namespace UnityEngine.Purchasing.Security {
    public class AppleTangle
    {
        private static byte[] data = System.Convert.FromBase64String("CQseD1hePB49HAsOWAkHHgdMfX3Nbj56+jcKIVvm1wIsA9e3fhRCuEhzEkFmXZtMhMl5bwYdjkyKPoeMb2FoLX55bGNpbH9pLXlof2B+LWx9YWgtX2JieS1OTD0TGgA9Oz05PxKc1hNKXeYI4FN0iSDmO69aQVjhZGtkbmx5ZGJjLUx4eWVif2R5dDw4Pzw5PT47VxoAPjg9Pz00Pzw5PYJ+jG3LFlYEIp+/9UlF/W01kxj4aoIFuS36xqEhLWJ9uzIMPYG6TsKl0XMvOMco2NQC22bZrykuHPqsoSs9KQsOWAkGHhBMfX1haC1OaH95dz2PDHs9AwsOWBACDAzyCQkODwy6FrCeTykfJ8oCELtAkVNuxUaNGtQ7csyKWNSqlLQ/T/bV2HyTc6xfPRwLDlgJBx4HTH19YWgtRGNuIzwbPRkLDlgJDh4ATH19YWgtX2JieV9oYWRsY25oLWJjLXllZH4tbmh/hhSE0/RGYfgKpi89D+UVM/VdBN5E1XuSPhlorHqZxCAPDgwNDK6PDAs9AgsOWBAeDAzyCQg9DgwM8j0QenojbH19YWgjbmJgImx9fWFobmwFJgsMCAgKDwwbE2V5eX1+NyIierw9VeFXCT+BZb6CENNofvJqU2ixVKoIBHEaTVscE3neuoYuNkqu2GItbGNpLW5of3lka2RubHlkYmMtfSI9jM4LBSYLDAgICg8PPYy7F4y+f2xueWRuaC1+eWx5aGBoY3l+Iz0+O1c9bzwGPQQLDlgJCx4PWF48HsQUf/hQA9hyUpb/KA63WIJAUAD8eWVif2R5dDwbPRkLDlgJDh4ATH15ZGtkbmx5aC1vdC1sY3QtfWx/eT2PCbY9jw6urQ4PDA8PDA89AAsEs/l+luPfaQLGdEI51a8z9HXyZsV9YWgtTmh/eWRrZG5seWRiYy1MeAVTPY8MHAsOWBAtCY8MBT2PDAk9YWgtRGNuIzwrPSkLDlgJBh4QTH0IDQ6PDAINPY8MBw+PDAwN6ZykBArhcDSOhl4t3jXJvLKXQgdm8ibxMCtqLYc+Z/oAj8LT5q4i9F5nVmkhLW5of3lka2RubHloLX1iYWRudGk4LhhGGFQQvpn6+5GTwl23zFVdI02r+kpAcgVTPRILDlgQLgkVPRu4N6D5AgMNnwa8LBsjedgxANZvGwALBCeLRYv6AAwMCAgNDo8MDA1Rpq58n0peWMyiIky+9fbufcDrrkELDlgQAwkbCRkm3WRKmXsE8/lmgC1OTD2PDC89AAsEJ4tFi/oADAwMmJN3AalKhlbZGzo+xskCQMMZZNwtYmsteWVoLXllaGMtbH19YWRubBKIjogWlDBKOv+klk2DIdm8nR/VjwwNCwQni0WL+m5pCAw9jP89Jwt0LWx+fnhgaH4tbG5uaH15bGNuaHJMpZX03MdrkSlmHN2utukWJ84SjRkm3WRKmXsE8/lmgCNNq/pKQHJjaS1uYmNpZHlkYmN+LWJrLXh+aCeLRYv6AAwMCAgNPW88Bj0ECw5YKe/m3Lp90gJI7CrH/GB14Oq4GhoCkDD+JkQlF8Xzw7i0A9RTEdvGMDuUQSB1uuCBltH+epb/e996PULMXaeH2Nfp8d0ECjq9eHgs");
        private static int[] order = new int[] { 2,57,25,42,6,15,11,7,55,21,54,54,28,59,58,21,28,38,28,27,29,36,35,26,52,45,37,46,55,46,36,39,45,50,54,50,42,58,51,56,44,46,56,58,47,52,56,59,55,56,56,54,59,54,59,58,57,59,59,59,60 };
        private static int key = 13;

        public static readonly bool IsPopulated = true;

        public static byte[] Data() {
        	if (IsPopulated == false)
        		return null;
            return Obfuscator.DeObfuscate(data, order, key);
        }
    }
}
#endif

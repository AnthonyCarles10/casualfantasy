#if UNITY_ANDROID || UNITY_IPHONE || UNITY_STANDALONE_OSX || UNITY_TVOS
// WARNING: Do not modify! Generated file.

namespace UnityEngine.Purchasing.Security {
    public class GooglePlayTangle
    {
        private static byte[] data = System.Convert.FromBase64String("eIYjeC1NB8V4s/umvj0+vyYbqgABszATATw3OBu3ebfGPDAwMDQxMldg7ym/QPkvo55yK4volWDs4aR2HkNS8umrQkEipWb9ztNjou+cySH7yDV8J7/8XixZkjH5dlAKD8NzRiIblHciXKtBfUujAiafdI/k17IIszA+MQGzMDszszAwMdmShBP0K7m/O0cbkn92xEmbA7R2aB82W2GfuE5XfdJ+wBOEJQHd2GDcNdjfiQ1GJFrBD3MKufIIXCLOXKVg8Xsvf82ZZlfBMlqChW2qqW03WmuD1ETODzcJ3onfY4W7oV967wTZ/ABhMp/qMCbxdaAh0Jg0ZiDh1Qzg1VmebUdmu5hg2Y1dts2XSM9Su7yOffKA9L8gT5oDPsfIIDMyMDEw");
        private static int[] order = new int[] { 3,3,2,5,13,11,11,9,10,13,10,13,12,13,14 };
        private static int key = 49;

        public static readonly bool IsPopulated = true;

        public static byte[] Data() {
        	if (IsPopulated == false)
        		return null;
            return Obfuscator.DeObfuscate(data, order, key);
        }
    }
}
#endif
